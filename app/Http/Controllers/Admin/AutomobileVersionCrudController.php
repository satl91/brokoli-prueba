<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AutomobileVersionRequest as StoreRequest;
use App\Http\Requests\AutomobileVersionRequest as UpdateRequest;
use App\Models\AutomobileManufacturer as AutomobileManufacturer;
use App\Models\AutomobileModel as AutomobileModel;
use App\Models\AutomobileFuel as AutomobileFuel;
use App\Models\AutomobileDoor as AutomobileDoor;
use Illuminate\Support\Facades\DB;

class AutomobileVersionCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\AutomobileVersion');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/automobileversion');
        $this->crud->setEntityNameStrings('automobileversion', 'automobile_versions');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'manufacturer',
            'label' => "Manufacturer",
            'type' => 'select2_from_array',
            'options' => AutomobileManufacturer::all()->pluck('str_description','id_manufacturer'),
            'allows_null' => false,
            'allows_multiple' => false
        ]);

        $this->crud->addField([
            'name' => 'model',
            'label' => "Model",
            'type' => 'select2_from_array',
            'options' => AutomobileModel::all()->pluck('str_description','id_model'),
            'allows_null' => false,
            'allows_multiple' => false
        ]);

        $this->crud->addField([
            'name' => 'fuel',
            'label' => "Fuel",
            'type' => 'select2_from_array',
            'options' => AutomobileFuel::all()->where('bol_active','=','1')->pluck('str_description','id_fuel'),
            'allows_null' => false,
            'allows_multiple' => false
        ]);

        $this->crud->addField([
            'name' => 'doors',
            'label' => "Doors",
            'type' => 'select2_from_array',
            'options' => AutomobileDoor::all()->pluck('int_numdoors','id_doors'),
            'allows_null' => false,
            'allows_multiple' => false
        ]);

        $this->crud->addField(['name' => 'str_level', 'type' => 'text', 'label' => 'Level']);
        $this->crud->addField(['name' => 'str_level_slug', 'type' => 'text', 'label' => 'Level (Slug)']);
        $this->crud->addField(['name' => 'int_power', 'type' => 'text', 'label' => 'Power']);
        $this->crud->addField(['name' => 'date_year', 'type' => 'text', 'label' => 'Year']);
        $this->crud->addField(['name' => 'int_month', 'type' => 'text', 'label' => 'Month']);
        $this->crud->addField(['name' => 'bol_active', 'type' => 'checkbox', 'label' => 'Active']);



        //$this->crud->enableAjaxTable();
        //$this->crud->enableExportButtons();


        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        $manufacturer = $request->input('manufacturer');
        $model = $request->input('model');
        $fuel = $request->input('fuel');
        $doors = $request->input('doors');
        $fuel = $request->input('fuel');

        // Now that we have the version manufacturer/model/fuel/doors data we store it on the different tables

        // Check if an "model_rel_fuel" exists
        $id_reg_model_rel_fuel = DB::table('model_rel_fuel')->select('id_reg')->where('id_model','=',$model)->where('id_fuel','=',$fuel)->first();

        // If it doesn't exists then we create one and obtain its ID
        if ($id_reg_model_rel_fuel == null) {
            DB::table('model_rel_fuel')->insert(['id_model' => $model, 'id_fuel' => $fuel]);
            $id_reg_model_rel_fuel = DB::table('model_rel_fuel')->select('id_reg')->where('id_model','=',$model)->where('id_fuel','=',$fuel)->first();
        }
        $id_reg_model_rel_fuel = $id_reg_model_rel_fuel->id_reg;


        // Check if an "model_fuel_numdoors" exists
        $id_reg_model_fuel_numdoors = DB::table('model_fuel_numdoors')->select('id_reg')->where('id_regmf','=',$id_reg_model_rel_fuel)->where('doors','=',$doors)->first();

        // If it doesn't exists then we create one and obtain its ID
        if ($id_reg_model_fuel_numdoors == null) {
            DB::table('model_fuel_numdoors')->insert(['id_regmf' => $id_reg_model_rel_fuel, 'doors' => $doors]);
            $id_reg_model_fuel_numdoors = DB::table('model_fuel_numdoors')->select('id_reg')->where('id_regmf','=',$id_reg_model_rel_fuel)->where('doors','=',$doors)->first();
        }
        $id_reg_model_fuel_numdoors = $id_reg_model_fuel_numdoors->id_reg;

        $request->request->set('id_regmf', $id_reg_model_fuel_numdoors);

        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

}
