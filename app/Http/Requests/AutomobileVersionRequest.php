<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AutomobileVersionRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'manufacturer' => 'required',
            'model' => 'required',
            'fuel' => 'required',
            'doors' => 'required',
            'str_level' => 'required|string',
            'str_level_slug' => 'required|string',
            'int_power' => 'required|integer',
            'date_year' => 'required|integer|min:1800|max:3000',
            'int_month' => 'required|integer|min:1|max:12',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
