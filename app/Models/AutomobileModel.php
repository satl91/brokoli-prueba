<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class AutomobileModel extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'automobile_models';
    protected $primaryKey = 'id_model';
    public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['id_manufacturer', 'str_description', 'str_description_slug', 'bol_active'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function manufacturer() {
        return $this->belongsTo('App\Models\AutomobileManufacturer', 'id_manufacturer');
    }

    public function fuels() {
        return $this->belongsToMany('App\Models\AutomobileFuel', 'model_rel_fuel','id_fuel','id_model');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
