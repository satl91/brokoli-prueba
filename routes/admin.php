<?php

// Backpack\CRUD: Define the resources for the entities you want to CRUD.
CRUD::resource('automobiledoor', 'AutomobileDoorCrudController');
CRUD::resource('automobilefuel', 'AutomobileFuelCrudController');
CRUD::resource('automobilemanufacturer', 'AutomobileManufacturerCrudController');
CRUD::resource('automobilemodel', 'AutomobileModelCrudController');
CRUD::resource('automobiletype', 'AutomobileTypeCrudController');
CRUD::resource('automobileversion', 'AutomobileVersionCrudController');
