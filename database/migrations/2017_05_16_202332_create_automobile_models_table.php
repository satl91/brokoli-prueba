<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutomobileModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('automobile_models', function (Blueprint $table) {
            $table->increments('id_model');
            $table->integer('id_manufacturer');
            $table->string('str_description', 150);
            $table->string('str_description_slug',150);
            $table->boolean('bol_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('automobile_models');
    }
}
