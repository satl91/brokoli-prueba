<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutomobileManufacturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('automobile_manufacturers', function (Blueprint $table) {
            $table->increments('id_manufacturer');
            $table->string('str_description');
            $table->string('str_description_slug');
            $table->boolean('bol_popular')->default(0);
            $table->tinyInteger('id_type');
            $table->boolean('bol_active')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('automobile_manufacturers');
    }
}
