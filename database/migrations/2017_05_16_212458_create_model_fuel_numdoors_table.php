<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelFuelNumdoorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_fuel_numdoors', function (Blueprint $table) {
            $table->increments('id_reg');
            $table->integer('id_regmf');
            $table->tinyInteger('doors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('model_fuel_numdoors');
    }
}
