<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrkAutomobileVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brk_automobile_versions', function (Blueprint $table) {
            $table->increments('id_version');
            $table->integer('id_regmf');
            $table->string('str_level', 120);
            $table->string('str_level_slug', 120);
            $table->smallInteger('int_power');
            $table->smallInteger('date_year');
            $table->tinyInteger('int_month');
            $table->boolean('bol_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('brk_automobile_versions');
    }
}
