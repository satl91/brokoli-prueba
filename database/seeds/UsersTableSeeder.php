<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Test',
                'email' => 'test@test.com',
                'password' => '$2y$10$8ituo8z.0IIUpwnjwOYwA.2tii0ZcfM/o5gfJ1lnmw87iqJD4ONFe',
                'remember_token' => NULL,
                'created_at' => '2017-05-16 19:41:44',
                'updated_at' => '2017-05-16 19:41:44',
            ),
        ));
        
        
    }
}