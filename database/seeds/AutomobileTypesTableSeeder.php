<?php

use Illuminate\Database\Seeder;

class AutomobileTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('automobile_types')->delete();
        
        \DB::table('automobile_types')->insert(array (
            0 => 
            array (
                'id_type' => 1,
                'str_description' => 'Coche',
                'bol_active' => 1,
            ),
            1 => 
            array (
                'id_type' => 2,
                'str_description' => 'Moto',
                'bol_active' => 1,
            ),
        ));
        
        
    }
}