<?php

use Illuminate\Database\Seeder;

class AutomobileManufacturersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('automobile_manufacturers')->delete();
        
        \DB::table('automobile_manufacturers')->insert(array (
            0 => 
            array (
                'id_manufacturer' => 1,
                'str_description' => 'abarth',
                'str_description_slug' => 'abarth',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            1 => 
            array (
                'id_manufacturer' => 2,
                'str_description' => 'ac',
                'str_description_slug' => 'ac',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            2 => 
            array (
                'id_manufacturer' => 3,
                'str_description' => 'acm',
                'str_description_slug' => 'acm',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            3 => 
            array (
                'id_manufacturer' => 4,
                'str_description' => 'acura',
                'str_description_slug' => 'acura',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            4 => 
            array (
                'id_manufacturer' => 5,
                'str_description' => 'adler',
                'str_description_slug' => 'adler',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            5 => 
            array (
                'id_manufacturer' => 6,
                'str_description' => 'adly moto',
                'str_description_slug' => 'adly-moto',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            6 => 
            array (
                'id_manufacturer' => 7,
                'str_description' => 'aeon',
                'str_description_slug' => 'aeon',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            7 => 
            array (
                'id_manufacturer' => 8,
                'str_description' => 'aixam',
                'str_description_slug' => 'aixam',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            8 => 
            array (
                'id_manufacturer' => 9,
                'str_description' => 'albisa-hugar',
                'str_description_slug' => 'albisa-hugar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            9 => 
            array (
                'id_manufacturer' => 10,
                'str_description' => 'aleko',
                'str_description_slug' => 'aleko',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            10 => 
            array (
                'id_manufacturer' => 11,
                'str_description' => 'alfa romeo',
                'str_description_slug' => 'alfa-romeo',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            11 => 
            array (
                'id_manufacturer' => 12,
                'str_description' => 'alpine',
                'str_description_slug' => 'alpine',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            12 => 
            array (
                'id_manufacturer' => 13,
                'str_description' => 'alvis',
                'str_description_slug' => 'alvis',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            13 => 
            array (
                'id_manufacturer' => 14,
                'str_description' => 'amc',
                'str_description_slug' => 'amc',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            14 => 
            array (
                'id_manufacturer' => 15,
                'str_description' => 'amilcar',
                'str_description_slug' => 'amilcar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            15 => 
            array (
                'id_manufacturer' => 16,
                'str_description' => 'anibal',
                'str_description_slug' => 'anibal',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            16 => 
            array (
                'id_manufacturer' => 17,
                'str_description' => 'arctic cat',
                'str_description_slug' => 'arctic-cat',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            17 => 
            array (
                'id_manufacturer' => 18,
                'str_description' => 'argo',
                'str_description_slug' => 'argo',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            18 => 
            array (
                'id_manufacturer' => 19,
                'str_description' => 'armstrong siddeley',
                'str_description_slug' => 'armstrong-siddeley',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            19 => 
            array (
                'id_manufacturer' => 20,
                'str_description' => 'aro',
                'str_description_slug' => 'aro',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            20 => 
            array (
                'id_manufacturer' => 21,
                'str_description' => 'arola',
                'str_description_slug' => 'arola',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            21 => 
            array (
                'id_manufacturer' => 22,
                'str_description' => 'asia motors',
                'str_description_slug' => 'asia-motors',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            22 => 
            array (
                'id_manufacturer' => 23,
                'str_description' => 'asquith',
                'str_description_slug' => 'asquith',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            23 => 
            array (
                'id_manufacturer' => 24,
                'str_description' => 'aston martin',
                'str_description_slug' => 'aston-martin',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            24 => 
            array (
                'id_manufacturer' => 25,
                'str_description' => 'auburn',
                'str_description_slug' => 'auburn',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            25 => 
            array (
                'id_manufacturer' => 26,
                'str_description' => 'audi',
                'str_description_slug' => 'audi',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            26 => 
            array (
                'id_manufacturer' => 27,
                'str_description' => 'ausa',
                'str_description_slug' => 'ausa',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            27 => 
            array (
                'id_manufacturer' => 28,
                'str_description' => 'austin',
                'str_description_slug' => 'austin',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            28 => 
            array (
                'id_manufacturer' => 29,
                'str_description' => 'austin healey',
                'str_description_slug' => 'austin-healey',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            29 => 
            array (
                'id_manufacturer' => 30,
                'str_description' => 'authi',
                'str_description_slug' => 'authi',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            30 => 
            array (
                'id_manufacturer' => 31,
                'str_description' => 'autobianchi',
                'str_description_slug' => 'autobianchi',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            31 => 
            array (
                'id_manufacturer' => 32,
                'str_description' => 'auverland',
                'str_description_slug' => 'auverland',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            32 => 
            array (
                'id_manufacturer' => 33,
                'str_description' => 'avia',
                'str_description_slug' => 'avia',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            33 => 
            array (
                'id_manufacturer' => 34,
                'str_description' => 'axr',
                'str_description_slug' => 'axr',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            34 => 
            array (
                'id_manufacturer' => 35,
                'str_description' => 'axy',
                'str_description_slug' => 'axy',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            35 => 
            array (
                'id_manufacturer' => 36,
                'str_description' => 'azel',
                'str_description_slug' => 'azel',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            36 => 
            array (
                'id_manufacturer' => 37,
                'str_description' => 'babieca',
                'str_description_slug' => 'babieca',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            37 => 
            array (
                'id_manufacturer' => 38,
                'str_description' => 'baijah',
                'str_description_slug' => 'baijah',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            38 => 
            array (
                'id_manufacturer' => 39,
                'str_description' => 'bajaj',
                'str_description_slug' => 'bajaj',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            39 => 
            array (
                'id_manufacturer' => 40,
                'str_description' => 'bakus',
                'str_description_slug' => 'bakus',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            40 => 
            array (
                'id_manufacturer' => 41,
                'str_description' => 'ballot',
                'str_description_slug' => 'ballot',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            41 => 
            array (
                'id_manufacturer' => 42,
                'str_description' => 'baoya',
                'str_description_slug' => 'baoya',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            42 => 
            array (
                'id_manufacturer' => 43,
                'str_description' => 'barreiros',
                'str_description_slug' => 'barreiros',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            43 => 
            array (
                'id_manufacturer' => 44,
                'str_description' => 'bedford',
                'str_description_slug' => 'bedford',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            44 => 
            array (
                'id_manufacturer' => 45,
                'str_description' => 'beepo',
                'str_description_slug' => 'beepo',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            45 => 
            array (
                'id_manufacturer' => 46,
                'str_description' => 'bellier',
                'str_description_slug' => 'bellier',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            46 => 
            array (
                'id_manufacturer' => 47,
                'str_description' => 'bensur',
                'str_description_slug' => 'bensur',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            47 => 
            array (
                'id_manufacturer' => 48,
                'str_description' => 'bentley',
                'str_description_slug' => 'bentley',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            48 => 
            array (
                'id_manufacturer' => 49,
                'str_description' => 'bertone',
                'str_description_slug' => 'bertone',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            49 => 
            array (
                'id_manufacturer' => 50,
                'str_description' => 'biscuter voisin',
                'str_description_slug' => 'biscuter-voisin',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            50 => 
            array (
                'id_manufacturer' => 51,
                'str_description' => 'blucar',
                'str_description_slug' => 'blucar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            51 => 
            array (
                'id_manufacturer' => 52,
                'str_description' => 'bmc',
                'str_description_slug' => 'bmc',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            52 => 
            array (
                'id_manufacturer' => 53,
                'str_description' => 'bmw',
                'str_description_slug' => 'bmw',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            53 => 
            array (
                'id_manufacturer' => 54,
                'str_description' => 'bombardier',
                'str_description_slug' => 'bombardier',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            54 => 
            array (
                'id_manufacturer' => 55,
                'str_description' => 'borgward',
                'str_description_slug' => 'borgward',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            55 => 
            array (
                'id_manufacturer' => 56,
                'str_description' => 'bremach',
                'str_description_slug' => 'bremach',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            56 => 
            array (
                'id_manufacturer' => 57,
                'str_description' => 'british leyland',
                'str_description_slug' => 'british-leyland',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            57 => 
            array (
                'id_manufacturer' => 58,
                'str_description' => 'brm',
                'str_description_slug' => 'brm',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            58 => 
            array (
                'id_manufacturer' => 59,
                'str_description' => 'bsa',
                'str_description_slug' => 'bsa',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            59 => 
            array (
                'id_manufacturer' => 60,
                'str_description' => 'bugato',
                'str_description_slug' => 'bugato',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            60 => 
            array (
                'id_manufacturer' => 61,
                'str_description' => 'bugatti',
                'str_description_slug' => 'bugatti',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            61 => 
            array (
                'id_manufacturer' => 62,
                'str_description' => 'bugs buggies',
                'str_description_slug' => 'bugs-buggies',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            62 => 
            array (
                'id_manufacturer' => 63,
                'str_description' => 'bugway',
                'str_description_slug' => 'bugway',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            63 => 
            array (
                'id_manufacturer' => 64,
                'str_description' => 'buick',
                'str_description_slug' => 'buick',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            64 => 
            array (
                'id_manufacturer' => 65,
                'str_description' => 'buomo',
                'str_description_slug' => 'buomo',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            65 => 
            array (
                'id_manufacturer' => 66,
                'str_description' => 'buyang',
                'str_description_slug' => 'buyang',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            66 => 
            array (
                'id_manufacturer' => 67,
                'str_description' => 'byd',
                'str_description_slug' => 'byd',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            67 => 
            array (
                'id_manufacturer' => 68,
                'str_description' => 'caddycar',
                'str_description_slug' => 'caddycar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            68 => 
            array (
                'id_manufacturer' => 69,
                'str_description' => 'cadillac',
                'str_description_slug' => 'cadillac',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            69 => 
            array (
                'id_manufacturer' => 70,
                'str_description' => 'campagna',
                'str_description_slug' => 'campagna',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            70 => 
            array (
                'id_manufacturer' => 71,
                'str_description' => 'can-am',
                'str_description_slug' => 'can-am',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            71 => 
            array (
                'id_manufacturer' => 72,
                'str_description' => 'carbodies',
                'str_description_slug' => 'carbodies',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            72 => 
            array (
                'id_manufacturer' => 73,
                'str_description' => 'carter',
                'str_description_slug' => 'carter',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            73 => 
            array (
                'id_manufacturer' => 74,
                'str_description' => 'carver',
                'str_description_slug' => 'carver',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            74 => 
            array (
                'id_manufacturer' => 75,
                'str_description' => 'casalini',
                'str_description_slug' => 'casalini',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            75 => 
            array (
                'id_manufacturer' => 76,
                'str_description' => 'caterham',
                'str_description_slug' => 'caterham',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            76 => 
            array (
                'id_manufacturer' => 77,
                'str_description' => 'chatelaine',
                'str_description_slug' => 'chatelaine',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            77 => 
            array (
                'id_manufacturer' => 78,
                'str_description' => 'chatenet',
                'str_description_slug' => 'chatenet',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            78 => 
            array (
                'id_manufacturer' => 79,
                'str_description' => 'chevrolet-gm',
                'str_description_slug' => 'chevrolet-gm',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            79 => 
            array (
                'id_manufacturer' => 80,
                'str_description' => 'chongqing',
                'str_description_slug' => 'chongqing',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            80 => 
            array (
                'id_manufacturer' => 81,
                'str_description' => 'chrysler',
                'str_description_slug' => 'chrysler',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            81 => 
            array (
                'id_manufacturer' => 82,
                'str_description' => 'citroen',
                'str_description_slug' => 'citroen',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            82 => 
            array (
                'id_manufacturer' => 83,
                'str_description' => 'clenet',
                'str_description_slug' => 'clenet',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            83 => 
            array (
                'id_manufacturer' => 84,
                'str_description' => 'clipic',
                'str_description_slug' => 'clipic',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            84 => 
            array (
                'id_manufacturer' => 85,
                'str_description' => 'clubcar',
                'str_description_slug' => 'clubcar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            85 => 
            array (
                'id_manufacturer' => 86,
                'str_description' => 'comai',
                'str_description_slug' => 'comai',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            86 => 
            array (
                'id_manufacturer' => 87,
                'str_description' => 'comarth',
                'str_description_slug' => 'comarth',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            87 => 
            array (
                'id_manufacturer' => 88,
                'str_description' => 'commer cob',
                'str_description_slug' => 'commer-cob',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            88 => 
            array (
                'id_manufacturer' => 89,
                'str_description' => 'corvette',
                'str_description_slug' => 'corvette',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            89 => 
            array (
                'id_manufacturer' => 90,
                'str_description' => 'csr',
                'str_description_slug' => 'csr',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            90 => 
            array (
                'id_manufacturer' => 91,
                'str_description' => 'cushman',
                'str_description_slug' => 'cushman',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            91 => 
            array (
                'id_manufacturer' => 92,
                'str_description' => 'dacia',
                'str_description_slug' => 'dacia',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            92 => 
            array (
                'id_manufacturer' => 93,
                'str_description' => 'daewoo',
                'str_description_slug' => 'daewoo',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            93 => 
            array (
                'id_manufacturer' => 94,
                'str_description' => 'daf',
                'str_description_slug' => 'daf',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            94 => 
            array (
                'id_manufacturer' => 95,
                'str_description' => 'daihatsu',
                'str_description_slug' => 'daihatsu',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            95 => 
            array (
                'id_manufacturer' => 96,
                'str_description' => 'daimler',
                'str_description_slug' => 'daimler',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            96 => 
            array (
                'id_manufacturer' => 97,
                'str_description' => 'datsun',
                'str_description_slug' => 'datsun',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            97 => 
            array (
                'id_manufacturer' => 98,
                'str_description' => 'dax',
                'str_description_slug' => 'dax',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            98 => 
            array (
                'id_manufacturer' => 99,
                'str_description' => 'dazon',
                'str_description_slug' => 'dazon',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            99 => 
            array (
                'id_manufacturer' => 100,
                'str_description' => 'de tomaso',
                'str_description_slug' => 'de-tomaso',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            100 => 
            array (
                'id_manufacturer' => 101,
                'str_description' => 'delage',
                'str_description_slug' => 'delage',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            101 => 
            array (
                'id_manufacturer' => 102,
                'str_description' => 'delahaye',
                'str_description_slug' => 'delahaye',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            102 => 
            array (
                'id_manufacturer' => 103,
                'str_description' => 'delorean',
                'str_description_slug' => 'delorean',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            103 => 
            array (
                'id_manufacturer' => 104,
                'str_description' => 'desoto',
                'str_description_slug' => 'desoto',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            104 => 
            array (
                'id_manufacturer' => 105,
                'str_description' => 'diavolino',
                'str_description_slug' => 'diavolino',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            105 => 
            array (
                'id_manufacturer' => 106,
                'str_description' => 'dic',
                'str_description_slug' => 'dic',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            106 => 
            array (
                'id_manufacturer' => 107,
                'str_description' => 'dilixi',
                'str_description_slug' => 'dilixi',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            107 => 
            array (
                'id_manufacturer' => 108,
                'str_description' => 'dkw',
                'str_description_slug' => 'dkw',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            108 => 
            array (
                'id_manufacturer' => 109,
                'str_description' => 'dodge',
                'str_description_slug' => 'dodge',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            109 => 
            array (
                'id_manufacturer' => 110,
                'str_description' => 'dongfeng',
                'str_description_slug' => 'dongfeng',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            110 => 
            array (
                'id_manufacturer' => 111,
                'str_description' => 'donkervoort',
                'str_description_slug' => 'donkervoort',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            111 => 
            array (
                'id_manufacturer' => 112,
                'str_description' => 'dorton',
                'str_description_slug' => 'dorton',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            112 => 
            array (
                'id_manufacturer' => 113,
                'str_description' => 'due',
                'str_description_slug' => 'due',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            113 => 
            array (
                'id_manufacturer' => 114,
                'str_description' => 'dunax',
                'str_description_slug' => 'dunax',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            114 => 
            array (
                'id_manufacturer' => 115,
                'str_description' => 'duport',
                'str_description_slug' => 'duport',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            115 => 
            array (
                'id_manufacturer' => 116,
                'str_description' => 'durso',
                'str_description_slug' => 'durso',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            116 => 
            array (
                'id_manufacturer' => 117,
                'str_description' => 'e-z-go',
                'str_description_slug' => 'e-z-go',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            117 => 
            array (
                'id_manufacturer' => 118,
                'str_description' => 'eagle',
                'str_description_slug' => 'eagle',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            118 => 
            array (
                'id_manufacturer' => 119,
                'str_description' => 'ebro',
                'str_description_slug' => 'ebro',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            119 => 
            array (
                'id_manufacturer' => 120,
                'str_description' => 'eco&mobilite',
                'str_description_slug' => 'eco&mobilite',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            120 => 
            array (
                'id_manufacturer' => 121,
                'str_description' => 'effedi',
                'str_description_slug' => 'effedi',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            121 => 
            array (
                'id_manufacturer' => 122,
                'str_description' => 'erad',
                'str_description_slug' => 'erad',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            122 => 
            array (
                'id_manufacturer' => 123,
                'str_description' => 'essex',
                'str_description_slug' => 'essex',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            123 => 
            array (
                'id_manufacturer' => 124,
                'str_description' => 'estrima',
                'str_description_slug' => 'estrima',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            124 => 
            array (
                'id_manufacturer' => 125,
                'str_description' => 'evasao',
                'str_description_slug' => 'evasao',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            125 => 
            array (
                'id_manufacturer' => 126,
                'str_description' => 'evf',
                'str_description_slug' => 'evf',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            126 => 
            array (
                'id_manufacturer' => 127,
                'str_description' => 'excalibur',
                'str_description_slug' => 'excalibur',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            127 => 
            array (
                'id_manufacturer' => 128,
                'str_description' => 'faam',
                'str_description_slug' => 'faam',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            128 => 
            array (
                'id_manufacturer' => 129,
                'str_description' => 'facel vega',
                'str_description_slug' => 'facel-vega',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            129 => 
            array (
                'id_manufacturer' => 130,
                'str_description' => 'ferrari',
                'str_description_slug' => 'ferrari',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            130 => 
            array (
                'id_manufacturer' => 131,
                'str_description' => 'fiat',
                'str_description_slug' => 'fiat',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            131 => 
            array (
                'id_manufacturer' => 132,
                'str_description' => 'fiat-iveco',
                'str_description_slug' => 'fiat-iveco',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            132 => 
            array (
                'id_manufacturer' => 133,
                'str_description' => 'fleur de lys',
                'str_description_slug' => 'fleur-de-lys',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            133 => 
            array (
                'id_manufacturer' => 134,
                'str_description' => 'ford',
                'str_description_slug' => 'ford',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            134 => 
            array (
                'id_manufacturer' => 135,
                'str_description' => 'fornasari',
                'str_description_slug' => 'fornasari',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            135 => 
            array (
                'id_manufacturer' => 136,
                'str_description' => 'fsm',
                'str_description_slug' => 'fsm',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            136 => 
            array (
                'id_manufacturer' => 137,
                'str_description' => 'fso',
                'str_description_slug' => 'fso',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            137 => 
            array (
                'id_manufacturer' => 138,
                'str_description' => 'galloper',
                'str_description_slug' => 'galloper',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            138 => 
            array (
                'id_manufacturer' => 139,
                'str_description' => 'garia',
                'str_description_slug' => 'garia',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            139 => 
            array (
                'id_manufacturer' => 140,
                'str_description' => 'gateau',
                'str_description_slug' => 'gateau',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            140 => 
            array (
                'id_manufacturer' => 141,
                'str_description' => 'gaz',
                'str_description_slug' => 'gaz',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            141 => 
            array (
                'id_manufacturer' => 142,
                'str_description' => 'gem',
                'str_description_slug' => 'gem',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            142 => 
            array (
                'id_manufacturer' => 143,
                'str_description' => 'geo',
                'str_description_slug' => 'geo',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            143 => 
            array (
                'id_manufacturer' => 144,
                'str_description' => 'ginetta',
                'str_description_slug' => 'ginetta',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            144 => 
            array (
                'id_manufacturer' => 145,
                'str_description' => 'giotti victoria',
                'str_description_slug' => 'giotti-victoria',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            145 => 
            array (
                'id_manufacturer' => 146,
                'str_description' => 'gmc',
                'str_description_slug' => 'gmc',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            146 => 
            array (
                'id_manufacturer' => 147,
                'str_description' => 'gme',
                'str_description_slug' => 'gme',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            147 => 
            array (
                'id_manufacturer' => 148,
                'str_description' => 'go speed',
                'str_description_slug' => 'go-speed',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            148 => 
            array (
                'id_manufacturer' => 149,
                'str_description' => 'goes',
                'str_description_slug' => 'goes',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            149 => 
            array (
                'id_manufacturer' => 150,
                'str_description' => 'goggomobil',
                'str_description_slug' => 'goggomobil',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            150 => 
            array (
                'id_manufacturer' => 151,
                'str_description' => 'goka',
                'str_description_slug' => 'goka',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            151 => 
            array (
                'id_manufacturer' => 152,
                'str_description' => 'gordon',
                'str_description_slug' => 'gordon',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            152 => 
            array (
                'id_manufacturer' => 153,
                'str_description' => 'goupil',
                'str_description_slug' => 'goupil',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            153 => 
            array (
                'id_manufacturer' => 154,
                'str_description' => 'grahan paige',
                'str_description_slug' => 'grahan-paige',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            154 => 
            array (
                'id_manufacturer' => 155,
                'str_description' => 'grecav',
                'str_description_slug' => 'grecav',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            155 => 
            array (
                'id_manufacturer' => 156,
                'str_description' => 'greencar',
                'str_description_slug' => 'greencar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            156 => 
            array (
                'id_manufacturer' => 157,
                'str_description' => 'gs-moon',
                'str_description_slug' => 'gs-moon',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            157 => 
            array (
                'id_manufacturer' => 158,
                'str_description' => 'gurgel',
                'str_description_slug' => 'gurgel',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            158 => 
            array (
                'id_manufacturer' => 159,
                'str_description' => 'hillman',
                'str_description_slug' => 'hillman',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            159 => 
            array (
                'id_manufacturer' => 160,
                'str_description' => 'hispano aleman',
                'str_description_slug' => 'hispano-aleman',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            160 => 
            array (
                'id_manufacturer' => 161,
                'str_description' => 'hispanosuiza',
                'str_description_slug' => 'hispanosuiza',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            161 => 
            array (
                'id_manufacturer' => 162,
                'str_description' => 'hodgep',
                'str_description_slug' => 'hodgep',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            162 => 
            array (
                'id_manufacturer' => 163,
                'str_description' => 'holden',
                'str_description_slug' => 'holden',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            163 => 
            array (
                'id_manufacturer' => 164,
                'str_description' => 'honda',
                'str_description_slug' => 'honda',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            164 => 
            array (
                'id_manufacturer' => 165,
                'str_description' => 'hongdu',
                'str_description_slug' => 'hongdu',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            165 => 
            array (
                'id_manufacturer' => 166,
                'str_description' => 'hsun',
                'str_description_slug' => 'hsun',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            166 => 
            array (
                'id_manufacturer' => 167,
                'str_description' => 'hudson',
                'str_description_slug' => 'hudson',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            167 => 
            array (
                'id_manufacturer' => 168,
                'str_description' => 'humber',
                'str_description_slug' => 'humber',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            168 => 
            array (
                'id_manufacturer' => 169,
                'str_description' => 'hummer',
                'str_description_slug' => 'hummer',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            169 => 
            array (
                'id_manufacturer' => 170,
                'str_description' => 'hurtan',
                'str_description_slug' => 'hurtan',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            170 => 
            array (
                'id_manufacturer' => 171,
                'str_description' => 'hyundai',
                'str_description_slug' => 'hyundai',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            171 => 
            array (
                'id_manufacturer' => 172,
                'str_description' => 'iato',
                'str_description_slug' => 'iato',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            172 => 
            array (
                'id_manufacturer' => 173,
                'str_description' => 'ifa multicar',
                'str_description_slug' => 'ifa-multicar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            173 => 
            array (
                'id_manufacturer' => 174,
                'str_description' => 'incalcu',
                'str_description_slug' => 'incalcu',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            174 => 
            array (
                'id_manufacturer' => 175,
                'str_description' => 'infiniti',
                'str_description_slug' => 'infiniti',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            175 => 
            array (
                'id_manufacturer' => 176,
                'str_description' => 'innocenti',
                'str_description_slug' => 'innocenti',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            176 => 
            array (
                'id_manufacturer' => 177,
            'str_description' => 'inter harv (ihc)',
            'str_description_slug' => 'inter-harv-(ihc)',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            177 => 
            array (
                'id_manufacturer' => 178,
                'str_description' => 'internacional',
                'str_description_slug' => 'internacional',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            178 => 
            array (
                'id_manufacturer' => 179,
            'str_description' => 'international (ihc)',
            'str_description_slug' => 'international-(ihc)',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            179 => 
            array (
                'id_manufacturer' => 180,
                'str_description' => 'isuzu',
                'str_description_slug' => 'isuzu',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            180 => 
            array (
                'id_manufacturer' => 181,
                'str_description' => 'italcar',
                'str_description_slug' => 'italcar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            181 => 
            array (
                'id_manufacturer' => 182,
                'str_description' => 'iveco',
                'str_description_slug' => 'iveco',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            182 => 
            array (
                'id_manufacturer' => 183,
                'str_description' => 'jago',
                'str_description_slug' => 'jago',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            183 => 
            array (
                'id_manufacturer' => 184,
                'str_description' => 'jaguar',
                'str_description_slug' => 'jaguar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            184 => 
            array (
                'id_manufacturer' => 185,
                'str_description' => 'jdm-simpa',
                'str_description_slug' => 'jdm-simpa',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            185 => 
            array (
                'id_manufacturer' => 186,
                'str_description' => 'jeep',
                'str_description_slug' => 'jeep',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            186 => 
            array (
                'id_manufacturer' => 187,
                'str_description' => 'jeep-viasa',
                'str_description_slug' => 'jeep-viasa',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            187 => 
            array (
                'id_manufacturer' => 188,
                'str_description' => 'jensen',
                'str_description_slug' => 'jensen',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            188 => 
            array (
                'id_manufacturer' => 189,
                'str_description' => 'jmstar',
                'str_description_slug' => 'jmstar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            189 => 
            array (
                'id_manufacturer' => 190,
                'str_description' => 'jocsports',
                'str_description_slug' => 'jocsports',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            190 => 
            array (
                'id_manufacturer' => 191,
                'str_description' => 'john deere',
                'str_description_slug' => 'john-deere',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            191 => 
            array (
                'id_manufacturer' => 192,
                'str_description' => 'jowett',
                'str_description_slug' => 'jowett',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            192 => 
            array (
                'id_manufacturer' => 193,
                'str_description' => 'joyner',
                'str_description_slug' => 'joyner',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            193 => 
            array (
                'id_manufacturer' => 194,
                'str_description' => 'kaiser',
                'str_description_slug' => 'kaiser',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            194 => 
            array (
                'id_manufacturer' => 195,
                'str_description' => 'kandi',
                'str_description_slug' => 'kandi',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            195 => 
            array (
                'id_manufacturer' => 196,
                'str_description' => 'kartex',
                'str_description_slug' => 'kartex',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            196 => 
            array (
                'id_manufacturer' => 197,
                'str_description' => 'kasea',
                'str_description_slug' => 'kasea',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            197 => 
            array (
                'id_manufacturer' => 198,
                'str_description' => 'kawasaki',
                'str_description_slug' => 'kawasaki',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            198 => 
            array (
                'id_manufacturer' => 199,
                'str_description' => 'kewet',
                'str_description_slug' => 'kewet',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            199 => 
            array (
                'id_manufacturer' => 200,
                'str_description' => 'kia',
                'str_description_slug' => 'kia',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            200 => 
            array (
                'id_manufacturer' => 201,
                'str_description' => 'kinroad',
                'str_description_slug' => 'kinroad',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            201 => 
            array (
                'id_manufacturer' => 202,
                'str_description' => 'kissel',
                'str_description_slug' => 'kissel',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            202 => 
            array (
                'id_manufacturer' => 203,
                'str_description' => 'ktm',
                'str_description_slug' => 'ktm',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            203 => 
            array (
                'id_manufacturer' => 204,
                'str_description' => 'l.d.v.',
                'str_description_slug' => 'l.d.v.',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            204 => 
            array (
                'id_manufacturer' => 205,
                'str_description' => 'lada',
                'str_description_slug' => 'lada',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            205 => 
            array (
                'id_manufacturer' => 206,
                'str_description' => 'lagonda',
                'str_description_slug' => 'lagonda',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            206 => 
            array (
                'id_manufacturer' => 207,
                'str_description' => 'lam',
                'str_description_slug' => 'lam',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            207 => 
            array (
                'id_manufacturer' => 208,
                'str_description' => 'lamborghini',
                'str_description_slug' => 'lamborghini',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            208 => 
            array (
                'id_manufacturer' => 209,
                'str_description' => 'lancia',
                'str_description_slug' => 'lancia',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            209 => 
            array (
                'id_manufacturer' => 210,
                'str_description' => 'land rover',
                'str_description_slug' => 'land-rover',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            210 => 
            array (
                'id_manufacturer' => 211,
                'str_description' => 'langqing',
                'str_description_slug' => 'langqing',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            211 => 
            array (
                'id_manufacturer' => 212,
                'str_description' => 'lanvertti',
                'str_description_slug' => 'lanvertti',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            212 => 
            array (
                'id_manufacturer' => 213,
                'str_description' => 'le zebre',
                'str_description_slug' => 'le-zebre',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            213 => 
            array (
                'id_manufacturer' => 214,
                'str_description' => 'leomar',
                'str_description_slug' => 'leomar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            214 => 
            array (
                'id_manufacturer' => 215,
                'str_description' => 'lexus',
                'str_description_slug' => 'lexus',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            215 => 
            array (
                'id_manufacturer' => 216,
                'str_description' => 'leyland',
                'str_description_slug' => 'leyland',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            216 => 
            array (
                'id_manufacturer' => 217,
                'str_description' => 'ligier',
                'str_description_slug' => 'ligier',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            217 => 
            array (
                'id_manufacturer' => 218,
                'str_description' => 'lincoln',
                'str_description_slug' => 'lincoln',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            218 => 
            array (
                'id_manufacturer' => 219,
                'str_description' => 'linhai',
                'str_description_slug' => 'linhai',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            219 => 
            array (
                'id_manufacturer' => 220,
                'str_description' => 'little',
                'str_description_slug' => 'little',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            220 => 
            array (
                'id_manufacturer' => 221,
                'str_description' => 'lloyd',
                'str_description_slug' => 'lloyd',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            221 => 
            array (
                'id_manufacturer' => 222,
                'str_description' => 'lotus',
                'str_description_slug' => 'lotus',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            222 => 
            array (
                'id_manufacturer' => 223,
                'str_description' => 'lti carbodies',
                'str_description_slug' => 'lti-carbodies',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            223 => 
            array (
                'id_manufacturer' => 224,
                'str_description' => 'mahindra',
                'str_description_slug' => 'mahindra',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            224 => 
            array (
                'id_manufacturer' => 225,
                'str_description' => 'man',
                'str_description_slug' => 'man',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            225 => 
            array (
                'id_manufacturer' => 226,
                'str_description' => 'mar',
                'str_description_slug' => 'mar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            226 => 
            array (
                'id_manufacturer' => 227,
                'str_description' => 'marcos',
                'str_description_slug' => 'marcos',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            227 => 
            array (
                'id_manufacturer' => 228,
                'str_description' => 'marden',
                'str_description_slug' => 'marden',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            228 => 
            array (
                'id_manufacturer' => 229,
                'str_description' => 'marlin',
                'str_description_slug' => 'marlin',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            229 => 
            array (
                'id_manufacturer' => 230,
                'str_description' => 'maserati',
                'str_description_slug' => 'maserati',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            230 => 
            array (
                'id_manufacturer' => 231,
                'str_description' => 'matford',
                'str_description_slug' => 'matford',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            231 => 
            array (
                'id_manufacturer' => 232,
                'str_description' => 'matra',
                'str_description_slug' => 'matra',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            232 => 
            array (
                'id_manufacturer' => 233,
                'str_description' => 'maybach',
                'str_description_slug' => 'maybach',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            233 => 
            array (
                'id_manufacturer' => 234,
                'str_description' => 'mazda',
                'str_description_slug' => 'mazda',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            234 => 
            array (
                'id_manufacturer' => 235,
                'str_description' => 'mcc',
                'str_description_slug' => 'mcc',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            235 => 
            array (
                'id_manufacturer' => 236,
                'str_description' => 'mclaren',
                'str_description_slug' => 'mclaren',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            236 => 
            array (
                'id_manufacturer' => 237,
                'str_description' => 'mega',
                'str_description_slug' => 'mega',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            237 => 
            array (
                'id_manufacturer' => 238,
                'str_description' => 'meitian',
                'str_description_slug' => 'meitian',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            238 => 
            array (
                'id_manufacturer' => 239,
                'str_description' => 'meko',
                'str_description_slug' => 'meko',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            239 => 
            array (
                'id_manufacturer' => 240,
                'str_description' => 'melex',
                'str_description_slug' => 'melex',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            240 => 
            array (
                'id_manufacturer' => 241,
                'str_description' => 'mercedes',
                'str_description_slug' => 'mercedes',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            241 => 
            array (
                'id_manufacturer' => 242,
                'str_description' => 'mercury',
                'str_description_slug' => 'mercury',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            242 => 
            array (
                'id_manufacturer' => 243,
                'str_description' => 'meta',
                'str_description_slug' => 'meta',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            243 => 
            array (
                'id_manufacturer' => 244,
                'str_description' => 'mg',
                'str_description_slug' => 'mg',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            244 => 
            array (
                'id_manufacturer' => 245,
                'str_description' => 'microcar',
                'str_description_slug' => 'microcar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            245 => 
            array (
                'id_manufacturer' => 246,
                'str_description' => 'microclasic',
                'str_description_slug' => 'microclasic',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            246 => 
            array (
                'id_manufacturer' => 247,
                'str_description' => 'mij',
                'str_description_slug' => 'mij',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            247 => 
            array (
                'id_manufacturer' => 248,
                'str_description' => 'minauto',
                'str_description_slug' => 'minauto',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            248 => 
            array (
                'id_manufacturer' => 249,
                'str_description' => 'minelli',
                'str_description_slug' => 'minelli',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            249 => 
            array (
                'id_manufacturer' => 250,
                'str_description' => 'mini',
                'str_description_slug' => 'mini',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            250 => 
            array (
                'id_manufacturer' => 251,
                'str_description' => 'mini hummer',
                'str_description_slug' => 'mini-hummer',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            251 => 
            array (
                'id_manufacturer' => 252,
                'str_description' => 'mitsubishi',
                'str_description_slug' => 'mitsubishi',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            252 => 
            array (
                'id_manufacturer' => 253,
                'str_description' => 'mitsuoka',
                'str_description_slug' => 'mitsuoka',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            253 => 
            array (
                'id_manufacturer' => 254,
                'str_description' => 'miura',
                'str_description_slug' => 'miura',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            254 => 
            array (
                'id_manufacturer' => 255,
                'str_description' => 'monteverdi',
                'str_description_slug' => 'monteverdi',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            255 => 
            array (
                'id_manufacturer' => 256,
                'str_description' => 'moon',
                'str_description_slug' => 'moon',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            256 => 
            array (
                'id_manufacturer' => 257,
                'str_description' => 'morgan',
                'str_description_slug' => 'morgan',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            257 => 
            array (
                'id_manufacturer' => 258,
                'str_description' => 'morris',
                'str_description_slug' => 'morris',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            258 => 
            array (
                'id_manufacturer' => 259,
                'str_description' => 'moskvich',
                'str_description_slug' => 'moskvich',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            259 => 
            array (
                'id_manufacturer' => 260,
                'str_description' => 'motivas',
                'str_description_slug' => 'motivas',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            260 => 
            array (
                'id_manufacturer' => 261,
                'str_description' => 'mowag',
                'str_description_slug' => 'mowag',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            261 => 
            array (
                'id_manufacturer' => 262,
                'str_description' => 'mplafer',
                'str_description_slug' => 'mplafer',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            262 => 
            array (
                'id_manufacturer' => 263,
                'str_description' => 'mtr',
                'str_description_slug' => 'mtr',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            263 => 
            array (
                'id_manufacturer' => 264,
                'str_description' => 'multicar',
                'str_description_slug' => 'multicar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            264 => 
            array (
                'id_manufacturer' => 265,
                'str_description' => 'mv',
                'str_description_slug' => 'mv',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            265 => 
            array (
                'id_manufacturer' => 266,
                'str_description' => 'mx motor',
                'str_description_slug' => 'mx-motor',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            266 => 
            array (
                'id_manufacturer' => 267,
                'str_description' => 'nanfang',
                'str_description_slug' => 'nanfang',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            267 => 
            array (
                'id_manufacturer' => 268,
                'str_description' => 'nanyi',
                'str_description_slug' => 'nanyi',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            268 => 
            array (
                'id_manufacturer' => 269,
                'str_description' => 'nash',
                'str_description_slug' => 'nash',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            269 => 
            array (
                'id_manufacturer' => 270,
                'str_description' => 'nbluck',
                'str_description_slug' => 'nbluck',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            270 => 
            array (
                'id_manufacturer' => 271,
                'str_description' => 'nissan',
                'str_description_slug' => 'nissan',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            271 => 
            array (
                'id_manufacturer' => 272,
                'str_description' => 'nsu',
                'str_description_slug' => 'nsu',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            272 => 
            array (
                'id_manufacturer' => 273,
                'str_description' => 'o.m.',
                'str_description_slug' => 'o.m.',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            273 => 
            array (
                'id_manufacturer' => 274,
                'str_description' => 'oakland',
                'str_description_slug' => 'oakland',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            274 => 
            array (
                'id_manufacturer' => 275,
                'str_description' => 'oelle',
                'str_description_slug' => 'oelle',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            275 => 
            array (
                'id_manufacturer' => 276,
                'str_description' => 'oldsmobile',
                'str_description_slug' => 'oldsmobile',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            276 => 
            array (
                'id_manufacturer' => 277,
                'str_description' => 'omnia',
                'str_description_slug' => 'omnia',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            277 => 
            array (
                'id_manufacturer' => 278,
                'str_description' => 'opel',
                'str_description_slug' => 'opel',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            278 => 
            array (
                'id_manufacturer' => 279,
                'str_description' => 'packard',
                'str_description_slug' => 'packard',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            279 => 
            array (
                'id_manufacturer' => 280,
                'str_description' => 'pagani',
                'str_description_slug' => 'pagani',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            280 => 
            array (
                'id_manufacturer' => 281,
                'str_description' => 'panther',
                'str_description_slug' => 'panther',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            281 => 
            array (
                'id_manufacturer' => 282,
                'str_description' => 'pegaso',
                'str_description_slug' => 'pegaso',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            282 => 
            array (
                'id_manufacturer' => 283,
                'str_description' => 'peugeot',
                'str_description_slug' => 'peugeot',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            283 => 
            array (
                'id_manufacturer' => 284,
                'str_description' => 'pgo',
                'str_description_slug' => 'pgo',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            284 => 
            array (
                'id_manufacturer' => 285,
                'str_description' => 'piaggio',
                'str_description_slug' => 'piaggio',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            285 => 
            array (
                'id_manufacturer' => 286,
                'str_description' => 'piaggio-vespa',
                'str_description_slug' => 'piaggio-vespa',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            286 => 
            array (
                'id_manufacturer' => 287,
                'str_description' => 'pierce arrow',
                'str_description_slug' => 'pierce-arrow',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            287 => 
            array (
                'id_manufacturer' => 288,
                'str_description' => 'pilgrim',
                'str_description_slug' => 'pilgrim',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            288 => 
            array (
                'id_manufacturer' => 289,
                'str_description' => 'plymouth',
                'str_description_slug' => 'plymouth',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            289 => 
            array (
                'id_manufacturer' => 290,
                'str_description' => 'polaris',
                'str_description_slug' => 'polaris',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            290 => 
            array (
                'id_manufacturer' => 291,
                'str_description' => 'pontiac',
                'str_description_slug' => 'pontiac',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            291 => 
            array (
                'id_manufacturer' => 292,
                'str_description' => 'porsche',
                'str_description_slug' => 'porsche',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            292 => 
            array (
                'id_manufacturer' => 293,
                'str_description' => 'praga',
                'str_description_slug' => 'praga',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            293 => 
            array (
                'id_manufacturer' => 294,
                'str_description' => 'pride',
                'str_description_slug' => 'pride',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            294 => 
            array (
                'id_manufacturer' => 295,
                'str_description' => 'proton',
                'str_description_slug' => 'proton',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            295 => 
            array (
                'id_manufacturer' => 296,
                'str_description' => 'ptv',
                'str_description_slug' => 'ptv',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            296 => 
            array (
                'id_manufacturer' => 297,
                'str_description' => 'puli',
                'str_description_slug' => 'puli',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            297 => 
            array (
                'id_manufacturer' => 298,
                'str_description' => 'puma',
                'str_description_slug' => 'puma',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            298 => 
            array (
                'id_manufacturer' => 299,
                'str_description' => 'qinyuan',
                'str_description_slug' => 'qinyuan',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            299 => 
            array (
                'id_manufacturer' => 300,
                'str_description' => 'qvale',
                'str_description_slug' => 'qvale',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            300 => 
            array (
                'id_manufacturer' => 301,
                'str_description' => 'rambler',
                'str_description_slug' => 'rambler',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            301 => 
            array (
                'id_manufacturer' => 302,
                'str_description' => 'ranger',
                'str_description_slug' => 'ranger',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            302 => 
            array (
                'id_manufacturer' => 303,
                'str_description' => 'rayton',
                'str_description_slug' => 'rayton',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            303 => 
            array (
                'id_manufacturer' => 304,
                'str_description' => 'rbs',
                'str_description_slug' => 'rbs',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            304 => 
            array (
                'id_manufacturer' => 305,
                'str_description' => 'reliant',
                'str_description_slug' => 'reliant',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            305 => 
            array (
                'id_manufacturer' => 306,
                'str_description' => 'renault',
                'str_description_slug' => 'renault',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            306 => 
            array (
                'id_manufacturer' => 307,
                'str_description' => 'renli',
                'str_description_slug' => 'renli',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            307 => 
            array (
                'id_manufacturer' => 308,
                'str_description' => 'reva',
                'str_description_slug' => 'reva',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            308 => 
            array (
                'id_manufacturer' => 309,
                'str_description' => 'riley',
                'str_description_slug' => 'riley',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            309 => 
            array (
                'id_manufacturer' => 310,
                'str_description' => 'rivolta',
                'str_description_slug' => 'rivolta',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            310 => 
            array (
                'id_manufacturer' => 311,
                'str_description' => 'rockne',
                'str_description_slug' => 'rockne',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            311 => 
            array (
                'id_manufacturer' => 312,
                'str_description' => 'rolls royce',
                'str_description_slug' => 'rolls-royce',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            312 => 
            array (
                'id_manufacturer' => 313,
                'str_description' => 'route buggy',
                'str_description_slug' => 'route-buggy',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            313 => 
            array (
                'id_manufacturer' => 314,
                'str_description' => 'rover',
                'str_description_slug' => 'rover',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            314 => 
            array (
                'id_manufacturer' => 315,
                'str_description' => 'saab',
                'str_description_slug' => 'saab',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            315 => 
            array (
                'id_manufacturer' => 316,
                'str_description' => 'saier',
                'str_description_slug' => 'saier',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            316 => 
            array (
                'id_manufacturer' => 317,
                'str_description' => 'saiting',
                'str_description_slug' => 'saiting',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            317 => 
            array (
                'id_manufacturer' => 318,
                'str_description' => 'samand',
                'str_description_slug' => 'samand',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            318 => 
            array (
                'id_manufacturer' => 319,
                'str_description' => 'samsung',
                'str_description_slug' => 'samsung',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            319 => 
            array (
                'id_manufacturer' => 320,
                'str_description' => 'santana',
                'str_description_slug' => 'santana',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            320 => 
            array (
                'id_manufacturer' => 321,
                'str_description' => 'saturn',
                'str_description_slug' => 'saturn',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            321 => 
            array (
                'id_manufacturer' => 322,
                'str_description' => 'sava',
                'str_description_slug' => 'sava',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            322 => 
            array (
                'id_manufacturer' => 323,
                'str_description' => 'saviem',
                'str_description_slug' => 'saviem',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            323 => 
            array (
                'id_manufacturer' => 324,
                'str_description' => 'scam',
                'str_description_slug' => 'scam',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            324 => 
            array (
                'id_manufacturer' => 325,
                'str_description' => 'scion',
                'str_description_slug' => 'scion',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            325 => 
            array (
                'id_manufacturer' => 326,
                'str_description' => 'seat',
                'str_description_slug' => 'seat',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            326 => 
            array (
                'id_manufacturer' => 327,
                'str_description' => 'secma',
                'str_description_slug' => 'secma',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            327 => 
            array (
                'id_manufacturer' => 328,
                'str_description' => 'shelby',
                'str_description_slug' => 'shelby',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            328 => 
            array (
                'id_manufacturer' => 329,
                'str_description' => 'shengqibao',
                'str_description_slug' => 'shengqibao',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            329 => 
            array (
                'id_manufacturer' => 330,
                'str_description' => 'shenzhen',
                'str_description_slug' => 'shenzhen',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            330 => 
            array (
                'id_manufacturer' => 331,
                'str_description' => 'shuanghuan',
                'str_description_slug' => 'shuanghuan',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            331 => 
            array (
                'id_manufacturer' => 332,
                'str_description' => 'siata',
                'str_description_slug' => 'siata',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            332 => 
            array (
                'id_manufacturer' => 333,
                'str_description' => 'simca',
                'str_description_slug' => 'simca',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            333 => 
            array (
                'id_manufacturer' => 334,
                'str_description' => 'singer',
                'str_description_slug' => 'singer',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            334 => 
            array (
                'id_manufacturer' => 335,
                'str_description' => 'sive',
                'str_description_slug' => 'sive',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            335 => 
            array (
                'id_manufacturer' => 336,
                'str_description' => 'skoda',
                'str_description_slug' => 'skoda',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            336 => 
            array (
                'id_manufacturer' => 337,
                'str_description' => 'smart',
                'str_description_slug' => 'smart',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            337 => 
            array (
                'id_manufacturer' => 338,
                'str_description' => 'smc',
                'str_description_slug' => 'smc',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            338 => 
            array (
                'id_manufacturer' => 339,
                'str_description' => 'smithco',
                'str_description_slug' => 'smithco',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            339 => 
            array (
                'id_manufacturer' => 340,
                'str_description' => 'sokon',
                'str_description_slug' => 'sokon',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            340 => 
            array (
                'id_manufacturer' => 341,
                'str_description' => 'ssangyong',
                'str_description_slug' => 'ssangyong',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            341 => 
            array (
                'id_manufacturer' => 342,
                'str_description' => 'start lab',
                'str_description_slug' => 'start-lab',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            342 => 
            array (
                'id_manufacturer' => 343,
                'str_description' => 'steyr-puch',
                'str_description_slug' => 'steyr-puch',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            343 => 
            array (
                'id_manufacturer' => 344,
                'str_description' => 'studebaker',
                'str_description_slug' => 'studebaker',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            344 => 
            array (
                'id_manufacturer' => 345,
                'str_description' => 'subaru',
                'str_description_slug' => 'subaru',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            345 => 
            array (
                'id_manufacturer' => 346,
                'str_description' => 'sunbeam',
                'str_description_slug' => 'sunbeam',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            346 => 
            array (
                'id_manufacturer' => 347,
                'str_description' => 'superbyke',
                'str_description_slug' => 'superbyke',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            347 => 
            array (
                'id_manufacturer' => 348,
                'str_description' => 'suzhou',
                'str_description_slug' => 'suzhou',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            348 => 
            array (
                'id_manufacturer' => 349,
                'str_description' => 'suzuki',
                'str_description_slug' => 'suzuki',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            349 => 
            array (
                'id_manufacturer' => 350,
                'str_description' => 'suzuki-santana',
                'str_description_slug' => 'suzuki-santana',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            350 => 
            array (
                'id_manufacturer' => 351,
                'str_description' => 'talbot',
                'str_description_slug' => 'talbot',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            351 => 
            array (
                'id_manufacturer' => 352,
                'str_description' => 'tarpan',
                'str_description_slug' => 'tarpan',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            352 => 
            array (
                'id_manufacturer' => 353,
                'str_description' => 'tasso',
                'str_description_slug' => 'tasso',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            353 => 
            array (
                'id_manufacturer' => 354,
                'str_description' => 'tata',
                'str_description_slug' => 'tata',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            354 => 
            array (
                'id_manufacturer' => 355,
                'str_description' => 'taylor dunn',
                'str_description_slug' => 'taylor-dunn',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            355 => 
            array (
                'id_manufacturer' => 356,
                'str_description' => 'tazzari',
                'str_description_slug' => 'tazzari',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            356 => 
            array (
                'id_manufacturer' => 357,
                'str_description' => 'teener',
                'str_description_slug' => 'teener',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            357 => 
            array (
                'id_manufacturer' => 358,
                'str_description' => 'teilhol',
                'str_description_slug' => 'teilhol',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            358 => 
            array (
                'id_manufacturer' => 359,
                'str_description' => 'tesla',
                'str_description_slug' => 'tesla',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            359 => 
            array (
                'id_manufacturer' => 360,
                'str_description' => 'teycars',
                'str_description_slug' => 'teycars',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            360 => 
            array (
                'id_manufacturer' => 361,
                'str_description' => 'think',
                'str_description_slug' => 'think',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            361 => 
            array (
                'id_manufacturer' => 362,
                'str_description' => 'tomberlin',
                'str_description_slug' => 'tomberlin',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            362 => 
            array (
                'id_manufacturer' => 363,
                'str_description' => 'tomcar',
                'str_description_slug' => 'tomcar',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            363 => 
            array (
                'id_manufacturer' => 364,
                'str_description' => 'town life',
                'str_description_slug' => 'town-life',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            364 => 
            array (
                'id_manufacturer' => 365,
                'str_description' => 'toyota',
                'str_description_slug' => 'toyota',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            365 => 
            array (
                'id_manufacturer' => 366,
                'str_description' => 'trabant',
                'str_description_slug' => 'trabant',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            366 => 
            array (
                'id_manufacturer' => 367,
                'str_description' => 'tramontana',
                'str_description_slug' => 'tramontana',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            367 => 
            array (
                'id_manufacturer' => 368,
                'str_description' => 'transport 2000',
                'str_description_slug' => 'transport-2000',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            368 => 
            array (
                'id_manufacturer' => 369,
                'str_description' => 'trigger',
                'str_description_slug' => 'trigger',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            369 => 
            array (
                'id_manufacturer' => 370,
                'str_description' => 'triumph',
                'str_description_slug' => 'triumph',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            370 => 
            array (
                'id_manufacturer' => 371,
                'str_description' => 'tron',
                'str_description_slug' => 'tron',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            371 => 
            array (
                'id_manufacturer' => 372,
                'str_description' => 'tugger',
                'str_description_slug' => 'tugger',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            372 => 
            array (
                'id_manufacturer' => 373,
                'str_description' => 'tvr',
                'str_description_slug' => 'tvr',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            373 => 
            array (
                'id_manufacturer' => 374,
                'str_description' => 'twike',
                'str_description_slug' => 'twike',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            374 => 
            array (
                'id_manufacturer' => 375,
                'str_description' => 'uaz',
                'str_description_slug' => 'uaz',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            375 => 
            array (
                'id_manufacturer' => 376,
                'str_description' => 'ultima',
                'str_description_slug' => 'ultima',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            376 => 
            array (
                'id_manufacturer' => 377,
                'str_description' => 'umm',
                'str_description_slug' => 'umm',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            377 => 
            array (
                'id_manufacturer' => 378,
                'str_description' => 'unic',
                'str_description_slug' => 'unic',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            378 => 
            array (
                'id_manufacturer' => 379,
                'str_description' => 'vauxhall',
                'str_description_slug' => 'vauxhall',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            379 => 
            array (
                'id_manufacturer' => 380,
                'str_description' => 'vaz',
                'str_description_slug' => 'vaz',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            380 => 
            array (
                'id_manufacturer' => 381,
                'str_description' => 'veloclasic',
                'str_description_slug' => 'veloclasic',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            381 => 
            array (
                'id_manufacturer' => 382,
                'str_description' => 'venturi',
                'str_description_slug' => 'venturi',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            382 => 
            array (
                'id_manufacturer' => 383,
                'str_description' => 'vexel',
                'str_description_slug' => 'vexel',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            383 => 
            array (
                'id_manufacturer' => 384,
                'str_description' => 'viasa',
                'str_description_slug' => 'viasa',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            384 => 
            array (
                'id_manufacturer' => 385,
                'str_description' => 'vidi',
                'str_description_slug' => 'vidi',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            385 => 
            array (
                'id_manufacturer' => 386,
                'str_description' => 'visita',
                'str_description_slug' => 'visita',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            386 => 
            array (
                'id_manufacturer' => 387,
                'str_description' => 'volkswagen',
                'str_description_slug' => 'volkswagen',
                'bol_popular' => 1,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            387 => 
            array (
                'id_manufacturer' => 388,
                'str_description' => 'volta',
                'str_description_slug' => 'volta',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            388 => 
            array (
                'id_manufacturer' => 389,
                'str_description' => 'volteis',
                'str_description_slug' => 'volteis',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            389 => 
            array (
                'id_manufacturer' => 390,
                'str_description' => 'volvo',
                'str_description_slug' => 'volvo',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            390 => 
            array (
                'id_manufacturer' => 391,
                'str_description' => 'vst',
                'str_description_slug' => 'vst',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            391 => 
            array (
                'id_manufacturer' => 392,
                'str_description' => 'warszawa',
                'str_description_slug' => 'warszawa',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            392 => 
            array (
                'id_manufacturer' => 393,
                'str_description' => 'wartburg',
                'str_description_slug' => 'wartburg',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            393 => 
            array (
                'id_manufacturer' => 394,
                'str_description' => 'westfield',
                'str_description_slug' => 'westfield',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            394 => 
            array (
                'id_manufacturer' => 395,
                'str_description' => 'wiesmann',
                'str_description_slug' => 'wiesmann',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            395 => 
            array (
                'id_manufacturer' => 396,
                'str_description' => 'wildlander',
                'str_description_slug' => 'wildlander',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            396 => 
            array (
                'id_manufacturer' => 397,
                'str_description' => 'willys',
                'str_description_slug' => 'willys',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            397 => 
            array (
                'id_manufacturer' => 398,
                'str_description' => 'wolseley',
                'str_description_slug' => 'wolseley',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            398 => 
            array (
                'id_manufacturer' => 399,
                'str_description' => 'xingyue',
                'str_description_slug' => 'xingyue',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            399 => 
            array (
                'id_manufacturer' => 400,
                'str_description' => 'xinling',
                'str_description_slug' => 'xinling',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            400 => 
            array (
                'id_manufacturer' => 401,
                'str_description' => 'xinyang',
                'str_description_slug' => 'xinyang',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            401 => 
            array (
                'id_manufacturer' => 402,
                'str_description' => 'yamaha',
                'str_description_slug' => 'yamaha',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            402 => 
            array (
                'id_manufacturer' => 403,
                'str_description' => 'yamazu',
                'str_description_slug' => 'yamazu',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            403 => 
            array (
                'id_manufacturer' => 404,
                'str_description' => 'yes',
                'str_description_slug' => 'yes',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            404 => 
            array (
                'id_manufacturer' => 405,
                'str_description' => 'yide field',
                'str_description_slug' => 'yide-field',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            405 => 
            array (
                'id_manufacturer' => 406,
                'str_description' => 'yongkang mtl',
                'str_description_slug' => 'yongkang-mtl',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            406 => 
            array (
                'id_manufacturer' => 407,
                'str_description' => 'yuantong',
                'str_description_slug' => 'yuantong',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            407 => 
            array (
                'id_manufacturer' => 408,
                'str_description' => 'yugo',
                'str_description_slug' => 'yugo',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            408 => 
            array (
                'id_manufacturer' => 409,
                'str_description' => 'yumei',
                'str_description_slug' => 'yumei',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            409 => 
            array (
                'id_manufacturer' => 410,
                'str_description' => 'zastava',
                'str_description_slug' => 'zastava',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            410 => 
            array (
                'id_manufacturer' => 411,
                'str_description' => 'zest',
                'str_description_slug' => 'zest',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            411 => 
            array (
                'id_manufacturer' => 412,
                'str_description' => 'zhe jiang',
                'str_description_slug' => 'zhe-jiang',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
            412 => 
            array (
                'id_manufacturer' => 413,
                'str_description' => 'zimmer',
                'str_description_slug' => 'zimmer',
                'bol_popular' => 0,
                'id_type' => 1,
                'bol_active' => 1,
            ),
        ));
        
        
    }
}