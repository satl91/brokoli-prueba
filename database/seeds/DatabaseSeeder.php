<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AutomobileDoorsTableSeeder::class);
        $this->call(AutomobileFuelsTableSeeder::class);
        $this->call(AutomobileManufacturersTableSeeder::class);
        $this->call(AutomobileModelsTableSeeder::class);
        $this->call(AutomobileTypesTableSeeder::class);
        $this->call(ModelFuelNumdoorsTableSeeder::class);
        $this->call(ModelRelFuelTableSeeder::class);
    }
}
