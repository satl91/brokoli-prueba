<?php

use Illuminate\Database\Seeder;

class AutomobileFuelsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('automobile_fuels')->delete();
        
        \DB::table('automobile_fuels')->insert(array (
            0 => 
            array (
                'id_fuel' => 1,
                'str_description' => 'Bioethanol',
                'bol_active' => 1,
            ),
            1 => 
            array (
                'id_fuel' => 2,
                'str_description' => 'Diesel',
                'bol_active' => 1,
            ),
            2 => 
            array (
                'id_fuel' => 3,
                'str_description' => 'Gas licuado',
                'bol_active' => 1,
            ),
            3 => 
            array (
                'id_fuel' => 4,
                'str_description' => 'Gasolina',
                'bol_active' => 1,
            ),
            4 => 
            array (
                'id_fuel' => 5,
                'str_description' => 'Hibrido G+E',
                'bol_active' => 1,
            ),
            5 => 
            array (
                'id_fuel' => 6,
                'str_description' => 'Electrico',
                'bol_active' => 1,
            ),
        ));
        
        
    }
}