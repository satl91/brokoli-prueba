<?php

use Illuminate\Database\Seeder;

class ModelRelFuelTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('model_rel_fuel')->delete();
        
        \DB::table('model_rel_fuel')->insert(array (
            0 => 
            array (
                'id_reg' => 1,
                'id_model' => 1,
                'id_fuel' => 4,
            ),
            1 => 
            array (
                'id_reg' => 2,
                'id_model' => 2,
                'id_fuel' => 4,
            ),
            2 => 
            array (
                'id_reg' => 3,
                'id_model' => 3,
                'id_fuel' => 4,
            ),
            3 => 
            array (
                'id_reg' => 4,
                'id_model' => 4,
                'id_fuel' => 4,
            ),
            4 => 
            array (
                'id_reg' => 5,
                'id_model' => 5,
                'id_fuel' => 4,
            ),
            5 => 
            array (
                'id_reg' => 6,
                'id_model' => 6,
                'id_fuel' => 4,
            ),
            6 => 
            array (
                'id_reg' => 7,
                'id_model' => 7,
                'id_fuel' => 4,
            ),
            7 => 
            array (
                'id_reg' => 8,
                'id_model' => 8,
                'id_fuel' => 4,
            ),
            8 => 
            array (
                'id_reg' => 9,
                'id_model' => 9,
                'id_fuel' => 4,
            ),
            9 => 
            array (
                'id_reg' => 10,
                'id_model' => 10,
                'id_fuel' => 4,
            ),
            10 => 
            array (
                'id_reg' => 11,
                'id_model' => 11,
                'id_fuel' => 2,
            ),
            11 => 
            array (
                'id_reg' => 12,
                'id_model' => 12,
                'id_fuel' => 4,
            ),
            12 => 
            array (
                'id_reg' => 13,
                'id_model' => 13,
                'id_fuel' => 4,
            ),
            13 => 
            array (
                'id_reg' => 14,
                'id_model' => 14,
                'id_fuel' => 4,
            ),
            14 => 
            array (
                'id_reg' => 15,
                'id_model' => 15,
                'id_fuel' => 4,
            ),
            15 => 
            array (
                'id_reg' => 16,
                'id_model' => 16,
                'id_fuel' => 4,
            ),
            16 => 
            array (
                'id_reg' => 17,
                'id_model' => 17,
                'id_fuel' => 4,
            ),
            17 => 
            array (
                'id_reg' => 18,
                'id_model' => 18,
                'id_fuel' => 4,
            ),
            18 => 
            array (
                'id_reg' => 19,
                'id_model' => 19,
                'id_fuel' => 4,
            ),
            19 => 
            array (
                'id_reg' => 20,
                'id_model' => 20,
                'id_fuel' => 4,
            ),
            20 => 
            array (
                'id_reg' => 21,
                'id_model' => 21,
                'id_fuel' => 4,
            ),
            21 => 
            array (
                'id_reg' => 22,
                'id_model' => 22,
                'id_fuel' => 2,
            ),
            22 => 
            array (
                'id_reg' => 23,
                'id_model' => 23,
                'id_fuel' => 2,
            ),
            23 => 
            array (
                'id_reg' => 24,
                'id_model' => 24,
                'id_fuel' => 2,
            ),
            24 => 
            array (
                'id_reg' => 25,
                'id_model' => 25,
                'id_fuel' => 2,
            ),
            25 => 
            array (
                'id_reg' => 26,
                'id_model' => 26,
                'id_fuel' => 2,
            ),
            26 => 
            array (
                'id_reg' => 27,
                'id_model' => 27,
                'id_fuel' => 2,
            ),
            27 => 
            array (
                'id_reg' => 28,
                'id_model' => 28,
                'id_fuel' => 2,
            ),
            28 => 
            array (
                'id_reg' => 29,
                'id_model' => 29,
                'id_fuel' => 2,
            ),
            29 => 
            array (
                'id_reg' => 30,
                'id_model' => 30,
                'id_fuel' => 2,
            ),
            30 => 
            array (
                'id_reg' => 31,
                'id_model' => 31,
                'id_fuel' => 2,
            ),
            31 => 
            array (
                'id_reg' => 32,
                'id_model' => 32,
                'id_fuel' => 2,
            ),
            32 => 
            array (
                'id_reg' => 33,
                'id_model' => 33,
                'id_fuel' => 2,
            ),
            33 => 
            array (
                'id_reg' => 34,
                'id_model' => 34,
                'id_fuel' => 2,
            ),
            34 => 
            array (
                'id_reg' => 35,
                'id_model' => 35,
                'id_fuel' => 2,
            ),
            35 => 
            array (
                'id_reg' => 36,
                'id_model' => 36,
                'id_fuel' => 2,
            ),
            36 => 
            array (
                'id_reg' => 37,
                'id_model' => 36,
                'id_fuel' => 6,
            ),
            37 => 
            array (
                'id_reg' => 38,
                'id_model' => 37,
                'id_fuel' => 2,
            ),
            38 => 
            array (
                'id_reg' => 39,
                'id_model' => 38,
                'id_fuel' => 2,
            ),
            39 => 
            array (
                'id_reg' => 40,
                'id_model' => 39,
                'id_fuel' => 2,
            ),
            40 => 
            array (
                'id_reg' => 41,
                'id_model' => 39,
                'id_fuel' => 6,
            ),
            41 => 
            array (
                'id_reg' => 42,
                'id_model' => 40,
                'id_fuel' => 2,
            ),
            42 => 
            array (
                'id_reg' => 43,
                'id_model' => 41,
                'id_fuel' => 2,
            ),
            43 => 
            array (
                'id_reg' => 44,
                'id_model' => 42,
                'id_fuel' => 2,
            ),
            44 => 
            array (
                'id_reg' => 45,
                'id_model' => 43,
                'id_fuel' => 2,
            ),
            45 => 
            array (
                'id_reg' => 46,
                'id_model' => 44,
                'id_fuel' => 2,
            ),
            46 => 
            array (
                'id_reg' => 47,
                'id_model' => 44,
                'id_fuel' => 4,
            ),
            47 => 
            array (
                'id_reg' => 48,
                'id_model' => 45,
                'id_fuel' => 2,
            ),
            48 => 
            array (
                'id_reg' => 49,
                'id_model' => 46,
                'id_fuel' => 4,
            ),
            49 => 
            array (
                'id_reg' => 50,
                'id_model' => 47,
                'id_fuel' => 4,
            ),
            50 => 
            array (
                'id_reg' => 51,
                'id_model' => 48,
                'id_fuel' => 4,
            ),
            51 => 
            array (
                'id_reg' => 52,
                'id_model' => 49,
                'id_fuel' => 4,
            ),
            52 => 
            array (
                'id_reg' => 53,
                'id_model' => 50,
                'id_fuel' => 4,
            ),
            53 => 
            array (
                'id_reg' => 54,
                'id_model' => 51,
                'id_fuel' => 4,
            ),
            54 => 
            array (
                'id_reg' => 55,
                'id_model' => 52,
                'id_fuel' => 4,
            ),
            55 => 
            array (
                'id_reg' => 56,
                'id_model' => 53,
                'id_fuel' => 2,
            ),
            56 => 
            array (
                'id_reg' => 57,
                'id_model' => 53,
                'id_fuel' => 4,
            ),
            57 => 
            array (
                'id_reg' => 58,
                'id_model' => 54,
                'id_fuel' => 2,
            ),
            58 => 
            array (
                'id_reg' => 59,
                'id_model' => 54,
                'id_fuel' => 4,
            ),
            59 => 
            array (
                'id_reg' => 60,
                'id_model' => 55,
                'id_fuel' => 2,
            ),
            60 => 
            array (
                'id_reg' => 61,
                'id_model' => 55,
                'id_fuel' => 4,
            ),
            61 => 
            array (
                'id_reg' => 62,
                'id_model' => 56,
                'id_fuel' => 2,
            ),
            62 => 
            array (
                'id_reg' => 63,
                'id_model' => 56,
                'id_fuel' => 4,
            ),
            63 => 
            array (
                'id_reg' => 64,
                'id_model' => 57,
                'id_fuel' => 4,
            ),
            64 => 
            array (
                'id_reg' => 65,
                'id_model' => 58,
                'id_fuel' => 4,
            ),
            65 => 
            array (
                'id_reg' => 66,
                'id_model' => 59,
                'id_fuel' => 4,
            ),
            66 => 
            array (
                'id_reg' => 67,
                'id_model' => 60,
                'id_fuel' => 4,
            ),
            67 => 
            array (
                'id_reg' => 68,
                'id_model' => 61,
                'id_fuel' => 4,
            ),
            68 => 
            array (
                'id_reg' => 69,
                'id_model' => 62,
                'id_fuel' => 2,
            ),
            69 => 
            array (
                'id_reg' => 70,
                'id_model' => 62,
                'id_fuel' => 4,
            ),
            70 => 
            array (
                'id_reg' => 71,
                'id_model' => 63,
                'id_fuel' => 4,
            ),
            71 => 
            array (
                'id_reg' => 72,
                'id_model' => 64,
                'id_fuel' => 2,
            ),
            72 => 
            array (
                'id_reg' => 73,
                'id_model' => 65,
                'id_fuel' => 2,
            ),
            73 => 
            array (
                'id_reg' => 74,
                'id_model' => 66,
                'id_fuel' => 2,
            ),
            74 => 
            array (
                'id_reg' => 75,
                'id_model' => 66,
                'id_fuel' => 4,
            ),
            75 => 
            array (
                'id_reg' => 76,
                'id_model' => 67,
                'id_fuel' => 4,
            ),
            76 => 
            array (
                'id_reg' => 77,
                'id_model' => 68,
                'id_fuel' => 4,
            ),
            77 => 
            array (
                'id_reg' => 78,
                'id_model' => 69,
                'id_fuel' => 2,
            ),
            78 => 
            array (
                'id_reg' => 79,
                'id_model' => 70,
                'id_fuel' => 2,
            ),
            79 => 
            array (
                'id_reg' => 80,
                'id_model' => 70,
                'id_fuel' => 4,
            ),
            80 => 
            array (
                'id_reg' => 81,
                'id_model' => 71,
                'id_fuel' => 4,
            ),
            81 => 
            array (
                'id_reg' => 82,
                'id_model' => 72,
                'id_fuel' => 4,
            ),
            82 => 
            array (
                'id_reg' => 83,
                'id_model' => 73,
                'id_fuel' => 4,
            ),
            83 => 
            array (
                'id_reg' => 84,
                'id_model' => 74,
                'id_fuel' => 4,
            ),
            84 => 
            array (
                'id_reg' => 85,
                'id_model' => 75,
                'id_fuel' => 4,
            ),
            85 => 
            array (
                'id_reg' => 86,
                'id_model' => 76,
                'id_fuel' => 4,
            ),
            86 => 
            array (
                'id_reg' => 87,
                'id_model' => 77,
                'id_fuel' => 4,
            ),
            87 => 
            array (
                'id_reg' => 88,
                'id_model' => 78,
                'id_fuel' => 4,
            ),
            88 => 
            array (
                'id_reg' => 89,
                'id_model' => 79,
                'id_fuel' => 4,
            ),
            89 => 
            array (
                'id_reg' => 90,
                'id_model' => 80,
                'id_fuel' => 2,
            ),
            90 => 
            array (
                'id_reg' => 91,
                'id_model' => 81,
                'id_fuel' => 4,
            ),
            91 => 
            array (
                'id_reg' => 92,
                'id_model' => 82,
                'id_fuel' => 4,
            ),
            92 => 
            array (
                'id_reg' => 93,
                'id_model' => 83,
                'id_fuel' => 4,
            ),
            93 => 
            array (
                'id_reg' => 94,
                'id_model' => 84,
                'id_fuel' => 4,
            ),
            94 => 
            array (
                'id_reg' => 95,
                'id_model' => 85,
                'id_fuel' => 4,
            ),
            95 => 
            array (
                'id_reg' => 96,
                'id_model' => 86,
                'id_fuel' => 4,
            ),
            96 => 
            array (
                'id_reg' => 97,
                'id_model' => 87,
                'id_fuel' => 4,
            ),
            97 => 
            array (
                'id_reg' => 98,
                'id_model' => 88,
                'id_fuel' => 4,
            ),
            98 => 
            array (
                'id_reg' => 99,
                'id_model' => 89,
                'id_fuel' => 4,
            ),
            99 => 
            array (
                'id_reg' => 100,
                'id_model' => 90,
                'id_fuel' => 4,
            ),
            100 => 
            array (
                'id_reg' => 101,
                'id_model' => 91,
                'id_fuel' => 4,
            ),
            101 => 
            array (
                'id_reg' => 102,
                'id_model' => 92,
                'id_fuel' => 4,
            ),
            102 => 
            array (
                'id_reg' => 103,
                'id_model' => 93,
                'id_fuel' => 4,
            ),
            103 => 
            array (
                'id_reg' => 104,
                'id_model' => 94,
                'id_fuel' => 4,
            ),
            104 => 
            array (
                'id_reg' => 105,
                'id_model' => 95,
                'id_fuel' => 4,
            ),
            105 => 
            array (
                'id_reg' => 106,
                'id_model' => 96,
                'id_fuel' => 4,
            ),
            106 => 
            array (
                'id_reg' => 107,
                'id_model' => 97,
                'id_fuel' => 2,
            ),
            107 => 
            array (
                'id_reg' => 108,
                'id_model' => 98,
                'id_fuel' => 4,
            ),
            108 => 
            array (
                'id_reg' => 109,
                'id_model' => 99,
                'id_fuel' => 2,
            ),
            109 => 
            array (
                'id_reg' => 110,
                'id_model' => 100,
                'id_fuel' => 2,
            ),
            110 => 
            array (
                'id_reg' => 111,
                'id_model' => 101,
                'id_fuel' => 2,
            ),
            111 => 
            array (
                'id_reg' => 112,
                'id_model' => 102,
                'id_fuel' => 4,
            ),
            112 => 
            array (
                'id_reg' => 113,
                'id_model' => 103,
                'id_fuel' => 2,
            ),
            113 => 
            array (
                'id_reg' => 114,
                'id_model' => 103,
                'id_fuel' => 4,
            ),
            114 => 
            array (
                'id_reg' => 115,
                'id_model' => 104,
                'id_fuel' => 4,
            ),
            115 => 
            array (
                'id_reg' => 116,
                'id_model' => 105,
                'id_fuel' => 2,
            ),
            116 => 
            array (
                'id_reg' => 117,
                'id_model' => 106,
                'id_fuel' => 2,
            ),
            117 => 
            array (
                'id_reg' => 118,
                'id_model' => 106,
                'id_fuel' => 4,
            ),
            118 => 
            array (
                'id_reg' => 119,
                'id_model' => 107,
                'id_fuel' => 2,
            ),
            119 => 
            array (
                'id_reg' => 120,
                'id_model' => 107,
                'id_fuel' => 4,
            ),
            120 => 
            array (
                'id_reg' => 121,
                'id_model' => 108,
                'id_fuel' => 2,
            ),
            121 => 
            array (
                'id_reg' => 122,
                'id_model' => 109,
                'id_fuel' => 4,
            ),
            122 => 
            array (
                'id_reg' => 123,
                'id_model' => 110,
                'id_fuel' => 4,
            ),
            123 => 
            array (
                'id_reg' => 124,
                'id_model' => 111,
                'id_fuel' => 4,
            ),
            124 => 
            array (
                'id_reg' => 125,
                'id_model' => 112,
                'id_fuel' => 4,
            ),
            125 => 
            array (
                'id_reg' => 126,
                'id_model' => 113,
                'id_fuel' => 4,
            ),
            126 => 
            array (
                'id_reg' => 127,
                'id_model' => 114,
                'id_fuel' => 4,
            ),
            127 => 
            array (
                'id_reg' => 128,
                'id_model' => 115,
                'id_fuel' => 4,
            ),
            128 => 
            array (
                'id_reg' => 129,
                'id_model' => 116,
                'id_fuel' => 4,
            ),
            129 => 
            array (
                'id_reg' => 130,
                'id_model' => 117,
                'id_fuel' => 4,
            ),
            130 => 
            array (
                'id_reg' => 131,
                'id_model' => 118,
                'id_fuel' => 4,
            ),
            131 => 
            array (
                'id_reg' => 132,
                'id_model' => 119,
                'id_fuel' => 4,
            ),
            132 => 
            array (
                'id_reg' => 133,
                'id_model' => 120,
                'id_fuel' => 4,
            ),
            133 => 
            array (
                'id_reg' => 134,
                'id_model' => 121,
                'id_fuel' => 4,
            ),
            134 => 
            array (
                'id_reg' => 135,
                'id_model' => 122,
                'id_fuel' => 4,
            ),
            135 => 
            array (
                'id_reg' => 136,
                'id_model' => 123,
                'id_fuel' => 4,
            ),
            136 => 
            array (
                'id_reg' => 137,
                'id_model' => 124,
                'id_fuel' => 4,
            ),
            137 => 
            array (
                'id_reg' => 138,
                'id_model' => 125,
                'id_fuel' => 4,
            ),
            138 => 
            array (
                'id_reg' => 139,
                'id_model' => 126,
                'id_fuel' => 4,
            ),
            139 => 
            array (
                'id_reg' => 140,
                'id_model' => 127,
                'id_fuel' => 4,
            ),
            140 => 
            array (
                'id_reg' => 141,
                'id_model' => 128,
                'id_fuel' => 4,
            ),
            141 => 
            array (
                'id_reg' => 142,
                'id_model' => 129,
                'id_fuel' => 4,
            ),
            142 => 
            array (
                'id_reg' => 143,
                'id_model' => 130,
                'id_fuel' => 4,
            ),
            143 => 
            array (
                'id_reg' => 144,
                'id_model' => 131,
                'id_fuel' => 4,
            ),
            144 => 
            array (
                'id_reg' => 145,
                'id_model' => 132,
                'id_fuel' => 4,
            ),
            145 => 
            array (
                'id_reg' => 146,
                'id_model' => 133,
                'id_fuel' => 2,
            ),
            146 => 
            array (
                'id_reg' => 147,
                'id_model' => 133,
                'id_fuel' => 4,
            ),
            147 => 
            array (
                'id_reg' => 148,
                'id_model' => 134,
                'id_fuel' => 2,
            ),
            148 => 
            array (
                'id_reg' => 149,
                'id_model' => 135,
                'id_fuel' => 4,
            ),
            149 => 
            array (
                'id_reg' => 150,
                'id_model' => 136,
                'id_fuel' => 2,
            ),
            150 => 
            array (
                'id_reg' => 151,
                'id_model' => 136,
                'id_fuel' => 4,
            ),
            151 => 
            array (
                'id_reg' => 152,
                'id_model' => 137,
                'id_fuel' => 2,
            ),
            152 => 
            array (
                'id_reg' => 153,
                'id_model' => 137,
                'id_fuel' => 4,
            ),
            153 => 
            array (
                'id_reg' => 154,
                'id_model' => 138,
                'id_fuel' => 2,
            ),
            154 => 
            array (
                'id_reg' => 155,
                'id_model' => 138,
                'id_fuel' => 4,
            ),
            155 => 
            array (
                'id_reg' => 156,
                'id_model' => 139,
                'id_fuel' => 2,
            ),
            156 => 
            array (
                'id_reg' => 157,
                'id_model' => 139,
                'id_fuel' => 4,
            ),
            157 => 
            array (
                'id_reg' => 158,
                'id_model' => 140,
                'id_fuel' => 2,
            ),
            158 => 
            array (
                'id_reg' => 159,
                'id_model' => 140,
                'id_fuel' => 4,
            ),
            159 => 
            array (
                'id_reg' => 160,
                'id_model' => 141,
                'id_fuel' => 2,
            ),
            160 => 
            array (
                'id_reg' => 161,
                'id_model' => 141,
                'id_fuel' => 4,
            ),
            161 => 
            array (
                'id_reg' => 162,
                'id_model' => 142,
                'id_fuel' => 2,
            ),
            162 => 
            array (
                'id_reg' => 163,
                'id_model' => 142,
                'id_fuel' => 4,
            ),
            163 => 
            array (
                'id_reg' => 164,
                'id_model' => 143,
                'id_fuel' => 2,
            ),
            164 => 
            array (
                'id_reg' => 165,
                'id_model' => 143,
                'id_fuel' => 4,
            ),
            165 => 
            array (
                'id_reg' => 166,
                'id_model' => 143,
                'id_fuel' => 5,
            ),
            166 => 
            array (
                'id_reg' => 167,
                'id_model' => 144,
                'id_fuel' => 2,
            ),
            167 => 
            array (
                'id_reg' => 168,
                'id_model' => 144,
                'id_fuel' => 4,
            ),
            168 => 
            array (
                'id_reg' => 169,
                'id_model' => 145,
                'id_fuel' => 4,
            ),
            169 => 
            array (
                'id_reg' => 170,
                'id_model' => 146,
                'id_fuel' => 2,
            ),
            170 => 
            array (
                'id_reg' => 171,
                'id_model' => 146,
                'id_fuel' => 4,
            ),
            171 => 
            array (
                'id_reg' => 172,
                'id_model' => 147,
                'id_fuel' => 2,
            ),
            172 => 
            array (
                'id_reg' => 173,
                'id_model' => 147,
                'id_fuel' => 4,
            ),
            173 => 
            array (
                'id_reg' => 174,
                'id_model' => 148,
                'id_fuel' => 2,
            ),
            174 => 
            array (
                'id_reg' => 175,
                'id_model' => 148,
                'id_fuel' => 4,
            ),
            175 => 
            array (
                'id_reg' => 176,
                'id_model' => 149,
                'id_fuel' => 4,
            ),
            176 => 
            array (
                'id_reg' => 177,
                'id_model' => 150,
                'id_fuel' => 4,
            ),
            177 => 
            array (
                'id_reg' => 178,
                'id_model' => 151,
                'id_fuel' => 4,
            ),
            178 => 
            array (
                'id_reg' => 179,
                'id_model' => 152,
                'id_fuel' => 4,
            ),
            179 => 
            array (
                'id_reg' => 180,
                'id_model' => 153,
                'id_fuel' => 4,
            ),
            180 => 
            array (
                'id_reg' => 181,
                'id_model' => 154,
                'id_fuel' => 4,
            ),
            181 => 
            array (
                'id_reg' => 182,
                'id_model' => 155,
                'id_fuel' => 4,
            ),
            182 => 
            array (
                'id_reg' => 183,
                'id_model' => 156,
                'id_fuel' => 4,
            ),
            183 => 
            array (
                'id_reg' => 184,
                'id_model' => 157,
                'id_fuel' => 4,
            ),
            184 => 
            array (
                'id_reg' => 185,
                'id_model' => 158,
                'id_fuel' => 4,
            ),
            185 => 
            array (
                'id_reg' => 186,
                'id_model' => 159,
                'id_fuel' => 4,
            ),
            186 => 
            array (
                'id_reg' => 187,
                'id_model' => 160,
                'id_fuel' => 4,
            ),
            187 => 
            array (
                'id_reg' => 188,
                'id_model' => 161,
                'id_fuel' => 4,
            ),
            188 => 
            array (
                'id_reg' => 189,
                'id_model' => 162,
                'id_fuel' => 4,
            ),
            189 => 
            array (
                'id_reg' => 190,
                'id_model' => 163,
                'id_fuel' => 4,
            ),
            190 => 
            array (
                'id_reg' => 191,
                'id_model' => 164,
                'id_fuel' => 4,
            ),
            191 => 
            array (
                'id_reg' => 192,
                'id_model' => 165,
                'id_fuel' => 4,
            ),
            192 => 
            array (
                'id_reg' => 193,
                'id_model' => 166,
                'id_fuel' => 4,
            ),
            193 => 
            array (
                'id_reg' => 194,
                'id_model' => 167,
                'id_fuel' => 4,
            ),
            194 => 
            array (
                'id_reg' => 195,
                'id_model' => 168,
                'id_fuel' => 4,
            ),
            195 => 
            array (
                'id_reg' => 196,
                'id_model' => 169,
                'id_fuel' => 2,
            ),
            196 => 
            array (
                'id_reg' => 197,
                'id_model' => 169,
                'id_fuel' => 4,
            ),
            197 => 
            array (
                'id_reg' => 198,
                'id_model' => 170,
                'id_fuel' => 2,
            ),
            198 => 
            array (
                'id_reg' => 199,
                'id_model' => 171,
                'id_fuel' => 4,
            ),
            199 => 
            array (
                'id_reg' => 200,
                'id_model' => 172,
                'id_fuel' => 4,
            ),
            200 => 
            array (
                'id_reg' => 201,
                'id_model' => 173,
                'id_fuel' => 4,
            ),
            201 => 
            array (
                'id_reg' => 202,
                'id_model' => 174,
                'id_fuel' => 2,
            ),
            202 => 
            array (
                'id_reg' => 203,
                'id_model' => 174,
                'id_fuel' => 4,
            ),
            203 => 
            array (
                'id_reg' => 204,
                'id_model' => 175,
                'id_fuel' => 4,
            ),
            204 => 
            array (
                'id_reg' => 205,
                'id_model' => 176,
                'id_fuel' => 4,
            ),
            205 => 
            array (
                'id_reg' => 206,
                'id_model' => 177,
                'id_fuel' => 4,
            ),
            206 => 
            array (
                'id_reg' => 207,
                'id_model' => 178,
                'id_fuel' => 4,
            ),
            207 => 
            array (
                'id_reg' => 208,
                'id_model' => 179,
                'id_fuel' => 4,
            ),
            208 => 
            array (
                'id_reg' => 209,
                'id_model' => 180,
                'id_fuel' => 4,
            ),
            209 => 
            array (
                'id_reg' => 210,
                'id_model' => 181,
                'id_fuel' => 4,
            ),
            210 => 
            array (
                'id_reg' => 211,
                'id_model' => 182,
                'id_fuel' => 4,
            ),
            211 => 
            array (
                'id_reg' => 212,
                'id_model' => 183,
                'id_fuel' => 4,
            ),
            212 => 
            array (
                'id_reg' => 213,
                'id_model' => 184,
                'id_fuel' => 4,
            ),
            213 => 
            array (
                'id_reg' => 214,
                'id_model' => 185,
                'id_fuel' => 4,
            ),
            214 => 
            array (
                'id_reg' => 215,
                'id_model' => 186,
                'id_fuel' => 4,
            ),
            215 => 
            array (
                'id_reg' => 216,
                'id_model' => 187,
                'id_fuel' => 4,
            ),
            216 => 
            array (
                'id_reg' => 217,
                'id_model' => 188,
                'id_fuel' => 4,
            ),
            217 => 
            array (
                'id_reg' => 218,
                'id_model' => 189,
                'id_fuel' => 4,
            ),
            218 => 
            array (
                'id_reg' => 219,
                'id_model' => 190,
                'id_fuel' => 4,
            ),
            219 => 
            array (
                'id_reg' => 220,
                'id_model' => 191,
                'id_fuel' => 2,
            ),
            220 => 
            array (
                'id_reg' => 221,
                'id_model' => 192,
                'id_fuel' => 2,
            ),
            221 => 
            array (
                'id_reg' => 222,
                'id_model' => 193,
                'id_fuel' => 2,
            ),
            222 => 
            array (
                'id_reg' => 223,
                'id_model' => 194,
                'id_fuel' => 4,
            ),
            223 => 
            array (
                'id_reg' => 224,
                'id_model' => 195,
                'id_fuel' => 4,
            ),
            224 => 
            array (
                'id_reg' => 225,
                'id_model' => 196,
                'id_fuel' => 4,
            ),
            225 => 
            array (
                'id_reg' => 226,
                'id_model' => 197,
                'id_fuel' => 4,
            ),
            226 => 
            array (
                'id_reg' => 227,
                'id_model' => 198,
                'id_fuel' => 2,
            ),
            227 => 
            array (
                'id_reg' => 228,
                'id_model' => 199,
                'id_fuel' => 4,
            ),
            228 => 
            array (
                'id_reg' => 229,
                'id_model' => 200,
                'id_fuel' => 2,
            ),
            229 => 
            array (
                'id_reg' => 230,
                'id_model' => 201,
                'id_fuel' => 4,
            ),
            230 => 
            array (
                'id_reg' => 231,
                'id_model' => 202,
                'id_fuel' => 4,
            ),
            231 => 
            array (
                'id_reg' => 232,
                'id_model' => 203,
                'id_fuel' => 4,
            ),
            232 => 
            array (
                'id_reg' => 233,
                'id_model' => 204,
                'id_fuel' => 4,
            ),
            233 => 
            array (
                'id_reg' => 234,
                'id_model' => 205,
                'id_fuel' => 4,
            ),
            234 => 
            array (
                'id_reg' => 235,
                'id_model' => 206,
                'id_fuel' => 4,
            ),
            235 => 
            array (
                'id_reg' => 236,
                'id_model' => 207,
                'id_fuel' => 4,
            ),
            236 => 
            array (
                'id_reg' => 237,
                'id_model' => 208,
                'id_fuel' => 4,
            ),
            237 => 
            array (
                'id_reg' => 238,
                'id_model' => 209,
                'id_fuel' => 4,
            ),
            238 => 
            array (
                'id_reg' => 239,
                'id_model' => 210,
                'id_fuel' => 4,
            ),
            239 => 
            array (
                'id_reg' => 240,
                'id_model' => 211,
                'id_fuel' => 2,
            ),
            240 => 
            array (
                'id_reg' => 241,
                'id_model' => 212,
                'id_fuel' => 2,
            ),
            241 => 
            array (
                'id_reg' => 242,
                'id_model' => 213,
                'id_fuel' => 2,
            ),
            242 => 
            array (
                'id_reg' => 243,
                'id_model' => 214,
                'id_fuel' => 2,
            ),
            243 => 
            array (
                'id_reg' => 244,
                'id_model' => 215,
                'id_fuel' => 2,
            ),
            244 => 
            array (
                'id_reg' => 245,
                'id_model' => 216,
                'id_fuel' => 2,
            ),
            245 => 
            array (
                'id_reg' => 246,
                'id_model' => 217,
                'id_fuel' => 2,
            ),
            246 => 
            array (
                'id_reg' => 247,
                'id_model' => 218,
                'id_fuel' => 2,
            ),
            247 => 
            array (
                'id_reg' => 248,
                'id_model' => 219,
                'id_fuel' => 2,
            ),
            248 => 
            array (
                'id_reg' => 249,
                'id_model' => 220,
                'id_fuel' => 2,
            ),
            249 => 
            array (
                'id_reg' => 250,
                'id_model' => 221,
                'id_fuel' => 2,
            ),
            250 => 
            array (
                'id_reg' => 251,
                'id_model' => 222,
                'id_fuel' => 2,
            ),
            251 => 
            array (
                'id_reg' => 252,
                'id_model' => 223,
                'id_fuel' => 2,
            ),
            252 => 
            array (
                'id_reg' => 253,
                'id_model' => 224,
                'id_fuel' => 2,
            ),
            253 => 
            array (
                'id_reg' => 254,
                'id_model' => 225,
                'id_fuel' => 2,
            ),
            254 => 
            array (
                'id_reg' => 255,
                'id_model' => 226,
                'id_fuel' => 2,
            ),
            255 => 
            array (
                'id_reg' => 256,
                'id_model' => 227,
                'id_fuel' => 2,
            ),
            256 => 
            array (
                'id_reg' => 257,
                'id_model' => 228,
                'id_fuel' => 2,
            ),
            257 => 
            array (
                'id_reg' => 258,
                'id_model' => 229,
                'id_fuel' => 2,
            ),
            258 => 
            array (
                'id_reg' => 259,
                'id_model' => 230,
                'id_fuel' => 2,
            ),
            259 => 
            array (
                'id_reg' => 260,
                'id_model' => 231,
                'id_fuel' => 2,
            ),
            260 => 
            array (
                'id_reg' => 261,
                'id_model' => 232,
                'id_fuel' => 2,
            ),
            261 => 
            array (
                'id_reg' => 262,
                'id_model' => 233,
                'id_fuel' => 2,
            ),
            262 => 
            array (
                'id_reg' => 263,
                'id_model' => 234,
                'id_fuel' => 2,
            ),
            263 => 
            array (
                'id_reg' => 264,
                'id_model' => 235,
                'id_fuel' => 2,
            ),
            264 => 
            array (
                'id_reg' => 265,
                'id_model' => 236,
                'id_fuel' => 2,
            ),
            265 => 
            array (
                'id_reg' => 266,
                'id_model' => 237,
                'id_fuel' => 2,
            ),
            266 => 
            array (
                'id_reg' => 267,
                'id_model' => 238,
                'id_fuel' => 2,
            ),
            267 => 
            array (
                'id_reg' => 268,
                'id_model' => 239,
                'id_fuel' => 2,
            ),
            268 => 
            array (
                'id_reg' => 269,
                'id_model' => 240,
                'id_fuel' => 2,
            ),
            269 => 
            array (
                'id_reg' => 270,
                'id_model' => 241,
                'id_fuel' => 2,
            ),
            270 => 
            array (
                'id_reg' => 271,
                'id_model' => 242,
                'id_fuel' => 4,
            ),
            271 => 
            array (
                'id_reg' => 272,
                'id_model' => 243,
                'id_fuel' => 4,
            ),
            272 => 
            array (
                'id_reg' => 273,
                'id_model' => 244,
                'id_fuel' => 4,
            ),
            273 => 
            array (
                'id_reg' => 274,
                'id_model' => 245,
                'id_fuel' => 4,
            ),
            274 => 
            array (
                'id_reg' => 275,
                'id_model' => 246,
                'id_fuel' => 4,
            ),
            275 => 
            array (
                'id_reg' => 276,
                'id_model' => 247,
                'id_fuel' => 4,
            ),
            276 => 
            array (
                'id_reg' => 277,
                'id_model' => 248,
                'id_fuel' => 4,
            ),
            277 => 
            array (
                'id_reg' => 278,
                'id_model' => 249,
                'id_fuel' => 4,
            ),
            278 => 
            array (
                'id_reg' => 279,
                'id_model' => 250,
                'id_fuel' => 4,
            ),
            279 => 
            array (
                'id_reg' => 280,
                'id_model' => 251,
                'id_fuel' => 6,
            ),
            280 => 
            array (
                'id_reg' => 281,
                'id_model' => 252,
                'id_fuel' => 2,
            ),
            281 => 
            array (
                'id_reg' => 282,
                'id_model' => 252,
                'id_fuel' => 4,
            ),
            282 => 
            array (
                'id_reg' => 283,
                'id_model' => 253,
                'id_fuel' => 2,
            ),
            283 => 
            array (
                'id_reg' => 284,
                'id_model' => 253,
                'id_fuel' => 4,
            ),
            284 => 
            array (
                'id_reg' => 285,
                'id_model' => 254,
                'id_fuel' => 2,
            ),
            285 => 
            array (
                'id_reg' => 286,
                'id_model' => 255,
                'id_fuel' => 4,
            ),
            286 => 
            array (
                'id_reg' => 287,
                'id_model' => 256,
                'id_fuel' => 4,
            ),
            287 => 
            array (
                'id_reg' => 288,
                'id_model' => 257,
                'id_fuel' => 6,
            ),
            288 => 
            array (
                'id_reg' => 289,
                'id_model' => 258,
                'id_fuel' => 2,
            ),
            289 => 
            array (
                'id_reg' => 290,
                'id_model' => 259,
                'id_fuel' => 2,
            ),
            290 => 
            array (
                'id_reg' => 291,
                'id_model' => 260,
                'id_fuel' => 2,
            ),
            291 => 
            array (
                'id_reg' => 292,
                'id_model' => 260,
                'id_fuel' => 4,
            ),
            292 => 
            array (
                'id_reg' => 293,
                'id_model' => 261,
                'id_fuel' => 6,
            ),
            293 => 
            array (
                'id_reg' => 294,
                'id_model' => 262,
                'id_fuel' => 2,
            ),
            294 => 
            array (
                'id_reg' => 295,
                'id_model' => 263,
                'id_fuel' => 2,
            ),
            295 => 
            array (
                'id_reg' => 296,
                'id_model' => 264,
                'id_fuel' => 2,
            ),
            296 => 
            array (
                'id_reg' => 297,
                'id_model' => 265,
                'id_fuel' => 2,
            ),
            297 => 
            array (
                'id_reg' => 298,
                'id_model' => 266,
                'id_fuel' => 2,
            ),
            298 => 
            array (
                'id_reg' => 299,
                'id_model' => 267,
                'id_fuel' => 6,
            ),
            299 => 
            array (
                'id_reg' => 300,
                'id_model' => 270,
                'id_fuel' => 4,
            ),
            300 => 
            array (
                'id_reg' => 301,
                'id_model' => 271,
                'id_fuel' => 4,
            ),
            301 => 
            array (
                'id_reg' => 302,
                'id_model' => 272,
                'id_fuel' => 4,
            ),
            302 => 
            array (
                'id_reg' => 303,
                'id_model' => 273,
                'id_fuel' => 4,
            ),
            303 => 
            array (
                'id_reg' => 304,
                'id_model' => 274,
                'id_fuel' => 1,
            ),
            304 => 
            array (
                'id_reg' => 305,
                'id_model' => 274,
                'id_fuel' => 4,
            ),
            305 => 
            array (
                'id_reg' => 306,
                'id_model' => 275,
                'id_fuel' => 4,
            ),
            306 => 
            array (
                'id_reg' => 307,
                'id_model' => 276,
                'id_fuel' => 4,
            ),
            307 => 
            array (
                'id_reg' => 308,
                'id_model' => 277,
                'id_fuel' => 4,
            ),
            308 => 
            array (
                'id_reg' => 309,
                'id_model' => 278,
                'id_fuel' => 4,
            ),
            309 => 
            array (
                'id_reg' => 310,
                'id_model' => 279,
                'id_fuel' => 4,
            ),
            310 => 
            array (
                'id_reg' => 311,
                'id_model' => 280,
                'id_fuel' => 4,
            ),
            311 => 
            array (
                'id_reg' => 312,
                'id_model' => 281,
                'id_fuel' => 4,
            ),
            312 => 
            array (
                'id_reg' => 313,
                'id_model' => 282,
                'id_fuel' => 4,
            ),
            313 => 
            array (
                'id_reg' => 314,
                'id_model' => 283,
                'id_fuel' => 4,
            ),
            314 => 
            array (
                'id_reg' => 315,
                'id_model' => 284,
                'id_fuel' => 4,
            ),
            315 => 
            array (
                'id_reg' => 316,
                'id_model' => 285,
                'id_fuel' => 4,
            ),
            316 => 
            array (
                'id_reg' => 317,
                'id_model' => 286,
                'id_fuel' => 4,
            ),
            317 => 
            array (
                'id_reg' => 318,
                'id_model' => 287,
                'id_fuel' => 4,
            ),
            318 => 
            array (
                'id_reg' => 319,
                'id_model' => 288,
                'id_fuel' => 4,
            ),
            319 => 
            array (
                'id_reg' => 320,
                'id_model' => 289,
                'id_fuel' => 2,
            ),
            320 => 
            array (
                'id_reg' => 321,
                'id_model' => 289,
                'id_fuel' => 4,
            ),
            321 => 
            array (
                'id_reg' => 322,
                'id_model' => 290,
                'id_fuel' => 4,
            ),
            322 => 
            array (
                'id_reg' => 323,
                'id_model' => 292,
                'id_fuel' => 4,
            ),
            323 => 
            array (
                'id_reg' => 324,
                'id_model' => 293,
                'id_fuel' => 4,
            ),
            324 => 
            array (
                'id_reg' => 325,
                'id_model' => 294,
                'id_fuel' => 4,
            ),
            325 => 
            array (
                'id_reg' => 326,
                'id_model' => 295,
                'id_fuel' => 4,
            ),
            326 => 
            array (
                'id_reg' => 327,
                'id_model' => 296,
                'id_fuel' => 4,
            ),
            327 => 
            array (
                'id_reg' => 328,
                'id_model' => 297,
                'id_fuel' => 6,
            ),
            328 => 
            array (
                'id_reg' => 329,
                'id_model' => 298,
                'id_fuel' => 4,
            ),
            329 => 
            array (
                'id_reg' => 330,
                'id_model' => 299,
                'id_fuel' => 4,
            ),
            330 => 
            array (
                'id_reg' => 331,
                'id_model' => 300,
                'id_fuel' => 4,
            ),
            331 => 
            array (
                'id_reg' => 332,
                'id_model' => 301,
                'id_fuel' => 4,
            ),
            332 => 
            array (
                'id_reg' => 333,
                'id_model' => 302,
                'id_fuel' => 4,
            ),
            333 => 
            array (
                'id_reg' => 334,
                'id_model' => 303,
                'id_fuel' => 4,
            ),
            334 => 
            array (
                'id_reg' => 335,
                'id_model' => 304,
                'id_fuel' => 4,
            ),
            335 => 
            array (
                'id_reg' => 336,
                'id_model' => 305,
                'id_fuel' => 4,
            ),
            336 => 
            array (
                'id_reg' => 337,
                'id_model' => 306,
                'id_fuel' => 4,
            ),
            337 => 
            array (
                'id_reg' => 338,
                'id_model' => 307,
                'id_fuel' => 4,
            ),
            338 => 
            array (
                'id_reg' => 339,
                'id_model' => 308,
                'id_fuel' => 4,
            ),
            339 => 
            array (
                'id_reg' => 340,
                'id_model' => 309,
                'id_fuel' => 4,
            ),
            340 => 
            array (
                'id_reg' => 341,
                'id_model' => 310,
                'id_fuel' => 4,
            ),
            341 => 
            array (
                'id_reg' => 342,
                'id_model' => 311,
                'id_fuel' => 4,
            ),
            342 => 
            array (
                'id_reg' => 343,
                'id_model' => 312,
                'id_fuel' => 4,
            ),
            343 => 
            array (
                'id_reg' => 344,
                'id_model' => 313,
                'id_fuel' => 4,
            ),
            344 => 
            array (
                'id_reg' => 345,
                'id_model' => 314,
                'id_fuel' => 6,
            ),
            345 => 
            array (
                'id_reg' => 346,
                'id_model' => 315,
                'id_fuel' => 5,
            ),
            346 => 
            array (
                'id_reg' => 347,
                'id_model' => 316,
                'id_fuel' => 4,
            ),
            347 => 
            array (
                'id_reg' => 348,
                'id_model' => 317,
                'id_fuel' => 4,
            ),
            348 => 
            array (
                'id_reg' => 349,
                'id_model' => 318,
                'id_fuel' => 2,
            ),
            349 => 
            array (
                'id_reg' => 350,
                'id_model' => 318,
                'id_fuel' => 4,
            ),
            350 => 
            array (
                'id_reg' => 351,
                'id_model' => 318,
                'id_fuel' => 5,
            ),
            351 => 
            array (
                'id_reg' => 352,
                'id_model' => 319,
                'id_fuel' => 2,
            ),
            352 => 
            array (
                'id_reg' => 353,
                'id_model' => 319,
                'id_fuel' => 4,
            ),
            353 => 
            array (
                'id_reg' => 354,
                'id_model' => 320,
                'id_fuel' => 4,
            ),
            354 => 
            array (
                'id_reg' => 355,
                'id_model' => 321,
                'id_fuel' => 2,
            ),
            355 => 
            array (
                'id_reg' => 356,
                'id_model' => 321,
                'id_fuel' => 4,
            ),
            356 => 
            array (
                'id_reg' => 357,
                'id_model' => 322,
                'id_fuel' => 2,
            ),
            357 => 
            array (
                'id_reg' => 358,
                'id_model' => 322,
                'id_fuel' => 4,
            ),
            358 => 
            array (
                'id_reg' => 359,
                'id_model' => 322,
                'id_fuel' => 5,
            ),
            359 => 
            array (
                'id_reg' => 360,
                'id_model' => 323,
                'id_fuel' => 4,
            ),
            360 => 
            array (
                'id_reg' => 361,
                'id_model' => 324,
                'id_fuel' => 4,
            ),
            361 => 
            array (
                'id_reg' => 362,
                'id_model' => 325,
                'id_fuel' => 6,
            ),
            362 => 
            array (
                'id_reg' => 363,
                'id_model' => 326,
                'id_fuel' => 4,
            ),
            363 => 
            array (
                'id_reg' => 364,
                'id_model' => 327,
                'id_fuel' => 2,
            ),
            364 => 
            array (
                'id_reg' => 365,
                'id_model' => 328,
                'id_fuel' => 2,
            ),
            365 => 
            array (
                'id_reg' => 366,
                'id_model' => 329,
                'id_fuel' => 4,
            ),
            366 => 
            array (
                'id_reg' => 367,
                'id_model' => 330,
                'id_fuel' => 4,
            ),
            367 => 
            array (
                'id_reg' => 368,
                'id_model' => 331,
                'id_fuel' => 4,
            ),
            368 => 
            array (
                'id_reg' => 369,
                'id_model' => 332,
                'id_fuel' => 4,
            ),
            369 => 
            array (
                'id_reg' => 370,
                'id_model' => 333,
                'id_fuel' => 4,
            ),
            370 => 
            array (
                'id_reg' => 371,
                'id_model' => 334,
                'id_fuel' => 4,
            ),
            371 => 
            array (
                'id_reg' => 372,
                'id_model' => 335,
                'id_fuel' => 4,
            ),
            372 => 
            array (
                'id_reg' => 373,
                'id_model' => 336,
                'id_fuel' => 4,
            ),
            373 => 
            array (
                'id_reg' => 374,
                'id_model' => 337,
                'id_fuel' => 4,
            ),
            374 => 
            array (
                'id_reg' => 375,
                'id_model' => 338,
                'id_fuel' => 4,
            ),
            375 => 
            array (
                'id_reg' => 376,
                'id_model' => 339,
                'id_fuel' => 4,
            ),
            376 => 
            array (
                'id_reg' => 377,
                'id_model' => 340,
                'id_fuel' => 4,
            ),
            377 => 
            array (
                'id_reg' => 378,
                'id_model' => 341,
                'id_fuel' => 4,
            ),
            378 => 
            array (
                'id_reg' => 379,
                'id_model' => 342,
                'id_fuel' => 4,
            ),
            379 => 
            array (
                'id_reg' => 380,
                'id_model' => 343,
                'id_fuel' => 4,
            ),
            380 => 
            array (
                'id_reg' => 381,
                'id_model' => 344,
                'id_fuel' => 4,
            ),
            381 => 
            array (
                'id_reg' => 382,
                'id_model' => 345,
                'id_fuel' => 4,
            ),
            382 => 
            array (
                'id_reg' => 383,
                'id_model' => 346,
                'id_fuel' => 4,
            ),
            383 => 
            array (
                'id_reg' => 384,
                'id_model' => 347,
                'id_fuel' => 4,
            ),
            384 => 
            array (
                'id_reg' => 385,
                'id_model' => 348,
                'id_fuel' => 4,
            ),
            385 => 
            array (
                'id_reg' => 386,
                'id_model' => 349,
                'id_fuel' => 4,
            ),
            386 => 
            array (
                'id_reg' => 387,
                'id_model' => 350,
                'id_fuel' => 4,
            ),
            387 => 
            array (
                'id_reg' => 388,
                'id_model' => 351,
                'id_fuel' => 4,
            ),
            388 => 
            array (
                'id_reg' => 389,
                'id_model' => 352,
                'id_fuel' => 4,
            ),
            389 => 
            array (
                'id_reg' => 390,
                'id_model' => 353,
                'id_fuel' => 4,
            ),
            390 => 
            array (
                'id_reg' => 391,
                'id_model' => 354,
                'id_fuel' => 4,
            ),
            391 => 
            array (
                'id_reg' => 392,
                'id_model' => 355,
                'id_fuel' => 4,
            ),
            392 => 
            array (
                'id_reg' => 393,
                'id_model' => 356,
                'id_fuel' => 4,
            ),
            393 => 
            array (
                'id_reg' => 394,
                'id_model' => 357,
                'id_fuel' => 4,
            ),
            394 => 
            array (
                'id_reg' => 395,
                'id_model' => 358,
                'id_fuel' => 4,
            ),
            395 => 
            array (
                'id_reg' => 396,
                'id_model' => 359,
                'id_fuel' => 4,
            ),
            396 => 
            array (
                'id_reg' => 397,
                'id_model' => 360,
                'id_fuel' => 4,
            ),
            397 => 
            array (
                'id_reg' => 398,
                'id_model' => 361,
                'id_fuel' => 4,
            ),
            398 => 
            array (
                'id_reg' => 399,
                'id_model' => 362,
                'id_fuel' => 4,
            ),
            399 => 
            array (
                'id_reg' => 400,
                'id_model' => 363,
                'id_fuel' => 4,
            ),
            400 => 
            array (
                'id_reg' => 401,
                'id_model' => 364,
                'id_fuel' => 4,
            ),
            401 => 
            array (
                'id_reg' => 402,
                'id_model' => 365,
                'id_fuel' => 4,
            ),
            402 => 
            array (
                'id_reg' => 403,
                'id_model' => 366,
                'id_fuel' => 4,
            ),
            403 => 
            array (
                'id_reg' => 404,
                'id_model' => 367,
                'id_fuel' => 4,
            ),
            404 => 
            array (
                'id_reg' => 405,
                'id_model' => 368,
                'id_fuel' => 4,
            ),
            405 => 
            array (
                'id_reg' => 406,
                'id_model' => 369,
                'id_fuel' => 2,
            ),
            406 => 
            array (
                'id_reg' => 407,
                'id_model' => 370,
                'id_fuel' => 4,
            ),
            407 => 
            array (
                'id_reg' => 408,
                'id_model' => 371,
                'id_fuel' => 6,
            ),
            408 => 
            array (
                'id_reg' => 409,
                'id_model' => 372,
                'id_fuel' => 5,
            ),
            409 => 
            array (
                'id_reg' => 410,
                'id_model' => 373,
                'id_fuel' => 6,
            ),
            410 => 
            array (
                'id_reg' => 411,
                'id_model' => 374,
                'id_fuel' => 4,
            ),
            411 => 
            array (
                'id_reg' => 412,
                'id_model' => 375,
                'id_fuel' => 4,
            ),
            412 => 
            array (
                'id_reg' => 413,
                'id_model' => 376,
                'id_fuel' => 4,
            ),
            413 => 
            array (
                'id_reg' => 414,
                'id_model' => 377,
                'id_fuel' => 1,
            ),
            414 => 
            array (
                'id_reg' => 415,
                'id_model' => 377,
                'id_fuel' => 2,
            ),
            415 => 
            array (
                'id_reg' => 416,
                'id_model' => 377,
                'id_fuel' => 4,
            ),
            416 => 
            array (
                'id_reg' => 417,
                'id_model' => 378,
                'id_fuel' => 4,
            ),
            417 => 
            array (
                'id_reg' => 418,
                'id_model' => 379,
                'id_fuel' => 4,
            ),
            418 => 
            array (
                'id_reg' => 419,
                'id_model' => 380,
                'id_fuel' => 4,
            ),
            419 => 
            array (
                'id_reg' => 420,
                'id_model' => 381,
                'id_fuel' => 4,
            ),
            420 => 
            array (
                'id_reg' => 421,
                'id_model' => 382,
                'id_fuel' => 4,
            ),
            421 => 
            array (
                'id_reg' => 422,
                'id_model' => 383,
                'id_fuel' => 4,
            ),
            422 => 
            array (
                'id_reg' => 423,
                'id_model' => 384,
                'id_fuel' => 4,
            ),
            423 => 
            array (
                'id_reg' => 424,
                'id_model' => 385,
                'id_fuel' => 4,
            ),
            424 => 
            array (
                'id_reg' => 425,
                'id_model' => 386,
                'id_fuel' => 4,
            ),
            425 => 
            array (
                'id_reg' => 426,
                'id_model' => 387,
                'id_fuel' => 2,
            ),
            426 => 
            array (
                'id_reg' => 427,
                'id_model' => 387,
                'id_fuel' => 4,
            ),
            427 => 
            array (
                'id_reg' => 428,
                'id_model' => 388,
                'id_fuel' => 4,
            ),
            428 => 
            array (
                'id_reg' => 429,
                'id_model' => 388,
                'id_fuel' => 5,
            ),
            429 => 
            array (
                'id_reg' => 430,
                'id_model' => 389,
                'id_fuel' => 2,
            ),
            430 => 
            array (
                'id_reg' => 431,
                'id_model' => 389,
                'id_fuel' => 4,
            ),
            431 => 
            array (
                'id_reg' => 432,
                'id_model' => 390,
                'id_fuel' => 4,
            ),
            432 => 
            array (
                'id_reg' => 433,
                'id_model' => 391,
                'id_fuel' => 4,
            ),
            433 => 
            array (
                'id_reg' => 434,
                'id_model' => 392,
                'id_fuel' => 4,
            ),
            434 => 
            array (
                'id_reg' => 435,
                'id_model' => 393,
                'id_fuel' => 2,
            ),
            435 => 
            array (
                'id_reg' => 436,
                'id_model' => 393,
                'id_fuel' => 4,
            ),
            436 => 
            array (
                'id_reg' => 437,
                'id_model' => 394,
                'id_fuel' => 4,
            ),
            437 => 
            array (
                'id_reg' => 438,
                'id_model' => 395,
                'id_fuel' => 2,
            ),
            438 => 
            array (
                'id_reg' => 439,
                'id_model' => 395,
                'id_fuel' => 4,
            ),
            439 => 
            array (
                'id_reg' => 440,
                'id_model' => 396,
                'id_fuel' => 4,
            ),
            440 => 
            array (
                'id_reg' => 441,
                'id_model' => 397,
                'id_fuel' => 4,
            ),
            441 => 
            array (
                'id_reg' => 442,
                'id_model' => 398,
                'id_fuel' => 4,
            ),
            442 => 
            array (
                'id_reg' => 443,
                'id_model' => 399,
                'id_fuel' => 4,
            ),
            443 => 
            array (
                'id_reg' => 444,
                'id_model' => 400,
                'id_fuel' => 4,
            ),
            444 => 
            array (
                'id_reg' => 445,
                'id_model' => 401,
                'id_fuel' => 4,
            ),
            445 => 
            array (
                'id_reg' => 446,
                'id_model' => 402,
                'id_fuel' => 4,
            ),
            446 => 
            array (
                'id_reg' => 447,
                'id_model' => 403,
                'id_fuel' => 2,
            ),
            447 => 
            array (
                'id_reg' => 448,
                'id_model' => 404,
                'id_fuel' => 2,
            ),
            448 => 
            array (
                'id_reg' => 449,
                'id_model' => 405,
                'id_fuel' => 4,
            ),
            449 => 
            array (
                'id_reg' => 450,
                'id_model' => 406,
                'id_fuel' => 4,
            ),
            450 => 
            array (
                'id_reg' => 451,
                'id_model' => 407,
                'id_fuel' => 2,
            ),
            451 => 
            array (
                'id_reg' => 452,
                'id_model' => 408,
                'id_fuel' => 2,
            ),
            452 => 
            array (
                'id_reg' => 453,
                'id_model' => 409,
                'id_fuel' => 2,
            ),
            453 => 
            array (
                'id_reg' => 454,
                'id_model' => 410,
                'id_fuel' => 4,
            ),
            454 => 
            array (
                'id_reg' => 455,
                'id_model' => 411,
                'id_fuel' => 4,
            ),
            455 => 
            array (
                'id_reg' => 456,
                'id_model' => 412,
                'id_fuel' => 2,
            ),
            456 => 
            array (
                'id_reg' => 457,
                'id_model' => 413,
                'id_fuel' => 2,
            ),
            457 => 
            array (
                'id_reg' => 458,
                'id_model' => 413,
                'id_fuel' => 4,
            ),
            458 => 
            array (
                'id_reg' => 459,
                'id_model' => 414,
                'id_fuel' => 2,
            ),
            459 => 
            array (
                'id_reg' => 460,
                'id_model' => 415,
                'id_fuel' => 2,
            ),
            460 => 
            array (
                'id_reg' => 461,
                'id_model' => 416,
                'id_fuel' => 2,
            ),
            461 => 
            array (
                'id_reg' => 462,
                'id_model' => 417,
                'id_fuel' => 2,
            ),
            462 => 
            array (
                'id_reg' => 463,
                'id_model' => 418,
                'id_fuel' => 2,
            ),
            463 => 
            array (
                'id_reg' => 464,
                'id_model' => 419,
                'id_fuel' => 4,
            ),
            464 => 
            array (
                'id_reg' => 465,
                'id_model' => 420,
                'id_fuel' => 4,
            ),
            465 => 
            array (
                'id_reg' => 466,
                'id_model' => 421,
                'id_fuel' => 4,
            ),
            466 => 
            array (
                'id_reg' => 467,
                'id_model' => 422,
                'id_fuel' => 4,
            ),
            467 => 
            array (
                'id_reg' => 468,
                'id_model' => 423,
                'id_fuel' => 4,
            ),
            468 => 
            array (
                'id_reg' => 469,
                'id_model' => 424,
                'id_fuel' => 4,
            ),
            469 => 
            array (
                'id_reg' => 470,
                'id_model' => 425,
                'id_fuel' => 4,
            ),
            470 => 
            array (
                'id_reg' => 471,
                'id_model' => 426,
                'id_fuel' => 4,
            ),
            471 => 
            array (
                'id_reg' => 472,
                'id_model' => 427,
                'id_fuel' => 4,
            ),
            472 => 
            array (
                'id_reg' => 473,
                'id_model' => 428,
                'id_fuel' => 2,
            ),
            473 => 
            array (
                'id_reg' => 474,
                'id_model' => 428,
                'id_fuel' => 4,
            ),
            474 => 
            array (
                'id_reg' => 475,
                'id_model' => 429,
                'id_fuel' => 4,
            ),
            475 => 
            array (
                'id_reg' => 476,
                'id_model' => 430,
                'id_fuel' => 4,
            ),
            476 => 
            array (
                'id_reg' => 477,
                'id_model' => 431,
                'id_fuel' => 4,
            ),
            477 => 
            array (
                'id_reg' => 478,
                'id_model' => 432,
                'id_fuel' => 4,
            ),
            478 => 
            array (
                'id_reg' => 479,
                'id_model' => 433,
                'id_fuel' => 4,
            ),
            479 => 
            array (
                'id_reg' => 480,
                'id_model' => 434,
                'id_fuel' => 4,
            ),
            480 => 
            array (
                'id_reg' => 481,
                'id_model' => 435,
                'id_fuel' => 4,
            ),
            481 => 
            array (
                'id_reg' => 482,
                'id_model' => 436,
                'id_fuel' => 4,
            ),
            482 => 
            array (
                'id_reg' => 483,
                'id_model' => 437,
                'id_fuel' => 4,
            ),
            483 => 
            array (
                'id_reg' => 484,
                'id_model' => 438,
                'id_fuel' => 4,
            ),
            484 => 
            array (
                'id_reg' => 485,
                'id_model' => 439,
                'id_fuel' => 4,
            ),
            485 => 
            array (
                'id_reg' => 486,
                'id_model' => 440,
                'id_fuel' => 4,
            ),
            486 => 
            array (
                'id_reg' => 487,
                'id_model' => 441,
                'id_fuel' => 4,
            ),
            487 => 
            array (
                'id_reg' => 488,
                'id_model' => 442,
                'id_fuel' => 2,
            ),
            488 => 
            array (
                'id_reg' => 489,
                'id_model' => 442,
                'id_fuel' => 4,
            ),
            489 => 
            array (
                'id_reg' => 490,
                'id_model' => 443,
                'id_fuel' => 4,
            ),
            490 => 
            array (
                'id_reg' => 491,
                'id_model' => 444,
                'id_fuel' => 4,
            ),
            491 => 
            array (
                'id_reg' => 492,
                'id_model' => 445,
                'id_fuel' => 2,
            ),
            492 => 
            array (
                'id_reg' => 493,
                'id_model' => 446,
                'id_fuel' => 4,
            ),
            493 => 
            array (
                'id_reg' => 494,
                'id_model' => 447,
                'id_fuel' => 2,
            ),
            494 => 
            array (
                'id_reg' => 495,
                'id_model' => 448,
                'id_fuel' => 4,
            ),
            495 => 
            array (
                'id_reg' => 496,
                'id_model' => 449,
                'id_fuel' => 4,
            ),
            496 => 
            array (
                'id_reg' => 497,
                'id_model' => 450,
                'id_fuel' => 4,
            ),
            497 => 
            array (
                'id_reg' => 498,
                'id_model' => 451,
                'id_fuel' => 4,
            ),
            498 => 
            array (
                'id_reg' => 499,
                'id_model' => 452,
                'id_fuel' => 4,
            ),
            499 => 
            array (
                'id_reg' => 500,
                'id_model' => 453,
                'id_fuel' => 2,
            ),
        ));
        \DB::table('model_rel_fuel')->insert(array (
            0 => 
            array (
                'id_reg' => 501,
                'id_model' => 454,
                'id_fuel' => 4,
            ),
            1 => 
            array (
                'id_reg' => 502,
                'id_model' => 455,
                'id_fuel' => 4,
            ),
            2 => 
            array (
                'id_reg' => 503,
                'id_model' => 456,
                'id_fuel' => 4,
            ),
            3 => 
            array (
                'id_reg' => 504,
                'id_model' => 457,
                'id_fuel' => 4,
            ),
            4 => 
            array (
                'id_reg' => 505,
                'id_model' => 458,
                'id_fuel' => 4,
            ),
            5 => 
            array (
                'id_reg' => 506,
                'id_model' => 459,
                'id_fuel' => 4,
            ),
            6 => 
            array (
                'id_reg' => 507,
                'id_model' => 460,
                'id_fuel' => 4,
            ),
            7 => 
            array (
                'id_reg' => 508,
                'id_model' => 461,
                'id_fuel' => 4,
            ),
            8 => 
            array (
                'id_reg' => 509,
                'id_model' => 462,
                'id_fuel' => 2,
            ),
            9 => 
            array (
                'id_reg' => 510,
                'id_model' => 463,
                'id_fuel' => 4,
            ),
            10 => 
            array (
                'id_reg' => 511,
                'id_model' => 464,
                'id_fuel' => 4,
            ),
            11 => 
            array (
                'id_reg' => 512,
                'id_model' => 465,
                'id_fuel' => 4,
            ),
            12 => 
            array (
                'id_reg' => 513,
                'id_model' => 466,
                'id_fuel' => 4,
            ),
            13 => 
            array (
                'id_reg' => 514,
                'id_model' => 467,
                'id_fuel' => 4,
            ),
            14 => 
            array (
                'id_reg' => 515,
                'id_model' => 468,
                'id_fuel' => 4,
            ),
            15 => 
            array (
                'id_reg' => 516,
                'id_model' => 469,
                'id_fuel' => 4,
            ),
            16 => 
            array (
                'id_reg' => 517,
                'id_model' => 470,
                'id_fuel' => 4,
            ),
            17 => 
            array (
                'id_reg' => 518,
                'id_model' => 471,
                'id_fuel' => 4,
            ),
            18 => 
            array (
                'id_reg' => 519,
                'id_model' => 472,
                'id_fuel' => 4,
            ),
            19 => 
            array (
                'id_reg' => 520,
                'id_model' => 473,
                'id_fuel' => 4,
            ),
            20 => 
            array (
                'id_reg' => 521,
                'id_model' => 474,
                'id_fuel' => 4,
            ),
            21 => 
            array (
                'id_reg' => 522,
                'id_model' => 475,
                'id_fuel' => 4,
            ),
            22 => 
            array (
                'id_reg' => 523,
                'id_model' => 476,
                'id_fuel' => 4,
            ),
            23 => 
            array (
                'id_reg' => 524,
                'id_model' => 477,
                'id_fuel' => 2,
            ),
            24 => 
            array (
                'id_reg' => 525,
                'id_model' => 477,
                'id_fuel' => 4,
            ),
            25 => 
            array (
                'id_reg' => 526,
                'id_model' => 478,
                'id_fuel' => 4,
            ),
            26 => 
            array (
                'id_reg' => 527,
                'id_model' => 479,
                'id_fuel' => 4,
            ),
            27 => 
            array (
                'id_reg' => 528,
                'id_model' => 480,
                'id_fuel' => 4,
            ),
            28 => 
            array (
                'id_reg' => 529,
                'id_model' => 481,
                'id_fuel' => 4,
            ),
            29 => 
            array (
                'id_reg' => 530,
                'id_model' => 482,
                'id_fuel' => 4,
            ),
            30 => 
            array (
                'id_reg' => 531,
                'id_model' => 483,
                'id_fuel' => 4,
            ),
            31 => 
            array (
                'id_reg' => 532,
                'id_model' => 484,
                'id_fuel' => 2,
            ),
            32 => 
            array (
                'id_reg' => 533,
                'id_model' => 484,
                'id_fuel' => 4,
            ),
            33 => 
            array (
                'id_reg' => 534,
                'id_model' => 485,
                'id_fuel' => 4,
            ),
            34 => 
            array (
                'id_reg' => 535,
                'id_model' => 486,
                'id_fuel' => 2,
            ),
            35 => 
            array (
                'id_reg' => 536,
                'id_model' => 486,
                'id_fuel' => 4,
            ),
            36 => 
            array (
                'id_reg' => 537,
                'id_model' => 487,
                'id_fuel' => 2,
            ),
            37 => 
            array (
                'id_reg' => 538,
                'id_model' => 488,
                'id_fuel' => 4,
            ),
            38 => 
            array (
                'id_reg' => 539,
                'id_model' => 489,
                'id_fuel' => 2,
            ),
            39 => 
            array (
                'id_reg' => 540,
                'id_model' => 489,
                'id_fuel' => 4,
            ),
            40 => 
            array (
                'id_reg' => 541,
                'id_model' => 490,
                'id_fuel' => 4,
            ),
            41 => 
            array (
                'id_reg' => 542,
                'id_model' => 491,
                'id_fuel' => 4,
            ),
            42 => 
            array (
                'id_reg' => 543,
                'id_model' => 492,
                'id_fuel' => 4,
            ),
            43 => 
            array (
                'id_reg' => 544,
                'id_model' => 493,
                'id_fuel' => 4,
            ),
            44 => 
            array (
                'id_reg' => 545,
                'id_model' => 494,
                'id_fuel' => 4,
            ),
            45 => 
            array (
                'id_reg' => 546,
                'id_model' => 495,
                'id_fuel' => 2,
            ),
            46 => 
            array (
                'id_reg' => 547,
                'id_model' => 496,
                'id_fuel' => 4,
            ),
            47 => 
            array (
                'id_reg' => 548,
                'id_model' => 497,
                'id_fuel' => 4,
            ),
            48 => 
            array (
                'id_reg' => 549,
                'id_model' => 498,
                'id_fuel' => 2,
            ),
            49 => 
            array (
                'id_reg' => 550,
                'id_model' => 498,
                'id_fuel' => 4,
            ),
            50 => 
            array (
                'id_reg' => 551,
                'id_model' => 499,
                'id_fuel' => 2,
            ),
            51 => 
            array (
                'id_reg' => 552,
                'id_model' => 499,
                'id_fuel' => 4,
            ),
            52 => 
            array (
                'id_reg' => 553,
                'id_model' => 500,
                'id_fuel' => 4,
            ),
            53 => 
            array (
                'id_reg' => 554,
                'id_model' => 501,
                'id_fuel' => 4,
            ),
            54 => 
            array (
                'id_reg' => 555,
                'id_model' => 502,
                'id_fuel' => 4,
            ),
            55 => 
            array (
                'id_reg' => 556,
                'id_model' => 503,
                'id_fuel' => 4,
            ),
            56 => 
            array (
                'id_reg' => 557,
                'id_model' => 504,
                'id_fuel' => 2,
            ),
            57 => 
            array (
                'id_reg' => 558,
                'id_model' => 504,
                'id_fuel' => 4,
            ),
            58 => 
            array (
                'id_reg' => 559,
                'id_model' => 505,
                'id_fuel' => 4,
            ),
            59 => 
            array (
                'id_reg' => 560,
                'id_model' => 506,
                'id_fuel' => 4,
            ),
            60 => 
            array (
                'id_reg' => 561,
                'id_model' => 507,
                'id_fuel' => 2,
            ),
            61 => 
            array (
                'id_reg' => 562,
                'id_model' => 507,
                'id_fuel' => 4,
            ),
            62 => 
            array (
                'id_reg' => 563,
                'id_model' => 508,
                'id_fuel' => 4,
            ),
            63 => 
            array (
                'id_reg' => 564,
                'id_model' => 509,
                'id_fuel' => 4,
            ),
            64 => 
            array (
                'id_reg' => 565,
                'id_model' => 510,
                'id_fuel' => 4,
            ),
            65 => 
            array (
                'id_reg' => 566,
                'id_model' => 511,
                'id_fuel' => 4,
            ),
            66 => 
            array (
                'id_reg' => 567,
                'id_model' => 512,
                'id_fuel' => 4,
            ),
            67 => 
            array (
                'id_reg' => 568,
                'id_model' => 513,
                'id_fuel' => 4,
            ),
            68 => 
            array (
                'id_reg' => 569,
                'id_model' => 514,
                'id_fuel' => 4,
            ),
            69 => 
            array (
                'id_reg' => 570,
                'id_model' => 515,
                'id_fuel' => 2,
            ),
            70 => 
            array (
                'id_reg' => 571,
                'id_model' => 515,
                'id_fuel' => 4,
            ),
            71 => 
            array (
                'id_reg' => 572,
                'id_model' => 516,
                'id_fuel' => 4,
            ),
            72 => 
            array (
                'id_reg' => 573,
                'id_model' => 517,
                'id_fuel' => 4,
            ),
            73 => 
            array (
                'id_reg' => 574,
                'id_model' => 518,
                'id_fuel' => 2,
            ),
            74 => 
            array (
                'id_reg' => 575,
                'id_model' => 518,
                'id_fuel' => 4,
            ),
            75 => 
            array (
                'id_reg' => 576,
                'id_model' => 519,
                'id_fuel' => 4,
            ),
            76 => 
            array (
                'id_reg' => 577,
                'id_model' => 520,
                'id_fuel' => 4,
            ),
            77 => 
            array (
                'id_reg' => 578,
                'id_model' => 521,
                'id_fuel' => 4,
            ),
            78 => 
            array (
                'id_reg' => 579,
                'id_model' => 522,
                'id_fuel' => 4,
            ),
            79 => 
            array (
                'id_reg' => 580,
                'id_model' => 523,
                'id_fuel' => 4,
            ),
            80 => 
            array (
                'id_reg' => 581,
                'id_model' => 524,
                'id_fuel' => 4,
            ),
            81 => 
            array (
                'id_reg' => 582,
                'id_model' => 525,
                'id_fuel' => 4,
            ),
            82 => 
            array (
                'id_reg' => 583,
                'id_model' => 526,
                'id_fuel' => 4,
            ),
            83 => 
            array (
                'id_reg' => 584,
                'id_model' => 527,
                'id_fuel' => 4,
            ),
            84 => 
            array (
                'id_reg' => 585,
                'id_model' => 528,
                'id_fuel' => 4,
            ),
            85 => 
            array (
                'id_reg' => 586,
                'id_model' => 529,
                'id_fuel' => 4,
            ),
            86 => 
            array (
                'id_reg' => 587,
                'id_model' => 530,
                'id_fuel' => 2,
            ),
            87 => 
            array (
                'id_reg' => 588,
                'id_model' => 530,
                'id_fuel' => 4,
            ),
            88 => 
            array (
                'id_reg' => 589,
                'id_model' => 531,
                'id_fuel' => 4,
            ),
            89 => 
            array (
                'id_reg' => 590,
                'id_model' => 532,
                'id_fuel' => 4,
            ),
            90 => 
            array (
                'id_reg' => 591,
                'id_model' => 533,
                'id_fuel' => 4,
            ),
            91 => 
            array (
                'id_reg' => 592,
                'id_model' => 534,
                'id_fuel' => 4,
            ),
            92 => 
            array (
                'id_reg' => 593,
                'id_model' => 535,
                'id_fuel' => 4,
            ),
            93 => 
            array (
                'id_reg' => 594,
                'id_model' => 536,
                'id_fuel' => 4,
            ),
            94 => 
            array (
                'id_reg' => 595,
                'id_model' => 537,
                'id_fuel' => 4,
            ),
            95 => 
            array (
                'id_reg' => 596,
                'id_model' => 538,
                'id_fuel' => 4,
            ),
            96 => 
            array (
                'id_reg' => 597,
                'id_model' => 539,
                'id_fuel' => 4,
            ),
            97 => 
            array (
                'id_reg' => 598,
                'id_model' => 540,
                'id_fuel' => 2,
            ),
            98 => 
            array (
                'id_reg' => 599,
                'id_model' => 540,
                'id_fuel' => 4,
            ),
            99 => 
            array (
                'id_reg' => 600,
                'id_model' => 541,
                'id_fuel' => 4,
            ),
            100 => 
            array (
                'id_reg' => 601,
                'id_model' => 542,
                'id_fuel' => 4,
            ),
            101 => 
            array (
                'id_reg' => 602,
                'id_model' => 543,
                'id_fuel' => 2,
            ),
            102 => 
            array (
                'id_reg' => 603,
                'id_model' => 543,
                'id_fuel' => 4,
            ),
            103 => 
            array (
                'id_reg' => 604,
                'id_model' => 544,
                'id_fuel' => 4,
            ),
            104 => 
            array (
                'id_reg' => 605,
                'id_model' => 545,
                'id_fuel' => 4,
            ),
            105 => 
            array (
                'id_reg' => 606,
                'id_model' => 546,
                'id_fuel' => 4,
            ),
            106 => 
            array (
                'id_reg' => 607,
                'id_model' => 547,
                'id_fuel' => 4,
            ),
            107 => 
            array (
                'id_reg' => 608,
                'id_model' => 548,
                'id_fuel' => 4,
            ),
            108 => 
            array (
                'id_reg' => 609,
                'id_model' => 549,
                'id_fuel' => 4,
            ),
            109 => 
            array (
                'id_reg' => 610,
                'id_model' => 550,
                'id_fuel' => 4,
            ),
            110 => 
            array (
                'id_reg' => 611,
                'id_model' => 551,
                'id_fuel' => 4,
            ),
            111 => 
            array (
                'id_reg' => 612,
                'id_model' => 552,
                'id_fuel' => 4,
            ),
            112 => 
            array (
                'id_reg' => 613,
                'id_model' => 553,
                'id_fuel' => 2,
            ),
            113 => 
            array (
                'id_reg' => 614,
                'id_model' => 553,
                'id_fuel' => 4,
            ),
            114 => 
            array (
                'id_reg' => 615,
                'id_model' => 554,
                'id_fuel' => 4,
            ),
            115 => 
            array (
                'id_reg' => 616,
                'id_model' => 555,
                'id_fuel' => 4,
            ),
            116 => 
            array (
                'id_reg' => 617,
                'id_model' => 556,
                'id_fuel' => 4,
            ),
            117 => 
            array (
                'id_reg' => 618,
                'id_model' => 557,
                'id_fuel' => 4,
            ),
            118 => 
            array (
                'id_reg' => 619,
                'id_model' => 558,
                'id_fuel' => 4,
            ),
            119 => 
            array (
                'id_reg' => 620,
                'id_model' => 559,
                'id_fuel' => 4,
            ),
            120 => 
            array (
                'id_reg' => 621,
                'id_model' => 560,
                'id_fuel' => 4,
            ),
            121 => 
            array (
                'id_reg' => 622,
                'id_model' => 561,
                'id_fuel' => 4,
            ),
            122 => 
            array (
                'id_reg' => 623,
                'id_model' => 562,
                'id_fuel' => 4,
            ),
            123 => 
            array (
                'id_reg' => 624,
                'id_model' => 563,
                'id_fuel' => 4,
            ),
            124 => 
            array (
                'id_reg' => 625,
                'id_model' => 564,
                'id_fuel' => 2,
            ),
            125 => 
            array (
                'id_reg' => 626,
                'id_model' => 564,
                'id_fuel' => 4,
            ),
            126 => 
            array (
                'id_reg' => 627,
                'id_model' => 565,
                'id_fuel' => 4,
            ),
            127 => 
            array (
                'id_reg' => 628,
                'id_model' => 566,
                'id_fuel' => 4,
            ),
            128 => 
            array (
                'id_reg' => 629,
                'id_model' => 567,
                'id_fuel' => 4,
            ),
            129 => 
            array (
                'id_reg' => 630,
                'id_model' => 568,
                'id_fuel' => 4,
            ),
            130 => 
            array (
                'id_reg' => 631,
                'id_model' => 569,
                'id_fuel' => 2,
            ),
            131 => 
            array (
                'id_reg' => 632,
                'id_model' => 569,
                'id_fuel' => 4,
            ),
            132 => 
            array (
                'id_reg' => 633,
                'id_model' => 570,
                'id_fuel' => 2,
            ),
            133 => 
            array (
                'id_reg' => 634,
                'id_model' => 570,
                'id_fuel' => 4,
            ),
            134 => 
            array (
                'id_reg' => 635,
                'id_model' => 571,
                'id_fuel' => 2,
            ),
            135 => 
            array (
                'id_reg' => 636,
                'id_model' => 572,
                'id_fuel' => 2,
            ),
            136 => 
            array (
                'id_reg' => 637,
                'id_model' => 572,
                'id_fuel' => 4,
            ),
            137 => 
            array (
                'id_reg' => 638,
                'id_model' => 573,
                'id_fuel' => 2,
            ),
            138 => 
            array (
                'id_reg' => 639,
                'id_model' => 573,
                'id_fuel' => 4,
            ),
            139 => 
            array (
                'id_reg' => 640,
                'id_model' => 574,
                'id_fuel' => 4,
            ),
            140 => 
            array (
                'id_reg' => 641,
                'id_model' => 575,
                'id_fuel' => 4,
            ),
            141 => 
            array (
                'id_reg' => 642,
                'id_model' => 576,
                'id_fuel' => 4,
            ),
            142 => 
            array (
                'id_reg' => 643,
                'id_model' => 577,
                'id_fuel' => 2,
            ),
            143 => 
            array (
                'id_reg' => 644,
                'id_model' => 577,
                'id_fuel' => 4,
            ),
            144 => 
            array (
                'id_reg' => 645,
                'id_model' => 578,
                'id_fuel' => 2,
            ),
            145 => 
            array (
                'id_reg' => 646,
                'id_model' => 578,
                'id_fuel' => 3,
            ),
            146 => 
            array (
                'id_reg' => 647,
                'id_model' => 578,
                'id_fuel' => 4,
            ),
            147 => 
            array (
                'id_reg' => 648,
                'id_model' => 579,
                'id_fuel' => 6,
            ),
            148 => 
            array (
                'id_reg' => 649,
                'id_model' => 580,
                'id_fuel' => 2,
            ),
            149 => 
            array (
                'id_reg' => 650,
                'id_model' => 580,
                'id_fuel' => 4,
            ),
            150 => 
            array (
                'id_reg' => 651,
                'id_model' => 581,
                'id_fuel' => 2,
            ),
            151 => 
            array (
                'id_reg' => 652,
                'id_model' => 581,
                'id_fuel' => 4,
            ),
            152 => 
            array (
                'id_reg' => 653,
                'id_model' => 582,
                'id_fuel' => 2,
            ),
            153 => 
            array (
                'id_reg' => 654,
                'id_model' => 582,
                'id_fuel' => 4,
            ),
            154 => 
            array (
                'id_reg' => 655,
                'id_model' => 583,
                'id_fuel' => 2,
            ),
            155 => 
            array (
                'id_reg' => 656,
                'id_model' => 583,
                'id_fuel' => 4,
            ),
            156 => 
            array (
                'id_reg' => 657,
                'id_model' => 584,
                'id_fuel' => 2,
            ),
            157 => 
            array (
                'id_reg' => 658,
                'id_model' => 584,
                'id_fuel' => 4,
            ),
            158 => 
            array (
                'id_reg' => 659,
                'id_model' => 585,
                'id_fuel' => 2,
            ),
            159 => 
            array (
                'id_reg' => 660,
                'id_model' => 585,
                'id_fuel' => 4,
            ),
            160 => 
            array (
                'id_reg' => 661,
                'id_model' => 586,
                'id_fuel' => 2,
            ),
            161 => 
            array (
                'id_reg' => 662,
                'id_model' => 586,
                'id_fuel' => 4,
            ),
            162 => 
            array (
                'id_reg' => 663,
                'id_model' => 587,
                'id_fuel' => 4,
            ),
            163 => 
            array (
                'id_reg' => 664,
                'id_model' => 588,
                'id_fuel' => 2,
            ),
            164 => 
            array (
                'id_reg' => 665,
                'id_model' => 588,
                'id_fuel' => 4,
            ),
            165 => 
            array (
                'id_reg' => 666,
                'id_model' => 589,
                'id_fuel' => 2,
            ),
            166 => 
            array (
                'id_reg' => 667,
                'id_model' => 589,
                'id_fuel' => 4,
            ),
            167 => 
            array (
                'id_reg' => 668,
                'id_model' => 590,
                'id_fuel' => 2,
            ),
            168 => 
            array (
                'id_reg' => 669,
                'id_model' => 590,
                'id_fuel' => 4,
            ),
            169 => 
            array (
                'id_reg' => 670,
                'id_model' => 591,
                'id_fuel' => 2,
            ),
            170 => 
            array (
                'id_reg' => 671,
                'id_model' => 591,
                'id_fuel' => 4,
            ),
            171 => 
            array (
                'id_reg' => 672,
                'id_model' => 592,
                'id_fuel' => 2,
            ),
            172 => 
            array (
                'id_reg' => 673,
                'id_model' => 592,
                'id_fuel' => 4,
            ),
            173 => 
            array (
                'id_reg' => 674,
                'id_model' => 593,
                'id_fuel' => 2,
            ),
            174 => 
            array (
                'id_reg' => 675,
                'id_model' => 593,
                'id_fuel' => 4,
            ),
            175 => 
            array (
                'id_reg' => 676,
                'id_model' => 594,
                'id_fuel' => 4,
            ),
            176 => 
            array (
                'id_reg' => 677,
                'id_model' => 595,
                'id_fuel' => 4,
            ),
            177 => 
            array (
                'id_reg' => 678,
                'id_model' => 596,
                'id_fuel' => 4,
            ),
            178 => 
            array (
                'id_reg' => 679,
                'id_model' => 597,
                'id_fuel' => 6,
            ),
            179 => 
            array (
                'id_reg' => 680,
                'id_model' => 598,
                'id_fuel' => 2,
            ),
            180 => 
            array (
                'id_reg' => 681,
                'id_model' => 598,
                'id_fuel' => 4,
            ),
            181 => 
            array (
                'id_reg' => 682,
                'id_model' => 599,
                'id_fuel' => 4,
            ),
            182 => 
            array (
                'id_reg' => 683,
                'id_model' => 600,
                'id_fuel' => 4,
            ),
            183 => 
            array (
                'id_reg' => 684,
                'id_model' => 601,
                'id_fuel' => 4,
            ),
            184 => 
            array (
                'id_reg' => 685,
                'id_model' => 602,
                'id_fuel' => 2,
            ),
            185 => 
            array (
                'id_reg' => 686,
                'id_model' => 602,
                'id_fuel' => 4,
            ),
            186 => 
            array (
                'id_reg' => 687,
                'id_model' => 603,
                'id_fuel' => 2,
            ),
            187 => 
            array (
                'id_reg' => 688,
                'id_model' => 603,
                'id_fuel' => 4,
            ),
            188 => 
            array (
                'id_reg' => 689,
                'id_model' => 604,
                'id_fuel' => 2,
            ),
            189 => 
            array (
                'id_reg' => 690,
                'id_model' => 604,
                'id_fuel' => 4,
            ),
            190 => 
            array (
                'id_reg' => 691,
                'id_model' => 605,
                'id_fuel' => 4,
            ),
            191 => 
            array (
                'id_reg' => 692,
                'id_model' => 606,
                'id_fuel' => 4,
            ),
            192 => 
            array (
                'id_reg' => 693,
                'id_model' => 607,
                'id_fuel' => 2,
            ),
            193 => 
            array (
                'id_reg' => 694,
                'id_model' => 607,
                'id_fuel' => 4,
            ),
            194 => 
            array (
                'id_reg' => 695,
                'id_model' => 608,
                'id_fuel' => 2,
            ),
            195 => 
            array (
                'id_reg' => 696,
                'id_model' => 609,
                'id_fuel' => 4,
            ),
            196 => 
            array (
                'id_reg' => 697,
                'id_model' => 610,
                'id_fuel' => 2,
            ),
            197 => 
            array (
                'id_reg' => 698,
                'id_model' => 610,
                'id_fuel' => 4,
            ),
            198 => 
            array (
                'id_reg' => 699,
                'id_model' => 611,
                'id_fuel' => 2,
            ),
            199 => 
            array (
                'id_reg' => 700,
                'id_model' => 612,
                'id_fuel' => 4,
            ),
            200 => 
            array (
                'id_reg' => 701,
                'id_model' => 613,
                'id_fuel' => 4,
            ),
            201 => 
            array (
                'id_reg' => 702,
                'id_model' => 614,
                'id_fuel' => 4,
            ),
            202 => 
            array (
                'id_reg' => 703,
                'id_model' => 615,
                'id_fuel' => 2,
            ),
            203 => 
            array (
                'id_reg' => 704,
                'id_model' => 615,
                'id_fuel' => 4,
            ),
            204 => 
            array (
                'id_reg' => 705,
                'id_model' => 616,
                'id_fuel' => 2,
            ),
            205 => 
            array (
                'id_reg' => 706,
                'id_model' => 616,
                'id_fuel' => 4,
            ),
            206 => 
            array (
                'id_reg' => 707,
                'id_model' => 617,
                'id_fuel' => 4,
            ),
            207 => 
            array (
                'id_reg' => 708,
                'id_model' => 618,
                'id_fuel' => 4,
            ),
            208 => 
            array (
                'id_reg' => 709,
                'id_model' => 619,
                'id_fuel' => 4,
            ),
            209 => 
            array (
                'id_reg' => 710,
                'id_model' => 620,
                'id_fuel' => 2,
            ),
            210 => 
            array (
                'id_reg' => 711,
                'id_model' => 620,
                'id_fuel' => 6,
            ),
            211 => 
            array (
                'id_reg' => 712,
                'id_model' => 620,
                'id_fuel' => 4,
            ),
            212 => 
            array (
                'id_reg' => 713,
                'id_model' => 621,
                'id_fuel' => 6,
            ),
            213 => 
            array (
                'id_reg' => 714,
                'id_model' => 622,
                'id_fuel' => 6,
            ),
            214 => 
            array (
                'id_reg' => 715,
                'id_model' => 623,
                'id_fuel' => 6,
            ),
            215 => 
            array (
                'id_reg' => 716,
                'id_model' => 624,
                'id_fuel' => 6,
            ),
            216 => 
            array (
                'id_reg' => 717,
                'id_model' => 625,
                'id_fuel' => 6,
            ),
            217 => 
            array (
                'id_reg' => 718,
                'id_model' => 626,
                'id_fuel' => 4,
            ),
            218 => 
            array (
                'id_reg' => 719,
                'id_model' => 627,
                'id_fuel' => 6,
            ),
            219 => 
            array (
                'id_reg' => 720,
                'id_model' => 627,
                'id_fuel' => 4,
            ),
            220 => 
            array (
                'id_reg' => 721,
                'id_model' => 628,
                'id_fuel' => 2,
            ),
            221 => 
            array (
                'id_reg' => 722,
                'id_model' => 629,
                'id_fuel' => 6,
            ),
            222 => 
            array (
                'id_reg' => 723,
                'id_model' => 630,
                'id_fuel' => 6,
            ),
            223 => 
            array (
                'id_reg' => 724,
                'id_model' => 631,
                'id_fuel' => 6,
            ),
            224 => 
            array (
                'id_reg' => 725,
                'id_model' => 632,
                'id_fuel' => 6,
            ),
            225 => 
            array (
                'id_reg' => 726,
                'id_model' => 633,
                'id_fuel' => 4,
            ),
            226 => 
            array (
                'id_reg' => 727,
                'id_model' => 634,
                'id_fuel' => 4,
            ),
            227 => 
            array (
                'id_reg' => 728,
                'id_model' => 635,
                'id_fuel' => 4,
            ),
            228 => 
            array (
                'id_reg' => 729,
                'id_model' => 636,
                'id_fuel' => 6,
            ),
            229 => 
            array (
                'id_reg' => 730,
                'id_model' => 637,
                'id_fuel' => 6,
            ),
            230 => 
            array (
                'id_reg' => 731,
                'id_model' => 638,
                'id_fuel' => 6,
            ),
            231 => 
            array (
                'id_reg' => 732,
                'id_model' => 639,
                'id_fuel' => 4,
            ),
            232 => 
            array (
                'id_reg' => 733,
                'id_model' => 640,
                'id_fuel' => 4,
            ),
            233 => 
            array (
                'id_reg' => 734,
                'id_model' => 641,
                'id_fuel' => 4,
            ),
            234 => 
            array (
                'id_reg' => 735,
                'id_model' => 642,
                'id_fuel' => 4,
            ),
            235 => 
            array (
                'id_reg' => 736,
                'id_model' => 643,
                'id_fuel' => 6,
            ),
            236 => 
            array (
                'id_reg' => 737,
                'id_model' => 644,
                'id_fuel' => 6,
            ),
            237 => 
            array (
                'id_reg' => 738,
                'id_model' => 644,
                'id_fuel' => 4,
            ),
            238 => 
            array (
                'id_reg' => 739,
                'id_model' => 645,
                'id_fuel' => 6,
            ),
            239 => 
            array (
                'id_reg' => 740,
                'id_model' => 646,
                'id_fuel' => 4,
            ),
            240 => 
            array (
                'id_reg' => 741,
                'id_model' => 647,
                'id_fuel' => 2,
            ),
            241 => 
            array (
                'id_reg' => 742,
                'id_model' => 648,
                'id_fuel' => 4,
            ),
            242 => 
            array (
                'id_reg' => 743,
                'id_model' => 649,
                'id_fuel' => 2,
            ),
            243 => 
            array (
                'id_reg' => 744,
                'id_model' => 649,
                'id_fuel' => 6,
            ),
            244 => 
            array (
                'id_reg' => 745,
                'id_model' => 649,
                'id_fuel' => 3,
            ),
            245 => 
            array (
                'id_reg' => 746,
                'id_model' => 649,
                'id_fuel' => 4,
            ),
            246 => 
            array (
                'id_reg' => 747,
                'id_model' => 650,
                'id_fuel' => 2,
            ),
            247 => 
            array (
                'id_reg' => 748,
                'id_model' => 651,
                'id_fuel' => 2,
            ),
            248 => 
            array (
                'id_reg' => 749,
                'id_model' => 651,
                'id_fuel' => 3,
            ),
            249 => 
            array (
                'id_reg' => 750,
                'id_model' => 651,
                'id_fuel' => 4,
            ),
            250 => 
            array (
                'id_reg' => 751,
                'id_model' => 652,
                'id_fuel' => 2,
            ),
            251 => 
            array (
                'id_reg' => 752,
                'id_model' => 652,
                'id_fuel' => 3,
            ),
            252 => 
            array (
                'id_reg' => 753,
                'id_model' => 652,
                'id_fuel' => 4,
            ),
            253 => 
            array (
                'id_reg' => 754,
                'id_model' => 653,
                'id_fuel' => 2,
            ),
            254 => 
            array (
                'id_reg' => 755,
                'id_model' => 654,
                'id_fuel' => 1,
            ),
            255 => 
            array (
                'id_reg' => 756,
                'id_model' => 654,
                'id_fuel' => 2,
            ),
            256 => 
            array (
                'id_reg' => 757,
                'id_model' => 654,
                'id_fuel' => 3,
            ),
            257 => 
            array (
                'id_reg' => 758,
                'id_model' => 654,
                'id_fuel' => 4,
            ),
            258 => 
            array (
                'id_reg' => 759,
                'id_model' => 655,
                'id_fuel' => 2,
            ),
            259 => 
            array (
                'id_reg' => 760,
                'id_model' => 655,
                'id_fuel' => 3,
            ),
            260 => 
            array (
                'id_reg' => 761,
                'id_model' => 655,
                'id_fuel' => 4,
            ),
            261 => 
            array (
                'id_reg' => 762,
                'id_model' => 656,
                'id_fuel' => 2,
            ),
            262 => 
            array (
                'id_reg' => 763,
                'id_model' => 656,
                'id_fuel' => 4,
            ),
            263 => 
            array (
                'id_reg' => 764,
                'id_model' => 657,
                'id_fuel' => 4,
            ),
            264 => 
            array (
                'id_reg' => 765,
                'id_model' => 658,
                'id_fuel' => 4,
            ),
            265 => 
            array (
                'id_reg' => 766,
                'id_model' => 659,
                'id_fuel' => 4,
            ),
            266 => 
            array (
                'id_reg' => 767,
                'id_model' => 660,
                'id_fuel' => 4,
            ),
            267 => 
            array (
                'id_reg' => 768,
                'id_model' => 661,
                'id_fuel' => 4,
            ),
            268 => 
            array (
                'id_reg' => 769,
                'id_model' => 662,
                'id_fuel' => 4,
            ),
            269 => 
            array (
                'id_reg' => 770,
                'id_model' => 663,
                'id_fuel' => 4,
            ),
            270 => 
            array (
                'id_reg' => 771,
                'id_model' => 664,
                'id_fuel' => 2,
            ),
            271 => 
            array (
                'id_reg' => 772,
                'id_model' => 665,
                'id_fuel' => 4,
            ),
            272 => 
            array (
                'id_reg' => 773,
                'id_model' => 666,
                'id_fuel' => 4,
            ),
            273 => 
            array (
                'id_reg' => 774,
                'id_model' => 667,
                'id_fuel' => 4,
            ),
            274 => 
            array (
                'id_reg' => 775,
                'id_model' => 668,
                'id_fuel' => 2,
            ),
            275 => 
            array (
                'id_reg' => 776,
                'id_model' => 669,
                'id_fuel' => 4,
            ),
            276 => 
            array (
                'id_reg' => 777,
                'id_model' => 670,
                'id_fuel' => 4,
            ),
            277 => 
            array (
                'id_reg' => 778,
                'id_model' => 671,
                'id_fuel' => 4,
            ),
            278 => 
            array (
                'id_reg' => 779,
                'id_model' => 672,
                'id_fuel' => 4,
            ),
            279 => 
            array (
                'id_reg' => 780,
                'id_model' => 673,
                'id_fuel' => 4,
            ),
            280 => 
            array (
                'id_reg' => 781,
                'id_model' => 674,
                'id_fuel' => 4,
            ),
            281 => 
            array (
                'id_reg' => 782,
                'id_model' => 675,
                'id_fuel' => 4,
            ),
            282 => 
            array (
                'id_reg' => 783,
                'id_model' => 676,
                'id_fuel' => 4,
            ),
            283 => 
            array (
                'id_reg' => 784,
                'id_model' => 677,
                'id_fuel' => 2,
            ),
            284 => 
            array (
                'id_reg' => 785,
                'id_model' => 678,
                'id_fuel' => 2,
            ),
            285 => 
            array (
                'id_reg' => 786,
                'id_model' => 679,
                'id_fuel' => 2,
            ),
            286 => 
            array (
                'id_reg' => 787,
                'id_model' => 680,
                'id_fuel' => 2,
            ),
            287 => 
            array (
                'id_reg' => 788,
                'id_model' => 681,
                'id_fuel' => 2,
            ),
            288 => 
            array (
                'id_reg' => 789,
                'id_model' => 682,
                'id_fuel' => 2,
            ),
            289 => 
            array (
                'id_reg' => 790,
                'id_model' => 683,
                'id_fuel' => 2,
            ),
            290 => 
            array (
                'id_reg' => 791,
                'id_model' => 684,
                'id_fuel' => 2,
            ),
            291 => 
            array (
                'id_reg' => 792,
                'id_model' => 685,
                'id_fuel' => 2,
            ),
            292 => 
            array (
                'id_reg' => 793,
                'id_model' => 686,
                'id_fuel' => 4,
            ),
            293 => 
            array (
                'id_reg' => 794,
                'id_model' => 687,
                'id_fuel' => 4,
            ),
            294 => 
            array (
                'id_reg' => 795,
                'id_model' => 688,
                'id_fuel' => 2,
            ),
            295 => 
            array (
                'id_reg' => 796,
                'id_model' => 688,
                'id_fuel' => 4,
            ),
            296 => 
            array (
                'id_reg' => 797,
                'id_model' => 689,
                'id_fuel' => 4,
            ),
            297 => 
            array (
                'id_reg' => 798,
                'id_model' => 690,
                'id_fuel' => 4,
            ),
            298 => 
            array (
                'id_reg' => 799,
                'id_model' => 691,
                'id_fuel' => 4,
            ),
            299 => 
            array (
                'id_reg' => 800,
                'id_model' => 692,
                'id_fuel' => 2,
            ),
            300 => 
            array (
                'id_reg' => 801,
                'id_model' => 693,
                'id_fuel' => 4,
            ),
            301 => 
            array (
                'id_reg' => 802,
                'id_model' => 694,
                'id_fuel' => 4,
            ),
            302 => 
            array (
                'id_reg' => 803,
                'id_model' => 695,
                'id_fuel' => 2,
            ),
            303 => 
            array (
                'id_reg' => 804,
                'id_model' => 696,
                'id_fuel' => 2,
            ),
            304 => 
            array (
                'id_reg' => 805,
                'id_model' => 697,
                'id_fuel' => 4,
            ),
            305 => 
            array (
                'id_reg' => 806,
                'id_model' => 698,
                'id_fuel' => 2,
            ),
            306 => 
            array (
                'id_reg' => 807,
                'id_model' => 699,
                'id_fuel' => 4,
            ),
            307 => 
            array (
                'id_reg' => 808,
                'id_model' => 700,
                'id_fuel' => 4,
            ),
            308 => 
            array (
                'id_reg' => 809,
                'id_model' => 701,
                'id_fuel' => 4,
            ),
            309 => 
            array (
                'id_reg' => 810,
                'id_model' => 702,
                'id_fuel' => 4,
            ),
            310 => 
            array (
                'id_reg' => 811,
                'id_model' => 703,
                'id_fuel' => 2,
            ),
            311 => 
            array (
                'id_reg' => 812,
                'id_model' => 703,
                'id_fuel' => 4,
            ),
            312 => 
            array (
                'id_reg' => 813,
                'id_model' => 704,
                'id_fuel' => 4,
            ),
            313 => 
            array (
                'id_reg' => 814,
                'id_model' => 705,
                'id_fuel' => 2,
            ),
            314 => 
            array (
                'id_reg' => 815,
                'id_model' => 705,
                'id_fuel' => 4,
            ),
            315 => 
            array (
                'id_reg' => 816,
                'id_model' => 706,
                'id_fuel' => 4,
            ),
            316 => 
            array (
                'id_reg' => 817,
                'id_model' => 707,
                'id_fuel' => 4,
            ),
            317 => 
            array (
                'id_reg' => 818,
                'id_model' => 708,
                'id_fuel' => 4,
            ),
            318 => 
            array (
                'id_reg' => 819,
                'id_model' => 709,
                'id_fuel' => 2,
            ),
            319 => 
            array (
                'id_reg' => 820,
                'id_model' => 709,
                'id_fuel' => 4,
            ),
            320 => 
            array (
                'id_reg' => 821,
                'id_model' => 710,
                'id_fuel' => 4,
            ),
            321 => 
            array (
                'id_reg' => 822,
                'id_model' => 711,
                'id_fuel' => 4,
            ),
            322 => 
            array (
                'id_reg' => 823,
                'id_model' => 712,
                'id_fuel' => 4,
            ),
            323 => 
            array (
                'id_reg' => 824,
                'id_model' => 713,
                'id_fuel' => 4,
            ),
            324 => 
            array (
                'id_reg' => 825,
                'id_model' => 714,
                'id_fuel' => 4,
            ),
            325 => 
            array (
                'id_reg' => 826,
                'id_model' => 715,
                'id_fuel' => 4,
            ),
            326 => 
            array (
                'id_reg' => 827,
                'id_model' => 716,
                'id_fuel' => 4,
            ),
            327 => 
            array (
                'id_reg' => 828,
                'id_model' => 717,
                'id_fuel' => 4,
            ),
            328 => 
            array (
                'id_reg' => 829,
                'id_model' => 718,
                'id_fuel' => 4,
            ),
            329 => 
            array (
                'id_reg' => 830,
                'id_model' => 719,
                'id_fuel' => 4,
            ),
            330 => 
            array (
                'id_reg' => 831,
                'id_model' => 720,
                'id_fuel' => 4,
            ),
            331 => 
            array (
                'id_reg' => 832,
                'id_model' => 721,
                'id_fuel' => 4,
            ),
            332 => 
            array (
                'id_reg' => 833,
                'id_model' => 722,
                'id_fuel' => 4,
            ),
            333 => 
            array (
                'id_reg' => 834,
                'id_model' => 723,
                'id_fuel' => 2,
            ),
            334 => 
            array (
                'id_reg' => 835,
                'id_model' => 723,
                'id_fuel' => 4,
            ),
            335 => 
            array (
                'id_reg' => 836,
                'id_model' => 724,
                'id_fuel' => 2,
            ),
            336 => 
            array (
                'id_reg' => 837,
                'id_model' => 725,
                'id_fuel' => 4,
            ),
            337 => 
            array (
                'id_reg' => 838,
                'id_model' => 726,
                'id_fuel' => 2,
            ),
            338 => 
            array (
                'id_reg' => 839,
                'id_model' => 726,
                'id_fuel' => 4,
            ),
            339 => 
            array (
                'id_reg' => 840,
                'id_model' => 727,
                'id_fuel' => 2,
            ),
            340 => 
            array (
                'id_reg' => 841,
                'id_model' => 727,
                'id_fuel' => 4,
            ),
            341 => 
            array (
                'id_reg' => 842,
                'id_model' => 728,
                'id_fuel' => 4,
            ),
            342 => 
            array (
                'id_reg' => 843,
                'id_model' => 729,
                'id_fuel' => 4,
            ),
            343 => 
            array (
                'id_reg' => 844,
                'id_model' => 730,
                'id_fuel' => 4,
            ),
            344 => 
            array (
                'id_reg' => 845,
                'id_model' => 731,
                'id_fuel' => 4,
            ),
            345 => 
            array (
                'id_reg' => 846,
                'id_model' => 732,
                'id_fuel' => 4,
            ),
            346 => 
            array (
                'id_reg' => 847,
                'id_model' => 733,
                'id_fuel' => 4,
            ),
            347 => 
            array (
                'id_reg' => 848,
                'id_model' => 734,
                'id_fuel' => 4,
            ),
            348 => 
            array (
                'id_reg' => 849,
                'id_model' => 735,
                'id_fuel' => 4,
            ),
            349 => 
            array (
                'id_reg' => 850,
                'id_model' => 736,
                'id_fuel' => 4,
            ),
            350 => 
            array (
                'id_reg' => 851,
                'id_model' => 737,
                'id_fuel' => 4,
            ),
            351 => 
            array (
                'id_reg' => 852,
                'id_model' => 738,
                'id_fuel' => 4,
            ),
            352 => 
            array (
                'id_reg' => 853,
                'id_model' => 739,
                'id_fuel' => 4,
            ),
            353 => 
            array (
                'id_reg' => 854,
                'id_model' => 740,
                'id_fuel' => 4,
            ),
            354 => 
            array (
                'id_reg' => 855,
                'id_model' => 741,
                'id_fuel' => 4,
            ),
            355 => 
            array (
                'id_reg' => 856,
                'id_model' => 742,
                'id_fuel' => 4,
            ),
            356 => 
            array (
                'id_reg' => 857,
                'id_model' => 743,
                'id_fuel' => 4,
            ),
            357 => 
            array (
                'id_reg' => 858,
                'id_model' => 744,
                'id_fuel' => 4,
            ),
            358 => 
            array (
                'id_reg' => 859,
                'id_model' => 745,
                'id_fuel' => 4,
            ),
            359 => 
            array (
                'id_reg' => 860,
                'id_model' => 746,
                'id_fuel' => 4,
            ),
            360 => 
            array (
                'id_reg' => 861,
                'id_model' => 747,
                'id_fuel' => 4,
            ),
            361 => 
            array (
                'id_reg' => 862,
                'id_model' => 748,
                'id_fuel' => 4,
            ),
            362 => 
            array (
                'id_reg' => 863,
                'id_model' => 749,
                'id_fuel' => 4,
            ),
            363 => 
            array (
                'id_reg' => 864,
                'id_model' => 750,
                'id_fuel' => 4,
            ),
            364 => 
            array (
                'id_reg' => 865,
                'id_model' => 751,
                'id_fuel' => 4,
            ),
            365 => 
            array (
                'id_reg' => 866,
                'id_model' => 752,
                'id_fuel' => 6,
            ),
            366 => 
            array (
                'id_reg' => 867,
                'id_model' => 753,
                'id_fuel' => 6,
            ),
            367 => 
            array (
                'id_reg' => 868,
                'id_model' => 754,
                'id_fuel' => 6,
            ),
            368 => 
            array (
                'id_reg' => 869,
                'id_model' => 755,
                'id_fuel' => 6,
            ),
            369 => 
            array (
                'id_reg' => 870,
                'id_model' => 756,
                'id_fuel' => 6,
            ),
            370 => 
            array (
                'id_reg' => 871,
                'id_model' => 757,
                'id_fuel' => 6,
            ),
            371 => 
            array (
                'id_reg' => 872,
                'id_model' => 758,
                'id_fuel' => 6,
            ),
            372 => 
            array (
                'id_reg' => 873,
                'id_model' => 759,
                'id_fuel' => 4,
            ),
            373 => 
            array (
                'id_reg' => 874,
                'id_model' => 760,
                'id_fuel' => 2,
            ),
            374 => 
            array (
                'id_reg' => 875,
                'id_model' => 760,
                'id_fuel' => 4,
            ),
            375 => 
            array (
                'id_reg' => 876,
                'id_model' => 761,
                'id_fuel' => 4,
            ),
            376 => 
            array (
                'id_reg' => 877,
                'id_model' => 762,
                'id_fuel' => 4,
            ),
            377 => 
            array (
                'id_reg' => 878,
                'id_model' => 763,
                'id_fuel' => 4,
            ),
            378 => 
            array (
                'id_reg' => 879,
                'id_model' => 764,
                'id_fuel' => 4,
            ),
            379 => 
            array (
                'id_reg' => 880,
                'id_model' => 765,
                'id_fuel' => 4,
            ),
            380 => 
            array (
                'id_reg' => 881,
                'id_model' => 766,
                'id_fuel' => 4,
            ),
            381 => 
            array (
                'id_reg' => 882,
                'id_model' => 767,
                'id_fuel' => 4,
            ),
            382 => 
            array (
                'id_reg' => 883,
                'id_model' => 768,
                'id_fuel' => 2,
            ),
            383 => 
            array (
                'id_reg' => 884,
                'id_model' => 768,
                'id_fuel' => 4,
            ),
            384 => 
            array (
                'id_reg' => 885,
                'id_model' => 769,
                'id_fuel' => 4,
            ),
            385 => 
            array (
                'id_reg' => 886,
                'id_model' => 770,
                'id_fuel' => 4,
            ),
            386 => 
            array (
                'id_reg' => 887,
                'id_model' => 771,
                'id_fuel' => 2,
            ),
            387 => 
            array (
                'id_reg' => 888,
                'id_model' => 771,
                'id_fuel' => 4,
            ),
            388 => 
            array (
                'id_reg' => 889,
                'id_model' => 772,
                'id_fuel' => 4,
            ),
            389 => 
            array (
                'id_reg' => 890,
                'id_model' => 773,
                'id_fuel' => 4,
            ),
            390 => 
            array (
                'id_reg' => 891,
                'id_model' => 774,
                'id_fuel' => 4,
            ),
            391 => 
            array (
                'id_reg' => 892,
                'id_model' => 775,
                'id_fuel' => 4,
            ),
            392 => 
            array (
                'id_reg' => 893,
                'id_model' => 776,
                'id_fuel' => 2,
            ),
            393 => 
            array (
                'id_reg' => 894,
                'id_model' => 777,
                'id_fuel' => 4,
            ),
            394 => 
            array (
                'id_reg' => 895,
                'id_model' => 778,
                'id_fuel' => 4,
            ),
            395 => 
            array (
                'id_reg' => 896,
                'id_model' => 779,
                'id_fuel' => 2,
            ),
            396 => 
            array (
                'id_reg' => 897,
                'id_model' => 779,
                'id_fuel' => 4,
            ),
            397 => 
            array (
                'id_reg' => 898,
                'id_model' => 780,
                'id_fuel' => 4,
            ),
            398 => 
            array (
                'id_reg' => 899,
                'id_model' => 781,
                'id_fuel' => 4,
            ),
            399 => 
            array (
                'id_reg' => 900,
                'id_model' => 782,
                'id_fuel' => 4,
            ),
            400 => 
            array (
                'id_reg' => 901,
                'id_model' => 783,
                'id_fuel' => 4,
            ),
            401 => 
            array (
                'id_reg' => 902,
                'id_model' => 784,
                'id_fuel' => 4,
            ),
            402 => 
            array (
                'id_reg' => 903,
                'id_model' => 785,
                'id_fuel' => 2,
            ),
            403 => 
            array (
                'id_reg' => 904,
                'id_model' => 785,
                'id_fuel' => 4,
            ),
            404 => 
            array (
                'id_reg' => 905,
                'id_model' => 786,
                'id_fuel' => 4,
            ),
            405 => 
            array (
                'id_reg' => 906,
                'id_model' => 787,
                'id_fuel' => 4,
            ),
            406 => 
            array (
                'id_reg' => 907,
                'id_model' => 788,
                'id_fuel' => 2,
            ),
            407 => 
            array (
                'id_reg' => 908,
                'id_model' => 789,
                'id_fuel' => 4,
            ),
            408 => 
            array (
                'id_reg' => 909,
                'id_model' => 790,
                'id_fuel' => 4,
            ),
            409 => 
            array (
                'id_reg' => 910,
                'id_model' => 791,
                'id_fuel' => 4,
            ),
            410 => 
            array (
                'id_reg' => 911,
                'id_model' => 792,
                'id_fuel' => 4,
            ),
            411 => 
            array (
                'id_reg' => 912,
                'id_model' => 793,
                'id_fuel' => 4,
            ),
            412 => 
            array (
                'id_reg' => 913,
                'id_model' => 794,
                'id_fuel' => 4,
            ),
            413 => 
            array (
                'id_reg' => 914,
                'id_model' => 795,
                'id_fuel' => 2,
            ),
            414 => 
            array (
                'id_reg' => 915,
                'id_model' => 795,
                'id_fuel' => 4,
            ),
            415 => 
            array (
                'id_reg' => 916,
                'id_model' => 796,
                'id_fuel' => 4,
            ),
            416 => 
            array (
                'id_reg' => 917,
                'id_model' => 797,
                'id_fuel' => 4,
            ),
            417 => 
            array (
                'id_reg' => 918,
                'id_model' => 798,
                'id_fuel' => 4,
            ),
            418 => 
            array (
                'id_reg' => 919,
                'id_model' => 799,
                'id_fuel' => 2,
            ),
            419 => 
            array (
                'id_reg' => 920,
                'id_model' => 799,
                'id_fuel' => 4,
            ),
            420 => 
            array (
                'id_reg' => 921,
                'id_model' => 800,
                'id_fuel' => 2,
            ),
            421 => 
            array (
                'id_reg' => 922,
                'id_model' => 800,
                'id_fuel' => 4,
            ),
            422 => 
            array (
                'id_reg' => 923,
                'id_model' => 801,
                'id_fuel' => 2,
            ),
            423 => 
            array (
                'id_reg' => 924,
                'id_model' => 801,
                'id_fuel' => 4,
            ),
            424 => 
            array (
                'id_reg' => 925,
                'id_model' => 802,
                'id_fuel' => 2,
            ),
            425 => 
            array (
                'id_reg' => 926,
                'id_model' => 802,
                'id_fuel' => 4,
            ),
            426 => 
            array (
                'id_reg' => 927,
                'id_model' => 803,
                'id_fuel' => 4,
            ),
            427 => 
            array (
                'id_reg' => 928,
                'id_model' => 804,
                'id_fuel' => 4,
            ),
            428 => 
            array (
                'id_reg' => 929,
                'id_model' => 805,
                'id_fuel' => 4,
            ),
            429 => 
            array (
                'id_reg' => 930,
                'id_model' => 806,
                'id_fuel' => 4,
            ),
            430 => 
            array (
                'id_reg' => 931,
                'id_model' => 807,
                'id_fuel' => 4,
            ),
            431 => 
            array (
                'id_reg' => 932,
                'id_model' => 808,
                'id_fuel' => 4,
            ),
            432 => 
            array (
                'id_reg' => 933,
                'id_model' => 809,
                'id_fuel' => 2,
            ),
            433 => 
            array (
                'id_reg' => 934,
                'id_model' => 810,
                'id_fuel' => 2,
            ),
            434 => 
            array (
                'id_reg' => 935,
                'id_model' => 810,
                'id_fuel' => 4,
            ),
            435 => 
            array (
                'id_reg' => 936,
                'id_model' => 811,
                'id_fuel' => 4,
            ),
            436 => 
            array (
                'id_reg' => 937,
                'id_model' => 812,
                'id_fuel' => 4,
            ),
            437 => 
            array (
                'id_reg' => 938,
                'id_model' => 813,
                'id_fuel' => 6,
            ),
            438 => 
            array (
                'id_reg' => 939,
                'id_model' => 814,
                'id_fuel' => 4,
            ),
            439 => 
            array (
                'id_reg' => 940,
                'id_model' => 815,
                'id_fuel' => 2,
            ),
            440 => 
            array (
                'id_reg' => 941,
                'id_model' => 815,
                'id_fuel' => 4,
            ),
            441 => 
            array (
                'id_reg' => 942,
                'id_model' => 816,
                'id_fuel' => 4,
            ),
            442 => 
            array (
                'id_reg' => 943,
                'id_model' => 817,
                'id_fuel' => 4,
            ),
            443 => 
            array (
                'id_reg' => 944,
                'id_model' => 818,
                'id_fuel' => 4,
            ),
            444 => 
            array (
                'id_reg' => 945,
                'id_model' => 819,
                'id_fuel' => 4,
            ),
            445 => 
            array (
                'id_reg' => 946,
                'id_model' => 820,
                'id_fuel' => 4,
            ),
            446 => 
            array (
                'id_reg' => 947,
                'id_model' => 821,
                'id_fuel' => 4,
            ),
            447 => 
            array (
                'id_reg' => 948,
                'id_model' => 822,
                'id_fuel' => 4,
            ),
            448 => 
            array (
                'id_reg' => 949,
                'id_model' => 823,
                'id_fuel' => 4,
            ),
            449 => 
            array (
                'id_reg' => 950,
                'id_model' => 824,
                'id_fuel' => 2,
            ),
            450 => 
            array (
                'id_reg' => 951,
                'id_model' => 825,
                'id_fuel' => 4,
            ),
            451 => 
            array (
                'id_reg' => 952,
                'id_model' => 826,
                'id_fuel' => 2,
            ),
            452 => 
            array (
                'id_reg' => 953,
                'id_model' => 827,
                'id_fuel' => 2,
            ),
            453 => 
            array (
                'id_reg' => 954,
                'id_model' => 828,
                'id_fuel' => 2,
            ),
            454 => 
            array (
                'id_reg' => 955,
                'id_model' => 829,
                'id_fuel' => 6,
            ),
            455 => 
            array (
                'id_reg' => 956,
                'id_model' => 829,
                'id_fuel' => 4,
            ),
            456 => 
            array (
                'id_reg' => 957,
                'id_model' => 830,
                'id_fuel' => 4,
            ),
            457 => 
            array (
                'id_reg' => 958,
                'id_model' => 831,
                'id_fuel' => 6,
            ),
            458 => 
            array (
                'id_reg' => 959,
                'id_model' => 831,
                'id_fuel' => 4,
            ),
            459 => 
            array (
                'id_reg' => 960,
                'id_model' => 832,
                'id_fuel' => 6,
            ),
            460 => 
            array (
                'id_reg' => 961,
                'id_model' => 833,
                'id_fuel' => 6,
            ),
            461 => 
            array (
                'id_reg' => 962,
                'id_model' => 833,
                'id_fuel' => 4,
            ),
            462 => 
            array (
                'id_reg' => 963,
                'id_model' => 834,
                'id_fuel' => 6,
            ),
            463 => 
            array (
                'id_reg' => 964,
                'id_model' => 834,
                'id_fuel' => 4,
            ),
            464 => 
            array (
                'id_reg' => 965,
                'id_model' => 835,
                'id_fuel' => 6,
            ),
            465 => 
            array (
                'id_reg' => 966,
                'id_model' => 835,
                'id_fuel' => 4,
            ),
            466 => 
            array (
                'id_reg' => 967,
                'id_model' => 836,
                'id_fuel' => 4,
            ),
            467 => 
            array (
                'id_reg' => 968,
                'id_model' => 837,
                'id_fuel' => 4,
            ),
            468 => 
            array (
                'id_reg' => 969,
                'id_model' => 838,
                'id_fuel' => 4,
            ),
            469 => 
            array (
                'id_reg' => 970,
                'id_model' => 839,
                'id_fuel' => 4,
            ),
            470 => 
            array (
                'id_reg' => 971,
                'id_model' => 840,
                'id_fuel' => 4,
            ),
            471 => 
            array (
                'id_reg' => 972,
                'id_model' => 841,
                'id_fuel' => 4,
            ),
            472 => 
            array (
                'id_reg' => 973,
                'id_model' => 842,
                'id_fuel' => 2,
            ),
            473 => 
            array (
                'id_reg' => 974,
                'id_model' => 843,
                'id_fuel' => 2,
            ),
            474 => 
            array (
                'id_reg' => 975,
                'id_model' => 844,
                'id_fuel' => 2,
            ),
            475 => 
            array (
                'id_reg' => 976,
                'id_model' => 845,
                'id_fuel' => 2,
            ),
            476 => 
            array (
                'id_reg' => 977,
                'id_model' => 846,
                'id_fuel' => 2,
            ),
            477 => 
            array (
                'id_reg' => 978,
                'id_model' => 847,
                'id_fuel' => 2,
            ),
            478 => 
            array (
                'id_reg' => 979,
                'id_model' => 848,
                'id_fuel' => 2,
            ),
            479 => 
            array (
                'id_reg' => 980,
                'id_model' => 849,
                'id_fuel' => 2,
            ),
            480 => 
            array (
                'id_reg' => 981,
                'id_model' => 850,
                'id_fuel' => 2,
            ),
            481 => 
            array (
                'id_reg' => 982,
                'id_model' => 851,
                'id_fuel' => 2,
            ),
            482 => 
            array (
                'id_reg' => 983,
                'id_model' => 852,
                'id_fuel' => 2,
            ),
            483 => 
            array (
                'id_reg' => 984,
                'id_model' => 853,
                'id_fuel' => 2,
            ),
            484 => 
            array (
                'id_reg' => 985,
                'id_model' => 854,
                'id_fuel' => 2,
            ),
            485 => 
            array (
                'id_reg' => 986,
                'id_model' => 854,
                'id_fuel' => 4,
            ),
            486 => 
            array (
                'id_reg' => 987,
                'id_model' => 855,
                'id_fuel' => 2,
            ),
            487 => 
            array (
                'id_reg' => 988,
                'id_model' => 856,
                'id_fuel' => 2,
            ),
            488 => 
            array (
                'id_reg' => 989,
                'id_model' => 857,
                'id_fuel' => 4,
            ),
            489 => 
            array (
                'id_reg' => 990,
                'id_model' => 858,
                'id_fuel' => 2,
            ),
            490 => 
            array (
                'id_reg' => 991,
                'id_model' => 859,
                'id_fuel' => 6,
            ),
            491 => 
            array (
                'id_reg' => 992,
                'id_model' => 860,
                'id_fuel' => 6,
            ),
            492 => 
            array (
                'id_reg' => 993,
                'id_model' => 861,
                'id_fuel' => 2,
            ),
            493 => 
            array (
                'id_reg' => 994,
                'id_model' => 862,
                'id_fuel' => 2,
            ),
            494 => 
            array (
                'id_reg' => 995,
                'id_model' => 862,
                'id_fuel' => 6,
            ),
            495 => 
            array (
                'id_reg' => 996,
                'id_model' => 862,
                'id_fuel' => 4,
            ),
            496 => 
            array (
                'id_reg' => 997,
                'id_model' => 862,
                'id_fuel' => 5,
            ),
            497 => 
            array (
                'id_reg' => 998,
                'id_model' => 863,
                'id_fuel' => 2,
            ),
            498 => 
            array (
                'id_reg' => 999,
                'id_model' => 864,
                'id_fuel' => 2,
            ),
            499 => 
            array (
                'id_reg' => 1000,
                'id_model' => 865,
                'id_fuel' => 2,
            ),
        ));
        \DB::table('model_rel_fuel')->insert(array (
            0 => 
            array (
                'id_reg' => 1001,
                'id_model' => 866,
                'id_fuel' => 4,
            ),
            1 => 
            array (
                'id_reg' => 1002,
                'id_model' => 867,
                'id_fuel' => 6,
            ),
            2 => 
            array (
                'id_reg' => 1003,
                'id_model' => 868,
                'id_fuel' => 2,
            ),
            3 => 
            array (
                'id_reg' => 1004,
                'id_model' => 869,
                'id_fuel' => 6,
            ),
            4 => 
            array (
                'id_reg' => 1005,
                'id_model' => 870,
                'id_fuel' => 6,
            ),
            5 => 
            array (
                'id_reg' => 1006,
                'id_model' => 871,
                'id_fuel' => 6,
            ),
            6 => 
            array (
                'id_reg' => 1007,
                'id_model' => 872,
                'id_fuel' => 4,
            ),
            7 => 
            array (
                'id_reg' => 1008,
                'id_model' => 873,
                'id_fuel' => 4,
            ),
            8 => 
            array (
                'id_reg' => 1009,
                'id_model' => 874,
                'id_fuel' => 6,
            ),
            9 => 
            array (
                'id_reg' => 1010,
                'id_model' => 875,
                'id_fuel' => 6,
            ),
            10 => 
            array (
                'id_reg' => 1011,
                'id_model' => 876,
                'id_fuel' => 6,
            ),
            11 => 
            array (
                'id_reg' => 1012,
                'id_model' => 877,
                'id_fuel' => 6,
            ),
            12 => 
            array (
                'id_reg' => 1013,
                'id_model' => 878,
                'id_fuel' => 4,
            ),
            13 => 
            array (
                'id_reg' => 1014,
                'id_model' => 879,
                'id_fuel' => 4,
            ),
            14 => 
            array (
                'id_reg' => 1015,
                'id_model' => 880,
                'id_fuel' => 4,
            ),
            15 => 
            array (
                'id_reg' => 1016,
                'id_model' => 881,
                'id_fuel' => 4,
            ),
            16 => 
            array (
                'id_reg' => 1017,
                'id_model' => 882,
                'id_fuel' => 4,
            ),
            17 => 
            array (
                'id_reg' => 1018,
                'id_model' => 883,
                'id_fuel' => 4,
            ),
            18 => 
            array (
                'id_reg' => 1019,
                'id_model' => 884,
                'id_fuel' => 4,
            ),
            19 => 
            array (
                'id_reg' => 1020,
                'id_model' => 885,
                'id_fuel' => 4,
            ),
            20 => 
            array (
                'id_reg' => 1021,
                'id_model' => 886,
                'id_fuel' => 4,
            ),
            21 => 
            array (
                'id_reg' => 1022,
                'id_model' => 887,
                'id_fuel' => 4,
            ),
            22 => 
            array (
                'id_reg' => 1023,
                'id_model' => 888,
                'id_fuel' => 4,
            ),
            23 => 
            array (
                'id_reg' => 1024,
                'id_model' => 889,
                'id_fuel' => 4,
            ),
            24 => 
            array (
                'id_reg' => 1025,
                'id_model' => 890,
                'id_fuel' => 4,
            ),
            25 => 
            array (
                'id_reg' => 1026,
                'id_model' => 891,
                'id_fuel' => 4,
            ),
            26 => 
            array (
                'id_reg' => 1027,
                'id_model' => 892,
                'id_fuel' => 4,
            ),
            27 => 
            array (
                'id_reg' => 1028,
                'id_model' => 893,
                'id_fuel' => 4,
            ),
            28 => 
            array (
                'id_reg' => 1029,
                'id_model' => 894,
                'id_fuel' => 4,
            ),
            29 => 
            array (
                'id_reg' => 1030,
                'id_model' => 895,
                'id_fuel' => 4,
            ),
            30 => 
            array (
                'id_reg' => 1031,
                'id_model' => 896,
                'id_fuel' => 4,
            ),
            31 => 
            array (
                'id_reg' => 1032,
                'id_model' => 897,
                'id_fuel' => 4,
            ),
            32 => 
            array (
                'id_reg' => 1033,
                'id_model' => 898,
                'id_fuel' => 4,
            ),
            33 => 
            array (
                'id_reg' => 1034,
                'id_model' => 899,
                'id_fuel' => 4,
            ),
            34 => 
            array (
                'id_reg' => 1035,
                'id_model' => 900,
                'id_fuel' => 4,
            ),
            35 => 
            array (
                'id_reg' => 1036,
                'id_model' => 901,
                'id_fuel' => 4,
            ),
            36 => 
            array (
                'id_reg' => 1037,
                'id_model' => 902,
                'id_fuel' => 4,
            ),
            37 => 
            array (
                'id_reg' => 1038,
                'id_model' => 903,
                'id_fuel' => 4,
            ),
            38 => 
            array (
                'id_reg' => 1039,
                'id_model' => 904,
                'id_fuel' => 4,
            ),
            39 => 
            array (
                'id_reg' => 1040,
                'id_model' => 905,
                'id_fuel' => 4,
            ),
            40 => 
            array (
                'id_reg' => 1041,
                'id_model' => 906,
                'id_fuel' => 4,
            ),
            41 => 
            array (
                'id_reg' => 1042,
                'id_model' => 907,
                'id_fuel' => 4,
            ),
            42 => 
            array (
                'id_reg' => 1043,
                'id_model' => 908,
                'id_fuel' => 4,
            ),
            43 => 
            array (
                'id_reg' => 1044,
                'id_model' => 909,
                'id_fuel' => 4,
            ),
            44 => 
            array (
                'id_reg' => 1045,
                'id_model' => 910,
                'id_fuel' => 4,
            ),
            45 => 
            array (
                'id_reg' => 1046,
                'id_model' => 911,
                'id_fuel' => 4,
            ),
            46 => 
            array (
                'id_reg' => 1047,
                'id_model' => 912,
                'id_fuel' => 4,
            ),
            47 => 
            array (
                'id_reg' => 1048,
                'id_model' => 913,
                'id_fuel' => 4,
            ),
            48 => 
            array (
                'id_reg' => 1049,
                'id_model' => 914,
                'id_fuel' => 4,
            ),
            49 => 
            array (
                'id_reg' => 1050,
                'id_model' => 915,
                'id_fuel' => 4,
            ),
            50 => 
            array (
                'id_reg' => 1051,
                'id_model' => 916,
                'id_fuel' => 5,
            ),
            51 => 
            array (
                'id_reg' => 1052,
                'id_model' => 917,
                'id_fuel' => 4,
            ),
            52 => 
            array (
                'id_reg' => 1053,
                'id_model' => 918,
                'id_fuel' => 4,
            ),
            53 => 
            array (
                'id_reg' => 1054,
                'id_model' => 919,
                'id_fuel' => 4,
            ),
            54 => 
            array (
                'id_reg' => 1055,
                'id_model' => 920,
                'id_fuel' => 4,
            ),
            55 => 
            array (
                'id_reg' => 1056,
                'id_model' => 921,
                'id_fuel' => 2,
            ),
            56 => 
            array (
                'id_reg' => 1057,
                'id_model' => 922,
                'id_fuel' => 2,
            ),
            57 => 
            array (
                'id_reg' => 1058,
                'id_model' => 923,
                'id_fuel' => 4,
            ),
            58 => 
            array (
                'id_reg' => 1059,
                'id_model' => 924,
                'id_fuel' => 4,
            ),
            59 => 
            array (
                'id_reg' => 1060,
                'id_model' => 925,
                'id_fuel' => 4,
            ),
            60 => 
            array (
                'id_reg' => 1061,
                'id_model' => 926,
                'id_fuel' => 4,
            ),
            61 => 
            array (
                'id_reg' => 1062,
                'id_model' => 927,
                'id_fuel' => 4,
            ),
            62 => 
            array (
                'id_reg' => 1063,
                'id_model' => 928,
                'id_fuel' => 4,
            ),
            63 => 
            array (
                'id_reg' => 1064,
                'id_model' => 929,
                'id_fuel' => 4,
            ),
            64 => 
            array (
                'id_reg' => 1065,
                'id_model' => 930,
                'id_fuel' => 2,
            ),
            65 => 
            array (
                'id_reg' => 1066,
                'id_model' => 930,
                'id_fuel' => 4,
            ),
            66 => 
            array (
                'id_reg' => 1067,
                'id_model' => 931,
                'id_fuel' => 2,
            ),
            67 => 
            array (
                'id_reg' => 1068,
                'id_model' => 931,
                'id_fuel' => 4,
            ),
            68 => 
            array (
                'id_reg' => 1069,
                'id_model' => 932,
                'id_fuel' => 4,
            ),
            69 => 
            array (
                'id_reg' => 1070,
                'id_model' => 933,
                'id_fuel' => 2,
            ),
            70 => 
            array (
                'id_reg' => 1071,
                'id_model' => 934,
                'id_fuel' => 2,
            ),
            71 => 
            array (
                'id_reg' => 1072,
                'id_model' => 934,
                'id_fuel' => 4,
            ),
            72 => 
            array (
                'id_reg' => 1073,
                'id_model' => 935,
                'id_fuel' => 2,
            ),
            73 => 
            array (
                'id_reg' => 1074,
                'id_model' => 935,
                'id_fuel' => 3,
            ),
            74 => 
            array (
                'id_reg' => 1075,
                'id_model' => 935,
                'id_fuel' => 4,
            ),
            75 => 
            array (
                'id_reg' => 1076,
                'id_model' => 936,
                'id_fuel' => 2,
            ),
            76 => 
            array (
                'id_reg' => 1077,
                'id_model' => 937,
                'id_fuel' => 2,
            ),
            77 => 
            array (
                'id_reg' => 1078,
                'id_model' => 937,
                'id_fuel' => 4,
            ),
            78 => 
            array (
                'id_reg' => 1079,
                'id_model' => 938,
                'id_fuel' => 4,
            ),
            79 => 
            array (
                'id_reg' => 1080,
                'id_model' => 939,
                'id_fuel' => 4,
            ),
            80 => 
            array (
                'id_reg' => 1081,
                'id_model' => 940,
                'id_fuel' => 4,
            ),
            81 => 
            array (
                'id_reg' => 1082,
                'id_model' => 941,
                'id_fuel' => 4,
            ),
            82 => 
            array (
                'id_reg' => 1083,
                'id_model' => 942,
                'id_fuel' => 4,
            ),
            83 => 
            array (
                'id_reg' => 1084,
                'id_model' => 943,
                'id_fuel' => 4,
            ),
            84 => 
            array (
                'id_reg' => 1085,
                'id_model' => 944,
                'id_fuel' => 2,
            ),
            85 => 
            array (
                'id_reg' => 1086,
                'id_model' => 945,
                'id_fuel' => 4,
            ),
            86 => 
            array (
                'id_reg' => 1087,
                'id_model' => 946,
                'id_fuel' => 2,
            ),
            87 => 
            array (
                'id_reg' => 1088,
                'id_model' => 947,
                'id_fuel' => 4,
            ),
            88 => 
            array (
                'id_reg' => 1089,
                'id_model' => 948,
                'id_fuel' => 4,
            ),
            89 => 
            array (
                'id_reg' => 1090,
                'id_model' => 949,
                'id_fuel' => 4,
            ),
            90 => 
            array (
                'id_reg' => 1091,
                'id_model' => 950,
                'id_fuel' => 4,
            ),
            91 => 
            array (
                'id_reg' => 1092,
                'id_model' => 951,
                'id_fuel' => 4,
            ),
            92 => 
            array (
                'id_reg' => 1093,
                'id_model' => 952,
                'id_fuel' => 4,
            ),
            93 => 
            array (
                'id_reg' => 1094,
                'id_model' => 953,
                'id_fuel' => 4,
            ),
            94 => 
            array (
                'id_reg' => 1095,
                'id_model' => 954,
                'id_fuel' => 4,
            ),
            95 => 
            array (
                'id_reg' => 1096,
                'id_model' => 955,
                'id_fuel' => 4,
            ),
            96 => 
            array (
                'id_reg' => 1097,
                'id_model' => 956,
                'id_fuel' => 4,
            ),
            97 => 
            array (
                'id_reg' => 1098,
                'id_model' => 957,
                'id_fuel' => 2,
            ),
            98 => 
            array (
                'id_reg' => 1099,
                'id_model' => 957,
                'id_fuel' => 4,
            ),
            99 => 
            array (
                'id_reg' => 1100,
                'id_model' => 958,
                'id_fuel' => 4,
            ),
            100 => 
            array (
                'id_reg' => 1101,
                'id_model' => 959,
                'id_fuel' => 2,
            ),
            101 => 
            array (
                'id_reg' => 1102,
                'id_model' => 959,
                'id_fuel' => 4,
            ),
            102 => 
            array (
                'id_reg' => 1103,
                'id_model' => 960,
                'id_fuel' => 4,
            ),
            103 => 
            array (
                'id_reg' => 1104,
                'id_model' => 961,
                'id_fuel' => 4,
            ),
            104 => 
            array (
                'id_reg' => 1105,
                'id_model' => 962,
                'id_fuel' => 2,
            ),
            105 => 
            array (
                'id_reg' => 1106,
                'id_model' => 962,
                'id_fuel' => 4,
            ),
            106 => 
            array (
                'id_reg' => 1107,
                'id_model' => 963,
                'id_fuel' => 2,
            ),
            107 => 
            array (
                'id_reg' => 1108,
                'id_model' => 964,
                'id_fuel' => 4,
            ),
            108 => 
            array (
                'id_reg' => 1109,
                'id_model' => 965,
                'id_fuel' => 2,
            ),
            109 => 
            array (
                'id_reg' => 1110,
                'id_model' => 965,
                'id_fuel' => 3,
            ),
            110 => 
            array (
                'id_reg' => 1111,
                'id_model' => 965,
                'id_fuel' => 4,
            ),
            111 => 
            array (
                'id_reg' => 1112,
                'id_model' => 966,
                'id_fuel' => 2,
            ),
            112 => 
            array (
                'id_reg' => 1113,
                'id_model' => 966,
                'id_fuel' => 4,
            ),
            113 => 
            array (
                'id_reg' => 1114,
                'id_model' => 967,
                'id_fuel' => 2,
            ),
            114 => 
            array (
                'id_reg' => 1115,
                'id_model' => 968,
                'id_fuel' => 2,
            ),
            115 => 
            array (
                'id_reg' => 1116,
                'id_model' => 969,
                'id_fuel' => 2,
            ),
            116 => 
            array (
                'id_reg' => 1117,
                'id_model' => 969,
                'id_fuel' => 3,
            ),
            117 => 
            array (
                'id_reg' => 1118,
                'id_model' => 969,
                'id_fuel' => 4,
            ),
            118 => 
            array (
                'id_reg' => 1119,
                'id_model' => 970,
                'id_fuel' => 2,
            ),
            119 => 
            array (
                'id_reg' => 1120,
                'id_model' => 971,
                'id_fuel' => 2,
            ),
            120 => 
            array (
                'id_reg' => 1121,
                'id_model' => 972,
                'id_fuel' => 2,
            ),
            121 => 
            array (
                'id_reg' => 1122,
                'id_model' => 972,
                'id_fuel' => 4,
            ),
            122 => 
            array (
                'id_reg' => 1123,
                'id_model' => 973,
                'id_fuel' => 2,
            ),
            123 => 
            array (
                'id_reg' => 1124,
                'id_model' => 973,
                'id_fuel' => 4,
            ),
            124 => 
            array (
                'id_reg' => 1125,
                'id_model' => 974,
                'id_fuel' => 2,
            ),
            125 => 
            array (
                'id_reg' => 1126,
                'id_model' => 974,
                'id_fuel' => 4,
            ),
            126 => 
            array (
                'id_reg' => 1127,
                'id_model' => 975,
                'id_fuel' => 2,
            ),
            127 => 
            array (
                'id_reg' => 1128,
                'id_model' => 975,
                'id_fuel' => 4,
            ),
            128 => 
            array (
                'id_reg' => 1129,
                'id_model' => 976,
                'id_fuel' => 2,
            ),
            129 => 
            array (
                'id_reg' => 1130,
                'id_model' => 976,
                'id_fuel' => 4,
            ),
            130 => 
            array (
                'id_reg' => 1131,
                'id_model' => 977,
                'id_fuel' => 2,
            ),
            131 => 
            array (
                'id_reg' => 1132,
                'id_model' => 977,
                'id_fuel' => 3,
            ),
            132 => 
            array (
                'id_reg' => 1133,
                'id_model' => 977,
                'id_fuel' => 4,
            ),
            133 => 
            array (
                'id_reg' => 1134,
                'id_model' => 978,
                'id_fuel' => 2,
            ),
            134 => 
            array (
                'id_reg' => 1135,
                'id_model' => 978,
                'id_fuel' => 4,
            ),
            135 => 
            array (
                'id_reg' => 1136,
                'id_model' => 979,
                'id_fuel' => 2,
            ),
            136 => 
            array (
                'id_reg' => 1137,
                'id_model' => 979,
                'id_fuel' => 4,
            ),
            137 => 
            array (
                'id_reg' => 1138,
                'id_model' => 980,
                'id_fuel' => 2,
            ),
            138 => 
            array (
                'id_reg' => 1139,
                'id_model' => 980,
                'id_fuel' => 4,
            ),
            139 => 
            array (
                'id_reg' => 1140,
                'id_model' => 981,
                'id_fuel' => 2,
            ),
            140 => 
            array (
                'id_reg' => 1141,
                'id_model' => 981,
                'id_fuel' => 4,
            ),
            141 => 
            array (
                'id_reg' => 1142,
                'id_model' => 982,
                'id_fuel' => 2,
            ),
            142 => 
            array (
                'id_reg' => 1143,
                'id_model' => 983,
                'id_fuel' => 4,
            ),
            143 => 
            array (
                'id_reg' => 1144,
                'id_model' => 984,
                'id_fuel' => 2,
            ),
            144 => 
            array (
                'id_reg' => 1145,
                'id_model' => 984,
                'id_fuel' => 4,
            ),
            145 => 
            array (
                'id_reg' => 1146,
                'id_model' => 985,
                'id_fuel' => 2,
            ),
            146 => 
            array (
                'id_reg' => 1147,
                'id_model' => 986,
                'id_fuel' => 2,
            ),
            147 => 
            array (
                'id_reg' => 1148,
                'id_model' => 986,
                'id_fuel' => 4,
            ),
            148 => 
            array (
                'id_reg' => 1149,
                'id_model' => 987,
                'id_fuel' => 2,
            ),
            149 => 
            array (
                'id_reg' => 1150,
                'id_model' => 987,
                'id_fuel' => 3,
            ),
            150 => 
            array (
                'id_reg' => 1151,
                'id_model' => 987,
                'id_fuel' => 4,
            ),
            151 => 
            array (
                'id_reg' => 1152,
                'id_model' => 988,
                'id_fuel' => 4,
            ),
            152 => 
            array (
                'id_reg' => 1153,
                'id_model' => 989,
                'id_fuel' => 2,
            ),
            153 => 
            array (
                'id_reg' => 1154,
                'id_model' => 989,
                'id_fuel' => 4,
            ),
            154 => 
            array (
                'id_reg' => 1155,
                'id_model' => 990,
                'id_fuel' => 2,
            ),
            155 => 
            array (
                'id_reg' => 1156,
                'id_model' => 990,
                'id_fuel' => 4,
            ),
            156 => 
            array (
                'id_reg' => 1157,
                'id_model' => 992,
                'id_fuel' => 2,
            ),
            157 => 
            array (
                'id_reg' => 1158,
                'id_model' => 993,
                'id_fuel' => 2,
            ),
            158 => 
            array (
                'id_reg' => 1159,
                'id_model' => 994,
                'id_fuel' => 2,
            ),
            159 => 
            array (
                'id_reg' => 1160,
                'id_model' => 995,
                'id_fuel' => 2,
            ),
            160 => 
            array (
                'id_reg' => 1161,
                'id_model' => 996,
                'id_fuel' => 2,
            ),
            161 => 
            array (
                'id_reg' => 1162,
                'id_model' => 997,
                'id_fuel' => 2,
            ),
            162 => 
            array (
                'id_reg' => 1163,
                'id_model' => 998,
                'id_fuel' => 2,
            ),
            163 => 
            array (
                'id_reg' => 1164,
                'id_model' => 999,
                'id_fuel' => 2,
            ),
            164 => 
            array (
                'id_reg' => 1165,
                'id_model' => 1000,
                'id_fuel' => 2,
            ),
            165 => 
            array (
                'id_reg' => 1166,
                'id_model' => 1001,
                'id_fuel' => 2,
            ),
            166 => 
            array (
                'id_reg' => 1167,
                'id_model' => 1002,
                'id_fuel' => 2,
            ),
            167 => 
            array (
                'id_reg' => 1168,
                'id_model' => 1003,
                'id_fuel' => 2,
            ),
            168 => 
            array (
                'id_reg' => 1169,
                'id_model' => 1004,
                'id_fuel' => 2,
            ),
            169 => 
            array (
                'id_reg' => 1170,
                'id_model' => 1005,
                'id_fuel' => 2,
            ),
            170 => 
            array (
                'id_reg' => 1171,
                'id_model' => 1006,
                'id_fuel' => 4,
            ),
            171 => 
            array (
                'id_reg' => 1172,
                'id_model' => 1007,
                'id_fuel' => 4,
            ),
            172 => 
            array (
                'id_reg' => 1173,
                'id_model' => 1008,
                'id_fuel' => 4,
            ),
            173 => 
            array (
                'id_reg' => 1174,
                'id_model' => 1009,
                'id_fuel' => 4,
            ),
            174 => 
            array (
                'id_reg' => 1175,
                'id_model' => 1010,
                'id_fuel' => 4,
            ),
            175 => 
            array (
                'id_reg' => 1176,
                'id_model' => 1011,
                'id_fuel' => 4,
            ),
            176 => 
            array (
                'id_reg' => 1177,
                'id_model' => 1012,
                'id_fuel' => 4,
            ),
            177 => 
            array (
                'id_reg' => 1178,
                'id_model' => 1013,
                'id_fuel' => 2,
            ),
            178 => 
            array (
                'id_reg' => 1179,
                'id_model' => 1013,
                'id_fuel' => 3,
            ),
            179 => 
            array (
                'id_reg' => 1180,
                'id_model' => 1013,
                'id_fuel' => 4,
            ),
            180 => 
            array (
                'id_reg' => 1181,
                'id_model' => 1014,
                'id_fuel' => 2,
            ),
            181 => 
            array (
                'id_reg' => 1182,
                'id_model' => 1015,
                'id_fuel' => 4,
            ),
            182 => 
            array (
                'id_reg' => 1183,
                'id_model' => 1016,
                'id_fuel' => 4,
            ),
            183 => 
            array (
                'id_reg' => 1184,
                'id_model' => 1017,
                'id_fuel' => 1,
            ),
            184 => 
            array (
                'id_reg' => 1185,
                'id_model' => 1017,
                'id_fuel' => 2,
            ),
            185 => 
            array (
                'id_reg' => 1186,
                'id_model' => 1017,
                'id_fuel' => 3,
            ),
            186 => 
            array (
                'id_reg' => 1187,
                'id_model' => 1017,
                'id_fuel' => 4,
            ),
            187 => 
            array (
                'id_reg' => 1188,
                'id_model' => 1018,
                'id_fuel' => 4,
            ),
            188 => 
            array (
                'id_reg' => 1189,
                'id_model' => 1019,
                'id_fuel' => 4,
            ),
            189 => 
            array (
                'id_reg' => 1190,
                'id_model' => 1020,
                'id_fuel' => 2,
            ),
            190 => 
            array (
                'id_reg' => 1191,
                'id_model' => 1020,
                'id_fuel' => 4,
            ),
            191 => 
            array (
                'id_reg' => 1192,
                'id_model' => 1021,
                'id_fuel' => 4,
            ),
            192 => 
            array (
                'id_reg' => 1193,
                'id_model' => 1022,
                'id_fuel' => 4,
            ),
            193 => 
            array (
                'id_reg' => 1194,
                'id_model' => 1023,
                'id_fuel' => 4,
            ),
            194 => 
            array (
                'id_reg' => 1195,
                'id_model' => 1024,
                'id_fuel' => 4,
            ),
            195 => 
            array (
                'id_reg' => 1196,
                'id_model' => 1025,
                'id_fuel' => 4,
            ),
            196 => 
            array (
                'id_reg' => 1197,
                'id_model' => 1026,
                'id_fuel' => 4,
            ),
            197 => 
            array (
                'id_reg' => 1198,
                'id_model' => 1027,
                'id_fuel' => 4,
            ),
            198 => 
            array (
                'id_reg' => 1199,
                'id_model' => 1028,
                'id_fuel' => 2,
            ),
            199 => 
            array (
                'id_reg' => 1200,
                'id_model' => 1028,
                'id_fuel' => 4,
            ),
            200 => 
            array (
                'id_reg' => 1201,
                'id_model' => 1029,
                'id_fuel' => 4,
            ),
            201 => 
            array (
                'id_reg' => 1202,
                'id_model' => 1030,
                'id_fuel' => 4,
            ),
            202 => 
            array (
                'id_reg' => 1203,
                'id_model' => 1031,
                'id_fuel' => 2,
            ),
            203 => 
            array (
                'id_reg' => 1204,
                'id_model' => 1032,
                'id_fuel' => 2,
            ),
            204 => 
            array (
                'id_reg' => 1205,
                'id_model' => 1033,
                'id_fuel' => 4,
            ),
            205 => 
            array (
                'id_reg' => 1206,
                'id_model' => 1034,
                'id_fuel' => 4,
            ),
            206 => 
            array (
                'id_reg' => 1207,
                'id_model' => 1035,
                'id_fuel' => 4,
            ),
            207 => 
            array (
                'id_reg' => 1208,
                'id_model' => 1036,
                'id_fuel' => 4,
            ),
            208 => 
            array (
                'id_reg' => 1209,
                'id_model' => 1037,
                'id_fuel' => 2,
            ),
            209 => 
            array (
                'id_reg' => 1210,
                'id_model' => 1038,
                'id_fuel' => 2,
            ),
            210 => 
            array (
                'id_reg' => 1211,
                'id_model' => 1039,
                'id_fuel' => 2,
            ),
            211 => 
            array (
                'id_reg' => 1212,
                'id_model' => 1039,
                'id_fuel' => 4,
            ),
            212 => 
            array (
                'id_reg' => 1213,
                'id_model' => 1040,
                'id_fuel' => 2,
            ),
            213 => 
            array (
                'id_reg' => 1214,
                'id_model' => 1040,
                'id_fuel' => 4,
            ),
            214 => 
            array (
                'id_reg' => 1215,
                'id_model' => 1041,
                'id_fuel' => 4,
            ),
            215 => 
            array (
                'id_reg' => 1216,
                'id_model' => 1041,
                'id_fuel' => 5,
            ),
            216 => 
            array (
                'id_reg' => 1217,
                'id_model' => 1042,
                'id_fuel' => 2,
            ),
            217 => 
            array (
                'id_reg' => 1218,
                'id_model' => 1042,
                'id_fuel' => 4,
            ),
            218 => 
            array (
                'id_reg' => 1219,
                'id_model' => 1043,
                'id_fuel' => 2,
            ),
            219 => 
            array (
                'id_reg' => 1220,
                'id_model' => 1043,
                'id_fuel' => 4,
            ),
            220 => 
            array (
                'id_reg' => 1221,
                'id_model' => 1044,
                'id_fuel' => 4,
            ),
            221 => 
            array (
                'id_reg' => 1222,
                'id_model' => 1045,
                'id_fuel' => 4,
            ),
            222 => 
            array (
                'id_reg' => 1223,
                'id_model' => 1046,
                'id_fuel' => 2,
            ),
            223 => 
            array (
                'id_reg' => 1224,
                'id_model' => 1046,
                'id_fuel' => 4,
            ),
            224 => 
            array (
                'id_reg' => 1225,
                'id_model' => 1047,
                'id_fuel' => 4,
            ),
            225 => 
            array (
                'id_reg' => 1226,
                'id_model' => 1048,
                'id_fuel' => 2,
            ),
            226 => 
            array (
                'id_reg' => 1227,
                'id_model' => 1048,
                'id_fuel' => 4,
            ),
            227 => 
            array (
                'id_reg' => 1228,
                'id_model' => 1049,
                'id_fuel' => 2,
            ),
            228 => 
            array (
                'id_reg' => 1229,
                'id_model' => 1049,
                'id_fuel' => 4,
            ),
            229 => 
            array (
                'id_reg' => 1230,
                'id_model' => 1050,
                'id_fuel' => 4,
            ),
            230 => 
            array (
                'id_reg' => 1231,
                'id_model' => 1051,
                'id_fuel' => 4,
            ),
            231 => 
            array (
                'id_reg' => 1232,
                'id_model' => 1052,
                'id_fuel' => 4,
            ),
            232 => 
            array (
                'id_reg' => 1233,
                'id_model' => 1053,
                'id_fuel' => 4,
            ),
            233 => 
            array (
                'id_reg' => 1234,
                'id_model' => 1054,
                'id_fuel' => 2,
            ),
            234 => 
            array (
                'id_reg' => 1235,
                'id_model' => 1054,
                'id_fuel' => 3,
            ),
            235 => 
            array (
                'id_reg' => 1236,
                'id_model' => 1054,
                'id_fuel' => 4,
            ),
            236 => 
            array (
                'id_reg' => 1237,
                'id_model' => 1055,
                'id_fuel' => 4,
            ),
            237 => 
            array (
                'id_reg' => 1238,
                'id_model' => 1056,
                'id_fuel' => 4,
            ),
            238 => 
            array (
                'id_reg' => 1239,
                'id_model' => 1057,
                'id_fuel' => 1,
            ),
            239 => 
            array (
                'id_reg' => 1240,
                'id_model' => 1057,
                'id_fuel' => 2,
            ),
            240 => 
            array (
                'id_reg' => 1241,
                'id_model' => 1057,
                'id_fuel' => 6,
            ),
            241 => 
            array (
                'id_reg' => 1242,
                'id_model' => 1057,
                'id_fuel' => 3,
            ),
            242 => 
            array (
                'id_reg' => 1243,
                'id_model' => 1057,
                'id_fuel' => 4,
            ),
            243 => 
            array (
                'id_reg' => 1244,
                'id_model' => 1058,
                'id_fuel' => 1,
            ),
            244 => 
            array (
                'id_reg' => 1245,
                'id_model' => 1058,
                'id_fuel' => 2,
            ),
            245 => 
            array (
                'id_reg' => 1246,
                'id_model' => 1058,
                'id_fuel' => 4,
            ),
            246 => 
            array (
                'id_reg' => 1247,
                'id_model' => 1059,
                'id_fuel' => 2,
            ),
            247 => 
            array (
                'id_reg' => 1248,
                'id_model' => 1059,
                'id_fuel' => 4,
            ),
            248 => 
            array (
                'id_reg' => 1249,
                'id_model' => 1060,
                'id_fuel' => 2,
            ),
            249 => 
            array (
                'id_reg' => 1250,
                'id_model' => 1061,
                'id_fuel' => 2,
            ),
            250 => 
            array (
                'id_reg' => 1251,
                'id_model' => 1061,
                'id_fuel' => 4,
            ),
            251 => 
            array (
                'id_reg' => 1252,
                'id_model' => 1062,
                'id_fuel' => 4,
            ),
            252 => 
            array (
                'id_reg' => 1253,
                'id_model' => 1063,
                'id_fuel' => 1,
            ),
            253 => 
            array (
                'id_reg' => 1254,
                'id_model' => 1063,
                'id_fuel' => 2,
            ),
            254 => 
            array (
                'id_reg' => 1255,
                'id_model' => 1063,
                'id_fuel' => 4,
            ),
            255 => 
            array (
                'id_reg' => 1256,
                'id_model' => 1064,
                'id_fuel' => 4,
            ),
            256 => 
            array (
                'id_reg' => 1257,
                'id_model' => 1065,
                'id_fuel' => 4,
            ),
            257 => 
            array (
                'id_reg' => 1258,
                'id_model' => 1066,
                'id_fuel' => 2,
            ),
            258 => 
            array (
                'id_reg' => 1259,
                'id_model' => 1066,
                'id_fuel' => 4,
            ),
            259 => 
            array (
                'id_reg' => 1260,
                'id_model' => 1067,
                'id_fuel' => 2,
            ),
            260 => 
            array (
                'id_reg' => 1261,
                'id_model' => 1067,
                'id_fuel' => 4,
            ),
            261 => 
            array (
                'id_reg' => 1262,
                'id_model' => 1068,
                'id_fuel' => 4,
            ),
            262 => 
            array (
                'id_reg' => 1263,
                'id_model' => 1069,
                'id_fuel' => 4,
            ),
            263 => 
            array (
                'id_reg' => 1264,
                'id_model' => 1070,
                'id_fuel' => 4,
            ),
            264 => 
            array (
                'id_reg' => 1265,
                'id_model' => 1071,
                'id_fuel' => 2,
            ),
            265 => 
            array (
                'id_reg' => 1266,
                'id_model' => 1071,
                'id_fuel' => 4,
            ),
            266 => 
            array (
                'id_reg' => 1267,
                'id_model' => 1072,
                'id_fuel' => 4,
            ),
            267 => 
            array (
                'id_reg' => 1268,
                'id_model' => 1073,
                'id_fuel' => 2,
            ),
            268 => 
            array (
                'id_reg' => 1269,
                'id_model' => 1073,
                'id_fuel' => 4,
            ),
            269 => 
            array (
                'id_reg' => 1270,
                'id_model' => 1074,
                'id_fuel' => 4,
            ),
            270 => 
            array (
                'id_reg' => 1271,
                'id_model' => 1075,
                'id_fuel' => 4,
            ),
            271 => 
            array (
                'id_reg' => 1272,
                'id_model' => 1076,
                'id_fuel' => 4,
            ),
            272 => 
            array (
                'id_reg' => 1273,
                'id_model' => 1077,
                'id_fuel' => 4,
            ),
            273 => 
            array (
                'id_reg' => 1274,
                'id_model' => 1078,
                'id_fuel' => 4,
            ),
            274 => 
            array (
                'id_reg' => 1275,
                'id_model' => 1079,
                'id_fuel' => 4,
            ),
            275 => 
            array (
                'id_reg' => 1276,
                'id_model' => 1080,
                'id_fuel' => 4,
            ),
            276 => 
            array (
                'id_reg' => 1277,
                'id_model' => 1081,
                'id_fuel' => 2,
            ),
            277 => 
            array (
                'id_reg' => 1278,
                'id_model' => 1081,
                'id_fuel' => 4,
            ),
            278 => 
            array (
                'id_reg' => 1279,
                'id_model' => 1082,
                'id_fuel' => 4,
            ),
            279 => 
            array (
                'id_reg' => 1280,
                'id_model' => 1083,
                'id_fuel' => 1,
            ),
            280 => 
            array (
                'id_reg' => 1281,
                'id_model' => 1083,
                'id_fuel' => 2,
            ),
            281 => 
            array (
                'id_reg' => 1282,
                'id_model' => 1083,
                'id_fuel' => 4,
            ),
            282 => 
            array (
                'id_reg' => 1283,
                'id_model' => 1083,
                'id_fuel' => 5,
            ),
            283 => 
            array (
                'id_reg' => 1284,
                'id_model' => 1084,
                'id_fuel' => 4,
            ),
            284 => 
            array (
                'id_reg' => 1285,
                'id_model' => 1085,
                'id_fuel' => 4,
            ),
            285 => 
            array (
                'id_reg' => 1286,
                'id_model' => 1086,
                'id_fuel' => 2,
            ),
            286 => 
            array (
                'id_reg' => 1287,
                'id_model' => 1086,
                'id_fuel' => 4,
            ),
            287 => 
            array (
                'id_reg' => 1288,
                'id_model' => 1087,
                'id_fuel' => 4,
            ),
            288 => 
            array (
                'id_reg' => 1289,
                'id_model' => 1088,
                'id_fuel' => 2,
            ),
            289 => 
            array (
                'id_reg' => 1290,
                'id_model' => 1088,
                'id_fuel' => 4,
            ),
            290 => 
            array (
                'id_reg' => 1291,
                'id_model' => 1089,
                'id_fuel' => 4,
            ),
            291 => 
            array (
                'id_reg' => 1292,
                'id_model' => 1090,
                'id_fuel' => 4,
            ),
            292 => 
            array (
                'id_reg' => 1293,
                'id_model' => 1091,
                'id_fuel' => 4,
            ),
            293 => 
            array (
                'id_reg' => 1294,
                'id_model' => 1092,
                'id_fuel' => 4,
            ),
            294 => 
            array (
                'id_reg' => 1295,
                'id_model' => 1093,
                'id_fuel' => 2,
            ),
            295 => 
            array (
                'id_reg' => 1296,
                'id_model' => 1093,
                'id_fuel' => 4,
            ),
            296 => 
            array (
                'id_reg' => 1297,
                'id_model' => 1094,
                'id_fuel' => 4,
            ),
            297 => 
            array (
                'id_reg' => 1298,
                'id_model' => 1095,
                'id_fuel' => 1,
            ),
            298 => 
            array (
                'id_reg' => 1299,
                'id_model' => 1095,
                'id_fuel' => 2,
            ),
            299 => 
            array (
                'id_reg' => 1300,
                'id_model' => 1095,
                'id_fuel' => 4,
            ),
            300 => 
            array (
                'id_reg' => 1301,
                'id_model' => 1096,
                'id_fuel' => 2,
            ),
            301 => 
            array (
                'id_reg' => 1302,
                'id_model' => 1096,
                'id_fuel' => 4,
            ),
            302 => 
            array (
                'id_reg' => 1303,
                'id_model' => 1097,
                'id_fuel' => 2,
            ),
            303 => 
            array (
                'id_reg' => 1304,
                'id_model' => 1097,
                'id_fuel' => 4,
            ),
            304 => 
            array (
                'id_reg' => 1305,
                'id_model' => 1098,
                'id_fuel' => 4,
            ),
            305 => 
            array (
                'id_reg' => 1306,
                'id_model' => 1099,
                'id_fuel' => 4,
            ),
            306 => 
            array (
                'id_reg' => 1307,
                'id_model' => 1100,
                'id_fuel' => 4,
            ),
            307 => 
            array (
                'id_reg' => 1308,
                'id_model' => 1101,
                'id_fuel' => 4,
            ),
            308 => 
            array (
                'id_reg' => 1309,
                'id_model' => 1102,
                'id_fuel' => 4,
            ),
            309 => 
            array (
                'id_reg' => 1310,
                'id_model' => 1103,
                'id_fuel' => 4,
            ),
            310 => 
            array (
                'id_reg' => 1311,
                'id_model' => 1104,
                'id_fuel' => 4,
            ),
            311 => 
            array (
                'id_reg' => 1312,
                'id_model' => 1105,
                'id_fuel' => 2,
            ),
            312 => 
            array (
                'id_reg' => 1313,
                'id_model' => 1105,
                'id_fuel' => 4,
            ),
            313 => 
            array (
                'id_reg' => 1314,
                'id_model' => 1106,
                'id_fuel' => 2,
            ),
            314 => 
            array (
                'id_reg' => 1315,
                'id_model' => 1106,
                'id_fuel' => 4,
            ),
            315 => 
            array (
                'id_reg' => 1316,
                'id_model' => 1107,
                'id_fuel' => 2,
            ),
            316 => 
            array (
                'id_reg' => 1317,
                'id_model' => 1107,
                'id_fuel' => 4,
            ),
            317 => 
            array (
                'id_reg' => 1318,
                'id_model' => 1108,
                'id_fuel' => 2,
            ),
            318 => 
            array (
                'id_reg' => 1319,
                'id_model' => 1109,
                'id_fuel' => 2,
            ),
            319 => 
            array (
                'id_reg' => 1320,
                'id_model' => 1109,
                'id_fuel' => 4,
            ),
            320 => 
            array (
                'id_reg' => 1321,
                'id_model' => 1110,
                'id_fuel' => 4,
            ),
            321 => 
            array (
                'id_reg' => 1322,
                'id_model' => 1111,
                'id_fuel' => 4,
            ),
            322 => 
            array (
                'id_reg' => 1323,
                'id_model' => 1112,
                'id_fuel' => 4,
            ),
            323 => 
            array (
                'id_reg' => 1324,
                'id_model' => 1113,
                'id_fuel' => 4,
            ),
            324 => 
            array (
                'id_reg' => 1325,
                'id_model' => 1114,
                'id_fuel' => 4,
            ),
            325 => 
            array (
                'id_reg' => 1326,
                'id_model' => 1115,
                'id_fuel' => 4,
            ),
            326 => 
            array (
                'id_reg' => 1327,
                'id_model' => 1116,
                'id_fuel' => 4,
            ),
            327 => 
            array (
                'id_reg' => 1328,
                'id_model' => 1117,
                'id_fuel' => 4,
            ),
            328 => 
            array (
                'id_reg' => 1329,
                'id_model' => 1118,
                'id_fuel' => 4,
            ),
            329 => 
            array (
                'id_reg' => 1330,
                'id_model' => 1119,
                'id_fuel' => 4,
            ),
            330 => 
            array (
                'id_reg' => 1331,
                'id_model' => 1120,
                'id_fuel' => 2,
            ),
            331 => 
            array (
                'id_reg' => 1332,
                'id_model' => 1120,
                'id_fuel' => 4,
            ),
            332 => 
            array (
                'id_reg' => 1333,
                'id_model' => 1121,
                'id_fuel' => 2,
            ),
            333 => 
            array (
                'id_reg' => 1334,
                'id_model' => 1121,
                'id_fuel' => 4,
            ),
            334 => 
            array (
                'id_reg' => 1335,
                'id_model' => 1122,
                'id_fuel' => 2,
            ),
            335 => 
            array (
                'id_reg' => 1336,
                'id_model' => 1122,
                'id_fuel' => 4,
            ),
            336 => 
            array (
                'id_reg' => 1337,
                'id_model' => 1123,
                'id_fuel' => 4,
            ),
            337 => 
            array (
                'id_reg' => 1338,
                'id_model' => 1124,
                'id_fuel' => 2,
            ),
            338 => 
            array (
                'id_reg' => 1339,
                'id_model' => 1125,
                'id_fuel' => 6,
            ),
            339 => 
            array (
                'id_reg' => 1340,
                'id_model' => 1126,
                'id_fuel' => 6,
            ),
            340 => 
            array (
                'id_reg' => 1341,
                'id_model' => 1127,
                'id_fuel' => 6,
            ),
            341 => 
            array (
                'id_reg' => 1342,
                'id_model' => 1128,
                'id_fuel' => 2,
            ),
            342 => 
            array (
                'id_reg' => 1343,
                'id_model' => 1129,
                'id_fuel' => 2,
            ),
            343 => 
            array (
                'id_reg' => 1344,
                'id_model' => 1130,
                'id_fuel' => 4,
            ),
            344 => 
            array (
                'id_reg' => 1345,
                'id_model' => 1131,
                'id_fuel' => 4,
            ),
            345 => 
            array (
                'id_reg' => 1346,
                'id_model' => 1132,
                'id_fuel' => 4,
            ),
            346 => 
            array (
                'id_reg' => 1347,
                'id_model' => 1133,
                'id_fuel' => 2,
            ),
            347 => 
            array (
                'id_reg' => 1348,
                'id_model' => 1134,
                'id_fuel' => 2,
            ),
            348 => 
            array (
                'id_reg' => 1349,
                'id_model' => 1135,
                'id_fuel' => 6,
            ),
            349 => 
            array (
                'id_reg' => 1350,
                'id_model' => 1136,
                'id_fuel' => 6,
            ),
            350 => 
            array (
                'id_reg' => 1351,
                'id_model' => 1137,
                'id_fuel' => 6,
            ),
            351 => 
            array (
                'id_reg' => 1352,
                'id_model' => 1138,
                'id_fuel' => 6,
            ),
            352 => 
            array (
                'id_reg' => 1353,
                'id_model' => 1139,
                'id_fuel' => 4,
            ),
            353 => 
            array (
                'id_reg' => 1354,
                'id_model' => 1140,
                'id_fuel' => 4,
            ),
            354 => 
            array (
                'id_reg' => 1355,
                'id_model' => 1141,
                'id_fuel' => 4,
            ),
            355 => 
            array (
                'id_reg' => 1356,
                'id_model' => 1142,
                'id_fuel' => 4,
            ),
            356 => 
            array (
                'id_reg' => 1357,
                'id_model' => 1143,
                'id_fuel' => 2,
            ),
            357 => 
            array (
                'id_reg' => 1358,
                'id_model' => 1143,
                'id_fuel' => 6,
            ),
            358 => 
            array (
                'id_reg' => 1359,
                'id_model' => 1143,
                'id_fuel' => 4,
            ),
            359 => 
            array (
                'id_reg' => 1360,
                'id_model' => 1144,
                'id_fuel' => 4,
            ),
            360 => 
            array (
                'id_reg' => 1361,
                'id_model' => 1145,
                'id_fuel' => 4,
            ),
            361 => 
            array (
                'id_reg' => 1362,
                'id_model' => 1146,
                'id_fuel' => 4,
            ),
            362 => 
            array (
                'id_reg' => 1363,
                'id_model' => 1147,
                'id_fuel' => 4,
            ),
            363 => 
            array (
                'id_reg' => 1364,
                'id_model' => 1148,
                'id_fuel' => 4,
            ),
            364 => 
            array (
                'id_reg' => 1365,
                'id_model' => 1149,
                'id_fuel' => 4,
            ),
            365 => 
            array (
                'id_reg' => 1366,
                'id_model' => 1150,
                'id_fuel' => 4,
            ),
            366 => 
            array (
                'id_reg' => 1367,
                'id_model' => 1151,
                'id_fuel' => 4,
            ),
            367 => 
            array (
                'id_reg' => 1368,
                'id_model' => 1152,
                'id_fuel' => 2,
            ),
            368 => 
            array (
                'id_reg' => 1369,
                'id_model' => 1152,
                'id_fuel' => 4,
            ),
            369 => 
            array (
                'id_reg' => 1370,
                'id_model' => 1153,
                'id_fuel' => 2,
            ),
            370 => 
            array (
                'id_reg' => 1371,
                'id_model' => 1153,
                'id_fuel' => 4,
            ),
            371 => 
            array (
                'id_reg' => 1372,
                'id_model' => 1154,
                'id_fuel' => 2,
            ),
            372 => 
            array (
                'id_reg' => 1373,
                'id_model' => 1154,
                'id_fuel' => 4,
            ),
            373 => 
            array (
                'id_reg' => 1374,
                'id_model' => 1155,
                'id_fuel' => 4,
            ),
            374 => 
            array (
                'id_reg' => 1375,
                'id_model' => 1156,
                'id_fuel' => 2,
            ),
            375 => 
            array (
                'id_reg' => 1376,
                'id_model' => 1156,
                'id_fuel' => 4,
            ),
            376 => 
            array (
                'id_reg' => 1377,
                'id_model' => 1157,
                'id_fuel' => 4,
            ),
            377 => 
            array (
                'id_reg' => 1378,
                'id_model' => 1158,
                'id_fuel' => 4,
            ),
            378 => 
            array (
                'id_reg' => 1379,
                'id_model' => 1159,
                'id_fuel' => 4,
            ),
            379 => 
            array (
                'id_reg' => 1380,
                'id_model' => 1160,
                'id_fuel' => 2,
            ),
            380 => 
            array (
                'id_reg' => 1381,
                'id_model' => 1160,
                'id_fuel' => 4,
            ),
            381 => 
            array (
                'id_reg' => 1382,
                'id_model' => 1161,
                'id_fuel' => 2,
            ),
            382 => 
            array (
                'id_reg' => 1383,
                'id_model' => 1161,
                'id_fuel' => 4,
            ),
            383 => 
            array (
                'id_reg' => 1384,
                'id_model' => 1162,
                'id_fuel' => 2,
            ),
            384 => 
            array (
                'id_reg' => 1385,
                'id_model' => 1162,
                'id_fuel' => 4,
            ),
            385 => 
            array (
                'id_reg' => 1386,
                'id_model' => 1163,
                'id_fuel' => 4,
            ),
            386 => 
            array (
                'id_reg' => 1387,
                'id_model' => 1164,
                'id_fuel' => 4,
            ),
            387 => 
            array (
                'id_reg' => 1388,
                'id_model' => 1165,
                'id_fuel' => 4,
            ),
            388 => 
            array (
                'id_reg' => 1389,
                'id_model' => 1166,
                'id_fuel' => 4,
            ),
            389 => 
            array (
                'id_reg' => 1390,
                'id_model' => 1167,
                'id_fuel' => 4,
            ),
            390 => 
            array (
                'id_reg' => 1391,
                'id_model' => 1168,
                'id_fuel' => 4,
            ),
            391 => 
            array (
                'id_reg' => 1392,
                'id_model' => 1169,
                'id_fuel' => 6,
            ),
            392 => 
            array (
                'id_reg' => 1393,
                'id_model' => 1170,
                'id_fuel' => 6,
            ),
            393 => 
            array (
                'id_reg' => 1394,
                'id_model' => 1171,
                'id_fuel' => 6,
            ),
            394 => 
            array (
                'id_reg' => 1395,
                'id_model' => 1172,
                'id_fuel' => 4,
            ),
            395 => 
            array (
                'id_reg' => 1396,
                'id_model' => 1173,
                'id_fuel' => 2,
            ),
            396 => 
            array (
                'id_reg' => 1397,
                'id_model' => 1173,
                'id_fuel' => 4,
            ),
            397 => 
            array (
                'id_reg' => 1398,
                'id_model' => 1174,
                'id_fuel' => 2,
            ),
            398 => 
            array (
                'id_reg' => 1399,
                'id_model' => 1175,
                'id_fuel' => 2,
            ),
            399 => 
            array (
                'id_reg' => 1400,
                'id_model' => 1176,
                'id_fuel' => 6,
            ),
            400 => 
            array (
                'id_reg' => 1401,
                'id_model' => 1177,
                'id_fuel' => 4,
            ),
            401 => 
            array (
                'id_reg' => 1402,
                'id_model' => 1178,
                'id_fuel' => 4,
            ),
            402 => 
            array (
                'id_reg' => 1403,
                'id_model' => 1179,
                'id_fuel' => 4,
            ),
            403 => 
            array (
                'id_reg' => 1404,
                'id_model' => 1180,
                'id_fuel' => 4,
            ),
            404 => 
            array (
                'id_reg' => 1405,
                'id_model' => 1181,
                'id_fuel' => 4,
            ),
            405 => 
            array (
                'id_reg' => 1406,
                'id_model' => 1182,
                'id_fuel' => 4,
            ),
            406 => 
            array (
                'id_reg' => 1407,
                'id_model' => 1183,
                'id_fuel' => 4,
            ),
            407 => 
            array (
                'id_reg' => 1408,
                'id_model' => 1184,
                'id_fuel' => 4,
            ),
            408 => 
            array (
                'id_reg' => 1409,
                'id_model' => 1185,
                'id_fuel' => 4,
            ),
            409 => 
            array (
                'id_reg' => 1410,
                'id_model' => 1186,
                'id_fuel' => 4,
            ),
            410 => 
            array (
                'id_reg' => 1411,
                'id_model' => 1187,
                'id_fuel' => 4,
            ),
            411 => 
            array (
                'id_reg' => 1412,
                'id_model' => 1188,
                'id_fuel' => 4,
            ),
            412 => 
            array (
                'id_reg' => 1413,
                'id_model' => 1189,
                'id_fuel' => 2,
            ),
            413 => 
            array (
                'id_reg' => 1414,
                'id_model' => 1190,
                'id_fuel' => 4,
            ),
            414 => 
            array (
                'id_reg' => 1415,
                'id_model' => 1191,
                'id_fuel' => 2,
            ),
            415 => 
            array (
                'id_reg' => 1416,
                'id_model' => 1192,
                'id_fuel' => 4,
            ),
            416 => 
            array (
                'id_reg' => 1417,
                'id_model' => 1193,
                'id_fuel' => 2,
            ),
            417 => 
            array (
                'id_reg' => 1418,
                'id_model' => 1193,
                'id_fuel' => 4,
            ),
            418 => 
            array (
                'id_reg' => 1419,
                'id_model' => 1194,
                'id_fuel' => 5,
            ),
            419 => 
            array (
                'id_reg' => 1420,
                'id_model' => 1195,
                'id_fuel' => 4,
            ),
            420 => 
            array (
                'id_reg' => 1421,
                'id_model' => 1196,
                'id_fuel' => 4,
            ),
            421 => 
            array (
                'id_reg' => 1422,
                'id_model' => 1197,
                'id_fuel' => 4,
            ),
            422 => 
            array (
                'id_reg' => 1423,
                'id_model' => 1198,
                'id_fuel' => 5,
            ),
            423 => 
            array (
                'id_reg' => 1424,
                'id_model' => 1199,
                'id_fuel' => 4,
            ),
            424 => 
            array (
                'id_reg' => 1425,
                'id_model' => 1200,
                'id_fuel' => 2,
            ),
            425 => 
            array (
                'id_reg' => 1426,
                'id_model' => 1201,
                'id_fuel' => 4,
            ),
            426 => 
            array (
                'id_reg' => 1427,
                'id_model' => 1202,
                'id_fuel' => 4,
            ),
            427 => 
            array (
                'id_reg' => 1428,
                'id_model' => 1203,
                'id_fuel' => 6,
            ),
            428 => 
            array (
                'id_reg' => 1429,
                'id_model' => 1204,
                'id_fuel' => 4,
            ),
            429 => 
            array (
                'id_reg' => 1430,
                'id_model' => 1205,
                'id_fuel' => 4,
            ),
            430 => 
            array (
                'id_reg' => 1431,
                'id_model' => 1206,
                'id_fuel' => 4,
            ),
            431 => 
            array (
                'id_reg' => 1432,
                'id_model' => 1207,
                'id_fuel' => 2,
            ),
            432 => 
            array (
                'id_reg' => 1433,
                'id_model' => 1207,
                'id_fuel' => 4,
            ),
            433 => 
            array (
                'id_reg' => 1434,
                'id_model' => 1208,
                'id_fuel' => 4,
            ),
            434 => 
            array (
                'id_reg' => 1435,
                'id_model' => 1209,
                'id_fuel' => 4,
            ),
            435 => 
            array (
                'id_reg' => 1436,
                'id_model' => 1210,
                'id_fuel' => 4,
            ),
            436 => 
            array (
                'id_reg' => 1437,
                'id_model' => 1211,
                'id_fuel' => 4,
            ),
            437 => 
            array (
                'id_reg' => 1438,
                'id_model' => 1212,
                'id_fuel' => 4,
            ),
            438 => 
            array (
                'id_reg' => 1439,
                'id_model' => 1213,
                'id_fuel' => 4,
            ),
            439 => 
            array (
                'id_reg' => 1440,
                'id_model' => 1214,
                'id_fuel' => 4,
            ),
            440 => 
            array (
                'id_reg' => 1441,
                'id_model' => 1215,
                'id_fuel' => 4,
            ),
            441 => 
            array (
                'id_reg' => 1442,
                'id_model' => 1216,
                'id_fuel' => 4,
            ),
            442 => 
            array (
                'id_reg' => 1443,
                'id_model' => 1217,
                'id_fuel' => 4,
            ),
            443 => 
            array (
                'id_reg' => 1444,
                'id_model' => 1218,
                'id_fuel' => 4,
            ),
            444 => 
            array (
                'id_reg' => 1445,
                'id_model' => 1219,
                'id_fuel' => 6,
            ),
            445 => 
            array (
                'id_reg' => 1446,
                'id_model' => 1220,
                'id_fuel' => 4,
            ),
            446 => 
            array (
                'id_reg' => 1447,
                'id_model' => 1221,
                'id_fuel' => 4,
            ),
            447 => 
            array (
                'id_reg' => 1448,
                'id_model' => 1222,
                'id_fuel' => 4,
            ),
            448 => 
            array (
                'id_reg' => 1449,
                'id_model' => 1223,
                'id_fuel' => 4,
            ),
            449 => 
            array (
                'id_reg' => 1450,
                'id_model' => 1224,
                'id_fuel' => 4,
            ),
            450 => 
            array (
                'id_reg' => 1451,
                'id_model' => 1225,
                'id_fuel' => 4,
            ),
            451 => 
            array (
                'id_reg' => 1452,
                'id_model' => 1226,
                'id_fuel' => 4,
            ),
            452 => 
            array (
                'id_reg' => 1453,
                'id_model' => 1227,
                'id_fuel' => 4,
            ),
            453 => 
            array (
                'id_reg' => 1454,
                'id_model' => 1228,
                'id_fuel' => 4,
            ),
            454 => 
            array (
                'id_reg' => 1455,
                'id_model' => 1229,
                'id_fuel' => 4,
            ),
            455 => 
            array (
                'id_reg' => 1456,
                'id_model' => 1230,
                'id_fuel' => 4,
            ),
            456 => 
            array (
                'id_reg' => 1457,
                'id_model' => 1231,
                'id_fuel' => 4,
            ),
            457 => 
            array (
                'id_reg' => 1458,
                'id_model' => 1232,
                'id_fuel' => 2,
            ),
            458 => 
            array (
                'id_reg' => 1459,
                'id_model' => 1232,
                'id_fuel' => 4,
            ),
            459 => 
            array (
                'id_reg' => 1460,
                'id_model' => 1233,
                'id_fuel' => 1,
            ),
            460 => 
            array (
                'id_reg' => 1461,
                'id_model' => 1233,
                'id_fuel' => 2,
            ),
            461 => 
            array (
                'id_reg' => 1462,
                'id_model' => 1233,
                'id_fuel' => 4,
            ),
            462 => 
            array (
                'id_reg' => 1463,
                'id_model' => 1234,
                'id_fuel' => 4,
            ),
            463 => 
            array (
                'id_reg' => 1464,
                'id_model' => 1235,
                'id_fuel' => 4,
            ),
            464 => 
            array (
                'id_reg' => 1465,
                'id_model' => 1236,
                'id_fuel' => 4,
            ),
            465 => 
            array (
                'id_reg' => 1466,
                'id_model' => 1237,
                'id_fuel' => 2,
            ),
            466 => 
            array (
                'id_reg' => 1467,
                'id_model' => 1237,
                'id_fuel' => 4,
            ),
            467 => 
            array (
                'id_reg' => 1468,
                'id_model' => 1238,
                'id_fuel' => 4,
            ),
            468 => 
            array (
                'id_reg' => 1469,
                'id_model' => 1239,
                'id_fuel' => 4,
            ),
            469 => 
            array (
                'id_reg' => 1470,
                'id_model' => 1240,
                'id_fuel' => 4,
            ),
            470 => 
            array (
                'id_reg' => 1471,
                'id_model' => 1241,
                'id_fuel' => 2,
            ),
            471 => 
            array (
                'id_reg' => 1472,
                'id_model' => 1241,
                'id_fuel' => 4,
            ),
            472 => 
            array (
                'id_reg' => 1473,
                'id_model' => 1242,
                'id_fuel' => 4,
            ),
            473 => 
            array (
                'id_reg' => 1474,
                'id_model' => 1243,
                'id_fuel' => 4,
            ),
            474 => 
            array (
                'id_reg' => 1475,
                'id_model' => 1244,
                'id_fuel' => 4,
            ),
            475 => 
            array (
                'id_reg' => 1476,
                'id_model' => 1245,
                'id_fuel' => 4,
            ),
            476 => 
            array (
                'id_reg' => 1477,
                'id_model' => 1246,
                'id_fuel' => 2,
            ),
            477 => 
            array (
                'id_reg' => 1478,
                'id_model' => 1247,
                'id_fuel' => 2,
            ),
            478 => 
            array (
                'id_reg' => 1479,
                'id_model' => 1247,
                'id_fuel' => 4,
            ),
            479 => 
            array (
                'id_reg' => 1480,
                'id_model' => 1248,
                'id_fuel' => 2,
            ),
            480 => 
            array (
                'id_reg' => 1481,
                'id_model' => 1248,
                'id_fuel' => 4,
            ),
            481 => 
            array (
                'id_reg' => 1482,
                'id_model' => 1249,
                'id_fuel' => 2,
            ),
            482 => 
            array (
                'id_reg' => 1483,
                'id_model' => 1250,
                'id_fuel' => 2,
            ),
            483 => 
            array (
                'id_reg' => 1484,
                'id_model' => 1251,
                'id_fuel' => 2,
            ),
            484 => 
            array (
                'id_reg' => 1485,
                'id_model' => 1251,
                'id_fuel' => 4,
            ),
            485 => 
            array (
                'id_reg' => 1486,
                'id_model' => 1252,
                'id_fuel' => 2,
            ),
            486 => 
            array (
                'id_reg' => 1487,
                'id_model' => 1253,
                'id_fuel' => 2,
            ),
            487 => 
            array (
                'id_reg' => 1488,
                'id_model' => 1254,
                'id_fuel' => 2,
            ),
            488 => 
            array (
                'id_reg' => 1489,
                'id_model' => 1255,
                'id_fuel' => 2,
            ),
            489 => 
            array (
                'id_reg' => 1490,
                'id_model' => 1255,
                'id_fuel' => 4,
            ),
            490 => 
            array (
                'id_reg' => 1491,
                'id_model' => 1256,
                'id_fuel' => 2,
            ),
            491 => 
            array (
                'id_reg' => 1492,
                'id_model' => 1256,
                'id_fuel' => 4,
            ),
            492 => 
            array (
                'id_reg' => 1493,
                'id_model' => 1257,
                'id_fuel' => 2,
            ),
            493 => 
            array (
                'id_reg' => 1494,
                'id_model' => 1257,
                'id_fuel' => 4,
            ),
            494 => 
            array (
                'id_reg' => 1495,
                'id_model' => 1258,
                'id_fuel' => 2,
            ),
            495 => 
            array (
                'id_reg' => 1496,
                'id_model' => 1258,
                'id_fuel' => 4,
            ),
            496 => 
            array (
                'id_reg' => 1497,
                'id_model' => 1259,
                'id_fuel' => 2,
            ),
            497 => 
            array (
                'id_reg' => 1498,
                'id_model' => 1260,
                'id_fuel' => 2,
            ),
            498 => 
            array (
                'id_reg' => 1499,
                'id_model' => 1261,
                'id_fuel' => 6,
            ),
            499 => 
            array (
                'id_reg' => 1500,
                'id_model' => 1261,
                'id_fuel' => 5,
            ),
        ));
        \DB::table('model_rel_fuel')->insert(array (
            0 => 
            array (
                'id_reg' => 1501,
                'id_model' => 1262,
                'id_fuel' => 2,
            ),
            1 => 
            array (
                'id_reg' => 1502,
                'id_model' => 1262,
                'id_fuel' => 4,
            ),
            2 => 
            array (
                'id_reg' => 1503,
                'id_model' => 1263,
                'id_fuel' => 2,
            ),
            3 => 
            array (
                'id_reg' => 1504,
                'id_model' => 1263,
                'id_fuel' => 4,
            ),
            4 => 
            array (
                'id_reg' => 1505,
                'id_model' => 1264,
                'id_fuel' => 2,
            ),
            5 => 
            array (
                'id_reg' => 1506,
                'id_model' => 1265,
                'id_fuel' => 4,
            ),
            6 => 
            array (
                'id_reg' => 1507,
                'id_model' => 1266,
                'id_fuel' => 4,
            ),
            7 => 
            array (
                'id_reg' => 1508,
                'id_model' => 1267,
                'id_fuel' => 4,
            ),
            8 => 
            array (
                'id_reg' => 1509,
                'id_model' => 1268,
                'id_fuel' => 2,
            ),
            9 => 
            array (
                'id_reg' => 1510,
                'id_model' => 1269,
                'id_fuel' => 4,
            ),
            10 => 
            array (
                'id_reg' => 1511,
                'id_model' => 1270,
                'id_fuel' => 4,
            ),
            11 => 
            array (
                'id_reg' => 1512,
                'id_model' => 1271,
                'id_fuel' => 4,
            ),
            12 => 
            array (
                'id_reg' => 1513,
                'id_model' => 1272,
                'id_fuel' => 2,
            ),
            13 => 
            array (
                'id_reg' => 1514,
                'id_model' => 1272,
                'id_fuel' => 4,
            ),
            14 => 
            array (
                'id_reg' => 1515,
                'id_model' => 1273,
                'id_fuel' => 2,
            ),
            15 => 
            array (
                'id_reg' => 1516,
                'id_model' => 1273,
                'id_fuel' => 4,
            ),
            16 => 
            array (
                'id_reg' => 1517,
                'id_model' => 1274,
                'id_fuel' => 4,
            ),
            17 => 
            array (
                'id_reg' => 1518,
                'id_model' => 1275,
                'id_fuel' => 2,
            ),
            18 => 
            array (
                'id_reg' => 1519,
                'id_model' => 1275,
                'id_fuel' => 4,
            ),
            19 => 
            array (
                'id_reg' => 1520,
                'id_model' => 1276,
                'id_fuel' => 4,
            ),
            20 => 
            array (
                'id_reg' => 1521,
                'id_model' => 1277,
                'id_fuel' => 2,
            ),
            21 => 
            array (
                'id_reg' => 1522,
                'id_model' => 1278,
                'id_fuel' => 2,
            ),
            22 => 
            array (
                'id_reg' => 1523,
                'id_model' => 1279,
                'id_fuel' => 2,
            ),
            23 => 
            array (
                'id_reg' => 1524,
                'id_model' => 1280,
                'id_fuel' => 6,
            ),
            24 => 
            array (
                'id_reg' => 1525,
                'id_model' => 1281,
                'id_fuel' => 2,
            ),
            25 => 
            array (
                'id_reg' => 1526,
                'id_model' => 1281,
                'id_fuel' => 4,
            ),
            26 => 
            array (
                'id_reg' => 1527,
                'id_model' => 1282,
                'id_fuel' => 2,
            ),
            27 => 
            array (
                'id_reg' => 1528,
                'id_model' => 1282,
                'id_fuel' => 4,
            ),
            28 => 
            array (
                'id_reg' => 1529,
                'id_model' => 1283,
                'id_fuel' => 4,
            ),
            29 => 
            array (
                'id_reg' => 1530,
                'id_model' => 1284,
                'id_fuel' => 4,
            ),
            30 => 
            array (
                'id_reg' => 1531,
                'id_model' => 1285,
                'id_fuel' => 2,
            ),
            31 => 
            array (
                'id_reg' => 1532,
                'id_model' => 1285,
                'id_fuel' => 4,
            ),
            32 => 
            array (
                'id_reg' => 1533,
                'id_model' => 1285,
                'id_fuel' => 5,
            ),
            33 => 
            array (
                'id_reg' => 1534,
                'id_model' => 1286,
                'id_fuel' => 4,
            ),
            34 => 
            array (
                'id_reg' => 1535,
                'id_model' => 1287,
                'id_fuel' => 2,
            ),
            35 => 
            array (
                'id_reg' => 1536,
                'id_model' => 1287,
                'id_fuel' => 4,
            ),
            36 => 
            array (
                'id_reg' => 1537,
                'id_model' => 1288,
                'id_fuel' => 2,
            ),
            37 => 
            array (
                'id_reg' => 1538,
                'id_model' => 1288,
                'id_fuel' => 4,
            ),
            38 => 
            array (
                'id_reg' => 1539,
                'id_model' => 1288,
                'id_fuel' => 5,
            ),
            39 => 
            array (
                'id_reg' => 1540,
                'id_model' => 1289,
                'id_fuel' => 2,
            ),
            40 => 
            array (
                'id_reg' => 1541,
                'id_model' => 1289,
                'id_fuel' => 4,
            ),
            41 => 
            array (
                'id_reg' => 1542,
                'id_model' => 1290,
                'id_fuel' => 2,
            ),
            42 => 
            array (
                'id_reg' => 1543,
                'id_model' => 1290,
                'id_fuel' => 4,
            ),
            43 => 
            array (
                'id_reg' => 1544,
                'id_model' => 1290,
                'id_fuel' => 5,
            ),
            44 => 
            array (
                'id_reg' => 1545,
                'id_model' => 1291,
                'id_fuel' => 4,
            ),
            45 => 
            array (
                'id_reg' => 1546,
                'id_model' => 1292,
                'id_fuel' => 2,
            ),
            46 => 
            array (
                'id_reg' => 1547,
                'id_model' => 1293,
                'id_fuel' => 2,
            ),
            47 => 
            array (
                'id_reg' => 1548,
                'id_model' => 1293,
                'id_fuel' => 4,
            ),
            48 => 
            array (
                'id_reg' => 1549,
                'id_model' => 1294,
                'id_fuel' => 2,
            ),
            49 => 
            array (
                'id_reg' => 1550,
                'id_model' => 1294,
                'id_fuel' => 4,
            ),
            50 => 
            array (
                'id_reg' => 1551,
                'id_model' => 1295,
                'id_fuel' => 4,
            ),
            51 => 
            array (
                'id_reg' => 1552,
                'id_model' => 1296,
                'id_fuel' => 4,
            ),
            52 => 
            array (
                'id_reg' => 1553,
                'id_model' => 1297,
                'id_fuel' => 2,
            ),
            53 => 
            array (
                'id_reg' => 1554,
                'id_model' => 1297,
                'id_fuel' => 4,
            ),
            54 => 
            array (
                'id_reg' => 1555,
                'id_model' => 1298,
                'id_fuel' => 4,
            ),
            55 => 
            array (
                'id_reg' => 1556,
                'id_model' => 1299,
                'id_fuel' => 2,
            ),
            56 => 
            array (
                'id_reg' => 1557,
                'id_model' => 1299,
                'id_fuel' => 4,
            ),
            57 => 
            array (
                'id_reg' => 1558,
                'id_model' => 1300,
                'id_fuel' => 2,
            ),
            58 => 
            array (
                'id_reg' => 1559,
                'id_model' => 1300,
                'id_fuel' => 4,
            ),
            59 => 
            array (
                'id_reg' => 1560,
                'id_model' => 1301,
                'id_fuel' => 2,
            ),
            60 => 
            array (
                'id_reg' => 1561,
                'id_model' => 1301,
                'id_fuel' => 4,
            ),
            61 => 
            array (
                'id_reg' => 1562,
                'id_model' => 1302,
                'id_fuel' => 2,
            ),
            62 => 
            array (
                'id_reg' => 1563,
                'id_model' => 1303,
                'id_fuel' => 4,
            ),
            63 => 
            array (
                'id_reg' => 1564,
                'id_model' => 1304,
                'id_fuel' => 2,
            ),
            64 => 
            array (
                'id_reg' => 1565,
                'id_model' => 1305,
                'id_fuel' => 2,
            ),
            65 => 
            array (
                'id_reg' => 1566,
                'id_model' => 1305,
                'id_fuel' => 4,
            ),
            66 => 
            array (
                'id_reg' => 1567,
                'id_model' => 1306,
                'id_fuel' => 4,
            ),
            67 => 
            array (
                'id_reg' => 1568,
                'id_model' => 1307,
                'id_fuel' => 4,
            ),
            68 => 
            array (
                'id_reg' => 1569,
                'id_model' => 1308,
                'id_fuel' => 4,
            ),
            69 => 
            array (
                'id_reg' => 1570,
                'id_model' => 1309,
                'id_fuel' => 4,
            ),
            70 => 
            array (
                'id_reg' => 1571,
                'id_model' => 1310,
                'id_fuel' => 2,
            ),
            71 => 
            array (
                'id_reg' => 1572,
                'id_model' => 1311,
                'id_fuel' => 2,
            ),
            72 => 
            array (
                'id_reg' => 1573,
                'id_model' => 1311,
                'id_fuel' => 4,
            ),
            73 => 
            array (
                'id_reg' => 1574,
                'id_model' => 1312,
                'id_fuel' => 2,
            ),
            74 => 
            array (
                'id_reg' => 1575,
                'id_model' => 1313,
                'id_fuel' => 2,
            ),
            75 => 
            array (
                'id_reg' => 1576,
                'id_model' => 1314,
                'id_fuel' => 2,
            ),
            76 => 
            array (
                'id_reg' => 1577,
                'id_model' => 1314,
                'id_fuel' => 4,
            ),
            77 => 
            array (
                'id_reg' => 1578,
                'id_model' => 1315,
                'id_fuel' => 4,
            ),
            78 => 
            array (
                'id_reg' => 1579,
                'id_model' => 1316,
                'id_fuel' => 2,
            ),
            79 => 
            array (
                'id_reg' => 1580,
                'id_model' => 1316,
                'id_fuel' => 4,
            ),
            80 => 
            array (
                'id_reg' => 1581,
                'id_model' => 1317,
                'id_fuel' => 4,
            ),
            81 => 
            array (
                'id_reg' => 1582,
                'id_model' => 1318,
                'id_fuel' => 2,
            ),
            82 => 
            array (
                'id_reg' => 1583,
                'id_model' => 1318,
                'id_fuel' => 4,
            ),
            83 => 
            array (
                'id_reg' => 1584,
                'id_model' => 1319,
                'id_fuel' => 2,
            ),
            84 => 
            array (
                'id_reg' => 1585,
                'id_model' => 1319,
                'id_fuel' => 4,
            ),
            85 => 
            array (
                'id_reg' => 1586,
                'id_model' => 1320,
                'id_fuel' => 2,
            ),
            86 => 
            array (
                'id_reg' => 1587,
                'id_model' => 1321,
                'id_fuel' => 2,
            ),
            87 => 
            array (
                'id_reg' => 1588,
                'id_model' => 1322,
                'id_fuel' => 2,
            ),
            88 => 
            array (
                'id_reg' => 1589,
                'id_model' => 1322,
                'id_fuel' => 4,
            ),
            89 => 
            array (
                'id_reg' => 1590,
                'id_model' => 1323,
                'id_fuel' => 2,
            ),
            90 => 
            array (
                'id_reg' => 1591,
                'id_model' => 1324,
                'id_fuel' => 6,
            ),
            91 => 
            array (
                'id_reg' => 1592,
                'id_model' => 1325,
                'id_fuel' => 6,
            ),
            92 => 
            array (
                'id_reg' => 1593,
                'id_model' => 1326,
                'id_fuel' => 2,
            ),
            93 => 
            array (
                'id_reg' => 1594,
                'id_model' => 1327,
                'id_fuel' => 2,
            ),
            94 => 
            array (
                'id_reg' => 1595,
                'id_model' => 1328,
                'id_fuel' => 2,
            ),
            95 => 
            array (
                'id_reg' => 1596,
                'id_model' => 1329,
                'id_fuel' => 2,
            ),
            96 => 
            array (
                'id_reg' => 1597,
                'id_model' => 1330,
                'id_fuel' => 2,
            ),
            97 => 
            array (
                'id_reg' => 1598,
                'id_model' => 1330,
                'id_fuel' => 3,
            ),
            98 => 
            array (
                'id_reg' => 1599,
                'id_model' => 1331,
                'id_fuel' => 2,
            ),
            99 => 
            array (
                'id_reg' => 1600,
                'id_model' => 1331,
                'id_fuel' => 3,
            ),
            100 => 
            array (
                'id_reg' => 1601,
                'id_model' => 1332,
                'id_fuel' => 2,
            ),
            101 => 
            array (
                'id_reg' => 1602,
                'id_model' => 1332,
                'id_fuel' => 3,
            ),
            102 => 
            array (
                'id_reg' => 1603,
                'id_model' => 1333,
                'id_fuel' => 2,
            ),
            103 => 
            array (
                'id_reg' => 1604,
                'id_model' => 1334,
                'id_fuel' => 2,
            ),
            104 => 
            array (
                'id_reg' => 1605,
                'id_model' => 1335,
                'id_fuel' => 2,
            ),
            105 => 
            array (
                'id_reg' => 1606,
                'id_model' => 1336,
                'id_fuel' => 2,
            ),
            106 => 
            array (
                'id_reg' => 1607,
                'id_model' => 1337,
                'id_fuel' => 4,
            ),
            107 => 
            array (
                'id_reg' => 1608,
                'id_model' => 1338,
                'id_fuel' => 4,
            ),
            108 => 
            array (
                'id_reg' => 1609,
                'id_model' => 1339,
                'id_fuel' => 4,
            ),
            109 => 
            array (
                'id_reg' => 1610,
                'id_model' => 1340,
                'id_fuel' => 4,
            ),
            110 => 
            array (
                'id_reg' => 1611,
                'id_model' => 1341,
                'id_fuel' => 4,
            ),
            111 => 
            array (
                'id_reg' => 1612,
                'id_model' => 1342,
                'id_fuel' => 2,
            ),
            112 => 
            array (
                'id_reg' => 1613,
                'id_model' => 1342,
                'id_fuel' => 4,
            ),
            113 => 
            array (
                'id_reg' => 1614,
                'id_model' => 1343,
                'id_fuel' => 4,
            ),
            114 => 
            array (
                'id_reg' => 1615,
                'id_model' => 1344,
                'id_fuel' => 4,
            ),
            115 => 
            array (
                'id_reg' => 1616,
                'id_model' => 1345,
                'id_fuel' => 4,
            ),
            116 => 
            array (
                'id_reg' => 1617,
                'id_model' => 1346,
                'id_fuel' => 4,
            ),
            117 => 
            array (
                'id_reg' => 1618,
                'id_model' => 1347,
                'id_fuel' => 4,
            ),
            118 => 
            array (
                'id_reg' => 1619,
                'id_model' => 1348,
                'id_fuel' => 4,
            ),
            119 => 
            array (
                'id_reg' => 1620,
                'id_model' => 1349,
                'id_fuel' => 4,
            ),
            120 => 
            array (
                'id_reg' => 1621,
                'id_model' => 1350,
                'id_fuel' => 4,
            ),
            121 => 
            array (
                'id_reg' => 1622,
                'id_model' => 1351,
                'id_fuel' => 2,
            ),
            122 => 
            array (
                'id_reg' => 1623,
                'id_model' => 1351,
                'id_fuel' => 4,
            ),
            123 => 
            array (
                'id_reg' => 1624,
                'id_model' => 1352,
                'id_fuel' => 4,
            ),
            124 => 
            array (
                'id_reg' => 1625,
                'id_model' => 1353,
                'id_fuel' => 4,
            ),
            125 => 
            array (
                'id_reg' => 1626,
                'id_model' => 1354,
                'id_fuel' => 4,
            ),
            126 => 
            array (
                'id_reg' => 1627,
                'id_model' => 1355,
                'id_fuel' => 4,
            ),
            127 => 
            array (
                'id_reg' => 1628,
                'id_model' => 1356,
                'id_fuel' => 2,
            ),
            128 => 
            array (
                'id_reg' => 1629,
                'id_model' => 1356,
                'id_fuel' => 4,
            ),
            129 => 
            array (
                'id_reg' => 1630,
                'id_model' => 1357,
                'id_fuel' => 2,
            ),
            130 => 
            array (
                'id_reg' => 1631,
                'id_model' => 1357,
                'id_fuel' => 4,
            ),
            131 => 
            array (
                'id_reg' => 1632,
                'id_model' => 1358,
                'id_fuel' => 2,
            ),
            132 => 
            array (
                'id_reg' => 1633,
                'id_model' => 1358,
                'id_fuel' => 4,
            ),
            133 => 
            array (
                'id_reg' => 1634,
                'id_model' => 1359,
                'id_fuel' => 2,
            ),
            134 => 
            array (
                'id_reg' => 1635,
                'id_model' => 1359,
                'id_fuel' => 4,
            ),
            135 => 
            array (
                'id_reg' => 1636,
                'id_model' => 1360,
                'id_fuel' => 4,
            ),
            136 => 
            array (
                'id_reg' => 1637,
                'id_model' => 1361,
                'id_fuel' => 4,
            ),
            137 => 
            array (
                'id_reg' => 1638,
                'id_model' => 1362,
                'id_fuel' => 4,
            ),
            138 => 
            array (
                'id_reg' => 1639,
                'id_model' => 1363,
                'id_fuel' => 4,
            ),
            139 => 
            array (
                'id_reg' => 1640,
                'id_model' => 1364,
                'id_fuel' => 4,
            ),
            140 => 
            array (
                'id_reg' => 1641,
                'id_model' => 1365,
                'id_fuel' => 4,
            ),
            141 => 
            array (
                'id_reg' => 1642,
                'id_model' => 1366,
                'id_fuel' => 4,
            ),
            142 => 
            array (
                'id_reg' => 1643,
                'id_model' => 1367,
                'id_fuel' => 4,
            ),
            143 => 
            array (
                'id_reg' => 1644,
                'id_model' => 1368,
                'id_fuel' => 4,
            ),
            144 => 
            array (
                'id_reg' => 1645,
                'id_model' => 1369,
                'id_fuel' => 4,
            ),
            145 => 
            array (
                'id_reg' => 1646,
                'id_model' => 1370,
                'id_fuel' => 4,
            ),
            146 => 
            array (
                'id_reg' => 1647,
                'id_model' => 1371,
                'id_fuel' => 2,
            ),
            147 => 
            array (
                'id_reg' => 1648,
                'id_model' => 1372,
                'id_fuel' => 2,
            ),
            148 => 
            array (
                'id_reg' => 1649,
                'id_model' => 1373,
                'id_fuel' => 2,
            ),
            149 => 
            array (
                'id_reg' => 1650,
                'id_model' => 1374,
                'id_fuel' => 2,
            ),
            150 => 
            array (
                'id_reg' => 1651,
                'id_model' => 1375,
                'id_fuel' => 2,
            ),
            151 => 
            array (
                'id_reg' => 1652,
                'id_model' => 1376,
                'id_fuel' => 2,
            ),
            152 => 
            array (
                'id_reg' => 1653,
                'id_model' => 1377,
                'id_fuel' => 2,
            ),
            153 => 
            array (
                'id_reg' => 1654,
                'id_model' => 1378,
                'id_fuel' => 2,
            ),
            154 => 
            array (
                'id_reg' => 1655,
                'id_model' => 1379,
                'id_fuel' => 2,
            ),
            155 => 
            array (
                'id_reg' => 1656,
                'id_model' => 1380,
                'id_fuel' => 2,
            ),
            156 => 
            array (
                'id_reg' => 1657,
                'id_model' => 1380,
                'id_fuel' => 4,
            ),
            157 => 
            array (
                'id_reg' => 1658,
                'id_model' => 1381,
                'id_fuel' => 4,
            ),
            158 => 
            array (
                'id_reg' => 1659,
                'id_model' => 1382,
                'id_fuel' => 2,
            ),
            159 => 
            array (
                'id_reg' => 1660,
                'id_model' => 1382,
                'id_fuel' => 4,
            ),
            160 => 
            array (
                'id_reg' => 1661,
                'id_model' => 1383,
                'id_fuel' => 4,
            ),
            161 => 
            array (
                'id_reg' => 1662,
                'id_model' => 1384,
                'id_fuel' => 4,
            ),
            162 => 
            array (
                'id_reg' => 1663,
                'id_model' => 1385,
                'id_fuel' => 2,
            ),
            163 => 
            array (
                'id_reg' => 1664,
                'id_model' => 1385,
                'id_fuel' => 4,
            ),
            164 => 
            array (
                'id_reg' => 1665,
                'id_model' => 1386,
                'id_fuel' => 2,
            ),
            165 => 
            array (
                'id_reg' => 1666,
                'id_model' => 1386,
                'id_fuel' => 4,
            ),
            166 => 
            array (
                'id_reg' => 1667,
                'id_model' => 1387,
                'id_fuel' => 1,
            ),
            167 => 
            array (
                'id_reg' => 1668,
                'id_model' => 1387,
                'id_fuel' => 2,
            ),
            168 => 
            array (
                'id_reg' => 1669,
                'id_model' => 1387,
                'id_fuel' => 4,
            ),
            169 => 
            array (
                'id_reg' => 1670,
                'id_model' => 1388,
                'id_fuel' => 4,
            ),
            170 => 
            array (
                'id_reg' => 1671,
                'id_model' => 1389,
                'id_fuel' => 2,
            ),
            171 => 
            array (
                'id_reg' => 1672,
                'id_model' => 1389,
                'id_fuel' => 4,
            ),
            172 => 
            array (
                'id_reg' => 1673,
                'id_model' => 1390,
                'id_fuel' => 2,
            ),
            173 => 
            array (
                'id_reg' => 1674,
                'id_model' => 1390,
                'id_fuel' => 4,
            ),
            174 => 
            array (
                'id_reg' => 1675,
                'id_model' => 1391,
                'id_fuel' => 2,
            ),
            175 => 
            array (
                'id_reg' => 1676,
                'id_model' => 1391,
                'id_fuel' => 4,
            ),
            176 => 
            array (
                'id_reg' => 1677,
                'id_model' => 1392,
                'id_fuel' => 4,
            ),
            177 => 
            array (
                'id_reg' => 1678,
                'id_model' => 1393,
                'id_fuel' => 2,
            ),
            178 => 
            array (
                'id_reg' => 1679,
                'id_model' => 1393,
                'id_fuel' => 4,
            ),
            179 => 
            array (
                'id_reg' => 1680,
                'id_model' => 1394,
                'id_fuel' => 2,
            ),
            180 => 
            array (
                'id_reg' => 1681,
                'id_model' => 1394,
                'id_fuel' => 4,
            ),
            181 => 
            array (
                'id_reg' => 1682,
                'id_model' => 1395,
                'id_fuel' => 2,
            ),
            182 => 
            array (
                'id_reg' => 1683,
                'id_model' => 1396,
                'id_fuel' => 2,
            ),
            183 => 
            array (
                'id_reg' => 1684,
                'id_model' => 1397,
                'id_fuel' => 2,
            ),
            184 => 
            array (
                'id_reg' => 1685,
                'id_model' => 1398,
                'id_fuel' => 2,
            ),
            185 => 
            array (
                'id_reg' => 1686,
                'id_model' => 1398,
                'id_fuel' => 4,
            ),
            186 => 
            array (
                'id_reg' => 1687,
                'id_model' => 1399,
                'id_fuel' => 4,
            ),
            187 => 
            array (
                'id_reg' => 1688,
                'id_model' => 1400,
                'id_fuel' => 4,
            ),
            188 => 
            array (
                'id_reg' => 1689,
                'id_model' => 1401,
                'id_fuel' => 6,
            ),
            189 => 
            array (
                'id_reg' => 1690,
                'id_model' => 1402,
                'id_fuel' => 4,
            ),
            190 => 
            array (
                'id_reg' => 1691,
                'id_model' => 1403,
                'id_fuel' => 4,
            ),
            191 => 
            array (
                'id_reg' => 1692,
                'id_model' => 1404,
                'id_fuel' => 4,
            ),
            192 => 
            array (
                'id_reg' => 1693,
                'id_model' => 1405,
                'id_fuel' => 2,
            ),
            193 => 
            array (
                'id_reg' => 1694,
                'id_model' => 1405,
                'id_fuel' => 6,
            ),
            194 => 
            array (
                'id_reg' => 1695,
                'id_model' => 1405,
                'id_fuel' => 4,
            ),
            195 => 
            array (
                'id_reg' => 1696,
                'id_model' => 1406,
                'id_fuel' => 4,
            ),
            196 => 
            array (
                'id_reg' => 1697,
                'id_model' => 1407,
                'id_fuel' => 4,
            ),
            197 => 
            array (
                'id_reg' => 1698,
                'id_model' => 1408,
                'id_fuel' => 4,
            ),
            198 => 
            array (
                'id_reg' => 1699,
                'id_model' => 1409,
                'id_fuel' => 4,
            ),
            199 => 
            array (
                'id_reg' => 1700,
                'id_model' => 1410,
                'id_fuel' => 4,
            ),
            200 => 
            array (
                'id_reg' => 1701,
                'id_model' => 1411,
                'id_fuel' => 4,
            ),
            201 => 
            array (
                'id_reg' => 1702,
                'id_model' => 1412,
                'id_fuel' => 4,
            ),
            202 => 
            array (
                'id_reg' => 1703,
                'id_model' => 1413,
                'id_fuel' => 2,
            ),
            203 => 
            array (
                'id_reg' => 1704,
                'id_model' => 1414,
                'id_fuel' => 4,
            ),
            204 => 
            array (
                'id_reg' => 1705,
                'id_model' => 1415,
                'id_fuel' => 4,
            ),
            205 => 
            array (
                'id_reg' => 1706,
                'id_model' => 1416,
                'id_fuel' => 4,
            ),
            206 => 
            array (
                'id_reg' => 1707,
                'id_model' => 1417,
                'id_fuel' => 4,
            ),
            207 => 
            array (
                'id_reg' => 1708,
                'id_model' => 1418,
                'id_fuel' => 2,
            ),
            208 => 
            array (
                'id_reg' => 1709,
                'id_model' => 1418,
                'id_fuel' => 4,
            ),
            209 => 
            array (
                'id_reg' => 1710,
                'id_model' => 1419,
                'id_fuel' => 4,
            ),
            210 => 
            array (
                'id_reg' => 1711,
                'id_model' => 1420,
                'id_fuel' => 6,
            ),
            211 => 
            array (
                'id_reg' => 1712,
                'id_model' => 1421,
                'id_fuel' => 2,
            ),
            212 => 
            array (
                'id_reg' => 1713,
                'id_model' => 1422,
                'id_fuel' => 2,
            ),
            213 => 
            array (
                'id_reg' => 1714,
                'id_model' => 1423,
                'id_fuel' => 4,
            ),
            214 => 
            array (
                'id_reg' => 1715,
                'id_model' => 1424,
                'id_fuel' => 4,
            ),
            215 => 
            array (
                'id_reg' => 1716,
                'id_model' => 1425,
                'id_fuel' => 4,
            ),
            216 => 
            array (
                'id_reg' => 1717,
                'id_model' => 1426,
                'id_fuel' => 2,
            ),
            217 => 
            array (
                'id_reg' => 1718,
                'id_model' => 1427,
                'id_fuel' => 4,
            ),
            218 => 
            array (
                'id_reg' => 1719,
                'id_model' => 1428,
                'id_fuel' => 2,
            ),
            219 => 
            array (
                'id_reg' => 1720,
                'id_model' => 1429,
                'id_fuel' => 2,
            ),
            220 => 
            array (
                'id_reg' => 1721,
                'id_model' => 1430,
                'id_fuel' => 2,
            ),
            221 => 
            array (
                'id_reg' => 1722,
                'id_model' => 1431,
                'id_fuel' => 4,
            ),
            222 => 
            array (
                'id_reg' => 1723,
                'id_model' => 1432,
                'id_fuel' => 2,
            ),
            223 => 
            array (
                'id_reg' => 1724,
                'id_model' => 1432,
                'id_fuel' => 4,
            ),
            224 => 
            array (
                'id_reg' => 1725,
                'id_model' => 1433,
                'id_fuel' => 4,
            ),
            225 => 
            array (
                'id_reg' => 1726,
                'id_model' => 1434,
                'id_fuel' => 5,
            ),
            226 => 
            array (
                'id_reg' => 1727,
                'id_model' => 1435,
                'id_fuel' => 4,
            ),
            227 => 
            array (
                'id_reg' => 1728,
                'id_model' => 1436,
                'id_fuel' => 2,
            ),
            228 => 
            array (
                'id_reg' => 1729,
                'id_model' => 1436,
                'id_fuel' => 4,
            ),
            229 => 
            array (
                'id_reg' => 1730,
                'id_model' => 1436,
                'id_fuel' => 5,
            ),
            230 => 
            array (
                'id_reg' => 1731,
                'id_model' => 1437,
                'id_fuel' => 2,
            ),
            231 => 
            array (
                'id_reg' => 1732,
                'id_model' => 1438,
                'id_fuel' => 4,
            ),
            232 => 
            array (
                'id_reg' => 1733,
                'id_model' => 1439,
                'id_fuel' => 2,
            ),
            233 => 
            array (
                'id_reg' => 1734,
                'id_model' => 1439,
                'id_fuel' => 4,
            ),
            234 => 
            array (
                'id_reg' => 1735,
                'id_model' => 1440,
                'id_fuel' => 2,
            ),
            235 => 
            array (
                'id_reg' => 1736,
                'id_model' => 1440,
                'id_fuel' => 4,
            ),
            236 => 
            array (
                'id_reg' => 1737,
                'id_model' => 1441,
                'id_fuel' => 4,
            ),
            237 => 
            array (
                'id_reg' => 1738,
                'id_model' => 1442,
                'id_fuel' => 4,
            ),
            238 => 
            array (
                'id_reg' => 1739,
                'id_model' => 1443,
                'id_fuel' => 4,
            ),
            239 => 
            array (
                'id_reg' => 1740,
                'id_model' => 1444,
                'id_fuel' => 2,
            ),
            240 => 
            array (
                'id_reg' => 1741,
                'id_model' => 1444,
                'id_fuel' => 4,
            ),
            241 => 
            array (
                'id_reg' => 1742,
                'id_model' => 1445,
                'id_fuel' => 4,
            ),
            242 => 
            array (
                'id_reg' => 1743,
                'id_model' => 1446,
                'id_fuel' => 4,
            ),
            243 => 
            array (
                'id_reg' => 1744,
                'id_model' => 1447,
                'id_fuel' => 4,
            ),
            244 => 
            array (
                'id_reg' => 1745,
                'id_model' => 1448,
                'id_fuel' => 4,
            ),
            245 => 
            array (
                'id_reg' => 1746,
                'id_model' => 1449,
                'id_fuel' => 4,
            ),
            246 => 
            array (
                'id_reg' => 1747,
                'id_model' => 1450,
                'id_fuel' => 4,
            ),
            247 => 
            array (
                'id_reg' => 1748,
                'id_model' => 1451,
                'id_fuel' => 4,
            ),
            248 => 
            array (
                'id_reg' => 1749,
                'id_model' => 1452,
                'id_fuel' => 4,
            ),
            249 => 
            array (
                'id_reg' => 1750,
                'id_model' => 1453,
                'id_fuel' => 2,
            ),
            250 => 
            array (
                'id_reg' => 1751,
                'id_model' => 1454,
                'id_fuel' => 2,
            ),
            251 => 
            array (
                'id_reg' => 1752,
                'id_model' => 1455,
                'id_fuel' => 2,
            ),
            252 => 
            array (
                'id_reg' => 1753,
                'id_model' => 1456,
                'id_fuel' => 2,
            ),
            253 => 
            array (
                'id_reg' => 1754,
                'id_model' => 1457,
                'id_fuel' => 2,
            ),
            254 => 
            array (
                'id_reg' => 1755,
                'id_model' => 1458,
                'id_fuel' => 2,
            ),
            255 => 
            array (
                'id_reg' => 1756,
                'id_model' => 1459,
                'id_fuel' => 2,
            ),
            256 => 
            array (
                'id_reg' => 1757,
                'id_model' => 1460,
                'id_fuel' => 4,
            ),
            257 => 
            array (
                'id_reg' => 1758,
                'id_model' => 1461,
                'id_fuel' => 4,
            ),
            258 => 
            array (
                'id_reg' => 1759,
                'id_model' => 1462,
                'id_fuel' => 4,
            ),
            259 => 
            array (
                'id_reg' => 1760,
                'id_model' => 1463,
                'id_fuel' => 4,
            ),
            260 => 
            array (
                'id_reg' => 1761,
                'id_model' => 1464,
                'id_fuel' => 4,
            ),
            261 => 
            array (
                'id_reg' => 1762,
                'id_model' => 1465,
                'id_fuel' => 4,
            ),
            262 => 
            array (
                'id_reg' => 1763,
                'id_model' => 1466,
                'id_fuel' => 4,
            ),
            263 => 
            array (
                'id_reg' => 1764,
                'id_model' => 1467,
                'id_fuel' => 4,
            ),
            264 => 
            array (
                'id_reg' => 1765,
                'id_model' => 1468,
                'id_fuel' => 4,
            ),
            265 => 
            array (
                'id_reg' => 1766,
                'id_model' => 1469,
                'id_fuel' => 4,
            ),
            266 => 
            array (
                'id_reg' => 1767,
                'id_model' => 1470,
                'id_fuel' => 4,
            ),
            267 => 
            array (
                'id_reg' => 1768,
                'id_model' => 1471,
                'id_fuel' => 4,
            ),
            268 => 
            array (
                'id_reg' => 1769,
                'id_model' => 1472,
                'id_fuel' => 2,
            ),
            269 => 
            array (
                'id_reg' => 1770,
                'id_model' => 1472,
                'id_fuel' => 4,
            ),
            270 => 
            array (
                'id_reg' => 1771,
                'id_model' => 1473,
                'id_fuel' => 4,
            ),
            271 => 
            array (
                'id_reg' => 1772,
                'id_model' => 1474,
                'id_fuel' => 4,
            ),
            272 => 
            array (
                'id_reg' => 1773,
                'id_model' => 1475,
                'id_fuel' => 4,
            ),
            273 => 
            array (
                'id_reg' => 1774,
                'id_model' => 1476,
                'id_fuel' => 4,
            ),
            274 => 
            array (
                'id_reg' => 1775,
                'id_model' => 1477,
                'id_fuel' => 4,
            ),
            275 => 
            array (
                'id_reg' => 1776,
                'id_model' => 1478,
                'id_fuel' => 4,
            ),
            276 => 
            array (
                'id_reg' => 1777,
                'id_model' => 1479,
                'id_fuel' => 4,
            ),
            277 => 
            array (
                'id_reg' => 1778,
                'id_model' => 1480,
                'id_fuel' => 4,
            ),
            278 => 
            array (
                'id_reg' => 1779,
                'id_model' => 1481,
                'id_fuel' => 4,
            ),
            279 => 
            array (
                'id_reg' => 1780,
                'id_model' => 1482,
                'id_fuel' => 4,
            ),
            280 => 
            array (
                'id_reg' => 1781,
                'id_model' => 1483,
                'id_fuel' => 4,
            ),
            281 => 
            array (
                'id_reg' => 1782,
                'id_model' => 1484,
                'id_fuel' => 4,
            ),
            282 => 
            array (
                'id_reg' => 1783,
                'id_model' => 1485,
                'id_fuel' => 4,
            ),
            283 => 
            array (
                'id_reg' => 1784,
                'id_model' => 1486,
                'id_fuel' => 4,
            ),
            284 => 
            array (
                'id_reg' => 1785,
                'id_model' => 1487,
                'id_fuel' => 6,
            ),
            285 => 
            array (
                'id_reg' => 1786,
                'id_model' => 1488,
                'id_fuel' => 4,
            ),
            286 => 
            array (
                'id_reg' => 1787,
                'id_model' => 1489,
                'id_fuel' => 4,
            ),
            287 => 
            array (
                'id_reg' => 1788,
                'id_model' => 1490,
                'id_fuel' => 4,
            ),
            288 => 
            array (
                'id_reg' => 1789,
                'id_model' => 1491,
                'id_fuel' => 4,
            ),
            289 => 
            array (
                'id_reg' => 1790,
                'id_model' => 1492,
                'id_fuel' => 4,
            ),
            290 => 
            array (
                'id_reg' => 1791,
                'id_model' => 1493,
                'id_fuel' => 4,
            ),
            291 => 
            array (
                'id_reg' => 1792,
                'id_model' => 1494,
                'id_fuel' => 4,
            ),
            292 => 
            array (
                'id_reg' => 1793,
                'id_model' => 1495,
                'id_fuel' => 4,
            ),
            293 => 
            array (
                'id_reg' => 1794,
                'id_model' => 1496,
                'id_fuel' => 4,
            ),
            294 => 
            array (
                'id_reg' => 1795,
                'id_model' => 1497,
                'id_fuel' => 4,
            ),
            295 => 
            array (
                'id_reg' => 1796,
                'id_model' => 1498,
                'id_fuel' => 4,
            ),
            296 => 
            array (
                'id_reg' => 1797,
                'id_model' => 1499,
                'id_fuel' => 4,
            ),
            297 => 
            array (
                'id_reg' => 1798,
                'id_model' => 1500,
                'id_fuel' => 2,
            ),
            298 => 
            array (
                'id_reg' => 1799,
                'id_model' => 1500,
                'id_fuel' => 4,
            ),
            299 => 
            array (
                'id_reg' => 1800,
                'id_model' => 1501,
                'id_fuel' => 2,
            ),
            300 => 
            array (
                'id_reg' => 1801,
                'id_model' => 1501,
                'id_fuel' => 3,
            ),
            301 => 
            array (
                'id_reg' => 1802,
                'id_model' => 1501,
                'id_fuel' => 4,
            ),
            302 => 
            array (
                'id_reg' => 1803,
                'id_model' => 1502,
                'id_fuel' => 4,
            ),
            303 => 
            array (
                'id_reg' => 1804,
                'id_model' => 1503,
                'id_fuel' => 4,
            ),
            304 => 
            array (
                'id_reg' => 1805,
                'id_model' => 1504,
                'id_fuel' => 4,
            ),
            305 => 
            array (
                'id_reg' => 1806,
                'id_model' => 1505,
                'id_fuel' => 4,
            ),
            306 => 
            array (
                'id_reg' => 1807,
                'id_model' => 1506,
                'id_fuel' => 4,
            ),
            307 => 
            array (
                'id_reg' => 1808,
                'id_model' => 1507,
                'id_fuel' => 4,
            ),
            308 => 
            array (
                'id_reg' => 1809,
                'id_model' => 1508,
                'id_fuel' => 2,
            ),
            309 => 
            array (
                'id_reg' => 1810,
                'id_model' => 1508,
                'id_fuel' => 4,
            ),
            310 => 
            array (
                'id_reg' => 1811,
                'id_model' => 1509,
                'id_fuel' => 2,
            ),
            311 => 
            array (
                'id_reg' => 1812,
                'id_model' => 1509,
                'id_fuel' => 4,
            ),
            312 => 
            array (
                'id_reg' => 1813,
                'id_model' => 1510,
                'id_fuel' => 4,
            ),
            313 => 
            array (
                'id_reg' => 1814,
                'id_model' => 1511,
                'id_fuel' => 2,
            ),
            314 => 
            array (
                'id_reg' => 1815,
                'id_model' => 1511,
                'id_fuel' => 4,
            ),
            315 => 
            array (
                'id_reg' => 1816,
                'id_model' => 1512,
                'id_fuel' => 2,
            ),
            316 => 
            array (
                'id_reg' => 1817,
                'id_model' => 1512,
                'id_fuel' => 4,
            ),
            317 => 
            array (
                'id_reg' => 1818,
                'id_model' => 1513,
                'id_fuel' => 2,
            ),
            318 => 
            array (
                'id_reg' => 1819,
                'id_model' => 1513,
                'id_fuel' => 4,
            ),
            319 => 
            array (
                'id_reg' => 1820,
                'id_model' => 1514,
                'id_fuel' => 4,
            ),
            320 => 
            array (
                'id_reg' => 1821,
                'id_model' => 1515,
                'id_fuel' => 2,
            ),
            321 => 
            array (
                'id_reg' => 1822,
                'id_model' => 1515,
                'id_fuel' => 4,
            ),
            322 => 
            array (
                'id_reg' => 1823,
                'id_model' => 1516,
                'id_fuel' => 2,
            ),
            323 => 
            array (
                'id_reg' => 1824,
                'id_model' => 1516,
                'id_fuel' => 4,
            ),
            324 => 
            array (
                'id_reg' => 1825,
                'id_model' => 1517,
                'id_fuel' => 4,
            ),
            325 => 
            array (
                'id_reg' => 1826,
                'id_model' => 1518,
                'id_fuel' => 2,
            ),
            326 => 
            array (
                'id_reg' => 1827,
                'id_model' => 1519,
                'id_fuel' => 4,
            ),
            327 => 
            array (
                'id_reg' => 1828,
                'id_model' => 1520,
                'id_fuel' => 4,
            ),
            328 => 
            array (
                'id_reg' => 1829,
                'id_model' => 1521,
                'id_fuel' => 2,
            ),
            329 => 
            array (
                'id_reg' => 1830,
                'id_model' => 1521,
                'id_fuel' => 3,
            ),
            330 => 
            array (
                'id_reg' => 1831,
                'id_model' => 1521,
                'id_fuel' => 4,
            ),
            331 => 
            array (
                'id_reg' => 1832,
                'id_model' => 1522,
                'id_fuel' => 2,
            ),
            332 => 
            array (
                'id_reg' => 1833,
                'id_model' => 1522,
                'id_fuel' => 4,
            ),
            333 => 
            array (
                'id_reg' => 1834,
                'id_model' => 1523,
                'id_fuel' => 2,
            ),
            334 => 
            array (
                'id_reg' => 1835,
                'id_model' => 1524,
                'id_fuel' => 4,
            ),
            335 => 
            array (
                'id_reg' => 1836,
                'id_model' => 1525,
                'id_fuel' => 2,
            ),
            336 => 
            array (
                'id_reg' => 1837,
                'id_model' => 1525,
                'id_fuel' => 4,
            ),
            337 => 
            array (
                'id_reg' => 1838,
                'id_model' => 1526,
                'id_fuel' => 4,
            ),
            338 => 
            array (
                'id_reg' => 1839,
                'id_model' => 1527,
                'id_fuel' => 2,
            ),
            339 => 
            array (
                'id_reg' => 1840,
                'id_model' => 1527,
                'id_fuel' => 4,
            ),
            340 => 
            array (
                'id_reg' => 1841,
                'id_model' => 1528,
                'id_fuel' => 2,
            ),
            341 => 
            array (
                'id_reg' => 1842,
                'id_model' => 1528,
                'id_fuel' => 4,
            ),
            342 => 
            array (
                'id_reg' => 1843,
                'id_model' => 1529,
                'id_fuel' => 2,
            ),
            343 => 
            array (
                'id_reg' => 1844,
                'id_model' => 1529,
                'id_fuel' => 4,
            ),
            344 => 
            array (
                'id_reg' => 1845,
                'id_model' => 1530,
                'id_fuel' => 2,
            ),
            345 => 
            array (
                'id_reg' => 1846,
                'id_model' => 1530,
                'id_fuel' => 4,
            ),
            346 => 
            array (
                'id_reg' => 1847,
                'id_model' => 1531,
                'id_fuel' => 2,
            ),
            347 => 
            array (
                'id_reg' => 1848,
                'id_model' => 1531,
                'id_fuel' => 4,
            ),
            348 => 
            array (
                'id_reg' => 1849,
                'id_model' => 1532,
                'id_fuel' => 2,
            ),
            349 => 
            array (
                'id_reg' => 1850,
                'id_model' => 1532,
                'id_fuel' => 4,
            ),
            350 => 
            array (
                'id_reg' => 1851,
                'id_model' => 1533,
                'id_fuel' => 2,
            ),
            351 => 
            array (
                'id_reg' => 1852,
                'id_model' => 1533,
                'id_fuel' => 4,
            ),
            352 => 
            array (
                'id_reg' => 1853,
                'id_model' => 1534,
                'id_fuel' => 4,
            ),
            353 => 
            array (
                'id_reg' => 1854,
                'id_model' => 1535,
                'id_fuel' => 6,
            ),
            354 => 
            array (
                'id_reg' => 1855,
                'id_model' => 1536,
                'id_fuel' => 6,
            ),
            355 => 
            array (
                'id_reg' => 1856,
                'id_model' => 1537,
                'id_fuel' => 6,
            ),
            356 => 
            array (
                'id_reg' => 1857,
                'id_model' => 1538,
                'id_fuel' => 4,
            ),
            357 => 
            array (
                'id_reg' => 1858,
                'id_model' => 1539,
                'id_fuel' => 4,
            ),
            358 => 
            array (
                'id_reg' => 1859,
                'id_model' => 1540,
                'id_fuel' => 4,
            ),
            359 => 
            array (
                'id_reg' => 1860,
                'id_model' => 1541,
                'id_fuel' => 2,
            ),
            360 => 
            array (
                'id_reg' => 1861,
                'id_model' => 1542,
                'id_fuel' => 5,
            ),
            361 => 
            array (
                'id_reg' => 1862,
                'id_model' => 1543,
                'id_fuel' => 4,
            ),
            362 => 
            array (
                'id_reg' => 1863,
                'id_model' => 1544,
                'id_fuel' => 4,
            ),
            363 => 
            array (
                'id_reg' => 1864,
                'id_model' => 1545,
                'id_fuel' => 4,
            ),
            364 => 
            array (
                'id_reg' => 1865,
                'id_model' => 1546,
                'id_fuel' => 4,
            ),
            365 => 
            array (
                'id_reg' => 1866,
                'id_model' => 1547,
                'id_fuel' => 4,
            ),
            366 => 
            array (
                'id_reg' => 1867,
                'id_model' => 1548,
                'id_fuel' => 4,
            ),
            367 => 
            array (
                'id_reg' => 1868,
                'id_model' => 1549,
                'id_fuel' => 4,
            ),
            368 => 
            array (
                'id_reg' => 1869,
                'id_model' => 1550,
                'id_fuel' => 5,
            ),
            369 => 
            array (
                'id_reg' => 1870,
                'id_model' => 1551,
                'id_fuel' => 4,
            ),
            370 => 
            array (
                'id_reg' => 1871,
                'id_model' => 1552,
                'id_fuel' => 4,
            ),
            371 => 
            array (
                'id_reg' => 1872,
                'id_model' => 1553,
                'id_fuel' => 4,
            ),
            372 => 
            array (
                'id_reg' => 1873,
                'id_model' => 1554,
                'id_fuel' => 5,
            ),
            373 => 
            array (
                'id_reg' => 1874,
                'id_model' => 1555,
                'id_fuel' => 4,
            ),
            374 => 
            array (
                'id_reg' => 1875,
                'id_model' => 1556,
                'id_fuel' => 4,
            ),
            375 => 
            array (
                'id_reg' => 1876,
                'id_model' => 1557,
                'id_fuel' => 4,
            ),
            376 => 
            array (
                'id_reg' => 1877,
                'id_model' => 1558,
                'id_fuel' => 4,
            ),
            377 => 
            array (
                'id_reg' => 1878,
                'id_model' => 1559,
                'id_fuel' => 4,
            ),
            378 => 
            array (
                'id_reg' => 1879,
                'id_model' => 1560,
                'id_fuel' => 2,
            ),
            379 => 
            array (
                'id_reg' => 1880,
                'id_model' => 1561,
                'id_fuel' => 2,
            ),
            380 => 
            array (
                'id_reg' => 1881,
                'id_model' => 1562,
                'id_fuel' => 4,
            ),
            381 => 
            array (
                'id_reg' => 1882,
                'id_model' => 1563,
                'id_fuel' => 4,
            ),
            382 => 
            array (
                'id_reg' => 1883,
                'id_model' => 1564,
                'id_fuel' => 5,
            ),
            383 => 
            array (
                'id_reg' => 1884,
                'id_model' => 1565,
                'id_fuel' => 4,
            ),
            384 => 
            array (
                'id_reg' => 1885,
                'id_model' => 1566,
                'id_fuel' => 4,
            ),
            385 => 
            array (
                'id_reg' => 1886,
                'id_model' => 1567,
                'id_fuel' => 4,
            ),
            386 => 
            array (
                'id_reg' => 1887,
                'id_model' => 1568,
                'id_fuel' => 5,
            ),
            387 => 
            array (
                'id_reg' => 1888,
                'id_model' => 1569,
                'id_fuel' => 4,
            ),
            388 => 
            array (
                'id_reg' => 1889,
                'id_model' => 1570,
                'id_fuel' => 4,
            ),
            389 => 
            array (
                'id_reg' => 1890,
                'id_model' => 1571,
                'id_fuel' => 4,
            ),
            390 => 
            array (
                'id_reg' => 1891,
                'id_model' => 1572,
                'id_fuel' => 4,
            ),
            391 => 
            array (
                'id_reg' => 1892,
                'id_model' => 1573,
                'id_fuel' => 5,
            ),
            392 => 
            array (
                'id_reg' => 1893,
                'id_model' => 1574,
                'id_fuel' => 4,
            ),
            393 => 
            array (
                'id_reg' => 1894,
                'id_model' => 1575,
                'id_fuel' => 4,
            ),
            394 => 
            array (
                'id_reg' => 1895,
                'id_model' => 1576,
                'id_fuel' => 2,
            ),
            395 => 
            array (
                'id_reg' => 1896,
                'id_model' => 1576,
                'id_fuel' => 4,
            ),
            396 => 
            array (
                'id_reg' => 1897,
                'id_model' => 1577,
                'id_fuel' => 4,
            ),
            397 => 
            array (
                'id_reg' => 1898,
                'id_model' => 1578,
                'id_fuel' => 5,
            ),
            398 => 
            array (
                'id_reg' => 1899,
                'id_model' => 1579,
                'id_fuel' => 5,
            ),
            399 => 
            array (
                'id_reg' => 1900,
                'id_model' => 1580,
                'id_fuel' => 4,
            ),
            400 => 
            array (
                'id_reg' => 1901,
                'id_model' => 1581,
                'id_fuel' => 4,
            ),
            401 => 
            array (
                'id_reg' => 1902,
                'id_model' => 1582,
                'id_fuel' => 4,
            ),
            402 => 
            array (
                'id_reg' => 1903,
                'id_model' => 1583,
                'id_fuel' => 4,
            ),
            403 => 
            array (
                'id_reg' => 1904,
                'id_model' => 1584,
                'id_fuel' => 5,
            ),
            404 => 
            array (
                'id_reg' => 1905,
                'id_model' => 1585,
                'id_fuel' => 5,
            ),
            405 => 
            array (
                'id_reg' => 1906,
                'id_model' => 1586,
                'id_fuel' => 4,
            ),
            406 => 
            array (
                'id_reg' => 1907,
                'id_model' => 1587,
                'id_fuel' => 4,
            ),
            407 => 
            array (
                'id_reg' => 1908,
                'id_model' => 1588,
                'id_fuel' => 4,
            ),
            408 => 
            array (
                'id_reg' => 1909,
                'id_model' => 1589,
                'id_fuel' => 4,
            ),
            409 => 
            array (
                'id_reg' => 1910,
                'id_model' => 1590,
                'id_fuel' => 4,
            ),
            410 => 
            array (
                'id_reg' => 1911,
                'id_model' => 1591,
                'id_fuel' => 4,
            ),
            411 => 
            array (
                'id_reg' => 1912,
                'id_model' => 1592,
                'id_fuel' => 4,
            ),
            412 => 
            array (
                'id_reg' => 1913,
                'id_model' => 1593,
                'id_fuel' => 2,
            ),
            413 => 
            array (
                'id_reg' => 1914,
                'id_model' => 1594,
                'id_fuel' => 2,
            ),
            414 => 
            array (
                'id_reg' => 1915,
                'id_model' => 1594,
                'id_fuel' => 4,
            ),
            415 => 
            array (
                'id_reg' => 1916,
                'id_model' => 1595,
                'id_fuel' => 6,
            ),
            416 => 
            array (
                'id_reg' => 1917,
                'id_model' => 1596,
                'id_fuel' => 4,
            ),
            417 => 
            array (
                'id_reg' => 1918,
                'id_model' => 1597,
                'id_fuel' => 2,
            ),
            418 => 
            array (
                'id_reg' => 1919,
                'id_model' => 1598,
                'id_fuel' => 4,
            ),
            419 => 
            array (
                'id_reg' => 1920,
                'id_model' => 1599,
                'id_fuel' => 2,
            ),
            420 => 
            array (
                'id_reg' => 1921,
                'id_model' => 1600,
                'id_fuel' => 2,
            ),
            421 => 
            array (
                'id_reg' => 1922,
                'id_model' => 1601,
                'id_fuel' => 2,
            ),
            422 => 
            array (
                'id_reg' => 1923,
                'id_model' => 1602,
                'id_fuel' => 2,
            ),
            423 => 
            array (
                'id_reg' => 1924,
                'id_model' => 1603,
                'id_fuel' => 2,
            ),
            424 => 
            array (
                'id_reg' => 1925,
                'id_model' => 1604,
                'id_fuel' => 2,
            ),
            425 => 
            array (
                'id_reg' => 1926,
                'id_model' => 1605,
                'id_fuel' => 2,
            ),
            426 => 
            array (
                'id_reg' => 1927,
                'id_model' => 1606,
                'id_fuel' => 2,
            ),
            427 => 
            array (
                'id_reg' => 1928,
                'id_model' => 1607,
                'id_fuel' => 2,
            ),
            428 => 
            array (
                'id_reg' => 1929,
                'id_model' => 1608,
                'id_fuel' => 2,
            ),
            429 => 
            array (
                'id_reg' => 1930,
                'id_model' => 1609,
                'id_fuel' => 2,
            ),
            430 => 
            array (
                'id_reg' => 1931,
                'id_model' => 1609,
                'id_fuel' => 4,
            ),
            431 => 
            array (
                'id_reg' => 1932,
                'id_model' => 1610,
                'id_fuel' => 2,
            ),
            432 => 
            array (
                'id_reg' => 1933,
                'id_model' => 1611,
                'id_fuel' => 4,
            ),
            433 => 
            array (
                'id_reg' => 1934,
                'id_model' => 1612,
                'id_fuel' => 4,
            ),
            434 => 
            array (
                'id_reg' => 1935,
                'id_model' => 1613,
                'id_fuel' => 4,
            ),
            435 => 
            array (
                'id_reg' => 1936,
                'id_model' => 1614,
                'id_fuel' => 4,
            ),
            436 => 
            array (
                'id_reg' => 1937,
                'id_model' => 1615,
                'id_fuel' => 4,
            ),
            437 => 
            array (
                'id_reg' => 1938,
                'id_model' => 1616,
                'id_fuel' => 4,
            ),
            438 => 
            array (
                'id_reg' => 1939,
                'id_model' => 1617,
                'id_fuel' => 4,
            ),
            439 => 
            array (
                'id_reg' => 1940,
                'id_model' => 1618,
                'id_fuel' => 4,
            ),
            440 => 
            array (
                'id_reg' => 1941,
                'id_model' => 1619,
                'id_fuel' => 4,
            ),
            441 => 
            array (
                'id_reg' => 1942,
                'id_model' => 1620,
                'id_fuel' => 4,
            ),
            442 => 
            array (
                'id_reg' => 1943,
                'id_model' => 1621,
                'id_fuel' => 4,
            ),
            443 => 
            array (
                'id_reg' => 1944,
                'id_model' => 1622,
                'id_fuel' => 4,
            ),
            444 => 
            array (
                'id_reg' => 1945,
                'id_model' => 1623,
                'id_fuel' => 4,
            ),
            445 => 
            array (
                'id_reg' => 1946,
                'id_model' => 1624,
                'id_fuel' => 4,
            ),
            446 => 
            array (
                'id_reg' => 1947,
                'id_model' => 1625,
                'id_fuel' => 4,
            ),
            447 => 
            array (
                'id_reg' => 1948,
                'id_model' => 1626,
                'id_fuel' => 4,
            ),
            448 => 
            array (
                'id_reg' => 1949,
                'id_model' => 1627,
                'id_fuel' => 4,
            ),
            449 => 
            array (
                'id_reg' => 1950,
                'id_model' => 1628,
                'id_fuel' => 4,
            ),
            450 => 
            array (
                'id_reg' => 1951,
                'id_model' => 1629,
                'id_fuel' => 4,
            ),
            451 => 
            array (
                'id_reg' => 1952,
                'id_model' => 1630,
                'id_fuel' => 4,
            ),
            452 => 
            array (
                'id_reg' => 1953,
                'id_model' => 1631,
                'id_fuel' => 4,
            ),
            453 => 
            array (
                'id_reg' => 1954,
                'id_model' => 1632,
                'id_fuel' => 4,
            ),
            454 => 
            array (
                'id_reg' => 1955,
                'id_model' => 1633,
                'id_fuel' => 4,
            ),
            455 => 
            array (
                'id_reg' => 1956,
                'id_model' => 1634,
                'id_fuel' => 4,
            ),
            456 => 
            array (
                'id_reg' => 1957,
                'id_model' => 1635,
                'id_fuel' => 4,
            ),
            457 => 
            array (
                'id_reg' => 1958,
                'id_model' => 1636,
                'id_fuel' => 6,
            ),
            458 => 
            array (
                'id_reg' => 1959,
                'id_model' => 1637,
                'id_fuel' => 4,
            ),
            459 => 
            array (
                'id_reg' => 1960,
                'id_model' => 1638,
                'id_fuel' => 4,
            ),
            460 => 
            array (
                'id_reg' => 1961,
                'id_model' => 1639,
                'id_fuel' => 4,
            ),
            461 => 
            array (
                'id_reg' => 1962,
                'id_model' => 1640,
                'id_fuel' => 4,
            ),
            462 => 
            array (
                'id_reg' => 1963,
                'id_model' => 1641,
                'id_fuel' => 4,
            ),
            463 => 
            array (
                'id_reg' => 1964,
                'id_model' => 1642,
                'id_fuel' => 4,
            ),
            464 => 
            array (
                'id_reg' => 1965,
                'id_model' => 1643,
                'id_fuel' => 4,
            ),
            465 => 
            array (
                'id_reg' => 1966,
                'id_model' => 1644,
                'id_fuel' => 4,
            ),
            466 => 
            array (
                'id_reg' => 1967,
                'id_model' => 1645,
                'id_fuel' => 4,
            ),
            467 => 
            array (
                'id_reg' => 1968,
                'id_model' => 1646,
                'id_fuel' => 4,
            ),
            468 => 
            array (
                'id_reg' => 1969,
                'id_model' => 1647,
                'id_fuel' => 2,
            ),
            469 => 
            array (
                'id_reg' => 1970,
                'id_model' => 1648,
                'id_fuel' => 2,
            ),
            470 => 
            array (
                'id_reg' => 1971,
                'id_model' => 1649,
                'id_fuel' => 2,
            ),
            471 => 
            array (
                'id_reg' => 1972,
                'id_model' => 1650,
                'id_fuel' => 2,
            ),
            472 => 
            array (
                'id_reg' => 1973,
                'id_model' => 1651,
                'id_fuel' => 2,
            ),
            473 => 
            array (
                'id_reg' => 1974,
                'id_model' => 1652,
                'id_fuel' => 4,
            ),
            474 => 
            array (
                'id_reg' => 1975,
                'id_model' => 1653,
                'id_fuel' => 2,
            ),
            475 => 
            array (
                'id_reg' => 1976,
                'id_model' => 1653,
                'id_fuel' => 4,
            ),
            476 => 
            array (
                'id_reg' => 1977,
                'id_model' => 1654,
                'id_fuel' => 2,
            ),
            477 => 
            array (
                'id_reg' => 1978,
                'id_model' => 1654,
                'id_fuel' => 4,
            ),
            478 => 
            array (
                'id_reg' => 1979,
                'id_model' => 1655,
                'id_fuel' => 2,
            ),
            479 => 
            array (
                'id_reg' => 1980,
                'id_model' => 1656,
                'id_fuel' => 2,
            ),
            480 => 
            array (
                'id_reg' => 1981,
                'id_model' => 1657,
                'id_fuel' => 2,
            ),
            481 => 
            array (
                'id_reg' => 1982,
                'id_model' => 1658,
                'id_fuel' => 2,
            ),
            482 => 
            array (
                'id_reg' => 1983,
                'id_model' => 1659,
                'id_fuel' => 2,
            ),
            483 => 
            array (
                'id_reg' => 1984,
                'id_model' => 1660,
                'id_fuel' => 2,
            ),
            484 => 
            array (
                'id_reg' => 1985,
                'id_model' => 1661,
                'id_fuel' => 2,
            ),
            485 => 
            array (
                'id_reg' => 1986,
                'id_model' => 1662,
                'id_fuel' => 2,
            ),
            486 => 
            array (
                'id_reg' => 1987,
                'id_model' => 1663,
                'id_fuel' => 2,
            ),
            487 => 
            array (
                'id_reg' => 1988,
                'id_model' => 1664,
                'id_fuel' => 4,
            ),
            488 => 
            array (
                'id_reg' => 1989,
                'id_model' => 1665,
                'id_fuel' => 4,
            ),
            489 => 
            array (
                'id_reg' => 1990,
                'id_model' => 1666,
                'id_fuel' => 4,
            ),
            490 => 
            array (
                'id_reg' => 1991,
                'id_model' => 1667,
                'id_fuel' => 4,
            ),
            491 => 
            array (
                'id_reg' => 1992,
                'id_model' => 1668,
                'id_fuel' => 4,
            ),
            492 => 
            array (
                'id_reg' => 1993,
                'id_model' => 1669,
                'id_fuel' => 4,
            ),
            493 => 
            array (
                'id_reg' => 1994,
                'id_model' => 1670,
                'id_fuel' => 2,
            ),
            494 => 
            array (
                'id_reg' => 1995,
                'id_model' => 1671,
                'id_fuel' => 2,
            ),
            495 => 
            array (
                'id_reg' => 1996,
                'id_model' => 1672,
                'id_fuel' => 4,
            ),
            496 => 
            array (
                'id_reg' => 1997,
                'id_model' => 1673,
                'id_fuel' => 4,
            ),
            497 => 
            array (
                'id_reg' => 1998,
                'id_model' => 1674,
                'id_fuel' => 4,
            ),
            498 => 
            array (
                'id_reg' => 1999,
                'id_model' => 1675,
                'id_fuel' => 4,
            ),
            499 => 
            array (
                'id_reg' => 2000,
                'id_model' => 1676,
                'id_fuel' => 4,
            ),
        ));
        \DB::table('model_rel_fuel')->insert(array (
            0 => 
            array (
                'id_reg' => 2001,
                'id_model' => 1677,
                'id_fuel' => 4,
            ),
            1 => 
            array (
                'id_reg' => 2002,
                'id_model' => 1678,
                'id_fuel' => 4,
            ),
            2 => 
            array (
                'id_reg' => 2003,
                'id_model' => 1679,
                'id_fuel' => 4,
            ),
            3 => 
            array (
                'id_reg' => 2004,
                'id_model' => 1680,
                'id_fuel' => 4,
            ),
            4 => 
            array (
                'id_reg' => 2005,
                'id_model' => 1681,
                'id_fuel' => 4,
            ),
            5 => 
            array (
                'id_reg' => 2006,
                'id_model' => 1682,
                'id_fuel' => 4,
            ),
            6 => 
            array (
                'id_reg' => 2007,
                'id_model' => 1683,
                'id_fuel' => 4,
            ),
            7 => 
            array (
                'id_reg' => 2008,
                'id_model' => 1684,
                'id_fuel' => 4,
            ),
            8 => 
            array (
                'id_reg' => 2009,
                'id_model' => 1685,
                'id_fuel' => 4,
            ),
            9 => 
            array (
                'id_reg' => 2010,
                'id_model' => 1686,
                'id_fuel' => 4,
            ),
            10 => 
            array (
                'id_reg' => 2011,
                'id_model' => 1687,
                'id_fuel' => 4,
            ),
            11 => 
            array (
                'id_reg' => 2012,
                'id_model' => 1688,
                'id_fuel' => 2,
            ),
            12 => 
            array (
                'id_reg' => 2013,
                'id_model' => 1688,
                'id_fuel' => 4,
            ),
            13 => 
            array (
                'id_reg' => 2014,
                'id_model' => 1689,
                'id_fuel' => 4,
            ),
            14 => 
            array (
                'id_reg' => 2015,
                'id_model' => 1690,
                'id_fuel' => 4,
            ),
            15 => 
            array (
                'id_reg' => 2016,
                'id_model' => 1691,
                'id_fuel' => 4,
            ),
            16 => 
            array (
                'id_reg' => 2017,
                'id_model' => 1692,
                'id_fuel' => 4,
            ),
            17 => 
            array (
                'id_reg' => 2018,
                'id_model' => 1693,
                'id_fuel' => 4,
            ),
            18 => 
            array (
                'id_reg' => 2019,
                'id_model' => 1694,
                'id_fuel' => 2,
            ),
            19 => 
            array (
                'id_reg' => 2020,
                'id_model' => 1694,
                'id_fuel' => 4,
            ),
            20 => 
            array (
                'id_reg' => 2021,
                'id_model' => 1695,
                'id_fuel' => 4,
            ),
            21 => 
            array (
                'id_reg' => 2022,
                'id_model' => 1696,
                'id_fuel' => 4,
            ),
            22 => 
            array (
                'id_reg' => 2023,
                'id_model' => 1697,
                'id_fuel' => 2,
            ),
            23 => 
            array (
                'id_reg' => 2024,
                'id_model' => 1697,
                'id_fuel' => 4,
            ),
            24 => 
            array (
                'id_reg' => 2025,
                'id_model' => 1698,
                'id_fuel' => 4,
            ),
            25 => 
            array (
                'id_reg' => 2026,
                'id_model' => 1699,
                'id_fuel' => 4,
            ),
            26 => 
            array (
                'id_reg' => 2027,
                'id_model' => 1700,
                'id_fuel' => 4,
            ),
            27 => 
            array (
                'id_reg' => 2028,
                'id_model' => 1701,
                'id_fuel' => 4,
            ),
            28 => 
            array (
                'id_reg' => 2029,
                'id_model' => 1702,
                'id_fuel' => 4,
            ),
            29 => 
            array (
                'id_reg' => 2030,
                'id_model' => 1703,
                'id_fuel' => 2,
            ),
            30 => 
            array (
                'id_reg' => 2031,
                'id_model' => 1703,
                'id_fuel' => 4,
            ),
            31 => 
            array (
                'id_reg' => 2032,
                'id_model' => 1704,
                'id_fuel' => 2,
            ),
            32 => 
            array (
                'id_reg' => 2033,
                'id_model' => 1705,
                'id_fuel' => 4,
            ),
            33 => 
            array (
                'id_reg' => 2034,
                'id_model' => 1706,
                'id_fuel' => 4,
            ),
            34 => 
            array (
                'id_reg' => 2035,
                'id_model' => 1707,
                'id_fuel' => 4,
            ),
            35 => 
            array (
                'id_reg' => 2036,
                'id_model' => 1708,
                'id_fuel' => 4,
            ),
            36 => 
            array (
                'id_reg' => 2037,
                'id_model' => 1709,
                'id_fuel' => 4,
            ),
            37 => 
            array (
                'id_reg' => 2038,
                'id_model' => 1710,
                'id_fuel' => 4,
            ),
            38 => 
            array (
                'id_reg' => 2039,
                'id_model' => 1711,
                'id_fuel' => 2,
            ),
            39 => 
            array (
                'id_reg' => 2040,
                'id_model' => 1711,
                'id_fuel' => 4,
            ),
            40 => 
            array (
                'id_reg' => 2041,
                'id_model' => 1712,
                'id_fuel' => 4,
            ),
            41 => 
            array (
                'id_reg' => 2042,
                'id_model' => 1713,
                'id_fuel' => 4,
            ),
            42 => 
            array (
                'id_reg' => 2043,
                'id_model' => 1714,
                'id_fuel' => 4,
            ),
            43 => 
            array (
                'id_reg' => 2044,
                'id_model' => 1715,
                'id_fuel' => 4,
            ),
            44 => 
            array (
                'id_reg' => 2045,
                'id_model' => 1716,
                'id_fuel' => 2,
            ),
            45 => 
            array (
                'id_reg' => 2046,
                'id_model' => 1716,
                'id_fuel' => 4,
            ),
            46 => 
            array (
                'id_reg' => 2047,
                'id_model' => 1717,
                'id_fuel' => 2,
            ),
            47 => 
            array (
                'id_reg' => 2048,
                'id_model' => 1718,
                'id_fuel' => 2,
            ),
            48 => 
            array (
                'id_reg' => 2049,
                'id_model' => 1719,
                'id_fuel' => 2,
            ),
            49 => 
            array (
                'id_reg' => 2050,
                'id_model' => 1719,
                'id_fuel' => 4,
            ),
            50 => 
            array (
                'id_reg' => 2051,
                'id_model' => 1720,
                'id_fuel' => 2,
            ),
            51 => 
            array (
                'id_reg' => 2052,
                'id_model' => 1720,
                'id_fuel' => 4,
            ),
            52 => 
            array (
                'id_reg' => 2053,
                'id_model' => 1721,
                'id_fuel' => 4,
            ),
            53 => 
            array (
                'id_reg' => 2054,
                'id_model' => 1722,
                'id_fuel' => 4,
            ),
            54 => 
            array (
                'id_reg' => 2055,
                'id_model' => 1723,
                'id_fuel' => 2,
            ),
            55 => 
            array (
                'id_reg' => 2056,
                'id_model' => 1723,
                'id_fuel' => 4,
            ),
            56 => 
            array (
                'id_reg' => 2057,
                'id_model' => 1724,
                'id_fuel' => 2,
            ),
            57 => 
            array (
                'id_reg' => 2058,
                'id_model' => 1725,
                'id_fuel' => 2,
            ),
            58 => 
            array (
                'id_reg' => 2059,
                'id_model' => 1725,
                'id_fuel' => 4,
            ),
            59 => 
            array (
                'id_reg' => 2060,
                'id_model' => 1726,
                'id_fuel' => 2,
            ),
            60 => 
            array (
                'id_reg' => 2061,
                'id_model' => 1727,
                'id_fuel' => 2,
            ),
            61 => 
            array (
                'id_reg' => 2062,
                'id_model' => 1727,
                'id_fuel' => 4,
            ),
            62 => 
            array (
                'id_reg' => 2063,
                'id_model' => 1728,
                'id_fuel' => 4,
            ),
            63 => 
            array (
                'id_reg' => 2064,
                'id_model' => 1729,
                'id_fuel' => 4,
            ),
            64 => 
            array (
                'id_reg' => 2065,
                'id_model' => 1730,
                'id_fuel' => 2,
            ),
            65 => 
            array (
                'id_reg' => 2066,
                'id_model' => 1730,
                'id_fuel' => 4,
            ),
            66 => 
            array (
                'id_reg' => 2067,
                'id_model' => 1731,
                'id_fuel' => 4,
            ),
            67 => 
            array (
                'id_reg' => 2068,
                'id_model' => 1732,
                'id_fuel' => 4,
            ),
            68 => 
            array (
                'id_reg' => 2069,
                'id_model' => 1733,
                'id_fuel' => 4,
            ),
            69 => 
            array (
                'id_reg' => 2070,
                'id_model' => 1734,
                'id_fuel' => 4,
            ),
            70 => 
            array (
                'id_reg' => 2071,
                'id_model' => 1735,
                'id_fuel' => 4,
            ),
            71 => 
            array (
                'id_reg' => 2072,
                'id_model' => 1736,
                'id_fuel' => 2,
            ),
            72 => 
            array (
                'id_reg' => 2073,
                'id_model' => 1737,
                'id_fuel' => 4,
            ),
            73 => 
            array (
                'id_reg' => 2074,
                'id_model' => 1738,
                'id_fuel' => 4,
            ),
            74 => 
            array (
                'id_reg' => 2075,
                'id_model' => 1739,
                'id_fuel' => 4,
            ),
            75 => 
            array (
                'id_reg' => 2076,
                'id_model' => 1740,
                'id_fuel' => 2,
            ),
            76 => 
            array (
                'id_reg' => 2077,
                'id_model' => 1740,
                'id_fuel' => 4,
            ),
            77 => 
            array (
                'id_reg' => 2078,
                'id_model' => 1741,
                'id_fuel' => 4,
            ),
            78 => 
            array (
                'id_reg' => 2079,
                'id_model' => 1742,
                'id_fuel' => 4,
            ),
            79 => 
            array (
                'id_reg' => 2080,
                'id_model' => 1743,
                'id_fuel' => 4,
            ),
            80 => 
            array (
                'id_reg' => 2081,
                'id_model' => 1744,
                'id_fuel' => 5,
            ),
            81 => 
            array (
                'id_reg' => 2082,
                'id_model' => 1745,
                'id_fuel' => 2,
            ),
            82 => 
            array (
                'id_reg' => 2083,
                'id_model' => 1745,
                'id_fuel' => 4,
            ),
            83 => 
            array (
                'id_reg' => 2084,
                'id_model' => 1746,
                'id_fuel' => 4,
            ),
            84 => 
            array (
                'id_reg' => 2085,
                'id_model' => 1747,
                'id_fuel' => 4,
            ),
            85 => 
            array (
                'id_reg' => 2086,
                'id_model' => 1748,
                'id_fuel' => 4,
            ),
            86 => 
            array (
                'id_reg' => 2087,
                'id_model' => 1749,
                'id_fuel' => 4,
            ),
            87 => 
            array (
                'id_reg' => 2088,
                'id_model' => 1750,
                'id_fuel' => 4,
            ),
            88 => 
            array (
                'id_reg' => 2089,
                'id_model' => 1751,
                'id_fuel' => 6,
            ),
            89 => 
            array (
                'id_reg' => 2090,
                'id_model' => 1752,
                'id_fuel' => 6,
            ),
            90 => 
            array (
                'id_reg' => 2091,
                'id_model' => 1753,
                'id_fuel' => 6,
            ),
            91 => 
            array (
                'id_reg' => 2092,
                'id_model' => 1754,
                'id_fuel' => 6,
            ),
            92 => 
            array (
                'id_reg' => 2093,
                'id_model' => 1755,
                'id_fuel' => 6,
            ),
            93 => 
            array (
                'id_reg' => 2094,
                'id_model' => 1756,
                'id_fuel' => 6,
            ),
            94 => 
            array (
                'id_reg' => 2095,
                'id_model' => 1757,
                'id_fuel' => 6,
            ),
            95 => 
            array (
                'id_reg' => 2096,
                'id_model' => 1758,
                'id_fuel' => 6,
            ),
            96 => 
            array (
                'id_reg' => 2097,
                'id_model' => 1759,
                'id_fuel' => 6,
            ),
            97 => 
            array (
                'id_reg' => 2098,
                'id_model' => 1760,
                'id_fuel' => 2,
            ),
            98 => 
            array (
                'id_reg' => 2099,
                'id_model' => 1760,
                'id_fuel' => 4,
            ),
            99 => 
            array (
                'id_reg' => 2100,
                'id_model' => 1761,
                'id_fuel' => 4,
            ),
            100 => 
            array (
                'id_reg' => 2101,
                'id_model' => 1762,
                'id_fuel' => 2,
            ),
            101 => 
            array (
                'id_reg' => 2102,
                'id_model' => 1762,
                'id_fuel' => 4,
            ),
            102 => 
            array (
                'id_reg' => 2103,
                'id_model' => 1763,
                'id_fuel' => 2,
            ),
            103 => 
            array (
                'id_reg' => 2104,
                'id_model' => 1763,
                'id_fuel' => 4,
            ),
            104 => 
            array (
                'id_reg' => 2105,
                'id_model' => 1764,
                'id_fuel' => 2,
            ),
            105 => 
            array (
                'id_reg' => 2106,
                'id_model' => 1764,
                'id_fuel' => 4,
            ),
            106 => 
            array (
                'id_reg' => 2107,
                'id_model' => 1765,
                'id_fuel' => 2,
            ),
            107 => 
            array (
                'id_reg' => 2108,
                'id_model' => 1765,
                'id_fuel' => 4,
            ),
            108 => 
            array (
                'id_reg' => 2109,
                'id_model' => 1766,
                'id_fuel' => 2,
            ),
            109 => 
            array (
                'id_reg' => 2110,
                'id_model' => 1767,
                'id_fuel' => 2,
            ),
            110 => 
            array (
                'id_reg' => 2111,
                'id_model' => 1767,
                'id_fuel' => 4,
            ),
            111 => 
            array (
                'id_reg' => 2112,
                'id_model' => 1768,
                'id_fuel' => 2,
            ),
            112 => 
            array (
                'id_reg' => 2113,
                'id_model' => 1768,
                'id_fuel' => 4,
            ),
            113 => 
            array (
                'id_reg' => 2114,
                'id_model' => 1769,
                'id_fuel' => 2,
            ),
            114 => 
            array (
                'id_reg' => 2115,
                'id_model' => 1769,
                'id_fuel' => 4,
            ),
            115 => 
            array (
                'id_reg' => 2116,
                'id_model' => 1770,
                'id_fuel' => 4,
            ),
            116 => 
            array (
                'id_reg' => 2117,
                'id_model' => 1771,
                'id_fuel' => 4,
            ),
            117 => 
            array (
                'id_reg' => 2118,
                'id_model' => 1772,
                'id_fuel' => 2,
            ),
            118 => 
            array (
                'id_reg' => 2119,
                'id_model' => 1773,
                'id_fuel' => 2,
            ),
            119 => 
            array (
                'id_reg' => 2120,
                'id_model' => 1773,
                'id_fuel' => 4,
            ),
            120 => 
            array (
                'id_reg' => 2121,
                'id_model' => 1774,
                'id_fuel' => 2,
            ),
            121 => 
            array (
                'id_reg' => 2122,
                'id_model' => 1775,
                'id_fuel' => 2,
            ),
            122 => 
            array (
                'id_reg' => 2123,
                'id_model' => 1775,
                'id_fuel' => 4,
            ),
            123 => 
            array (
                'id_reg' => 2124,
                'id_model' => 1776,
                'id_fuel' => 4,
            ),
            124 => 
            array (
                'id_reg' => 2125,
                'id_model' => 1777,
                'id_fuel' => 2,
            ),
            125 => 
            array (
                'id_reg' => 2126,
                'id_model' => 1777,
                'id_fuel' => 4,
            ),
            126 => 
            array (
                'id_reg' => 2127,
                'id_model' => 1778,
                'id_fuel' => 2,
            ),
            127 => 
            array (
                'id_reg' => 2128,
                'id_model' => 1779,
                'id_fuel' => 4,
            ),
            128 => 
            array (
                'id_reg' => 2129,
                'id_model' => 1780,
                'id_fuel' => 4,
            ),
            129 => 
            array (
                'id_reg' => 2130,
                'id_model' => 1781,
                'id_fuel' => 4,
            ),
            130 => 
            array (
                'id_reg' => 2131,
                'id_model' => 1782,
                'id_fuel' => 4,
            ),
            131 => 
            array (
                'id_reg' => 2132,
                'id_model' => 1783,
                'id_fuel' => 4,
            ),
            132 => 
            array (
                'id_reg' => 2133,
                'id_model' => 1784,
                'id_fuel' => 4,
            ),
            133 => 
            array (
                'id_reg' => 2134,
                'id_model' => 1785,
                'id_fuel' => 4,
            ),
            134 => 
            array (
                'id_reg' => 2135,
                'id_model' => 1786,
                'id_fuel' => 4,
            ),
            135 => 
            array (
                'id_reg' => 2136,
                'id_model' => 1787,
                'id_fuel' => 4,
            ),
            136 => 
            array (
                'id_reg' => 2137,
                'id_model' => 1788,
                'id_fuel' => 4,
            ),
            137 => 
            array (
                'id_reg' => 2138,
                'id_model' => 1789,
                'id_fuel' => 4,
            ),
            138 => 
            array (
                'id_reg' => 2139,
                'id_model' => 1790,
                'id_fuel' => 4,
            ),
            139 => 
            array (
                'id_reg' => 2140,
                'id_model' => 1791,
                'id_fuel' => 2,
            ),
            140 => 
            array (
                'id_reg' => 2141,
                'id_model' => 1792,
                'id_fuel' => 2,
            ),
            141 => 
            array (
                'id_reg' => 2142,
                'id_model' => 1793,
                'id_fuel' => 2,
            ),
            142 => 
            array (
                'id_reg' => 2143,
                'id_model' => 1794,
                'id_fuel' => 4,
            ),
            143 => 
            array (
                'id_reg' => 2144,
                'id_model' => 1795,
                'id_fuel' => 4,
            ),
            144 => 
            array (
                'id_reg' => 2145,
                'id_model' => 1796,
                'id_fuel' => 4,
            ),
            145 => 
            array (
                'id_reg' => 2146,
                'id_model' => 1797,
                'id_fuel' => 4,
            ),
            146 => 
            array (
                'id_reg' => 2147,
                'id_model' => 1798,
                'id_fuel' => 2,
            ),
            147 => 
            array (
                'id_reg' => 2148,
                'id_model' => 1798,
                'id_fuel' => 4,
            ),
            148 => 
            array (
                'id_reg' => 2149,
                'id_model' => 1799,
                'id_fuel' => 2,
            ),
            149 => 
            array (
                'id_reg' => 2150,
                'id_model' => 1800,
                'id_fuel' => 2,
            ),
            150 => 
            array (
                'id_reg' => 2151,
                'id_model' => 1801,
                'id_fuel' => 4,
            ),
            151 => 
            array (
                'id_reg' => 2152,
                'id_model' => 1802,
                'id_fuel' => 4,
            ),
            152 => 
            array (
                'id_reg' => 2153,
                'id_model' => 1803,
                'id_fuel' => 4,
            ),
            153 => 
            array (
                'id_reg' => 2154,
                'id_model' => 1804,
                'id_fuel' => 4,
            ),
            154 => 
            array (
                'id_reg' => 2155,
                'id_model' => 1805,
                'id_fuel' => 2,
            ),
            155 => 
            array (
                'id_reg' => 2156,
                'id_model' => 1805,
                'id_fuel' => 4,
            ),
            156 => 
            array (
                'id_reg' => 2157,
                'id_model' => 1806,
                'id_fuel' => 4,
            ),
            157 => 
            array (
                'id_reg' => 2158,
                'id_model' => 1807,
                'id_fuel' => 4,
            ),
            158 => 
            array (
                'id_reg' => 2159,
                'id_model' => 1808,
                'id_fuel' => 4,
            ),
            159 => 
            array (
                'id_reg' => 2160,
                'id_model' => 1809,
                'id_fuel' => 4,
            ),
            160 => 
            array (
                'id_reg' => 2161,
                'id_model' => 1810,
                'id_fuel' => 4,
            ),
            161 => 
            array (
                'id_reg' => 2162,
                'id_model' => 1811,
                'id_fuel' => 2,
            ),
            162 => 
            array (
                'id_reg' => 2163,
                'id_model' => 1811,
                'id_fuel' => 4,
            ),
            163 => 
            array (
                'id_reg' => 2164,
                'id_model' => 1812,
                'id_fuel' => 2,
            ),
            164 => 
            array (
                'id_reg' => 2165,
                'id_model' => 1812,
                'id_fuel' => 4,
            ),
            165 => 
            array (
                'id_reg' => 2166,
                'id_model' => 1813,
                'id_fuel' => 2,
            ),
            166 => 
            array (
                'id_reg' => 2167,
                'id_model' => 1814,
                'id_fuel' => 2,
            ),
            167 => 
            array (
                'id_reg' => 2168,
                'id_model' => 1815,
                'id_fuel' => 4,
            ),
            168 => 
            array (
                'id_reg' => 2169,
                'id_model' => 1816,
                'id_fuel' => 4,
            ),
            169 => 
            array (
                'id_reg' => 2170,
                'id_model' => 1817,
                'id_fuel' => 2,
            ),
            170 => 
            array (
                'id_reg' => 2171,
                'id_model' => 1818,
                'id_fuel' => 2,
            ),
            171 => 
            array (
                'id_reg' => 2172,
                'id_model' => 1819,
                'id_fuel' => 2,
            ),
            172 => 
            array (
                'id_reg' => 2173,
                'id_model' => 1820,
                'id_fuel' => 2,
            ),
            173 => 
            array (
                'id_reg' => 2174,
                'id_model' => 1820,
                'id_fuel' => 4,
            ),
            174 => 
            array (
                'id_reg' => 2175,
                'id_model' => 1821,
                'id_fuel' => 2,
            ),
            175 => 
            array (
                'id_reg' => 2176,
                'id_model' => 1821,
                'id_fuel' => 4,
            ),
            176 => 
            array (
                'id_reg' => 2177,
                'id_model' => 1822,
                'id_fuel' => 4,
            ),
            177 => 
            array (
                'id_reg' => 2178,
                'id_model' => 1823,
                'id_fuel' => 2,
            ),
            178 => 
            array (
                'id_reg' => 2179,
                'id_model' => 1823,
                'id_fuel' => 4,
            ),
            179 => 
            array (
                'id_reg' => 2180,
                'id_model' => 1824,
                'id_fuel' => 4,
            ),
            180 => 
            array (
                'id_reg' => 2181,
                'id_model' => 1825,
                'id_fuel' => 4,
            ),
            181 => 
            array (
                'id_reg' => 2182,
                'id_model' => 1826,
                'id_fuel' => 4,
            ),
            182 => 
            array (
                'id_reg' => 2183,
                'id_model' => 1827,
                'id_fuel' => 2,
            ),
            183 => 
            array (
                'id_reg' => 2184,
                'id_model' => 1828,
                'id_fuel' => 2,
            ),
            184 => 
            array (
                'id_reg' => 2185,
                'id_model' => 1829,
                'id_fuel' => 4,
            ),
            185 => 
            array (
                'id_reg' => 2186,
                'id_model' => 1830,
                'id_fuel' => 2,
            ),
            186 => 
            array (
                'id_reg' => 2187,
                'id_model' => 1831,
                'id_fuel' => 2,
            ),
            187 => 
            array (
                'id_reg' => 2188,
                'id_model' => 1832,
                'id_fuel' => 4,
            ),
            188 => 
            array (
                'id_reg' => 2189,
                'id_model' => 1833,
                'id_fuel' => 4,
            ),
            189 => 
            array (
                'id_reg' => 2190,
                'id_model' => 1834,
                'id_fuel' => 4,
            ),
            190 => 
            array (
                'id_reg' => 2191,
                'id_model' => 1835,
                'id_fuel' => 2,
            ),
            191 => 
            array (
                'id_reg' => 2192,
                'id_model' => 1836,
                'id_fuel' => 2,
            ),
            192 => 
            array (
                'id_reg' => 2193,
                'id_model' => 1837,
                'id_fuel' => 2,
            ),
            193 => 
            array (
                'id_reg' => 2194,
                'id_model' => 1839,
                'id_fuel' => 4,
            ),
            194 => 
            array (
                'id_reg' => 2195,
                'id_model' => 1840,
                'id_fuel' => 4,
            ),
            195 => 
            array (
                'id_reg' => 2196,
                'id_model' => 1841,
                'id_fuel' => 4,
            ),
            196 => 
            array (
                'id_reg' => 2197,
                'id_model' => 1842,
                'id_fuel' => 2,
            ),
            197 => 
            array (
                'id_reg' => 2198,
                'id_model' => 1843,
                'id_fuel' => 2,
            ),
            198 => 
            array (
                'id_reg' => 2199,
                'id_model' => 1844,
                'id_fuel' => 4,
            ),
            199 => 
            array (
                'id_reg' => 2200,
                'id_model' => 1845,
                'id_fuel' => 4,
            ),
            200 => 
            array (
                'id_reg' => 2201,
                'id_model' => 1846,
                'id_fuel' => 4,
            ),
            201 => 
            array (
                'id_reg' => 2202,
                'id_model' => 1847,
                'id_fuel' => 4,
            ),
            202 => 
            array (
                'id_reg' => 2203,
                'id_model' => 1848,
                'id_fuel' => 4,
            ),
            203 => 
            array (
                'id_reg' => 2204,
                'id_model' => 1849,
                'id_fuel' => 4,
            ),
            204 => 
            array (
                'id_reg' => 2205,
                'id_model' => 1850,
                'id_fuel' => 4,
            ),
            205 => 
            array (
                'id_reg' => 2206,
                'id_model' => 1851,
                'id_fuel' => 4,
            ),
            206 => 
            array (
                'id_reg' => 2207,
                'id_model' => 1852,
                'id_fuel' => 4,
            ),
            207 => 
            array (
                'id_reg' => 2208,
                'id_model' => 1853,
                'id_fuel' => 4,
            ),
            208 => 
            array (
                'id_reg' => 2209,
                'id_model' => 1854,
                'id_fuel' => 2,
            ),
            209 => 
            array (
                'id_reg' => 2210,
                'id_model' => 1855,
                'id_fuel' => 4,
            ),
            210 => 
            array (
                'id_reg' => 2211,
                'id_model' => 1856,
                'id_fuel' => 4,
            ),
            211 => 
            array (
                'id_reg' => 2212,
                'id_model' => 1857,
                'id_fuel' => 4,
            ),
            212 => 
            array (
                'id_reg' => 2213,
                'id_model' => 1858,
                'id_fuel' => 4,
            ),
            213 => 
            array (
                'id_reg' => 2214,
                'id_model' => 1859,
                'id_fuel' => 2,
            ),
            214 => 
            array (
                'id_reg' => 2215,
                'id_model' => 1860,
                'id_fuel' => 2,
            ),
            215 => 
            array (
                'id_reg' => 2216,
                'id_model' => 1861,
                'id_fuel' => 2,
            ),
            216 => 
            array (
                'id_reg' => 2217,
                'id_model' => 1861,
                'id_fuel' => 4,
            ),
            217 => 
            array (
                'id_reg' => 2218,
                'id_model' => 1862,
                'id_fuel' => 4,
            ),
            218 => 
            array (
                'id_reg' => 2219,
                'id_model' => 1864,
                'id_fuel' => 2,
            ),
            219 => 
            array (
                'id_reg' => 2220,
                'id_model' => 1864,
                'id_fuel' => 4,
            ),
            220 => 
            array (
                'id_reg' => 2221,
                'id_model' => 1864,
                'id_fuel' => 5,
            ),
            221 => 
            array (
                'id_reg' => 2222,
                'id_model' => 1865,
                'id_fuel' => 2,
            ),
            222 => 
            array (
                'id_reg' => 2223,
                'id_model' => 1865,
                'id_fuel' => 4,
            ),
            223 => 
            array (
                'id_reg' => 2224,
                'id_model' => 1866,
                'id_fuel' => 4,
            ),
            224 => 
            array (
                'id_reg' => 2225,
                'id_model' => 1867,
                'id_fuel' => 4,
            ),
            225 => 
            array (
                'id_reg' => 2226,
                'id_model' => 1868,
                'id_fuel' => 2,
            ),
            226 => 
            array (
                'id_reg' => 2227,
                'id_model' => 1868,
                'id_fuel' => 4,
            ),
            227 => 
            array (
                'id_reg' => 2228,
                'id_model' => 1869,
                'id_fuel' => 2,
            ),
            228 => 
            array (
                'id_reg' => 2229,
                'id_model' => 1869,
                'id_fuel' => 4,
            ),
            229 => 
            array (
                'id_reg' => 2230,
                'id_model' => 1870,
                'id_fuel' => 2,
            ),
            230 => 
            array (
                'id_reg' => 2231,
                'id_model' => 1870,
                'id_fuel' => 4,
            ),
            231 => 
            array (
                'id_reg' => 2232,
                'id_model' => 1871,
                'id_fuel' => 2,
            ),
            232 => 
            array (
                'id_reg' => 2233,
                'id_model' => 1871,
                'id_fuel' => 4,
            ),
            233 => 
            array (
                'id_reg' => 2234,
                'id_model' => 1872,
                'id_fuel' => 2,
            ),
            234 => 
            array (
                'id_reg' => 2235,
                'id_model' => 1872,
                'id_fuel' => 4,
            ),
            235 => 
            array (
                'id_reg' => 2236,
                'id_model' => 1872,
                'id_fuel' => 5,
            ),
            236 => 
            array (
                'id_reg' => 2237,
                'id_model' => 1873,
                'id_fuel' => 2,
            ),
            237 => 
            array (
                'id_reg' => 2238,
                'id_model' => 1873,
                'id_fuel' => 4,
            ),
            238 => 
            array (
                'id_reg' => 2239,
                'id_model' => 1874,
                'id_fuel' => 4,
            ),
            239 => 
            array (
                'id_reg' => 2240,
                'id_model' => 1875,
                'id_fuel' => 2,
            ),
            240 => 
            array (
                'id_reg' => 2241,
                'id_model' => 1876,
                'id_fuel' => 2,
            ),
            241 => 
            array (
                'id_reg' => 2242,
                'id_model' => 1877,
                'id_fuel' => 2,
            ),
            242 => 
            array (
                'id_reg' => 2243,
                'id_model' => 1878,
                'id_fuel' => 2,
            ),
            243 => 
            array (
                'id_reg' => 2244,
                'id_model' => 1879,
                'id_fuel' => 2,
            ),
            244 => 
            array (
                'id_reg' => 2245,
                'id_model' => 1880,
                'id_fuel' => 2,
            ),
            245 => 
            array (
                'id_reg' => 2246,
                'id_model' => 1880,
                'id_fuel' => 4,
            ),
            246 => 
            array (
                'id_reg' => 2247,
                'id_model' => 1881,
                'id_fuel' => 4,
            ),
            247 => 
            array (
                'id_reg' => 2248,
                'id_model' => 1882,
                'id_fuel' => 2,
            ),
            248 => 
            array (
                'id_reg' => 2249,
                'id_model' => 1883,
                'id_fuel' => 4,
            ),
            249 => 
            array (
                'id_reg' => 2250,
                'id_model' => 1884,
                'id_fuel' => 2,
            ),
            250 => 
            array (
                'id_reg' => 2251,
                'id_model' => 1884,
                'id_fuel' => 4,
            ),
            251 => 
            array (
                'id_reg' => 2252,
                'id_model' => 1885,
                'id_fuel' => 2,
            ),
            252 => 
            array (
                'id_reg' => 2253,
                'id_model' => 1885,
                'id_fuel' => 4,
            ),
            253 => 
            array (
                'id_reg' => 2254,
                'id_model' => 1886,
                'id_fuel' => 2,
            ),
            254 => 
            array (
                'id_reg' => 2255,
                'id_model' => 1886,
                'id_fuel' => 4,
            ),
            255 => 
            array (
                'id_reg' => 2256,
                'id_model' => 1887,
                'id_fuel' => 2,
            ),
            256 => 
            array (
                'id_reg' => 2257,
                'id_model' => 1887,
                'id_fuel' => 4,
            ),
            257 => 
            array (
                'id_reg' => 2258,
                'id_model' => 1887,
                'id_fuel' => 5,
            ),
            258 => 
            array (
                'id_reg' => 2259,
                'id_model' => 1888,
                'id_fuel' => 2,
            ),
            259 => 
            array (
                'id_reg' => 2260,
                'id_model' => 1888,
                'id_fuel' => 4,
            ),
            260 => 
            array (
                'id_reg' => 2261,
                'id_model' => 1888,
                'id_fuel' => 5,
            ),
            261 => 
            array (
                'id_reg' => 2262,
                'id_model' => 1889,
                'id_fuel' => 2,
            ),
            262 => 
            array (
                'id_reg' => 2263,
                'id_model' => 1889,
                'id_fuel' => 4,
            ),
            263 => 
            array (
                'id_reg' => 2264,
                'id_model' => 1890,
                'id_fuel' => 2,
            ),
            264 => 
            array (
                'id_reg' => 2265,
                'id_model' => 1890,
                'id_fuel' => 4,
            ),
            265 => 
            array (
                'id_reg' => 2266,
                'id_model' => 1891,
                'id_fuel' => 2,
            ),
            266 => 
            array (
                'id_reg' => 2267,
                'id_model' => 1892,
                'id_fuel' => 2,
            ),
            267 => 
            array (
                'id_reg' => 2268,
                'id_model' => 1893,
                'id_fuel' => 2,
            ),
            268 => 
            array (
                'id_reg' => 2269,
                'id_model' => 1894,
                'id_fuel' => 2,
            ),
            269 => 
            array (
                'id_reg' => 2270,
                'id_model' => 1895,
                'id_fuel' => 2,
            ),
            270 => 
            array (
                'id_reg' => 2271,
                'id_model' => 1896,
                'id_fuel' => 2,
            ),
            271 => 
            array (
                'id_reg' => 2272,
                'id_model' => 1897,
                'id_fuel' => 2,
            ),
            272 => 
            array (
                'id_reg' => 2273,
                'id_model' => 1898,
                'id_fuel' => 2,
            ),
            273 => 
            array (
                'id_reg' => 2274,
                'id_model' => 1899,
                'id_fuel' => 2,
            ),
            274 => 
            array (
                'id_reg' => 2275,
                'id_model' => 1900,
                'id_fuel' => 2,
            ),
            275 => 
            array (
                'id_reg' => 2276,
                'id_model' => 1901,
                'id_fuel' => 2,
            ),
            276 => 
            array (
                'id_reg' => 2277,
                'id_model' => 1901,
                'id_fuel' => 4,
            ),
            277 => 
            array (
                'id_reg' => 2278,
                'id_model' => 1902,
                'id_fuel' => 4,
            ),
            278 => 
            array (
                'id_reg' => 2279,
                'id_model' => 1903,
                'id_fuel' => 2,
            ),
            279 => 
            array (
                'id_reg' => 2280,
                'id_model' => 1904,
                'id_fuel' => 2,
            ),
            280 => 
            array (
                'id_reg' => 2281,
                'id_model' => 1905,
                'id_fuel' => 2,
            ),
            281 => 
            array (
                'id_reg' => 2282,
                'id_model' => 1906,
                'id_fuel' => 2,
            ),
            282 => 
            array (
                'id_reg' => 2283,
                'id_model' => 1907,
                'id_fuel' => 2,
            ),
            283 => 
            array (
                'id_reg' => 2284,
                'id_model' => 1908,
                'id_fuel' => 2,
            ),
            284 => 
            array (
                'id_reg' => 2285,
                'id_model' => 1909,
                'id_fuel' => 2,
            ),
            285 => 
            array (
                'id_reg' => 2286,
                'id_model' => 1910,
                'id_fuel' => 2,
            ),
            286 => 
            array (
                'id_reg' => 2287,
                'id_model' => 1911,
                'id_fuel' => 2,
            ),
            287 => 
            array (
                'id_reg' => 2288,
                'id_model' => 1912,
                'id_fuel' => 2,
            ),
            288 => 
            array (
                'id_reg' => 2289,
                'id_model' => 1913,
                'id_fuel' => 2,
            ),
            289 => 
            array (
                'id_reg' => 2290,
                'id_model' => 1914,
                'id_fuel' => 2,
            ),
            290 => 
            array (
                'id_reg' => 2291,
                'id_model' => 1915,
                'id_fuel' => 2,
            ),
            291 => 
            array (
                'id_reg' => 2292,
                'id_model' => 1915,
                'id_fuel' => 4,
            ),
            292 => 
            array (
                'id_reg' => 2293,
                'id_model' => 1916,
                'id_fuel' => 2,
            ),
            293 => 
            array (
                'id_reg' => 2294,
                'id_model' => 1917,
                'id_fuel' => 2,
            ),
            294 => 
            array (
                'id_reg' => 2295,
                'id_model' => 1918,
                'id_fuel' => 2,
            ),
            295 => 
            array (
                'id_reg' => 2296,
                'id_model' => 1919,
                'id_fuel' => 2,
            ),
            296 => 
            array (
                'id_reg' => 2297,
                'id_model' => 1920,
                'id_fuel' => 2,
            ),
            297 => 
            array (
                'id_reg' => 2298,
                'id_model' => 1921,
                'id_fuel' => 2,
            ),
            298 => 
            array (
                'id_reg' => 2299,
                'id_model' => 1922,
                'id_fuel' => 2,
            ),
            299 => 
            array (
                'id_reg' => 2300,
                'id_model' => 1922,
                'id_fuel' => 4,
            ),
            300 => 
            array (
                'id_reg' => 2301,
                'id_model' => 1923,
                'id_fuel' => 4,
            ),
            301 => 
            array (
                'id_reg' => 2302,
                'id_model' => 1924,
                'id_fuel' => 2,
            ),
            302 => 
            array (
                'id_reg' => 2303,
                'id_model' => 1924,
                'id_fuel' => 4,
            ),
            303 => 
            array (
                'id_reg' => 2304,
                'id_model' => 1924,
                'id_fuel' => 5,
            ),
            304 => 
            array (
                'id_reg' => 2305,
                'id_model' => 1925,
                'id_fuel' => 2,
            ),
            305 => 
            array (
                'id_reg' => 2306,
                'id_model' => 1925,
                'id_fuel' => 4,
            ),
            306 => 
            array (
                'id_reg' => 2307,
                'id_model' => 1925,
                'id_fuel' => 5,
            ),
            307 => 
            array (
                'id_reg' => 2308,
                'id_model' => 1926,
                'id_fuel' => 4,
            ),
            308 => 
            array (
                'id_reg' => 2309,
                'id_model' => 1927,
                'id_fuel' => 2,
            ),
            309 => 
            array (
                'id_reg' => 2310,
                'id_model' => 1928,
                'id_fuel' => 4,
            ),
            310 => 
            array (
                'id_reg' => 2311,
                'id_model' => 1929,
                'id_fuel' => 2,
            ),
            311 => 
            array (
                'id_reg' => 2312,
                'id_model' => 1930,
                'id_fuel' => 4,
            ),
            312 => 
            array (
                'id_reg' => 2313,
                'id_model' => 1931,
                'id_fuel' => 4,
            ),
            313 => 
            array (
                'id_reg' => 2314,
                'id_model' => 1932,
                'id_fuel' => 4,
            ),
            314 => 
            array (
                'id_reg' => 2315,
                'id_model' => 1933,
                'id_fuel' => 4,
            ),
            315 => 
            array (
                'id_reg' => 2316,
                'id_model' => 1934,
                'id_fuel' => 4,
            ),
            316 => 
            array (
                'id_reg' => 2317,
                'id_model' => 1935,
                'id_fuel' => 4,
            ),
            317 => 
            array (
                'id_reg' => 2318,
                'id_model' => 1936,
                'id_fuel' => 4,
            ),
            318 => 
            array (
                'id_reg' => 2319,
                'id_model' => 1937,
                'id_fuel' => 4,
            ),
            319 => 
            array (
                'id_reg' => 2320,
                'id_model' => 1938,
                'id_fuel' => 4,
            ),
            320 => 
            array (
                'id_reg' => 2321,
                'id_model' => 1939,
                'id_fuel' => 4,
            ),
            321 => 
            array (
                'id_reg' => 2322,
                'id_model' => 1940,
                'id_fuel' => 4,
            ),
            322 => 
            array (
                'id_reg' => 2323,
                'id_model' => 1941,
                'id_fuel' => 4,
            ),
            323 => 
            array (
                'id_reg' => 2324,
                'id_model' => 1942,
                'id_fuel' => 4,
            ),
            324 => 
            array (
                'id_reg' => 2325,
                'id_model' => 1943,
                'id_fuel' => 4,
            ),
            325 => 
            array (
                'id_reg' => 2326,
                'id_model' => 1944,
                'id_fuel' => 4,
            ),
            326 => 
            array (
                'id_reg' => 2327,
                'id_model' => 1945,
                'id_fuel' => 4,
            ),
            327 => 
            array (
                'id_reg' => 2328,
                'id_model' => 1946,
                'id_fuel' => 2,
            ),
            328 => 
            array (
                'id_reg' => 2329,
                'id_model' => 1946,
                'id_fuel' => 4,
            ),
            329 => 
            array (
                'id_reg' => 2330,
                'id_model' => 1947,
                'id_fuel' => 2,
            ),
            330 => 
            array (
                'id_reg' => 2331,
                'id_model' => 1947,
                'id_fuel' => 4,
            ),
            331 => 
            array (
                'id_reg' => 2332,
                'id_model' => 1948,
                'id_fuel' => 4,
            ),
            332 => 
            array (
                'id_reg' => 2333,
                'id_model' => 1949,
                'id_fuel' => 4,
            ),
            333 => 
            array (
                'id_reg' => 2334,
                'id_model' => 1950,
                'id_fuel' => 2,
            ),
            334 => 
            array (
                'id_reg' => 2335,
                'id_model' => 1950,
                'id_fuel' => 4,
            ),
            335 => 
            array (
                'id_reg' => 2336,
                'id_model' => 1951,
                'id_fuel' => 2,
            ),
            336 => 
            array (
                'id_reg' => 2337,
                'id_model' => 1951,
                'id_fuel' => 3,
            ),
            337 => 
            array (
                'id_reg' => 2338,
                'id_model' => 1951,
                'id_fuel' => 4,
            ),
            338 => 
            array (
                'id_reg' => 2339,
                'id_model' => 1952,
                'id_fuel' => 4,
            ),
            339 => 
            array (
                'id_reg' => 2340,
                'id_model' => 1953,
                'id_fuel' => 2,
            ),
            340 => 
            array (
                'id_reg' => 2341,
                'id_model' => 1954,
                'id_fuel' => 2,
            ),
            341 => 
            array (
                'id_reg' => 2342,
                'id_model' => 1955,
                'id_fuel' => 2,
            ),
            342 => 
            array (
                'id_reg' => 2343,
                'id_model' => 1956,
                'id_fuel' => 2,
            ),
            343 => 
            array (
                'id_reg' => 2344,
                'id_model' => 1957,
                'id_fuel' => 2,
            ),
            344 => 
            array (
                'id_reg' => 2345,
                'id_model' => 1958,
                'id_fuel' => 2,
            ),
            345 => 
            array (
                'id_reg' => 2346,
                'id_model' => 1959,
                'id_fuel' => 2,
            ),
            346 => 
            array (
                'id_reg' => 2347,
                'id_model' => 1959,
                'id_fuel' => 4,
            ),
            347 => 
            array (
                'id_reg' => 2348,
                'id_model' => 1960,
                'id_fuel' => 2,
            ),
            348 => 
            array (
                'id_reg' => 2349,
                'id_model' => 1960,
                'id_fuel' => 4,
            ),
            349 => 
            array (
                'id_reg' => 2350,
                'id_model' => 1961,
                'id_fuel' => 2,
            ),
            350 => 
            array (
                'id_reg' => 2351,
                'id_model' => 1961,
                'id_fuel' => 4,
            ),
            351 => 
            array (
                'id_reg' => 2352,
                'id_model' => 1962,
                'id_fuel' => 2,
            ),
            352 => 
            array (
                'id_reg' => 2353,
                'id_model' => 1963,
                'id_fuel' => 2,
            ),
            353 => 
            array (
                'id_reg' => 2354,
                'id_model' => 1963,
                'id_fuel' => 4,
            ),
            354 => 
            array (
                'id_reg' => 2355,
                'id_model' => 1964,
                'id_fuel' => 4,
            ),
            355 => 
            array (
                'id_reg' => 2356,
                'id_model' => 1965,
                'id_fuel' => 4,
            ),
            356 => 
            array (
                'id_reg' => 2357,
                'id_model' => 1966,
                'id_fuel' => 4,
            ),
            357 => 
            array (
                'id_reg' => 2358,
                'id_model' => 1967,
                'id_fuel' => 4,
            ),
            358 => 
            array (
                'id_reg' => 2359,
                'id_model' => 1968,
                'id_fuel' => 4,
            ),
            359 => 
            array (
                'id_reg' => 2360,
                'id_model' => 1969,
                'id_fuel' => 4,
            ),
            360 => 
            array (
                'id_reg' => 2361,
                'id_model' => 1970,
                'id_fuel' => 4,
            ),
            361 => 
            array (
                'id_reg' => 2362,
                'id_model' => 1971,
                'id_fuel' => 4,
            ),
            362 => 
            array (
                'id_reg' => 2363,
                'id_model' => 1972,
                'id_fuel' => 4,
            ),
            363 => 
            array (
                'id_reg' => 2364,
                'id_model' => 1973,
                'id_fuel' => 4,
            ),
            364 => 
            array (
                'id_reg' => 2365,
                'id_model' => 1974,
                'id_fuel' => 4,
            ),
            365 => 
            array (
                'id_reg' => 2366,
                'id_model' => 1975,
                'id_fuel' => 4,
            ),
            366 => 
            array (
                'id_reg' => 2367,
                'id_model' => 1976,
                'id_fuel' => 4,
            ),
            367 => 
            array (
                'id_reg' => 2368,
                'id_model' => 1977,
                'id_fuel' => 4,
            ),
            368 => 
            array (
                'id_reg' => 2369,
                'id_model' => 1978,
                'id_fuel' => 4,
            ),
            369 => 
            array (
                'id_reg' => 2370,
                'id_model' => 1979,
                'id_fuel' => 2,
            ),
            370 => 
            array (
                'id_reg' => 2371,
                'id_model' => 1979,
                'id_fuel' => 4,
            ),
            371 => 
            array (
                'id_reg' => 2372,
                'id_model' => 1980,
                'id_fuel' => 4,
            ),
            372 => 
            array (
                'id_reg' => 2373,
                'id_model' => 1981,
                'id_fuel' => 4,
            ),
            373 => 
            array (
                'id_reg' => 2374,
                'id_model' => 1982,
                'id_fuel' => 4,
            ),
            374 => 
            array (
                'id_reg' => 2375,
                'id_model' => 1983,
                'id_fuel' => 2,
            ),
            375 => 
            array (
                'id_reg' => 2376,
                'id_model' => 1984,
                'id_fuel' => 2,
            ),
            376 => 
            array (
                'id_reg' => 2377,
                'id_model' => 1984,
                'id_fuel' => 4,
            ),
            377 => 
            array (
                'id_reg' => 2378,
                'id_model' => 1985,
                'id_fuel' => 2,
            ),
            378 => 
            array (
                'id_reg' => 2379,
                'id_model' => 1985,
                'id_fuel' => 4,
            ),
            379 => 
            array (
                'id_reg' => 2380,
                'id_model' => 1986,
                'id_fuel' => 2,
            ),
            380 => 
            array (
                'id_reg' => 2381,
                'id_model' => 1986,
                'id_fuel' => 4,
            ),
            381 => 
            array (
                'id_reg' => 2382,
                'id_model' => 1987,
                'id_fuel' => 4,
            ),
            382 => 
            array (
                'id_reg' => 2383,
                'id_model' => 1988,
                'id_fuel' => 4,
            ),
            383 => 
            array (
                'id_reg' => 2384,
                'id_model' => 1989,
                'id_fuel' => 4,
            ),
            384 => 
            array (
                'id_reg' => 2385,
                'id_model' => 1990,
                'id_fuel' => 4,
            ),
            385 => 
            array (
                'id_reg' => 2386,
                'id_model' => 1991,
                'id_fuel' => 4,
            ),
            386 => 
            array (
                'id_reg' => 2387,
                'id_model' => 1992,
                'id_fuel' => 4,
            ),
            387 => 
            array (
                'id_reg' => 2388,
                'id_model' => 1993,
                'id_fuel' => 4,
            ),
            388 => 
            array (
                'id_reg' => 2389,
                'id_model' => 1994,
                'id_fuel' => 4,
            ),
            389 => 
            array (
                'id_reg' => 2390,
                'id_model' => 1995,
                'id_fuel' => 4,
            ),
            390 => 
            array (
                'id_reg' => 2391,
                'id_model' => 1996,
                'id_fuel' => 4,
            ),
            391 => 
            array (
                'id_reg' => 2392,
                'id_model' => 1997,
                'id_fuel' => 4,
            ),
            392 => 
            array (
                'id_reg' => 2393,
                'id_model' => 1998,
                'id_fuel' => 4,
            ),
            393 => 
            array (
                'id_reg' => 2394,
                'id_model' => 1999,
                'id_fuel' => 2,
            ),
            394 => 
            array (
                'id_reg' => 2395,
                'id_model' => 2000,
                'id_fuel' => 2,
            ),
            395 => 
            array (
                'id_reg' => 2396,
                'id_model' => 2001,
                'id_fuel' => 2,
            ),
            396 => 
            array (
                'id_reg' => 2397,
                'id_model' => 2002,
                'id_fuel' => 2,
            ),
            397 => 
            array (
                'id_reg' => 2398,
                'id_model' => 2003,
                'id_fuel' => 2,
            ),
            398 => 
            array (
                'id_reg' => 2399,
                'id_model' => 2004,
                'id_fuel' => 2,
            ),
            399 => 
            array (
                'id_reg' => 2400,
                'id_model' => 2005,
                'id_fuel' => 2,
            ),
            400 => 
            array (
                'id_reg' => 2401,
                'id_model' => 2006,
                'id_fuel' => 2,
            ),
            401 => 
            array (
                'id_reg' => 2402,
                'id_model' => 2007,
                'id_fuel' => 2,
            ),
            402 => 
            array (
                'id_reg' => 2403,
                'id_model' => 2008,
                'id_fuel' => 2,
            ),
            403 => 
            array (
                'id_reg' => 2404,
                'id_model' => 2009,
                'id_fuel' => 2,
            ),
            404 => 
            array (
                'id_reg' => 2405,
                'id_model' => 2010,
                'id_fuel' => 2,
            ),
            405 => 
            array (
                'id_reg' => 2406,
                'id_model' => 2011,
                'id_fuel' => 2,
            ),
            406 => 
            array (
                'id_reg' => 2407,
                'id_model' => 2012,
                'id_fuel' => 2,
            ),
            407 => 
            array (
                'id_reg' => 2408,
                'id_model' => 2013,
                'id_fuel' => 2,
            ),
            408 => 
            array (
                'id_reg' => 2409,
                'id_model' => 2013,
                'id_fuel' => 4,
            ),
            409 => 
            array (
                'id_reg' => 2410,
                'id_model' => 2014,
                'id_fuel' => 2,
            ),
            410 => 
            array (
                'id_reg' => 2411,
                'id_model' => 2015,
                'id_fuel' => 2,
            ),
            411 => 
            array (
                'id_reg' => 2412,
                'id_model' => 2016,
                'id_fuel' => 2,
            ),
            412 => 
            array (
                'id_reg' => 2413,
                'id_model' => 2016,
                'id_fuel' => 4,
            ),
            413 => 
            array (
                'id_reg' => 2414,
                'id_model' => 2017,
                'id_fuel' => 2,
            ),
            414 => 
            array (
                'id_reg' => 2415,
                'id_model' => 2018,
                'id_fuel' => 2,
            ),
            415 => 
            array (
                'id_reg' => 2416,
                'id_model' => 2019,
                'id_fuel' => 2,
            ),
            416 => 
            array (
                'id_reg' => 2417,
                'id_model' => 2020,
                'id_fuel' => 2,
            ),
            417 => 
            array (
                'id_reg' => 2418,
                'id_model' => 2021,
                'id_fuel' => 2,
            ),
            418 => 
            array (
                'id_reg' => 2419,
                'id_model' => 2022,
                'id_fuel' => 2,
            ),
            419 => 
            array (
                'id_reg' => 2420,
                'id_model' => 2023,
                'id_fuel' => 2,
            ),
            420 => 
            array (
                'id_reg' => 2421,
                'id_model' => 2024,
                'id_fuel' => 2,
            ),
            421 => 
            array (
                'id_reg' => 2422,
                'id_model' => 2025,
                'id_fuel' => 4,
            ),
            422 => 
            array (
                'id_reg' => 2423,
                'id_model' => 2026,
                'id_fuel' => 2,
            ),
            423 => 
            array (
                'id_reg' => 2424,
                'id_model' => 2026,
                'id_fuel' => 4,
            ),
            424 => 
            array (
                'id_reg' => 2425,
                'id_model' => 2026,
                'id_fuel' => 5,
            ),
            425 => 
            array (
                'id_reg' => 2426,
                'id_model' => 2027,
                'id_fuel' => 2,
            ),
            426 => 
            array (
                'id_reg' => 2427,
                'id_model' => 2027,
                'id_fuel' => 4,
            ),
            427 => 
            array (
                'id_reg' => 2428,
                'id_model' => 2028,
                'id_fuel' => 2,
            ),
            428 => 
            array (
                'id_reg' => 2429,
                'id_model' => 2028,
                'id_fuel' => 4,
            ),
            429 => 
            array (
                'id_reg' => 2430,
                'id_model' => 2029,
                'id_fuel' => 6,
            ),
            430 => 
            array (
                'id_reg' => 2431,
                'id_model' => 2030,
                'id_fuel' => 4,
            ),
            431 => 
            array (
                'id_reg' => 2432,
                'id_model' => 2031,
                'id_fuel' => 2,
            ),
            432 => 
            array (
                'id_reg' => 2433,
                'id_model' => 2032,
                'id_fuel' => 2,
            ),
            433 => 
            array (
                'id_reg' => 2434,
                'id_model' => 2032,
                'id_fuel' => 4,
            ),
            434 => 
            array (
                'id_reg' => 2435,
                'id_model' => 2033,
                'id_fuel' => 2,
            ),
            435 => 
            array (
                'id_reg' => 2436,
                'id_model' => 2034,
                'id_fuel' => 2,
            ),
            436 => 
            array (
                'id_reg' => 2437,
                'id_model' => 2035,
                'id_fuel' => 2,
            ),
            437 => 
            array (
                'id_reg' => 2438,
                'id_model' => 2036,
                'id_fuel' => 2,
            ),
            438 => 
            array (
                'id_reg' => 2439,
                'id_model' => 2036,
                'id_fuel' => 4,
            ),
            439 => 
            array (
                'id_reg' => 2440,
                'id_model' => 2037,
                'id_fuel' => 4,
            ),
            440 => 
            array (
                'id_reg' => 2441,
                'id_model' => 2038,
                'id_fuel' => 2,
            ),
            441 => 
            array (
                'id_reg' => 2442,
                'id_model' => 2038,
                'id_fuel' => 4,
            ),
            442 => 
            array (
                'id_reg' => 2443,
                'id_model' => 2039,
                'id_fuel' => 4,
            ),
            443 => 
            array (
                'id_reg' => 2444,
                'id_model' => 2040,
                'id_fuel' => 2,
            ),
            444 => 
            array (
                'id_reg' => 2445,
                'id_model' => 2040,
                'id_fuel' => 4,
            ),
            445 => 
            array (
                'id_reg' => 2446,
                'id_model' => 2041,
                'id_fuel' => 2,
            ),
            446 => 
            array (
                'id_reg' => 2447,
                'id_model' => 2042,
                'id_fuel' => 4,
            ),
            447 => 
            array (
                'id_reg' => 2448,
                'id_model' => 2043,
                'id_fuel' => 4,
            ),
            448 => 
            array (
                'id_reg' => 2449,
                'id_model' => 2044,
                'id_fuel' => 4,
            ),
            449 => 
            array (
                'id_reg' => 2450,
                'id_model' => 2045,
                'id_fuel' => 4,
            ),
            450 => 
            array (
                'id_reg' => 2451,
                'id_model' => 2046,
                'id_fuel' => 4,
            ),
            451 => 
            array (
                'id_reg' => 2452,
                'id_model' => 2047,
                'id_fuel' => 4,
            ),
            452 => 
            array (
                'id_reg' => 2453,
                'id_model' => 2048,
                'id_fuel' => 4,
            ),
            453 => 
            array (
                'id_reg' => 2454,
                'id_model' => 2049,
                'id_fuel' => 2,
            ),
            454 => 
            array (
                'id_reg' => 2455,
                'id_model' => 2049,
                'id_fuel' => 4,
            ),
            455 => 
            array (
                'id_reg' => 2456,
                'id_model' => 2050,
                'id_fuel' => 4,
            ),
            456 => 
            array (
                'id_reg' => 2457,
                'id_model' => 2051,
                'id_fuel' => 2,
            ),
            457 => 
            array (
                'id_reg' => 2458,
                'id_model' => 2051,
                'id_fuel' => 4,
            ),
            458 => 
            array (
                'id_reg' => 2459,
                'id_model' => 2052,
                'id_fuel' => 4,
            ),
            459 => 
            array (
                'id_reg' => 2460,
                'id_model' => 2053,
                'id_fuel' => 6,
            ),
            460 => 
            array (
                'id_reg' => 2461,
                'id_model' => 2053,
                'id_fuel' => 4,
            ),
            461 => 
            array (
                'id_reg' => 2462,
                'id_model' => 2054,
                'id_fuel' => 2,
            ),
            462 => 
            array (
                'id_reg' => 2463,
                'id_model' => 2055,
                'id_fuel' => 2,
            ),
            463 => 
            array (
                'id_reg' => 2464,
                'id_model' => 2056,
                'id_fuel' => 2,
            ),
            464 => 
            array (
                'id_reg' => 2465,
                'id_model' => 2056,
                'id_fuel' => 4,
            ),
            465 => 
            array (
                'id_reg' => 2466,
                'id_model' => 2057,
                'id_fuel' => 2,
            ),
            466 => 
            array (
                'id_reg' => 2467,
                'id_model' => 2057,
                'id_fuel' => 4,
            ),
            467 => 
            array (
                'id_reg' => 2468,
                'id_model' => 2058,
                'id_fuel' => 2,
            ),
            468 => 
            array (
                'id_reg' => 2469,
                'id_model' => 2058,
                'id_fuel' => 4,
            ),
            469 => 
            array (
                'id_reg' => 2470,
                'id_model' => 2059,
                'id_fuel' => 4,
            ),
            470 => 
            array (
                'id_reg' => 2471,
                'id_model' => 2060,
                'id_fuel' => 2,
            ),
            471 => 
            array (
                'id_reg' => 2472,
                'id_model' => 2060,
                'id_fuel' => 4,
            ),
            472 => 
            array (
                'id_reg' => 2473,
                'id_model' => 2061,
                'id_fuel' => 4,
            ),
            473 => 
            array (
                'id_reg' => 2474,
                'id_model' => 2062,
                'id_fuel' => 2,
            ),
            474 => 
            array (
                'id_reg' => 2475,
                'id_model' => 2062,
                'id_fuel' => 4,
            ),
            475 => 
            array (
                'id_reg' => 2476,
                'id_model' => 2063,
                'id_fuel' => 2,
            ),
            476 => 
            array (
                'id_reg' => 2477,
                'id_model' => 2064,
                'id_fuel' => 2,
            ),
            477 => 
            array (
                'id_reg' => 2478,
                'id_model' => 2064,
                'id_fuel' => 4,
            ),
            478 => 
            array (
                'id_reg' => 2479,
                'id_model' => 2064,
                'id_fuel' => 5,
            ),
            479 => 
            array (
                'id_reg' => 2480,
                'id_model' => 2065,
                'id_fuel' => 2,
            ),
            480 => 
            array (
                'id_reg' => 2481,
                'id_model' => 2065,
                'id_fuel' => 4,
            ),
            481 => 
            array (
                'id_reg' => 2482,
                'id_model' => 2066,
                'id_fuel' => 4,
            ),
            482 => 
            array (
                'id_reg' => 2483,
                'id_model' => 2067,
                'id_fuel' => 4,
            ),
            483 => 
            array (
                'id_reg' => 2484,
                'id_model' => 2068,
                'id_fuel' => 4,
            ),
            484 => 
            array (
                'id_reg' => 2485,
                'id_model' => 2069,
                'id_fuel' => 2,
            ),
            485 => 
            array (
                'id_reg' => 2486,
                'id_model' => 2069,
                'id_fuel' => 4,
            ),
            486 => 
            array (
                'id_reg' => 2487,
                'id_model' => 2070,
                'id_fuel' => 4,
            ),
            487 => 
            array (
                'id_reg' => 2488,
                'id_model' => 2071,
                'id_fuel' => 2,
            ),
            488 => 
            array (
                'id_reg' => 2489,
                'id_model' => 2071,
                'id_fuel' => 4,
            ),
            489 => 
            array (
                'id_reg' => 2490,
                'id_model' => 2072,
                'id_fuel' => 2,
            ),
            490 => 
            array (
                'id_reg' => 2491,
                'id_model' => 2072,
                'id_fuel' => 4,
            ),
            491 => 
            array (
                'id_reg' => 2492,
                'id_model' => 2073,
                'id_fuel' => 2,
            ),
            492 => 
            array (
                'id_reg' => 2493,
                'id_model' => 2073,
                'id_fuel' => 4,
            ),
            493 => 
            array (
                'id_reg' => 2494,
                'id_model' => 2074,
                'id_fuel' => 2,
            ),
            494 => 
            array (
                'id_reg' => 2495,
                'id_model' => 2074,
                'id_fuel' => 4,
            ),
            495 => 
            array (
                'id_reg' => 2496,
                'id_model' => 2075,
                'id_fuel' => 2,
            ),
            496 => 
            array (
                'id_reg' => 2497,
                'id_model' => 2075,
                'id_fuel' => 4,
            ),
            497 => 
            array (
                'id_reg' => 2498,
                'id_model' => 2076,
                'id_fuel' => 2,
            ),
            498 => 
            array (
                'id_reg' => 2499,
                'id_model' => 2076,
                'id_fuel' => 4,
            ),
            499 => 
            array (
                'id_reg' => 2500,
                'id_model' => 2077,
                'id_fuel' => 4,
            ),
        ));
        \DB::table('model_rel_fuel')->insert(array (
            0 => 
            array (
                'id_reg' => 2501,
                'id_model' => 2078,
                'id_fuel' => 4,
            ),
            1 => 
            array (
                'id_reg' => 2502,
                'id_model' => 2079,
                'id_fuel' => 4,
            ),
            2 => 
            array (
                'id_reg' => 2503,
                'id_model' => 2080,
                'id_fuel' => 2,
            ),
            3 => 
            array (
                'id_reg' => 2504,
                'id_model' => 2080,
                'id_fuel' => 4,
            ),
            4 => 
            array (
                'id_reg' => 2505,
                'id_model' => 2081,
                'id_fuel' => 4,
            ),
            5 => 
            array (
                'id_reg' => 2506,
                'id_model' => 2082,
                'id_fuel' => 4,
            ),
            6 => 
            array (
                'id_reg' => 2507,
                'id_model' => 2083,
                'id_fuel' => 4,
            ),
            7 => 
            array (
                'id_reg' => 2508,
                'id_model' => 2084,
                'id_fuel' => 4,
            ),
            8 => 
            array (
                'id_reg' => 2509,
                'id_model' => 2085,
                'id_fuel' => 4,
            ),
            9 => 
            array (
                'id_reg' => 2510,
                'id_model' => 2087,
                'id_fuel' => 4,
            ),
            10 => 
            array (
                'id_reg' => 2511,
                'id_model' => 2088,
                'id_fuel' => 4,
            ),
            11 => 
            array (
                'id_reg' => 2512,
                'id_model' => 2089,
                'id_fuel' => 4,
            ),
            12 => 
            array (
                'id_reg' => 2513,
                'id_model' => 2090,
                'id_fuel' => 4,
            ),
            13 => 
            array (
                'id_reg' => 2514,
                'id_model' => 2091,
                'id_fuel' => 4,
            ),
            14 => 
            array (
                'id_reg' => 2515,
                'id_model' => 2092,
                'id_fuel' => 4,
            ),
            15 => 
            array (
                'id_reg' => 2516,
                'id_model' => 2093,
                'id_fuel' => 4,
            ),
            16 => 
            array (
                'id_reg' => 2517,
                'id_model' => 2094,
                'id_fuel' => 4,
            ),
            17 => 
            array (
                'id_reg' => 2518,
                'id_model' => 2095,
                'id_fuel' => 4,
            ),
            18 => 
            array (
                'id_reg' => 2519,
                'id_model' => 2096,
                'id_fuel' => 4,
            ),
            19 => 
            array (
                'id_reg' => 2520,
                'id_model' => 2097,
                'id_fuel' => 4,
            ),
            20 => 
            array (
                'id_reg' => 2521,
                'id_model' => 2098,
                'id_fuel' => 2,
            ),
            21 => 
            array (
                'id_reg' => 2522,
                'id_model' => 2098,
                'id_fuel' => 4,
            ),
            22 => 
            array (
                'id_reg' => 2523,
                'id_model' => 2099,
                'id_fuel' => 4,
            ),
            23 => 
            array (
                'id_reg' => 2524,
                'id_model' => 2100,
                'id_fuel' => 4,
            ),
            24 => 
            array (
                'id_reg' => 2525,
                'id_model' => 2101,
                'id_fuel' => 4,
            ),
            25 => 
            array (
                'id_reg' => 2526,
                'id_model' => 2102,
                'id_fuel' => 4,
            ),
            26 => 
            array (
                'id_reg' => 2527,
                'id_model' => 2103,
                'id_fuel' => 4,
            ),
            27 => 
            array (
                'id_reg' => 2528,
                'id_model' => 2104,
                'id_fuel' => 4,
            ),
            28 => 
            array (
                'id_reg' => 2529,
                'id_model' => 2105,
                'id_fuel' => 4,
            ),
            29 => 
            array (
                'id_reg' => 2530,
                'id_model' => 2106,
                'id_fuel' => 4,
            ),
            30 => 
            array (
                'id_reg' => 2531,
                'id_model' => 2107,
                'id_fuel' => 4,
            ),
            31 => 
            array (
                'id_reg' => 2532,
                'id_model' => 2108,
                'id_fuel' => 4,
            ),
            32 => 
            array (
                'id_reg' => 2533,
                'id_model' => 2109,
                'id_fuel' => 2,
            ),
            33 => 
            array (
                'id_reg' => 2534,
                'id_model' => 2110,
                'id_fuel' => 6,
            ),
            34 => 
            array (
                'id_reg' => 2535,
                'id_model' => 2111,
                'id_fuel' => 4,
            ),
            35 => 
            array (
                'id_reg' => 2536,
                'id_model' => 2112,
                'id_fuel' => 4,
            ),
            36 => 
            array (
                'id_reg' => 2537,
                'id_model' => 2113,
                'id_fuel' => 6,
            ),
            37 => 
            array (
                'id_reg' => 2538,
                'id_model' => 2114,
                'id_fuel' => 4,
            ),
            38 => 
            array (
                'id_reg' => 2539,
                'id_model' => 2115,
                'id_fuel' => 4,
            ),
            39 => 
            array (
                'id_reg' => 2540,
                'id_model' => 2116,
                'id_fuel' => 4,
            ),
            40 => 
            array (
                'id_reg' => 2541,
                'id_model' => 2117,
                'id_fuel' => 4,
            ),
            41 => 
            array (
                'id_reg' => 2542,
                'id_model' => 2118,
                'id_fuel' => 4,
            ),
            42 => 
            array (
                'id_reg' => 2543,
                'id_model' => 2119,
                'id_fuel' => 4,
            ),
            43 => 
            array (
                'id_reg' => 2544,
                'id_model' => 2120,
                'id_fuel' => 4,
            ),
            44 => 
            array (
                'id_reg' => 2545,
                'id_model' => 2121,
                'id_fuel' => 4,
            ),
            45 => 
            array (
                'id_reg' => 2546,
                'id_model' => 2122,
                'id_fuel' => 4,
            ),
            46 => 
            array (
                'id_reg' => 2547,
                'id_model' => 2123,
                'id_fuel' => 4,
            ),
            47 => 
            array (
                'id_reg' => 2548,
                'id_model' => 2124,
                'id_fuel' => 4,
            ),
            48 => 
            array (
                'id_reg' => 2549,
                'id_model' => 2125,
                'id_fuel' => 4,
            ),
            49 => 
            array (
                'id_reg' => 2550,
                'id_model' => 2126,
                'id_fuel' => 4,
            ),
            50 => 
            array (
                'id_reg' => 2551,
                'id_model' => 2127,
                'id_fuel' => 4,
            ),
            51 => 
            array (
                'id_reg' => 2552,
                'id_model' => 2128,
                'id_fuel' => 4,
            ),
            52 => 
            array (
                'id_reg' => 2553,
                'id_model' => 2129,
                'id_fuel' => 2,
            ),
            53 => 
            array (
                'id_reg' => 2554,
                'id_model' => 2130,
                'id_fuel' => 2,
            ),
            54 => 
            array (
                'id_reg' => 2555,
                'id_model' => 2130,
                'id_fuel' => 4,
            ),
            55 => 
            array (
                'id_reg' => 2556,
                'id_model' => 2131,
                'id_fuel' => 2,
            ),
            56 => 
            array (
                'id_reg' => 2557,
                'id_model' => 2132,
                'id_fuel' => 2,
            ),
            57 => 
            array (
                'id_reg' => 2558,
                'id_model' => 2133,
                'id_fuel' => 2,
            ),
            58 => 
            array (
                'id_reg' => 2559,
                'id_model' => 2133,
                'id_fuel' => 4,
            ),
            59 => 
            array (
                'id_reg' => 2560,
                'id_model' => 2134,
                'id_fuel' => 2,
            ),
            60 => 
            array (
                'id_reg' => 2561,
                'id_model' => 2134,
                'id_fuel' => 4,
            ),
            61 => 
            array (
                'id_reg' => 2562,
                'id_model' => 2135,
                'id_fuel' => 2,
            ),
            62 => 
            array (
                'id_reg' => 2563,
                'id_model' => 2135,
                'id_fuel' => 4,
            ),
            63 => 
            array (
                'id_reg' => 2564,
                'id_model' => 2136,
                'id_fuel' => 2,
            ),
            64 => 
            array (
                'id_reg' => 2565,
                'id_model' => 2137,
                'id_fuel' => 2,
            ),
            65 => 
            array (
                'id_reg' => 2566,
                'id_model' => 2138,
                'id_fuel' => 2,
            ),
            66 => 
            array (
                'id_reg' => 2567,
                'id_model' => 2139,
                'id_fuel' => 2,
            ),
            67 => 
            array (
                'id_reg' => 2568,
                'id_model' => 2140,
                'id_fuel' => 6,
            ),
            68 => 
            array (
                'id_reg' => 2569,
                'id_model' => 2141,
                'id_fuel' => 2,
            ),
            69 => 
            array (
                'id_reg' => 2570,
                'id_model' => 2142,
                'id_fuel' => 2,
            ),
            70 => 
            array (
                'id_reg' => 2571,
                'id_model' => 2142,
                'id_fuel' => 4,
            ),
            71 => 
            array (
                'id_reg' => 2572,
                'id_model' => 2143,
                'id_fuel' => 2,
            ),
            72 => 
            array (
                'id_reg' => 2573,
                'id_model' => 2143,
                'id_fuel' => 6,
            ),
            73 => 
            array (
                'id_reg' => 2574,
                'id_model' => 2143,
                'id_fuel' => 4,
            ),
            74 => 
            array (
                'id_reg' => 2575,
                'id_model' => 2144,
                'id_fuel' => 2,
            ),
            75 => 
            array (
                'id_reg' => 2576,
                'id_model' => 2145,
                'id_fuel' => 2,
            ),
            76 => 
            array (
                'id_reg' => 2577,
                'id_model' => 2146,
                'id_fuel' => 2,
            ),
            77 => 
            array (
                'id_reg' => 2578,
                'id_model' => 2147,
                'id_fuel' => 2,
            ),
            78 => 
            array (
                'id_reg' => 2579,
                'id_model' => 2148,
                'id_fuel' => 4,
            ),
            79 => 
            array (
                'id_reg' => 2580,
                'id_model' => 2149,
                'id_fuel' => 4,
            ),
            80 => 
            array (
                'id_reg' => 2581,
                'id_model' => 2150,
                'id_fuel' => 2,
            ),
            81 => 
            array (
                'id_reg' => 2582,
                'id_model' => 2150,
                'id_fuel' => 4,
            ),
            82 => 
            array (
                'id_reg' => 2583,
                'id_model' => 2151,
                'id_fuel' => 4,
            ),
            83 => 
            array (
                'id_reg' => 2584,
                'id_model' => 2152,
                'id_fuel' => 4,
            ),
            84 => 
            array (
                'id_reg' => 2585,
                'id_model' => 2153,
                'id_fuel' => 2,
            ),
            85 => 
            array (
                'id_reg' => 2586,
                'id_model' => 2154,
                'id_fuel' => 2,
            ),
            86 => 
            array (
                'id_reg' => 2587,
                'id_model' => 2155,
                'id_fuel' => 2,
            ),
            87 => 
            array (
                'id_reg' => 2588,
                'id_model' => 2155,
                'id_fuel' => 4,
            ),
            88 => 
            array (
                'id_reg' => 2589,
                'id_model' => 2156,
                'id_fuel' => 2,
            ),
            89 => 
            array (
                'id_reg' => 2590,
                'id_model' => 2157,
                'id_fuel' => 2,
            ),
            90 => 
            array (
                'id_reg' => 2591,
                'id_model' => 2157,
                'id_fuel' => 4,
            ),
            91 => 
            array (
                'id_reg' => 2592,
                'id_model' => 2158,
                'id_fuel' => 2,
            ),
            92 => 
            array (
                'id_reg' => 2593,
                'id_model' => 2159,
                'id_fuel' => 2,
            ),
            93 => 
            array (
                'id_reg' => 2594,
                'id_model' => 2160,
                'id_fuel' => 2,
            ),
            94 => 
            array (
                'id_reg' => 2595,
                'id_model' => 2161,
                'id_fuel' => 2,
            ),
            95 => 
            array (
                'id_reg' => 2596,
                'id_model' => 2162,
                'id_fuel' => 2,
            ),
            96 => 
            array (
                'id_reg' => 2597,
                'id_model' => 2162,
                'id_fuel' => 4,
            ),
            97 => 
            array (
                'id_reg' => 2598,
                'id_model' => 2163,
                'id_fuel' => 6,
            ),
            98 => 
            array (
                'id_reg' => 2599,
                'id_model' => 2164,
                'id_fuel' => 4,
            ),
            99 => 
            array (
                'id_reg' => 2600,
                'id_model' => 2165,
                'id_fuel' => 4,
            ),
            100 => 
            array (
                'id_reg' => 2601,
                'id_model' => 2166,
                'id_fuel' => 2,
            ),
            101 => 
            array (
                'id_reg' => 2602,
                'id_model' => 2167,
                'id_fuel' => 2,
            ),
            102 => 
            array (
                'id_reg' => 2603,
                'id_model' => 2167,
                'id_fuel' => 4,
            ),
            103 => 
            array (
                'id_reg' => 2604,
                'id_model' => 2168,
                'id_fuel' => 2,
            ),
            104 => 
            array (
                'id_reg' => 2605,
                'id_model' => 2169,
                'id_fuel' => 2,
            ),
            105 => 
            array (
                'id_reg' => 2606,
                'id_model' => 2170,
                'id_fuel' => 2,
            ),
            106 => 
            array (
                'id_reg' => 2607,
                'id_model' => 2171,
                'id_fuel' => 2,
            ),
            107 => 
            array (
                'id_reg' => 2608,
                'id_model' => 2172,
                'id_fuel' => 2,
            ),
            108 => 
            array (
                'id_reg' => 2609,
                'id_model' => 2173,
                'id_fuel' => 2,
            ),
            109 => 
            array (
                'id_reg' => 2610,
                'id_model' => 2174,
                'id_fuel' => 2,
            ),
            110 => 
            array (
                'id_reg' => 2611,
                'id_model' => 2174,
                'id_fuel' => 6,
            ),
            111 => 
            array (
                'id_reg' => 2612,
                'id_model' => 2174,
                'id_fuel' => 4,
            ),
            112 => 
            array (
                'id_reg' => 2613,
                'id_model' => 2175,
                'id_fuel' => 2,
            ),
            113 => 
            array (
                'id_reg' => 2614,
                'id_model' => 2176,
                'id_fuel' => 2,
            ),
            114 => 
            array (
                'id_reg' => 2615,
                'id_model' => 2176,
                'id_fuel' => 4,
            ),
            115 => 
            array (
                'id_reg' => 2616,
                'id_model' => 2177,
                'id_fuel' => 2,
            ),
            116 => 
            array (
                'id_reg' => 2617,
                'id_model' => 2177,
                'id_fuel' => 4,
            ),
            117 => 
            array (
                'id_reg' => 2618,
                'id_model' => 2178,
                'id_fuel' => 2,
            ),
            118 => 
            array (
                'id_reg' => 2619,
                'id_model' => 2178,
                'id_fuel' => 4,
            ),
            119 => 
            array (
                'id_reg' => 2620,
                'id_model' => 2179,
                'id_fuel' => 4,
            ),
            120 => 
            array (
                'id_reg' => 2621,
                'id_model' => 2180,
                'id_fuel' => 4,
            ),
            121 => 
            array (
                'id_reg' => 2622,
                'id_model' => 2181,
                'id_fuel' => 2,
            ),
            122 => 
            array (
                'id_reg' => 2623,
                'id_model' => 2182,
                'id_fuel' => 2,
            ),
            123 => 
            array (
                'id_reg' => 2624,
                'id_model' => 2182,
                'id_fuel' => 4,
            ),
            124 => 
            array (
                'id_reg' => 2625,
                'id_model' => 2183,
                'id_fuel' => 4,
            ),
            125 => 
            array (
                'id_reg' => 2626,
                'id_model' => 2184,
                'id_fuel' => 4,
            ),
            126 => 
            array (
                'id_reg' => 2627,
                'id_model' => 2185,
                'id_fuel' => 2,
            ),
            127 => 
            array (
                'id_reg' => 2628,
                'id_model' => 2185,
                'id_fuel' => 4,
            ),
            128 => 
            array (
                'id_reg' => 2629,
                'id_model' => 2186,
                'id_fuel' => 2,
            ),
            129 => 
            array (
                'id_reg' => 2630,
                'id_model' => 2186,
                'id_fuel' => 4,
            ),
            130 => 
            array (
                'id_reg' => 2631,
                'id_model' => 2187,
                'id_fuel' => 4,
            ),
            131 => 
            array (
                'id_reg' => 2632,
                'id_model' => 2188,
                'id_fuel' => 4,
            ),
            132 => 
            array (
                'id_reg' => 2633,
                'id_model' => 2189,
                'id_fuel' => 4,
            ),
            133 => 
            array (
                'id_reg' => 2634,
                'id_model' => 2190,
                'id_fuel' => 2,
            ),
            134 => 
            array (
                'id_reg' => 2635,
                'id_model' => 2190,
                'id_fuel' => 4,
            ),
            135 => 
            array (
                'id_reg' => 2636,
                'id_model' => 2191,
                'id_fuel' => 4,
            ),
            136 => 
            array (
                'id_reg' => 2637,
                'id_model' => 2192,
                'id_fuel' => 2,
            ),
            137 => 
            array (
                'id_reg' => 2638,
                'id_model' => 2192,
                'id_fuel' => 4,
            ),
            138 => 
            array (
                'id_reg' => 2639,
                'id_model' => 2193,
                'id_fuel' => 2,
            ),
            139 => 
            array (
                'id_reg' => 2640,
                'id_model' => 2193,
                'id_fuel' => 4,
            ),
            140 => 
            array (
                'id_reg' => 2641,
                'id_model' => 2194,
                'id_fuel' => 4,
            ),
            141 => 
            array (
                'id_reg' => 2642,
                'id_model' => 2195,
                'id_fuel' => 2,
            ),
            142 => 
            array (
                'id_reg' => 2643,
                'id_model' => 2196,
                'id_fuel' => 4,
            ),
            143 => 
            array (
                'id_reg' => 2644,
                'id_model' => 2197,
                'id_fuel' => 2,
            ),
            144 => 
            array (
                'id_reg' => 2645,
                'id_model' => 2197,
                'id_fuel' => 4,
            ),
            145 => 
            array (
                'id_reg' => 2646,
                'id_model' => 2198,
                'id_fuel' => 4,
            ),
            146 => 
            array (
                'id_reg' => 2647,
                'id_model' => 2199,
                'id_fuel' => 2,
            ),
            147 => 
            array (
                'id_reg' => 2648,
                'id_model' => 2199,
                'id_fuel' => 4,
            ),
            148 => 
            array (
                'id_reg' => 2649,
                'id_model' => 2200,
                'id_fuel' => 2,
            ),
            149 => 
            array (
                'id_reg' => 2650,
                'id_model' => 2200,
                'id_fuel' => 4,
            ),
            150 => 
            array (
                'id_reg' => 2651,
                'id_model' => 2201,
                'id_fuel' => 4,
            ),
            151 => 
            array (
                'id_reg' => 2652,
                'id_model' => 2202,
                'id_fuel' => 4,
            ),
            152 => 
            array (
                'id_reg' => 2653,
                'id_model' => 2203,
                'id_fuel' => 4,
            ),
            153 => 
            array (
                'id_reg' => 2654,
                'id_model' => 2204,
                'id_fuel' => 4,
            ),
            154 => 
            array (
                'id_reg' => 2655,
                'id_model' => 2205,
                'id_fuel' => 2,
            ),
            155 => 
            array (
                'id_reg' => 2656,
                'id_model' => 2206,
                'id_fuel' => 4,
            ),
            156 => 
            array (
                'id_reg' => 2657,
                'id_model' => 2207,
                'id_fuel' => 2,
            ),
            157 => 
            array (
                'id_reg' => 2658,
                'id_model' => 2208,
                'id_fuel' => 4,
            ),
            158 => 
            array (
                'id_reg' => 2659,
                'id_model' => 2209,
                'id_fuel' => 4,
            ),
            159 => 
            array (
                'id_reg' => 2660,
                'id_model' => 2210,
                'id_fuel' => 4,
            ),
            160 => 
            array (
                'id_reg' => 2661,
                'id_model' => 2211,
                'id_fuel' => 4,
            ),
            161 => 
            array (
                'id_reg' => 2662,
                'id_model' => 2212,
                'id_fuel' => 4,
            ),
            162 => 
            array (
                'id_reg' => 2663,
                'id_model' => 2213,
                'id_fuel' => 4,
            ),
            163 => 
            array (
                'id_reg' => 2664,
                'id_model' => 2214,
                'id_fuel' => 4,
            ),
            164 => 
            array (
                'id_reg' => 2665,
                'id_model' => 2215,
                'id_fuel' => 4,
            ),
            165 => 
            array (
                'id_reg' => 2666,
                'id_model' => 2216,
                'id_fuel' => 4,
            ),
            166 => 
            array (
                'id_reg' => 2667,
                'id_model' => 2217,
                'id_fuel' => 4,
            ),
            167 => 
            array (
                'id_reg' => 2668,
                'id_model' => 2218,
                'id_fuel' => 4,
            ),
            168 => 
            array (
                'id_reg' => 2669,
                'id_model' => 2219,
                'id_fuel' => 4,
            ),
            169 => 
            array (
                'id_reg' => 2670,
                'id_model' => 2220,
                'id_fuel' => 4,
            ),
            170 => 
            array (
                'id_reg' => 2671,
                'id_model' => 2221,
                'id_fuel' => 4,
            ),
            171 => 
            array (
                'id_reg' => 2672,
                'id_model' => 2222,
                'id_fuel' => 4,
            ),
            172 => 
            array (
                'id_reg' => 2673,
                'id_model' => 2223,
                'id_fuel' => 4,
            ),
            173 => 
            array (
                'id_reg' => 2674,
                'id_model' => 2224,
                'id_fuel' => 6,
            ),
            174 => 
            array (
                'id_reg' => 2675,
                'id_model' => 2225,
                'id_fuel' => 4,
            ),
            175 => 
            array (
                'id_reg' => 2676,
                'id_model' => 2226,
                'id_fuel' => 3,
            ),
            176 => 
            array (
                'id_reg' => 2677,
                'id_model' => 2226,
                'id_fuel' => 4,
            ),
            177 => 
            array (
                'id_reg' => 2678,
                'id_model' => 2227,
                'id_fuel' => 4,
            ),
            178 => 
            array (
                'id_reg' => 2679,
                'id_model' => 2228,
                'id_fuel' => 2,
            ),
            179 => 
            array (
                'id_reg' => 2680,
                'id_model' => 2228,
                'id_fuel' => 4,
            ),
            180 => 
            array (
                'id_reg' => 2681,
                'id_model' => 2229,
                'id_fuel' => 2,
            ),
            181 => 
            array (
                'id_reg' => 2682,
                'id_model' => 2229,
                'id_fuel' => 4,
            ),
            182 => 
            array (
                'id_reg' => 2683,
                'id_model' => 2230,
                'id_fuel' => 2,
            ),
            183 => 
            array (
                'id_reg' => 2684,
                'id_model' => 2231,
                'id_fuel' => 2,
            ),
            184 => 
            array (
                'id_reg' => 2685,
                'id_model' => 2231,
                'id_fuel' => 4,
            ),
            185 => 
            array (
                'id_reg' => 2686,
                'id_model' => 2232,
                'id_fuel' => 2,
            ),
            186 => 
            array (
                'id_reg' => 2687,
                'id_model' => 2232,
                'id_fuel' => 4,
            ),
            187 => 
            array (
                'id_reg' => 2688,
                'id_model' => 2233,
                'id_fuel' => 4,
            ),
            188 => 
            array (
                'id_reg' => 2689,
                'id_model' => 2234,
                'id_fuel' => 2,
            ),
            189 => 
            array (
                'id_reg' => 2690,
                'id_model' => 2234,
                'id_fuel' => 4,
            ),
            190 => 
            array (
                'id_reg' => 2691,
                'id_model' => 2235,
                'id_fuel' => 4,
            ),
            191 => 
            array (
                'id_reg' => 2692,
                'id_model' => 2236,
                'id_fuel' => 2,
            ),
            192 => 
            array (
                'id_reg' => 2693,
                'id_model' => 2236,
                'id_fuel' => 4,
            ),
            193 => 
            array (
                'id_reg' => 2694,
                'id_model' => 2237,
                'id_fuel' => 4,
            ),
            194 => 
            array (
                'id_reg' => 2695,
                'id_model' => 2238,
                'id_fuel' => 4,
            ),
            195 => 
            array (
                'id_reg' => 2696,
                'id_model' => 2239,
                'id_fuel' => 4,
            ),
            196 => 
            array (
                'id_reg' => 2697,
                'id_model' => 2240,
                'id_fuel' => 2,
            ),
            197 => 
            array (
                'id_reg' => 2698,
                'id_model' => 2240,
                'id_fuel' => 4,
            ),
            198 => 
            array (
                'id_reg' => 2699,
                'id_model' => 2241,
                'id_fuel' => 4,
            ),
            199 => 
            array (
                'id_reg' => 2700,
                'id_model' => 2242,
                'id_fuel' => 2,
            ),
            200 => 
            array (
                'id_reg' => 2701,
                'id_model' => 2243,
                'id_fuel' => 4,
            ),
            201 => 
            array (
                'id_reg' => 2702,
                'id_model' => 2244,
                'id_fuel' => 4,
            ),
            202 => 
            array (
                'id_reg' => 2703,
                'id_model' => 2245,
                'id_fuel' => 4,
            ),
            203 => 
            array (
                'id_reg' => 2704,
                'id_model' => 2246,
                'id_fuel' => 4,
            ),
            204 => 
            array (
                'id_reg' => 2705,
                'id_model' => 2247,
                'id_fuel' => 2,
            ),
            205 => 
            array (
                'id_reg' => 2706,
                'id_model' => 2247,
                'id_fuel' => 3,
            ),
            206 => 
            array (
                'id_reg' => 2707,
                'id_model' => 2247,
                'id_fuel' => 4,
            ),
            207 => 
            array (
                'id_reg' => 2708,
                'id_model' => 2248,
                'id_fuel' => 2,
            ),
            208 => 
            array (
                'id_reg' => 2709,
                'id_model' => 2248,
                'id_fuel' => 4,
            ),
            209 => 
            array (
                'id_reg' => 2710,
                'id_model' => 2249,
                'id_fuel' => 2,
            ),
            210 => 
            array (
                'id_reg' => 2711,
                'id_model' => 2249,
                'id_fuel' => 4,
            ),
            211 => 
            array (
                'id_reg' => 2712,
                'id_model' => 2250,
                'id_fuel' => 4,
            ),
            212 => 
            array (
                'id_reg' => 2713,
                'id_model' => 2251,
                'id_fuel' => 2,
            ),
            213 => 
            array (
                'id_reg' => 2714,
                'id_model' => 2252,
                'id_fuel' => 4,
            ),
            214 => 
            array (
                'id_reg' => 2715,
                'id_model' => 2253,
                'id_fuel' => 2,
            ),
            215 => 
            array (
                'id_reg' => 2716,
                'id_model' => 2253,
                'id_fuel' => 4,
            ),
            216 => 
            array (
                'id_reg' => 2717,
                'id_model' => 2254,
                'id_fuel' => 4,
            ),
            217 => 
            array (
                'id_reg' => 2718,
                'id_model' => 2255,
                'id_fuel' => 2,
            ),
            218 => 
            array (
                'id_reg' => 2719,
                'id_model' => 2256,
                'id_fuel' => 2,
            ),
            219 => 
            array (
                'id_reg' => 2720,
                'id_model' => 2256,
                'id_fuel' => 4,
            ),
            220 => 
            array (
                'id_reg' => 2721,
                'id_model' => 2257,
                'id_fuel' => 4,
            ),
            221 => 
            array (
                'id_reg' => 2722,
                'id_model' => 2258,
                'id_fuel' => 2,
            ),
            222 => 
            array (
                'id_reg' => 2723,
                'id_model' => 2258,
                'id_fuel' => 4,
            ),
            223 => 
            array (
                'id_reg' => 2724,
                'id_model' => 2259,
                'id_fuel' => 2,
            ),
            224 => 
            array (
                'id_reg' => 2725,
                'id_model' => 2259,
                'id_fuel' => 4,
            ),
            225 => 
            array (
                'id_reg' => 2726,
                'id_model' => 2260,
                'id_fuel' => 4,
            ),
            226 => 
            array (
                'id_reg' => 2727,
                'id_model' => 2261,
                'id_fuel' => 4,
            ),
            227 => 
            array (
                'id_reg' => 2728,
                'id_model' => 2262,
                'id_fuel' => 2,
            ),
            228 => 
            array (
                'id_reg' => 2729,
                'id_model' => 2262,
                'id_fuel' => 4,
            ),
            229 => 
            array (
                'id_reg' => 2730,
                'id_model' => 2263,
                'id_fuel' => 2,
            ),
            230 => 
            array (
                'id_reg' => 2731,
                'id_model' => 2264,
                'id_fuel' => 2,
            ),
            231 => 
            array (
                'id_reg' => 2732,
                'id_model' => 2264,
                'id_fuel' => 3,
            ),
            232 => 
            array (
                'id_reg' => 2733,
                'id_model' => 2264,
                'id_fuel' => 4,
            ),
            233 => 
            array (
                'id_reg' => 2734,
                'id_model' => 2265,
                'id_fuel' => 4,
            ),
            234 => 
            array (
                'id_reg' => 2735,
                'id_model' => 2266,
                'id_fuel' => 4,
            ),
            235 => 
            array (
                'id_reg' => 2736,
                'id_model' => 2267,
                'id_fuel' => 4,
            ),
            236 => 
            array (
                'id_reg' => 2737,
                'id_model' => 2268,
                'id_fuel' => 4,
            ),
            237 => 
            array (
                'id_reg' => 2738,
                'id_model' => 2269,
                'id_fuel' => 4,
            ),
            238 => 
            array (
                'id_reg' => 2739,
                'id_model' => 2270,
                'id_fuel' => 4,
            ),
            239 => 
            array (
                'id_reg' => 2740,
                'id_model' => 2271,
                'id_fuel' => 4,
            ),
            240 => 
            array (
                'id_reg' => 2741,
                'id_model' => 2272,
                'id_fuel' => 4,
            ),
            241 => 
            array (
                'id_reg' => 2742,
                'id_model' => 2273,
                'id_fuel' => 4,
            ),
            242 => 
            array (
                'id_reg' => 2743,
                'id_model' => 2274,
                'id_fuel' => 4,
            ),
            243 => 
            array (
                'id_reg' => 2744,
                'id_model' => 2275,
                'id_fuel' => 4,
            ),
            244 => 
            array (
                'id_reg' => 2745,
                'id_model' => 2276,
                'id_fuel' => 4,
            ),
            245 => 
            array (
                'id_reg' => 2746,
                'id_model' => 2277,
                'id_fuel' => 4,
            ),
            246 => 
            array (
                'id_reg' => 2747,
                'id_model' => 2278,
                'id_fuel' => 4,
            ),
            247 => 
            array (
                'id_reg' => 2748,
                'id_model' => 2279,
                'id_fuel' => 4,
            ),
            248 => 
            array (
                'id_reg' => 2749,
                'id_model' => 2280,
                'id_fuel' => 4,
            ),
            249 => 
            array (
                'id_reg' => 2750,
                'id_model' => 2281,
                'id_fuel' => 4,
            ),
            250 => 
            array (
                'id_reg' => 2751,
                'id_model' => 2282,
                'id_fuel' => 2,
            ),
            251 => 
            array (
                'id_reg' => 2752,
                'id_model' => 2283,
                'id_fuel' => 2,
            ),
            252 => 
            array (
                'id_reg' => 2753,
                'id_model' => 2284,
                'id_fuel' => 2,
            ),
            253 => 
            array (
                'id_reg' => 2754,
                'id_model' => 2285,
                'id_fuel' => 2,
            ),
            254 => 
            array (
                'id_reg' => 2755,
                'id_model' => 2286,
                'id_fuel' => 2,
            ),
            255 => 
            array (
                'id_reg' => 2756,
                'id_model' => 2287,
                'id_fuel' => 2,
            ),
            256 => 
            array (
                'id_reg' => 2757,
                'id_model' => 2288,
                'id_fuel' => 2,
            ),
            257 => 
            array (
                'id_reg' => 2758,
                'id_model' => 2289,
                'id_fuel' => 4,
            ),
            258 => 
            array (
                'id_reg' => 2759,
                'id_model' => 2290,
                'id_fuel' => 4,
            ),
            259 => 
            array (
                'id_reg' => 2760,
                'id_model' => 2291,
                'id_fuel' => 4,
            ),
            260 => 
            array (
                'id_reg' => 2761,
                'id_model' => 2292,
                'id_fuel' => 2,
            ),
            261 => 
            array (
                'id_reg' => 2762,
                'id_model' => 2292,
                'id_fuel' => 4,
            ),
            262 => 
            array (
                'id_reg' => 2763,
                'id_model' => 2293,
                'id_fuel' => 4,
            ),
            263 => 
            array (
                'id_reg' => 2764,
                'id_model' => 2294,
                'id_fuel' => 2,
            ),
            264 => 
            array (
                'id_reg' => 2765,
                'id_model' => 2295,
                'id_fuel' => 4,
            ),
            265 => 
            array (
                'id_reg' => 2766,
                'id_model' => 2296,
                'id_fuel' => 4,
            ),
            266 => 
            array (
                'id_reg' => 2767,
                'id_model' => 2297,
                'id_fuel' => 4,
            ),
            267 => 
            array (
                'id_reg' => 2768,
                'id_model' => 2298,
                'id_fuel' => 4,
            ),
            268 => 
            array (
                'id_reg' => 2769,
                'id_model' => 2299,
                'id_fuel' => 4,
            ),
            269 => 
            array (
                'id_reg' => 2770,
                'id_model' => 2300,
                'id_fuel' => 4,
            ),
            270 => 
            array (
                'id_reg' => 2771,
                'id_model' => 2301,
                'id_fuel' => 2,
            ),
            271 => 
            array (
                'id_reg' => 2772,
                'id_model' => 2301,
                'id_fuel' => 4,
            ),
            272 => 
            array (
                'id_reg' => 2773,
                'id_model' => 2302,
                'id_fuel' => 2,
            ),
            273 => 
            array (
                'id_reg' => 2774,
                'id_model' => 2302,
                'id_fuel' => 4,
            ),
            274 => 
            array (
                'id_reg' => 2775,
                'id_model' => 2303,
                'id_fuel' => 2,
            ),
            275 => 
            array (
                'id_reg' => 2776,
                'id_model' => 2303,
                'id_fuel' => 4,
            ),
            276 => 
            array (
                'id_reg' => 2777,
                'id_model' => 2304,
                'id_fuel' => 2,
            ),
            277 => 
            array (
                'id_reg' => 2778,
                'id_model' => 2304,
                'id_fuel' => 4,
            ),
            278 => 
            array (
                'id_reg' => 2779,
                'id_model' => 2305,
                'id_fuel' => 2,
            ),
            279 => 
            array (
                'id_reg' => 2780,
                'id_model' => 2305,
                'id_fuel' => 3,
            ),
            280 => 
            array (
                'id_reg' => 2781,
                'id_model' => 2305,
                'id_fuel' => 4,
            ),
            281 => 
            array (
                'id_reg' => 2782,
                'id_model' => 2306,
                'id_fuel' => 2,
            ),
            282 => 
            array (
                'id_reg' => 2783,
                'id_model' => 2306,
                'id_fuel' => 4,
            ),
            283 => 
            array (
                'id_reg' => 2784,
                'id_model' => 2307,
                'id_fuel' => 4,
            ),
            284 => 
            array (
                'id_reg' => 2785,
                'id_model' => 2308,
                'id_fuel' => 2,
            ),
            285 => 
            array (
                'id_reg' => 2786,
                'id_model' => 2308,
                'id_fuel' => 4,
            ),
            286 => 
            array (
                'id_reg' => 2787,
                'id_model' => 2309,
                'id_fuel' => 2,
            ),
            287 => 
            array (
                'id_reg' => 2788,
                'id_model' => 2309,
                'id_fuel' => 4,
            ),
            288 => 
            array (
                'id_reg' => 2789,
                'id_model' => 2310,
                'id_fuel' => 2,
            ),
            289 => 
            array (
                'id_reg' => 2790,
                'id_model' => 2310,
                'id_fuel' => 4,
            ),
            290 => 
            array (
                'id_reg' => 2791,
                'id_model' => 2311,
                'id_fuel' => 4,
            ),
            291 => 
            array (
                'id_reg' => 2792,
                'id_model' => 2312,
                'id_fuel' => 4,
            ),
            292 => 
            array (
                'id_reg' => 2793,
                'id_model' => 2313,
                'id_fuel' => 4,
            ),
            293 => 
            array (
                'id_reg' => 2794,
                'id_model' => 2314,
                'id_fuel' => 2,
            ),
            294 => 
            array (
                'id_reg' => 2795,
                'id_model' => 2314,
                'id_fuel' => 4,
            ),
            295 => 
            array (
                'id_reg' => 2796,
                'id_model' => 2315,
                'id_fuel' => 2,
            ),
            296 => 
            array (
                'id_reg' => 2797,
                'id_model' => 2315,
                'id_fuel' => 4,
            ),
            297 => 
            array (
                'id_reg' => 2798,
                'id_model' => 2316,
                'id_fuel' => 4,
            ),
            298 => 
            array (
                'id_reg' => 2799,
                'id_model' => 2317,
                'id_fuel' => 2,
            ),
            299 => 
            array (
                'id_reg' => 2800,
                'id_model' => 2317,
                'id_fuel' => 4,
            ),
            300 => 
            array (
                'id_reg' => 2801,
                'id_model' => 2318,
                'id_fuel' => 2,
            ),
            301 => 
            array (
                'id_reg' => 2802,
                'id_model' => 2318,
                'id_fuel' => 4,
            ),
            302 => 
            array (
                'id_reg' => 2803,
                'id_model' => 2319,
                'id_fuel' => 2,
            ),
            303 => 
            array (
                'id_reg' => 2804,
                'id_model' => 2319,
                'id_fuel' => 4,
            ),
            304 => 
            array (
                'id_reg' => 2805,
                'id_model' => 2320,
                'id_fuel' => 2,
            ),
            305 => 
            array (
                'id_reg' => 2806,
                'id_model' => 2320,
                'id_fuel' => 4,
            ),
            306 => 
            array (
                'id_reg' => 2807,
                'id_model' => 2321,
                'id_fuel' => 2,
            ),
            307 => 
            array (
                'id_reg' => 2808,
                'id_model' => 2321,
                'id_fuel' => 4,
            ),
            308 => 
            array (
                'id_reg' => 2809,
                'id_model' => 2322,
                'id_fuel' => 2,
            ),
            309 => 
            array (
                'id_reg' => 2810,
                'id_model' => 2322,
                'id_fuel' => 4,
            ),
            310 => 
            array (
                'id_reg' => 2811,
                'id_model' => 2323,
                'id_fuel' => 2,
            ),
            311 => 
            array (
                'id_reg' => 2812,
                'id_model' => 2323,
                'id_fuel' => 4,
            ),
            312 => 
            array (
                'id_reg' => 2813,
                'id_model' => 2324,
                'id_fuel' => 2,
            ),
            313 => 
            array (
                'id_reg' => 2814,
                'id_model' => 2324,
                'id_fuel' => 4,
            ),
            314 => 
            array (
                'id_reg' => 2815,
                'id_model' => 2325,
                'id_fuel' => 2,
            ),
            315 => 
            array (
                'id_reg' => 2816,
                'id_model' => 2325,
                'id_fuel' => 4,
            ),
            316 => 
            array (
                'id_reg' => 2817,
                'id_model' => 2326,
                'id_fuel' => 2,
            ),
            317 => 
            array (
                'id_reg' => 2818,
                'id_model' => 2326,
                'id_fuel' => 4,
            ),
            318 => 
            array (
                'id_reg' => 2819,
                'id_model' => 2327,
                'id_fuel' => 2,
            ),
            319 => 
            array (
                'id_reg' => 2820,
                'id_model' => 2327,
                'id_fuel' => 4,
            ),
            320 => 
            array (
                'id_reg' => 2821,
                'id_model' => 2328,
                'id_fuel' => 2,
            ),
            321 => 
            array (
                'id_reg' => 2822,
                'id_model' => 2328,
                'id_fuel' => 4,
            ),
            322 => 
            array (
                'id_reg' => 2823,
                'id_model' => 2329,
                'id_fuel' => 2,
            ),
            323 => 
            array (
                'id_reg' => 2824,
                'id_model' => 2329,
                'id_fuel' => 4,
            ),
            324 => 
            array (
                'id_reg' => 2825,
                'id_model' => 2330,
                'id_fuel' => 2,
            ),
            325 => 
            array (
                'id_reg' => 2826,
                'id_model' => 2331,
                'id_fuel' => 2,
            ),
            326 => 
            array (
                'id_reg' => 2827,
                'id_model' => 2331,
                'id_fuel' => 4,
            ),
            327 => 
            array (
                'id_reg' => 2828,
                'id_model' => 2332,
                'id_fuel' => 2,
            ),
            328 => 
            array (
                'id_reg' => 2829,
                'id_model' => 2332,
                'id_fuel' => 4,
            ),
            329 => 
            array (
                'id_reg' => 2830,
                'id_model' => 2333,
                'id_fuel' => 2,
            ),
            330 => 
            array (
                'id_reg' => 2831,
                'id_model' => 2333,
                'id_fuel' => 4,
            ),
            331 => 
            array (
                'id_reg' => 2832,
                'id_model' => 2334,
                'id_fuel' => 2,
            ),
            332 => 
            array (
                'id_reg' => 2833,
                'id_model' => 2334,
                'id_fuel' => 4,
            ),
            333 => 
            array (
                'id_reg' => 2834,
                'id_model' => 2335,
                'id_fuel' => 2,
            ),
            334 => 
            array (
                'id_reg' => 2835,
                'id_model' => 2335,
                'id_fuel' => 4,
            ),
            335 => 
            array (
                'id_reg' => 2836,
                'id_model' => 2336,
                'id_fuel' => 6,
            ),
            336 => 
            array (
                'id_reg' => 2837,
                'id_model' => 2337,
                'id_fuel' => 2,
            ),
            337 => 
            array (
                'id_reg' => 2838,
                'id_model' => 2337,
                'id_fuel' => 4,
            ),
            338 => 
            array (
                'id_reg' => 2839,
                'id_model' => 2338,
                'id_fuel' => 4,
            ),
            339 => 
            array (
                'id_reg' => 2840,
                'id_model' => 2339,
                'id_fuel' => 2,
            ),
            340 => 
            array (
                'id_reg' => 2841,
                'id_model' => 2340,
                'id_fuel' => 2,
            ),
            341 => 
            array (
                'id_reg' => 2842,
                'id_model' => 2341,
                'id_fuel' => 2,
            ),
            342 => 
            array (
                'id_reg' => 2843,
                'id_model' => 2341,
                'id_fuel' => 4,
            ),
            343 => 
            array (
                'id_reg' => 2844,
                'id_model' => 2342,
                'id_fuel' => 2,
            ),
            344 => 
            array (
                'id_reg' => 2845,
                'id_model' => 2342,
                'id_fuel' => 4,
            ),
            345 => 
            array (
                'id_reg' => 2846,
                'id_model' => 2343,
                'id_fuel' => 2,
            ),
            346 => 
            array (
                'id_reg' => 2847,
                'id_model' => 2344,
                'id_fuel' => 4,
            ),
            347 => 
            array (
                'id_reg' => 2848,
                'id_model' => 2345,
                'id_fuel' => 4,
            ),
            348 => 
            array (
                'id_reg' => 2849,
                'id_model' => 2346,
                'id_fuel' => 4,
            ),
            349 => 
            array (
                'id_reg' => 2850,
                'id_model' => 2347,
                'id_fuel' => 4,
            ),
            350 => 
            array (
                'id_reg' => 2851,
                'id_model' => 2348,
                'id_fuel' => 4,
            ),
            351 => 
            array (
                'id_reg' => 2852,
                'id_model' => 2349,
                'id_fuel' => 2,
            ),
            352 => 
            array (
                'id_reg' => 2853,
                'id_model' => 2349,
                'id_fuel' => 6,
            ),
            353 => 
            array (
                'id_reg' => 2854,
                'id_model' => 2349,
                'id_fuel' => 3,
            ),
            354 => 
            array (
                'id_reg' => 2855,
                'id_model' => 2349,
                'id_fuel' => 4,
            ),
            355 => 
            array (
                'id_reg' => 2856,
                'id_model' => 2350,
                'id_fuel' => 2,
            ),
            356 => 
            array (
                'id_reg' => 2857,
                'id_model' => 2351,
                'id_fuel' => 2,
            ),
            357 => 
            array (
                'id_reg' => 2858,
                'id_model' => 2352,
                'id_fuel' => 2,
            ),
            358 => 
            array (
                'id_reg' => 2859,
                'id_model' => 2353,
                'id_fuel' => 4,
            ),
            359 => 
            array (
                'id_reg' => 2860,
                'id_model' => 2354,
                'id_fuel' => 4,
            ),
            360 => 
            array (
                'id_reg' => 2861,
                'id_model' => 2355,
                'id_fuel' => 4,
            ),
            361 => 
            array (
                'id_reg' => 2862,
                'id_model' => 2356,
                'id_fuel' => 4,
            ),
            362 => 
            array (
                'id_reg' => 2863,
                'id_model' => 2357,
                'id_fuel' => 4,
            ),
            363 => 
            array (
                'id_reg' => 2864,
                'id_model' => 2358,
                'id_fuel' => 4,
            ),
            364 => 
            array (
                'id_reg' => 2865,
                'id_model' => 2359,
                'id_fuel' => 4,
            ),
            365 => 
            array (
                'id_reg' => 2866,
                'id_model' => 2360,
                'id_fuel' => 2,
            ),
            366 => 
            array (
                'id_reg' => 2867,
                'id_model' => 2360,
                'id_fuel' => 4,
            ),
            367 => 
            array (
                'id_reg' => 2868,
                'id_model' => 2361,
                'id_fuel' => 4,
            ),
            368 => 
            array (
                'id_reg' => 2869,
                'id_model' => 2362,
                'id_fuel' => 4,
            ),
            369 => 
            array (
                'id_reg' => 2870,
                'id_model' => 2363,
                'id_fuel' => 4,
            ),
            370 => 
            array (
                'id_reg' => 2871,
                'id_model' => 2364,
                'id_fuel' => 4,
            ),
            371 => 
            array (
                'id_reg' => 2872,
                'id_model' => 2365,
                'id_fuel' => 4,
            ),
            372 => 
            array (
                'id_reg' => 2873,
                'id_model' => 2366,
                'id_fuel' => 4,
            ),
            373 => 
            array (
                'id_reg' => 2874,
                'id_model' => 2367,
                'id_fuel' => 4,
            ),
            374 => 
            array (
                'id_reg' => 2875,
                'id_model' => 2368,
                'id_fuel' => 4,
            ),
            375 => 
            array (
                'id_reg' => 2876,
                'id_model' => 2369,
                'id_fuel' => 4,
            ),
            376 => 
            array (
                'id_reg' => 2877,
                'id_model' => 2370,
                'id_fuel' => 4,
            ),
            377 => 
            array (
                'id_reg' => 2878,
                'id_model' => 2371,
                'id_fuel' => 4,
            ),
            378 => 
            array (
                'id_reg' => 2879,
                'id_model' => 2372,
                'id_fuel' => 4,
            ),
            379 => 
            array (
                'id_reg' => 2880,
                'id_model' => 2373,
                'id_fuel' => 4,
            ),
            380 => 
            array (
                'id_reg' => 2881,
                'id_model' => 2374,
                'id_fuel' => 4,
            ),
            381 => 
            array (
                'id_reg' => 2882,
                'id_model' => 2375,
                'id_fuel' => 4,
            ),
            382 => 
            array (
                'id_reg' => 2883,
                'id_model' => 2376,
                'id_fuel' => 2,
            ),
            383 => 
            array (
                'id_reg' => 2884,
                'id_model' => 2376,
                'id_fuel' => 4,
            ),
            384 => 
            array (
                'id_reg' => 2885,
                'id_model' => 2377,
                'id_fuel' => 2,
            ),
            385 => 
            array (
                'id_reg' => 2886,
                'id_model' => 2377,
                'id_fuel' => 4,
            ),
            386 => 
            array (
                'id_reg' => 2887,
                'id_model' => 2378,
                'id_fuel' => 4,
            ),
            387 => 
            array (
                'id_reg' => 2888,
                'id_model' => 2379,
                'id_fuel' => 4,
            ),
            388 => 
            array (
                'id_reg' => 2889,
                'id_model' => 2380,
                'id_fuel' => 4,
            ),
            389 => 
            array (
                'id_reg' => 2890,
                'id_model' => 2381,
                'id_fuel' => 4,
            ),
            390 => 
            array (
                'id_reg' => 2891,
                'id_model' => 2382,
                'id_fuel' => 4,
            ),
            391 => 
            array (
                'id_reg' => 2892,
                'id_model' => 2383,
                'id_fuel' => 4,
            ),
            392 => 
            array (
                'id_reg' => 2893,
                'id_model' => 2384,
                'id_fuel' => 2,
            ),
            393 => 
            array (
                'id_reg' => 2894,
                'id_model' => 2384,
                'id_fuel' => 4,
            ),
            394 => 
            array (
                'id_reg' => 2895,
                'id_model' => 2385,
                'id_fuel' => 6,
            ),
            395 => 
            array (
                'id_reg' => 2896,
                'id_model' => 2386,
                'id_fuel' => 4,
            ),
            396 => 
            array (
                'id_reg' => 2897,
                'id_model' => 2387,
                'id_fuel' => 4,
            ),
            397 => 
            array (
                'id_reg' => 2898,
                'id_model' => 2388,
                'id_fuel' => 4,
            ),
            398 => 
            array (
                'id_reg' => 2899,
                'id_model' => 2389,
                'id_fuel' => 4,
            ),
            399 => 
            array (
                'id_reg' => 2900,
                'id_model' => 2390,
                'id_fuel' => 4,
            ),
            400 => 
            array (
                'id_reg' => 2901,
                'id_model' => 2391,
                'id_fuel' => 4,
            ),
            401 => 
            array (
                'id_reg' => 2902,
                'id_model' => 2392,
                'id_fuel' => 4,
            ),
            402 => 
            array (
                'id_reg' => 2903,
                'id_model' => 2393,
                'id_fuel' => 4,
            ),
            403 => 
            array (
                'id_reg' => 2904,
                'id_model' => 2394,
                'id_fuel' => 4,
            ),
            404 => 
            array (
                'id_reg' => 2905,
                'id_model' => 2395,
                'id_fuel' => 4,
            ),
            405 => 
            array (
                'id_reg' => 2906,
                'id_model' => 2396,
                'id_fuel' => 4,
            ),
            406 => 
            array (
                'id_reg' => 2907,
                'id_model' => 2397,
                'id_fuel' => 4,
            ),
            407 => 
            array (
                'id_reg' => 2908,
                'id_model' => 2398,
                'id_fuel' => 4,
            ),
            408 => 
            array (
                'id_reg' => 2909,
                'id_model' => 2399,
                'id_fuel' => 4,
            ),
            409 => 
            array (
                'id_reg' => 2910,
                'id_model' => 2400,
                'id_fuel' => 4,
            ),
            410 => 
            array (
                'id_reg' => 2911,
                'id_model' => 2401,
                'id_fuel' => 4,
            ),
            411 => 
            array (
                'id_reg' => 2912,
                'id_model' => 2402,
                'id_fuel' => 4,
            ),
            412 => 
            array (
                'id_reg' => 2913,
                'id_model' => 2403,
                'id_fuel' => 4,
            ),
            413 => 
            array (
                'id_reg' => 2914,
                'id_model' => 2404,
                'id_fuel' => 4,
            ),
            414 => 
            array (
                'id_reg' => 2915,
                'id_model' => 2405,
                'id_fuel' => 4,
            ),
            415 => 
            array (
                'id_reg' => 2916,
                'id_model' => 2406,
                'id_fuel' => 4,
            ),
            416 => 
            array (
                'id_reg' => 2917,
                'id_model' => 2407,
                'id_fuel' => 4,
            ),
            417 => 
            array (
                'id_reg' => 2918,
                'id_model' => 2408,
                'id_fuel' => 4,
            ),
            418 => 
            array (
                'id_reg' => 2919,
                'id_model' => 2409,
                'id_fuel' => 4,
            ),
            419 => 
            array (
                'id_reg' => 2920,
                'id_model' => 2410,
                'id_fuel' => 4,
            ),
            420 => 
            array (
                'id_reg' => 2921,
                'id_model' => 2411,
                'id_fuel' => 4,
            ),
            421 => 
            array (
                'id_reg' => 2922,
                'id_model' => 2412,
                'id_fuel' => 4,
            ),
            422 => 
            array (
                'id_reg' => 2923,
                'id_model' => 2413,
                'id_fuel' => 4,
            ),
            423 => 
            array (
                'id_reg' => 2924,
                'id_model' => 2414,
                'id_fuel' => 4,
            ),
            424 => 
            array (
                'id_reg' => 2925,
                'id_model' => 2415,
                'id_fuel' => 4,
            ),
            425 => 
            array (
                'id_reg' => 2926,
                'id_model' => 2416,
                'id_fuel' => 4,
            ),
            426 => 
            array (
                'id_reg' => 2927,
                'id_model' => 2417,
                'id_fuel' => 4,
            ),
            427 => 
            array (
                'id_reg' => 2928,
                'id_model' => 2418,
                'id_fuel' => 4,
            ),
            428 => 
            array (
                'id_reg' => 2929,
                'id_model' => 2419,
                'id_fuel' => 4,
            ),
            429 => 
            array (
                'id_reg' => 2930,
                'id_model' => 2420,
                'id_fuel' => 4,
            ),
            430 => 
            array (
                'id_reg' => 2931,
                'id_model' => 2421,
                'id_fuel' => 4,
            ),
            431 => 
            array (
                'id_reg' => 2932,
                'id_model' => 2422,
                'id_fuel' => 4,
            ),
            432 => 
            array (
                'id_reg' => 2933,
                'id_model' => 2424,
                'id_fuel' => 4,
            ),
            433 => 
            array (
                'id_reg' => 2934,
                'id_model' => 2425,
                'id_fuel' => 4,
            ),
            434 => 
            array (
                'id_reg' => 2935,
                'id_model' => 2426,
                'id_fuel' => 4,
            ),
            435 => 
            array (
                'id_reg' => 2936,
                'id_model' => 2427,
                'id_fuel' => 4,
            ),
            436 => 
            array (
                'id_reg' => 2937,
                'id_model' => 2428,
                'id_fuel' => 5,
            ),
            437 => 
            array (
                'id_reg' => 2938,
                'id_model' => 2429,
                'id_fuel' => 4,
            ),
            438 => 
            array (
                'id_reg' => 2939,
                'id_model' => 2430,
                'id_fuel' => 4,
            ),
            439 => 
            array (
                'id_reg' => 2940,
                'id_model' => 2431,
                'id_fuel' => 4,
            ),
            440 => 
            array (
                'id_reg' => 2941,
                'id_model' => 2432,
                'id_fuel' => 4,
            ),
            441 => 
            array (
                'id_reg' => 2942,
                'id_model' => 2433,
                'id_fuel' => 4,
            ),
            442 => 
            array (
                'id_reg' => 2943,
                'id_model' => 2434,
                'id_fuel' => 4,
            ),
            443 => 
            array (
                'id_reg' => 2944,
                'id_model' => 2435,
                'id_fuel' => 4,
            ),
            444 => 
            array (
                'id_reg' => 2945,
                'id_model' => 2436,
                'id_fuel' => 2,
            ),
            445 => 
            array (
                'id_reg' => 2946,
                'id_model' => 2436,
                'id_fuel' => 4,
            ),
            446 => 
            array (
                'id_reg' => 2947,
                'id_model' => 2436,
                'id_fuel' => 5,
            ),
            447 => 
            array (
                'id_reg' => 2948,
                'id_model' => 2437,
                'id_fuel' => 4,
            ),
            448 => 
            array (
                'id_reg' => 2949,
                'id_model' => 2438,
                'id_fuel' => 2,
            ),
            449 => 
            array (
                'id_reg' => 2950,
                'id_model' => 2438,
                'id_fuel' => 4,
            ),
            450 => 
            array (
                'id_reg' => 2951,
                'id_model' => 2439,
                'id_fuel' => 2,
            ),
            451 => 
            array (
                'id_reg' => 2952,
                'id_model' => 2439,
                'id_fuel' => 4,
            ),
            452 => 
            array (
                'id_reg' => 2953,
                'id_model' => 2439,
                'id_fuel' => 5,
            ),
            453 => 
            array (
                'id_reg' => 2954,
                'id_model' => 2440,
                'id_fuel' => 6,
            ),
            454 => 
            array (
                'id_reg' => 2955,
                'id_model' => 2441,
                'id_fuel' => 4,
            ),
            455 => 
            array (
                'id_reg' => 2956,
                'id_model' => 2442,
                'id_fuel' => 6,
            ),
            456 => 
            array (
                'id_reg' => 2957,
                'id_model' => 2443,
                'id_fuel' => 2,
            ),
            457 => 
            array (
                'id_reg' => 2958,
                'id_model' => 2443,
                'id_fuel' => 4,
            ),
            458 => 
            array (
                'id_reg' => 2959,
                'id_model' => 2444,
                'id_fuel' => 4,
            ),
            459 => 
            array (
                'id_reg' => 2960,
                'id_model' => 2445,
                'id_fuel' => 4,
            ),
            460 => 
            array (
                'id_reg' => 2961,
                'id_model' => 2446,
                'id_fuel' => 4,
            ),
            461 => 
            array (
                'id_reg' => 2962,
                'id_model' => 2447,
                'id_fuel' => 4,
            ),
            462 => 
            array (
                'id_reg' => 2963,
                'id_model' => 2448,
                'id_fuel' => 6,
            ),
            463 => 
            array (
                'id_reg' => 2964,
                'id_model' => 2449,
                'id_fuel' => 4,
            ),
            464 => 
            array (
                'id_reg' => 2965,
                'id_model' => 2450,
                'id_fuel' => 4,
            ),
            465 => 
            array (
                'id_reg' => 2966,
                'id_model' => 2451,
                'id_fuel' => 4,
            ),
            466 => 
            array (
                'id_reg' => 2967,
                'id_model' => 2452,
                'id_fuel' => 4,
            ),
            467 => 
            array (
                'id_reg' => 2968,
                'id_model' => 2453,
                'id_fuel' => 4,
            ),
            468 => 
            array (
                'id_reg' => 2969,
                'id_model' => 2454,
                'id_fuel' => 4,
            ),
            469 => 
            array (
                'id_reg' => 2970,
                'id_model' => 2455,
                'id_fuel' => 4,
            ),
            470 => 
            array (
                'id_reg' => 2971,
                'id_model' => 2456,
                'id_fuel' => 2,
            ),
            471 => 
            array (
                'id_reg' => 2972,
                'id_model' => 2457,
                'id_fuel' => 4,
            ),
            472 => 
            array (
                'id_reg' => 2973,
                'id_model' => 2458,
                'id_fuel' => 4,
            ),
            473 => 
            array (
                'id_reg' => 2974,
                'id_model' => 2459,
                'id_fuel' => 4,
            ),
            474 => 
            array (
                'id_reg' => 2975,
                'id_model' => 2460,
                'id_fuel' => 4,
            ),
            475 => 
            array (
                'id_reg' => 2976,
                'id_model' => 2461,
                'id_fuel' => 4,
            ),
            476 => 
            array (
                'id_reg' => 2977,
                'id_model' => 2462,
                'id_fuel' => 4,
            ),
            477 => 
            array (
                'id_reg' => 2978,
                'id_model' => 2463,
                'id_fuel' => 4,
            ),
            478 => 
            array (
                'id_reg' => 2979,
                'id_model' => 2464,
                'id_fuel' => 2,
            ),
            479 => 
            array (
                'id_reg' => 2980,
                'id_model' => 2464,
                'id_fuel' => 4,
            ),
            480 => 
            array (
                'id_reg' => 2981,
                'id_model' => 2465,
                'id_fuel' => 4,
            ),
            481 => 
            array (
                'id_reg' => 2982,
                'id_model' => 2466,
                'id_fuel' => 4,
            ),
            482 => 
            array (
                'id_reg' => 2983,
                'id_model' => 2467,
                'id_fuel' => 4,
            ),
            483 => 
            array (
                'id_reg' => 2984,
                'id_model' => 2468,
                'id_fuel' => 2,
            ),
            484 => 
            array (
                'id_reg' => 2985,
                'id_model' => 2468,
                'id_fuel' => 4,
            ),
            485 => 
            array (
                'id_reg' => 2986,
                'id_model' => 2469,
                'id_fuel' => 4,
            ),
            486 => 
            array (
                'id_reg' => 2987,
                'id_model' => 2470,
                'id_fuel' => 2,
            ),
            487 => 
            array (
                'id_reg' => 2988,
                'id_model' => 2470,
                'id_fuel' => 4,
            ),
            488 => 
            array (
                'id_reg' => 2989,
                'id_model' => 2471,
                'id_fuel' => 4,
            ),
            489 => 
            array (
                'id_reg' => 2990,
                'id_model' => 2472,
                'id_fuel' => 4,
            ),
            490 => 
            array (
                'id_reg' => 2991,
                'id_model' => 2473,
                'id_fuel' => 4,
            ),
            491 => 
            array (
                'id_reg' => 2992,
                'id_model' => 2474,
                'id_fuel' => 4,
            ),
            492 => 
            array (
                'id_reg' => 2993,
                'id_model' => 2475,
                'id_fuel' => 4,
            ),
            493 => 
            array (
                'id_reg' => 2994,
                'id_model' => 2476,
                'id_fuel' => 2,
            ),
            494 => 
            array (
                'id_reg' => 2995,
                'id_model' => 2476,
                'id_fuel' => 4,
            ),
            495 => 
            array (
                'id_reg' => 2996,
                'id_model' => 2477,
                'id_fuel' => 2,
            ),
            496 => 
            array (
                'id_reg' => 2997,
                'id_model' => 2477,
                'id_fuel' => 4,
            ),
            497 => 
            array (
                'id_reg' => 2998,
                'id_model' => 2478,
                'id_fuel' => 2,
            ),
            498 => 
            array (
                'id_reg' => 2999,
                'id_model' => 2478,
                'id_fuel' => 4,
            ),
            499 => 
            array (
                'id_reg' => 3000,
                'id_model' => 2479,
                'id_fuel' => 2,
            ),
        ));
        \DB::table('model_rel_fuel')->insert(array (
            0 => 
            array (
                'id_reg' => 3001,
                'id_model' => 2479,
                'id_fuel' => 4,
            ),
            1 => 
            array (
                'id_reg' => 3002,
                'id_model' => 2480,
                'id_fuel' => 2,
            ),
            2 => 
            array (
                'id_reg' => 3003,
                'id_model' => 2480,
                'id_fuel' => 4,
            ),
            3 => 
            array (
                'id_reg' => 3004,
                'id_model' => 2481,
                'id_fuel' => 4,
            ),
            4 => 
            array (
                'id_reg' => 3005,
                'id_model' => 2482,
                'id_fuel' => 4,
            ),
            5 => 
            array (
                'id_reg' => 3006,
                'id_model' => 2483,
                'id_fuel' => 2,
            ),
            6 => 
            array (
                'id_reg' => 3007,
                'id_model' => 2484,
                'id_fuel' => 2,
            ),
            7 => 
            array (
                'id_reg' => 3008,
                'id_model' => 2484,
                'id_fuel' => 4,
            ),
            8 => 
            array (
                'id_reg' => 3009,
                'id_model' => 2485,
                'id_fuel' => 2,
            ),
            9 => 
            array (
                'id_reg' => 3010,
                'id_model' => 2486,
                'id_fuel' => 2,
            ),
            10 => 
            array (
                'id_reg' => 3011,
                'id_model' => 2487,
                'id_fuel' => 2,
            ),
            11 => 
            array (
                'id_reg' => 3012,
                'id_model' => 2488,
                'id_fuel' => 2,
            ),
            12 => 
            array (
                'id_reg' => 3013,
                'id_model' => 2489,
                'id_fuel' => 2,
            ),
            13 => 
            array (
                'id_reg' => 3014,
                'id_model' => 2490,
                'id_fuel' => 2,
            ),
            14 => 
            array (
                'id_reg' => 3015,
                'id_model' => 2491,
                'id_fuel' => 2,
            ),
            15 => 
            array (
                'id_reg' => 3016,
                'id_model' => 2491,
                'id_fuel' => 4,
            ),
            16 => 
            array (
                'id_reg' => 3017,
                'id_model' => 2492,
                'id_fuel' => 4,
            ),
            17 => 
            array (
                'id_reg' => 3018,
                'id_model' => 2493,
                'id_fuel' => 4,
            ),
            18 => 
            array (
                'id_reg' => 3019,
                'id_model' => 2494,
                'id_fuel' => 4,
            ),
            19 => 
            array (
                'id_reg' => 3020,
                'id_model' => 2495,
                'id_fuel' => 4,
            ),
            20 => 
            array (
                'id_reg' => 3021,
                'id_model' => 2496,
                'id_fuel' => 2,
            ),
            21 => 
            array (
                'id_reg' => 3022,
                'id_model' => 2496,
                'id_fuel' => 4,
            ),
            22 => 
            array (
                'id_reg' => 3023,
                'id_model' => 2497,
                'id_fuel' => 4,
            ),
            23 => 
            array (
                'id_reg' => 3024,
                'id_model' => 2498,
                'id_fuel' => 2,
            ),
            24 => 
            array (
                'id_reg' => 3025,
                'id_model' => 2498,
                'id_fuel' => 4,
            ),
            25 => 
            array (
                'id_reg' => 3026,
                'id_model' => 2499,
                'id_fuel' => 4,
            ),
            26 => 
            array (
                'id_reg' => 3027,
                'id_model' => 2500,
                'id_fuel' => 2,
            ),
            27 => 
            array (
                'id_reg' => 3028,
                'id_model' => 2500,
                'id_fuel' => 6,
            ),
            28 => 
            array (
                'id_reg' => 3029,
                'id_model' => 2500,
                'id_fuel' => 4,
            ),
            29 => 
            array (
                'id_reg' => 3030,
                'id_model' => 2501,
                'id_fuel' => 4,
            ),
            30 => 
            array (
                'id_reg' => 3031,
                'id_model' => 2502,
                'id_fuel' => 2,
            ),
            31 => 
            array (
                'id_reg' => 3032,
                'id_model' => 2502,
                'id_fuel' => 4,
            ),
            32 => 
            array (
                'id_reg' => 3033,
                'id_model' => 2503,
                'id_fuel' => 4,
            ),
            33 => 
            array (
                'id_reg' => 3034,
                'id_model' => 2504,
                'id_fuel' => 4,
            ),
            34 => 
            array (
                'id_reg' => 3035,
                'id_model' => 2505,
                'id_fuel' => 2,
            ),
            35 => 
            array (
                'id_reg' => 3036,
                'id_model' => 2505,
                'id_fuel' => 4,
            ),
            36 => 
            array (
                'id_reg' => 3037,
                'id_model' => 2506,
                'id_fuel' => 2,
            ),
            37 => 
            array (
                'id_reg' => 3038,
                'id_model' => 2507,
                'id_fuel' => 2,
            ),
            38 => 
            array (
                'id_reg' => 3039,
                'id_model' => 2507,
                'id_fuel' => 4,
            ),
            39 => 
            array (
                'id_reg' => 3040,
                'id_model' => 2508,
                'id_fuel' => 4,
            ),
            40 => 
            array (
                'id_reg' => 3041,
                'id_model' => 2509,
                'id_fuel' => 2,
            ),
            41 => 
            array (
                'id_reg' => 3042,
                'id_model' => 2509,
                'id_fuel' => 4,
            ),
            42 => 
            array (
                'id_reg' => 3043,
                'id_model' => 2510,
                'id_fuel' => 2,
            ),
            43 => 
            array (
                'id_reg' => 3044,
                'id_model' => 2510,
                'id_fuel' => 4,
            ),
            44 => 
            array (
                'id_reg' => 3045,
                'id_model' => 2511,
                'id_fuel' => 2,
            ),
            45 => 
            array (
                'id_reg' => 3046,
                'id_model' => 2512,
                'id_fuel' => 2,
            ),
            46 => 
            array (
                'id_reg' => 3047,
                'id_model' => 2512,
                'id_fuel' => 6,
            ),
            47 => 
            array (
                'id_reg' => 3048,
                'id_model' => 2513,
                'id_fuel' => 2,
            ),
            48 => 
            array (
                'id_reg' => 3049,
                'id_model' => 2513,
                'id_fuel' => 4,
            ),
            49 => 
            array (
                'id_reg' => 3050,
                'id_model' => 2514,
                'id_fuel' => 4,
            ),
            50 => 
            array (
                'id_reg' => 3051,
                'id_model' => 2515,
                'id_fuel' => 2,
            ),
            51 => 
            array (
                'id_reg' => 3052,
                'id_model' => 2515,
                'id_fuel' => 4,
            ),
            52 => 
            array (
                'id_reg' => 3053,
                'id_model' => 2516,
                'id_fuel' => 2,
            ),
            53 => 
            array (
                'id_reg' => 3054,
                'id_model' => 2516,
                'id_fuel' => 4,
            ),
            54 => 
            array (
                'id_reg' => 3055,
                'id_model' => 2517,
                'id_fuel' => 4,
            ),
            55 => 
            array (
                'id_reg' => 3056,
                'id_model' => 2518,
                'id_fuel' => 2,
            ),
            56 => 
            array (
                'id_reg' => 3057,
                'id_model' => 2519,
                'id_fuel' => 2,
            ),
            57 => 
            array (
                'id_reg' => 3058,
                'id_model' => 2519,
                'id_fuel' => 4,
            ),
            58 => 
            array (
                'id_reg' => 3059,
                'id_model' => 2520,
                'id_fuel' => 2,
            ),
            59 => 
            array (
                'id_reg' => 3060,
                'id_model' => 2521,
                'id_fuel' => 2,
            ),
            60 => 
            array (
                'id_reg' => 3061,
                'id_model' => 2521,
                'id_fuel' => 4,
            ),
            61 => 
            array (
                'id_reg' => 3062,
                'id_model' => 2522,
                'id_fuel' => 1,
            ),
            62 => 
            array (
                'id_reg' => 3063,
                'id_model' => 2522,
                'id_fuel' => 2,
            ),
            63 => 
            array (
                'id_reg' => 3064,
                'id_model' => 2522,
                'id_fuel' => 4,
            ),
            64 => 
            array (
                'id_reg' => 3065,
                'id_model' => 2523,
                'id_fuel' => 2,
            ),
            65 => 
            array (
                'id_reg' => 3066,
                'id_model' => 2524,
                'id_fuel' => 2,
            ),
            66 => 
            array (
                'id_reg' => 3067,
                'id_model' => 2524,
                'id_fuel' => 4,
            ),
            67 => 
            array (
                'id_reg' => 3068,
                'id_model' => 2525,
                'id_fuel' => 4,
            ),
            68 => 
            array (
                'id_reg' => 3069,
                'id_model' => 2526,
                'id_fuel' => 4,
            ),
            69 => 
            array (
                'id_reg' => 3070,
                'id_model' => 2527,
                'id_fuel' => 4,
            ),
            70 => 
            array (
                'id_reg' => 3071,
                'id_model' => 2528,
                'id_fuel' => 4,
            ),
            71 => 
            array (
                'id_reg' => 3072,
                'id_model' => 2529,
                'id_fuel' => 4,
            ),
            72 => 
            array (
                'id_reg' => 3073,
                'id_model' => 2530,
                'id_fuel' => 4,
            ),
            73 => 
            array (
                'id_reg' => 3074,
                'id_model' => 2531,
                'id_fuel' => 4,
            ),
            74 => 
            array (
                'id_reg' => 3075,
                'id_model' => 2532,
                'id_fuel' => 4,
            ),
            75 => 
            array (
                'id_reg' => 3076,
                'id_model' => 2533,
                'id_fuel' => 2,
            ),
            76 => 
            array (
                'id_reg' => 3077,
                'id_model' => 2533,
                'id_fuel' => 4,
            ),
            77 => 
            array (
                'id_reg' => 3078,
                'id_model' => 2534,
                'id_fuel' => 4,
            ),
            78 => 
            array (
                'id_reg' => 3079,
                'id_model' => 2535,
                'id_fuel' => 2,
            ),
            79 => 
            array (
                'id_reg' => 3080,
                'id_model' => 2535,
                'id_fuel' => 4,
            ),
            80 => 
            array (
                'id_reg' => 3081,
                'id_model' => 2536,
                'id_fuel' => 2,
            ),
            81 => 
            array (
                'id_reg' => 3082,
                'id_model' => 2536,
                'id_fuel' => 4,
            ),
            82 => 
            array (
                'id_reg' => 3083,
                'id_model' => 2537,
                'id_fuel' => 2,
            ),
            83 => 
            array (
                'id_reg' => 3084,
                'id_model' => 2537,
                'id_fuel' => 4,
            ),
            84 => 
            array (
                'id_reg' => 3085,
                'id_model' => 2538,
                'id_fuel' => 2,
            ),
            85 => 
            array (
                'id_reg' => 3086,
                'id_model' => 2538,
                'id_fuel' => 4,
            ),
            86 => 
            array (
                'id_reg' => 3087,
                'id_model' => 2539,
                'id_fuel' => 2,
            ),
            87 => 
            array (
                'id_reg' => 3088,
                'id_model' => 2539,
                'id_fuel' => 4,
            ),
            88 => 
            array (
                'id_reg' => 3089,
                'id_model' => 2540,
                'id_fuel' => 4,
            ),
            89 => 
            array (
                'id_reg' => 3090,
                'id_model' => 2541,
                'id_fuel' => 4,
            ),
            90 => 
            array (
                'id_reg' => 3091,
                'id_model' => 2542,
                'id_fuel' => 6,
            ),
            91 => 
            array (
                'id_reg' => 3092,
                'id_model' => 2543,
                'id_fuel' => 4,
            ),
            92 => 
            array (
                'id_reg' => 3093,
                'id_model' => 2544,
                'id_fuel' => 2,
            ),
            93 => 
            array (
                'id_reg' => 3094,
                'id_model' => 2544,
                'id_fuel' => 4,
            ),
            94 => 
            array (
                'id_reg' => 3095,
                'id_model' => 2545,
                'id_fuel' => 4,
            ),
            95 => 
            array (
                'id_reg' => 3096,
                'id_model' => 2546,
                'id_fuel' => 4,
            ),
            96 => 
            array (
                'id_reg' => 3097,
                'id_model' => 2547,
                'id_fuel' => 6,
            ),
            97 => 
            array (
                'id_reg' => 3098,
                'id_model' => 2548,
                'id_fuel' => 4,
            ),
            98 => 
            array (
                'id_reg' => 3099,
                'id_model' => 2549,
                'id_fuel' => 4,
            ),
            99 => 
            array (
                'id_reg' => 3100,
                'id_model' => 2550,
                'id_fuel' => 6,
            ),
            100 => 
            array (
                'id_reg' => 3101,
                'id_model' => 2551,
                'id_fuel' => 6,
            ),
            101 => 
            array (
                'id_reg' => 3102,
                'id_model' => 2552,
                'id_fuel' => 6,
            ),
            102 => 
            array (
                'id_reg' => 3103,
                'id_model' => 2553,
                'id_fuel' => 6,
            ),
            103 => 
            array (
                'id_reg' => 3104,
                'id_model' => 2555,
                'id_fuel' => 4,
            ),
            104 => 
            array (
                'id_reg' => 3105,
                'id_model' => 2556,
                'id_fuel' => 4,
            ),
            105 => 
            array (
                'id_reg' => 3106,
                'id_model' => 2557,
                'id_fuel' => 4,
            ),
            106 => 
            array (
                'id_reg' => 3107,
                'id_model' => 2558,
                'id_fuel' => 2,
            ),
            107 => 
            array (
                'id_reg' => 3108,
                'id_model' => 2558,
                'id_fuel' => 4,
            ),
            108 => 
            array (
                'id_reg' => 3109,
                'id_model' => 2559,
                'id_fuel' => 4,
            ),
            109 => 
            array (
                'id_reg' => 3110,
                'id_model' => 2560,
                'id_fuel' => 4,
            ),
            110 => 
            array (
                'id_reg' => 3111,
                'id_model' => 2563,
                'id_fuel' => 4,
            ),
            111 => 
            array (
                'id_reg' => 3112,
                'id_model' => 2564,
                'id_fuel' => 4,
            ),
            112 => 
            array (
                'id_reg' => 3113,
                'id_model' => 2565,
                'id_fuel' => 4,
            ),
            113 => 
            array (
                'id_reg' => 3114,
                'id_model' => 2566,
                'id_fuel' => 4,
            ),
            114 => 
            array (
                'id_reg' => 3115,
                'id_model' => 2567,
                'id_fuel' => 4,
            ),
            115 => 
            array (
                'id_reg' => 3116,
                'id_model' => 2568,
                'id_fuel' => 4,
            ),
            116 => 
            array (
                'id_reg' => 3117,
                'id_model' => 2569,
                'id_fuel' => 4,
            ),
            117 => 
            array (
                'id_reg' => 3118,
                'id_model' => 2570,
                'id_fuel' => 4,
            ),
            118 => 
            array (
                'id_reg' => 3119,
                'id_model' => 2571,
                'id_fuel' => 4,
            ),
            119 => 
            array (
                'id_reg' => 3120,
                'id_model' => 2572,
                'id_fuel' => 4,
            ),
            120 => 
            array (
                'id_reg' => 3121,
                'id_model' => 2573,
                'id_fuel' => 4,
            ),
            121 => 
            array (
                'id_reg' => 3122,
                'id_model' => 2574,
                'id_fuel' => 4,
            ),
            122 => 
            array (
                'id_reg' => 3123,
                'id_model' => 2575,
                'id_fuel' => 4,
            ),
            123 => 
            array (
                'id_reg' => 3124,
                'id_model' => 2576,
                'id_fuel' => 4,
            ),
            124 => 
            array (
                'id_reg' => 3125,
                'id_model' => 2577,
                'id_fuel' => 4,
            ),
            125 => 
            array (
                'id_reg' => 3126,
                'id_model' => 2578,
                'id_fuel' => 4,
            ),
            126 => 
            array (
                'id_reg' => 3127,
                'id_model' => 2579,
                'id_fuel' => 4,
            ),
            127 => 
            array (
                'id_reg' => 3128,
                'id_model' => 2580,
                'id_fuel' => 4,
            ),
            128 => 
            array (
                'id_reg' => 3129,
                'id_model' => 2581,
                'id_fuel' => 4,
            ),
            129 => 
            array (
                'id_reg' => 3130,
                'id_model' => 2582,
                'id_fuel' => 2,
            ),
            130 => 
            array (
                'id_reg' => 3131,
                'id_model' => 2582,
                'id_fuel' => 4,
            ),
            131 => 
            array (
                'id_reg' => 3132,
                'id_model' => 2583,
                'id_fuel' => 2,
            ),
            132 => 
            array (
                'id_reg' => 3133,
                'id_model' => 2583,
                'id_fuel' => 4,
            ),
            133 => 
            array (
                'id_reg' => 3134,
                'id_model' => 2584,
                'id_fuel' => 2,
            ),
            134 => 
            array (
                'id_reg' => 3135,
                'id_model' => 2584,
                'id_fuel' => 4,
            ),
            135 => 
            array (
                'id_reg' => 3136,
                'id_model' => 2585,
                'id_fuel' => 4,
            ),
            136 => 
            array (
                'id_reg' => 3137,
                'id_model' => 2586,
                'id_fuel' => 4,
            ),
            137 => 
            array (
                'id_reg' => 3138,
                'id_model' => 2587,
                'id_fuel' => 2,
            ),
            138 => 
            array (
                'id_reg' => 3139,
                'id_model' => 2587,
                'id_fuel' => 4,
            ),
            139 => 
            array (
                'id_reg' => 3140,
                'id_model' => 2588,
                'id_fuel' => 2,
            ),
            140 => 
            array (
                'id_reg' => 3141,
                'id_model' => 2589,
                'id_fuel' => 4,
            ),
            141 => 
            array (
                'id_reg' => 3142,
                'id_model' => 2590,
                'id_fuel' => 4,
            ),
            142 => 
            array (
                'id_reg' => 3143,
                'id_model' => 2591,
                'id_fuel' => 4,
            ),
            143 => 
            array (
                'id_reg' => 3144,
                'id_model' => 2592,
                'id_fuel' => 4,
            ),
            144 => 
            array (
                'id_reg' => 3145,
                'id_model' => 2593,
                'id_fuel' => 4,
            ),
            145 => 
            array (
                'id_reg' => 3146,
                'id_model' => 2594,
                'id_fuel' => 2,
            ),
            146 => 
            array (
                'id_reg' => 3147,
                'id_model' => 2594,
                'id_fuel' => 4,
            ),
            147 => 
            array (
                'id_reg' => 3148,
                'id_model' => 2595,
                'id_fuel' => 2,
            ),
            148 => 
            array (
                'id_reg' => 3149,
                'id_model' => 2595,
                'id_fuel' => 4,
            ),
            149 => 
            array (
                'id_reg' => 3150,
                'id_model' => 2596,
                'id_fuel' => 4,
            ),
            150 => 
            array (
                'id_reg' => 3151,
                'id_model' => 2597,
                'id_fuel' => 4,
            ),
            151 => 
            array (
                'id_reg' => 3152,
                'id_model' => 2598,
                'id_fuel' => 2,
            ),
            152 => 
            array (
                'id_reg' => 3153,
                'id_model' => 2598,
                'id_fuel' => 4,
            ),
            153 => 
            array (
                'id_reg' => 3154,
                'id_model' => 2599,
                'id_fuel' => 2,
            ),
            154 => 
            array (
                'id_reg' => 3155,
                'id_model' => 2599,
                'id_fuel' => 4,
            ),
            155 => 
            array (
                'id_reg' => 3156,
                'id_model' => 2600,
                'id_fuel' => 4,
            ),
            156 => 
            array (
                'id_reg' => 3157,
                'id_model' => 2601,
                'id_fuel' => 2,
            ),
            157 => 
            array (
                'id_reg' => 3158,
                'id_model' => 2601,
                'id_fuel' => 4,
            ),
            158 => 
            array (
                'id_reg' => 3159,
                'id_model' => 2602,
                'id_fuel' => 4,
            ),
            159 => 
            array (
                'id_reg' => 3160,
                'id_model' => 2603,
                'id_fuel' => 4,
            ),
            160 => 
            array (
                'id_reg' => 3161,
                'id_model' => 2604,
                'id_fuel' => 2,
            ),
            161 => 
            array (
                'id_reg' => 3162,
                'id_model' => 2604,
                'id_fuel' => 4,
            ),
            162 => 
            array (
                'id_reg' => 3163,
                'id_model' => 2605,
                'id_fuel' => 4,
            ),
            163 => 
            array (
                'id_reg' => 3164,
                'id_model' => 2606,
                'id_fuel' => 4,
            ),
            164 => 
            array (
                'id_reg' => 3165,
                'id_model' => 2607,
                'id_fuel' => 4,
            ),
            165 => 
            array (
                'id_reg' => 3166,
                'id_model' => 2608,
                'id_fuel' => 2,
            ),
            166 => 
            array (
                'id_reg' => 3167,
                'id_model' => 2609,
                'id_fuel' => 4,
            ),
            167 => 
            array (
                'id_reg' => 3168,
                'id_model' => 2610,
                'id_fuel' => 4,
            ),
            168 => 
            array (
                'id_reg' => 3169,
                'id_model' => 2611,
                'id_fuel' => 4,
            ),
            169 => 
            array (
                'id_reg' => 3170,
                'id_model' => 2612,
                'id_fuel' => 2,
            ),
            170 => 
            array (
                'id_reg' => 3171,
                'id_model' => 2612,
                'id_fuel' => 4,
            ),
            171 => 
            array (
                'id_reg' => 3172,
                'id_model' => 2613,
                'id_fuel' => 4,
            ),
            172 => 
            array (
                'id_reg' => 3173,
                'id_model' => 2614,
                'id_fuel' => 4,
            ),
            173 => 
            array (
                'id_reg' => 3174,
                'id_model' => 2615,
                'id_fuel' => 4,
            ),
            174 => 
            array (
                'id_reg' => 3175,
                'id_model' => 2616,
                'id_fuel' => 2,
            ),
            175 => 
            array (
                'id_reg' => 3176,
                'id_model' => 2616,
                'id_fuel' => 4,
            ),
            176 => 
            array (
                'id_reg' => 3177,
                'id_model' => 2617,
                'id_fuel' => 2,
            ),
            177 => 
            array (
                'id_reg' => 3178,
                'id_model' => 2617,
                'id_fuel' => 4,
            ),
            178 => 
            array (
                'id_reg' => 3179,
                'id_model' => 2618,
                'id_fuel' => 2,
            ),
            179 => 
            array (
                'id_reg' => 3180,
                'id_model' => 2618,
                'id_fuel' => 4,
            ),
            180 => 
            array (
                'id_reg' => 3181,
                'id_model' => 2619,
                'id_fuel' => 4,
            ),
            181 => 
            array (
                'id_reg' => 3182,
                'id_model' => 2620,
                'id_fuel' => 4,
            ),
            182 => 
            array (
                'id_reg' => 3183,
                'id_model' => 2621,
                'id_fuel' => 2,
            ),
            183 => 
            array (
                'id_reg' => 3184,
                'id_model' => 2621,
                'id_fuel' => 4,
            ),
            184 => 
            array (
                'id_reg' => 3185,
                'id_model' => 2622,
                'id_fuel' => 4,
            ),
            185 => 
            array (
                'id_reg' => 3186,
                'id_model' => 2623,
                'id_fuel' => 4,
            ),
            186 => 
            array (
                'id_reg' => 3187,
                'id_model' => 2624,
                'id_fuel' => 2,
            ),
            187 => 
            array (
                'id_reg' => 3188,
                'id_model' => 2624,
                'id_fuel' => 4,
            ),
            188 => 
            array (
                'id_reg' => 3189,
                'id_model' => 2625,
                'id_fuel' => 1,
            ),
            189 => 
            array (
                'id_reg' => 3190,
                'id_model' => 2625,
                'id_fuel' => 2,
            ),
            190 => 
            array (
                'id_reg' => 3191,
                'id_model' => 2625,
                'id_fuel' => 4,
            ),
            191 => 
            array (
                'id_reg' => 3192,
                'id_model' => 2626,
                'id_fuel' => 1,
            ),
            192 => 
            array (
                'id_reg' => 3193,
                'id_model' => 2626,
                'id_fuel' => 2,
            ),
            193 => 
            array (
                'id_reg' => 3194,
                'id_model' => 2626,
                'id_fuel' => 4,
            ),
            194 => 
            array (
                'id_reg' => 3195,
                'id_model' => 2627,
                'id_fuel' => 4,
            ),
            195 => 
            array (
                'id_reg' => 3196,
                'id_model' => 2628,
                'id_fuel' => 1,
            ),
            196 => 
            array (
                'id_reg' => 3197,
                'id_model' => 2628,
                'id_fuel' => 2,
            ),
            197 => 
            array (
                'id_reg' => 3198,
                'id_model' => 2628,
                'id_fuel' => 4,
            ),
            198 => 
            array (
                'id_reg' => 3199,
                'id_model' => 2629,
                'id_fuel' => 4,
            ),
            199 => 
            array (
                'id_reg' => 3200,
                'id_model' => 2630,
                'id_fuel' => 4,
            ),
            200 => 
            array (
                'id_reg' => 3201,
                'id_model' => 2631,
                'id_fuel' => 4,
            ),
            201 => 
            array (
                'id_reg' => 3202,
                'id_model' => 2632,
                'id_fuel' => 4,
            ),
            202 => 
            array (
                'id_reg' => 3203,
                'id_model' => 2633,
                'id_fuel' => 4,
            ),
            203 => 
            array (
                'id_reg' => 3204,
                'id_model' => 2634,
                'id_fuel' => 4,
            ),
            204 => 
            array (
                'id_reg' => 3205,
                'id_model' => 2635,
                'id_fuel' => 4,
            ),
            205 => 
            array (
                'id_reg' => 3206,
                'id_model' => 2636,
                'id_fuel' => 4,
            ),
            206 => 
            array (
                'id_reg' => 3207,
                'id_model' => 2637,
                'id_fuel' => 4,
            ),
            207 => 
            array (
                'id_reg' => 3208,
                'id_model' => 2638,
                'id_fuel' => 4,
            ),
            208 => 
            array (
                'id_reg' => 3209,
                'id_model' => 2639,
                'id_fuel' => 4,
            ),
            209 => 
            array (
                'id_reg' => 3210,
                'id_model' => 2640,
                'id_fuel' => 4,
            ),
            210 => 
            array (
                'id_reg' => 3211,
                'id_model' => 2641,
                'id_fuel' => 2,
            ),
            211 => 
            array (
                'id_reg' => 3212,
                'id_model' => 2642,
                'id_fuel' => 2,
            ),
            212 => 
            array (
                'id_reg' => 3213,
                'id_model' => 2643,
                'id_fuel' => 2,
            ),
            213 => 
            array (
                'id_reg' => 3214,
                'id_model' => 2643,
                'id_fuel' => 4,
            ),
            214 => 
            array (
                'id_reg' => 3215,
                'id_model' => 2644,
                'id_fuel' => 2,
            ),
            215 => 
            array (
                'id_reg' => 3216,
                'id_model' => 2644,
                'id_fuel' => 4,
            ),
            216 => 
            array (
                'id_reg' => 3217,
                'id_model' => 2645,
                'id_fuel' => 2,
            ),
            217 => 
            array (
                'id_reg' => 3218,
                'id_model' => 2646,
                'id_fuel' => 2,
            ),
            218 => 
            array (
                'id_reg' => 3219,
                'id_model' => 2647,
                'id_fuel' => 2,
            ),
            219 => 
            array (
                'id_reg' => 3220,
                'id_model' => 2648,
                'id_fuel' => 2,
            ),
            220 => 
            array (
                'id_reg' => 3221,
                'id_model' => 2649,
                'id_fuel' => 2,
            ),
            221 => 
            array (
                'id_reg' => 3222,
                'id_model' => 2649,
                'id_fuel' => 4,
            ),
            222 => 
            array (
                'id_reg' => 3223,
                'id_model' => 2650,
                'id_fuel' => 2,
            ),
            223 => 
            array (
                'id_reg' => 3224,
                'id_model' => 2651,
                'id_fuel' => 4,
            ),
            224 => 
            array (
                'id_reg' => 3225,
                'id_model' => 2652,
                'id_fuel' => 4,
            ),
            225 => 
            array (
                'id_reg' => 3226,
                'id_model' => 2653,
                'id_fuel' => 4,
            ),
            226 => 
            array (
                'id_reg' => 3227,
                'id_model' => 2654,
                'id_fuel' => 4,
            ),
            227 => 
            array (
                'id_reg' => 3228,
                'id_model' => 2655,
                'id_fuel' => 2,
            ),
            228 => 
            array (
                'id_reg' => 3229,
                'id_model' => 2656,
                'id_fuel' => 2,
            ),
            229 => 
            array (
                'id_reg' => 3230,
                'id_model' => 2657,
                'id_fuel' => 2,
            ),
            230 => 
            array (
                'id_reg' => 3231,
                'id_model' => 2658,
                'id_fuel' => 2,
            ),
            231 => 
            array (
                'id_reg' => 3232,
                'id_model' => 2659,
                'id_fuel' => 2,
            ),
            232 => 
            array (
                'id_reg' => 3233,
                'id_model' => 2660,
                'id_fuel' => 2,
            ),
            233 => 
            array (
                'id_reg' => 3234,
                'id_model' => 2661,
                'id_fuel' => 2,
            ),
            234 => 
            array (
                'id_reg' => 3235,
                'id_model' => 2662,
                'id_fuel' => 4,
            ),
            235 => 
            array (
                'id_reg' => 3236,
                'id_model' => 2663,
                'id_fuel' => 2,
            ),
            236 => 
            array (
                'id_reg' => 3237,
                'id_model' => 2663,
                'id_fuel' => 4,
            ),
            237 => 
            array (
                'id_reg' => 3238,
                'id_model' => 2664,
                'id_fuel' => 4,
            ),
            238 => 
            array (
                'id_reg' => 3239,
                'id_model' => 2665,
                'id_fuel' => 4,
            ),
            239 => 
            array (
                'id_reg' => 3240,
                'id_model' => 2666,
                'id_fuel' => 2,
            ),
            240 => 
            array (
                'id_reg' => 3241,
                'id_model' => 2666,
                'id_fuel' => 4,
            ),
            241 => 
            array (
                'id_reg' => 3242,
                'id_model' => 2667,
                'id_fuel' => 2,
            ),
            242 => 
            array (
                'id_reg' => 3243,
                'id_model' => 2667,
                'id_fuel' => 4,
            ),
            243 => 
            array (
                'id_reg' => 3244,
                'id_model' => 2668,
                'id_fuel' => 4,
            ),
            244 => 
            array (
                'id_reg' => 3245,
                'id_model' => 2669,
                'id_fuel' => 4,
            ),
            245 => 
            array (
                'id_reg' => 3246,
                'id_model' => 2670,
                'id_fuel' => 4,
            ),
            246 => 
            array (
                'id_reg' => 3247,
                'id_model' => 2671,
                'id_fuel' => 4,
            ),
            247 => 
            array (
                'id_reg' => 3248,
                'id_model' => 2672,
                'id_fuel' => 4,
            ),
            248 => 
            array (
                'id_reg' => 3249,
                'id_model' => 2673,
                'id_fuel' => 4,
            ),
            249 => 
            array (
                'id_reg' => 3250,
                'id_model' => 2674,
                'id_fuel' => 4,
            ),
            250 => 
            array (
                'id_reg' => 3251,
                'id_model' => 2675,
                'id_fuel' => 4,
            ),
            251 => 
            array (
                'id_reg' => 3252,
                'id_model' => 2676,
                'id_fuel' => 2,
            ),
            252 => 
            array (
                'id_reg' => 3253,
                'id_model' => 2677,
                'id_fuel' => 2,
            ),
            253 => 
            array (
                'id_reg' => 3254,
                'id_model' => 2678,
                'id_fuel' => 4,
            ),
            254 => 
            array (
                'id_reg' => 3255,
                'id_model' => 2679,
                'id_fuel' => 2,
            ),
            255 => 
            array (
                'id_reg' => 3256,
                'id_model' => 2679,
                'id_fuel' => 4,
            ),
            256 => 
            array (
                'id_reg' => 3257,
                'id_model' => 2680,
                'id_fuel' => 2,
            ),
            257 => 
            array (
                'id_reg' => 3258,
                'id_model' => 2680,
                'id_fuel' => 4,
            ),
            258 => 
            array (
                'id_reg' => 3259,
                'id_model' => 2681,
                'id_fuel' => 4,
            ),
            259 => 
            array (
                'id_reg' => 3260,
                'id_model' => 2682,
                'id_fuel' => 2,
            ),
            260 => 
            array (
                'id_reg' => 3261,
                'id_model' => 2683,
                'id_fuel' => 2,
            ),
            261 => 
            array (
                'id_reg' => 3262,
                'id_model' => 2683,
                'id_fuel' => 4,
            ),
            262 => 
            array (
                'id_reg' => 3263,
                'id_model' => 2684,
                'id_fuel' => 2,
            ),
            263 => 
            array (
                'id_reg' => 3264,
                'id_model' => 2684,
                'id_fuel' => 4,
            ),
            264 => 
            array (
                'id_reg' => 3265,
                'id_model' => 2685,
                'id_fuel' => 2,
            ),
            265 => 
            array (
                'id_reg' => 3266,
                'id_model' => 2685,
                'id_fuel' => 4,
            ),
            266 => 
            array (
                'id_reg' => 3267,
                'id_model' => 2686,
                'id_fuel' => 3,
            ),
            267 => 
            array (
                'id_reg' => 3268,
                'id_model' => 2686,
                'id_fuel' => 4,
            ),
            268 => 
            array (
                'id_reg' => 3269,
                'id_model' => 2687,
                'id_fuel' => 2,
            ),
            269 => 
            array (
                'id_reg' => 3270,
                'id_model' => 2688,
                'id_fuel' => 4,
            ),
            270 => 
            array (
                'id_reg' => 3271,
                'id_model' => 2689,
                'id_fuel' => 2,
            ),
            271 => 
            array (
                'id_reg' => 3272,
                'id_model' => 2689,
                'id_fuel' => 4,
            ),
            272 => 
            array (
                'id_reg' => 3273,
                'id_model' => 2690,
                'id_fuel' => 2,
            ),
            273 => 
            array (
                'id_reg' => 3274,
                'id_model' => 2690,
                'id_fuel' => 4,
            ),
            274 => 
            array (
                'id_reg' => 3275,
                'id_model' => 2691,
                'id_fuel' => 2,
            ),
            275 => 
            array (
                'id_reg' => 3276,
                'id_model' => 2691,
                'id_fuel' => 4,
            ),
            276 => 
            array (
                'id_reg' => 3277,
                'id_model' => 2692,
                'id_fuel' => 4,
            ),
            277 => 
            array (
                'id_reg' => 3278,
                'id_model' => 2693,
                'id_fuel' => 4,
            ),
            278 => 
            array (
                'id_reg' => 3279,
                'id_model' => 2694,
                'id_fuel' => 4,
            ),
            279 => 
            array (
                'id_reg' => 3280,
                'id_model' => 2695,
                'id_fuel' => 2,
            ),
            280 => 
            array (
                'id_reg' => 3281,
                'id_model' => 2695,
                'id_fuel' => 4,
            ),
            281 => 
            array (
                'id_reg' => 3282,
                'id_model' => 2696,
                'id_fuel' => 4,
            ),
            282 => 
            array (
                'id_reg' => 3283,
                'id_model' => 2697,
                'id_fuel' => 4,
            ),
            283 => 
            array (
                'id_reg' => 3284,
                'id_model' => 2698,
                'id_fuel' => 4,
            ),
            284 => 
            array (
                'id_reg' => 3285,
                'id_model' => 2699,
                'id_fuel' => 6,
            ),
            285 => 
            array (
                'id_reg' => 3286,
                'id_model' => 2699,
                'id_fuel' => 4,
            ),
            286 => 
            array (
                'id_reg' => 3287,
                'id_model' => 2700,
                'id_fuel' => 4,
            ),
            287 => 
            array (
                'id_reg' => 3288,
                'id_model' => 2701,
                'id_fuel' => 4,
            ),
            288 => 
            array (
                'id_reg' => 3289,
                'id_model' => 2702,
                'id_fuel' => 6,
            ),
            289 => 
            array (
                'id_reg' => 3290,
                'id_model' => 2703,
                'id_fuel' => 4,
            ),
            290 => 
            array (
                'id_reg' => 3291,
                'id_model' => 2704,
                'id_fuel' => 4,
            ),
            291 => 
            array (
                'id_reg' => 3292,
                'id_model' => 2705,
                'id_fuel' => 4,
            ),
            292 => 
            array (
                'id_reg' => 3293,
                'id_model' => 2706,
                'id_fuel' => 4,
            ),
            293 => 
            array (
                'id_reg' => 3294,
                'id_model' => 2707,
                'id_fuel' => 4,
            ),
            294 => 
            array (
                'id_reg' => 3295,
                'id_model' => 2708,
                'id_fuel' => 4,
            ),
            295 => 
            array (
                'id_reg' => 3296,
                'id_model' => 2709,
                'id_fuel' => 4,
            ),
            296 => 
            array (
                'id_reg' => 3297,
                'id_model' => 2710,
                'id_fuel' => 4,
            ),
            297 => 
            array (
                'id_reg' => 3298,
                'id_model' => 2711,
                'id_fuel' => 4,
            ),
            298 => 
            array (
                'id_reg' => 3299,
                'id_model' => 2712,
                'id_fuel' => 4,
            ),
            299 => 
            array (
                'id_reg' => 3300,
                'id_model' => 2713,
                'id_fuel' => 4,
            ),
            300 => 
            array (
                'id_reg' => 3301,
                'id_model' => 2714,
                'id_fuel' => 4,
            ),
            301 => 
            array (
                'id_reg' => 3302,
                'id_model' => 2715,
                'id_fuel' => 4,
            ),
            302 => 
            array (
                'id_reg' => 3303,
                'id_model' => 2716,
                'id_fuel' => 4,
            ),
            303 => 
            array (
                'id_reg' => 3304,
                'id_model' => 2717,
                'id_fuel' => 4,
            ),
            304 => 
            array (
                'id_reg' => 3305,
                'id_model' => 2718,
                'id_fuel' => 4,
            ),
            305 => 
            array (
                'id_reg' => 3306,
                'id_model' => 2719,
                'id_fuel' => 4,
            ),
            306 => 
            array (
                'id_reg' => 3307,
                'id_model' => 2720,
                'id_fuel' => 2,
            ),
            307 => 
            array (
                'id_reg' => 3308,
                'id_model' => 2721,
                'id_fuel' => 2,
            ),
            308 => 
            array (
                'id_reg' => 3309,
                'id_model' => 2722,
                'id_fuel' => 4,
            ),
            309 => 
            array (
                'id_reg' => 3310,
                'id_model' => 2723,
                'id_fuel' => 4,
            ),
            310 => 
            array (
                'id_reg' => 3311,
                'id_model' => 2724,
                'id_fuel' => 4,
            ),
            311 => 
            array (
                'id_reg' => 3312,
                'id_model' => 2725,
                'id_fuel' => 4,
            ),
            312 => 
            array (
                'id_reg' => 3313,
                'id_model' => 2726,
                'id_fuel' => 2,
            ),
            313 => 
            array (
                'id_reg' => 3314,
                'id_model' => 2727,
                'id_fuel' => 2,
            ),
            314 => 
            array (
                'id_reg' => 3315,
                'id_model' => 2727,
                'id_fuel' => 4,
            ),
            315 => 
            array (
                'id_reg' => 3316,
                'id_model' => 2728,
                'id_fuel' => 4,
            ),
            316 => 
            array (
                'id_reg' => 3317,
                'id_model' => 2729,
                'id_fuel' => 4,
            ),
            317 => 
            array (
                'id_reg' => 3318,
                'id_model' => 2730,
                'id_fuel' => 2,
            ),
            318 => 
            array (
                'id_reg' => 3319,
                'id_model' => 2730,
                'id_fuel' => 4,
            ),
            319 => 
            array (
                'id_reg' => 3320,
                'id_model' => 2731,
                'id_fuel' => 2,
            ),
            320 => 
            array (
                'id_reg' => 3321,
                'id_model' => 2731,
                'id_fuel' => 4,
            ),
            321 => 
            array (
                'id_reg' => 3322,
                'id_model' => 2732,
                'id_fuel' => 2,
            ),
            322 => 
            array (
                'id_reg' => 3323,
                'id_model' => 2732,
                'id_fuel' => 4,
            ),
            323 => 
            array (
                'id_reg' => 3324,
                'id_model' => 2733,
                'id_fuel' => 4,
            ),
            324 => 
            array (
                'id_reg' => 3325,
                'id_model' => 2734,
                'id_fuel' => 2,
            ),
            325 => 
            array (
                'id_reg' => 3326,
                'id_model' => 2734,
                'id_fuel' => 4,
            ),
            326 => 
            array (
                'id_reg' => 3327,
                'id_model' => 2735,
                'id_fuel' => 2,
            ),
            327 => 
            array (
                'id_reg' => 3328,
                'id_model' => 2735,
                'id_fuel' => 6,
            ),
            328 => 
            array (
                'id_reg' => 3329,
                'id_model' => 2735,
                'id_fuel' => 4,
            ),
            329 => 
            array (
                'id_reg' => 3330,
                'id_model' => 2736,
                'id_fuel' => 2,
            ),
            330 => 
            array (
                'id_reg' => 3331,
                'id_model' => 2736,
                'id_fuel' => 6,
            ),
            331 => 
            array (
                'id_reg' => 3332,
                'id_model' => 2736,
                'id_fuel' => 4,
            ),
            332 => 
            array (
                'id_reg' => 3333,
                'id_model' => 2737,
                'id_fuel' => 4,
            ),
            333 => 
            array (
                'id_reg' => 3334,
                'id_model' => 2738,
                'id_fuel' => 4,
            ),
            334 => 
            array (
                'id_reg' => 3335,
                'id_model' => 2739,
                'id_fuel' => 4,
            ),
            335 => 
            array (
                'id_reg' => 3336,
                'id_model' => 2740,
                'id_fuel' => 4,
            ),
            336 => 
            array (
                'id_reg' => 3337,
                'id_model' => 2741,
                'id_fuel' => 4,
            ),
            337 => 
            array (
                'id_reg' => 3338,
                'id_model' => 2742,
                'id_fuel' => 4,
            ),
            338 => 
            array (
                'id_reg' => 3339,
                'id_model' => 2743,
                'id_fuel' => 4,
            ),
            339 => 
            array (
                'id_reg' => 3340,
                'id_model' => 2744,
                'id_fuel' => 6,
            ),
            340 => 
            array (
                'id_reg' => 3341,
                'id_model' => 2744,
                'id_fuel' => 4,
            ),
            341 => 
            array (
                'id_reg' => 3342,
                'id_model' => 2745,
                'id_fuel' => 6,
            ),
            342 => 
            array (
                'id_reg' => 3343,
                'id_model' => 2745,
                'id_fuel' => 4,
            ),
            343 => 
            array (
                'id_reg' => 3344,
                'id_model' => 2746,
                'id_fuel' => 6,
            ),
            344 => 
            array (
                'id_reg' => 3345,
                'id_model' => 2746,
                'id_fuel' => 4,
            ),
            345 => 
            array (
                'id_reg' => 3346,
                'id_model' => 2747,
                'id_fuel' => 6,
            ),
            346 => 
            array (
                'id_reg' => 3347,
                'id_model' => 2747,
                'id_fuel' => 4,
            ),
            347 => 
            array (
                'id_reg' => 3348,
                'id_model' => 2748,
                'id_fuel' => 2,
            ),
            348 => 
            array (
                'id_reg' => 3349,
                'id_model' => 2748,
                'id_fuel' => 4,
            ),
            349 => 
            array (
                'id_reg' => 3350,
                'id_model' => 2749,
                'id_fuel' => 2,
            ),
            350 => 
            array (
                'id_reg' => 3351,
                'id_model' => 2750,
                'id_fuel' => 4,
            ),
            351 => 
            array (
                'id_reg' => 3352,
                'id_model' => 2751,
                'id_fuel' => 2,
            ),
            352 => 
            array (
                'id_reg' => 3353,
                'id_model' => 2752,
                'id_fuel' => 2,
            ),
            353 => 
            array (
                'id_reg' => 3354,
                'id_model' => 2753,
                'id_fuel' => 4,
            ),
            354 => 
            array (
                'id_reg' => 3355,
                'id_model' => 2754,
                'id_fuel' => 2,
            ),
            355 => 
            array (
                'id_reg' => 3356,
                'id_model' => 2754,
                'id_fuel' => 4,
            ),
            356 => 
            array (
                'id_reg' => 3357,
                'id_model' => 2755,
                'id_fuel' => 2,
            ),
            357 => 
            array (
                'id_reg' => 3358,
                'id_model' => 2755,
                'id_fuel' => 4,
            ),
            358 => 
            array (
                'id_reg' => 3359,
                'id_model' => 2756,
                'id_fuel' => 2,
            ),
            359 => 
            array (
                'id_reg' => 3360,
                'id_model' => 2757,
                'id_fuel' => 2,
            ),
            360 => 
            array (
                'id_reg' => 3361,
                'id_model' => 2757,
                'id_fuel' => 4,
            ),
            361 => 
            array (
                'id_reg' => 3362,
                'id_model' => 2758,
                'id_fuel' => 2,
            ),
            362 => 
            array (
                'id_reg' => 3363,
                'id_model' => 2759,
                'id_fuel' => 2,
            ),
            363 => 
            array (
                'id_reg' => 3364,
                'id_model' => 2759,
                'id_fuel' => 4,
            ),
            364 => 
            array (
                'id_reg' => 3365,
                'id_model' => 2760,
                'id_fuel' => 2,
            ),
            365 => 
            array (
                'id_reg' => 3366,
                'id_model' => 2761,
                'id_fuel' => 2,
            ),
            366 => 
            array (
                'id_reg' => 3367,
                'id_model' => 2762,
                'id_fuel' => 2,
            ),
            367 => 
            array (
                'id_reg' => 3368,
                'id_model' => 2763,
                'id_fuel' => 2,
            ),
            368 => 
            array (
                'id_reg' => 3369,
                'id_model' => 2764,
                'id_fuel' => 2,
            ),
            369 => 
            array (
                'id_reg' => 3370,
                'id_model' => 2764,
                'id_fuel' => 3,
            ),
            370 => 
            array (
                'id_reg' => 3371,
                'id_model' => 2764,
                'id_fuel' => 4,
            ),
            371 => 
            array (
                'id_reg' => 3372,
                'id_model' => 2765,
                'id_fuel' => 2,
            ),
            372 => 
            array (
                'id_reg' => 3373,
                'id_model' => 2765,
                'id_fuel' => 4,
            ),
            373 => 
            array (
                'id_reg' => 3374,
                'id_model' => 2766,
                'id_fuel' => 6,
            ),
            374 => 
            array (
                'id_reg' => 3375,
                'id_model' => 2767,
                'id_fuel' => 4,
            ),
            375 => 
            array (
                'id_reg' => 3376,
                'id_model' => 2768,
                'id_fuel' => 2,
            ),
            376 => 
            array (
                'id_reg' => 3377,
                'id_model' => 2769,
                'id_fuel' => 4,
            ),
            377 => 
            array (
                'id_reg' => 3378,
                'id_model' => 2770,
                'id_fuel' => 2,
            ),
            378 => 
            array (
                'id_reg' => 3379,
                'id_model' => 2770,
                'id_fuel' => 4,
            ),
            379 => 
            array (
                'id_reg' => 3380,
                'id_model' => 2771,
                'id_fuel' => 4,
            ),
            380 => 
            array (
                'id_reg' => 3381,
                'id_model' => 2772,
                'id_fuel' => 4,
            ),
            381 => 
            array (
                'id_reg' => 3382,
                'id_model' => 2773,
                'id_fuel' => 4,
            ),
            382 => 
            array (
                'id_reg' => 3383,
                'id_model' => 2774,
                'id_fuel' => 4,
            ),
            383 => 
            array (
                'id_reg' => 3384,
                'id_model' => 2775,
                'id_fuel' => 4,
            ),
            384 => 
            array (
                'id_reg' => 3385,
                'id_model' => 2776,
                'id_fuel' => 4,
            ),
            385 => 
            array (
                'id_reg' => 3386,
                'id_model' => 2777,
                'id_fuel' => 4,
            ),
            386 => 
            array (
                'id_reg' => 3387,
                'id_model' => 2778,
                'id_fuel' => 4,
            ),
            387 => 
            array (
                'id_reg' => 3388,
                'id_model' => 2779,
                'id_fuel' => 4,
            ),
            388 => 
            array (
                'id_reg' => 3389,
                'id_model' => 2780,
                'id_fuel' => 4,
            ),
            389 => 
            array (
                'id_reg' => 3390,
                'id_model' => 2781,
                'id_fuel' => 4,
            ),
            390 => 
            array (
                'id_reg' => 3391,
                'id_model' => 2782,
                'id_fuel' => 4,
            ),
            391 => 
            array (
                'id_reg' => 3392,
                'id_model' => 2783,
                'id_fuel' => 4,
            ),
            392 => 
            array (
                'id_reg' => 3393,
                'id_model' => 2784,
                'id_fuel' => 4,
            ),
            393 => 
            array (
                'id_reg' => 3394,
                'id_model' => 2785,
                'id_fuel' => 4,
            ),
            394 => 
            array (
                'id_reg' => 3395,
                'id_model' => 2786,
                'id_fuel' => 4,
            ),
            395 => 
            array (
                'id_reg' => 3396,
                'id_model' => 2787,
                'id_fuel' => 2,
            ),
            396 => 
            array (
                'id_reg' => 3397,
                'id_model' => 2787,
                'id_fuel' => 4,
            ),
            397 => 
            array (
                'id_reg' => 3398,
                'id_model' => 2788,
                'id_fuel' => 2,
            ),
            398 => 
            array (
                'id_reg' => 3399,
                'id_model' => 2788,
                'id_fuel' => 4,
            ),
            399 => 
            array (
                'id_reg' => 3400,
                'id_model' => 2789,
                'id_fuel' => 2,
            ),
            400 => 
            array (
                'id_reg' => 3401,
                'id_model' => 2789,
                'id_fuel' => 4,
            ),
            401 => 
            array (
                'id_reg' => 3402,
                'id_model' => 2790,
                'id_fuel' => 2,
            ),
            402 => 
            array (
                'id_reg' => 3403,
                'id_model' => 2790,
                'id_fuel' => 4,
            ),
            403 => 
            array (
                'id_reg' => 3404,
                'id_model' => 2791,
                'id_fuel' => 4,
            ),
            404 => 
            array (
                'id_reg' => 3405,
                'id_model' => 2792,
                'id_fuel' => 4,
            ),
            405 => 
            array (
                'id_reg' => 3406,
                'id_model' => 2793,
                'id_fuel' => 4,
            ),
            406 => 
            array (
                'id_reg' => 3407,
                'id_model' => 2794,
                'id_fuel' => 4,
            ),
            407 => 
            array (
                'id_reg' => 3408,
                'id_model' => 2795,
                'id_fuel' => 4,
            ),
            408 => 
            array (
                'id_reg' => 3409,
                'id_model' => 2796,
                'id_fuel' => 2,
            ),
            409 => 
            array (
                'id_reg' => 3410,
                'id_model' => 2796,
                'id_fuel' => 3,
            ),
            410 => 
            array (
                'id_reg' => 3411,
                'id_model' => 2796,
                'id_fuel' => 4,
            ),
            411 => 
            array (
                'id_reg' => 3412,
                'id_model' => 2797,
                'id_fuel' => 4,
            ),
            412 => 
            array (
                'id_reg' => 3413,
                'id_model' => 2798,
                'id_fuel' => 4,
            ),
            413 => 
            array (
                'id_reg' => 3414,
                'id_model' => 2799,
                'id_fuel' => 4,
            ),
            414 => 
            array (
                'id_reg' => 3415,
                'id_model' => 2800,
                'id_fuel' => 2,
            ),
            415 => 
            array (
                'id_reg' => 3416,
                'id_model' => 2800,
                'id_fuel' => 4,
            ),
            416 => 
            array (
                'id_reg' => 3417,
                'id_model' => 2801,
                'id_fuel' => 4,
            ),
            417 => 
            array (
                'id_reg' => 3418,
                'id_model' => 2802,
                'id_fuel' => 4,
            ),
            418 => 
            array (
                'id_reg' => 3419,
                'id_model' => 2803,
                'id_fuel' => 4,
            ),
            419 => 
            array (
                'id_reg' => 3420,
                'id_model' => 2804,
                'id_fuel' => 4,
            ),
            420 => 
            array (
                'id_reg' => 3421,
                'id_model' => 2805,
                'id_fuel' => 4,
            ),
            421 => 
            array (
                'id_reg' => 3422,
                'id_model' => 2806,
                'id_fuel' => 2,
            ),
            422 => 
            array (
                'id_reg' => 3423,
                'id_model' => 2806,
                'id_fuel' => 3,
            ),
            423 => 
            array (
                'id_reg' => 3424,
                'id_model' => 2806,
                'id_fuel' => 4,
            ),
            424 => 
            array (
                'id_reg' => 3425,
                'id_model' => 2807,
                'id_fuel' => 4,
            ),
            425 => 
            array (
                'id_reg' => 3426,
                'id_model' => 2808,
                'id_fuel' => 4,
            ),
            426 => 
            array (
                'id_reg' => 3427,
                'id_model' => 2809,
                'id_fuel' => 4,
            ),
            427 => 
            array (
                'id_reg' => 3428,
                'id_model' => 2810,
                'id_fuel' => 4,
            ),
            428 => 
            array (
                'id_reg' => 3429,
                'id_model' => 2811,
                'id_fuel' => 4,
            ),
            429 => 
            array (
                'id_reg' => 3430,
                'id_model' => 2812,
                'id_fuel' => 4,
            ),
            430 => 
            array (
                'id_reg' => 3431,
                'id_model' => 2813,
                'id_fuel' => 4,
            ),
            431 => 
            array (
                'id_reg' => 3432,
                'id_model' => 2814,
                'id_fuel' => 4,
            ),
            432 => 
            array (
                'id_reg' => 3433,
                'id_model' => 2815,
                'id_fuel' => 4,
            ),
            433 => 
            array (
                'id_reg' => 3434,
                'id_model' => 2816,
                'id_fuel' => 6,
            ),
            434 => 
            array (
                'id_reg' => 3435,
                'id_model' => 2817,
                'id_fuel' => 4,
            ),
            435 => 
            array (
                'id_reg' => 3436,
                'id_model' => 2818,
                'id_fuel' => 4,
            ),
            436 => 
            array (
                'id_reg' => 3437,
                'id_model' => 2819,
                'id_fuel' => 2,
            ),
            437 => 
            array (
                'id_reg' => 3438,
                'id_model' => 2819,
                'id_fuel' => 4,
            ),
            438 => 
            array (
                'id_reg' => 3439,
                'id_model' => 2819,
                'id_fuel' => 5,
            ),
            439 => 
            array (
                'id_reg' => 3440,
                'id_model' => 2820,
                'id_fuel' => 4,
            ),
            440 => 
            array (
                'id_reg' => 3441,
                'id_model' => 2821,
                'id_fuel' => 4,
            ),
            441 => 
            array (
                'id_reg' => 3442,
                'id_model' => 2822,
                'id_fuel' => 4,
            ),
            442 => 
            array (
                'id_reg' => 3443,
                'id_model' => 2823,
                'id_fuel' => 4,
            ),
            443 => 
            array (
                'id_reg' => 3444,
                'id_model' => 2824,
                'id_fuel' => 4,
            ),
            444 => 
            array (
                'id_reg' => 3445,
                'id_model' => 2825,
                'id_fuel' => 4,
            ),
            445 => 
            array (
                'id_reg' => 3446,
                'id_model' => 2826,
                'id_fuel' => 2,
            ),
            446 => 
            array (
                'id_reg' => 3447,
                'id_model' => 2826,
                'id_fuel' => 4,
            ),
            447 => 
            array (
                'id_reg' => 3448,
                'id_model' => 2827,
                'id_fuel' => 2,
            ),
            448 => 
            array (
                'id_reg' => 3449,
                'id_model' => 2827,
                'id_fuel' => 4,
            ),
            449 => 
            array (
                'id_reg' => 3450,
                'id_model' => 2828,
                'id_fuel' => 2,
            ),
            450 => 
            array (
                'id_reg' => 3451,
                'id_model' => 2828,
                'id_fuel' => 4,
            ),
            451 => 
            array (
                'id_reg' => 3452,
                'id_model' => 2829,
                'id_fuel' => 4,
            ),
            452 => 
            array (
                'id_reg' => 3453,
                'id_model' => 2830,
                'id_fuel' => 4,
            ),
            453 => 
            array (
                'id_reg' => 3454,
                'id_model' => 2831,
                'id_fuel' => 2,
            ),
            454 => 
            array (
                'id_reg' => 3455,
                'id_model' => 2831,
                'id_fuel' => 4,
            ),
            455 => 
            array (
                'id_reg' => 3456,
                'id_model' => 2832,
                'id_fuel' => 4,
            ),
            456 => 
            array (
                'id_reg' => 3457,
                'id_model' => 2833,
                'id_fuel' => 4,
            ),
            457 => 
            array (
                'id_reg' => 3458,
                'id_model' => 2834,
                'id_fuel' => 4,
            ),
            458 => 
            array (
                'id_reg' => 3459,
                'id_model' => 2835,
                'id_fuel' => 2,
            ),
            459 => 
            array (
                'id_reg' => 3460,
                'id_model' => 2835,
                'id_fuel' => 4,
            ),
            460 => 
            array (
                'id_reg' => 3461,
                'id_model' => 2836,
                'id_fuel' => 4,
            ),
            461 => 
            array (
                'id_reg' => 3462,
                'id_model' => 2837,
                'id_fuel' => 4,
            ),
            462 => 
            array (
                'id_reg' => 3463,
                'id_model' => 2838,
                'id_fuel' => 4,
            ),
            463 => 
            array (
                'id_reg' => 3464,
                'id_model' => 2839,
                'id_fuel' => 4,
            ),
            464 => 
            array (
                'id_reg' => 3465,
                'id_model' => 2840,
                'id_fuel' => 2,
            ),
            465 => 
            array (
                'id_reg' => 3466,
                'id_model' => 2840,
                'id_fuel' => 4,
            ),
            466 => 
            array (
                'id_reg' => 3467,
                'id_model' => 2841,
                'id_fuel' => 4,
            ),
            467 => 
            array (
                'id_reg' => 3468,
                'id_model' => 2842,
                'id_fuel' => 2,
            ),
            468 => 
            array (
                'id_reg' => 3469,
                'id_model' => 2842,
                'id_fuel' => 4,
            ),
            469 => 
            array (
                'id_reg' => 3470,
                'id_model' => 2843,
                'id_fuel' => 2,
            ),
            470 => 
            array (
                'id_reg' => 3471,
                'id_model' => 2843,
                'id_fuel' => 4,
            ),
            471 => 
            array (
                'id_reg' => 3472,
                'id_model' => 2844,
                'id_fuel' => 2,
            ),
            472 => 
            array (
                'id_reg' => 3473,
                'id_model' => 2844,
                'id_fuel' => 4,
            ),
            473 => 
            array (
                'id_reg' => 3474,
                'id_model' => 2845,
                'id_fuel' => 2,
            ),
            474 => 
            array (
                'id_reg' => 3475,
                'id_model' => 2845,
                'id_fuel' => 4,
            ),
            475 => 
            array (
                'id_reg' => 3476,
                'id_model' => 2846,
                'id_fuel' => 4,
            ),
            476 => 
            array (
                'id_reg' => 3477,
                'id_model' => 2847,
                'id_fuel' => 2,
            ),
            477 => 
            array (
                'id_reg' => 3478,
                'id_model' => 2847,
                'id_fuel' => 4,
            ),
            478 => 
            array (
                'id_reg' => 3479,
                'id_model' => 2848,
                'id_fuel' => 2,
            ),
            479 => 
            array (
                'id_reg' => 3480,
                'id_model' => 2848,
                'id_fuel' => 4,
            ),
            480 => 
            array (
                'id_reg' => 3481,
                'id_model' => 2849,
                'id_fuel' => 4,
            ),
            481 => 
            array (
                'id_reg' => 3482,
                'id_model' => 2850,
                'id_fuel' => 4,
            ),
            482 => 
            array (
                'id_reg' => 3483,
                'id_model' => 2851,
                'id_fuel' => 2,
            ),
            483 => 
            array (
                'id_reg' => 3484,
                'id_model' => 2852,
                'id_fuel' => 4,
            ),
            484 => 
            array (
                'id_reg' => 3485,
                'id_model' => 2853,
                'id_fuel' => 4,
            ),
            485 => 
            array (
                'id_reg' => 3486,
                'id_model' => 2854,
                'id_fuel' => 4,
            ),
            486 => 
            array (
                'id_reg' => 3487,
                'id_model' => 2855,
                'id_fuel' => 4,
            ),
            487 => 
            array (
                'id_reg' => 3488,
                'id_model' => 2856,
                'id_fuel' => 2,
            ),
            488 => 
            array (
                'id_reg' => 3489,
                'id_model' => 2856,
                'id_fuel' => 4,
            ),
            489 => 
            array (
                'id_reg' => 3490,
                'id_model' => 2857,
                'id_fuel' => 2,
            ),
            490 => 
            array (
                'id_reg' => 3491,
                'id_model' => 2857,
                'id_fuel' => 4,
            ),
            491 => 
            array (
                'id_reg' => 3492,
                'id_model' => 2858,
                'id_fuel' => 4,
            ),
            492 => 
            array (
                'id_reg' => 3493,
                'id_model' => 2859,
                'id_fuel' => 4,
            ),
            493 => 
            array (
                'id_reg' => 3494,
                'id_model' => 2860,
                'id_fuel' => 2,
            ),
            494 => 
            array (
                'id_reg' => 3495,
                'id_model' => 2860,
                'id_fuel' => 4,
            ),
            495 => 
            array (
                'id_reg' => 3496,
                'id_model' => 2861,
                'id_fuel' => 4,
            ),
            496 => 
            array (
                'id_reg' => 3497,
                'id_model' => 2862,
                'id_fuel' => 2,
            ),
            497 => 
            array (
                'id_reg' => 3498,
                'id_model' => 2862,
                'id_fuel' => 4,
            ),
            498 => 
            array (
                'id_reg' => 3499,
                'id_model' => 2863,
                'id_fuel' => 4,
            ),
            499 => 
            array (
                'id_reg' => 3500,
                'id_model' => 2864,
                'id_fuel' => 2,
            ),
        ));
        \DB::table('model_rel_fuel')->insert(array (
            0 => 
            array (
                'id_reg' => 3501,
                'id_model' => 2865,
                'id_fuel' => 2,
            ),
            1 => 
            array (
                'id_reg' => 3502,
                'id_model' => 2866,
                'id_fuel' => 2,
            ),
            2 => 
            array (
                'id_reg' => 3503,
                'id_model' => 2867,
                'id_fuel' => 2,
            ),
            3 => 
            array (
                'id_reg' => 3504,
                'id_model' => 2868,
                'id_fuel' => 2,
            ),
            4 => 
            array (
                'id_reg' => 3505,
                'id_model' => 2869,
                'id_fuel' => 6,
            ),
            5 => 
            array (
                'id_reg' => 3506,
                'id_model' => 2870,
                'id_fuel' => 2,
            ),
            6 => 
            array (
                'id_reg' => 3507,
                'id_model' => 2871,
                'id_fuel' => 2,
            ),
            7 => 
            array (
                'id_reg' => 3508,
                'id_model' => 2872,
                'id_fuel' => 2,
            ),
            8 => 
            array (
                'id_reg' => 3509,
                'id_model' => 2872,
                'id_fuel' => 4,
            ),
            9 => 
            array (
                'id_reg' => 3510,
                'id_model' => 2873,
                'id_fuel' => 2,
            ),
            10 => 
            array (
                'id_reg' => 3511,
                'id_model' => 2873,
                'id_fuel' => 4,
            ),
            11 => 
            array (
                'id_reg' => 3512,
                'id_model' => 2874,
                'id_fuel' => 2,
            ),
            12 => 
            array (
                'id_reg' => 3513,
                'id_model' => 2875,
                'id_fuel' => 2,
            ),
            13 => 
            array (
                'id_reg' => 3514,
                'id_model' => 2875,
                'id_fuel' => 4,
            ),
            14 => 
            array (
                'id_reg' => 3515,
                'id_model' => 2876,
                'id_fuel' => 2,
            ),
            15 => 
            array (
                'id_reg' => 3516,
                'id_model' => 2877,
                'id_fuel' => 2,
            ),
            16 => 
            array (
                'id_reg' => 3517,
                'id_model' => 2877,
                'id_fuel' => 4,
            ),
            17 => 
            array (
                'id_reg' => 3518,
                'id_model' => 2878,
                'id_fuel' => 2,
            ),
            18 => 
            array (
                'id_reg' => 3519,
                'id_model' => 2878,
                'id_fuel' => 4,
            ),
            19 => 
            array (
                'id_reg' => 3520,
                'id_model' => 2879,
                'id_fuel' => 2,
            ),
            20 => 
            array (
                'id_reg' => 3521,
                'id_model' => 2880,
                'id_fuel' => 6,
            ),
            21 => 
            array (
                'id_reg' => 3522,
                'id_model' => 2881,
                'id_fuel' => 6,
            ),
            22 => 
            array (
                'id_reg' => 3523,
                'id_model' => 2882,
                'id_fuel' => 6,
            ),
            23 => 
            array (
                'id_reg' => 3524,
                'id_model' => 2883,
                'id_fuel' => 6,
            ),
            24 => 
            array (
                'id_reg' => 3525,
                'id_model' => 2884,
                'id_fuel' => 6,
            ),
            25 => 
            array (
                'id_reg' => 3526,
                'id_model' => 2885,
                'id_fuel' => 6,
            ),
            26 => 
            array (
                'id_reg' => 3527,
                'id_model' => 2886,
                'id_fuel' => 6,
            ),
            27 => 
            array (
                'id_reg' => 3528,
                'id_model' => 2887,
                'id_fuel' => 6,
            ),
            28 => 
            array (
                'id_reg' => 3529,
                'id_model' => 2888,
                'id_fuel' => 6,
            ),
            29 => 
            array (
                'id_reg' => 3530,
                'id_model' => 2889,
                'id_fuel' => 6,
            ),
            30 => 
            array (
                'id_reg' => 3531,
                'id_model' => 2890,
                'id_fuel' => 6,
            ),
            31 => 
            array (
                'id_reg' => 3532,
                'id_model' => 2891,
                'id_fuel' => 6,
            ),
            32 => 
            array (
                'id_reg' => 3533,
                'id_model' => 2892,
                'id_fuel' => 6,
            ),
            33 => 
            array (
                'id_reg' => 3534,
                'id_model' => 2893,
                'id_fuel' => 6,
            ),
            34 => 
            array (
                'id_reg' => 3535,
                'id_model' => 2894,
                'id_fuel' => 6,
            ),
            35 => 
            array (
                'id_reg' => 3536,
                'id_model' => 2895,
                'id_fuel' => 6,
            ),
            36 => 
            array (
                'id_reg' => 3537,
                'id_model' => 2896,
                'id_fuel' => 6,
            ),
            37 => 
            array (
                'id_reg' => 3538,
                'id_model' => 2897,
                'id_fuel' => 6,
            ),
            38 => 
            array (
                'id_reg' => 3539,
                'id_model' => 2898,
                'id_fuel' => 6,
            ),
            39 => 
            array (
                'id_reg' => 3540,
                'id_model' => 2899,
                'id_fuel' => 4,
            ),
            40 => 
            array (
                'id_reg' => 3541,
                'id_model' => 2900,
                'id_fuel' => 4,
            ),
            41 => 
            array (
                'id_reg' => 3542,
                'id_model' => 2901,
                'id_fuel' => 4,
            ),
            42 => 
            array (
                'id_reg' => 3543,
                'id_model' => 2902,
                'id_fuel' => 2,
            ),
            43 => 
            array (
                'id_reg' => 3544,
                'id_model' => 2903,
                'id_fuel' => 4,
            ),
            44 => 
            array (
                'id_reg' => 3545,
                'id_model' => 2904,
                'id_fuel' => 6,
            ),
            45 => 
            array (
                'id_reg' => 3546,
                'id_model' => 2905,
                'id_fuel' => 6,
            ),
            46 => 
            array (
                'id_reg' => 3547,
                'id_model' => 2906,
                'id_fuel' => 6,
            ),
            47 => 
            array (
                'id_reg' => 3548,
                'id_model' => 2907,
                'id_fuel' => 6,
            ),
            48 => 
            array (
                'id_reg' => 3549,
                'id_model' => 2908,
                'id_fuel' => 6,
            ),
            49 => 
            array (
                'id_reg' => 3550,
                'id_model' => 2909,
                'id_fuel' => 6,
            ),
            50 => 
            array (
                'id_reg' => 3551,
                'id_model' => 2910,
                'id_fuel' => 6,
            ),
            51 => 
            array (
                'id_reg' => 3552,
                'id_model' => 2911,
                'id_fuel' => 6,
            ),
            52 => 
            array (
                'id_reg' => 3553,
                'id_model' => 2912,
                'id_fuel' => 6,
            ),
            53 => 
            array (
                'id_reg' => 3554,
                'id_model' => 2913,
                'id_fuel' => 6,
            ),
            54 => 
            array (
                'id_reg' => 3555,
                'id_model' => 2914,
                'id_fuel' => 6,
            ),
            55 => 
            array (
                'id_reg' => 3556,
                'id_model' => 2915,
                'id_fuel' => 6,
            ),
            56 => 
            array (
                'id_reg' => 3557,
                'id_model' => 2916,
                'id_fuel' => 6,
            ),
            57 => 
            array (
                'id_reg' => 3558,
                'id_model' => 2917,
                'id_fuel' => 6,
            ),
            58 => 
            array (
                'id_reg' => 3559,
                'id_model' => 2918,
                'id_fuel' => 6,
            ),
            59 => 
            array (
                'id_reg' => 3560,
                'id_model' => 2919,
                'id_fuel' => 6,
            ),
            60 => 
            array (
                'id_reg' => 3561,
                'id_model' => 2920,
                'id_fuel' => 6,
            ),
            61 => 
            array (
                'id_reg' => 3562,
                'id_model' => 2921,
                'id_fuel' => 6,
            ),
            62 => 
            array (
                'id_reg' => 3563,
                'id_model' => 2922,
                'id_fuel' => 6,
            ),
            63 => 
            array (
                'id_reg' => 3564,
                'id_model' => 2923,
                'id_fuel' => 4,
            ),
            64 => 
            array (
                'id_reg' => 3565,
                'id_model' => 2924,
                'id_fuel' => 2,
            ),
            65 => 
            array (
                'id_reg' => 3566,
                'id_model' => 2925,
                'id_fuel' => 2,
            ),
            66 => 
            array (
                'id_reg' => 3567,
                'id_model' => 2925,
                'id_fuel' => 4,
            ),
            67 => 
            array (
                'id_reg' => 3568,
                'id_model' => 2926,
                'id_fuel' => 2,
            ),
            68 => 
            array (
                'id_reg' => 3569,
                'id_model' => 2926,
                'id_fuel' => 4,
            ),
            69 => 
            array (
                'id_reg' => 3570,
                'id_model' => 2927,
                'id_fuel' => 2,
            ),
            70 => 
            array (
                'id_reg' => 3571,
                'id_model' => 2927,
                'id_fuel' => 4,
            ),
            71 => 
            array (
                'id_reg' => 3572,
                'id_model' => 2928,
                'id_fuel' => 2,
            ),
            72 => 
            array (
                'id_reg' => 3573,
                'id_model' => 2929,
                'id_fuel' => 2,
            ),
            73 => 
            array (
                'id_reg' => 3574,
                'id_model' => 2929,
                'id_fuel' => 6,
            ),
            74 => 
            array (
                'id_reg' => 3575,
                'id_model' => 2930,
                'id_fuel' => 2,
            ),
            75 => 
            array (
                'id_reg' => 3576,
                'id_model' => 2930,
                'id_fuel' => 4,
            ),
            76 => 
            array (
                'id_reg' => 3577,
                'id_model' => 2931,
                'id_fuel' => 4,
            ),
            77 => 
            array (
                'id_reg' => 3578,
                'id_model' => 2932,
                'id_fuel' => 4,
            ),
            78 => 
            array (
                'id_reg' => 3579,
                'id_model' => 2933,
                'id_fuel' => 4,
            ),
            79 => 
            array (
                'id_reg' => 3580,
                'id_model' => 2934,
                'id_fuel' => 4,
            ),
            80 => 
            array (
                'id_reg' => 3581,
                'id_model' => 2935,
                'id_fuel' => 4,
            ),
            81 => 
            array (
                'id_reg' => 3582,
                'id_model' => 2935,
                'id_fuel' => 5,
            ),
            82 => 
            array (
                'id_reg' => 3583,
                'id_model' => 2936,
                'id_fuel' => 2,
            ),
            83 => 
            array (
                'id_reg' => 3584,
                'id_model' => 2936,
                'id_fuel' => 4,
            ),
            84 => 
            array (
                'id_reg' => 3585,
                'id_model' => 2937,
                'id_fuel' => 2,
            ),
            85 => 
            array (
                'id_reg' => 3586,
                'id_model' => 2937,
                'id_fuel' => 4,
            ),
            86 => 
            array (
                'id_reg' => 3587,
                'id_model' => 2938,
                'id_fuel' => 4,
            ),
            87 => 
            array (
                'id_reg' => 3588,
                'id_model' => 2939,
                'id_fuel' => 4,
            ),
            88 => 
            array (
                'id_reg' => 3589,
                'id_model' => 2940,
                'id_fuel' => 2,
            ),
            89 => 
            array (
                'id_reg' => 3590,
                'id_model' => 2940,
                'id_fuel' => 4,
            ),
            90 => 
            array (
                'id_reg' => 3591,
                'id_model' => 2941,
                'id_fuel' => 2,
            ),
            91 => 
            array (
                'id_reg' => 3592,
                'id_model' => 2941,
                'id_fuel' => 4,
            ),
            92 => 
            array (
                'id_reg' => 3593,
                'id_model' => 2942,
                'id_fuel' => 2,
            ),
            93 => 
            array (
                'id_reg' => 3594,
                'id_model' => 2943,
                'id_fuel' => 2,
            ),
            94 => 
            array (
                'id_reg' => 3595,
                'id_model' => 2944,
                'id_fuel' => 2,
            ),
            95 => 
            array (
                'id_reg' => 3596,
                'id_model' => 2945,
                'id_fuel' => 2,
            ),
            96 => 
            array (
                'id_reg' => 3597,
                'id_model' => 2946,
                'id_fuel' => 4,
            ),
            97 => 
            array (
                'id_reg' => 3598,
                'id_model' => 2947,
                'id_fuel' => 2,
            ),
            98 => 
            array (
                'id_reg' => 3599,
                'id_model' => 2947,
                'id_fuel' => 4,
            ),
            99 => 
            array (
                'id_reg' => 3600,
                'id_model' => 2948,
                'id_fuel' => 4,
            ),
            100 => 
            array (
                'id_reg' => 3601,
                'id_model' => 2949,
                'id_fuel' => 4,
            ),
            101 => 
            array (
                'id_reg' => 3602,
                'id_model' => 2950,
                'id_fuel' => 2,
            ),
            102 => 
            array (
                'id_reg' => 3603,
                'id_model' => 2951,
                'id_fuel' => 2,
            ),
            103 => 
            array (
                'id_reg' => 3604,
                'id_model' => 2952,
                'id_fuel' => 4,
            ),
            104 => 
            array (
                'id_reg' => 3605,
                'id_model' => 2953,
                'id_fuel' => 2,
            ),
            105 => 
            array (
                'id_reg' => 3606,
                'id_model' => 2953,
                'id_fuel' => 4,
            ),
            106 => 
            array (
                'id_reg' => 3607,
                'id_model' => 2954,
                'id_fuel' => 4,
            ),
            107 => 
            array (
                'id_reg' => 3608,
                'id_model' => 2954,
                'id_fuel' => 5,
            ),
            108 => 
            array (
                'id_reg' => 3609,
                'id_model' => 2955,
                'id_fuel' => 2,
            ),
            109 => 
            array (
                'id_reg' => 3610,
                'id_model' => 2955,
                'id_fuel' => 4,
            ),
            110 => 
            array (
                'id_reg' => 3611,
                'id_model' => 2956,
                'id_fuel' => 2,
            ),
            111 => 
            array (
                'id_reg' => 3612,
                'id_model' => 2957,
                'id_fuel' => 2,
            ),
            112 => 
            array (
                'id_reg' => 3613,
                'id_model' => 2957,
                'id_fuel' => 4,
            ),
            113 => 
            array (
                'id_reg' => 3614,
                'id_model' => 2958,
                'id_fuel' => 2,
            ),
            114 => 
            array (
                'id_reg' => 3615,
                'id_model' => 2958,
                'id_fuel' => 4,
            ),
            115 => 
            array (
                'id_reg' => 3616,
                'id_model' => 2959,
                'id_fuel' => 2,
            ),
            116 => 
            array (
                'id_reg' => 3617,
                'id_model' => 2959,
                'id_fuel' => 4,
            ),
            117 => 
            array (
                'id_reg' => 3618,
                'id_model' => 2960,
                'id_fuel' => 2,
            ),
            118 => 
            array (
                'id_reg' => 3619,
                'id_model' => 2960,
                'id_fuel' => 4,
            ),
            119 => 
            array (
                'id_reg' => 3620,
                'id_model' => 2961,
                'id_fuel' => 2,
            ),
            120 => 
            array (
                'id_reg' => 3621,
                'id_model' => 2962,
                'id_fuel' => 4,
            ),
            121 => 
            array (
                'id_reg' => 3622,
                'id_model' => 2963,
                'id_fuel' => 2,
            ),
            122 => 
            array (
                'id_reg' => 3623,
                'id_model' => 2964,
                'id_fuel' => 4,
            ),
            123 => 
            array (
                'id_reg' => 3624,
                'id_model' => 2965,
                'id_fuel' => 4,
            ),
            124 => 
            array (
                'id_reg' => 3625,
                'id_model' => 2966,
                'id_fuel' => 4,
            ),
            125 => 
            array (
                'id_reg' => 3626,
                'id_model' => 2967,
                'id_fuel' => 4,
            ),
            126 => 
            array (
                'id_reg' => 3627,
                'id_model' => 2968,
                'id_fuel' => 2,
            ),
            127 => 
            array (
                'id_reg' => 3628,
                'id_model' => 2968,
                'id_fuel' => 4,
            ),
            128 => 
            array (
                'id_reg' => 3629,
                'id_model' => 2969,
                'id_fuel' => 2,
            ),
            129 => 
            array (
                'id_reg' => 3630,
                'id_model' => 2969,
                'id_fuel' => 4,
            ),
            130 => 
            array (
                'id_reg' => 3631,
                'id_model' => 2970,
                'id_fuel' => 5,
            ),
            131 => 
            array (
                'id_reg' => 3632,
                'id_model' => 2971,
                'id_fuel' => 2,
            ),
            132 => 
            array (
                'id_reg' => 3633,
                'id_model' => 2972,
                'id_fuel' => 2,
            ),
            133 => 
            array (
                'id_reg' => 3634,
                'id_model' => 2973,
                'id_fuel' => 4,
            ),
            134 => 
            array (
                'id_reg' => 3635,
                'id_model' => 2974,
                'id_fuel' => 2,
            ),
            135 => 
            array (
                'id_reg' => 3636,
                'id_model' => 2975,
                'id_fuel' => 4,
            ),
            136 => 
            array (
                'id_reg' => 3637,
                'id_model' => 2976,
                'id_fuel' => 4,
            ),
            137 => 
            array (
                'id_reg' => 3638,
                'id_model' => 2977,
                'id_fuel' => 4,
            ),
            138 => 
            array (
                'id_reg' => 3639,
                'id_model' => 2978,
                'id_fuel' => 4,
            ),
            139 => 
            array (
                'id_reg' => 3640,
                'id_model' => 2979,
                'id_fuel' => 2,
            ),
            140 => 
            array (
                'id_reg' => 3641,
                'id_model' => 2979,
                'id_fuel' => 4,
            ),
            141 => 
            array (
                'id_reg' => 3642,
                'id_model' => 2980,
                'id_fuel' => 4,
            ),
            142 => 
            array (
                'id_reg' => 3643,
                'id_model' => 2981,
                'id_fuel' => 2,
            ),
            143 => 
            array (
                'id_reg' => 3644,
                'id_model' => 2981,
                'id_fuel' => 4,
            ),
            144 => 
            array (
                'id_reg' => 3645,
                'id_model' => 2982,
                'id_fuel' => 4,
            ),
            145 => 
            array (
                'id_reg' => 3646,
                'id_model' => 2983,
                'id_fuel' => 4,
            ),
            146 => 
            array (
                'id_reg' => 3647,
                'id_model' => 2984,
                'id_fuel' => 4,
            ),
            147 => 
            array (
                'id_reg' => 3648,
                'id_model' => 2985,
                'id_fuel' => 2,
            ),
            148 => 
            array (
                'id_reg' => 3649,
                'id_model' => 2986,
                'id_fuel' => 4,
            ),
            149 => 
            array (
                'id_reg' => 3650,
                'id_model' => 2987,
                'id_fuel' => 4,
            ),
            150 => 
            array (
                'id_reg' => 3651,
                'id_model' => 2988,
                'id_fuel' => 2,
            ),
            151 => 
            array (
                'id_reg' => 3652,
                'id_model' => 2988,
                'id_fuel' => 4,
            ),
            152 => 
            array (
                'id_reg' => 3653,
                'id_model' => 2989,
                'id_fuel' => 4,
            ),
            153 => 
            array (
                'id_reg' => 3654,
                'id_model' => 2990,
                'id_fuel' => 2,
            ),
            154 => 
            array (
                'id_reg' => 3655,
                'id_model' => 2990,
                'id_fuel' => 4,
            ),
            155 => 
            array (
                'id_reg' => 3656,
                'id_model' => 2991,
                'id_fuel' => 4,
            ),
            156 => 
            array (
                'id_reg' => 3657,
                'id_model' => 2992,
                'id_fuel' => 4,
            ),
            157 => 
            array (
                'id_reg' => 3658,
                'id_model' => 2993,
                'id_fuel' => 4,
            ),
            158 => 
            array (
                'id_reg' => 3659,
                'id_model' => 2994,
                'id_fuel' => 4,
            ),
            159 => 
            array (
                'id_reg' => 3660,
                'id_model' => 2995,
                'id_fuel' => 4,
            ),
            160 => 
            array (
                'id_reg' => 3661,
                'id_model' => 2996,
                'id_fuel' => 6,
            ),
            161 => 
            array (
                'id_reg' => 3662,
                'id_model' => 2997,
                'id_fuel' => 6,
            ),
            162 => 
            array (
                'id_reg' => 3663,
                'id_model' => 2998,
                'id_fuel' => 4,
            ),
            163 => 
            array (
                'id_reg' => 3664,
                'id_model' => 2999,
                'id_fuel' => 4,
            ),
            164 => 
            array (
                'id_reg' => 3665,
                'id_model' => 3000,
                'id_fuel' => 4,
            ),
            165 => 
            array (
                'id_reg' => 3666,
                'id_model' => 3001,
                'id_fuel' => 4,
            ),
            166 => 
            array (
                'id_reg' => 3667,
                'id_model' => 3002,
                'id_fuel' => 4,
            ),
            167 => 
            array (
                'id_reg' => 3668,
                'id_model' => 3003,
                'id_fuel' => 4,
            ),
            168 => 
            array (
                'id_reg' => 3669,
                'id_model' => 3004,
                'id_fuel' => 4,
            ),
            169 => 
            array (
                'id_reg' => 3670,
                'id_model' => 3005,
                'id_fuel' => 4,
            ),
            170 => 
            array (
                'id_reg' => 3671,
                'id_model' => 3006,
                'id_fuel' => 4,
            ),
            171 => 
            array (
                'id_reg' => 3672,
                'id_model' => 3007,
                'id_fuel' => 4,
            ),
            172 => 
            array (
                'id_reg' => 3673,
                'id_model' => 3008,
                'id_fuel' => 4,
            ),
            173 => 
            array (
                'id_reg' => 3674,
                'id_model' => 3009,
                'id_fuel' => 4,
            ),
            174 => 
            array (
                'id_reg' => 3675,
                'id_model' => 3010,
                'id_fuel' => 4,
            ),
            175 => 
            array (
                'id_reg' => 3676,
                'id_model' => 3011,
                'id_fuel' => 4,
            ),
            176 => 
            array (
                'id_reg' => 3677,
                'id_model' => 3012,
                'id_fuel' => 4,
            ),
            177 => 
            array (
                'id_reg' => 3678,
                'id_model' => 3013,
                'id_fuel' => 4,
            ),
            178 => 
            array (
                'id_reg' => 3679,
                'id_model' => 3014,
                'id_fuel' => 4,
            ),
            179 => 
            array (
                'id_reg' => 3680,
                'id_model' => 3015,
                'id_fuel' => 4,
            ),
            180 => 
            array (
                'id_reg' => 3681,
                'id_model' => 3016,
                'id_fuel' => 6,
            ),
            181 => 
            array (
                'id_reg' => 3682,
                'id_model' => 3017,
                'id_fuel' => 4,
            ),
            182 => 
            array (
                'id_reg' => 3683,
                'id_model' => 3018,
                'id_fuel' => 4,
            ),
            183 => 
            array (
                'id_reg' => 3684,
                'id_model' => 3019,
                'id_fuel' => 4,
            ),
            184 => 
            array (
                'id_reg' => 3685,
                'id_model' => 3020,
                'id_fuel' => 4,
            ),
            185 => 
            array (
                'id_reg' => 3686,
                'id_model' => 3021,
                'id_fuel' => 4,
            ),
            186 => 
            array (
                'id_reg' => 3687,
                'id_model' => 3022,
                'id_fuel' => 4,
            ),
            187 => 
            array (
                'id_reg' => 3688,
                'id_model' => 3023,
                'id_fuel' => 4,
            ),
            188 => 
            array (
                'id_reg' => 3689,
                'id_model' => 3024,
                'id_fuel' => 4,
            ),
            189 => 
            array (
                'id_reg' => 3690,
                'id_model' => 3025,
                'id_fuel' => 6,
            ),
            190 => 
            array (
                'id_reg' => 3691,
                'id_model' => 3026,
                'id_fuel' => 2,
            ),
            191 => 
            array (
                'id_reg' => 3692,
                'id_model' => 3026,
                'id_fuel' => 4,
            ),
            192 => 
            array (
                'id_reg' => 3693,
                'id_model' => 3027,
                'id_fuel' => 2,
            ),
            193 => 
            array (
                'id_reg' => 3694,
                'id_model' => 3027,
                'id_fuel' => 4,
            ),
            194 => 
            array (
                'id_reg' => 3695,
                'id_model' => 3028,
                'id_fuel' => 4,
            ),
            195 => 
            array (
                'id_reg' => 3696,
                'id_model' => 3029,
                'id_fuel' => 4,
            ),
            196 => 
            array (
                'id_reg' => 3697,
                'id_model' => 3030,
                'id_fuel' => 2,
            ),
            197 => 
            array (
                'id_reg' => 3698,
                'id_model' => 3030,
                'id_fuel' => 4,
            ),
            198 => 
            array (
                'id_reg' => 3699,
                'id_model' => 3031,
                'id_fuel' => 2,
            ),
            199 => 
            array (
                'id_reg' => 3700,
                'id_model' => 3031,
                'id_fuel' => 4,
            ),
            200 => 
            array (
                'id_reg' => 3701,
                'id_model' => 3032,
                'id_fuel' => 2,
            ),
            201 => 
            array (
                'id_reg' => 3702,
                'id_model' => 3033,
                'id_fuel' => 2,
            ),
            202 => 
            array (
                'id_reg' => 3703,
                'id_model' => 3034,
                'id_fuel' => 4,
            ),
            203 => 
            array (
                'id_reg' => 3704,
                'id_model' => 3035,
                'id_fuel' => 4,
            ),
            204 => 
            array (
                'id_reg' => 3705,
                'id_model' => 3036,
                'id_fuel' => 2,
            ),
            205 => 
            array (
                'id_reg' => 3706,
                'id_model' => 3037,
                'id_fuel' => 2,
            ),
            206 => 
            array (
                'id_reg' => 3707,
                'id_model' => 3037,
                'id_fuel' => 4,
            ),
            207 => 
            array (
                'id_reg' => 3708,
                'id_model' => 3038,
                'id_fuel' => 4,
            ),
            208 => 
            array (
                'id_reg' => 3709,
                'id_model' => 3039,
                'id_fuel' => 2,
            ),
            209 => 
            array (
                'id_reg' => 3710,
                'id_model' => 3039,
                'id_fuel' => 4,
            ),
            210 => 
            array (
                'id_reg' => 3711,
                'id_model' => 3040,
                'id_fuel' => 2,
            ),
            211 => 
            array (
                'id_reg' => 3712,
                'id_model' => 3040,
                'id_fuel' => 4,
            ),
            212 => 
            array (
                'id_reg' => 3713,
                'id_model' => 3041,
                'id_fuel' => 4,
            ),
            213 => 
            array (
                'id_reg' => 3714,
                'id_model' => 3042,
                'id_fuel' => 4,
            ),
            214 => 
            array (
                'id_reg' => 3715,
                'id_model' => 3043,
                'id_fuel' => 4,
            ),
            215 => 
            array (
                'id_reg' => 3716,
                'id_model' => 3044,
                'id_fuel' => 4,
            ),
            216 => 
            array (
                'id_reg' => 3717,
                'id_model' => 3045,
                'id_fuel' => 2,
            ),
            217 => 
            array (
                'id_reg' => 3718,
                'id_model' => 3046,
                'id_fuel' => 4,
            ),
            218 => 
            array (
                'id_reg' => 3719,
                'id_model' => 3047,
                'id_fuel' => 4,
            ),
            219 => 
            array (
                'id_reg' => 3720,
                'id_model' => 3048,
                'id_fuel' => 4,
            ),
            220 => 
            array (
                'id_reg' => 3721,
                'id_model' => 3049,
                'id_fuel' => 4,
            ),
            221 => 
            array (
                'id_reg' => 3722,
                'id_model' => 3050,
                'id_fuel' => 4,
            ),
            222 => 
            array (
                'id_reg' => 3723,
                'id_model' => 3051,
                'id_fuel' => 4,
            ),
            223 => 
            array (
                'id_reg' => 3724,
                'id_model' => 3052,
                'id_fuel' => 2,
            ),
            224 => 
            array (
                'id_reg' => 3725,
                'id_model' => 3053,
                'id_fuel' => 4,
            ),
            225 => 
            array (
                'id_reg' => 3726,
                'id_model' => 3054,
                'id_fuel' => 4,
            ),
            226 => 
            array (
                'id_reg' => 3727,
                'id_model' => 3055,
                'id_fuel' => 2,
            ),
            227 => 
            array (
                'id_reg' => 3728,
                'id_model' => 3055,
                'id_fuel' => 4,
            ),
            228 => 
            array (
                'id_reg' => 3729,
                'id_model' => 3056,
                'id_fuel' => 4,
            ),
            229 => 
            array (
                'id_reg' => 3730,
                'id_model' => 3057,
                'id_fuel' => 2,
            ),
            230 => 
            array (
                'id_reg' => 3731,
                'id_model' => 3058,
                'id_fuel' => 4,
            ),
            231 => 
            array (
                'id_reg' => 3732,
                'id_model' => 3059,
                'id_fuel' => 4,
            ),
            232 => 
            array (
                'id_reg' => 3733,
                'id_model' => 3060,
                'id_fuel' => 2,
            ),
            233 => 
            array (
                'id_reg' => 3734,
                'id_model' => 3061,
                'id_fuel' => 4,
            ),
            234 => 
            array (
                'id_reg' => 3735,
                'id_model' => 3062,
                'id_fuel' => 2,
            ),
            235 => 
            array (
                'id_reg' => 3736,
                'id_model' => 3062,
                'id_fuel' => 4,
            ),
            236 => 
            array (
                'id_reg' => 3737,
                'id_model' => 3063,
                'id_fuel' => 2,
            ),
            237 => 
            array (
                'id_reg' => 3738,
                'id_model' => 3064,
                'id_fuel' => 2,
            ),
            238 => 
            array (
                'id_reg' => 3739,
                'id_model' => 3064,
                'id_fuel' => 4,
            ),
            239 => 
            array (
                'id_reg' => 3740,
                'id_model' => 3065,
                'id_fuel' => 2,
            ),
            240 => 
            array (
                'id_reg' => 3741,
                'id_model' => 3066,
                'id_fuel' => 4,
            ),
            241 => 
            array (
                'id_reg' => 3742,
                'id_model' => 3067,
                'id_fuel' => 4,
            ),
            242 => 
            array (
                'id_reg' => 3743,
                'id_model' => 3068,
                'id_fuel' => 4,
            ),
            243 => 
            array (
                'id_reg' => 3744,
                'id_model' => 3069,
                'id_fuel' => 2,
            ),
            244 => 
            array (
                'id_reg' => 3745,
                'id_model' => 3070,
                'id_fuel' => 4,
            ),
            245 => 
            array (
                'id_reg' => 3746,
                'id_model' => 3071,
                'id_fuel' => 4,
            ),
            246 => 
            array (
                'id_reg' => 3747,
                'id_model' => 3072,
                'id_fuel' => 4,
            ),
            247 => 
            array (
                'id_reg' => 3748,
                'id_model' => 3073,
                'id_fuel' => 2,
            ),
            248 => 
            array (
                'id_reg' => 3749,
                'id_model' => 3074,
                'id_fuel' => 4,
            ),
            249 => 
            array (
                'id_reg' => 3750,
                'id_model' => 3075,
                'id_fuel' => 2,
            ),
            250 => 
            array (
                'id_reg' => 3751,
                'id_model' => 3075,
                'id_fuel' => 4,
            ),
            251 => 
            array (
                'id_reg' => 3752,
                'id_model' => 3076,
                'id_fuel' => 2,
            ),
            252 => 
            array (
                'id_reg' => 3753,
                'id_model' => 3077,
                'id_fuel' => 2,
            ),
            253 => 
            array (
                'id_reg' => 3754,
                'id_model' => 3077,
                'id_fuel' => 4,
            ),
            254 => 
            array (
                'id_reg' => 3755,
                'id_model' => 3078,
                'id_fuel' => 4,
            ),
            255 => 
            array (
                'id_reg' => 3756,
                'id_model' => 3079,
                'id_fuel' => 4,
            ),
            256 => 
            array (
                'id_reg' => 3757,
                'id_model' => 3080,
                'id_fuel' => 2,
            ),
            257 => 
            array (
                'id_reg' => 3758,
                'id_model' => 3080,
                'id_fuel' => 3,
            ),
            258 => 
            array (
                'id_reg' => 3759,
                'id_model' => 3080,
                'id_fuel' => 4,
            ),
            259 => 
            array (
                'id_reg' => 3760,
                'id_model' => 3081,
                'id_fuel' => 2,
            ),
            260 => 
            array (
                'id_reg' => 3761,
                'id_model' => 3081,
                'id_fuel' => 3,
            ),
            261 => 
            array (
                'id_reg' => 3762,
                'id_model' => 3081,
                'id_fuel' => 4,
            ),
            262 => 
            array (
                'id_reg' => 3763,
                'id_model' => 3082,
                'id_fuel' => 2,
            ),
            263 => 
            array (
                'id_reg' => 3764,
                'id_model' => 3082,
                'id_fuel' => 4,
            ),
            264 => 
            array (
                'id_reg' => 3765,
                'id_model' => 3083,
                'id_fuel' => 2,
            ),
            265 => 
            array (
                'id_reg' => 3766,
                'id_model' => 3083,
                'id_fuel' => 4,
            ),
            266 => 
            array (
                'id_reg' => 3767,
                'id_model' => 3084,
                'id_fuel' => 2,
            ),
            267 => 
            array (
                'id_reg' => 3768,
                'id_model' => 3084,
                'id_fuel' => 4,
            ),
            268 => 
            array (
                'id_reg' => 3769,
                'id_model' => 3085,
                'id_fuel' => 4,
            ),
            269 => 
            array (
                'id_reg' => 3770,
                'id_model' => 3086,
                'id_fuel' => 2,
            ),
            270 => 
            array (
                'id_reg' => 3771,
                'id_model' => 3086,
                'id_fuel' => 4,
            ),
            271 => 
            array (
                'id_reg' => 3772,
                'id_model' => 3087,
                'id_fuel' => 4,
            ),
            272 => 
            array (
                'id_reg' => 3773,
                'id_model' => 3088,
                'id_fuel' => 2,
            ),
            273 => 
            array (
                'id_reg' => 3774,
                'id_model' => 3089,
                'id_fuel' => 4,
            ),
            274 => 
            array (
                'id_reg' => 3775,
                'id_model' => 3090,
                'id_fuel' => 2,
            ),
            275 => 
            array (
                'id_reg' => 3776,
                'id_model' => 3090,
                'id_fuel' => 4,
            ),
            276 => 
            array (
                'id_reg' => 3777,
                'id_model' => 3091,
                'id_fuel' => 2,
            ),
            277 => 
            array (
                'id_reg' => 3778,
                'id_model' => 3091,
                'id_fuel' => 4,
            ),
            278 => 
            array (
                'id_reg' => 3779,
                'id_model' => 3092,
                'id_fuel' => 2,
            ),
            279 => 
            array (
                'id_reg' => 3780,
                'id_model' => 3093,
                'id_fuel' => 4,
            ),
            280 => 
            array (
                'id_reg' => 3781,
                'id_model' => 3094,
                'id_fuel' => 4,
            ),
            281 => 
            array (
                'id_reg' => 3782,
                'id_model' => 3095,
                'id_fuel' => 2,
            ),
            282 => 
            array (
                'id_reg' => 3783,
                'id_model' => 3095,
                'id_fuel' => 4,
            ),
            283 => 
            array (
                'id_reg' => 3784,
                'id_model' => 3096,
                'id_fuel' => 4,
            ),
            284 => 
            array (
                'id_reg' => 3785,
                'id_model' => 3097,
                'id_fuel' => 2,
            ),
            285 => 
            array (
                'id_reg' => 3786,
                'id_model' => 3098,
                'id_fuel' => 2,
            ),
            286 => 
            array (
                'id_reg' => 3787,
                'id_model' => 3098,
                'id_fuel' => 4,
            ),
            287 => 
            array (
                'id_reg' => 3788,
                'id_model' => 3099,
                'id_fuel' => 4,
            ),
            288 => 
            array (
                'id_reg' => 3789,
                'id_model' => 3100,
                'id_fuel' => 4,
            ),
            289 => 
            array (
                'id_reg' => 3790,
                'id_model' => 3101,
                'id_fuel' => 2,
            ),
            290 => 
            array (
                'id_reg' => 3791,
                'id_model' => 3102,
                'id_fuel' => 2,
            ),
            291 => 
            array (
                'id_reg' => 3792,
                'id_model' => 3102,
                'id_fuel' => 4,
            ),
            292 => 
            array (
                'id_reg' => 3793,
                'id_model' => 3103,
                'id_fuel' => 2,
            ),
            293 => 
            array (
                'id_reg' => 3794,
                'id_model' => 3103,
                'id_fuel' => 4,
            ),
            294 => 
            array (
                'id_reg' => 3795,
                'id_model' => 3104,
                'id_fuel' => 2,
            ),
            295 => 
            array (
                'id_reg' => 3796,
                'id_model' => 3104,
                'id_fuel' => 4,
            ),
            296 => 
            array (
                'id_reg' => 3797,
                'id_model' => 3105,
                'id_fuel' => 4,
            ),
            297 => 
            array (
                'id_reg' => 3798,
                'id_model' => 3106,
                'id_fuel' => 4,
            ),
            298 => 
            array (
                'id_reg' => 3799,
                'id_model' => 3107,
                'id_fuel' => 4,
            ),
            299 => 
            array (
                'id_reg' => 3800,
                'id_model' => 3108,
                'id_fuel' => 4,
            ),
            300 => 
            array (
                'id_reg' => 3801,
                'id_model' => 3109,
                'id_fuel' => 4,
            ),
            301 => 
            array (
                'id_reg' => 3802,
                'id_model' => 3110,
                'id_fuel' => 2,
            ),
            302 => 
            array (
                'id_reg' => 3803,
                'id_model' => 3111,
                'id_fuel' => 4,
            ),
            303 => 
            array (
                'id_reg' => 3804,
                'id_model' => 3112,
                'id_fuel' => 2,
            ),
            304 => 
            array (
                'id_reg' => 3805,
                'id_model' => 3112,
                'id_fuel' => 4,
            ),
            305 => 
            array (
                'id_reg' => 3806,
                'id_model' => 3113,
                'id_fuel' => 2,
            ),
            306 => 
            array (
                'id_reg' => 3807,
                'id_model' => 3114,
                'id_fuel' => 2,
            ),
            307 => 
            array (
                'id_reg' => 3808,
                'id_model' => 3115,
                'id_fuel' => 2,
            ),
            308 => 
            array (
                'id_reg' => 3809,
                'id_model' => 3115,
                'id_fuel' => 4,
            ),
            309 => 
            array (
                'id_reg' => 3810,
                'id_model' => 3116,
                'id_fuel' => 2,
            ),
            310 => 
            array (
                'id_reg' => 3811,
                'id_model' => 3117,
                'id_fuel' => 2,
            ),
            311 => 
            array (
                'id_reg' => 3812,
                'id_model' => 3117,
                'id_fuel' => 4,
            ),
            312 => 
            array (
                'id_reg' => 3813,
                'id_model' => 3118,
                'id_fuel' => 4,
            ),
            313 => 
            array (
                'id_reg' => 3814,
                'id_model' => 3119,
                'id_fuel' => 2,
            ),
            314 => 
            array (
                'id_reg' => 3815,
                'id_model' => 3119,
                'id_fuel' => 4,
            ),
            315 => 
            array (
                'id_reg' => 3816,
                'id_model' => 3120,
                'id_fuel' => 2,
            ),
            316 => 
            array (
                'id_reg' => 3817,
                'id_model' => 3120,
                'id_fuel' => 4,
            ),
            317 => 
            array (
                'id_reg' => 3818,
                'id_model' => 3121,
                'id_fuel' => 2,
            ),
            318 => 
            array (
                'id_reg' => 3819,
                'id_model' => 3121,
                'id_fuel' => 4,
            ),
            319 => 
            array (
                'id_reg' => 3820,
                'id_model' => 3122,
                'id_fuel' => 4,
            ),
            320 => 
            array (
                'id_reg' => 3821,
                'id_model' => 3123,
                'id_fuel' => 4,
            ),
            321 => 
            array (
                'id_reg' => 3822,
                'id_model' => 3124,
                'id_fuel' => 4,
            ),
            322 => 
            array (
                'id_reg' => 3823,
                'id_model' => 3125,
                'id_fuel' => 4,
            ),
            323 => 
            array (
                'id_reg' => 3824,
                'id_model' => 3126,
                'id_fuel' => 2,
            ),
            324 => 
            array (
                'id_reg' => 3825,
                'id_model' => 3126,
                'id_fuel' => 4,
            ),
            325 => 
            array (
                'id_reg' => 3826,
                'id_model' => 3127,
                'id_fuel' => 2,
            ),
            326 => 
            array (
                'id_reg' => 3827,
                'id_model' => 3127,
                'id_fuel' => 4,
            ),
            327 => 
            array (
                'id_reg' => 3828,
                'id_model' => 3128,
                'id_fuel' => 4,
            ),
            328 => 
            array (
                'id_reg' => 3829,
                'id_model' => 3129,
                'id_fuel' => 4,
            ),
            329 => 
            array (
                'id_reg' => 3830,
                'id_model' => 3130,
                'id_fuel' => 2,
            ),
            330 => 
            array (
                'id_reg' => 3831,
                'id_model' => 3130,
                'id_fuel' => 4,
            ),
            331 => 
            array (
                'id_reg' => 3832,
                'id_model' => 3131,
                'id_fuel' => 4,
            ),
            332 => 
            array (
                'id_reg' => 3833,
                'id_model' => 3132,
                'id_fuel' => 2,
            ),
            333 => 
            array (
                'id_reg' => 3834,
                'id_model' => 3132,
                'id_fuel' => 4,
            ),
            334 => 
            array (
                'id_reg' => 3835,
                'id_model' => 3133,
                'id_fuel' => 4,
            ),
            335 => 
            array (
                'id_reg' => 3836,
                'id_model' => 3134,
                'id_fuel' => 4,
            ),
            336 => 
            array (
                'id_reg' => 3837,
                'id_model' => 3135,
                'id_fuel' => 2,
            ),
            337 => 
            array (
                'id_reg' => 3838,
                'id_model' => 3136,
                'id_fuel' => 2,
            ),
            338 => 
            array (
                'id_reg' => 3839,
                'id_model' => 3136,
                'id_fuel' => 4,
            ),
            339 => 
            array (
                'id_reg' => 3840,
                'id_model' => 3137,
                'id_fuel' => 2,
            ),
            340 => 
            array (
                'id_reg' => 3841,
                'id_model' => 3138,
                'id_fuel' => 6,
            ),
            341 => 
            array (
                'id_reg' => 3842,
                'id_model' => 3138,
                'id_fuel' => 3,
            ),
            342 => 
            array (
                'id_reg' => 3843,
                'id_model' => 3138,
                'id_fuel' => 4,
            ),
            343 => 
            array (
                'id_reg' => 3844,
                'id_model' => 3139,
                'id_fuel' => 2,
            ),
            344 => 
            array (
                'id_reg' => 3845,
                'id_model' => 3139,
                'id_fuel' => 4,
            ),
            345 => 
            array (
                'id_reg' => 3846,
                'id_model' => 3140,
                'id_fuel' => 2,
            ),
            346 => 
            array (
                'id_reg' => 3847,
                'id_model' => 3140,
                'id_fuel' => 4,
            ),
            347 => 
            array (
                'id_reg' => 3848,
                'id_model' => 3141,
                'id_fuel' => 6,
            ),
            348 => 
            array (
                'id_reg' => 3849,
                'id_model' => 3142,
                'id_fuel' => 6,
            ),
            349 => 
            array (
                'id_reg' => 3850,
                'id_model' => 3143,
                'id_fuel' => 4,
            ),
            350 => 
            array (
                'id_reg' => 3851,
                'id_model' => 3144,
                'id_fuel' => 4,
            ),
            351 => 
            array (
                'id_reg' => 3852,
                'id_model' => 3145,
                'id_fuel' => 4,
            ),
            352 => 
            array (
                'id_reg' => 3853,
                'id_model' => 3146,
                'id_fuel' => 4,
            ),
            353 => 
            array (
                'id_reg' => 3854,
                'id_model' => 3147,
                'id_fuel' => 4,
            ),
            354 => 
            array (
                'id_reg' => 3855,
                'id_model' => 3148,
                'id_fuel' => 4,
            ),
            355 => 
            array (
                'id_reg' => 3856,
                'id_model' => 3149,
                'id_fuel' => 4,
            ),
            356 => 
            array (
                'id_reg' => 3857,
                'id_model' => 3150,
                'id_fuel' => 2,
            ),
            357 => 
            array (
                'id_reg' => 3858,
                'id_model' => 3150,
                'id_fuel' => 4,
            ),
            358 => 
            array (
                'id_reg' => 3859,
                'id_model' => 3151,
                'id_fuel' => 4,
            ),
            359 => 
            array (
                'id_reg' => 3860,
                'id_model' => 3152,
                'id_fuel' => 2,
            ),
            360 => 
            array (
                'id_reg' => 3861,
                'id_model' => 3152,
                'id_fuel' => 4,
            ),
            361 => 
            array (
                'id_reg' => 3862,
                'id_model' => 3153,
                'id_fuel' => 2,
            ),
            362 => 
            array (
                'id_reg' => 3863,
                'id_model' => 3153,
                'id_fuel' => 4,
            ),
            363 => 
            array (
                'id_reg' => 3864,
                'id_model' => 3154,
                'id_fuel' => 4,
            ),
            364 => 
            array (
                'id_reg' => 3865,
                'id_model' => 3155,
                'id_fuel' => 4,
            ),
            365 => 
            array (
                'id_reg' => 3866,
                'id_model' => 3156,
                'id_fuel' => 4,
            ),
            366 => 
            array (
                'id_reg' => 3867,
                'id_model' => 3157,
                'id_fuel' => 4,
            ),
            367 => 
            array (
                'id_reg' => 3868,
                'id_model' => 3158,
                'id_fuel' => 2,
            ),
            368 => 
            array (
                'id_reg' => 3869,
                'id_model' => 3158,
                'id_fuel' => 4,
            ),
            369 => 
            array (
                'id_reg' => 3870,
                'id_model' => 3159,
                'id_fuel' => 4,
            ),
            370 => 
            array (
                'id_reg' => 3871,
                'id_model' => 3160,
                'id_fuel' => 4,
            ),
            371 => 
            array (
                'id_reg' => 3872,
                'id_model' => 3161,
                'id_fuel' => 4,
            ),
            372 => 
            array (
                'id_reg' => 3873,
                'id_model' => 3162,
                'id_fuel' => 2,
            ),
            373 => 
            array (
                'id_reg' => 3874,
                'id_model' => 3162,
                'id_fuel' => 4,
            ),
            374 => 
            array (
                'id_reg' => 3875,
                'id_model' => 3163,
                'id_fuel' => 2,
            ),
            375 => 
            array (
                'id_reg' => 3876,
                'id_model' => 3163,
                'id_fuel' => 4,
            ),
            376 => 
            array (
                'id_reg' => 3877,
                'id_model' => 3164,
                'id_fuel' => 4,
            ),
            377 => 
            array (
                'id_reg' => 3878,
                'id_model' => 3165,
                'id_fuel' => 4,
            ),
            378 => 
            array (
                'id_reg' => 3879,
                'id_model' => 3166,
                'id_fuel' => 2,
            ),
            379 => 
            array (
                'id_reg' => 3880,
                'id_model' => 3166,
                'id_fuel' => 4,
            ),
            380 => 
            array (
                'id_reg' => 3881,
                'id_model' => 3167,
                'id_fuel' => 2,
            ),
            381 => 
            array (
                'id_reg' => 3882,
                'id_model' => 3167,
                'id_fuel' => 4,
            ),
            382 => 
            array (
                'id_reg' => 3883,
                'id_model' => 3168,
                'id_fuel' => 4,
            ),
            383 => 
            array (
                'id_reg' => 3884,
                'id_model' => 3169,
                'id_fuel' => 2,
            ),
            384 => 
            array (
                'id_reg' => 3885,
                'id_model' => 3169,
                'id_fuel' => 4,
            ),
            385 => 
            array (
                'id_reg' => 3886,
                'id_model' => 3170,
                'id_fuel' => 2,
            ),
            386 => 
            array (
                'id_reg' => 3887,
                'id_model' => 3170,
                'id_fuel' => 4,
            ),
            387 => 
            array (
                'id_reg' => 3888,
                'id_model' => 3171,
                'id_fuel' => 2,
            ),
            388 => 
            array (
                'id_reg' => 3889,
                'id_model' => 3171,
                'id_fuel' => 4,
            ),
            389 => 
            array (
                'id_reg' => 3890,
                'id_model' => 3172,
                'id_fuel' => 2,
            ),
            390 => 
            array (
                'id_reg' => 3891,
                'id_model' => 3172,
                'id_fuel' => 4,
            ),
            391 => 
            array (
                'id_reg' => 3892,
                'id_model' => 3173,
                'id_fuel' => 4,
            ),
            392 => 
            array (
                'id_reg' => 3893,
                'id_model' => 3174,
                'id_fuel' => 2,
            ),
            393 => 
            array (
                'id_reg' => 3894,
                'id_model' => 3174,
                'id_fuel' => 4,
            ),
            394 => 
            array (
                'id_reg' => 3895,
                'id_model' => 3175,
                'id_fuel' => 4,
            ),
            395 => 
            array (
                'id_reg' => 3896,
                'id_model' => 3176,
                'id_fuel' => 1,
            ),
            396 => 
            array (
                'id_reg' => 3897,
                'id_model' => 3176,
                'id_fuel' => 2,
            ),
            397 => 
            array (
                'id_reg' => 3898,
                'id_model' => 3176,
                'id_fuel' => 4,
            ),
            398 => 
            array (
                'id_reg' => 3899,
                'id_model' => 3177,
                'id_fuel' => 4,
            ),
            399 => 
            array (
                'id_reg' => 3900,
                'id_model' => 3178,
                'id_fuel' => 2,
            ),
            400 => 
            array (
                'id_reg' => 3901,
                'id_model' => 3178,
                'id_fuel' => 4,
            ),
            401 => 
            array (
                'id_reg' => 3902,
                'id_model' => 3179,
                'id_fuel' => 4,
            ),
            402 => 
            array (
                'id_reg' => 3903,
                'id_model' => 3180,
                'id_fuel' => 4,
            ),
            403 => 
            array (
                'id_reg' => 3904,
                'id_model' => 3181,
                'id_fuel' => 4,
            ),
            404 => 
            array (
                'id_reg' => 3905,
                'id_model' => 3182,
                'id_fuel' => 4,
            ),
            405 => 
            array (
                'id_reg' => 3906,
                'id_model' => 3183,
                'id_fuel' => 4,
            ),
            406 => 
            array (
                'id_reg' => 3907,
                'id_model' => 3184,
                'id_fuel' => 4,
            ),
            407 => 
            array (
                'id_reg' => 3908,
                'id_model' => 3185,
                'id_fuel' => 1,
            ),
            408 => 
            array (
                'id_reg' => 3909,
                'id_model' => 3185,
                'id_fuel' => 2,
            ),
            409 => 
            array (
                'id_reg' => 3910,
                'id_model' => 3185,
                'id_fuel' => 4,
            ),
            410 => 
            array (
                'id_reg' => 3911,
                'id_model' => 3186,
                'id_fuel' => 1,
            ),
            411 => 
            array (
                'id_reg' => 3912,
                'id_model' => 3186,
                'id_fuel' => 2,
            ),
            412 => 
            array (
                'id_reg' => 3913,
                'id_model' => 3186,
                'id_fuel' => 3,
            ),
            413 => 
            array (
                'id_reg' => 3914,
                'id_model' => 3186,
                'id_fuel' => 4,
            ),
            414 => 
            array (
                'id_reg' => 3915,
                'id_model' => 3187,
                'id_fuel' => 2,
            ),
            415 => 
            array (
                'id_reg' => 3916,
                'id_model' => 3187,
                'id_fuel' => 4,
            ),
            416 => 
            array (
                'id_reg' => 3917,
                'id_model' => 3188,
                'id_fuel' => 2,
            ),
            417 => 
            array (
                'id_reg' => 3918,
                'id_model' => 3188,
                'id_fuel' => 4,
            ),
            418 => 
            array (
                'id_reg' => 3919,
                'id_model' => 3189,
                'id_fuel' => 1,
            ),
            419 => 
            array (
                'id_reg' => 3920,
                'id_model' => 3189,
                'id_fuel' => 2,
            ),
            420 => 
            array (
                'id_reg' => 3921,
                'id_model' => 3189,
                'id_fuel' => 3,
            ),
            421 => 
            array (
                'id_reg' => 3922,
                'id_model' => 3189,
                'id_fuel' => 4,
            ),
            422 => 
            array (
                'id_reg' => 3923,
                'id_model' => 3190,
                'id_fuel' => 2,
            ),
            423 => 
            array (
                'id_reg' => 3924,
                'id_model' => 3190,
                'id_fuel' => 4,
            ),
            424 => 
            array (
                'id_reg' => 3925,
                'id_model' => 3190,
                'id_fuel' => 5,
            ),
            425 => 
            array (
                'id_reg' => 3926,
                'id_model' => 3191,
                'id_fuel' => 2,
            ),
            426 => 
            array (
                'id_reg' => 3927,
                'id_model' => 3191,
                'id_fuel' => 4,
            ),
            427 => 
            array (
                'id_reg' => 3928,
                'id_model' => 3192,
                'id_fuel' => 2,
            ),
            428 => 
            array (
                'id_reg' => 3929,
                'id_model' => 3192,
                'id_fuel' => 4,
            ),
            429 => 
            array (
                'id_reg' => 3930,
                'id_model' => 3193,
                'id_fuel' => 1,
            ),
            430 => 
            array (
                'id_reg' => 3931,
                'id_model' => 3193,
                'id_fuel' => 2,
            ),
            431 => 
            array (
                'id_reg' => 3932,
                'id_model' => 3193,
                'id_fuel' => 4,
            ),
            432 => 
            array (
                'id_reg' => 3933,
                'id_model' => 3194,
                'id_fuel' => 1,
            ),
            433 => 
            array (
                'id_reg' => 3934,
                'id_model' => 3194,
                'id_fuel' => 2,
            ),
            434 => 
            array (
                'id_reg' => 3935,
                'id_model' => 3194,
                'id_fuel' => 4,
            ),
            435 => 
            array (
                'id_reg' => 3936,
                'id_model' => 3195,
                'id_fuel' => 2,
            ),
            436 => 
            array (
                'id_reg' => 3937,
                'id_model' => 3195,
                'id_fuel' => 4,
            ),
            437 => 
            array (
                'id_reg' => 3938,
                'id_model' => 3196,
                'id_fuel' => 1,
            ),
            438 => 
            array (
                'id_reg' => 3939,
                'id_model' => 3196,
                'id_fuel' => 2,
            ),
            439 => 
            array (
                'id_reg' => 3940,
                'id_model' => 3196,
                'id_fuel' => 3,
            ),
            440 => 
            array (
                'id_reg' => 3941,
                'id_model' => 3196,
                'id_fuel' => 4,
            ),
            441 => 
            array (
                'id_reg' => 3942,
                'id_model' => 3197,
                'id_fuel' => 4,
            ),
            442 => 
            array (
                'id_reg' => 3943,
                'id_model' => 3198,
                'id_fuel' => 2,
            ),
            443 => 
            array (
                'id_reg' => 3944,
                'id_model' => 3198,
                'id_fuel' => 4,
            ),
            444 => 
            array (
                'id_reg' => 3945,
                'id_model' => 3198,
                'id_fuel' => 5,
            ),
            445 => 
            array (
                'id_reg' => 3946,
                'id_model' => 3199,
                'id_fuel' => 2,
            ),
            446 => 
            array (
                'id_reg' => 3947,
                'id_model' => 3199,
                'id_fuel' => 4,
            ),
            447 => 
            array (
                'id_reg' => 3948,
                'id_model' => 3200,
                'id_fuel' => 2,
            ),
            448 => 
            array (
                'id_reg' => 3949,
                'id_model' => 3200,
                'id_fuel' => 4,
            ),
            449 => 
            array (
                'id_reg' => 3950,
                'id_model' => 3201,
                'id_fuel' => 2,
            ),
            450 => 
            array (
                'id_reg' => 3951,
                'id_model' => 3201,
                'id_fuel' => 4,
            ),
            451 => 
            array (
                'id_reg' => 3952,
                'id_model' => 3202,
                'id_fuel' => 2,
            ),
            452 => 
            array (
                'id_reg' => 3953,
                'id_model' => 3202,
                'id_fuel' => 4,
            ),
            453 => 
            array (
                'id_reg' => 3954,
                'id_model' => 3202,
                'id_fuel' => 5,
            ),
            454 => 
            array (
                'id_reg' => 3955,
                'id_model' => 3203,
                'id_fuel' => 4,
            ),
            455 => 
            array (
                'id_reg' => 3956,
                'id_model' => 3204,
                'id_fuel' => 4,
            ),
            456 => 
            array (
                'id_reg' => 3957,
                'id_model' => 3205,
                'id_fuel' => 4,
            ),
            457 => 
            array (
                'id_reg' => 3958,
                'id_model' => 3206,
                'id_fuel' => 4,
            ),
            458 => 
            array (
                'id_reg' => 3959,
                'id_model' => 3207,
                'id_fuel' => 4,
            ),
            459 => 
            array (
                'id_reg' => 3960,
                'id_model' => 3208,
                'id_fuel' => 4,
            ),
            460 => 
            array (
                'id_reg' => 3961,
                'id_model' => 3209,
                'id_fuel' => 4,
            ),
            461 => 
            array (
                'id_reg' => 3962,
                'id_model' => 3210,
                'id_fuel' => 4,
            ),
            462 => 
            array (
                'id_reg' => 3963,
                'id_model' => 3211,
                'id_fuel' => 4,
            ),
            463 => 
            array (
                'id_reg' => 3964,
                'id_model' => 3212,
                'id_fuel' => 4,
            ),
            464 => 
            array (
                'id_reg' => 3965,
                'id_model' => 3213,
                'id_fuel' => 4,
            ),
            465 => 
            array (
                'id_reg' => 3966,
                'id_model' => 3214,
                'id_fuel' => 4,
            ),
            466 => 
            array (
                'id_reg' => 3967,
                'id_model' => 3215,
                'id_fuel' => 4,
            ),
            467 => 
            array (
                'id_reg' => 3968,
                'id_model' => 3216,
                'id_fuel' => 4,
            ),
            468 => 
            array (
                'id_reg' => 3969,
                'id_model' => 3217,
                'id_fuel' => 4,
            ),
            469 => 
            array (
                'id_reg' => 3970,
                'id_model' => 3218,
                'id_fuel' => 4,
            ),
            470 => 
            array (
                'id_reg' => 3971,
                'id_model' => 3219,
                'id_fuel' => 2,
            ),
            471 => 
            array (
                'id_reg' => 3972,
                'id_model' => 3219,
                'id_fuel' => 4,
            ),
            472 => 
            array (
                'id_reg' => 3973,
                'id_model' => 3220,
                'id_fuel' => 4,
            ),
            473 => 
            array (
                'id_reg' => 3974,
                'id_model' => 3221,
                'id_fuel' => 4,
            ),
            474 => 
            array (
                'id_reg' => 3975,
                'id_model' => 3222,
                'id_fuel' => 4,
            ),
            475 => 
            array (
                'id_reg' => 3976,
                'id_model' => 3223,
                'id_fuel' => 4,
            ),
            476 => 
            array (
                'id_reg' => 3977,
                'id_model' => 3224,
                'id_fuel' => 4,
            ),
            477 => 
            array (
                'id_reg' => 3978,
                'id_model' => 3225,
                'id_fuel' => 4,
            ),
            478 => 
            array (
                'id_reg' => 3979,
                'id_model' => 3226,
                'id_fuel' => 4,
            ),
            479 => 
            array (
                'id_reg' => 3980,
                'id_model' => 3227,
                'id_fuel' => 4,
            ),
            480 => 
            array (
                'id_reg' => 3981,
                'id_model' => 3228,
                'id_fuel' => 6,
            ),
            481 => 
            array (
                'id_reg' => 3982,
                'id_model' => 3229,
                'id_fuel' => 4,
            ),
            482 => 
            array (
                'id_reg' => 3983,
                'id_model' => 3230,
                'id_fuel' => 6,
            ),
            483 => 
            array (
                'id_reg' => 3984,
                'id_model' => 3230,
                'id_fuel' => 3,
            ),
            484 => 
            array (
                'id_reg' => 3985,
                'id_model' => 3230,
                'id_fuel' => 4,
            ),
            485 => 
            array (
                'id_reg' => 3986,
                'id_model' => 3231,
                'id_fuel' => 4,
            ),
            486 => 
            array (
                'id_reg' => 3987,
                'id_model' => 3232,
                'id_fuel' => 4,
            ),
            487 => 
            array (
                'id_reg' => 3988,
                'id_model' => 3233,
                'id_fuel' => 4,
            ),
            488 => 
            array (
                'id_reg' => 3989,
                'id_model' => 3234,
                'id_fuel' => 4,
            ),
            489 => 
            array (
                'id_reg' => 3990,
                'id_model' => 3235,
                'id_fuel' => 4,
            ),
            490 => 
            array (
                'id_reg' => 3991,
                'id_model' => 3236,
                'id_fuel' => 4,
            ),
            491 => 
            array (
                'id_reg' => 3992,
                'id_model' => 3237,
                'id_fuel' => 4,
            ),
            492 => 
            array (
                'id_reg' => 3993,
                'id_model' => 3238,
                'id_fuel' => 4,
            ),
            493 => 
            array (
                'id_reg' => 3994,
                'id_model' => 3239,
                'id_fuel' => 4,
            ),
            494 => 
            array (
                'id_reg' => 3995,
                'id_model' => 3240,
                'id_fuel' => 4,
            ),
            495 => 
            array (
                'id_reg' => 3996,
                'id_model' => 3241,
                'id_fuel' => 4,
            ),
            496 => 
            array (
                'id_reg' => 3997,
                'id_model' => 3242,
                'id_fuel' => 4,
            ),
            497 => 
            array (
                'id_reg' => 3998,
                'id_model' => 3243,
                'id_fuel' => 4,
            ),
            498 => 
            array (
                'id_reg' => 3999,
                'id_model' => 3244,
                'id_fuel' => 4,
            ),
            499 => 
            array (
                'id_reg' => 4000,
                'id_model' => 3245,
                'id_fuel' => 4,
            ),
        ));
        \DB::table('model_rel_fuel')->insert(array (
            0 => 
            array (
                'id_reg' => 4001,
                'id_model' => 3246,
                'id_fuel' => 4,
            ),
            1 => 
            array (
                'id_reg' => 4002,
                'id_model' => 3247,
                'id_fuel' => 4,
            ),
            2 => 
            array (
                'id_reg' => 4003,
                'id_model' => 3248,
                'id_fuel' => 4,
            ),
            3 => 
            array (
                'id_reg' => 4004,
                'id_model' => 3249,
                'id_fuel' => 4,
            ),
            4 => 
            array (
                'id_reg' => 4005,
                'id_model' => 3250,
                'id_fuel' => 4,
            ),
            5 => 
            array (
                'id_reg' => 4006,
                'id_model' => 3251,
                'id_fuel' => 4,
            ),
        ));
        
        
    }
}