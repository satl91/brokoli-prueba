<?php

use Illuminate\Database\Seeder;

class ModelFuelNumdoorsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('model_fuel_numdoors')->delete();
        
        \DB::table('model_fuel_numdoors')->insert(array (
            0 => 
            array (
                'id_reg' => 1,
                'id_regmf' => 1,
                'doors' => 2,
            ),
            1 => 
            array (
                'id_reg' => 2,
                'id_regmf' => 2,
                'doors' => 3,
            ),
            2 => 
            array (
                'id_reg' => 3,
                'id_regmf' => 3,
                'doors' => 2,
            ),
            3 => 
            array (
                'id_reg' => 4,
                'id_regmf' => 4,
                'doors' => 3,
            ),
            4 => 
            array (
                'id_reg' => 5,
                'id_regmf' => 5,
                'doors' => 2,
            ),
            5 => 
            array (
                'id_reg' => 6,
                'id_regmf' => 6,
                'doors' => 3,
            ),
            6 => 
            array (
                'id_reg' => 7,
                'id_regmf' => 7,
                'doors' => 3,
            ),
            7 => 
            array (
                'id_reg' => 8,
                'id_regmf' => 8,
                'doors' => 3,
            ),
            8 => 
            array (
                'id_reg' => 9,
                'id_regmf' => 9,
                'doors' => 2,
            ),
            9 => 
            array (
                'id_reg' => 10,
                'id_regmf' => 10,
                'doors' => 4,
            ),
            10 => 
            array (
                'id_reg' => 11,
                'id_regmf' => 11,
                'doors' => 3,
            ),
            11 => 
            array (
                'id_reg' => 12,
                'id_regmf' => 12,
                'doors' => 3,
            ),
            12 => 
            array (
                'id_reg' => 13,
                'id_regmf' => 12,
                'doors' => 4,
            ),
            13 => 
            array (
                'id_reg' => 14,
                'id_regmf' => 13,
                'doors' => 4,
            ),
            14 => 
            array (
                'id_reg' => 15,
                'id_regmf' => 14,
                'doors' => 5,
            ),
            15 => 
            array (
                'id_reg' => 16,
                'id_regmf' => 15,
                'doors' => 2,
            ),
            16 => 
            array (
                'id_reg' => 17,
                'id_regmf' => 16,
                'doors' => 4,
            ),
            17 => 
            array (
                'id_reg' => 18,
                'id_regmf' => 17,
                'doors' => 4,
            ),
            18 => 
            array (
                'id_reg' => 19,
                'id_regmf' => 18,
                'doors' => 5,
            ),
            19 => 
            array (
                'id_reg' => 20,
                'id_regmf' => 19,
                'doors' => 2,
            ),
            20 => 
            array (
                'id_reg' => 21,
                'id_regmf' => 20,
                'doors' => 0,
            ),
            21 => 
            array (
                'id_reg' => 22,
                'id_regmf' => 21,
                'doors' => 0,
            ),
            22 => 
            array (
                'id_reg' => 23,
                'id_regmf' => 22,
                'doors' => 3,
            ),
            23 => 
            array (
                'id_reg' => 24,
                'id_regmf' => 23,
                'doors' => 2,
            ),
            24 => 
            array (
                'id_reg' => 25,
                'id_regmf' => 23,
                'doors' => 3,
            ),
            25 => 
            array (
                'id_reg' => 26,
                'id_regmf' => 24,
                'doors' => 3,
            ),
            26 => 
            array (
                'id_reg' => 27,
                'id_regmf' => 25,
                'doors' => 3,
            ),
            27 => 
            array (
                'id_reg' => 28,
                'id_regmf' => 26,
                'doors' => 3,
            ),
            28 => 
            array (
                'id_reg' => 29,
                'id_regmf' => 27,
                'doors' => 3,
            ),
            29 => 
            array (
                'id_reg' => 30,
                'id_regmf' => 28,
                'doors' => 3,
            ),
            30 => 
            array (
                'id_reg' => 31,
                'id_regmf' => 29,
                'doors' => 3,
            ),
            31 => 
            array (
                'id_reg' => 32,
                'id_regmf' => 30,
                'doors' => 3,
            ),
            32 => 
            array (
                'id_reg' => 33,
                'id_regmf' => 31,
                'doors' => 3,
            ),
            33 => 
            array (
                'id_reg' => 34,
                'id_regmf' => 32,
                'doors' => 3,
            ),
            34 => 
            array (
                'id_reg' => 35,
                'id_regmf' => 33,
                'doors' => 3,
            ),
            35 => 
            array (
                'id_reg' => 36,
                'id_regmf' => 34,
                'doors' => 2,
            ),
            36 => 
            array (
                'id_reg' => 37,
                'id_regmf' => 34,
                'doors' => 3,
            ),
            37 => 
            array (
                'id_reg' => 38,
                'id_regmf' => 35,
                'doors' => 3,
            ),
            38 => 
            array (
                'id_reg' => 39,
                'id_regmf' => 36,
                'doors' => 3,
            ),
            39 => 
            array (
                'id_reg' => 40,
                'id_regmf' => 37,
                'doors' => 3,
            ),
            40 => 
            array (
                'id_reg' => 41,
                'id_regmf' => 38,
                'doors' => 3,
            ),
            41 => 
            array (
                'id_reg' => 42,
                'id_regmf' => 39,
                'doors' => 0,
            ),
            42 => 
            array (
                'id_reg' => 43,
                'id_regmf' => 41,
                'doors' => 2,
            ),
            43 => 
            array (
                'id_reg' => 44,
                'id_regmf' => 40,
                'doors' => 2,
            ),
            44 => 
            array (
                'id_reg' => 45,
                'id_regmf' => 42,
                'doors' => 2,
            ),
            45 => 
            array (
                'id_reg' => 46,
                'id_regmf' => 43,
                'doors' => 2,
            ),
            46 => 
            array (
                'id_reg' => 47,
                'id_regmf' => 44,
                'doors' => 2,
            ),
            47 => 
            array (
                'id_reg' => 48,
                'id_regmf' => 45,
                'doors' => 2,
            ),
            48 => 
            array (
                'id_reg' => 49,
                'id_regmf' => 47,
                'doors' => 2,
            ),
            49 => 
            array (
                'id_reg' => 50,
                'id_regmf' => 46,
                'doors' => 2,
            ),
            50 => 
            array (
                'id_reg' => 51,
                'id_regmf' => 48,
                'doors' => 3,
            ),
            51 => 
            array (
                'id_reg' => 52,
                'id_regmf' => 49,
                'doors' => 3,
            ),
            52 => 
            array (
                'id_reg' => 53,
                'id_regmf' => 50,
                'doors' => 5,
            ),
            53 => 
            array (
                'id_reg' => 54,
                'id_regmf' => 51,
                'doors' => 5,
            ),
            54 => 
            array (
                'id_reg' => 55,
                'id_regmf' => 52,
                'doors' => 5,
            ),
            55 => 
            array (
                'id_reg' => 56,
                'id_regmf' => 61,
                'doors' => 4,
            ),
            56 => 
            array (
                'id_reg' => 57,
                'id_regmf' => 60,
                'doors' => 4,
            ),
            57 => 
            array (
                'id_reg' => 58,
                'id_regmf' => 63,
                'doors' => 4,
            ),
            58 => 
            array (
                'id_reg' => 59,
                'id_regmf' => 62,
                'doors' => 4,
            ),
            59 => 
            array (
                'id_reg' => 60,
                'id_regmf' => 64,
                'doors' => 2,
            ),
            60 => 
            array (
                'id_reg' => 61,
                'id_regmf' => 65,
                'doors' => 4,
            ),
            61 => 
            array (
                'id_reg' => 62,
                'id_regmf' => 66,
                'doors' => 2,
            ),
            62 => 
            array (
                'id_reg' => 63,
                'id_regmf' => 55,
                'doors' => 5,
            ),
            63 => 
            array (
                'id_reg' => 64,
                'id_regmf' => 53,
                'doors' => 2,
            ),
            64 => 
            array (
                'id_reg' => 65,
                'id_regmf' => 57,
                'doors' => 4,
            ),
            65 => 
            array (
                'id_reg' => 66,
                'id_regmf' => 56,
                'doors' => 4,
            ),
            66 => 
            array (
                'id_reg' => 67,
                'id_regmf' => 54,
                'doors' => 2,
            ),
            67 => 
            array (
                'id_reg' => 68,
                'id_regmf' => 58,
                'doors' => 4,
            ),
            68 => 
            array (
                'id_reg' => 69,
                'id_regmf' => 59,
                'doors' => 4,
            ),
            69 => 
            array (
                'id_reg' => 70,
                'id_regmf' => 67,
                'doors' => 4,
            ),
            70 => 
            array (
                'id_reg' => 71,
                'id_regmf' => 68,
                'doors' => 3,
            ),
            71 => 
            array (
                'id_reg' => 72,
                'id_regmf' => 68,
                'doors' => 5,
            ),
            72 => 
            array (
                'id_reg' => 73,
                'id_regmf' => 69,
                'doors' => 4,
            ),
            73 => 
            array (
                'id_reg' => 74,
                'id_regmf' => 70,
                'doors' => 2,
            ),
            74 => 
            array (
                'id_reg' => 75,
                'id_regmf' => 70,
                'doors' => 4,
            ),
            75 => 
            array (
                'id_reg' => 76,
                'id_regmf' => 71,
                'doors' => 5,
            ),
            76 => 
            array (
                'id_reg' => 77,
                'id_regmf' => 72,
                'doors' => 5,
            ),
            77 => 
            array (
                'id_reg' => 78,
                'id_regmf' => 73,
                'doors' => 4,
            ),
            78 => 
            array (
                'id_reg' => 79,
                'id_regmf' => 75,
                'doors' => 2,
            ),
            79 => 
            array (
                'id_reg' => 80,
                'id_regmf' => 75,
                'doors' => 3,
            ),
            80 => 
            array (
                'id_reg' => 81,
                'id_regmf' => 75,
                'doors' => 4,
            ),
            81 => 
            array (
                'id_reg' => 82,
                'id_regmf' => 74,
                'doors' => 4,
            ),
            82 => 
            array (
                'id_reg' => 83,
                'id_regmf' => 76,
                'doors' => 2,
            ),
            83 => 
            array (
                'id_reg' => 84,
                'id_regmf' => 77,
                'doors' => 2,
            ),
            84 => 
            array (
                'id_reg' => 85,
                'id_regmf' => 78,
                'doors' => 0,
            ),
            85 => 
            array (
                'id_reg' => 86,
                'id_regmf' => 79,
                'doors' => 2,
            ),
            86 => 
            array (
                'id_reg' => 87,
                'id_regmf' => 80,
                'doors' => 2,
            ),
            87 => 
            array (
                'id_reg' => 88,
                'id_regmf' => 81,
                'doors' => 2,
            ),
            88 => 
            array (
                'id_reg' => 89,
                'id_regmf' => 81,
                'doors' => 3,
            ),
            89 => 
            array (
                'id_reg' => 90,
                'id_regmf' => 82,
                'doors' => 2,
            ),
            90 => 
            array (
                'id_reg' => 91,
                'id_regmf' => 83,
                'doors' => 2,
            ),
            91 => 
            array (
                'id_reg' => 92,
                'id_regmf' => 84,
                'doors' => 2,
            ),
            92 => 
            array (
                'id_reg' => 93,
                'id_regmf' => 85,
                'doors' => 2,
            ),
            93 => 
            array (
                'id_reg' => 94,
                'id_regmf' => 86,
                'doors' => 3,
            ),
            94 => 
            array (
                'id_reg' => 95,
                'id_regmf' => 87,
                'doors' => 2,
            ),
            95 => 
            array (
                'id_reg' => 96,
                'id_regmf' => 88,
                'doors' => 2,
            ),
            96 => 
            array (
                'id_reg' => 97,
                'id_regmf' => 89,
                'doors' => 3,
            ),
            97 => 
            array (
                'id_reg' => 98,
                'id_regmf' => 90,
                'doors' => 2,
            ),
            98 => 
            array (
                'id_reg' => 99,
                'id_regmf' => 91,
                'doors' => 0,
            ),
            99 => 
            array (
                'id_reg' => 100,
                'id_regmf' => 92,
                'doors' => 2,
            ),
            100 => 
            array (
                'id_reg' => 101,
                'id_regmf' => 93,
                'doors' => 4,
            ),
            101 => 
            array (
                'id_reg' => 102,
                'id_regmf' => 94,
                'doors' => 2,
            ),
            102 => 
            array (
                'id_reg' => 103,
                'id_regmf' => 95,
                'doors' => 0,
            ),
            103 => 
            array (
                'id_reg' => 104,
                'id_regmf' => 96,
                'doors' => 0,
            ),
            104 => 
            array (
                'id_reg' => 105,
                'id_regmf' => 98,
                'doors' => 0,
            ),
            105 => 
            array (
                'id_reg' => 106,
                'id_regmf' => 97,
                'doors' => 0,
            ),
            106 => 
            array (
                'id_reg' => 107,
                'id_regmf' => 99,
                'doors' => 0,
            ),
            107 => 
            array (
                'id_reg' => 108,
                'id_regmf' => 100,
                'doors' => 0,
            ),
            108 => 
            array (
                'id_reg' => 109,
                'id_regmf' => 101,
                'doors' => 0,
            ),
            109 => 
            array (
                'id_reg' => 110,
                'id_regmf' => 102,
                'doors' => 0,
            ),
            110 => 
            array (
                'id_reg' => 111,
                'id_regmf' => 103,
                'doors' => 0,
            ),
            111 => 
            array (
                'id_reg' => 112,
                'id_regmf' => 104,
                'doors' => 0,
            ),
            112 => 
            array (
                'id_reg' => 113,
                'id_regmf' => 105,
                'doors' => 0,
            ),
            113 => 
            array (
                'id_reg' => 114,
                'id_regmf' => 106,
                'doors' => 0,
            ),
            114 => 
            array (
                'id_reg' => 115,
                'id_regmf' => 107,
                'doors' => 4,
            ),
            115 => 
            array (
                'id_reg' => 116,
                'id_regmf' => 108,
                'doors' => 3,
            ),
            116 => 
            array (
                'id_reg' => 117,
                'id_regmf' => 109,
                'doors' => 3,
            ),
            117 => 
            array (
                'id_reg' => 118,
                'id_regmf' => 110,
                'doors' => 5,
            ),
            118 => 
            array (
                'id_reg' => 119,
                'id_regmf' => 111,
                'doors' => 3,
            ),
            119 => 
            array (
                'id_reg' => 120,
                'id_regmf' => 112,
                'doors' => 3,
            ),
            120 => 
            array (
                'id_reg' => 121,
                'id_regmf' => 113,
                'doors' => 3,
            ),
            121 => 
            array (
                'id_reg' => 122,
                'id_regmf' => 114,
                'doors' => 3,
            ),
            122 => 
            array (
                'id_reg' => 123,
                'id_regmf' => 115,
                'doors' => 2,
            ),
            123 => 
            array (
                'id_reg' => 124,
                'id_regmf' => 116,
                'doors' => 5,
            ),
            124 => 
            array (
                'id_reg' => 125,
                'id_regmf' => 117,
                'doors' => 3,
            ),
            125 => 
            array (
                'id_reg' => 126,
                'id_regmf' => 118,
                'doors' => 3,
            ),
            126 => 
            array (
                'id_reg' => 127,
                'id_regmf' => 120,
                'doors' => 3,
            ),
            127 => 
            array (
                'id_reg' => 128,
                'id_regmf' => 119,
                'doors' => 3,
            ),
            128 => 
            array (
                'id_reg' => 129,
                'id_regmf' => 121,
                'doors' => 4,
            ),
            129 => 
            array (
                'id_reg' => 130,
                'id_regmf' => 122,
                'doors' => 3,
            ),
            130 => 
            array (
                'id_reg' => 131,
                'id_regmf' => 123,
                'doors' => 2,
            ),
            131 => 
            array (
                'id_reg' => 132,
                'id_regmf' => 124,
                'doors' => 2,
            ),
            132 => 
            array (
                'id_reg' => 133,
                'id_regmf' => 125,
                'doors' => 2,
            ),
            133 => 
            array (
                'id_reg' => 134,
                'id_regmf' => 126,
                'doors' => 2,
            ),
            134 => 
            array (
                'id_reg' => 135,
                'id_regmf' => 127,
                'doors' => 2,
            ),
            135 => 
            array (
                'id_reg' => 136,
                'id_regmf' => 128,
                'doors' => 2,
            ),
            136 => 
            array (
                'id_reg' => 137,
                'id_regmf' => 129,
                'doors' => 2,
            ),
            137 => 
            array (
                'id_reg' => 138,
                'id_regmf' => 130,
                'doors' => 4,
            ),
            138 => 
            array (
                'id_reg' => 139,
                'id_regmf' => 131,
                'doors' => 2,
            ),
            139 => 
            array (
                'id_reg' => 140,
                'id_regmf' => 132,
                'doors' => 2,
            ),
            140 => 
            array (
                'id_reg' => 141,
                'id_regmf' => 132,
                'doors' => 5,
            ),
            141 => 
            array (
                'id_reg' => 142,
                'id_regmf' => 133,
                'doors' => 2,
            ),
            142 => 
            array (
                'id_reg' => 143,
                'id_regmf' => 133,
                'doors' => 3,
            ),
            143 => 
            array (
                'id_reg' => 144,
                'id_regmf' => 134,
                'doors' => 2,
            ),
            144 => 
            array (
                'id_reg' => 145,
                'id_regmf' => 135,
                'doors' => 2,
            ),
            145 => 
            array (
                'id_reg' => 146,
                'id_regmf' => 136,
                'doors' => 2,
            ),
            146 => 
            array (
                'id_reg' => 147,
                'id_regmf' => 136,
                'doors' => 3,
            ),
            147 => 
            array (
                'id_reg' => 148,
                'id_regmf' => 137,
                'doors' => 2,
            ),
            148 => 
            array (
                'id_reg' => 149,
                'id_regmf' => 138,
                'doors' => 2,
            ),
            149 => 
            array (
                'id_reg' => 150,
                'id_regmf' => 139,
                'doors' => 2,
            ),
            150 => 
            array (
                'id_reg' => 151,
                'id_regmf' => 140,
                'doors' => 2,
            ),
            151 => 
            array (
                'id_reg' => 152,
                'id_regmf' => 141,
                'doors' => 2,
            ),
            152 => 
            array (
                'id_reg' => 153,
                'id_regmf' => 142,
                'doors' => 2,
            ),
            153 => 
            array (
                'id_reg' => 154,
                'id_regmf' => 143,
                'doors' => 2,
            ),
            154 => 
            array (
                'id_reg' => 155,
                'id_regmf' => 144,
                'doors' => 2,
            ),
            155 => 
            array (
                'id_reg' => 156,
                'id_regmf' => 147,
                'doors' => 4,
            ),
            156 => 
            array (
                'id_reg' => 157,
                'id_regmf' => 146,
                'doors' => 5,
            ),
            157 => 
            array (
                'id_reg' => 158,
                'id_regmf' => 149,
                'doors' => 2,
            ),
            158 => 
            array (
                'id_reg' => 159,
                'id_regmf' => 149,
                'doors' => 4,
            ),
            159 => 
            array (
                'id_reg' => 160,
                'id_regmf' => 148,
                'doors' => 4,
            ),
            160 => 
            array (
                'id_reg' => 161,
                'id_regmf' => 145,
                'doors' => 4,
            ),
            161 => 
            array (
                'id_reg' => 162,
                'id_regmf' => 151,
                'doors' => 3,
            ),
            162 => 
            array (
                'id_reg' => 163,
                'id_regmf' => 150,
                'doors' => 3,
            ),
            163 => 
            array (
                'id_reg' => 164,
                'id_regmf' => 153,
                'doors' => 5,
            ),
            164 => 
            array (
                'id_reg' => 165,
                'id_regmf' => 152,
                'doors' => 5,
            ),
            165 => 
            array (
                'id_reg' => 166,
                'id_regmf' => 154,
                'doors' => 5,
            ),
            166 => 
            array (
                'id_reg' => 167,
                'id_regmf' => 155,
                'doors' => 5,
            ),
            167 => 
            array (
                'id_reg' => 168,
                'id_regmf' => 157,
                'doors' => 5,
            ),
            168 => 
            array (
                'id_reg' => 169,
                'id_regmf' => 156,
                'doors' => 5,
            ),
            169 => 
            array (
                'id_reg' => 170,
                'id_regmf' => 159,
                'doors' => 5,
            ),
            170 => 
            array (
                'id_reg' => 171,
                'id_regmf' => 158,
                'doors' => 5,
            ),
            171 => 
            array (
                'id_reg' => 172,
                'id_regmf' => 161,
                'doors' => 5,
            ),
            172 => 
            array (
                'id_reg' => 173,
                'id_regmf' => 160,
                'doors' => 5,
            ),
            173 => 
            array (
                'id_reg' => 174,
                'id_regmf' => 163,
                'doors' => 5,
            ),
            174 => 
            array (
                'id_reg' => 175,
                'id_regmf' => 162,
                'doors' => 5,
            ),
            175 => 
            array (
                'id_reg' => 176,
                'id_regmf' => 166,
                'doors' => 4,
            ),
            176 => 
            array (
                'id_reg' => 177,
                'id_regmf' => 165,
                'doors' => 4,
            ),
            177 => 
            array (
                'id_reg' => 178,
                'id_regmf' => 164,
                'doors' => 4,
            ),
            178 => 
            array (
                'id_reg' => 179,
                'id_regmf' => 168,
                'doors' => 2,
            ),
            179 => 
            array (
                'id_reg' => 180,
                'id_regmf' => 167,
                'doors' => 2,
            ),
            180 => 
            array (
                'id_reg' => 181,
                'id_regmf' => 169,
                'doors' => 2,
            ),
            181 => 
            array (
                'id_reg' => 182,
                'id_regmf' => 169,
                'doors' => 3,
            ),
            182 => 
            array (
                'id_reg' => 183,
                'id_regmf' => 170,
                'doors' => 5,
            ),
            183 => 
            array (
                'id_reg' => 184,
                'id_regmf' => 171,
                'doors' => 5,
            ),
            184 => 
            array (
                'id_reg' => 185,
                'id_regmf' => 172,
                'doors' => 5,
            ),
            185 => 
            array (
                'id_reg' => 186,
                'id_regmf' => 173,
                'doors' => 5,
            ),
            186 => 
            array (
                'id_reg' => 187,
                'id_regmf' => 174,
                'doors' => 5,
            ),
            187 => 
            array (
                'id_reg' => 188,
                'id_regmf' => 175,
                'doors' => 5,
            ),
            188 => 
            array (
                'id_reg' => 189,
                'id_regmf' => 176,
                'doors' => 2,
            ),
            189 => 
            array (
                'id_reg' => 190,
                'id_regmf' => 177,
                'doors' => 2,
            ),
            190 => 
            array (
                'id_reg' => 191,
                'id_regmf' => 178,
                'doors' => 5,
            ),
            191 => 
            array (
                'id_reg' => 192,
                'id_regmf' => 179,
                'doors' => 5,
            ),
            192 => 
            array (
                'id_reg' => 193,
                'id_regmf' => 180,
                'doors' => 5,
            ),
            193 => 
            array (
                'id_reg' => 194,
                'id_regmf' => 181,
                'doors' => 2,
            ),
            194 => 
            array (
                'id_reg' => 195,
                'id_regmf' => 181,
                'doors' => 4,
            ),
            195 => 
            array (
                'id_reg' => 196,
                'id_regmf' => 181,
                'doors' => 5,
            ),
            196 => 
            array (
                'id_reg' => 197,
                'id_regmf' => 182,
                'doors' => 2,
            ),
            197 => 
            array (
                'id_reg' => 198,
                'id_regmf' => 183,
                'doors' => 4,
            ),
            198 => 
            array (
                'id_reg' => 199,
                'id_regmf' => 183,
                'doors' => 5,
            ),
            199 => 
            array (
                'id_reg' => 200,
                'id_regmf' => 184,
                'doors' => 5,
            ),
            200 => 
            array (
                'id_reg' => 201,
                'id_regmf' => 185,
                'doors' => 3,
            ),
            201 => 
            array (
                'id_reg' => 202,
                'id_regmf' => 186,
                'doors' => 5,
            ),
            202 => 
            array (
                'id_reg' => 203,
                'id_regmf' => 187,
                'doors' => 5,
            ),
            203 => 
            array (
                'id_reg' => 204,
                'id_regmf' => 188,
                'doors' => 5,
            ),
            204 => 
            array (
                'id_reg' => 205,
                'id_regmf' => 189,
                'doors' => 2,
            ),
            205 => 
            array (
                'id_reg' => 206,
                'id_regmf' => 189,
                'doors' => 4,
            ),
            206 => 
            array (
                'id_reg' => 207,
                'id_regmf' => 189,
                'doors' => 5,
            ),
            207 => 
            array (
                'id_reg' => 208,
                'id_regmf' => 190,
                'doors' => 2,
            ),
            208 => 
            array (
                'id_reg' => 209,
                'id_regmf' => 191,
                'doors' => 5,
            ),
            209 => 
            array (
                'id_reg' => 210,
                'id_regmf' => 192,
                'doors' => 4,
            ),
            210 => 
            array (
                'id_reg' => 211,
                'id_regmf' => 192,
                'doors' => 5,
            ),
            211 => 
            array (
                'id_reg' => 212,
                'id_regmf' => 193,
                'doors' => 5,
            ),
            212 => 
            array (
                'id_reg' => 213,
                'id_regmf' => 194,
                'doors' => 4,
            ),
            213 => 
            array (
                'id_reg' => 214,
                'id_regmf' => 195,
                'doors' => 3,
            ),
            214 => 
            array (
                'id_reg' => 215,
                'id_regmf' => 196,
                'doors' => 5,
            ),
            215 => 
            array (
                'id_reg' => 216,
                'id_regmf' => 197,
                'doors' => 5,
            ),
            216 => 
            array (
                'id_reg' => 217,
                'id_regmf' => 198,
                'doors' => 5,
            ),
            217 => 
            array (
                'id_reg' => 218,
                'id_regmf' => 199,
                'doors' => 2,
            ),
            218 => 
            array (
                'id_reg' => 219,
                'id_regmf' => 199,
                'doors' => 3,
            ),
            219 => 
            array (
                'id_reg' => 220,
                'id_regmf' => 200,
                'doors' => 2,
            ),
            220 => 
            array (
                'id_reg' => 221,
                'id_regmf' => 200,
                'doors' => 3,
            ),
            221 => 
            array (
                'id_reg' => 222,
                'id_regmf' => 201,
                'doors' => 4,
            ),
            222 => 
            array (
                'id_reg' => 223,
                'id_regmf' => 203,
                'doors' => 2,
            ),
            223 => 
            array (
                'id_reg' => 224,
                'id_regmf' => 202,
                'doors' => 2,
            ),
            224 => 
            array (
                'id_reg' => 225,
                'id_regmf' => 204,
                'doors' => 2,
            ),
            225 => 
            array (
                'id_reg' => 226,
                'id_regmf' => 206,
                'doors' => 4,
            ),
            226 => 
            array (
                'id_reg' => 227,
                'id_regmf' => 207,
                'doors' => 2,
            ),
            227 => 
            array (
                'id_reg' => 228,
                'id_regmf' => 208,
                'doors' => 4,
            ),
            228 => 
            array (
                'id_reg' => 229,
                'id_regmf' => 209,
                'doors' => 4,
            ),
            229 => 
            array (
                'id_reg' => 230,
                'id_regmf' => 205,
                'doors' => 4,
            ),
            230 => 
            array (
                'id_reg' => 231,
                'id_regmf' => 210,
                'doors' => 4,
            ),
            231 => 
            array (
                'id_reg' => 232,
                'id_regmf' => 211,
                'doors' => 4,
            ),
            232 => 
            array (
                'id_reg' => 233,
                'id_regmf' => 212,
                'doors' => 4,
            ),
            233 => 
            array (
                'id_reg' => 234,
                'id_regmf' => 213,
                'doors' => 4,
            ),
            234 => 
            array (
                'id_reg' => 235,
                'id_regmf' => 214,
                'doors' => 2,
            ),
            235 => 
            array (
                'id_reg' => 236,
                'id_regmf' => 214,
                'doors' => 5,
            ),
            236 => 
            array (
                'id_reg' => 237,
                'id_regmf' => 215,
                'doors' => 4,
            ),
            237 => 
            array (
                'id_reg' => 238,
                'id_regmf' => 216,
                'doors' => 4,
            ),
            238 => 
            array (
                'id_reg' => 239,
                'id_regmf' => 217,
                'doors' => 4,
            ),
            239 => 
            array (
                'id_reg' => 240,
                'id_regmf' => 218,
                'doors' => 4,
            ),
            240 => 
            array (
                'id_reg' => 241,
                'id_regmf' => 219,
                'doors' => 4,
            ),
            241 => 
            array (
                'id_reg' => 242,
                'id_regmf' => 220,
                'doors' => 5,
            ),
            242 => 
            array (
                'id_reg' => 243,
                'id_regmf' => 221,
                'doors' => 0,
            ),
            243 => 
            array (
                'id_reg' => 244,
                'id_regmf' => 222,
                'doors' => 4,
            ),
            244 => 
            array (
                'id_reg' => 245,
                'id_regmf' => 223,
                'doors' => 3,
            ),
            245 => 
            array (
                'id_reg' => 246,
                'id_regmf' => 224,
                'doors' => 5,
            ),
            246 => 
            array (
                'id_reg' => 247,
                'id_regmf' => 225,
                'doors' => 2,
            ),
            247 => 
            array (
                'id_reg' => 248,
                'id_regmf' => 225,
                'doors' => 4,
            ),
            248 => 
            array (
                'id_reg' => 249,
                'id_regmf' => 226,
                'doors' => 4,
            ),
            249 => 
            array (
                'id_reg' => 250,
                'id_regmf' => 227,
                'doors' => 0,
            ),
            250 => 
            array (
                'id_reg' => 251,
                'id_regmf' => 227,
                'doors' => 2,
            ),
            251 => 
            array (
                'id_reg' => 252,
                'id_regmf' => 227,
                'doors' => 4,
            ),
            252 => 
            array (
                'id_reg' => 253,
                'id_regmf' => 227,
                'doors' => 5,
            ),
            253 => 
            array (
                'id_reg' => 254,
                'id_regmf' => 228,
                'doors' => 2,
            ),
            254 => 
            array (
                'id_reg' => 255,
                'id_regmf' => 229,
                'doors' => 4,
            ),
            255 => 
            array (
                'id_reg' => 256,
                'id_regmf' => 230,
                'doors' => 4,
            ),
            256 => 
            array (
                'id_reg' => 257,
                'id_regmf' => 231,
                'doors' => 4,
            ),
            257 => 
            array (
                'id_reg' => 258,
                'id_regmf' => 232,
                'doors' => 2,
            ),
            258 => 
            array (
                'id_reg' => 259,
                'id_regmf' => 233,
                'doors' => 2,
            ),
            259 => 
            array (
                'id_reg' => 260,
                'id_regmf' => 234,
                'doors' => 2,
            ),
            260 => 
            array (
                'id_reg' => 261,
                'id_regmf' => 235,
                'doors' => 2,
            ),
            261 => 
            array (
                'id_reg' => 262,
                'id_regmf' => 236,
                'doors' => 4,
            ),
            262 => 
            array (
                'id_reg' => 263,
                'id_regmf' => 237,
                'doors' => 3,
            ),
            263 => 
            array (
                'id_reg' => 264,
                'id_regmf' => 238,
                'doors' => 2,
            ),
            264 => 
            array (
                'id_reg' => 265,
                'id_regmf' => 239,
                'doors' => 3,
            ),
            265 => 
            array (
                'id_reg' => 266,
                'id_regmf' => 240,
                'doors' => 5,
            ),
            266 => 
            array (
                'id_reg' => 267,
                'id_regmf' => 241,
                'doors' => 0,
            ),
            267 => 
            array (
                'id_reg' => 268,
                'id_regmf' => 241,
                'doors' => 2,
            ),
            268 => 
            array (
                'id_reg' => 269,
                'id_regmf' => 241,
                'doors' => 5,
            ),
            269 => 
            array (
                'id_reg' => 270,
                'id_regmf' => 242,
                'doors' => 5,
            ),
            270 => 
            array (
                'id_reg' => 271,
                'id_regmf' => 243,
                'doors' => 5,
            ),
            271 => 
            array (
                'id_reg' => 272,
                'id_regmf' => 244,
                'doors' => 0,
            ),
            272 => 
            array (
                'id_reg' => 273,
                'id_regmf' => 244,
                'doors' => 2,
            ),
            273 => 
            array (
                'id_reg' => 274,
                'id_regmf' => 244,
                'doors' => 5,
            ),
            274 => 
            array (
                'id_reg' => 275,
                'id_regmf' => 245,
                'doors' => 5,
            ),
            275 => 
            array (
                'id_reg' => 276,
                'id_regmf' => 246,
                'doors' => 5,
            ),
            276 => 
            array (
                'id_reg' => 277,
                'id_regmf' => 247,
                'doors' => 5,
            ),
            277 => 
            array (
                'id_reg' => 278,
                'id_regmf' => 248,
                'doors' => 5,
            ),
            278 => 
            array (
                'id_reg' => 279,
                'id_regmf' => 249,
                'doors' => 5,
            ),
            279 => 
            array (
                'id_reg' => 280,
                'id_regmf' => 250,
                'doors' => 5,
            ),
            280 => 
            array (
                'id_reg' => 281,
                'id_regmf' => 251,
                'doors' => 2,
            ),
            281 => 
            array (
                'id_reg' => 282,
                'id_regmf' => 251,
                'doors' => 5,
            ),
            282 => 
            array (
                'id_reg' => 283,
                'id_regmf' => 252,
                'doors' => 0,
            ),
            283 => 
            array (
                'id_reg' => 284,
                'id_regmf' => 252,
                'doors' => 5,
            ),
            284 => 
            array (
                'id_reg' => 285,
                'id_regmf' => 253,
                'doors' => 2,
            ),
            285 => 
            array (
                'id_reg' => 286,
                'id_regmf' => 253,
                'doors' => 5,
            ),
            286 => 
            array (
                'id_reg' => 287,
                'id_regmf' => 254,
                'doors' => 5,
            ),
            287 => 
            array (
                'id_reg' => 288,
                'id_regmf' => 255,
                'doors' => 0,
            ),
            288 => 
            array (
                'id_reg' => 289,
                'id_regmf' => 255,
                'doors' => 2,
            ),
            289 => 
            array (
                'id_reg' => 290,
                'id_regmf' => 255,
                'doors' => 5,
            ),
            290 => 
            array (
                'id_reg' => 291,
                'id_regmf' => 256,
                'doors' => 5,
            ),
            291 => 
            array (
                'id_reg' => 292,
                'id_regmf' => 257,
                'doors' => 5,
            ),
            292 => 
            array (
                'id_reg' => 293,
                'id_regmf' => 258,
                'doors' => 2,
            ),
            293 => 
            array (
                'id_reg' => 294,
                'id_regmf' => 258,
                'doors' => 5,
            ),
            294 => 
            array (
                'id_reg' => 295,
                'id_regmf' => 259,
                'doors' => 5,
            ),
            295 => 
            array (
                'id_reg' => 296,
                'id_regmf' => 266,
                'doors' => 5,
            ),
            296 => 
            array (
                'id_reg' => 297,
                'id_regmf' => 260,
                'doors' => 0,
            ),
            297 => 
            array (
                'id_reg' => 298,
                'id_regmf' => 260,
                'doors' => 2,
            ),
            298 => 
            array (
                'id_reg' => 299,
                'id_regmf' => 260,
                'doors' => 4,
            ),
            299 => 
            array (
                'id_reg' => 300,
                'id_regmf' => 260,
                'doors' => 5,
            ),
            300 => 
            array (
                'id_reg' => 301,
                'id_regmf' => 261,
                'doors' => 5,
            ),
            301 => 
            array (
                'id_reg' => 302,
                'id_regmf' => 262,
                'doors' => 5,
            ),
            302 => 
            array (
                'id_reg' => 303,
                'id_regmf' => 263,
                'doors' => 5,
            ),
            303 => 
            array (
                'id_reg' => 304,
                'id_regmf' => 264,
                'doors' => 5,
            ),
            304 => 
            array (
                'id_reg' => 305,
                'id_regmf' => 265,
                'doors' => 5,
            ),
            305 => 
            array (
                'id_reg' => 306,
                'id_regmf' => 267,
                'doors' => 0,
            ),
            306 => 
            array (
                'id_reg' => 307,
                'id_regmf' => 267,
                'doors' => 2,
            ),
            307 => 
            array (
                'id_reg' => 308,
                'id_regmf' => 268,
                'doors' => 0,
            ),
            308 => 
            array (
                'id_reg' => 309,
                'id_regmf' => 268,
                'doors' => 2,
            ),
            309 => 
            array (
                'id_reg' => 310,
                'id_regmf' => 269,
                'doors' => 0,
            ),
            310 => 
            array (
                'id_reg' => 311,
                'id_regmf' => 269,
                'doors' => 2,
            ),
            311 => 
            array (
                'id_reg' => 312,
                'id_regmf' => 269,
                'doors' => 4,
            ),
            312 => 
            array (
                'id_reg' => 313,
                'id_regmf' => 269,
                'doors' => 5,
            ),
            313 => 
            array (
                'id_reg' => 314,
                'id_regmf' => 270,
                'doors' => 5,
            ),
            314 => 
            array (
                'id_reg' => 315,
                'id_regmf' => 271,
                'doors' => 0,
            ),
            315 => 
            array (
                'id_reg' => 316,
                'id_regmf' => 272,
                'doors' => 0,
            ),
            316 => 
            array (
                'id_reg' => 317,
                'id_regmf' => 273,
                'doors' => 0,
            ),
            317 => 
            array (
                'id_reg' => 318,
                'id_regmf' => 274,
                'doors' => 0,
            ),
            318 => 
            array (
                'id_reg' => 319,
                'id_regmf' => 275,
                'doors' => 0,
            ),
            319 => 
            array (
                'id_reg' => 320,
                'id_regmf' => 279,
                'doors' => 0,
            ),
            320 => 
            array (
                'id_reg' => 321,
                'id_regmf' => 276,
                'doors' => 0,
            ),
            321 => 
            array (
                'id_reg' => 322,
                'id_regmf' => 277,
                'doors' => 0,
            ),
            322 => 
            array (
                'id_reg' => 323,
                'id_regmf' => 278,
                'doors' => 0,
            ),
            323 => 
            array (
                'id_reg' => 324,
                'id_regmf' => 280,
                'doors' => 0,
            ),
            324 => 
            array (
                'id_reg' => 325,
                'id_regmf' => 282,
                'doors' => 5,
            ),
            325 => 
            array (
                'id_reg' => 326,
                'id_regmf' => 281,
                'doors' => 5,
            ),
            326 => 
            array (
                'id_reg' => 327,
                'id_regmf' => 284,
                'doors' => 2,
            ),
            327 => 
            array (
                'id_reg' => 328,
                'id_regmf' => 284,
                'doors' => 5,
            ),
            328 => 
            array (
                'id_reg' => 329,
                'id_regmf' => 283,
                'doors' => 2,
            ),
            329 => 
            array (
                'id_reg' => 330,
                'id_regmf' => 283,
                'doors' => 5,
            ),
            330 => 
            array (
                'id_reg' => 331,
                'id_regmf' => 285,
                'doors' => 2,
            ),
            331 => 
            array (
                'id_reg' => 332,
                'id_regmf' => 286,
                'doors' => 0,
            ),
            332 => 
            array (
                'id_reg' => 333,
                'id_regmf' => 287,
                'doors' => 4,
            ),
            333 => 
            array (
                'id_reg' => 334,
                'id_regmf' => 288,
                'doors' => 2,
            ),
            334 => 
            array (
                'id_reg' => 335,
                'id_regmf' => 288,
                'doors' => 3,
            ),
            335 => 
            array (
                'id_reg' => 336,
                'id_regmf' => 288,
                'doors' => 5,
            ),
            336 => 
            array (
                'id_reg' => 337,
                'id_regmf' => 289,
                'doors' => 2,
            ),
            337 => 
            array (
                'id_reg' => 338,
                'id_regmf' => 290,
                'doors' => 2,
            ),
            338 => 
            array (
                'id_reg' => 339,
                'id_regmf' => 291,
                'doors' => 0,
            ),
            339 => 
            array (
                'id_reg' => 340,
                'id_regmf' => 291,
                'doors' => 2,
            ),
            340 => 
            array (
                'id_reg' => 341,
                'id_regmf' => 292,
                'doors' => 0,
            ),
            341 => 
            array (
                'id_reg' => 342,
                'id_regmf' => 292,
                'doors' => 5,
            ),
            342 => 
            array (
                'id_reg' => 343,
                'id_regmf' => 293,
                'doors' => 2,
            ),
            343 => 
            array (
                'id_reg' => 344,
                'id_regmf' => 294,
                'doors' => 2,
            ),
            344 => 
            array (
                'id_reg' => 345,
                'id_regmf' => 295,
                'doors' => 3,
            ),
            345 => 
            array (
                'id_reg' => 346,
                'id_regmf' => 296,
                'doors' => 3,
            ),
            346 => 
            array (
                'id_reg' => 347,
                'id_regmf' => 297,
                'doors' => 3,
            ),
            347 => 
            array (
                'id_reg' => 348,
                'id_regmf' => 298,
                'doors' => 3,
            ),
            348 => 
            array (
                'id_reg' => 349,
                'id_regmf' => 299,
                'doors' => 3,
            ),
            349 => 
            array (
                'id_reg' => 350,
                'id_regmf' => 300,
                'doors' => 4,
            ),
            350 => 
            array (
                'id_reg' => 351,
                'id_regmf' => 301,
                'doors' => 2,
            ),
            351 => 
            array (
                'id_reg' => 352,
                'id_regmf' => 302,
                'doors' => 5,
            ),
            352 => 
            array (
                'id_reg' => 353,
                'id_regmf' => 303,
                'doors' => 2,
            ),
            353 => 
            array (
                'id_reg' => 354,
                'id_regmf' => 303,
                'doors' => 4,
            ),
            354 => 
            array (
                'id_reg' => 355,
                'id_regmf' => 304,
                'doors' => 2,
            ),
            355 => 
            array (
                'id_reg' => 356,
                'id_regmf' => 304,
                'doors' => 4,
            ),
            356 => 
            array (
                'id_reg' => 357,
                'id_regmf' => 305,
                'doors' => 2,
            ),
            357 => 
            array (
                'id_reg' => 358,
                'id_regmf' => 305,
                'doors' => 4,
            ),
            358 => 
            array (
                'id_reg' => 359,
                'id_regmf' => 306,
                'doors' => 4,
            ),
            359 => 
            array (
                'id_reg' => 360,
                'id_regmf' => 307,
                'doors' => 4,
            ),
            360 => 
            array (
                'id_reg' => 361,
                'id_regmf' => 308,
                'doors' => 4,
            ),
            361 => 
            array (
                'id_reg' => 362,
                'id_regmf' => 309,
                'doors' => 4,
            ),
            362 => 
            array (
                'id_reg' => 363,
                'id_regmf' => 310,
                'doors' => 4,
            ),
            363 => 
            array (
                'id_reg' => 364,
                'id_regmf' => 311,
                'doors' => 4,
            ),
            364 => 
            array (
                'id_reg' => 365,
                'id_regmf' => 312,
                'doors' => 4,
            ),
            365 => 
            array (
                'id_reg' => 366,
                'id_regmf' => 313,
                'doors' => 4,
            ),
            366 => 
            array (
                'id_reg' => 367,
                'id_regmf' => 314,
                'doors' => 4,
            ),
            367 => 
            array (
                'id_reg' => 368,
                'id_regmf' => 315,
                'doors' => 4,
            ),
            368 => 
            array (
                'id_reg' => 369,
                'id_regmf' => 316,
                'doors' => 4,
            ),
            369 => 
            array (
                'id_reg' => 370,
                'id_regmf' => 317,
                'doors' => 4,
            ),
            370 => 
            array (
                'id_reg' => 371,
                'id_regmf' => 318,
                'doors' => 4,
            ),
            371 => 
            array (
                'id_reg' => 372,
                'id_regmf' => 319,
                'doors' => 4,
            ),
            372 => 
            array (
                'id_reg' => 373,
                'id_regmf' => 320,
                'doors' => 3,
            ),
            373 => 
            array (
                'id_reg' => 374,
                'id_regmf' => 321,
                'doors' => 3,
            ),
            374 => 
            array (
                'id_reg' => 375,
                'id_regmf' => 322,
                'doors' => 2,
            ),
            375 => 
            array (
                'id_reg' => 376,
                'id_regmf' => 323,
                'doors' => 0,
            ),
            376 => 
            array (
                'id_reg' => 377,
                'id_regmf' => 324,
                'doors' => 0,
            ),
            377 => 
            array (
                'id_reg' => 378,
                'id_regmf' => 325,
                'doors' => 2,
            ),
            378 => 
            array (
                'id_reg' => 379,
                'id_regmf' => 326,
                'doors' => 2,
            ),
            379 => 
            array (
                'id_reg' => 380,
                'id_regmf' => 327,
                'doors' => 2,
            ),
            380 => 
            array (
                'id_reg' => 381,
                'id_regmf' => 328,
                'doors' => 2,
            ),
            381 => 
            array (
                'id_reg' => 382,
                'id_regmf' => 329,
                'doors' => 2,
            ),
            382 => 
            array (
                'id_reg' => 383,
                'id_regmf' => 336,
                'doors' => 2,
            ),
            383 => 
            array (
                'id_reg' => 384,
                'id_regmf' => 337,
                'doors' => 2,
            ),
            384 => 
            array (
                'id_reg' => 385,
                'id_regmf' => 338,
                'doors' => 2,
            ),
            385 => 
            array (
                'id_reg' => 386,
                'id_regmf' => 339,
                'doors' => 4,
            ),
            386 => 
            array (
                'id_reg' => 387,
                'id_regmf' => 340,
                'doors' => 2,
            ),
            387 => 
            array (
                'id_reg' => 388,
                'id_regmf' => 341,
                'doors' => 4,
            ),
            388 => 
            array (
                'id_reg' => 389,
                'id_regmf' => 342,
                'doors' => 2,
            ),
            389 => 
            array (
                'id_reg' => 390,
                'id_regmf' => 343,
                'doors' => 4,
            ),
            390 => 
            array (
                'id_reg' => 391,
                'id_regmf' => 344,
                'doors' => 2,
            ),
            391 => 
            array (
                'id_reg' => 392,
                'id_regmf' => 330,
                'doors' => 2,
            ),
            392 => 
            array (
                'id_reg' => 393,
                'id_regmf' => 330,
                'doors' => 4,
            ),
            393 => 
            array (
                'id_reg' => 394,
                'id_regmf' => 331,
                'doors' => 2,
            ),
            394 => 
            array (
                'id_reg' => 395,
                'id_regmf' => 332,
                'doors' => 2,
            ),
            395 => 
            array (
                'id_reg' => 396,
                'id_regmf' => 332,
                'doors' => 4,
            ),
            396 => 
            array (
                'id_reg' => 397,
                'id_regmf' => 333,
                'doors' => 2,
            ),
            397 => 
            array (
                'id_reg' => 398,
                'id_regmf' => 333,
                'doors' => 4,
            ),
            398 => 
            array (
                'id_reg' => 399,
                'id_regmf' => 334,
                'doors' => 2,
            ),
            399 => 
            array (
                'id_reg' => 400,
                'id_regmf' => 335,
                'doors' => 2,
            ),
            400 => 
            array (
                'id_reg' => 401,
                'id_regmf' => 345,
                'doors' => 5,
            ),
            401 => 
            array (
                'id_reg' => 402,
                'id_regmf' => 346,
                'doors' => 2,
            ),
            402 => 
            array (
                'id_reg' => 403,
                'id_regmf' => 347,
                'doors' => 1,
            ),
            403 => 
            array (
                'id_reg' => 404,
                'id_regmf' => 348,
                'doors' => 2,
            ),
            404 => 
            array (
                'id_reg' => 405,
                'id_regmf' => 349,
                'doors' => 2,
            ),
            405 => 
            array (
                'id_reg' => 406,
                'id_regmf' => 349,
                'doors' => 5,
            ),
            406 => 
            array (
                'id_reg' => 407,
                'id_regmf' => 351,
                'doors' => 5,
            ),
            407 => 
            array (
                'id_reg' => 408,
                'id_regmf' => 350,
                'doors' => 2,
            ),
            408 => 
            array (
                'id_reg' => 409,
                'id_regmf' => 350,
                'doors' => 5,
            ),
            409 => 
            array (
                'id_reg' => 410,
                'id_regmf' => 352,
                'doors' => 2,
            ),
            410 => 
            array (
                'id_reg' => 411,
                'id_regmf' => 352,
                'doors' => 5,
            ),
            411 => 
            array (
                'id_reg' => 412,
                'id_regmf' => 353,
                'doors' => 2,
            ),
            412 => 
            array (
                'id_reg' => 413,
                'id_regmf' => 353,
                'doors' => 5,
            ),
            413 => 
            array (
                'id_reg' => 414,
                'id_regmf' => 354,
                'doors' => 2,
            ),
            414 => 
            array (
                'id_reg' => 415,
                'id_regmf' => 356,
                'doors' => 5,
            ),
            415 => 
            array (
                'id_reg' => 416,
                'id_regmf' => 355,
                'doors' => 5,
            ),
            416 => 
            array (
                'id_reg' => 417,
                'id_regmf' => 357,
                'doors' => 5,
            ),
            417 => 
            array (
                'id_reg' => 418,
                'id_regmf' => 359,
                'doors' => 5,
            ),
            418 => 
            array (
                'id_reg' => 419,
                'id_regmf' => 358,
                'doors' => 5,
            ),
            419 => 
            array (
                'id_reg' => 420,
                'id_regmf' => 360,
                'doors' => 2,
            ),
            420 => 
            array (
                'id_reg' => 421,
                'id_regmf' => 361,
                'doors' => 2,
            ),
            421 => 
            array (
                'id_reg' => 422,
                'id_regmf' => 362,
                'doors' => 2,
            ),
            422 => 
            array (
                'id_reg' => 423,
                'id_regmf' => 363,
                'doors' => 2,
            ),
            423 => 
            array (
                'id_reg' => 424,
                'id_regmf' => 364,
                'doors' => 2,
            ),
            424 => 
            array (
                'id_reg' => 425,
                'id_regmf' => 365,
                'doors' => 5,
            ),
            425 => 
            array (
                'id_reg' => 426,
                'id_regmf' => 366,
                'doors' => 2,
            ),
            426 => 
            array (
                'id_reg' => 427,
                'id_regmf' => 367,
                'doors' => 2,
            ),
            427 => 
            array (
                'id_reg' => 428,
                'id_regmf' => 368,
                'doors' => 0,
            ),
            428 => 
            array (
                'id_reg' => 429,
                'id_regmf' => 369,
                'doors' => 2,
            ),
            429 => 
            array (
                'id_reg' => 430,
                'id_regmf' => 370,
                'doors' => 2,
            ),
            430 => 
            array (
                'id_reg' => 431,
                'id_regmf' => 371,
                'doors' => 2,
            ),
            431 => 
            array (
                'id_reg' => 432,
                'id_regmf' => 372,
                'doors' => 0,
            ),
            432 => 
            array (
                'id_reg' => 433,
                'id_regmf' => 373,
                'doors' => 2,
            ),
            433 => 
            array (
                'id_reg' => 434,
                'id_regmf' => 374,
                'doors' => 2,
            ),
            434 => 
            array (
                'id_reg' => 435,
                'id_regmf' => 375,
                'doors' => 2,
            ),
            435 => 
            array (
                'id_reg' => 436,
                'id_regmf' => 376,
                'doors' => 0,
            ),
            436 => 
            array (
                'id_reg' => 437,
                'id_regmf' => 377,
                'doors' => 0,
            ),
            437 => 
            array (
                'id_reg' => 438,
                'id_regmf' => 378,
                'doors' => 0,
            ),
            438 => 
            array (
                'id_reg' => 439,
                'id_regmf' => 379,
                'doors' => 0,
            ),
            439 => 
            array (
                'id_reg' => 440,
                'id_regmf' => 381,
                'doors' => 4,
            ),
            440 => 
            array (
                'id_reg' => 441,
                'id_regmf' => 382,
                'doors' => 2,
            ),
            441 => 
            array (
                'id_reg' => 442,
                'id_regmf' => 383,
                'doors' => 4,
            ),
            442 => 
            array (
                'id_reg' => 443,
                'id_regmf' => 384,
                'doors' => 4,
            ),
            443 => 
            array (
                'id_reg' => 444,
                'id_regmf' => 385,
                'doors' => 4,
            ),
            444 => 
            array (
                'id_reg' => 445,
                'id_regmf' => 380,
                'doors' => 2,
            ),
            445 => 
            array (
                'id_reg' => 446,
                'id_regmf' => 380,
                'doors' => 4,
            ),
            446 => 
            array (
                'id_reg' => 447,
                'id_regmf' => 386,
                'doors' => 4,
            ),
            447 => 
            array (
                'id_reg' => 448,
                'id_regmf' => 387,
                'doors' => 4,
            ),
            448 => 
            array (
                'id_reg' => 449,
                'id_regmf' => 387,
                'doors' => 5,
            ),
            449 => 
            array (
                'id_reg' => 450,
                'id_regmf' => 388,
                'doors' => 2,
            ),
            450 => 
            array (
                'id_reg' => 451,
                'id_regmf' => 389,
                'doors' => 4,
            ),
            451 => 
            array (
                'id_reg' => 452,
                'id_regmf' => 390,
                'doors' => 5,
            ),
            452 => 
            array (
                'id_reg' => 453,
                'id_regmf' => 391,
                'doors' => 2,
            ),
            453 => 
            array (
                'id_reg' => 454,
                'id_regmf' => 391,
                'doors' => 4,
            ),
            454 => 
            array (
                'id_reg' => 455,
                'id_regmf' => 392,
                'doors' => 4,
            ),
            455 => 
            array (
                'id_reg' => 456,
                'id_regmf' => 393,
                'doors' => 4,
            ),
            456 => 
            array (
                'id_reg' => 457,
                'id_regmf' => 394,
                'doors' => 4,
            ),
            457 => 
            array (
                'id_reg' => 458,
                'id_regmf' => 395,
                'doors' => 4,
            ),
            458 => 
            array (
                'id_reg' => 459,
                'id_regmf' => 396,
                'doors' => 4,
            ),
            459 => 
            array (
                'id_reg' => 460,
                'id_regmf' => 397,
                'doors' => 2,
            ),
            460 => 
            array (
                'id_reg' => 461,
                'id_regmf' => 398,
                'doors' => 3,
            ),
            461 => 
            array (
                'id_reg' => 462,
                'id_regmf' => 399,
                'doors' => 2,
            ),
            462 => 
            array (
                'id_reg' => 463,
                'id_regmf' => 399,
                'doors' => 4,
            ),
            463 => 
            array (
                'id_reg' => 464,
                'id_regmf' => 400,
                'doors' => 5,
            ),
            464 => 
            array (
                'id_reg' => 465,
                'id_regmf' => 401,
                'doors' => 2,
            ),
            465 => 
            array (
                'id_reg' => 466,
                'id_regmf' => 402,
                'doors' => 2,
            ),
            466 => 
            array (
                'id_reg' => 467,
                'id_regmf' => 402,
                'doors' => 5,
            ),
            467 => 
            array (
                'id_reg' => 468,
                'id_regmf' => 403,
                'doors' => 3,
            ),
            468 => 
            array (
                'id_reg' => 469,
                'id_regmf' => 404,
                'doors' => 2,
            ),
            469 => 
            array (
                'id_reg' => 470,
                'id_regmf' => 404,
                'doors' => 4,
            ),
            470 => 
            array (
                'id_reg' => 471,
                'id_regmf' => 405,
                'doors' => 2,
            ),
            471 => 
            array (
                'id_reg' => 472,
                'id_regmf' => 405,
                'doors' => 4,
            ),
            472 => 
            array (
                'id_reg' => 473,
                'id_regmf' => 406,
                'doors' => 3,
            ),
            473 => 
            array (
                'id_reg' => 474,
                'id_regmf' => 407,
                'doors' => 0,
            ),
            474 => 
            array (
                'id_reg' => 475,
                'id_regmf' => 408,
                'doors' => 5,
            ),
            475 => 
            array (
                'id_reg' => 476,
                'id_regmf' => 409,
                'doors' => 4,
            ),
            476 => 
            array (
                'id_reg' => 477,
                'id_regmf' => 410,
                'doors' => 0,
            ),
            477 => 
            array (
                'id_reg' => 478,
                'id_regmf' => 411,
                'doors' => 4,
            ),
            478 => 
            array (
                'id_reg' => 479,
                'id_regmf' => 412,
                'doors' => 2,
            ),
            479 => 
            array (
                'id_reg' => 480,
                'id_regmf' => 413,
                'doors' => 4,
            ),
            480 => 
            array (
                'id_reg' => 481,
                'id_regmf' => 414,
                'doors' => 4,
            ),
            481 => 
            array (
                'id_reg' => 482,
                'id_regmf' => 414,
                'doors' => 5,
            ),
            482 => 
            array (
                'id_reg' => 483,
                'id_regmf' => 416,
                'doors' => 4,
            ),
            483 => 
            array (
                'id_reg' => 484,
                'id_regmf' => 416,
                'doors' => 5,
            ),
            484 => 
            array (
                'id_reg' => 485,
                'id_regmf' => 415,
                'doors' => 4,
            ),
            485 => 
            array (
                'id_reg' => 486,
                'id_regmf' => 415,
                'doors' => 5,
            ),
            486 => 
            array (
                'id_reg' => 487,
                'id_regmf' => 417,
                'doors' => 4,
            ),
            487 => 
            array (
                'id_reg' => 488,
                'id_regmf' => 418,
                'doors' => 4,
            ),
            488 => 
            array (
                'id_reg' => 489,
                'id_regmf' => 419,
                'doors' => 4,
            ),
            489 => 
            array (
                'id_reg' => 490,
                'id_regmf' => 420,
                'doors' => 4,
            ),
            490 => 
            array (
                'id_reg' => 491,
                'id_regmf' => 421,
                'doors' => 2,
            ),
            491 => 
            array (
                'id_reg' => 492,
                'id_regmf' => 422,
                'doors' => 4,
            ),
            492 => 
            array (
                'id_reg' => 493,
                'id_regmf' => 423,
                'doors' => 4,
            ),
            493 => 
            array (
                'id_reg' => 494,
                'id_regmf' => 424,
                'doors' => 2,
            ),
            494 => 
            array (
                'id_reg' => 495,
                'id_regmf' => 424,
                'doors' => 4,
            ),
            495 => 
            array (
                'id_reg' => 496,
                'id_regmf' => 425,
                'doors' => 4,
            ),
            496 => 
            array (
                'id_reg' => 497,
                'id_regmf' => 427,
                'doors' => 2,
            ),
            497 => 
            array (
                'id_reg' => 498,
                'id_regmf' => 427,
                'doors' => 4,
            ),
            498 => 
            array (
                'id_reg' => 499,
                'id_regmf' => 426,
                'doors' => 2,
            ),
            499 => 
            array (
                'id_reg' => 500,
                'id_regmf' => 429,
                'doors' => 5,
            ),
        ));
        \DB::table('model_fuel_numdoors')->insert(array (
            0 => 
            array (
                'id_reg' => 501,
                'id_regmf' => 428,
                'doors' => 5,
            ),
            1 => 
            array (
                'id_reg' => 502,
                'id_regmf' => 430,
                'doors' => 4,
            ),
            2 => 
            array (
                'id_reg' => 503,
                'id_regmf' => 431,
                'doors' => 4,
            ),
            3 => 
            array (
                'id_reg' => 504,
                'id_regmf' => 432,
                'doors' => 2,
            ),
            4 => 
            array (
                'id_reg' => 505,
                'id_regmf' => 432,
                'doors' => 4,
            ),
            5 => 
            array (
                'id_reg' => 506,
                'id_regmf' => 433,
                'doors' => 2,
            ),
            6 => 
            array (
                'id_reg' => 507,
                'id_regmf' => 434,
                'doors' => 2,
            ),
            7 => 
            array (
                'id_reg' => 508,
                'id_regmf' => 434,
                'doors' => 4,
            ),
            8 => 
            array (
                'id_reg' => 509,
                'id_regmf' => 435,
                'doors' => 4,
            ),
            9 => 
            array (
                'id_reg' => 510,
                'id_regmf' => 436,
                'doors' => 4,
            ),
            10 => 
            array (
                'id_reg' => 511,
                'id_regmf' => 437,
                'doors' => 2,
            ),
            11 => 
            array (
                'id_reg' => 512,
                'id_regmf' => 439,
                'doors' => 5,
            ),
            12 => 
            array (
                'id_reg' => 513,
                'id_regmf' => 438,
                'doors' => 5,
            ),
            13 => 
            array (
                'id_reg' => 514,
                'id_regmf' => 440,
                'doors' => 3,
            ),
            14 => 
            array (
                'id_reg' => 515,
                'id_regmf' => 441,
                'doors' => 4,
            ),
            15 => 
            array (
                'id_reg' => 516,
                'id_regmf' => 442,
                'doors' => 2,
            ),
            16 => 
            array (
                'id_reg' => 517,
                'id_regmf' => 443,
                'doors' => 2,
            ),
            17 => 
            array (
                'id_reg' => 518,
                'id_regmf' => 444,
                'doors' => 0,
            ),
            18 => 
            array (
                'id_reg' => 519,
                'id_regmf' => 445,
                'doors' => 0,
            ),
            19 => 
            array (
                'id_reg' => 520,
                'id_regmf' => 446,
                'doors' => 0,
            ),
            20 => 
            array (
                'id_reg' => 521,
                'id_regmf' => 447,
                'doors' => 4,
            ),
            21 => 
            array (
                'id_reg' => 522,
                'id_regmf' => 448,
                'doors' => 4,
            ),
            22 => 
            array (
                'id_reg' => 523,
                'id_regmf' => 449,
                'doors' => 0,
            ),
            23 => 
            array (
                'id_reg' => 524,
                'id_regmf' => 450,
                'doors' => 1,
            ),
            24 => 
            array (
                'id_reg' => 525,
                'id_regmf' => 451,
                'doors' => 2,
            ),
            25 => 
            array (
                'id_reg' => 526,
                'id_regmf' => 451,
                'doors' => 3,
            ),
            26 => 
            array (
                'id_reg' => 527,
                'id_regmf' => 452,
                'doors' => 2,
            ),
            27 => 
            array (
                'id_reg' => 528,
                'id_regmf' => 453,
                'doors' => 3,
            ),
            28 => 
            array (
                'id_reg' => 529,
                'id_regmf' => 454,
                'doors' => 2,
            ),
            29 => 
            array (
                'id_reg' => 530,
                'id_regmf' => 455,
                'doors' => 0,
            ),
            30 => 
            array (
                'id_reg' => 531,
                'id_regmf' => 456,
                'doors' => 3,
            ),
            31 => 
            array (
                'id_reg' => 532,
                'id_regmf' => 457,
                'doors' => 2,
            ),
            32 => 
            array (
                'id_reg' => 533,
                'id_regmf' => 458,
                'doors' => 2,
            ),
            33 => 
            array (
                'id_reg' => 534,
                'id_regmf' => 459,
                'doors' => 3,
            ),
            34 => 
            array (
                'id_reg' => 535,
                'id_regmf' => 460,
                'doors' => 3,
            ),
            35 => 
            array (
                'id_reg' => 536,
                'id_regmf' => 461,
                'doors' => 3,
            ),
            36 => 
            array (
                'id_reg' => 537,
                'id_regmf' => 462,
                'doors' => 2,
            ),
            37 => 
            array (
                'id_reg' => 538,
                'id_regmf' => 463,
                'doors' => 3,
            ),
            38 => 
            array (
                'id_reg' => 539,
                'id_regmf' => 464,
                'doors' => 2,
            ),
            39 => 
            array (
                'id_reg' => 540,
                'id_regmf' => 465,
                'doors' => 4,
            ),
            40 => 
            array (
                'id_reg' => 541,
                'id_regmf' => 466,
                'doors' => 2,
            ),
            41 => 
            array (
                'id_reg' => 542,
                'id_regmf' => 467,
                'doors' => 4,
            ),
            42 => 
            array (
                'id_reg' => 543,
                'id_regmf' => 468,
                'doors' => 4,
            ),
            43 => 
            array (
                'id_reg' => 544,
                'id_regmf' => 469,
                'doors' => 4,
            ),
            44 => 
            array (
                'id_reg' => 545,
                'id_regmf' => 470,
                'doors' => 6,
            ),
            45 => 
            array (
                'id_reg' => 546,
                'id_regmf' => 471,
                'doors' => 2,
            ),
            46 => 
            array (
                'id_reg' => 547,
                'id_regmf' => 471,
                'doors' => 4,
            ),
            47 => 
            array (
                'id_reg' => 548,
                'id_regmf' => 472,
                'doors' => 2,
            ),
            48 => 
            array (
                'id_reg' => 549,
                'id_regmf' => 474,
                'doors' => 2,
            ),
            49 => 
            array (
                'id_reg' => 550,
                'id_regmf' => 474,
                'doors' => 3,
            ),
            50 => 
            array (
                'id_reg' => 551,
                'id_regmf' => 474,
                'doors' => 4,
            ),
            51 => 
            array (
                'id_reg' => 552,
                'id_regmf' => 474,
                'doors' => 5,
            ),
            52 => 
            array (
                'id_reg' => 553,
                'id_regmf' => 473,
                'doors' => 2,
            ),
            53 => 
            array (
                'id_reg' => 554,
                'id_regmf' => 473,
                'doors' => 3,
            ),
            54 => 
            array (
                'id_reg' => 555,
                'id_regmf' => 473,
                'doors' => 5,
            ),
            55 => 
            array (
                'id_reg' => 556,
                'id_regmf' => 475,
                'doors' => 2,
            ),
            56 => 
            array (
                'id_reg' => 557,
                'id_regmf' => 476,
                'doors' => 2,
            ),
            57 => 
            array (
                'id_reg' => 558,
                'id_regmf' => 476,
                'doors' => 3,
            ),
            58 => 
            array (
                'id_reg' => 559,
                'id_regmf' => 477,
                'doors' => 2,
            ),
            59 => 
            array (
                'id_reg' => 560,
                'id_regmf' => 477,
                'doors' => 4,
            ),
            60 => 
            array (
                'id_reg' => 561,
                'id_regmf' => 477,
                'doors' => 5,
            ),
            61 => 
            array (
                'id_reg' => 562,
                'id_regmf' => 478,
                'doors' => 5,
            ),
            62 => 
            array (
                'id_reg' => 563,
                'id_regmf' => 479,
                'doors' => 2,
            ),
            63 => 
            array (
                'id_reg' => 564,
                'id_regmf' => 479,
                'doors' => 4,
            ),
            64 => 
            array (
                'id_reg' => 565,
                'id_regmf' => 480,
                'doors' => 4,
            ),
            65 => 
            array (
                'id_reg' => 566,
                'id_regmf' => 481,
                'doors' => 4,
            ),
            66 => 
            array (
                'id_reg' => 567,
                'id_regmf' => 482,
                'doors' => 2,
            ),
            67 => 
            array (
                'id_reg' => 568,
                'id_regmf' => 483,
                'doors' => 2,
            ),
            68 => 
            array (
                'id_reg' => 569,
                'id_regmf' => 483,
                'doors' => 4,
            ),
            69 => 
            array (
                'id_reg' => 570,
                'id_regmf' => 484,
                'doors' => 4,
            ),
            70 => 
            array (
                'id_reg' => 571,
                'id_regmf' => 485,
                'doors' => 2,
            ),
            71 => 
            array (
                'id_reg' => 572,
                'id_regmf' => 486,
                'doors' => 2,
            ),
            72 => 
            array (
                'id_reg' => 573,
                'id_regmf' => 487,
                'doors' => 4,
            ),
            73 => 
            array (
                'id_reg' => 574,
                'id_regmf' => 489,
                'doors' => 4,
            ),
            74 => 
            array (
                'id_reg' => 575,
                'id_regmf' => 489,
                'doors' => 5,
            ),
            75 => 
            array (
                'id_reg' => 576,
                'id_regmf' => 488,
                'doors' => 4,
            ),
            76 => 
            array (
                'id_reg' => 577,
                'id_regmf' => 488,
                'doors' => 5,
            ),
            77 => 
            array (
                'id_reg' => 578,
                'id_regmf' => 490,
                'doors' => 2,
            ),
            78 => 
            array (
                'id_reg' => 579,
                'id_regmf' => 491,
                'doors' => 4,
            ),
            79 => 
            array (
                'id_reg' => 580,
                'id_regmf' => 492,
                'doors' => 2,
            ),
            80 => 
            array (
                'id_reg' => 581,
                'id_regmf' => 493,
                'doors' => 2,
            ),
            81 => 
            array (
                'id_reg' => 582,
                'id_regmf' => 494,
                'doors' => 6,
            ),
            82 => 
            array (
                'id_reg' => 583,
                'id_regmf' => 495,
                'doors' => 2,
            ),
            83 => 
            array (
                'id_reg' => 584,
                'id_regmf' => 496,
                'doors' => 2,
            ),
            84 => 
            array (
                'id_reg' => 585,
                'id_regmf' => 497,
                'doors' => 4,
            ),
            85 => 
            array (
                'id_reg' => 586,
                'id_regmf' => 497,
                'doors' => 5,
            ),
            86 => 
            array (
                'id_reg' => 587,
                'id_regmf' => 498,
                'doors' => 2,
            ),
            87 => 
            array (
                'id_reg' => 588,
                'id_regmf' => 498,
                'doors' => 4,
            ),
            88 => 
            array (
                'id_reg' => 589,
                'id_regmf' => 499,
                'doors' => 2,
            ),
            89 => 
            array (
                'id_reg' => 590,
                'id_regmf' => 499,
                'doors' => 3,
            ),
            90 => 
            array (
                'id_reg' => 591,
                'id_regmf' => 500,
                'doors' => 2,
            ),
            91 => 
            array (
                'id_reg' => 592,
                'id_regmf' => 501,
                'doors' => 2,
            ),
            92 => 
            array (
                'id_reg' => 593,
                'id_regmf' => 502,
                'doors' => 2,
            ),
            93 => 
            array (
                'id_reg' => 594,
                'id_regmf' => 503,
                'doors' => 2,
            ),
            94 => 
            array (
                'id_reg' => 595,
                'id_regmf' => 504,
                'doors' => 5,
            ),
            95 => 
            array (
                'id_reg' => 596,
                'id_regmf' => 505,
                'doors' => 4,
            ),
            96 => 
            array (
                'id_reg' => 597,
                'id_regmf' => 506,
                'doors' => 5,
            ),
            97 => 
            array (
                'id_reg' => 598,
                'id_regmf' => 507,
                'doors' => 5,
            ),
            98 => 
            array (
                'id_reg' => 599,
                'id_regmf' => 508,
                'doors' => 5,
            ),
            99 => 
            array (
                'id_reg' => 600,
                'id_regmf' => 508,
                'doors' => 6,
            ),
            100 => 
            array (
                'id_reg' => 601,
                'id_regmf' => 509,
                'doors' => 2,
            ),
            101 => 
            array (
                'id_reg' => 602,
                'id_regmf' => 510,
                'doors' => 2,
            ),
            102 => 
            array (
                'id_reg' => 603,
                'id_regmf' => 511,
                'doors' => 3,
            ),
            103 => 
            array (
                'id_reg' => 604,
                'id_regmf' => 512,
                'doors' => 3,
            ),
            104 => 
            array (
                'id_reg' => 605,
                'id_regmf' => 513,
                'doors' => 2,
            ),
            105 => 
            array (
                'id_reg' => 606,
                'id_regmf' => 514,
                'doors' => 2,
            ),
            106 => 
            array (
                'id_reg' => 607,
                'id_regmf' => 515,
                'doors' => 5,
            ),
            107 => 
            array (
                'id_reg' => 608,
                'id_regmf' => 516,
                'doors' => 2,
            ),
            108 => 
            array (
                'id_reg' => 609,
                'id_regmf' => 516,
                'doors' => 4,
            ),
            109 => 
            array (
                'id_reg' => 610,
                'id_regmf' => 517,
                'doors' => 4,
            ),
            110 => 
            array (
                'id_reg' => 611,
                'id_regmf' => 518,
                'doors' => 2,
            ),
            111 => 
            array (
                'id_reg' => 612,
                'id_regmf' => 519,
                'doors' => 2,
            ),
            112 => 
            array (
                'id_reg' => 613,
                'id_regmf' => 520,
                'doors' => 2,
            ),
            113 => 
            array (
                'id_reg' => 614,
                'id_regmf' => 521,
                'doors' => 2,
            ),
            114 => 
            array (
                'id_reg' => 615,
                'id_regmf' => 522,
                'doors' => 2,
            ),
            115 => 
            array (
                'id_reg' => 616,
                'id_regmf' => 522,
                'doors' => 4,
            ),
            116 => 
            array (
                'id_reg' => 617,
                'id_regmf' => 522,
                'doors' => 5,
            ),
            117 => 
            array (
                'id_reg' => 618,
                'id_regmf' => 523,
                'doors' => 2,
            ),
            118 => 
            array (
                'id_reg' => 619,
                'id_regmf' => 525,
                'doors' => 4,
            ),
            119 => 
            array (
                'id_reg' => 620,
                'id_regmf' => 525,
                'doors' => 5,
            ),
            120 => 
            array (
                'id_reg' => 621,
                'id_regmf' => 524,
                'doors' => 4,
            ),
            121 => 
            array (
                'id_reg' => 622,
                'id_regmf' => 526,
                'doors' => 4,
            ),
            122 => 
            array (
                'id_reg' => 623,
                'id_regmf' => 527,
                'doors' => 4,
            ),
            123 => 
            array (
                'id_reg' => 624,
                'id_regmf' => 528,
                'doors' => 4,
            ),
            124 => 
            array (
                'id_reg' => 625,
                'id_regmf' => 529,
                'doors' => 2,
            ),
            125 => 
            array (
                'id_reg' => 626,
                'id_regmf' => 529,
                'doors' => 3,
            ),
            126 => 
            array (
                'id_reg' => 627,
                'id_regmf' => 529,
                'doors' => 4,
            ),
            127 => 
            array (
                'id_reg' => 628,
                'id_regmf' => 530,
                'doors' => 4,
            ),
            128 => 
            array (
                'id_reg' => 629,
                'id_regmf' => 531,
                'doors' => 2,
            ),
            129 => 
            array (
                'id_reg' => 630,
                'id_regmf' => 532,
                'doors' => 5,
            ),
            130 => 
            array (
                'id_reg' => 631,
                'id_regmf' => 533,
                'doors' => 5,
            ),
            131 => 
            array (
                'id_reg' => 632,
                'id_regmf' => 534,
                'doors' => 4,
            ),
            132 => 
            array (
                'id_reg' => 633,
                'id_regmf' => 535,
                'doors' => 2,
            ),
            133 => 
            array (
                'id_reg' => 634,
                'id_regmf' => 536,
                'doors' => 2,
            ),
            134 => 
            array (
                'id_reg' => 635,
                'id_regmf' => 536,
                'doors' => 4,
            ),
            135 => 
            array (
                'id_reg' => 636,
                'id_regmf' => 537,
                'doors' => 4,
            ),
            136 => 
            array (
                'id_reg' => 637,
                'id_regmf' => 538,
                'doors' => 5,
            ),
            137 => 
            array (
                'id_reg' => 638,
                'id_regmf' => 539,
                'doors' => 2,
            ),
            138 => 
            array (
                'id_reg' => 639,
                'id_regmf' => 539,
                'doors' => 4,
            ),
            139 => 
            array (
                'id_reg' => 640,
                'id_regmf' => 540,
                'doors' => 2,
            ),
            140 => 
            array (
                'id_reg' => 641,
                'id_regmf' => 540,
                'doors' => 4,
            ),
            141 => 
            array (
                'id_reg' => 642,
                'id_regmf' => 541,
                'doors' => 4,
            ),
            142 => 
            array (
                'id_reg' => 643,
                'id_regmf' => 542,
                'doors' => 4,
            ),
            143 => 
            array (
                'id_reg' => 644,
                'id_regmf' => 543,
                'doors' => 3,
            ),
            144 => 
            array (
                'id_reg' => 645,
                'id_regmf' => 544,
                'doors' => 2,
            ),
            145 => 
            array (
                'id_reg' => 646,
                'id_regmf' => 545,
                'doors' => 2,
            ),
            146 => 
            array (
                'id_reg' => 647,
                'id_regmf' => 546,
                'doors' => 5,
            ),
            147 => 
            array (
                'id_reg' => 648,
                'id_regmf' => 547,
                'doors' => 3,
            ),
            148 => 
            array (
                'id_reg' => 649,
                'id_regmf' => 548,
                'doors' => 2,
            ),
            149 => 
            array (
                'id_reg' => 650,
                'id_regmf' => 550,
                'doors' => 2,
            ),
            150 => 
            array (
                'id_reg' => 651,
                'id_regmf' => 550,
                'doors' => 3,
            ),
            151 => 
            array (
                'id_reg' => 652,
                'id_regmf' => 550,
                'doors' => 4,
            ),
            152 => 
            array (
                'id_reg' => 653,
                'id_regmf' => 550,
                'doors' => 5,
            ),
            153 => 
            array (
                'id_reg' => 654,
                'id_regmf' => 549,
                'doors' => 4,
            ),
            154 => 
            array (
                'id_reg' => 655,
                'id_regmf' => 549,
                'doors' => 5,
            ),
            155 => 
            array (
                'id_reg' => 656,
                'id_regmf' => 552,
                'doors' => 5,
            ),
            156 => 
            array (
                'id_reg' => 657,
                'id_regmf' => 551,
                'doors' => 5,
            ),
            157 => 
            array (
                'id_reg' => 658,
                'id_regmf' => 553,
                'doors' => 5,
            ),
            158 => 
            array (
                'id_reg' => 659,
                'id_regmf' => 554,
                'doors' => 5,
            ),
            159 => 
            array (
                'id_reg' => 660,
                'id_regmf' => 555,
                'doors' => 5,
            ),
            160 => 
            array (
                'id_reg' => 661,
                'id_regmf' => 556,
                'doors' => 5,
            ),
            161 => 
            array (
                'id_reg' => 662,
                'id_regmf' => 557,
                'doors' => 5,
            ),
            162 => 
            array (
                'id_reg' => 663,
                'id_regmf' => 558,
                'doors' => 5,
            ),
            163 => 
            array (
                'id_reg' => 664,
                'id_regmf' => 559,
                'doors' => 5,
            ),
            164 => 
            array (
                'id_reg' => 665,
                'id_regmf' => 560,
                'doors' => 5,
            ),
            165 => 
            array (
                'id_reg' => 666,
                'id_regmf' => 562,
                'doors' => 4,
            ),
            166 => 
            array (
                'id_reg' => 667,
                'id_regmf' => 561,
                'doors' => 5,
            ),
            167 => 
            array (
                'id_reg' => 668,
                'id_regmf' => 563,
                'doors' => 5,
            ),
            168 => 
            array (
                'id_reg' => 669,
                'id_regmf' => 564,
                'doors' => 3,
            ),
            169 => 
            array (
                'id_reg' => 670,
                'id_regmf' => 564,
                'doors' => 5,
            ),
            170 => 
            array (
                'id_reg' => 671,
                'id_regmf' => 565,
                'doors' => 5,
            ),
            171 => 
            array (
                'id_reg' => 672,
                'id_regmf' => 566,
                'doors' => 0,
            ),
            172 => 
            array (
                'id_reg' => 673,
                'id_regmf' => 577,
                'doors' => 4,
            ),
            173 => 
            array (
                'id_reg' => 674,
                'id_regmf' => 569,
                'doors' => 5,
            ),
            174 => 
            array (
                'id_reg' => 675,
                'id_regmf' => 570,
                'doors' => 4,
            ),
            175 => 
            array (
                'id_reg' => 676,
                'id_regmf' => 571,
                'doors' => 4,
            ),
            176 => 
            array (
                'id_reg' => 677,
                'id_regmf' => 567,
                'doors' => 4,
            ),
            177 => 
            array (
                'id_reg' => 678,
                'id_regmf' => 572,
                'doors' => 4,
            ),
            178 => 
            array (
                'id_reg' => 679,
                'id_regmf' => 573,
                'doors' => 2,
            ),
            179 => 
            array (
                'id_reg' => 680,
                'id_regmf' => 574,
                'doors' => 4,
            ),
            180 => 
            array (
                'id_reg' => 681,
                'id_regmf' => 574,
                'doors' => 5,
            ),
            181 => 
            array (
                'id_reg' => 682,
                'id_regmf' => 575,
                'doors' => 4,
            ),
            182 => 
            array (
                'id_reg' => 683,
                'id_regmf' => 575,
                'doors' => 5,
            ),
            183 => 
            array (
                'id_reg' => 684,
                'id_regmf' => 576,
                'doors' => 4,
            ),
            184 => 
            array (
                'id_reg' => 685,
                'id_regmf' => 568,
                'doors' => 4,
            ),
            185 => 
            array (
                'id_reg' => 686,
                'id_regmf' => 578,
                'doors' => 5,
            ),
            186 => 
            array (
                'id_reg' => 687,
                'id_regmf' => 579,
                'doors' => 5,
            ),
            187 => 
            array (
                'id_reg' => 688,
                'id_regmf' => 580,
                'doors' => 2,
            ),
            188 => 
            array (
                'id_reg' => 689,
                'id_regmf' => 581,
                'doors' => 4,
            ),
            189 => 
            array (
                'id_reg' => 690,
                'id_regmf' => 582,
                'doors' => 2,
            ),
            190 => 
            array (
                'id_reg' => 691,
                'id_regmf' => 582,
                'doors' => 3,
            ),
            191 => 
            array (
                'id_reg' => 692,
                'id_regmf' => 583,
                'doors' => 3,
            ),
            192 => 
            array (
                'id_reg' => 693,
                'id_regmf' => 584,
                'doors' => 4,
            ),
            193 => 
            array (
                'id_reg' => 694,
                'id_regmf' => 585,
                'doors' => 3,
            ),
            194 => 
            array (
                'id_reg' => 695,
                'id_regmf' => 586,
                'doors' => 4,
            ),
            195 => 
            array (
                'id_reg' => 696,
                'id_regmf' => 587,
                'doors' => 4,
            ),
            196 => 
            array (
                'id_reg' => 697,
                'id_regmf' => 587,
                'doors' => 5,
            ),
            197 => 
            array (
                'id_reg' => 698,
                'id_regmf' => 588,
                'doors' => 4,
            ),
            198 => 
            array (
                'id_reg' => 699,
                'id_regmf' => 588,
                'doors' => 5,
            ),
            199 => 
            array (
                'id_reg' => 700,
                'id_regmf' => 589,
                'doors' => 3,
            ),
            200 => 
            array (
                'id_reg' => 701,
                'id_regmf' => 590,
                'doors' => 2,
            ),
            201 => 
            array (
                'id_reg' => 702,
                'id_regmf' => 590,
                'doors' => 4,
            ),
            202 => 
            array (
                'id_reg' => 703,
                'id_regmf' => 591,
                'doors' => 2,
            ),
            203 => 
            array (
                'id_reg' => 704,
                'id_regmf' => 591,
                'doors' => 4,
            ),
            204 => 
            array (
                'id_reg' => 705,
                'id_regmf' => 592,
                'doors' => 4,
            ),
            205 => 
            array (
                'id_reg' => 706,
                'id_regmf' => 593,
                'doors' => 4,
            ),
            206 => 
            array (
                'id_reg' => 707,
                'id_regmf' => 594,
                'doors' => 4,
            ),
            207 => 
            array (
                'id_reg' => 708,
                'id_regmf' => 595,
                'doors' => 2,
            ),
            208 => 
            array (
                'id_reg' => 709,
                'id_regmf' => 595,
                'doors' => 4,
            ),
            209 => 
            array (
                'id_reg' => 710,
                'id_regmf' => 596,
                'doors' => 5,
            ),
            210 => 
            array (
                'id_reg' => 711,
                'id_regmf' => 597,
                'doors' => 2,
            ),
            211 => 
            array (
                'id_reg' => 712,
                'id_regmf' => 598,
                'doors' => 5,
            ),
            212 => 
            array (
                'id_reg' => 713,
                'id_regmf' => 599,
                'doors' => 2,
            ),
            213 => 
            array (
                'id_reg' => 714,
                'id_regmf' => 599,
                'doors' => 5,
            ),
            214 => 
            array (
                'id_reg' => 715,
                'id_regmf' => 600,
                'doors' => 4,
            ),
            215 => 
            array (
                'id_reg' => 716,
                'id_regmf' => 601,
                'doors' => 4,
            ),
            216 => 
            array (
                'id_reg' => 717,
                'id_regmf' => 602,
                'doors' => 2,
            ),
            217 => 
            array (
                'id_reg' => 718,
                'id_regmf' => 602,
                'doors' => 4,
            ),
            218 => 
            array (
                'id_reg' => 719,
                'id_regmf' => 603,
                'doors' => 2,
            ),
            219 => 
            array (
                'id_reg' => 720,
                'id_regmf' => 603,
                'doors' => 4,
            ),
            220 => 
            array (
                'id_reg' => 721,
                'id_regmf' => 604,
                'doors' => 4,
            ),
            221 => 
            array (
                'id_reg' => 722,
                'id_regmf' => 605,
                'doors' => 4,
            ),
            222 => 
            array (
                'id_reg' => 723,
                'id_regmf' => 606,
                'doors' => 4,
            ),
            223 => 
            array (
                'id_reg' => 724,
                'id_regmf' => 607,
                'doors' => 2,
            ),
            224 => 
            array (
                'id_reg' => 725,
                'id_regmf' => 607,
                'doors' => 4,
            ),
            225 => 
            array (
                'id_reg' => 726,
                'id_regmf' => 608,
                'doors' => 2,
            ),
            226 => 
            array (
                'id_reg' => 727,
                'id_regmf' => 609,
                'doors' => 2,
            ),
            227 => 
            array (
                'id_reg' => 728,
                'id_regmf' => 610,
                'doors' => 4,
            ),
            228 => 
            array (
                'id_reg' => 729,
                'id_regmf' => 610,
                'doors' => 5,
            ),
            229 => 
            array (
                'id_reg' => 730,
                'id_regmf' => 611,
                'doors' => 2,
            ),
            230 => 
            array (
                'id_reg' => 731,
                'id_regmf' => 612,
                'doors' => 4,
            ),
            231 => 
            array (
                'id_reg' => 732,
                'id_regmf' => 614,
                'doors' => 4,
            ),
            232 => 
            array (
                'id_reg' => 733,
                'id_regmf' => 614,
                'doors' => 5,
            ),
            233 => 
            array (
                'id_reg' => 734,
                'id_regmf' => 613,
                'doors' => 4,
            ),
            234 => 
            array (
                'id_reg' => 735,
                'id_regmf' => 613,
                'doors' => 5,
            ),
            235 => 
            array (
                'id_reg' => 736,
                'id_regmf' => 615,
                'doors' => 4,
            ),
            236 => 
            array (
                'id_reg' => 737,
                'id_regmf' => 618,
                'doors' => 4,
            ),
            237 => 
            array (
                'id_reg' => 738,
                'id_regmf' => 619,
                'doors' => 4,
            ),
            238 => 
            array (
                'id_reg' => 739,
                'id_regmf' => 616,
                'doors' => 2,
            ),
            239 => 
            array (
                'id_reg' => 740,
                'id_regmf' => 616,
                'doors' => 4,
            ),
            240 => 
            array (
                'id_reg' => 741,
                'id_regmf' => 617,
                'doors' => 2,
            ),
            241 => 
            array (
                'id_reg' => 742,
                'id_regmf' => 620,
                'doors' => 2,
            ),
            242 => 
            array (
                'id_reg' => 743,
                'id_regmf' => 621,
                'doors' => 4,
            ),
            243 => 
            array (
                'id_reg' => 744,
                'id_regmf' => 622,
                'doors' => 4,
            ),
            244 => 
            array (
                'id_reg' => 745,
                'id_regmf' => 623,
                'doors' => 5,
            ),
            245 => 
            array (
                'id_reg' => 746,
                'id_regmf' => 624,
                'doors' => 4,
            ),
            246 => 
            array (
                'id_reg' => 747,
                'id_regmf' => 624,
                'doors' => 5,
            ),
            247 => 
            array (
                'id_reg' => 748,
                'id_regmf' => 626,
                'doors' => 3,
            ),
            248 => 
            array (
                'id_reg' => 749,
                'id_regmf' => 626,
                'doors' => 5,
            ),
            249 => 
            array (
                'id_reg' => 750,
                'id_regmf' => 625,
                'doors' => 3,
            ),
            250 => 
            array (
                'id_reg' => 751,
                'id_regmf' => 625,
                'doors' => 5,
            ),
            251 => 
            array (
                'id_reg' => 752,
                'id_regmf' => 627,
                'doors' => 3,
            ),
            252 => 
            array (
                'id_reg' => 753,
                'id_regmf' => 628,
                'doors' => 4,
            ),
            253 => 
            array (
                'id_reg' => 754,
                'id_regmf' => 629,
                'doors' => 2,
            ),
            254 => 
            array (
                'id_reg' => 755,
                'id_regmf' => 629,
                'doors' => 4,
            ),
            255 => 
            array (
                'id_reg' => 756,
                'id_regmf' => 630,
                'doors' => 4,
            ),
            256 => 
            array (
                'id_reg' => 757,
                'id_regmf' => 632,
                'doors' => 4,
            ),
            257 => 
            array (
                'id_reg' => 758,
                'id_regmf' => 632,
                'doors' => 5,
            ),
            258 => 
            array (
                'id_reg' => 759,
                'id_regmf' => 632,
                'doors' => 6,
            ),
            259 => 
            array (
                'id_reg' => 760,
                'id_regmf' => 631,
                'doors' => 4,
            ),
            260 => 
            array (
                'id_reg' => 761,
                'id_regmf' => 631,
                'doors' => 5,
            ),
            261 => 
            array (
                'id_reg' => 762,
                'id_regmf' => 631,
                'doors' => 6,
            ),
            262 => 
            array (
                'id_reg' => 763,
                'id_regmf' => 634,
                'doors' => 5,
            ),
            263 => 
            array (
                'id_reg' => 764,
                'id_regmf' => 633,
                'doors' => 5,
            ),
            264 => 
            array (
                'id_reg' => 765,
                'id_regmf' => 635,
                'doors' => 4,
            ),
            265 => 
            array (
                'id_reg' => 766,
                'id_regmf' => 637,
                'doors' => 3,
            ),
            266 => 
            array (
                'id_reg' => 767,
                'id_regmf' => 637,
                'doors' => 4,
            ),
            267 => 
            array (
                'id_reg' => 768,
                'id_regmf' => 636,
                'doors' => 2,
            ),
            268 => 
            array (
                'id_reg' => 769,
                'id_regmf' => 636,
                'doors' => 3,
            ),
            269 => 
            array (
                'id_reg' => 770,
                'id_regmf' => 636,
                'doors' => 4,
            ),
            270 => 
            array (
                'id_reg' => 771,
                'id_regmf' => 638,
                'doors' => 2,
            ),
            271 => 
            array (
                'id_reg' => 772,
                'id_regmf' => 638,
                'doors' => 3,
            ),
            272 => 
            array (
                'id_reg' => 773,
                'id_regmf' => 638,
                'doors' => 4,
            ),
            273 => 
            array (
                'id_reg' => 774,
                'id_regmf' => 638,
                'doors' => 5,
            ),
            274 => 
            array (
                'id_reg' => 775,
                'id_regmf' => 638,
                'doors' => 6,
            ),
            275 => 
            array (
                'id_reg' => 776,
                'id_regmf' => 639,
                'doors' => 4,
            ),
            276 => 
            array (
                'id_reg' => 777,
                'id_regmf' => 640,
                'doors' => 4,
            ),
            277 => 
            array (
                'id_reg' => 778,
                'id_regmf' => 641,
                'doors' => 4,
            ),
            278 => 
            array (
                'id_reg' => 779,
                'id_regmf' => 642,
                'doors' => 4,
            ),
            279 => 
            array (
                'id_reg' => 780,
                'id_regmf' => 642,
                'doors' => 5,
            ),
            280 => 
            array (
                'id_reg' => 781,
                'id_regmf' => 644,
                'doors' => 5,
            ),
            281 => 
            array (
                'id_reg' => 782,
                'id_regmf' => 643,
                'doors' => 5,
            ),
            282 => 
            array (
                'id_reg' => 783,
                'id_regmf' => 646,
                'doors' => 4,
            ),
            283 => 
            array (
                'id_reg' => 784,
                'id_regmf' => 647,
                'doors' => 4,
            ),
            284 => 
            array (
                'id_reg' => 785,
                'id_regmf' => 645,
                'doors' => 4,
            ),
            285 => 
            array (
                'id_reg' => 786,
                'id_regmf' => 648,
                'doors' => 5,
            ),
            286 => 
            array (
                'id_reg' => 787,
                'id_regmf' => 650,
                'doors' => 3,
            ),
            287 => 
            array (
                'id_reg' => 788,
                'id_regmf' => 650,
                'doors' => 5,
            ),
            288 => 
            array (
                'id_reg' => 789,
                'id_regmf' => 649,
                'doors' => 3,
            ),
            289 => 
            array (
                'id_reg' => 790,
                'id_regmf' => 649,
                'doors' => 5,
            ),
            290 => 
            array (
                'id_reg' => 791,
                'id_regmf' => 652,
                'doors' => 5,
            ),
            291 => 
            array (
                'id_reg' => 792,
                'id_regmf' => 651,
                'doors' => 5,
            ),
            292 => 
            array (
                'id_reg' => 793,
                'id_regmf' => 654,
                'doors' => 2,
            ),
            293 => 
            array (
                'id_reg' => 794,
                'id_regmf' => 653,
                'doors' => 2,
            ),
            294 => 
            array (
                'id_reg' => 795,
                'id_regmf' => 656,
                'doors' => 5,
            ),
            295 => 
            array (
                'id_reg' => 796,
                'id_regmf' => 655,
                'doors' => 5,
            ),
            296 => 
            array (
                'id_reg' => 797,
                'id_regmf' => 657,
                'doors' => 5,
            ),
            297 => 
            array (
                'id_reg' => 798,
                'id_regmf' => 658,
                'doors' => 5,
            ),
            298 => 
            array (
                'id_reg' => 799,
                'id_regmf' => 660,
                'doors' => 5,
            ),
            299 => 
            array (
                'id_reg' => 800,
                'id_regmf' => 659,
                'doors' => 5,
            ),
            300 => 
            array (
                'id_reg' => 801,
                'id_regmf' => 661,
                'doors' => 5,
            ),
            301 => 
            array (
                'id_reg' => 802,
                'id_regmf' => 662,
                'doors' => 5,
            ),
            302 => 
            array (
                'id_reg' => 803,
                'id_regmf' => 663,
                'doors' => 2,
            ),
            303 => 
            array (
                'id_reg' => 804,
                'id_regmf' => 664,
                'doors' => 4,
            ),
            304 => 
            array (
                'id_reg' => 805,
                'id_regmf' => 665,
                'doors' => 4,
            ),
            305 => 
            array (
                'id_reg' => 806,
                'id_regmf' => 667,
                'doors' => 5,
            ),
            306 => 
            array (
                'id_reg' => 807,
                'id_regmf' => 666,
                'doors' => 5,
            ),
            307 => 
            array (
                'id_reg' => 808,
                'id_regmf' => 668,
                'doors' => 4,
            ),
            308 => 
            array (
                'id_reg' => 809,
                'id_regmf' => 668,
                'doors' => 5,
            ),
            309 => 
            array (
                'id_reg' => 810,
                'id_regmf' => 669,
                'doors' => 4,
            ),
            310 => 
            array (
                'id_reg' => 811,
                'id_regmf' => 669,
                'doors' => 5,
            ),
            311 => 
            array (
                'id_reg' => 812,
                'id_regmf' => 671,
                'doors' => 2,
            ),
            312 => 
            array (
                'id_reg' => 813,
                'id_regmf' => 671,
                'doors' => 3,
            ),
            313 => 
            array (
                'id_reg' => 814,
                'id_regmf' => 670,
                'doors' => 2,
            ),
            314 => 
            array (
                'id_reg' => 815,
                'id_regmf' => 670,
                'doors' => 3,
            ),
            315 => 
            array (
                'id_reg' => 816,
                'id_regmf' => 672,
                'doors' => 5,
            ),
            316 => 
            array (
                'id_reg' => 817,
                'id_regmf' => 673,
                'doors' => 5,
            ),
            317 => 
            array (
                'id_reg' => 818,
                'id_regmf' => 674,
                'doors' => 5,
            ),
            318 => 
            array (
                'id_reg' => 819,
                'id_regmf' => 675,
                'doors' => 5,
            ),
            319 => 
            array (
                'id_reg' => 820,
                'id_regmf' => 676,
                'doors' => 4,
            ),
            320 => 
            array (
                'id_reg' => 821,
                'id_regmf' => 676,
                'doors' => 5,
            ),
            321 => 
            array (
                'id_reg' => 822,
                'id_regmf' => 677,
                'doors' => 4,
            ),
            322 => 
            array (
                'id_reg' => 823,
                'id_regmf' => 678,
                'doors' => 5,
            ),
            323 => 
            array (
                'id_reg' => 824,
                'id_regmf' => 679,
                'doors' => 3,
            ),
            324 => 
            array (
                'id_reg' => 825,
                'id_regmf' => 680,
                'doors' => 5,
            ),
            325 => 
            array (
                'id_reg' => 826,
                'id_regmf' => 681,
                'doors' => 5,
            ),
            326 => 
            array (
                'id_reg' => 827,
                'id_regmf' => 682,
                'doors' => 5,
            ),
            327 => 
            array (
                'id_reg' => 828,
                'id_regmf' => 683,
                'doors' => 5,
            ),
            328 => 
            array (
                'id_reg' => 829,
                'id_regmf' => 684,
                'doors' => 2,
            ),
            329 => 
            array (
                'id_reg' => 830,
                'id_regmf' => 686,
                'doors' => 4,
            ),
            330 => 
            array (
                'id_reg' => 831,
                'id_regmf' => 685,
                'doors' => 2,
            ),
            331 => 
            array (
                'id_reg' => 832,
                'id_regmf' => 688,
                'doors' => 5,
            ),
            332 => 
            array (
                'id_reg' => 833,
                'id_regmf' => 687,
                'doors' => 2,
            ),
            333 => 
            array (
                'id_reg' => 834,
                'id_regmf' => 687,
                'doors' => 3,
            ),
            334 => 
            array (
                'id_reg' => 835,
                'id_regmf' => 687,
                'doors' => 4,
            ),
            335 => 
            array (
                'id_reg' => 836,
                'id_regmf' => 687,
                'doors' => 5,
            ),
            336 => 
            array (
                'id_reg' => 837,
                'id_regmf' => 690,
                'doors' => 5,
            ),
            337 => 
            array (
                'id_reg' => 838,
                'id_regmf' => 689,
                'doors' => 2,
            ),
            338 => 
            array (
                'id_reg' => 839,
                'id_regmf' => 689,
                'doors' => 3,
            ),
            339 => 
            array (
                'id_reg' => 840,
                'id_regmf' => 689,
                'doors' => 4,
            ),
            340 => 
            array (
                'id_reg' => 841,
                'id_regmf' => 689,
                'doors' => 5,
            ),
            341 => 
            array (
                'id_reg' => 842,
                'id_regmf' => 691,
                'doors' => 3,
            ),
            342 => 
            array (
                'id_reg' => 843,
                'id_regmf' => 692,
                'doors' => 2,
            ),
            343 => 
            array (
                'id_reg' => 844,
                'id_regmf' => 694,
                'doors' => 4,
            ),
            344 => 
            array (
                'id_reg' => 845,
                'id_regmf' => 694,
                'doors' => 5,
            ),
            345 => 
            array (
                'id_reg' => 846,
                'id_regmf' => 694,
                'doors' => 6,
            ),
            346 => 
            array (
                'id_reg' => 847,
                'id_regmf' => 693,
                'doors' => 4,
            ),
            347 => 
            array (
                'id_reg' => 848,
                'id_regmf' => 693,
                'doors' => 5,
            ),
            348 => 
            array (
                'id_reg' => 849,
                'id_regmf' => 693,
                'doors' => 6,
            ),
            349 => 
            array (
                'id_reg' => 850,
                'id_regmf' => 695,
                'doors' => 5,
            ),
            350 => 
            array (
                'id_reg' => 851,
                'id_regmf' => 696,
                'doors' => 2,
            ),
            351 => 
            array (
                'id_reg' => 852,
                'id_regmf' => 696,
                'doors' => 4,
            ),
            352 => 
            array (
                'id_reg' => 853,
                'id_regmf' => 698,
                'doors' => 5,
            ),
            353 => 
            array (
                'id_reg' => 854,
                'id_regmf' => 697,
                'doors' => 5,
            ),
            354 => 
            array (
                'id_reg' => 855,
                'id_regmf' => 699,
                'doors' => 5,
            ),
            355 => 
            array (
                'id_reg' => 856,
                'id_regmf' => 700,
                'doors' => 4,
            ),
            356 => 
            array (
                'id_reg' => 857,
                'id_regmf' => 701,
                'doors' => 2,
            ),
            357 => 
            array (
                'id_reg' => 858,
                'id_regmf' => 701,
                'doors' => 3,
            ),
            358 => 
            array (
                'id_reg' => 859,
                'id_regmf' => 701,
                'doors' => 4,
            ),
            359 => 
            array (
                'id_reg' => 860,
                'id_regmf' => 702,
                'doors' => 4,
            ),
            360 => 
            array (
                'id_reg' => 861,
                'id_regmf' => 704,
                'doors' => 5,
            ),
            361 => 
            array (
                'id_reg' => 862,
                'id_regmf' => 703,
                'doors' => 5,
            ),
            362 => 
            array (
                'id_reg' => 863,
                'id_regmf' => 705,
                'doors' => 5,
            ),
            363 => 
            array (
                'id_reg' => 864,
                'id_regmf' => 706,
                'doors' => 5,
            ),
            364 => 
            array (
                'id_reg' => 865,
                'id_regmf' => 707,
                'doors' => 2,
            ),
            365 => 
            array (
                'id_reg' => 866,
                'id_regmf' => 708,
                'doors' => 2,
            ),
            366 => 
            array (
                'id_reg' => 867,
                'id_regmf' => 709,
                'doors' => 0,
            ),
            367 => 
            array (
                'id_reg' => 868,
                'id_regmf' => 712,
                'doors' => 0,
            ),
            368 => 
            array (
                'id_reg' => 869,
                'id_regmf' => 710,
                'doors' => 0,
            ),
            369 => 
            array (
                'id_reg' => 870,
                'id_regmf' => 711,
                'doors' => 0,
            ),
            370 => 
            array (
                'id_reg' => 871,
                'id_regmf' => 713,
                'doors' => 0,
            ),
            371 => 
            array (
                'id_reg' => 872,
                'id_regmf' => 714,
                'doors' => 0,
            ),
            372 => 
            array (
                'id_reg' => 873,
                'id_regmf' => 715,
                'doors' => 0,
            ),
            373 => 
            array (
                'id_reg' => 874,
                'id_regmf' => 716,
                'doors' => 0,
            ),
            374 => 
            array (
                'id_reg' => 875,
                'id_regmf' => 717,
                'doors' => 0,
            ),
            375 => 
            array (
                'id_reg' => 876,
                'id_regmf' => 718,
                'doors' => 0,
            ),
            376 => 
            array (
                'id_reg' => 877,
                'id_regmf' => 719,
                'doors' => 0,
            ),
            377 => 
            array (
                'id_reg' => 878,
                'id_regmf' => 720,
                'doors' => 0,
            ),
            378 => 
            array (
                'id_reg' => 879,
                'id_regmf' => 721,
                'doors' => 2,
            ),
            379 => 
            array (
                'id_reg' => 880,
                'id_regmf' => 722,
                'doors' => 0,
            ),
            380 => 
            array (
                'id_reg' => 881,
                'id_regmf' => 723,
                'doors' => 0,
            ),
            381 => 
            array (
                'id_reg' => 882,
                'id_regmf' => 724,
                'doors' => 0,
            ),
            382 => 
            array (
                'id_reg' => 883,
                'id_regmf' => 725,
                'doors' => 0,
            ),
            383 => 
            array (
                'id_reg' => 884,
                'id_regmf' => 726,
                'doors' => 2,
            ),
            384 => 
            array (
                'id_reg' => 885,
                'id_regmf' => 727,
                'doors' => 2,
            ),
            385 => 
            array (
                'id_reg' => 886,
                'id_regmf' => 728,
                'doors' => 2,
            ),
            386 => 
            array (
                'id_reg' => 887,
                'id_regmf' => 729,
                'doors' => 0,
            ),
            387 => 
            array (
                'id_reg' => 888,
                'id_regmf' => 730,
                'doors' => 2,
            ),
            388 => 
            array (
                'id_reg' => 889,
                'id_regmf' => 731,
                'doors' => 2,
            ),
            389 => 
            array (
                'id_reg' => 890,
                'id_regmf' => 732,
                'doors' => 2,
            ),
            390 => 
            array (
                'id_reg' => 891,
                'id_regmf' => 733,
                'doors' => 3,
            ),
            391 => 
            array (
                'id_reg' => 892,
                'id_regmf' => 734,
                'doors' => 2,
            ),
            392 => 
            array (
                'id_reg' => 893,
                'id_regmf' => 734,
                'doors' => 3,
            ),
            393 => 
            array (
                'id_reg' => 894,
                'id_regmf' => 735,
                'doors' => 0,
            ),
            394 => 
            array (
                'id_reg' => 895,
                'id_regmf' => 736,
                'doors' => 0,
            ),
            395 => 
            array (
                'id_reg' => 896,
                'id_regmf' => 737,
                'doors' => 0,
            ),
            396 => 
            array (
                'id_reg' => 897,
                'id_regmf' => 738,
                'doors' => 0,
            ),
            397 => 
            array (
                'id_reg' => 898,
                'id_regmf' => 739,
                'doors' => 0,
            ),
            398 => 
            array (
                'id_reg' => 899,
                'id_regmf' => 740,
                'doors' => 0,
            ),
            399 => 
            array (
                'id_reg' => 900,
                'id_regmf' => 741,
                'doors' => 2,
            ),
            400 => 
            array (
                'id_reg' => 901,
                'id_regmf' => 742,
                'doors' => 4,
            ),
            401 => 
            array (
                'id_reg' => 902,
                'id_regmf' => 742,
                'doors' => 5,
            ),
            402 => 
            array (
                'id_reg' => 903,
                'id_regmf' => 744,
                'doors' => 5,
            ),
            403 => 
            array (
                'id_reg' => 904,
                'id_regmf' => 746,
                'doors' => 4,
            ),
            404 => 
            array (
                'id_reg' => 905,
                'id_regmf' => 746,
                'doors' => 5,
            ),
            405 => 
            array (
                'id_reg' => 906,
                'id_regmf' => 745,
                'doors' => 4,
            ),
            406 => 
            array (
                'id_reg' => 907,
                'id_regmf' => 745,
                'doors' => 5,
            ),
            407 => 
            array (
                'id_reg' => 908,
                'id_regmf' => 743,
                'doors' => 4,
            ),
            408 => 
            array (
                'id_reg' => 909,
                'id_regmf' => 743,
                'doors' => 5,
            ),
            409 => 
            array (
                'id_reg' => 910,
                'id_regmf' => 747,
                'doors' => 4,
            ),
            410 => 
            array (
                'id_reg' => 911,
                'id_regmf' => 748,
                'doors' => 5,
            ),
            411 => 
            array (
                'id_reg' => 912,
                'id_regmf' => 750,
                'doors' => 5,
            ),
            412 => 
            array (
                'id_reg' => 913,
                'id_regmf' => 749,
                'doors' => 5,
            ),
            413 => 
            array (
                'id_reg' => 914,
                'id_regmf' => 751,
                'doors' => 5,
            ),
            414 => 
            array (
                'id_reg' => 915,
                'id_regmf' => 753,
                'doors' => 5,
            ),
            415 => 
            array (
                'id_reg' => 916,
                'id_regmf' => 752,
                'doors' => 5,
            ),
            416 => 
            array (
                'id_reg' => 917,
                'id_regmf' => 754,
                'doors' => 5,
            ),
            417 => 
            array (
                'id_reg' => 918,
                'id_regmf' => 758,
                'doors' => 4,
            ),
            418 => 
            array (
                'id_reg' => 919,
                'id_regmf' => 758,
                'doors' => 5,
            ),
            419 => 
            array (
                'id_reg' => 920,
                'id_regmf' => 758,
                'doors' => 6,
            ),
            420 => 
            array (
                'id_reg' => 921,
                'id_regmf' => 757,
                'doors' => 4,
            ),
            421 => 
            array (
                'id_reg' => 922,
                'id_regmf' => 757,
                'doors' => 5,
            ),
            422 => 
            array (
                'id_reg' => 923,
                'id_regmf' => 755,
                'doors' => 5,
            ),
            423 => 
            array (
                'id_reg' => 924,
                'id_regmf' => 756,
                'doors' => 4,
            ),
            424 => 
            array (
                'id_reg' => 925,
                'id_regmf' => 756,
                'doors' => 5,
            ),
            425 => 
            array (
                'id_reg' => 926,
                'id_regmf' => 756,
                'doors' => 6,
            ),
            426 => 
            array (
                'id_reg' => 927,
                'id_regmf' => 761,
                'doors' => 5,
            ),
            427 => 
            array (
                'id_reg' => 928,
                'id_regmf' => 760,
                'doors' => 5,
            ),
            428 => 
            array (
                'id_reg' => 929,
                'id_regmf' => 759,
                'doors' => 5,
            ),
            429 => 
            array (
                'id_reg' => 930,
                'id_regmf' => 763,
                'doors' => 5,
            ),
            430 => 
            array (
                'id_reg' => 931,
                'id_regmf' => 762,
                'doors' => 5,
            ),
            431 => 
            array (
                'id_reg' => 932,
                'id_regmf' => 764,
                'doors' => 4,
            ),
            432 => 
            array (
                'id_reg' => 933,
                'id_regmf' => 765,
                'doors' => 4,
            ),
            433 => 
            array (
                'id_reg' => 934,
                'id_regmf' => 766,
                'doors' => 5,
            ),
            434 => 
            array (
                'id_reg' => 935,
                'id_regmf' => 767,
                'doors' => 5,
            ),
            435 => 
            array (
                'id_reg' => 936,
                'id_regmf' => 768,
                'doors' => 4,
            ),
            436 => 
            array (
                'id_reg' => 937,
                'id_regmf' => 769,
                'doors' => 4,
            ),
            437 => 
            array (
                'id_reg' => 938,
                'id_regmf' => 770,
                'doors' => 4,
            ),
            438 => 
            array (
                'id_reg' => 939,
                'id_regmf' => 770,
                'doors' => 5,
            ),
            439 => 
            array (
                'id_reg' => 940,
                'id_regmf' => 771,
                'doors' => 3,
            ),
            440 => 
            array (
                'id_reg' => 941,
                'id_regmf' => 772,
                'doors' => 5,
            ),
            441 => 
            array (
                'id_reg' => 942,
                'id_regmf' => 773,
                'doors' => 3,
            ),
            442 => 
            array (
                'id_reg' => 943,
                'id_regmf' => 773,
                'doors' => 4,
            ),
            443 => 
            array (
                'id_reg' => 944,
                'id_regmf' => 773,
                'doors' => 5,
            ),
            444 => 
            array (
                'id_reg' => 945,
                'id_regmf' => 774,
                'doors' => 4,
            ),
            445 => 
            array (
                'id_reg' => 946,
                'id_regmf' => 775,
                'doors' => 5,
            ),
            446 => 
            array (
                'id_reg' => 947,
                'id_regmf' => 776,
                'doors' => 5,
            ),
            447 => 
            array (
                'id_reg' => 948,
                'id_regmf' => 777,
                'doors' => 3,
            ),
            448 => 
            array (
                'id_reg' => 949,
                'id_regmf' => 777,
                'doors' => 4,
            ),
            449 => 
            array (
                'id_reg' => 950,
                'id_regmf' => 777,
                'doors' => 5,
            ),
            450 => 
            array (
                'id_reg' => 951,
                'id_regmf' => 778,
                'doors' => 4,
            ),
            451 => 
            array (
                'id_reg' => 952,
                'id_regmf' => 778,
                'doors' => 5,
            ),
            452 => 
            array (
                'id_reg' => 953,
                'id_regmf' => 779,
                'doors' => 5,
            ),
            453 => 
            array (
                'id_reg' => 954,
                'id_regmf' => 780,
                'doors' => 5,
            ),
            454 => 
            array (
                'id_reg' => 955,
                'id_regmf' => 781,
                'doors' => 5,
            ),
            455 => 
            array (
                'id_reg' => 956,
                'id_regmf' => 782,
                'doors' => 2,
            ),
            456 => 
            array (
                'id_reg' => 957,
                'id_regmf' => 783,
                'doors' => 2,
            ),
            457 => 
            array (
                'id_reg' => 958,
                'id_regmf' => 784,
                'doors' => 5,
            ),
            458 => 
            array (
                'id_reg' => 959,
                'id_regmf' => 785,
                'doors' => 2,
            ),
            459 => 
            array (
                'id_reg' => 960,
                'id_regmf' => 786,
                'doors' => 2,
            ),
            460 => 
            array (
                'id_reg' => 961,
                'id_regmf' => 787,
                'doors' => 3,
            ),
            461 => 
            array (
                'id_reg' => 962,
                'id_regmf' => 788,
                'doors' => 5,
            ),
            462 => 
            array (
                'id_reg' => 963,
                'id_regmf' => 789,
                'doors' => 2,
            ),
            463 => 
            array (
                'id_reg' => 964,
                'id_regmf' => 790,
                'doors' => 5,
            ),
            464 => 
            array (
                'id_reg' => 965,
                'id_regmf' => 791,
                'doors' => 5,
            ),
            465 => 
            array (
                'id_reg' => 966,
                'id_regmf' => 792,
                'doors' => 5,
            ),
            466 => 
            array (
                'id_reg' => 967,
                'id_regmf' => 793,
                'doors' => 2,
            ),
            467 => 
            array (
                'id_reg' => 968,
                'id_regmf' => 794,
                'doors' => 4,
            ),
            468 => 
            array (
                'id_reg' => 969,
                'id_regmf' => 795,
                'doors' => 3,
            ),
            469 => 
            array (
                'id_reg' => 970,
                'id_regmf' => 795,
                'doors' => 5,
            ),
            470 => 
            array (
                'id_reg' => 971,
                'id_regmf' => 796,
                'doors' => 3,
            ),
            471 => 
            array (
                'id_reg' => 972,
                'id_regmf' => 796,
                'doors' => 4,
            ),
            472 => 
            array (
                'id_reg' => 973,
                'id_regmf' => 796,
                'doors' => 5,
            ),
            473 => 
            array (
                'id_reg' => 974,
                'id_regmf' => 797,
                'doors' => 4,
            ),
            474 => 
            array (
                'id_reg' => 975,
                'id_regmf' => 798,
                'doors' => 2,
            ),
            475 => 
            array (
                'id_reg' => 976,
                'id_regmf' => 799,
                'doors' => 3,
            ),
            476 => 
            array (
                'id_reg' => 977,
                'id_regmf' => 799,
                'doors' => 5,
            ),
            477 => 
            array (
                'id_reg' => 978,
                'id_regmf' => 800,
                'doors' => 2,
            ),
            478 => 
            array (
                'id_reg' => 979,
                'id_regmf' => 801,
                'doors' => 3,
            ),
            479 => 
            array (
                'id_reg' => 980,
                'id_regmf' => 802,
                'doors' => 3,
            ),
            480 => 
            array (
                'id_reg' => 981,
                'id_regmf' => 803,
                'doors' => 3,
            ),
            481 => 
            array (
                'id_reg' => 982,
                'id_regmf' => 804,
                'doors' => 3,
            ),
            482 => 
            array (
                'id_reg' => 983,
                'id_regmf' => 805,
                'doors' => 3,
            ),
            483 => 
            array (
                'id_reg' => 984,
                'id_regmf' => 806,
                'doors' => 3,
            ),
            484 => 
            array (
                'id_reg' => 985,
                'id_regmf' => 807,
                'doors' => 5,
            ),
            485 => 
            array (
                'id_reg' => 986,
                'id_regmf' => 808,
                'doors' => 4,
            ),
            486 => 
            array (
                'id_reg' => 987,
                'id_regmf' => 809,
                'doors' => 5,
            ),
            487 => 
            array (
                'id_reg' => 988,
                'id_regmf' => 810,
                'doors' => 5,
            ),
            488 => 
            array (
                'id_reg' => 989,
                'id_regmf' => 811,
                'doors' => 3,
            ),
            489 => 
            array (
                'id_reg' => 990,
                'id_regmf' => 812,
                'doors' => 3,
            ),
            490 => 
            array (
                'id_reg' => 991,
                'id_regmf' => 813,
                'doors' => 5,
            ),
            491 => 
            array (
                'id_reg' => 992,
                'id_regmf' => 815,
                'doors' => 3,
            ),
            492 => 
            array (
                'id_reg' => 993,
                'id_regmf' => 814,
                'doors' => 3,
            ),
            493 => 
            array (
                'id_reg' => 994,
                'id_regmf' => 816,
                'doors' => 5,
            ),
            494 => 
            array (
                'id_reg' => 995,
                'id_regmf' => 817,
                'doors' => 5,
            ),
            495 => 
            array (
                'id_reg' => 996,
                'id_regmf' => 818,
                'doors' => 5,
            ),
            496 => 
            array (
                'id_reg' => 997,
                'id_regmf' => 819,
                'doors' => 3,
            ),
            497 => 
            array (
                'id_reg' => 998,
                'id_regmf' => 820,
                'doors' => 3,
            ),
            498 => 
            array (
                'id_reg' => 999,
                'id_regmf' => 821,
                'doors' => 5,
            ),
            499 => 
            array (
                'id_reg' => 1000,
                'id_regmf' => 822,
                'doors' => 2,
            ),
        ));
        \DB::table('model_fuel_numdoors')->insert(array (
            0 => 
            array (
                'id_reg' => 1001,
                'id_regmf' => 822,
                'doors' => 4,
            ),
            1 => 
            array (
                'id_reg' => 1002,
                'id_regmf' => 823,
                'doors' => 4,
            ),
            2 => 
            array (
                'id_reg' => 1003,
                'id_regmf' => 824,
                'doors' => 4,
            ),
            3 => 
            array (
                'id_reg' => 1004,
                'id_regmf' => 825,
                'doors' => 2,
            ),
            4 => 
            array (
                'id_reg' => 1005,
                'id_regmf' => 826,
                'doors' => 2,
            ),
            5 => 
            array (
                'id_reg' => 1006,
                'id_regmf' => 833,
                'doors' => 3,
            ),
            6 => 
            array (
                'id_reg' => 1007,
                'id_regmf' => 827,
                'doors' => 3,
            ),
            7 => 
            array (
                'id_reg' => 1008,
                'id_regmf' => 828,
                'doors' => 3,
            ),
            8 => 
            array (
                'id_reg' => 1009,
                'id_regmf' => 829,
                'doors' => 3,
            ),
            9 => 
            array (
                'id_reg' => 1010,
                'id_regmf' => 830,
                'doors' => 3,
            ),
            10 => 
            array (
                'id_reg' => 1011,
                'id_regmf' => 831,
                'doors' => 3,
            ),
            11 => 
            array (
                'id_reg' => 1012,
                'id_regmf' => 832,
                'doors' => 3,
            ),
            12 => 
            array (
                'id_reg' => 1013,
                'id_regmf' => 832,
                'doors' => 4,
            ),
            13 => 
            array (
                'id_reg' => 1014,
                'id_regmf' => 835,
                'doors' => 4,
            ),
            14 => 
            array (
                'id_reg' => 1015,
                'id_regmf' => 834,
                'doors' => 5,
            ),
            15 => 
            array (
                'id_reg' => 1016,
                'id_regmf' => 836,
                'doors' => 4,
            ),
            16 => 
            array (
                'id_reg' => 1017,
                'id_regmf' => 837,
                'doors' => 2,
            ),
            17 => 
            array (
                'id_reg' => 1018,
                'id_regmf' => 837,
                'doors' => 3,
            ),
            18 => 
            array (
                'id_reg' => 1019,
                'id_regmf' => 839,
                'doors' => 5,
            ),
            19 => 
            array (
                'id_reg' => 1020,
                'id_regmf' => 838,
                'doors' => 3,
            ),
            20 => 
            array (
                'id_reg' => 1021,
                'id_regmf' => 841,
                'doors' => 2,
            ),
            21 => 
            array (
                'id_reg' => 1022,
                'id_regmf' => 840,
                'doors' => 2,
            ),
            22 => 
            array (
                'id_reg' => 1023,
                'id_regmf' => 840,
                'doors' => 4,
            ),
            23 => 
            array (
                'id_reg' => 1024,
                'id_regmf' => 842,
                'doors' => 3,
            ),
            24 => 
            array (
                'id_reg' => 1025,
                'id_regmf' => 843,
                'doors' => 3,
            ),
            25 => 
            array (
                'id_reg' => 1026,
                'id_regmf' => 844,
                'doors' => 3,
            ),
            26 => 
            array (
                'id_reg' => 1027,
                'id_regmf' => 844,
                'doors' => 5,
            ),
            27 => 
            array (
                'id_reg' => 1028,
                'id_regmf' => 845,
                'doors' => 0,
            ),
            28 => 
            array (
                'id_reg' => 1029,
                'id_regmf' => 846,
                'doors' => 4,
            ),
            29 => 
            array (
                'id_reg' => 1030,
                'id_regmf' => 847,
                'doors' => 0,
            ),
            30 => 
            array (
                'id_reg' => 1031,
                'id_regmf' => 847,
                'doors' => 2,
            ),
            31 => 
            array (
                'id_reg' => 1032,
                'id_regmf' => 848,
                'doors' => 2,
            ),
            32 => 
            array (
                'id_reg' => 1033,
                'id_regmf' => 849,
                'doors' => 0,
            ),
            33 => 
            array (
                'id_reg' => 1034,
                'id_regmf' => 850,
                'doors' => 0,
            ),
            34 => 
            array (
                'id_reg' => 1035,
                'id_regmf' => 851,
                'doors' => 0,
            ),
            35 => 
            array (
                'id_reg' => 1036,
                'id_regmf' => 852,
                'doors' => 2,
            ),
            36 => 
            array (
                'id_reg' => 1037,
                'id_regmf' => 853,
                'doors' => 2,
            ),
            37 => 
            array (
                'id_reg' => 1038,
                'id_regmf' => 854,
                'doors' => 2,
            ),
            38 => 
            array (
                'id_reg' => 1039,
                'id_regmf' => 855,
                'doors' => 2,
            ),
            39 => 
            array (
                'id_reg' => 1040,
                'id_regmf' => 856,
                'doors' => 2,
            ),
            40 => 
            array (
                'id_reg' => 1041,
                'id_regmf' => 857,
                'doors' => 2,
            ),
            41 => 
            array (
                'id_reg' => 1042,
                'id_regmf' => 858,
                'doors' => 2,
            ),
            42 => 
            array (
                'id_reg' => 1043,
                'id_regmf' => 859,
                'doors' => 2,
            ),
            43 => 
            array (
                'id_reg' => 1044,
                'id_regmf' => 860,
                'doors' => 4,
            ),
            44 => 
            array (
                'id_reg' => 1045,
                'id_regmf' => 861,
                'doors' => 2,
            ),
            45 => 
            array (
                'id_reg' => 1046,
                'id_regmf' => 862,
                'doors' => 4,
            ),
            46 => 
            array (
                'id_reg' => 1047,
                'id_regmf' => 863,
                'doors' => 0,
            ),
            47 => 
            array (
                'id_reg' => 1048,
                'id_regmf' => 864,
                'doors' => 3,
            ),
            48 => 
            array (
                'id_reg' => 1049,
                'id_regmf' => 865,
                'doors' => 0,
            ),
            49 => 
            array (
                'id_reg' => 1050,
                'id_regmf' => 866,
                'doors' => 0,
            ),
            50 => 
            array (
                'id_reg' => 1051,
                'id_regmf' => 867,
                'doors' => 2,
            ),
            51 => 
            array (
                'id_reg' => 1052,
                'id_regmf' => 868,
                'doors' => 3,
            ),
            52 => 
            array (
                'id_reg' => 1053,
                'id_regmf' => 869,
                'doors' => 0,
            ),
            53 => 
            array (
                'id_reg' => 1054,
                'id_regmf' => 870,
                'doors' => 0,
            ),
            54 => 
            array (
                'id_reg' => 1055,
                'id_regmf' => 871,
                'doors' => 2,
            ),
            55 => 
            array (
                'id_reg' => 1056,
                'id_regmf' => 872,
                'doors' => 0,
            ),
            56 => 
            array (
                'id_reg' => 1057,
                'id_regmf' => 873,
                'doors' => 2,
            ),
            57 => 
            array (
                'id_reg' => 1058,
                'id_regmf' => 875,
                'doors' => 3,
            ),
            58 => 
            array (
                'id_reg' => 1059,
                'id_regmf' => 874,
                'doors' => 0,
            ),
            59 => 
            array (
                'id_reg' => 1060,
                'id_regmf' => 874,
                'doors' => 5,
            ),
            60 => 
            array (
                'id_reg' => 1061,
                'id_regmf' => 874,
                'doors' => 6,
            ),
            61 => 
            array (
                'id_reg' => 1062,
                'id_regmf' => 876,
                'doors' => 4,
            ),
            62 => 
            array (
                'id_reg' => 1063,
                'id_regmf' => 877,
                'doors' => 2,
            ),
            63 => 
            array (
                'id_reg' => 1064,
                'id_regmf' => 878,
                'doors' => 0,
            ),
            64 => 
            array (
                'id_reg' => 1065,
                'id_regmf' => 879,
                'doors' => 4,
            ),
            65 => 
            array (
                'id_reg' => 1066,
                'id_regmf' => 880,
                'doors' => 2,
            ),
            66 => 
            array (
                'id_reg' => 1067,
                'id_regmf' => 881,
                'doors' => 2,
            ),
            67 => 
            array (
                'id_reg' => 1068,
                'id_regmf' => 882,
                'doors' => 4,
            ),
            68 => 
            array (
                'id_reg' => 1069,
                'id_regmf' => 884,
                'doors' => 2,
            ),
            69 => 
            array (
                'id_reg' => 1070,
                'id_regmf' => 884,
                'doors' => 4,
            ),
            70 => 
            array (
                'id_reg' => 1071,
                'id_regmf' => 883,
                'doors' => 4,
            ),
            71 => 
            array (
                'id_reg' => 1072,
                'id_regmf' => 885,
                'doors' => 5,
            ),
            72 => 
            array (
                'id_reg' => 1073,
                'id_regmf' => 886,
                'doors' => 4,
            ),
            73 => 
            array (
                'id_reg' => 1074,
                'id_regmf' => 887,
                'doors' => 5,
            ),
            74 => 
            array (
                'id_reg' => 1075,
                'id_regmf' => 888,
                'doors' => 5,
            ),
            75 => 
            array (
                'id_reg' => 1076,
                'id_regmf' => 889,
                'doors' => 4,
            ),
            76 => 
            array (
                'id_reg' => 1077,
                'id_regmf' => 889,
                'doors' => 5,
            ),
            77 => 
            array (
                'id_reg' => 1078,
                'id_regmf' => 890,
                'doors' => 2,
            ),
            78 => 
            array (
                'id_reg' => 1079,
                'id_regmf' => 891,
                'doors' => 2,
            ),
            79 => 
            array (
                'id_reg' => 1080,
                'id_regmf' => 891,
                'doors' => 4,
            ),
            80 => 
            array (
                'id_reg' => 1081,
                'id_regmf' => 892,
                'doors' => 5,
            ),
            81 => 
            array (
                'id_reg' => 1082,
                'id_regmf' => 893,
                'doors' => 2,
            ),
            82 => 
            array (
                'id_reg' => 1083,
                'id_regmf' => 894,
                'doors' => 2,
            ),
            83 => 
            array (
                'id_reg' => 1084,
                'id_regmf' => 894,
                'doors' => 4,
            ),
            84 => 
            array (
                'id_reg' => 1085,
                'id_regmf' => 895,
                'doors' => 2,
            ),
            85 => 
            array (
                'id_reg' => 1086,
                'id_regmf' => 895,
                'doors' => 4,
            ),
            86 => 
            array (
                'id_reg' => 1087,
                'id_regmf' => 896,
                'doors' => 4,
            ),
            87 => 
            array (
                'id_reg' => 1088,
                'id_regmf' => 897,
                'doors' => 2,
            ),
            88 => 
            array (
                'id_reg' => 1089,
                'id_regmf' => 897,
                'doors' => 4,
            ),
            89 => 
            array (
                'id_reg' => 1090,
                'id_regmf' => 898,
                'doors' => 3,
            ),
            90 => 
            array (
                'id_reg' => 1091,
                'id_regmf' => 899,
                'doors' => 5,
            ),
            91 => 
            array (
                'id_reg' => 1092,
                'id_regmf' => 900,
                'doors' => 5,
            ),
            92 => 
            array (
                'id_reg' => 1093,
                'id_regmf' => 901,
                'doors' => 4,
            ),
            93 => 
            array (
                'id_reg' => 1094,
                'id_regmf' => 902,
                'doors' => 4,
            ),
            94 => 
            array (
                'id_reg' => 1095,
                'id_regmf' => 903,
                'doors' => 5,
            ),
            95 => 
            array (
                'id_reg' => 1096,
                'id_regmf' => 904,
                'doors' => 5,
            ),
            96 => 
            array (
                'id_reg' => 1097,
                'id_regmf' => 905,
                'doors' => 2,
            ),
            97 => 
            array (
                'id_reg' => 1098,
                'id_regmf' => 906,
                'doors' => 4,
            ),
            98 => 
            array (
                'id_reg' => 1099,
                'id_regmf' => 907,
                'doors' => 5,
            ),
            99 => 
            array (
                'id_reg' => 1100,
                'id_regmf' => 908,
                'doors' => 5,
            ),
            100 => 
            array (
                'id_reg' => 1101,
                'id_regmf' => 909,
                'doors' => 4,
            ),
            101 => 
            array (
                'id_reg' => 1102,
                'id_regmf' => 910,
                'doors' => 5,
            ),
            102 => 
            array (
                'id_reg' => 1103,
                'id_regmf' => 911,
                'doors' => 4,
            ),
            103 => 
            array (
                'id_reg' => 1104,
                'id_regmf' => 912,
                'doors' => 4,
            ),
            104 => 
            array (
                'id_reg' => 1105,
                'id_regmf' => 912,
                'doors' => 5,
            ),
            105 => 
            array (
                'id_reg' => 1106,
                'id_regmf' => 913,
                'doors' => 4,
            ),
            106 => 
            array (
                'id_reg' => 1107,
                'id_regmf' => 914,
                'doors' => 5,
            ),
            107 => 
            array (
                'id_reg' => 1108,
                'id_regmf' => 915,
                'doors' => 5,
            ),
            108 => 
            array (
                'id_reg' => 1109,
                'id_regmf' => 916,
                'doors' => 5,
            ),
            109 => 
            array (
                'id_reg' => 1110,
                'id_regmf' => 917,
                'doors' => 4,
            ),
            110 => 
            array (
                'id_reg' => 1111,
                'id_regmf' => 918,
                'doors' => 2,
            ),
            111 => 
            array (
                'id_reg' => 1112,
                'id_regmf' => 918,
                'doors' => 4,
            ),
            112 => 
            array (
                'id_reg' => 1113,
                'id_regmf' => 918,
                'doors' => 5,
            ),
            113 => 
            array (
                'id_reg' => 1114,
                'id_regmf' => 920,
                'doors' => 2,
            ),
            114 => 
            array (
                'id_reg' => 1115,
                'id_regmf' => 920,
                'doors' => 4,
            ),
            115 => 
            array (
                'id_reg' => 1116,
                'id_regmf' => 919,
                'doors' => 4,
            ),
            116 => 
            array (
                'id_reg' => 1117,
                'id_regmf' => 921,
                'doors' => 2,
            ),
            117 => 
            array (
                'id_reg' => 1118,
                'id_regmf' => 921,
                'doors' => 4,
            ),
            118 => 
            array (
                'id_reg' => 1119,
                'id_regmf' => 922,
                'doors' => 2,
            ),
            119 => 
            array (
                'id_reg' => 1120,
                'id_regmf' => 922,
                'doors' => 4,
            ),
            120 => 
            array (
                'id_reg' => 1121,
                'id_regmf' => 924,
                'doors' => 2,
            ),
            121 => 
            array (
                'id_reg' => 1122,
                'id_regmf' => 923,
                'doors' => 2,
            ),
            122 => 
            array (
                'id_reg' => 1123,
                'id_regmf' => 923,
                'doors' => 4,
            ),
            123 => 
            array (
                'id_reg' => 1124,
                'id_regmf' => 925,
                'doors' => 4,
            ),
            124 => 
            array (
                'id_reg' => 1125,
                'id_regmf' => 925,
                'doors' => 5,
            ),
            125 => 
            array (
                'id_reg' => 1126,
                'id_regmf' => 926,
                'doors' => 4,
            ),
            126 => 
            array (
                'id_reg' => 1127,
                'id_regmf' => 926,
                'doors' => 5,
            ),
            127 => 
            array (
                'id_reg' => 1128,
                'id_regmf' => 927,
                'doors' => 4,
            ),
            128 => 
            array (
                'id_reg' => 1129,
                'id_regmf' => 928,
                'doors' => 3,
            ),
            129 => 
            array (
                'id_reg' => 1130,
                'id_regmf' => 928,
                'doors' => 5,
            ),
            130 => 
            array (
                'id_reg' => 1131,
                'id_regmf' => 929,
                'doors' => 3,
            ),
            131 => 
            array (
                'id_reg' => 1132,
                'id_regmf' => 930,
                'doors' => 4,
            ),
            132 => 
            array (
                'id_reg' => 1133,
                'id_regmf' => 931,
                'doors' => 2,
            ),
            133 => 
            array (
                'id_reg' => 1134,
                'id_regmf' => 932,
                'doors' => 2,
            ),
            134 => 
            array (
                'id_reg' => 1135,
                'id_regmf' => 933,
                'doors' => 2,
            ),
            135 => 
            array (
                'id_reg' => 1136,
                'id_regmf' => 934,
                'doors' => 4,
            ),
            136 => 
            array (
                'id_reg' => 1137,
                'id_regmf' => 935,
                'doors' => 2,
            ),
            137 => 
            array (
                'id_reg' => 1138,
                'id_regmf' => 936,
                'doors' => 0,
            ),
            138 => 
            array (
                'id_reg' => 1139,
                'id_regmf' => 937,
                'doors' => 4,
            ),
            139 => 
            array (
                'id_reg' => 1140,
                'id_regmf' => 938,
                'doors' => 2,
            ),
            140 => 
            array (
                'id_reg' => 1141,
                'id_regmf' => 939,
                'doors' => 0,
            ),
            141 => 
            array (
                'id_reg' => 1142,
                'id_regmf' => 941,
                'doors' => 2,
            ),
            142 => 
            array (
                'id_reg' => 1143,
                'id_regmf' => 940,
                'doors' => 2,
            ),
            143 => 
            array (
                'id_reg' => 1144,
                'id_regmf' => 942,
                'doors' => 4,
            ),
            144 => 
            array (
                'id_reg' => 1145,
                'id_regmf' => 943,
                'doors' => 4,
            ),
            145 => 
            array (
                'id_reg' => 1146,
                'id_regmf' => 944,
                'doors' => 5,
            ),
            146 => 
            array (
                'id_reg' => 1147,
                'id_regmf' => 945,
                'doors' => 2,
            ),
            147 => 
            array (
                'id_reg' => 1148,
                'id_regmf' => 946,
                'doors' => 0,
            ),
            148 => 
            array (
                'id_reg' => 1149,
                'id_regmf' => 947,
                'doors' => 0,
            ),
            149 => 
            array (
                'id_reg' => 1150,
                'id_regmf' => 948,
                'doors' => 0,
            ),
            150 => 
            array (
                'id_reg' => 1151,
                'id_regmf' => 949,
                'doors' => 0,
            ),
            151 => 
            array (
                'id_reg' => 1152,
                'id_regmf' => 950,
                'doors' => 3,
            ),
            152 => 
            array (
                'id_reg' => 1153,
                'id_regmf' => 951,
                'doors' => 0,
            ),
            153 => 
            array (
                'id_reg' => 1154,
                'id_regmf' => 952,
                'doors' => 3,
            ),
            154 => 
            array (
                'id_reg' => 1155,
                'id_regmf' => 953,
                'doors' => 2,
            ),
            155 => 
            array (
                'id_reg' => 1156,
                'id_regmf' => 954,
                'doors' => 2,
            ),
            156 => 
            array (
                'id_reg' => 1157,
                'id_regmf' => 955,
                'doors' => 0,
            ),
            157 => 
            array (
                'id_reg' => 1158,
                'id_regmf' => 956,
                'doors' => 0,
            ),
            158 => 
            array (
                'id_reg' => 1159,
                'id_regmf' => 957,
                'doors' => 0,
            ),
            159 => 
            array (
                'id_reg' => 1160,
                'id_regmf' => 958,
                'doors' => 0,
            ),
            160 => 
            array (
                'id_reg' => 1161,
                'id_regmf' => 959,
                'doors' => 0,
            ),
            161 => 
            array (
                'id_reg' => 1162,
                'id_regmf' => 960,
                'doors' => 0,
            ),
            162 => 
            array (
                'id_reg' => 1163,
                'id_regmf' => 962,
                'doors' => 0,
            ),
            163 => 
            array (
                'id_reg' => 1164,
                'id_regmf' => 961,
                'doors' => 0,
            ),
            164 => 
            array (
                'id_reg' => 1165,
                'id_regmf' => 964,
                'doors' => 0,
            ),
            165 => 
            array (
                'id_reg' => 1166,
                'id_regmf' => 963,
                'doors' => 0,
            ),
            166 => 
            array (
                'id_reg' => 1167,
                'id_regmf' => 965,
                'doors' => 0,
            ),
            167 => 
            array (
                'id_reg' => 1168,
                'id_regmf' => 966,
                'doors' => 0,
            ),
            168 => 
            array (
                'id_reg' => 1169,
                'id_regmf' => 967,
                'doors' => 4,
            ),
            169 => 
            array (
                'id_reg' => 1170,
                'id_regmf' => 968,
                'doors' => 2,
            ),
            170 => 
            array (
                'id_reg' => 1171,
                'id_regmf' => 969,
                'doors' => 2,
            ),
            171 => 
            array (
                'id_reg' => 1172,
                'id_regmf' => 970,
                'doors' => 3,
            ),
            172 => 
            array (
                'id_reg' => 1173,
                'id_regmf' => 971,
                'doors' => 4,
            ),
            173 => 
            array (
                'id_reg' => 1174,
                'id_regmf' => 972,
                'doors' => 4,
            ),
            174 => 
            array (
                'id_reg' => 1175,
                'id_regmf' => 973,
                'doors' => 5,
            ),
            175 => 
            array (
                'id_reg' => 1176,
                'id_regmf' => 974,
                'doors' => 5,
            ),
            176 => 
            array (
                'id_reg' => 1177,
                'id_regmf' => 975,
                'doors' => 2,
            ),
            177 => 
            array (
                'id_reg' => 1178,
                'id_regmf' => 976,
                'doors' => 2,
            ),
            178 => 
            array (
                'id_reg' => 1179,
                'id_regmf' => 976,
                'doors' => 5,
            ),
            179 => 
            array (
                'id_reg' => 1180,
                'id_regmf' => 977,
                'doors' => 2,
            ),
            180 => 
            array (
                'id_reg' => 1181,
                'id_regmf' => 977,
                'doors' => 5,
            ),
            181 => 
            array (
                'id_reg' => 1182,
                'id_regmf' => 978,
                'doors' => 5,
            ),
            182 => 
            array (
                'id_reg' => 1183,
                'id_regmf' => 979,
                'doors' => 2,
            ),
            183 => 
            array (
                'id_reg' => 1184,
                'id_regmf' => 979,
                'doors' => 5,
            ),
            184 => 
            array (
                'id_reg' => 1185,
                'id_regmf' => 980,
                'doors' => 0,
            ),
            185 => 
            array (
                'id_reg' => 1186,
                'id_regmf' => 980,
                'doors' => 2,
            ),
            186 => 
            array (
                'id_reg' => 1187,
                'id_regmf' => 980,
                'doors' => 3,
            ),
            187 => 
            array (
                'id_reg' => 1188,
                'id_regmf' => 980,
                'doors' => 5,
            ),
            188 => 
            array (
                'id_reg' => 1189,
                'id_regmf' => 981,
                'doors' => 0,
            ),
            189 => 
            array (
                'id_reg' => 1190,
                'id_regmf' => 981,
                'doors' => 2,
            ),
            190 => 
            array (
                'id_reg' => 1191,
                'id_regmf' => 981,
                'doors' => 5,
            ),
            191 => 
            array (
                'id_reg' => 1192,
                'id_regmf' => 982,
                'doors' => 0,
            ),
            192 => 
            array (
                'id_reg' => 1193,
                'id_regmf' => 982,
                'doors' => 5,
            ),
            193 => 
            array (
                'id_reg' => 1194,
                'id_regmf' => 983,
                'doors' => 0,
            ),
            194 => 
            array (
                'id_reg' => 1195,
                'id_regmf' => 983,
                'doors' => 2,
            ),
            195 => 
            array (
                'id_reg' => 1196,
                'id_regmf' => 983,
                'doors' => 5,
            ),
            196 => 
            array (
                'id_reg' => 1197,
                'id_regmf' => 984,
                'doors' => 0,
            ),
            197 => 
            array (
                'id_reg' => 1198,
                'id_regmf' => 984,
                'doors' => 2,
            ),
            198 => 
            array (
                'id_reg' => 1199,
                'id_regmf' => 984,
                'doors' => 5,
            ),
            199 => 
            array (
                'id_reg' => 1200,
                'id_regmf' => 986,
                'doors' => 3,
            ),
            200 => 
            array (
                'id_reg' => 1201,
                'id_regmf' => 985,
                'doors' => 3,
            ),
            201 => 
            array (
                'id_reg' => 1202,
                'id_regmf' => 987,
                'doors' => 2,
            ),
            202 => 
            array (
                'id_reg' => 1203,
                'id_regmf' => 987,
                'doors' => 5,
            ),
            203 => 
            array (
                'id_reg' => 1204,
                'id_regmf' => 988,
                'doors' => 0,
            ),
            204 => 
            array (
                'id_reg' => 1205,
                'id_regmf' => 988,
                'doors' => 2,
            ),
            205 => 
            array (
                'id_reg' => 1206,
                'id_regmf' => 988,
                'doors' => 5,
            ),
            206 => 
            array (
                'id_reg' => 1207,
                'id_regmf' => 989,
                'doors' => 5,
            ),
            207 => 
            array (
                'id_reg' => 1208,
                'id_regmf' => 990,
                'doors' => 2,
            ),
            208 => 
            array (
                'id_reg' => 1209,
                'id_regmf' => 990,
                'doors' => 5,
            ),
            209 => 
            array (
                'id_reg' => 1210,
                'id_regmf' => 990,
                'doors' => 6,
            ),
            210 => 
            array (
                'id_reg' => 1211,
                'id_regmf' => 991,
                'doors' => 0,
            ),
            211 => 
            array (
                'id_reg' => 1212,
                'id_regmf' => 992,
                'doors' => 0,
            ),
            212 => 
            array (
                'id_reg' => 1213,
                'id_regmf' => 993,
                'doors' => 2,
            ),
            213 => 
            array (
                'id_reg' => 1214,
                'id_regmf' => 994,
                'doors' => 3,
            ),
            214 => 
            array (
                'id_reg' => 1215,
                'id_regmf' => 995,
                'doors' => 3,
            ),
            215 => 
            array (
                'id_reg' => 1216,
                'id_regmf' => 997,
                'doors' => 3,
            ),
            216 => 
            array (
                'id_reg' => 1217,
                'id_regmf' => 996,
                'doors' => 3,
            ),
            217 => 
            array (
                'id_reg' => 1218,
                'id_regmf' => 998,
                'doors' => 3,
            ),
            218 => 
            array (
                'id_reg' => 1219,
                'id_regmf' => 999,
                'doors' => 3,
            ),
            219 => 
            array (
                'id_reg' => 1220,
                'id_regmf' => 1000,
                'doors' => 3,
            ),
            220 => 
            array (
                'id_reg' => 1221,
                'id_regmf' => 1001,
                'doors' => 4,
            ),
            221 => 
            array (
                'id_reg' => 1222,
                'id_regmf' => 1002,
                'doors' => 2,
            ),
            222 => 
            array (
                'id_reg' => 1223,
                'id_regmf' => 1003,
                'doors' => 3,
            ),
            223 => 
            array (
                'id_reg' => 1224,
                'id_regmf' => 1004,
                'doors' => 2,
            ),
            224 => 
            array (
                'id_reg' => 1225,
                'id_regmf' => 1005,
                'doors' => 2,
            ),
            225 => 
            array (
                'id_reg' => 1226,
                'id_regmf' => 1006,
                'doors' => 2,
            ),
            226 => 
            array (
                'id_reg' => 1227,
                'id_regmf' => 1007,
                'doors' => 2,
            ),
            227 => 
            array (
                'id_reg' => 1228,
                'id_regmf' => 1007,
                'doors' => 4,
            ),
            228 => 
            array (
                'id_reg' => 1229,
                'id_regmf' => 1008,
                'doors' => 4,
            ),
            229 => 
            array (
                'id_reg' => 1230,
                'id_regmf' => 1009,
                'doors' => 2,
            ),
            230 => 
            array (
                'id_reg' => 1231,
                'id_regmf' => 1010,
                'doors' => 2,
            ),
            231 => 
            array (
                'id_reg' => 1232,
                'id_regmf' => 1011,
                'doors' => 5,
            ),
            232 => 
            array (
                'id_reg' => 1233,
                'id_regmf' => 1012,
                'doors' => 2,
            ),
            233 => 
            array (
                'id_reg' => 1234,
                'id_regmf' => 1013,
                'doors' => 2,
            ),
            234 => 
            array (
                'id_reg' => 1235,
                'id_regmf' => 1014,
                'doors' => 2,
            ),
            235 => 
            array (
                'id_reg' => 1236,
                'id_regmf' => 1015,
                'doors' => 2,
            ),
            236 => 
            array (
                'id_reg' => 1237,
                'id_regmf' => 1016,
                'doors' => 2,
            ),
            237 => 
            array (
                'id_reg' => 1238,
                'id_regmf' => 1017,
                'doors' => 2,
            ),
            238 => 
            array (
                'id_reg' => 1239,
                'id_regmf' => 1018,
                'doors' => 2,
            ),
            239 => 
            array (
                'id_reg' => 1240,
                'id_regmf' => 1019,
                'doors' => 2,
            ),
            240 => 
            array (
                'id_reg' => 1241,
                'id_regmf' => 1020,
                'doors' => 2,
            ),
            241 => 
            array (
                'id_reg' => 1242,
                'id_regmf' => 1021,
                'doors' => 2,
            ),
            242 => 
            array (
                'id_reg' => 1243,
                'id_regmf' => 1022,
                'doors' => 2,
            ),
            243 => 
            array (
                'id_reg' => 1244,
                'id_regmf' => 1023,
                'doors' => 2,
            ),
            244 => 
            array (
                'id_reg' => 1245,
                'id_regmf' => 1024,
                'doors' => 2,
            ),
            245 => 
            array (
                'id_reg' => 1246,
                'id_regmf' => 1025,
                'doors' => 2,
            ),
            246 => 
            array (
                'id_reg' => 1247,
                'id_regmf' => 1026,
                'doors' => 2,
            ),
            247 => 
            array (
                'id_reg' => 1248,
                'id_regmf' => 1027,
                'doors' => 2,
            ),
            248 => 
            array (
                'id_reg' => 1249,
                'id_regmf' => 1028,
                'doors' => 2,
            ),
            249 => 
            array (
                'id_reg' => 1250,
                'id_regmf' => 1029,
                'doors' => 2,
            ),
            250 => 
            array (
                'id_reg' => 1251,
                'id_regmf' => 1030,
                'doors' => 2,
            ),
            251 => 
            array (
                'id_reg' => 1252,
                'id_regmf' => 1031,
                'doors' => 2,
            ),
            252 => 
            array (
                'id_reg' => 1253,
                'id_regmf' => 1032,
                'doors' => 2,
            ),
            253 => 
            array (
                'id_reg' => 1254,
                'id_regmf' => 1033,
                'doors' => 2,
            ),
            254 => 
            array (
                'id_reg' => 1255,
                'id_regmf' => 1034,
                'doors' => 2,
            ),
            255 => 
            array (
                'id_reg' => 1256,
                'id_regmf' => 1035,
                'doors' => 2,
            ),
            256 => 
            array (
                'id_reg' => 1257,
                'id_regmf' => 1036,
                'doors' => 2,
            ),
            257 => 
            array (
                'id_reg' => 1258,
                'id_regmf' => 1037,
                'doors' => 2,
            ),
            258 => 
            array (
                'id_reg' => 1259,
                'id_regmf' => 1038,
                'doors' => 2,
            ),
            259 => 
            array (
                'id_reg' => 1260,
                'id_regmf' => 1039,
                'doors' => 2,
            ),
            260 => 
            array (
                'id_reg' => 1261,
                'id_regmf' => 1040,
                'doors' => 2,
            ),
            261 => 
            array (
                'id_reg' => 1262,
                'id_regmf' => 1041,
                'doors' => 2,
            ),
            262 => 
            array (
                'id_reg' => 1263,
                'id_regmf' => 1042,
                'doors' => 2,
            ),
            263 => 
            array (
                'id_reg' => 1264,
                'id_regmf' => 1043,
                'doors' => 2,
            ),
            264 => 
            array (
                'id_reg' => 1265,
                'id_regmf' => 1044,
                'doors' => 2,
            ),
            265 => 
            array (
                'id_reg' => 1266,
                'id_regmf' => 1045,
                'doors' => 2,
            ),
            266 => 
            array (
                'id_reg' => 1267,
                'id_regmf' => 1046,
                'doors' => 2,
            ),
            267 => 
            array (
                'id_reg' => 1268,
                'id_regmf' => 1047,
                'doors' => 2,
            ),
            268 => 
            array (
                'id_reg' => 1269,
                'id_regmf' => 1048,
                'doors' => 3,
            ),
            269 => 
            array (
                'id_reg' => 1270,
                'id_regmf' => 1049,
                'doors' => 3,
            ),
            270 => 
            array (
                'id_reg' => 1271,
                'id_regmf' => 1050,
                'doors' => 3,
            ),
            271 => 
            array (
                'id_reg' => 1272,
                'id_regmf' => 1051,
                'doors' => 2,
            ),
            272 => 
            array (
                'id_reg' => 1273,
                'id_regmf' => 1052,
                'doors' => 2,
            ),
            273 => 
            array (
                'id_reg' => 1274,
                'id_regmf' => 1053,
                'doors' => 2,
            ),
            274 => 
            array (
                'id_reg' => 1275,
                'id_regmf' => 1054,
                'doors' => 2,
            ),
            275 => 
            array (
                'id_reg' => 1276,
                'id_regmf' => 1055,
                'doors' => 2,
            ),
            276 => 
            array (
                'id_reg' => 1277,
                'id_regmf' => 1090,
                'doors' => 2,
            ),
            277 => 
            array (
                'id_reg' => 1278,
                'id_regmf' => 1090,
                'doors' => 4,
            ),
            278 => 
            array (
                'id_reg' => 1279,
                'id_regmf' => 1090,
                'doors' => 5,
            ),
            279 => 
            array (
                'id_reg' => 1280,
                'id_regmf' => 1091,
                'doors' => 2,
            ),
            280 => 
            array (
                'id_reg' => 1281,
                'id_regmf' => 1058,
                'doors' => 2,
            ),
            281 => 
            array (
                'id_reg' => 1282,
                'id_regmf' => 1058,
                'doors' => 5,
            ),
            282 => 
            array (
                'id_reg' => 1283,
                'id_regmf' => 1059,
                'doors' => 2,
            ),
            283 => 
            array (
                'id_reg' => 1284,
                'id_regmf' => 1060,
                'doors' => 2,
            ),
            284 => 
            array (
                'id_reg' => 1285,
                'id_regmf' => 1060,
                'doors' => 4,
            ),
            285 => 
            array (
                'id_reg' => 1286,
                'id_regmf' => 1060,
                'doors' => 5,
            ),
            286 => 
            array (
                'id_reg' => 1287,
                'id_regmf' => 1061,
                'doors' => 2,
            ),
            287 => 
            array (
                'id_reg' => 1288,
                'id_regmf' => 1061,
                'doors' => 3,
            ),
            288 => 
            array (
                'id_reg' => 1289,
                'id_regmf' => 1062,
                'doors' => 3,
            ),
            289 => 
            array (
                'id_reg' => 1290,
                'id_regmf' => 1063,
                'doors' => 2,
            ),
            290 => 
            array (
                'id_reg' => 1291,
                'id_regmf' => 1063,
                'doors' => 3,
            ),
            291 => 
            array (
                'id_reg' => 1292,
                'id_regmf' => 1063,
                'doors' => 4,
            ),
            292 => 
            array (
                'id_reg' => 1293,
                'id_regmf' => 1064,
                'doors' => 4,
            ),
            293 => 
            array (
                'id_reg' => 1294,
                'id_regmf' => 1092,
                'doors' => 4,
            ),
            294 => 
            array (
                'id_reg' => 1295,
                'id_regmf' => 1066,
                'doors' => 4,
            ),
            295 => 
            array (
                'id_reg' => 1296,
                'id_regmf' => 1065,
                'doors' => 4,
            ),
            296 => 
            array (
                'id_reg' => 1297,
                'id_regmf' => 1065,
                'doors' => 5,
            ),
            297 => 
            array (
                'id_reg' => 1298,
                'id_regmf' => 1068,
                'doors' => 4,
            ),
            298 => 
            array (
                'id_reg' => 1299,
                'id_regmf' => 1067,
                'doors' => 4,
            ),
            299 => 
            array (
                'id_reg' => 1300,
                'id_regmf' => 1093,
                'doors' => 4,
            ),
            300 => 
            array (
                'id_reg' => 1301,
                'id_regmf' => 1094,
                'doors' => 4,
            ),
            301 => 
            array (
                'id_reg' => 1302,
                'id_regmf' => 1095,
                'doors' => 4,
            ),
            302 => 
            array (
                'id_reg' => 1303,
                'id_regmf' => 1096,
                'doors' => 4,
            ),
            303 => 
            array (
                'id_reg' => 1304,
                'id_regmf' => 1069,
                'doors' => 2,
            ),
            304 => 
            array (
                'id_reg' => 1305,
                'id_regmf' => 1069,
                'doors' => 5,
            ),
            305 => 
            array (
                'id_reg' => 1306,
                'id_regmf' => 1070,
                'doors' => 4,
            ),
            306 => 
            array (
                'id_reg' => 1307,
                'id_regmf' => 1072,
                'doors' => 2,
            ),
            307 => 
            array (
                'id_reg' => 1308,
                'id_regmf' => 1071,
                'doors' => 2,
            ),
            308 => 
            array (
                'id_reg' => 1309,
                'id_regmf' => 1074,
                'doors' => 5,
            ),
            309 => 
            array (
                'id_reg' => 1310,
                'id_regmf' => 1075,
                'doors' => 5,
            ),
            310 => 
            array (
                'id_reg' => 1311,
                'id_regmf' => 1073,
                'doors' => 5,
            ),
            311 => 
            array (
                'id_reg' => 1312,
                'id_regmf' => 1076,
                'doors' => 5,
            ),
            312 => 
            array (
                'id_reg' => 1313,
                'id_regmf' => 1078,
                'doors' => 5,
            ),
            313 => 
            array (
                'id_reg' => 1314,
                'id_regmf' => 1077,
                'doors' => 5,
            ),
            314 => 
            array (
                'id_reg' => 1315,
                'id_regmf' => 1079,
                'doors' => 2,
            ),
            315 => 
            array (
                'id_reg' => 1316,
                'id_regmf' => 1080,
                'doors' => 4,
            ),
            316 => 
            array (
                'id_reg' => 1317,
                'id_regmf' => 1081,
                'doors' => 4,
            ),
            317 => 
            array (
                'id_reg' => 1318,
                'id_regmf' => 1082,
                'doors' => 4,
            ),
            318 => 
            array (
                'id_reg' => 1319,
                'id_regmf' => 1083,
                'doors' => 2,
            ),
            319 => 
            array (
                'id_reg' => 1320,
                'id_regmf' => 1083,
                'doors' => 4,
            ),
            320 => 
            array (
                'id_reg' => 1321,
                'id_regmf' => 1056,
                'doors' => 2,
            ),
            321 => 
            array (
                'id_reg' => 1322,
                'id_regmf' => 1084,
                'doors' => 2,
            ),
            322 => 
            array (
                'id_reg' => 1323,
                'id_regmf' => 1084,
                'doors' => 3,
            ),
            323 => 
            array (
                'id_reg' => 1324,
                'id_regmf' => 1084,
                'doors' => 4,
            ),
            324 => 
            array (
                'id_reg' => 1325,
                'id_regmf' => 1085,
                'doors' => 5,
            ),
            325 => 
            array (
                'id_reg' => 1326,
                'id_regmf' => 1086,
                'doors' => 2,
            ),
            326 => 
            array (
                'id_reg' => 1327,
                'id_regmf' => 1087,
                'doors' => 5,
            ),
            327 => 
            array (
                'id_reg' => 1328,
                'id_regmf' => 1057,
                'doors' => 2,
            ),
            328 => 
            array (
                'id_reg' => 1329,
                'id_regmf' => 1088,
                'doors' => 2,
            ),
            329 => 
            array (
                'id_reg' => 1330,
                'id_regmf' => 1088,
                'doors' => 5,
            ),
            330 => 
            array (
                'id_reg' => 1331,
                'id_regmf' => 1089,
                'doors' => 4,
            ),
            331 => 
            array (
                'id_reg' => 1332,
                'id_regmf' => 1097,
                'doors' => 4,
            ),
            332 => 
            array (
                'id_reg' => 1333,
                'id_regmf' => 1099,
                'doors' => 4,
            ),
            333 => 
            array (
                'id_reg' => 1334,
                'id_regmf' => 1098,
                'doors' => 4,
            ),
            334 => 
            array (
                'id_reg' => 1335,
                'id_regmf' => 1100,
                'doors' => 2,
            ),
            335 => 
            array (
                'id_reg' => 1336,
                'id_regmf' => 1102,
                'doors' => 2,
            ),
            336 => 
            array (
                'id_reg' => 1337,
                'id_regmf' => 1101,
                'doors' => 3,
            ),
            337 => 
            array (
                'id_reg' => 1338,
                'id_regmf' => 1103,
                'doors' => 3,
            ),
            338 => 
            array (
                'id_reg' => 1339,
                'id_regmf' => 1104,
                'doors' => 2,
            ),
            339 => 
            array (
                'id_reg' => 1340,
                'id_regmf' => 1105,
                'doors' => 5,
            ),
            340 => 
            array (
                'id_reg' => 1341,
                'id_regmf' => 1106,
                'doors' => 5,
            ),
            341 => 
            array (
                'id_reg' => 1342,
                'id_regmf' => 1107,
                'doors' => 0,
            ),
            342 => 
            array (
                'id_reg' => 1343,
                'id_regmf' => 1108,
                'doors' => 2,
            ),
            343 => 
            array (
                'id_reg' => 1344,
                'id_regmf' => 1110,
                'doors' => 4,
            ),
            344 => 
            array (
                'id_reg' => 1345,
                'id_regmf' => 1110,
                'doors' => 5,
            ),
            345 => 
            array (
                'id_reg' => 1346,
                'id_regmf' => 1109,
                'doors' => 2,
            ),
            346 => 
            array (
                'id_reg' => 1347,
                'id_regmf' => 1109,
                'doors' => 4,
            ),
            347 => 
            array (
                'id_reg' => 1348,
                'id_regmf' => 1109,
                'doors' => 5,
            ),
            348 => 
            array (
                'id_reg' => 1349,
                'id_regmf' => 1111,
                'doors' => 4,
            ),
            349 => 
            array (
                'id_reg' => 1350,
                'id_regmf' => 1111,
                'doors' => 5,
            ),
            350 => 
            array (
                'id_reg' => 1351,
                'id_regmf' => 1112,
                'doors' => 4,
            ),
            351 => 
            array (
                'id_reg' => 1352,
                'id_regmf' => 1113,
                'doors' => 4,
            ),
            352 => 
            array (
                'id_reg' => 1353,
                'id_regmf' => 1114,
                'doors' => 0,
            ),
            353 => 
            array (
                'id_reg' => 1354,
                'id_regmf' => 1114,
                'doors' => 2,
            ),
            354 => 
            array (
                'id_reg' => 1355,
                'id_regmf' => 1114,
                'doors' => 4,
            ),
            355 => 
            array (
                'id_reg' => 1356,
                'id_regmf' => 1114,
                'doors' => 5,
            ),
            356 => 
            array (
                'id_reg' => 1357,
                'id_regmf' => 1115,
                'doors' => 0,
            ),
            357 => 
            array (
                'id_reg' => 1358,
                'id_regmf' => 1115,
                'doors' => 2,
            ),
            358 => 
            array (
                'id_reg' => 1359,
                'id_regmf' => 1118,
                'doors' => 2,
            ),
            359 => 
            array (
                'id_reg' => 1360,
                'id_regmf' => 1118,
                'doors' => 4,
            ),
            360 => 
            array (
                'id_reg' => 1361,
                'id_regmf' => 1118,
                'doors' => 5,
            ),
            361 => 
            array (
                'id_reg' => 1362,
                'id_regmf' => 1117,
                'doors' => 3,
            ),
            362 => 
            array (
                'id_reg' => 1363,
                'id_regmf' => 1117,
                'doors' => 4,
            ),
            363 => 
            array (
                'id_reg' => 1364,
                'id_regmf' => 1117,
                'doors' => 5,
            ),
            364 => 
            array (
                'id_reg' => 1365,
                'id_regmf' => 1116,
                'doors' => 2,
            ),
            365 => 
            array (
                'id_reg' => 1366,
                'id_regmf' => 1116,
                'doors' => 3,
            ),
            366 => 
            array (
                'id_reg' => 1367,
                'id_regmf' => 1116,
                'doors' => 4,
            ),
            367 => 
            array (
                'id_reg' => 1368,
                'id_regmf' => 1116,
                'doors' => 5,
            ),
            368 => 
            array (
                'id_reg' => 1369,
                'id_regmf' => 1119,
                'doors' => 5,
            ),
            369 => 
            array (
                'id_reg' => 1370,
                'id_regmf' => 1120,
                'doors' => 2,
            ),
            370 => 
            array (
                'id_reg' => 1371,
                'id_regmf' => 1120,
                'doors' => 4,
            ),
            371 => 
            array (
                'id_reg' => 1372,
                'id_regmf' => 1121,
                'doors' => 5,
            ),
            372 => 
            array (
                'id_reg' => 1373,
                'id_regmf' => 1122,
                'doors' => 5,
            ),
            373 => 
            array (
                'id_reg' => 1374,
                'id_regmf' => 1123,
                'doors' => 4,
            ),
            374 => 
            array (
                'id_reg' => 1375,
                'id_regmf' => 1124,
                'doors' => 4,
            ),
            375 => 
            array (
                'id_reg' => 1376,
                'id_regmf' => 1125,
                'doors' => 4,
            ),
            376 => 
            array (
                'id_reg' => 1377,
                'id_regmf' => 1125,
                'doors' => 5,
            ),
            377 => 
            array (
                'id_reg' => 1378,
                'id_regmf' => 1126,
                'doors' => 4,
            ),
            378 => 
            array (
                'id_reg' => 1379,
                'id_regmf' => 1126,
                'doors' => 5,
            ),
            379 => 
            array (
                'id_reg' => 1380,
                'id_regmf' => 1127,
                'doors' => 5,
            ),
            380 => 
            array (
                'id_reg' => 1381,
                'id_regmf' => 1128,
                'doors' => 5,
            ),
            381 => 
            array (
                'id_reg' => 1382,
                'id_regmf' => 1129,
                'doors' => 5,
            ),
            382 => 
            array (
                'id_reg' => 1383,
                'id_regmf' => 1130,
                'doors' => 5,
            ),
            383 => 
            array (
                'id_reg' => 1384,
                'id_regmf' => 1132,
                'doors' => 5,
            ),
            384 => 
            array (
                'id_reg' => 1385,
                'id_regmf' => 1131,
                'doors' => 3,
            ),
            385 => 
            array (
                'id_reg' => 1386,
                'id_regmf' => 1131,
                'doors' => 5,
            ),
            386 => 
            array (
                'id_reg' => 1387,
                'id_regmf' => 1133,
                'doors' => 3,
            ),
            387 => 
            array (
                'id_reg' => 1388,
                'id_regmf' => 1133,
                'doors' => 5,
            ),
            388 => 
            array (
                'id_reg' => 1389,
                'id_regmf' => 1135,
                'doors' => 5,
            ),
            389 => 
            array (
                'id_reg' => 1390,
                'id_regmf' => 1134,
                'doors' => 5,
            ),
            390 => 
            array (
                'id_reg' => 1391,
                'id_regmf' => 1136,
                'doors' => 4,
            ),
            391 => 
            array (
                'id_reg' => 1392,
                'id_regmf' => 1136,
                'doors' => 5,
            ),
            392 => 
            array (
                'id_reg' => 1393,
                'id_regmf' => 1137,
                'doors' => 4,
            ),
            393 => 
            array (
                'id_reg' => 1394,
                'id_regmf' => 1137,
                'doors' => 5,
            ),
            394 => 
            array (
                'id_reg' => 1395,
                'id_regmf' => 1139,
                'doors' => 2,
            ),
            395 => 
            array (
                'id_reg' => 1396,
                'id_regmf' => 1139,
                'doors' => 3,
            ),
            396 => 
            array (
                'id_reg' => 1397,
                'id_regmf' => 1139,
                'doors' => 5,
            ),
            397 => 
            array (
                'id_reg' => 1398,
                'id_regmf' => 1138,
                'doors' => 3,
            ),
            398 => 
            array (
                'id_reg' => 1399,
                'id_regmf' => 1140,
                'doors' => 5,
            ),
            399 => 
            array (
                'id_reg' => 1400,
                'id_regmf' => 1141,
                'doors' => 5,
            ),
            400 => 
            array (
                'id_reg' => 1401,
                'id_regmf' => 1142,
                'doors' => 2,
            ),
            401 => 
            array (
                'id_reg' => 1402,
                'id_regmf' => 1143,
                'doors' => 2,
            ),
            402 => 
            array (
                'id_reg' => 1403,
                'id_regmf' => 1144,
                'doors' => 2,
            ),
            403 => 
            array (
                'id_reg' => 1404,
                'id_regmf' => 1145,
                'doors' => 2,
            ),
            404 => 
            array (
                'id_reg' => 1405,
                'id_regmf' => 1146,
                'doors' => 2,
            ),
            405 => 
            array (
                'id_reg' => 1406,
                'id_regmf' => 1146,
                'doors' => 5,
            ),
            406 => 
            array (
                'id_reg' => 1407,
                'id_regmf' => 1148,
                'doors' => 4,
            ),
            407 => 
            array (
                'id_reg' => 1408,
                'id_regmf' => 1148,
                'doors' => 5,
            ),
            408 => 
            array (
                'id_reg' => 1409,
                'id_regmf' => 1147,
                'doors' => 4,
            ),
            409 => 
            array (
                'id_reg' => 1410,
                'id_regmf' => 1147,
                'doors' => 5,
            ),
            410 => 
            array (
                'id_reg' => 1411,
                'id_regmf' => 1151,
                'doors' => 3,
            ),
            411 => 
            array (
                'id_reg' => 1412,
                'id_regmf' => 1151,
                'doors' => 4,
            ),
            412 => 
            array (
                'id_reg' => 1413,
                'id_regmf' => 1151,
                'doors' => 5,
            ),
            413 => 
            array (
                'id_reg' => 1414,
                'id_regmf' => 1149,
                'doors' => 3,
            ),
            414 => 
            array (
                'id_reg' => 1415,
                'id_regmf' => 1149,
                'doors' => 4,
            ),
            415 => 
            array (
                'id_reg' => 1416,
                'id_regmf' => 1149,
                'doors' => 5,
            ),
            416 => 
            array (
                'id_reg' => 1417,
                'id_regmf' => 1150,
                'doors' => 4,
            ),
            417 => 
            array (
                'id_reg' => 1418,
                'id_regmf' => 1150,
                'doors' => 5,
            ),
            418 => 
            array (
                'id_reg' => 1419,
                'id_regmf' => 1152,
                'doors' => 2,
            ),
            419 => 
            array (
                'id_reg' => 1420,
                'id_regmf' => 1154,
                'doors' => 5,
            ),
            420 => 
            array (
                'id_reg' => 1421,
                'id_regmf' => 1153,
                'doors' => 5,
            ),
            421 => 
            array (
                'id_reg' => 1422,
                'id_regmf' => 1155,
                'doors' => 3,
            ),
            422 => 
            array (
                'id_reg' => 1423,
                'id_regmf' => 1155,
                'doors' => 5,
            ),
            423 => 
            array (
                'id_reg' => 1424,
                'id_regmf' => 1156,
                'doors' => 3,
            ),
            424 => 
            array (
                'id_reg' => 1425,
                'id_regmf' => 1156,
                'doors' => 5,
            ),
            425 => 
            array (
                'id_reg' => 1426,
                'id_regmf' => 1157,
                'doors' => 5,
            ),
            426 => 
            array (
                'id_reg' => 1427,
                'id_regmf' => 1158,
                'doors' => 2,
            ),
            427 => 
            array (
                'id_reg' => 1428,
                'id_regmf' => 1158,
                'doors' => 3,
            ),
            428 => 
            array (
                'id_reg' => 1429,
                'id_regmf' => 1158,
                'doors' => 4,
            ),
            429 => 
            array (
                'id_reg' => 1430,
                'id_regmf' => 1158,
                'doors' => 5,
            ),
            430 => 
            array (
                'id_reg' => 1431,
                'id_regmf' => 1159,
                'doors' => 2,
            ),
            431 => 
            array (
                'id_reg' => 1432,
                'id_regmf' => 1159,
                'doors' => 3,
            ),
            432 => 
            array (
                'id_reg' => 1433,
                'id_regmf' => 1159,
                'doors' => 4,
            ),
            433 => 
            array (
                'id_reg' => 1434,
                'id_regmf' => 1159,
                'doors' => 5,
            ),
            434 => 
            array (
                'id_reg' => 1435,
                'id_regmf' => 1160,
                'doors' => 2,
            ),
            435 => 
            array (
                'id_reg' => 1436,
                'id_regmf' => 1161,
                'doors' => 2,
            ),
            436 => 
            array (
                'id_reg' => 1437,
                'id_regmf' => 1161,
                'doors' => 3,
            ),
            437 => 
            array (
                'id_reg' => 1438,
                'id_regmf' => 1161,
                'doors' => 5,
            ),
            438 => 
            array (
                'id_reg' => 1439,
                'id_regmf' => 1162,
                'doors' => 2,
            ),
            439 => 
            array (
                'id_reg' => 1440,
                'id_regmf' => 1163,
                'doors' => 2,
            ),
            440 => 
            array (
                'id_reg' => 1441,
                'id_regmf' => 1164,
                'doors' => 5,
            ),
            441 => 
            array (
                'id_reg' => 1442,
                'id_regmf' => 1165,
                'doors' => 2,
            ),
            442 => 
            array (
                'id_reg' => 1443,
                'id_regmf' => 1165,
                'doors' => 3,
            ),
            443 => 
            array (
                'id_reg' => 1444,
                'id_regmf' => 1165,
                'doors' => 5,
            ),
            444 => 
            array (
                'id_reg' => 1445,
                'id_regmf' => 1166,
                'doors' => 5,
            ),
            445 => 
            array (
                'id_reg' => 1446,
                'id_regmf' => 1167,
                'doors' => 2,
            ),
            446 => 
            array (
                'id_reg' => 1447,
                'id_regmf' => 1168,
                'doors' => 5,
            ),
            447 => 
            array (
                'id_reg' => 1448,
                'id_regmf' => 1169,
                'doors' => 2,
            ),
            448 => 
            array (
                'id_reg' => 1449,
                'id_regmf' => 1170,
                'doors' => 4,
            ),
            449 => 
            array (
                'id_reg' => 1450,
                'id_regmf' => 1171,
                'doors' => 4,
            ),
            450 => 
            array (
                'id_reg' => 1451,
                'id_regmf' => 1172,
                'doors' => 2,
            ),
            451 => 
            array (
                'id_reg' => 1452,
                'id_regmf' => 1172,
                'doors' => 4,
            ),
            452 => 
            array (
                'id_reg' => 1453,
                'id_regmf' => 1173,
                'doors' => 2,
            ),
            453 => 
            array (
                'id_reg' => 1454,
                'id_regmf' => 1174,
                'doors' => 4,
            ),
            454 => 
            array (
                'id_reg' => 1455,
                'id_regmf' => 1174,
                'doors' => 5,
            ),
            455 => 
            array (
                'id_reg' => 1456,
                'id_regmf' => 1175,
                'doors' => 2,
            ),
            456 => 
            array (
                'id_reg' => 1457,
                'id_regmf' => 1176,
                'doors' => 5,
            ),
            457 => 
            array (
                'id_reg' => 1458,
                'id_regmf' => 1177,
                'doors' => 2,
            ),
            458 => 
            array (
                'id_reg' => 1459,
                'id_regmf' => 1177,
                'doors' => 4,
            ),
            459 => 
            array (
                'id_reg' => 1460,
                'id_regmf' => 1180,
                'doors' => 5,
            ),
            460 => 
            array (
                'id_reg' => 1461,
                'id_regmf' => 1178,
                'doors' => 5,
            ),
            461 => 
            array (
                'id_reg' => 1462,
                'id_regmf' => 1179,
                'doors' => 5,
            ),
            462 => 
            array (
                'id_reg' => 1463,
                'id_regmf' => 1181,
                'doors' => 5,
            ),
            463 => 
            array (
                'id_reg' => 1464,
                'id_regmf' => 1182,
                'doors' => 3,
            ),
            464 => 
            array (
                'id_reg' => 1465,
                'id_regmf' => 1183,
                'doors' => 3,
            ),
            465 => 
            array (
                'id_reg' => 1466,
                'id_regmf' => 1184,
                'doors' => 5,
            ),
            466 => 
            array (
                'id_reg' => 1467,
                'id_regmf' => 1187,
                'doors' => 5,
            ),
            467 => 
            array (
                'id_reg' => 1468,
                'id_regmf' => 1186,
                'doors' => 5,
            ),
            468 => 
            array (
                'id_reg' => 1469,
                'id_regmf' => 1185,
                'doors' => 5,
            ),
            469 => 
            array (
                'id_reg' => 1470,
                'id_regmf' => 1188,
                'doors' => 2,
            ),
            470 => 
            array (
                'id_reg' => 1471,
                'id_regmf' => 1188,
                'doors' => 3,
            ),
            471 => 
            array (
                'id_reg' => 1472,
                'id_regmf' => 1189,
                'doors' => 2,
            ),
            472 => 
            array (
                'id_reg' => 1473,
                'id_regmf' => 1190,
                'doors' => 4,
            ),
            473 => 
            array (
                'id_reg' => 1474,
                'id_regmf' => 1190,
                'doors' => 5,
            ),
            474 => 
            array (
                'id_reg' => 1475,
                'id_regmf' => 1190,
                'doors' => 6,
            ),
            475 => 
            array (
                'id_reg' => 1476,
                'id_regmf' => 1191,
                'doors' => 4,
            ),
            476 => 
            array (
                'id_reg' => 1477,
                'id_regmf' => 1191,
                'doors' => 5,
            ),
            477 => 
            array (
                'id_reg' => 1478,
                'id_regmf' => 1192,
                'doors' => 4,
            ),
            478 => 
            array (
                'id_reg' => 1479,
                'id_regmf' => 1193,
                'doors' => 2,
            ),
            479 => 
            array (
                'id_reg' => 1480,
                'id_regmf' => 1193,
                'doors' => 4,
            ),
            480 => 
            array (
                'id_reg' => 1481,
                'id_regmf' => 1194,
                'doors' => 4,
            ),
            481 => 
            array (
                'id_reg' => 1482,
                'id_regmf' => 1195,
                'doors' => 4,
            ),
            482 => 
            array (
                'id_reg' => 1483,
                'id_regmf' => 1196,
                'doors' => 4,
            ),
            483 => 
            array (
                'id_reg' => 1484,
                'id_regmf' => 1197,
                'doors' => 3,
            ),
            484 => 
            array (
                'id_reg' => 1485,
                'id_regmf' => 1198,
                'doors' => 2,
            ),
            485 => 
            array (
                'id_reg' => 1486,
                'id_regmf' => 1200,
                'doors' => 4,
            ),
            486 => 
            array (
                'id_reg' => 1487,
                'id_regmf' => 1199,
                'doors' => 4,
            ),
            487 => 
            array (
                'id_reg' => 1488,
                'id_regmf' => 1201,
                'doors' => 2,
            ),
            488 => 
            array (
                'id_reg' => 1489,
                'id_regmf' => 1202,
                'doors' => 4,
            ),
            489 => 
            array (
                'id_reg' => 1490,
                'id_regmf' => 1203,
                'doors' => 4,
            ),
            490 => 
            array (
                'id_reg' => 1491,
                'id_regmf' => 1203,
                'doors' => 5,
            ),
            491 => 
            array (
                'id_reg' => 1492,
                'id_regmf' => 1203,
                'doors' => 6,
            ),
            492 => 
            array (
                'id_reg' => 1493,
                'id_regmf' => 1204,
                'doors' => 5,
            ),
            493 => 
            array (
                'id_reg' => 1494,
                'id_regmf' => 1204,
                'doors' => 6,
            ),
            494 => 
            array (
                'id_reg' => 1495,
                'id_regmf' => 1205,
                'doors' => 5,
            ),
            495 => 
            array (
                'id_reg' => 1496,
                'id_regmf' => 1206,
                'doors' => 2,
            ),
            496 => 
            array (
                'id_reg' => 1497,
                'id_regmf' => 1206,
                'doors' => 5,
            ),
            497 => 
            array (
                'id_reg' => 1498,
                'id_regmf' => 1207,
                'doors' => 5,
            ),
            498 => 
            array (
                'id_reg' => 1499,
                'id_regmf' => 1208,
                'doors' => 5,
            ),
            499 => 
            array (
                'id_reg' => 1500,
                'id_regmf' => 1209,
                'doors' => 5,
            ),
        ));
        \DB::table('model_fuel_numdoors')->insert(array (
            0 => 
            array (
                'id_reg' => 1501,
                'id_regmf' => 1210,
                'doors' => 0,
            ),
            1 => 
            array (
                'id_reg' => 1502,
                'id_regmf' => 1212,
                'doors' => 5,
            ),
            2 => 
            array (
                'id_reg' => 1503,
                'id_regmf' => 1211,
                'doors' => 5,
            ),
            3 => 
            array (
                'id_reg' => 1504,
                'id_regmf' => 1214,
                'doors' => 5,
            ),
            4 => 
            array (
                'id_reg' => 1505,
                'id_regmf' => 1213,
                'doors' => 5,
            ),
            5 => 
            array (
                'id_reg' => 1506,
                'id_regmf' => 1215,
                'doors' => 5,
            ),
            6 => 
            array (
                'id_reg' => 1507,
                'id_regmf' => 1216,
                'doors' => 5,
            ),
            7 => 
            array (
                'id_reg' => 1508,
                'id_regmf' => 1217,
                'doors' => 2,
            ),
            8 => 
            array (
                'id_reg' => 1509,
                'id_regmf' => 1217,
                'doors' => 3,
            ),
            9 => 
            array (
                'id_reg' => 1510,
                'id_regmf' => 1217,
                'doors' => 4,
            ),
            10 => 
            array (
                'id_reg' => 1511,
                'id_regmf' => 1217,
                'doors' => 5,
            ),
            11 => 
            array (
                'id_reg' => 1512,
                'id_regmf' => 1218,
                'doors' => 2,
            ),
            12 => 
            array (
                'id_reg' => 1513,
                'id_regmf' => 1218,
                'doors' => 3,
            ),
            13 => 
            array (
                'id_reg' => 1514,
                'id_regmf' => 1218,
                'doors' => 4,
            ),
            14 => 
            array (
                'id_reg' => 1515,
                'id_regmf' => 1218,
                'doors' => 5,
            ),
            15 => 
            array (
                'id_reg' => 1516,
                'id_regmf' => 1219,
                'doors' => 5,
            ),
            16 => 
            array (
                'id_reg' => 1517,
                'id_regmf' => 1220,
                'doors' => 5,
            ),
            17 => 
            array (
                'id_reg' => 1518,
                'id_regmf' => 1221,
                'doors' => 5,
            ),
            18 => 
            array (
                'id_reg' => 1519,
                'id_regmf' => 1222,
                'doors' => 2,
            ),
            19 => 
            array (
                'id_reg' => 1520,
                'id_regmf' => 1222,
                'doors' => 3,
            ),
            20 => 
            array (
                'id_reg' => 1521,
                'id_regmf' => 1222,
                'doors' => 4,
            ),
            21 => 
            array (
                'id_reg' => 1522,
                'id_regmf' => 1222,
                'doors' => 5,
            ),
            22 => 
            array (
                'id_reg' => 1523,
                'id_regmf' => 1223,
                'doors' => 2,
            ),
            23 => 
            array (
                'id_reg' => 1524,
                'id_regmf' => 1224,
                'doors' => 2,
            ),
            24 => 
            array (
                'id_reg' => 1525,
                'id_regmf' => 1225,
                'doors' => 2,
            ),
            25 => 
            array (
                'id_reg' => 1526,
                'id_regmf' => 1225,
                'doors' => 4,
            ),
            26 => 
            array (
                'id_reg' => 1527,
                'id_regmf' => 1227,
                'doors' => 2,
            ),
            27 => 
            array (
                'id_reg' => 1528,
                'id_regmf' => 1227,
                'doors' => 4,
            ),
            28 => 
            array (
                'id_reg' => 1529,
                'id_regmf' => 1226,
                'doors' => 2,
            ),
            29 => 
            array (
                'id_reg' => 1530,
                'id_regmf' => 1226,
                'doors' => 4,
            ),
            30 => 
            array (
                'id_reg' => 1531,
                'id_regmf' => 1228,
                'doors' => 4,
            ),
            31 => 
            array (
                'id_reg' => 1532,
                'id_regmf' => 1229,
                'doors' => 2,
            ),
            32 => 
            array (
                'id_reg' => 1533,
                'id_regmf' => 1229,
                'doors' => 4,
            ),
            33 => 
            array (
                'id_reg' => 1534,
                'id_regmf' => 1230,
                'doors' => 2,
            ),
            34 => 
            array (
                'id_reg' => 1535,
                'id_regmf' => 1230,
                'doors' => 4,
            ),
            35 => 
            array (
                'id_reg' => 1536,
                'id_regmf' => 1231,
                'doors' => 2,
            ),
            36 => 
            array (
                'id_reg' => 1537,
                'id_regmf' => 1232,
                'doors' => 2,
            ),
            37 => 
            array (
                'id_reg' => 1538,
                'id_regmf' => 1232,
                'doors' => 4,
            ),
            38 => 
            array (
                'id_reg' => 1539,
                'id_regmf' => 1233,
                'doors' => 3,
            ),
            39 => 
            array (
                'id_reg' => 1540,
                'id_regmf' => 1236,
                'doors' => 3,
            ),
            40 => 
            array (
                'id_reg' => 1541,
                'id_regmf' => 1236,
                'doors' => 5,
            ),
            41 => 
            array (
                'id_reg' => 1542,
                'id_regmf' => 1235,
                'doors' => 5,
            ),
            42 => 
            array (
                'id_reg' => 1543,
                'id_regmf' => 1234,
                'doors' => 3,
            ),
            43 => 
            array (
                'id_reg' => 1544,
                'id_regmf' => 1234,
                'doors' => 5,
            ),
            44 => 
            array (
                'id_reg' => 1545,
                'id_regmf' => 1237,
                'doors' => 4,
            ),
            45 => 
            array (
                'id_reg' => 1546,
                'id_regmf' => 1238,
                'doors' => 5,
            ),
            46 => 
            array (
                'id_reg' => 1547,
                'id_regmf' => 1242,
                'doors' => 5,
            ),
            47 => 
            array (
                'id_reg' => 1548,
                'id_regmf' => 1240,
                'doors' => 3,
            ),
            48 => 
            array (
                'id_reg' => 1549,
                'id_regmf' => 1240,
                'doors' => 4,
            ),
            49 => 
            array (
                'id_reg' => 1550,
                'id_regmf' => 1240,
                'doors' => 5,
            ),
            50 => 
            array (
                'id_reg' => 1551,
                'id_regmf' => 1243,
                'doors' => 3,
            ),
            51 => 
            array (
                'id_reg' => 1552,
                'id_regmf' => 1243,
                'doors' => 4,
            ),
            52 => 
            array (
                'id_reg' => 1553,
                'id_regmf' => 1243,
                'doors' => 5,
            ),
            53 => 
            array (
                'id_reg' => 1554,
                'id_regmf' => 1241,
                'doors' => 5,
            ),
            54 => 
            array (
                'id_reg' => 1555,
                'id_regmf' => 1239,
                'doors' => 5,
            ),
            55 => 
            array (
                'id_reg' => 1556,
                'id_regmf' => 1244,
                'doors' => 5,
            ),
            56 => 
            array (
                'id_reg' => 1557,
                'id_regmf' => 1245,
                'doors' => 5,
            ),
            57 => 
            array (
                'id_reg' => 1558,
                'id_regmf' => 1246,
                'doors' => 5,
            ),
            58 => 
            array (
                'id_reg' => 1559,
                'id_regmf' => 1247,
                'doors' => 2,
            ),
            59 => 
            array (
                'id_reg' => 1560,
                'id_regmf' => 1248,
                'doors' => 2,
            ),
            60 => 
            array (
                'id_reg' => 1561,
                'id_regmf' => 1249,
                'doors' => 5,
            ),
            61 => 
            array (
                'id_reg' => 1562,
                'id_regmf' => 1250,
                'doors' => 5,
            ),
            62 => 
            array (
                'id_reg' => 1563,
                'id_regmf' => 1251,
                'doors' => 5,
            ),
            63 => 
            array (
                'id_reg' => 1564,
                'id_regmf' => 1252,
                'doors' => 2,
            ),
            64 => 
            array (
                'id_reg' => 1565,
                'id_regmf' => 1252,
                'doors' => 4,
            ),
            65 => 
            array (
                'id_reg' => 1566,
                'id_regmf' => 1254,
                'doors' => 5,
            ),
            66 => 
            array (
                'id_reg' => 1567,
                'id_regmf' => 1253,
                'doors' => 5,
            ),
            67 => 
            array (
                'id_reg' => 1568,
                'id_regmf' => 1255,
                'doors' => 5,
            ),
            68 => 
            array (
                'id_reg' => 1569,
                'id_regmf' => 1256,
                'doors' => 0,
            ),
            69 => 
            array (
                'id_reg' => 1570,
                'id_regmf' => 1257,
                'doors' => 2,
            ),
            70 => 
            array (
                'id_reg' => 1571,
                'id_regmf' => 1258,
                'doors' => 4,
            ),
            71 => 
            array (
                'id_reg' => 1572,
                'id_regmf' => 1258,
                'doors' => 5,
            ),
            72 => 
            array (
                'id_reg' => 1573,
                'id_regmf' => 1259,
                'doors' => 4,
            ),
            73 => 
            array (
                'id_reg' => 1574,
                'id_regmf' => 1259,
                'doors' => 5,
            ),
            74 => 
            array (
                'id_reg' => 1575,
                'id_regmf' => 1260,
                'doors' => 5,
            ),
            75 => 
            array (
                'id_reg' => 1576,
                'id_regmf' => 1261,
                'doors' => 5,
            ),
            76 => 
            array (
                'id_reg' => 1577,
                'id_regmf' => 1262,
                'doors' => 2,
            ),
            77 => 
            array (
                'id_reg' => 1578,
                'id_regmf' => 1263,
                'doors' => 2,
            ),
            78 => 
            array (
                'id_reg' => 1579,
                'id_regmf' => 1264,
                'doors' => 2,
            ),
            79 => 
            array (
                'id_reg' => 1580,
                'id_regmf' => 1266,
                'doors' => 2,
            ),
            80 => 
            array (
                'id_reg' => 1581,
                'id_regmf' => 1266,
                'doors' => 3,
            ),
            81 => 
            array (
                'id_reg' => 1582,
                'id_regmf' => 1265,
                'doors' => 3,
            ),
            82 => 
            array (
                'id_reg' => 1583,
                'id_regmf' => 1267,
                'doors' => 5,
            ),
            83 => 
            array (
                'id_reg' => 1584,
                'id_regmf' => 1269,
                'doors' => 5,
            ),
            84 => 
            array (
                'id_reg' => 1585,
                'id_regmf' => 1268,
                'doors' => 5,
            ),
            85 => 
            array (
                'id_reg' => 1586,
                'id_regmf' => 1270,
                'doors' => 4,
            ),
            86 => 
            array (
                'id_reg' => 1587,
                'id_regmf' => 1271,
                'doors' => 5,
            ),
            87 => 
            array (
                'id_reg' => 1588,
                'id_regmf' => 1272,
                'doors' => 4,
            ),
            88 => 
            array (
                'id_reg' => 1589,
                'id_regmf' => 1273,
                'doors' => 2,
            ),
            89 => 
            array (
                'id_reg' => 1590,
                'id_regmf' => 1273,
                'doors' => 4,
            ),
            90 => 
            array (
                'id_reg' => 1591,
                'id_regmf' => 1273,
                'doors' => 5,
            ),
            91 => 
            array (
                'id_reg' => 1592,
                'id_regmf' => 1274,
                'doors' => 4,
            ),
            92 => 
            array (
                'id_reg' => 1593,
                'id_regmf' => 1275,
                'doors' => 0,
            ),
            93 => 
            array (
                'id_reg' => 1594,
                'id_regmf' => 1276,
                'doors' => 2,
            ),
            94 => 
            array (
                'id_reg' => 1595,
                'id_regmf' => 1278,
                'doors' => 3,
            ),
            95 => 
            array (
                'id_reg' => 1596,
                'id_regmf' => 1278,
                'doors' => 4,
            ),
            96 => 
            array (
                'id_reg' => 1597,
                'id_regmf' => 1278,
                'doors' => 5,
            ),
            97 => 
            array (
                'id_reg' => 1598,
                'id_regmf' => 1277,
                'doors' => 3,
            ),
            98 => 
            array (
                'id_reg' => 1599,
                'id_regmf' => 1277,
                'doors' => 5,
            ),
            99 => 
            array (
                'id_reg' => 1600,
                'id_regmf' => 1279,
                'doors' => 0,
            ),
            100 => 
            array (
                'id_reg' => 1601,
                'id_regmf' => 1279,
                'doors' => 4,
            ),
            101 => 
            array (
                'id_reg' => 1602,
                'id_regmf' => 1281,
                'doors' => 4,
            ),
            102 => 
            array (
                'id_reg' => 1603,
                'id_regmf' => 1281,
                'doors' => 5,
            ),
            103 => 
            array (
                'id_reg' => 1604,
                'id_regmf' => 1280,
                'doors' => 4,
            ),
            104 => 
            array (
                'id_reg' => 1605,
                'id_regmf' => 1280,
                'doors' => 5,
            ),
            105 => 
            array (
                'id_reg' => 1606,
                'id_regmf' => 1282,
                'doors' => 4,
            ),
            106 => 
            array (
                'id_reg' => 1607,
                'id_regmf' => 1282,
                'doors' => 5,
            ),
            107 => 
            array (
                'id_reg' => 1608,
                'id_regmf' => 1283,
                'doors' => 4,
            ),
            108 => 
            array (
                'id_reg' => 1609,
                'id_regmf' => 1284,
                'doors' => 2,
            ),
            109 => 
            array (
                'id_reg' => 1610,
                'id_regmf' => 1284,
                'doors' => 3,
            ),
            110 => 
            array (
                'id_reg' => 1611,
                'id_regmf' => 1285,
                'doors' => 5,
            ),
            111 => 
            array (
                'id_reg' => 1612,
                'id_regmf' => 1286,
                'doors' => 4,
            ),
            112 => 
            array (
                'id_reg' => 1613,
                'id_regmf' => 1287,
                'doors' => 4,
            ),
            113 => 
            array (
                'id_reg' => 1614,
                'id_regmf' => 1288,
                'doors' => 4,
            ),
            114 => 
            array (
                'id_reg' => 1615,
                'id_regmf' => 1290,
                'doors' => 2,
            ),
            115 => 
            array (
                'id_reg' => 1616,
                'id_regmf' => 1289,
                'doors' => 2,
            ),
            116 => 
            array (
                'id_reg' => 1617,
                'id_regmf' => 1291,
                'doors' => 4,
            ),
            117 => 
            array (
                'id_reg' => 1618,
                'id_regmf' => 1292,
                'doors' => 3,
            ),
            118 => 
            array (
                'id_reg' => 1619,
                'id_regmf' => 1293,
                'doors' => 3,
            ),
            119 => 
            array (
                'id_reg' => 1620,
                'id_regmf' => 1294,
                'doors' => 3,
            ),
            120 => 
            array (
                'id_reg' => 1621,
                'id_regmf' => 1295,
                'doors' => 2,
            ),
            121 => 
            array (
                'id_reg' => 1622,
                'id_regmf' => 1295,
                'doors' => 4,
            ),
            122 => 
            array (
                'id_reg' => 1623,
                'id_regmf' => 1296,
                'doors' => 2,
            ),
            123 => 
            array (
                'id_reg' => 1624,
                'id_regmf' => 1296,
                'doors' => 4,
            ),
            124 => 
            array (
                'id_reg' => 1625,
                'id_regmf' => 1297,
                'doors' => 3,
            ),
            125 => 
            array (
                'id_reg' => 1626,
                'id_regmf' => 1298,
                'doors' => 5,
            ),
            126 => 
            array (
                'id_reg' => 1627,
                'id_regmf' => 1300,
                'doors' => 5,
            ),
            127 => 
            array (
                'id_reg' => 1628,
                'id_regmf' => 1299,
                'doors' => 5,
            ),
            128 => 
            array (
                'id_reg' => 1629,
                'id_regmf' => 1302,
                'doors' => 4,
            ),
            129 => 
            array (
                'id_reg' => 1630,
                'id_regmf' => 1302,
                'doors' => 5,
            ),
            130 => 
            array (
                'id_reg' => 1631,
                'id_regmf' => 1301,
                'doors' => 4,
            ),
            131 => 
            array (
                'id_reg' => 1632,
                'id_regmf' => 1301,
                'doors' => 5,
            ),
            132 => 
            array (
                'id_reg' => 1633,
                'id_regmf' => 1304,
                'doors' => 3,
            ),
            133 => 
            array (
                'id_reg' => 1634,
                'id_regmf' => 1304,
                'doors' => 4,
            ),
            134 => 
            array (
                'id_reg' => 1635,
                'id_regmf' => 1304,
                'doors' => 5,
            ),
            135 => 
            array (
                'id_reg' => 1636,
                'id_regmf' => 1303,
                'doors' => 3,
            ),
            136 => 
            array (
                'id_reg' => 1637,
                'id_regmf' => 1303,
                'doors' => 4,
            ),
            137 => 
            array (
                'id_reg' => 1638,
                'id_regmf' => 1303,
                'doors' => 5,
            ),
            138 => 
            array (
                'id_reg' => 1639,
                'id_regmf' => 1305,
                'doors' => 4,
            ),
            139 => 
            array (
                'id_reg' => 1640,
                'id_regmf' => 1306,
                'doors' => 0,
            ),
            140 => 
            array (
                'id_reg' => 1641,
                'id_regmf' => 1306,
                'doors' => 4,
            ),
            141 => 
            array (
                'id_reg' => 1642,
                'id_regmf' => 1307,
                'doors' => 2,
            ),
            142 => 
            array (
                'id_reg' => 1643,
                'id_regmf' => 1307,
                'doors' => 4,
            ),
            143 => 
            array (
                'id_reg' => 1644,
                'id_regmf' => 1307,
                'doors' => 5,
            ),
            144 => 
            array (
                'id_reg' => 1645,
                'id_regmf' => 1308,
                'doors' => 4,
            ),
            145 => 
            array (
                'id_reg' => 1646,
                'id_regmf' => 1308,
                'doors' => 5,
            ),
            146 => 
            array (
                'id_reg' => 1647,
                'id_regmf' => 1309,
                'doors' => 4,
            ),
            147 => 
            array (
                'id_reg' => 1648,
                'id_regmf' => 1310,
                'doors' => 2,
            ),
            148 => 
            array (
                'id_reg' => 1649,
                'id_regmf' => 1311,
                'doors' => 2,
            ),
            149 => 
            array (
                'id_reg' => 1650,
                'id_regmf' => 1313,
                'doors' => 4,
            ),
            150 => 
            array (
                'id_reg' => 1651,
                'id_regmf' => 1313,
                'doors' => 5,
            ),
            151 => 
            array (
                'id_reg' => 1652,
                'id_regmf' => 1313,
                'doors' => 6,
            ),
            152 => 
            array (
                'id_reg' => 1653,
                'id_regmf' => 1312,
                'doors' => 4,
            ),
            153 => 
            array (
                'id_reg' => 1654,
                'id_regmf' => 1312,
                'doors' => 5,
            ),
            154 => 
            array (
                'id_reg' => 1655,
                'id_regmf' => 1312,
                'doors' => 6,
            ),
            155 => 
            array (
                'id_reg' => 1656,
                'id_regmf' => 1315,
                'doors' => 5,
            ),
            156 => 
            array (
                'id_reg' => 1657,
                'id_regmf' => 1314,
                'doors' => 5,
            ),
            157 => 
            array (
                'id_reg' => 1658,
                'id_regmf' => 1316,
                'doors' => 2,
            ),
            158 => 
            array (
                'id_reg' => 1659,
                'id_regmf' => 1316,
                'doors' => 3,
            ),
            159 => 
            array (
                'id_reg' => 1660,
                'id_regmf' => 1316,
                'doors' => 4,
            ),
            160 => 
            array (
                'id_reg' => 1661,
                'id_regmf' => 1316,
                'doors' => 5,
            ),
            161 => 
            array (
                'id_reg' => 1662,
                'id_regmf' => 1316,
                'doors' => 6,
            ),
            162 => 
            array (
                'id_reg' => 1663,
                'id_regmf' => 1317,
                'doors' => 2,
            ),
            163 => 
            array (
                'id_reg' => 1664,
                'id_regmf' => 1317,
                'doors' => 5,
            ),
            164 => 
            array (
                'id_reg' => 1665,
                'id_regmf' => 1317,
                'doors' => 6,
            ),
            165 => 
            array (
                'id_reg' => 1666,
                'id_regmf' => 1318,
                'doors' => 2,
            ),
            166 => 
            array (
                'id_reg' => 1667,
                'id_regmf' => 1318,
                'doors' => 4,
            ),
            167 => 
            array (
                'id_reg' => 1668,
                'id_regmf' => 1318,
                'doors' => 5,
            ),
            168 => 
            array (
                'id_reg' => 1669,
                'id_regmf' => 1320,
                'doors' => 4,
            ),
            169 => 
            array (
                'id_reg' => 1670,
                'id_regmf' => 1320,
                'doors' => 5,
            ),
            170 => 
            array (
                'id_reg' => 1671,
                'id_regmf' => 1319,
                'doors' => 4,
            ),
            171 => 
            array (
                'id_reg' => 1672,
                'id_regmf' => 1319,
                'doors' => 5,
            ),
            172 => 
            array (
                'id_reg' => 1673,
                'id_regmf' => 1321,
                'doors' => 2,
            ),
            173 => 
            array (
                'id_reg' => 1674,
                'id_regmf' => 1322,
                'doors' => 2,
            ),
            174 => 
            array (
                'id_reg' => 1675,
                'id_regmf' => 1323,
                'doors' => 4,
            ),
            175 => 
            array (
                'id_reg' => 1676,
                'id_regmf' => 1324,
                'doors' => 4,
            ),
            176 => 
            array (
                'id_reg' => 1677,
                'id_regmf' => 1325,
                'doors' => 2,
            ),
            177 => 
            array (
                'id_reg' => 1678,
                'id_regmf' => 1326,
                'doors' => 4,
            ),
            178 => 
            array (
                'id_reg' => 1679,
                'id_regmf' => 1327,
                'doors' => 2,
            ),
            179 => 
            array (
                'id_reg' => 1680,
                'id_regmf' => 1328,
                'doors' => 5,
            ),
            180 => 
            array (
                'id_reg' => 1681,
                'id_regmf' => 1329,
                'doors' => 2,
            ),
            181 => 
            array (
                'id_reg' => 1682,
                'id_regmf' => 1329,
                'doors' => 3,
            ),
            182 => 
            array (
                'id_reg' => 1683,
                'id_regmf' => 1330,
                'doors' => 4,
            ),
            183 => 
            array (
                'id_reg' => 1684,
                'id_regmf' => 1330,
                'doors' => 5,
            ),
            184 => 
            array (
                'id_reg' => 1685,
                'id_regmf' => 1332,
                'doors' => 2,
            ),
            185 => 
            array (
                'id_reg' => 1686,
                'id_regmf' => 1332,
                'doors' => 5,
            ),
            186 => 
            array (
                'id_reg' => 1687,
                'id_regmf' => 1331,
                'doors' => 2,
            ),
            187 => 
            array (
                'id_reg' => 1688,
                'id_regmf' => 1334,
                'doors' => 3,
            ),
            188 => 
            array (
                'id_reg' => 1689,
                'id_regmf' => 1334,
                'doors' => 5,
            ),
            189 => 
            array (
                'id_reg' => 1690,
                'id_regmf' => 1333,
                'doors' => 3,
            ),
            190 => 
            array (
                'id_reg' => 1691,
                'id_regmf' => 1333,
                'doors' => 5,
            ),
            191 => 
            array (
                'id_reg' => 1692,
                'id_regmf' => 1336,
                'doors' => 3,
            ),
            192 => 
            array (
                'id_reg' => 1693,
                'id_regmf' => 1335,
                'doors' => 3,
            ),
            193 => 
            array (
                'id_reg' => 1694,
                'id_regmf' => 1337,
                'doors' => 5,
            ),
            194 => 
            array (
                'id_reg' => 1695,
                'id_regmf' => 1338,
                'doors' => 3,
            ),
            195 => 
            array (
                'id_reg' => 1696,
                'id_regmf' => 1338,
                'doors' => 5,
            ),
            196 => 
            array (
                'id_reg' => 1697,
                'id_regmf' => 1339,
                'doors' => 0,
            ),
            197 => 
            array (
                'id_reg' => 1698,
                'id_regmf' => 1340,
                'doors' => 0,
            ),
            198 => 
            array (
                'id_reg' => 1699,
                'id_regmf' => 1341,
                'doors' => 0,
            ),
            199 => 
            array (
                'id_reg' => 1700,
                'id_regmf' => 1342,
                'doors' => 3,
            ),
            200 => 
            array (
                'id_reg' => 1701,
                'id_regmf' => 1343,
                'doors' => 3,
            ),
            201 => 
            array (
                'id_reg' => 1702,
                'id_regmf' => 1344,
                'doors' => 4,
            ),
            202 => 
            array (
                'id_reg' => 1703,
                'id_regmf' => 1346,
                'doors' => 5,
            ),
            203 => 
            array (
                'id_reg' => 1704,
                'id_regmf' => 1345,
                'doors' => 0,
            ),
            204 => 
            array (
                'id_reg' => 1705,
                'id_regmf' => 1347,
                'doors' => 2,
            ),
            205 => 
            array (
                'id_reg' => 1706,
                'id_regmf' => 1348,
                'doors' => 4,
            ),
            206 => 
            array (
                'id_reg' => 1707,
                'id_regmf' => 1348,
                'doors' => 5,
            ),
            207 => 
            array (
                'id_reg' => 1708,
                'id_regmf' => 1349,
                'doors' => 0,
            ),
            208 => 
            array (
                'id_reg' => 1709,
                'id_regmf' => 1350,
                'doors' => 0,
            ),
            209 => 
            array (
                'id_reg' => 1710,
                'id_regmf' => 1351,
                'doors' => 2,
            ),
            210 => 
            array (
                'id_reg' => 1711,
                'id_regmf' => 1352,
                'doors' => 2,
            ),
            211 => 
            array (
                'id_reg' => 1712,
                'id_regmf' => 1353,
                'doors' => 2,
            ),
            212 => 
            array (
                'id_reg' => 1713,
                'id_regmf' => 1354,
                'doors' => 3,
            ),
            213 => 
            array (
                'id_reg' => 1714,
                'id_regmf' => 1355,
                'doors' => 3,
            ),
            214 => 
            array (
                'id_reg' => 1715,
                'id_regmf' => 1356,
                'doors' => 2,
            ),
            215 => 
            array (
                'id_reg' => 1716,
                'id_regmf' => 1357,
                'doors' => 3,
            ),
            216 => 
            array (
                'id_reg' => 1717,
                'id_regmf' => 1358,
                'doors' => 3,
            ),
            217 => 
            array (
                'id_reg' => 1718,
                'id_regmf' => 1359,
                'doors' => 3,
            ),
            218 => 
            array (
                'id_reg' => 1719,
                'id_regmf' => 1360,
                'doors' => 4,
            ),
            219 => 
            array (
                'id_reg' => 1720,
                'id_regmf' => 1361,
                'doors' => 5,
            ),
            220 => 
            array (
                'id_reg' => 1721,
                'id_regmf' => 1362,
                'doors' => 0,
            ),
            221 => 
            array (
                'id_reg' => 1722,
                'id_regmf' => 1363,
                'doors' => 5,
            ),
            222 => 
            array (
                'id_reg' => 1723,
                'id_regmf' => 1364,
                'doors' => 4,
            ),
            223 => 
            array (
                'id_reg' => 1724,
                'id_regmf' => 1365,
                'doors' => 3,
            ),
            224 => 
            array (
                'id_reg' => 1725,
                'id_regmf' => 1365,
                'doors' => 5,
            ),
            225 => 
            array (
                'id_reg' => 1726,
                'id_regmf' => 1366,
                'doors' => 2,
            ),
            226 => 
            array (
                'id_reg' => 1727,
                'id_regmf' => 1367,
                'doors' => 4,
            ),
            227 => 
            array (
                'id_reg' => 1728,
                'id_regmf' => 1368,
                'doors' => 5,
            ),
            228 => 
            array (
                'id_reg' => 1729,
                'id_regmf' => 1369,
                'doors' => 5,
            ),
            229 => 
            array (
                'id_reg' => 1730,
                'id_regmf' => 1371,
                'doors' => 2,
            ),
            230 => 
            array (
                'id_reg' => 1731,
                'id_regmf' => 1371,
                'doors' => 4,
            ),
            231 => 
            array (
                'id_reg' => 1732,
                'id_regmf' => 1371,
                'doors' => 5,
            ),
            232 => 
            array (
                'id_reg' => 1733,
                'id_regmf' => 1370,
                'doors' => 4,
            ),
            233 => 
            array (
                'id_reg' => 1734,
                'id_regmf' => 1372,
                'doors' => 2,
            ),
            234 => 
            array (
                'id_reg' => 1735,
                'id_regmf' => 1373,
                'doors' => 2,
            ),
            235 => 
            array (
                'id_reg' => 1736,
                'id_regmf' => 1374,
                'doors' => 4,
            ),
            236 => 
            array (
                'id_reg' => 1737,
                'id_regmf' => 1375,
                'doors' => 4,
            ),
            237 => 
            array (
                'id_reg' => 1738,
                'id_regmf' => 1375,
                'doors' => 5,
            ),
            238 => 
            array (
                'id_reg' => 1739,
                'id_regmf' => 1376,
                'doors' => 5,
            ),
            239 => 
            array (
                'id_reg' => 1740,
                'id_regmf' => 1377,
                'doors' => 2,
            ),
            240 => 
            array (
                'id_reg' => 1741,
                'id_regmf' => 1378,
                'doors' => 3,
            ),
            241 => 
            array (
                'id_reg' => 1742,
                'id_regmf' => 1379,
                'doors' => 5,
            ),
            242 => 
            array (
                'id_reg' => 1743,
                'id_regmf' => 1380,
                'doors' => 5,
            ),
            243 => 
            array (
                'id_reg' => 1744,
                'id_regmf' => 1381,
                'doors' => 5,
            ),
            244 => 
            array (
                'id_reg' => 1745,
                'id_regmf' => 1382,
                'doors' => 3,
            ),
            245 => 
            array (
                'id_reg' => 1746,
                'id_regmf' => 1382,
                'doors' => 5,
            ),
            246 => 
            array (
                'id_reg' => 1747,
                'id_regmf' => 1383,
                'doors' => 3,
            ),
            247 => 
            array (
                'id_reg' => 1748,
                'id_regmf' => 1383,
                'doors' => 5,
            ),
            248 => 
            array (
                'id_reg' => 1749,
                'id_regmf' => 1385,
                'doors' => 4,
            ),
            249 => 
            array (
                'id_reg' => 1750,
                'id_regmf' => 1384,
                'doors' => 4,
            ),
            250 => 
            array (
                'id_reg' => 1751,
                'id_regmf' => 1384,
                'doors' => 5,
            ),
            251 => 
            array (
                'id_reg' => 1752,
                'id_regmf' => 1386,
                'doors' => 2,
            ),
            252 => 
            array (
                'id_reg' => 1753,
                'id_regmf' => 1386,
                'doors' => 4,
            ),
            253 => 
            array (
                'id_reg' => 1754,
                'id_regmf' => 1387,
                'doors' => 0,
            ),
            254 => 
            array (
                'id_reg' => 1755,
                'id_regmf' => 1388,
                'doors' => 0,
            ),
            255 => 
            array (
                'id_reg' => 1756,
                'id_regmf' => 1389,
                'doors' => 2,
            ),
            256 => 
            array (
                'id_reg' => 1757,
                'id_regmf' => 1390,
                'doors' => 0,
            ),
            257 => 
            array (
                'id_reg' => 1758,
                'id_regmf' => 1391,
                'doors' => 2,
            ),
            258 => 
            array (
                'id_reg' => 1759,
                'id_regmf' => 1392,
                'doors' => 0,
            ),
            259 => 
            array (
                'id_reg' => 1760,
                'id_regmf' => 1393,
                'doors' => 2,
            ),
            260 => 
            array (
                'id_reg' => 1761,
                'id_regmf' => 1394,
                'doors' => 2,
            ),
            261 => 
            array (
                'id_reg' => 1762,
                'id_regmf' => 1395,
                'doors' => 4,
            ),
            262 => 
            array (
                'id_reg' => 1763,
                'id_regmf' => 1396,
                'doors' => 3,
            ),
            263 => 
            array (
                'id_reg' => 1764,
                'id_regmf' => 1397,
                'doors' => 3,
            ),
            264 => 
            array (
                'id_reg' => 1765,
                'id_regmf' => 1398,
                'doors' => 2,
            ),
            265 => 
            array (
                'id_reg' => 1766,
                'id_regmf' => 1398,
                'doors' => 3,
            ),
            266 => 
            array (
                'id_reg' => 1767,
                'id_regmf' => 1399,
                'doors' => 2,
            ),
            267 => 
            array (
                'id_reg' => 1768,
                'id_regmf' => 1400,
                'doors' => 0,
            ),
            268 => 
            array (
                'id_reg' => 1769,
                'id_regmf' => 1401,
                'doors' => 0,
            ),
            269 => 
            array (
                'id_reg' => 1770,
                'id_regmf' => 1402,
                'doors' => 0,
            ),
            270 => 
            array (
                'id_reg' => 1771,
                'id_regmf' => 1403,
                'doors' => 0,
            ),
            271 => 
            array (
                'id_reg' => 1772,
                'id_regmf' => 1404,
                'doors' => 2,
            ),
            272 => 
            array (
                'id_reg' => 1773,
                'id_regmf' => 1405,
                'doors' => 3,
            ),
            273 => 
            array (
                'id_reg' => 1774,
                'id_regmf' => 1406,
                'doors' => 2,
            ),
            274 => 
            array (
                'id_reg' => 1775,
                'id_regmf' => 1406,
                'doors' => 4,
            ),
            275 => 
            array (
                'id_reg' => 1776,
                'id_regmf' => 1407,
                'doors' => 5,
            ),
            276 => 
            array (
                'id_reg' => 1777,
                'id_regmf' => 1408,
                'doors' => 2,
            ),
            277 => 
            array (
                'id_reg' => 1778,
                'id_regmf' => 1409,
                'doors' => 4,
            ),
            278 => 
            array (
                'id_reg' => 1779,
                'id_regmf' => 1410,
                'doors' => 4,
            ),
            279 => 
            array (
                'id_reg' => 1780,
                'id_regmf' => 1411,
                'doors' => 0,
            ),
            280 => 
            array (
                'id_reg' => 1781,
                'id_regmf' => 1412,
                'doors' => 4,
            ),
            281 => 
            array (
                'id_reg' => 1782,
                'id_regmf' => 1413,
                'doors' => 2,
            ),
            282 => 
            array (
                'id_reg' => 1783,
                'id_regmf' => 1414,
                'doors' => 2,
            ),
            283 => 
            array (
                'id_reg' => 1784,
                'id_regmf' => 1415,
                'doors' => 3,
            ),
            284 => 
            array (
                'id_reg' => 1785,
                'id_regmf' => 1416,
                'doors' => 4,
            ),
            285 => 
            array (
                'id_reg' => 1786,
                'id_regmf' => 1418,
                'doors' => 5,
            ),
            286 => 
            array (
                'id_reg' => 1787,
                'id_regmf' => 1417,
                'doors' => 5,
            ),
            287 => 
            array (
                'id_reg' => 1788,
                'id_regmf' => 1419,
                'doors' => 3,
            ),
            288 => 
            array (
                'id_reg' => 1789,
                'id_regmf' => 1420,
                'doors' => 5,
            ),
            289 => 
            array (
                'id_reg' => 1790,
                'id_regmf' => 1421,
                'doors' => 5,
            ),
            290 => 
            array (
                'id_reg' => 1791,
                'id_regmf' => 1422,
                'doors' => 5,
            ),
            291 => 
            array (
                'id_reg' => 1792,
                'id_regmf' => 1423,
                'doors' => 5,
            ),
            292 => 
            array (
                'id_reg' => 1793,
                'id_regmf' => 1424,
                'doors' => 2,
            ),
            293 => 
            array (
                'id_reg' => 1794,
                'id_regmf' => 1424,
                'doors' => 3,
            ),
            294 => 
            array (
                'id_reg' => 1795,
                'id_regmf' => 1425,
                'doors' => 3,
            ),
            295 => 
            array (
                'id_reg' => 1796,
                'id_regmf' => 1426,
                'doors' => 2,
            ),
            296 => 
            array (
                'id_reg' => 1797,
                'id_regmf' => 1426,
                'doors' => 4,
            ),
            297 => 
            array (
                'id_reg' => 1798,
                'id_regmf' => 1427,
                'doors' => 3,
            ),
            298 => 
            array (
                'id_reg' => 1799,
                'id_regmf' => 1428,
                'doors' => 0,
            ),
            299 => 
            array (
                'id_reg' => 1800,
                'id_regmf' => 1429,
                'doors' => 2,
            ),
            300 => 
            array (
                'id_reg' => 1801,
                'id_regmf' => 1430,
                'doors' => 5,
            ),
            301 => 
            array (
                'id_reg' => 1802,
                'id_regmf' => 1431,
                'doors' => 5,
            ),
            302 => 
            array (
                'id_reg' => 1803,
                'id_regmf' => 1432,
                'doors' => 5,
            ),
            303 => 
            array (
                'id_reg' => 1804,
                'id_regmf' => 1433,
                'doors' => 5,
            ),
            304 => 
            array (
                'id_reg' => 1805,
                'id_regmf' => 1434,
                'doors' => 3,
            ),
            305 => 
            array (
                'id_reg' => 1806,
                'id_regmf' => 1435,
                'doors' => 5,
            ),
            306 => 
            array (
                'id_reg' => 1807,
                'id_regmf' => 1436,
                'doors' => 4,
            ),
            307 => 
            array (
                'id_reg' => 1808,
                'id_regmf' => 1437,
                'doors' => 2,
            ),
            308 => 
            array (
                'id_reg' => 1809,
                'id_regmf' => 1438,
                'doors' => 5,
            ),
            309 => 
            array (
                'id_reg' => 1810,
                'id_regmf' => 1439,
                'doors' => 5,
            ),
            310 => 
            array (
                'id_reg' => 1811,
                'id_regmf' => 1440,
                'doors' => 5,
            ),
            311 => 
            array (
                'id_reg' => 1812,
                'id_regmf' => 1441,
                'doors' => 2,
            ),
            312 => 
            array (
                'id_reg' => 1813,
                'id_regmf' => 1442,
                'doors' => 2,
            ),
            313 => 
            array (
                'id_reg' => 1814,
                'id_regmf' => 1443,
                'doors' => 4,
            ),
            314 => 
            array (
                'id_reg' => 1815,
                'id_regmf' => 1444,
                'doors' => 3,
            ),
            315 => 
            array (
                'id_reg' => 1816,
                'id_regmf' => 1445,
                'doors' => 0,
            ),
            316 => 
            array (
                'id_reg' => 1817,
                'id_regmf' => 1446,
                'doors' => 0,
            ),
            317 => 
            array (
                'id_reg' => 1818,
                'id_regmf' => 1447,
                'doors' => 0,
            ),
            318 => 
            array (
                'id_reg' => 1819,
                'id_regmf' => 1448,
                'doors' => 0,
            ),
            319 => 
            array (
                'id_reg' => 1820,
                'id_regmf' => 1449,
                'doors' => 0,
            ),
            320 => 
            array (
                'id_reg' => 1821,
                'id_regmf' => 1450,
                'doors' => 2,
            ),
            321 => 
            array (
                'id_reg' => 1822,
                'id_regmf' => 1451,
                'doors' => 4,
            ),
            322 => 
            array (
                'id_reg' => 1823,
                'id_regmf' => 1452,
                'doors' => 4,
            ),
            323 => 
            array (
                'id_reg' => 1824,
                'id_regmf' => 1453,
                'doors' => 4,
            ),
            324 => 
            array (
                'id_reg' => 1825,
                'id_regmf' => 1454,
                'doors' => 2,
            ),
            325 => 
            array (
                'id_reg' => 1826,
                'id_regmf' => 1455,
                'doors' => 4,
            ),
            326 => 
            array (
                'id_reg' => 1827,
                'id_regmf' => 1456,
                'doors' => 4,
            ),
            327 => 
            array (
                'id_reg' => 1828,
                'id_regmf' => 1457,
                'doors' => 2,
            ),
            328 => 
            array (
                'id_reg' => 1829,
                'id_regmf' => 1459,
                'doors' => 4,
            ),
            329 => 
            array (
                'id_reg' => 1830,
                'id_regmf' => 1458,
                'doors' => 4,
            ),
            330 => 
            array (
                'id_reg' => 1831,
                'id_regmf' => 1458,
                'doors' => 5,
            ),
            331 => 
            array (
                'id_reg' => 1832,
                'id_regmf' => 1461,
                'doors' => 5,
            ),
            332 => 
            array (
                'id_reg' => 1833,
                'id_regmf' => 1462,
                'doors' => 4,
            ),
            333 => 
            array (
                'id_reg' => 1834,
                'id_regmf' => 1462,
                'doors' => 5,
            ),
            334 => 
            array (
                'id_reg' => 1835,
                'id_regmf' => 1460,
                'doors' => 4,
            ),
            335 => 
            array (
                'id_reg' => 1836,
                'id_regmf' => 1460,
                'doors' => 5,
            ),
            336 => 
            array (
                'id_reg' => 1837,
                'id_regmf' => 1463,
                'doors' => 5,
            ),
            337 => 
            array (
                'id_reg' => 1838,
                'id_regmf' => 1464,
                'doors' => 4,
            ),
            338 => 
            array (
                'id_reg' => 1839,
                'id_regmf' => 1465,
                'doors' => 2,
            ),
            339 => 
            array (
                'id_reg' => 1840,
                'id_regmf' => 1467,
                'doors' => 2,
            ),
            340 => 
            array (
                'id_reg' => 1841,
                'id_regmf' => 1466,
                'doors' => 2,
            ),
            341 => 
            array (
                'id_reg' => 1842,
                'id_regmf' => 1468,
                'doors' => 2,
            ),
            342 => 
            array (
                'id_reg' => 1843,
                'id_regmf' => 1469,
                'doors' => 2,
            ),
            343 => 
            array (
                'id_reg' => 1844,
                'id_regmf' => 1470,
                'doors' => 4,
            ),
            344 => 
            array (
                'id_reg' => 1845,
                'id_regmf' => 1471,
                'doors' => 4,
            ),
            345 => 
            array (
                'id_reg' => 1846,
                'id_regmf' => 1471,
                'doors' => 5,
            ),
            346 => 
            array (
                'id_reg' => 1847,
                'id_regmf' => 1472,
                'doors' => 4,
            ),
            347 => 
            array (
                'id_reg' => 1848,
                'id_regmf' => 1472,
                'doors' => 5,
            ),
            348 => 
            array (
                'id_reg' => 1849,
                'id_regmf' => 1473,
                'doors' => 4,
            ),
            349 => 
            array (
                'id_reg' => 1850,
                'id_regmf' => 1474,
                'doors' => 3,
            ),
            350 => 
            array (
                'id_reg' => 1851,
                'id_regmf' => 1474,
                'doors' => 4,
            ),
            351 => 
            array (
                'id_reg' => 1852,
                'id_regmf' => 1474,
                'doors' => 5,
            ),
            352 => 
            array (
                'id_reg' => 1853,
                'id_regmf' => 1475,
                'doors' => 4,
            ),
            353 => 
            array (
                'id_reg' => 1854,
                'id_regmf' => 1476,
                'doors' => 3,
            ),
            354 => 
            array (
                'id_reg' => 1855,
                'id_regmf' => 1476,
                'doors' => 4,
            ),
            355 => 
            array (
                'id_reg' => 1856,
                'id_regmf' => 1477,
                'doors' => 4,
            ),
            356 => 
            array (
                'id_reg' => 1857,
                'id_regmf' => 1478,
                'doors' => 5,
            ),
            357 => 
            array (
                'id_reg' => 1858,
                'id_regmf' => 1479,
                'doors' => 5,
            ),
            358 => 
            array (
                'id_reg' => 1859,
                'id_regmf' => 1480,
                'doors' => 4,
            ),
            359 => 
            array (
                'id_reg' => 1860,
                'id_regmf' => 1481,
                'doors' => 4,
            ),
            360 => 
            array (
                'id_reg' => 1861,
                'id_regmf' => 1482,
                'doors' => 5,
            ),
            361 => 
            array (
                'id_reg' => 1862,
                'id_regmf' => 1483,
                'doors' => 2,
            ),
            362 => 
            array (
                'id_reg' => 1863,
                'id_regmf' => 1483,
                'doors' => 5,
            ),
            363 => 
            array (
                'id_reg' => 1864,
                'id_regmf' => 1485,
                'doors' => 4,
            ),
            364 => 
            array (
                'id_reg' => 1865,
                'id_regmf' => 1484,
                'doors' => 2,
            ),
            365 => 
            array (
                'id_reg' => 1866,
                'id_regmf' => 1484,
                'doors' => 4,
            ),
            366 => 
            array (
                'id_reg' => 1867,
                'id_regmf' => 1486,
                'doors' => 5,
            ),
            367 => 
            array (
                'id_reg' => 1868,
                'id_regmf' => 1487,
                'doors' => 2,
            ),
            368 => 
            array (
                'id_reg' => 1869,
                'id_regmf' => 1487,
                'doors' => 4,
            ),
            369 => 
            array (
                'id_reg' => 1870,
                'id_regmf' => 1487,
                'doors' => 5,
            ),
            370 => 
            array (
                'id_reg' => 1871,
                'id_regmf' => 1487,
                'doors' => 6,
            ),
            371 => 
            array (
                'id_reg' => 1872,
                'id_regmf' => 1488,
                'doors' => 2,
            ),
            372 => 
            array (
                'id_reg' => 1873,
                'id_regmf' => 1488,
                'doors' => 4,
            ),
            373 => 
            array (
                'id_reg' => 1874,
                'id_regmf' => 1489,
                'doors' => 5,
            ),
            374 => 
            array (
                'id_reg' => 1875,
                'id_regmf' => 1490,
                'doors' => 5,
            ),
            375 => 
            array (
                'id_reg' => 1876,
                'id_regmf' => 1492,
                'doors' => 3,
            ),
            376 => 
            array (
                'id_reg' => 1877,
                'id_regmf' => 1492,
                'doors' => 5,
            ),
            377 => 
            array (
                'id_reg' => 1878,
                'id_regmf' => 1491,
                'doors' => 3,
            ),
            378 => 
            array (
                'id_reg' => 1879,
                'id_regmf' => 1491,
                'doors' => 5,
            ),
            379 => 
            array (
                'id_reg' => 1880,
                'id_regmf' => 1493,
                'doors' => 5,
            ),
            380 => 
            array (
                'id_reg' => 1881,
                'id_regmf' => 1494,
                'doors' => 5,
            ),
            381 => 
            array (
                'id_reg' => 1882,
                'id_regmf' => 1495,
                'doors' => 4,
            ),
            382 => 
            array (
                'id_reg' => 1883,
                'id_regmf' => 1495,
                'doors' => 5,
            ),
            383 => 
            array (
                'id_reg' => 1884,
                'id_regmf' => 1496,
                'doors' => 4,
            ),
            384 => 
            array (
                'id_reg' => 1885,
                'id_regmf' => 1497,
                'doors' => 5,
            ),
            385 => 
            array (
                'id_reg' => 1886,
                'id_regmf' => 1498,
                'doors' => 5,
            ),
            386 => 
            array (
                'id_reg' => 1887,
                'id_regmf' => 1499,
                'doors' => 5,
            ),
            387 => 
            array (
                'id_reg' => 1888,
                'id_regmf' => 1500,
                'doors' => 5,
            ),
            388 => 
            array (
                'id_reg' => 1889,
                'id_regmf' => 1501,
                'doors' => 5,
            ),
            389 => 
            array (
                'id_reg' => 1890,
                'id_regmf' => 1502,
                'doors' => 5,
            ),
            390 => 
            array (
                'id_reg' => 1891,
                'id_regmf' => 1503,
                'doors' => 5,
            ),
            391 => 
            array (
                'id_reg' => 1892,
                'id_regmf' => 1504,
                'doors' => 5,
            ),
            392 => 
            array (
                'id_reg' => 1893,
                'id_regmf' => 1505,
                'doors' => 5,
            ),
            393 => 
            array (
                'id_reg' => 1894,
                'id_regmf' => 1506,
                'doors' => 4,
            ),
            394 => 
            array (
                'id_reg' => 1895,
                'id_regmf' => 1507,
                'doors' => 3,
            ),
            395 => 
            array (
                'id_reg' => 1896,
                'id_regmf' => 1507,
                'doors' => 4,
            ),
            396 => 
            array (
                'id_reg' => 1897,
                'id_regmf' => 1507,
                'doors' => 5,
            ),
            397 => 
            array (
                'id_reg' => 1898,
                'id_regmf' => 1508,
                'doors' => 5,
            ),
            398 => 
            array (
                'id_reg' => 1899,
                'id_regmf' => 1509,
                'doors' => 4,
            ),
            399 => 
            array (
                'id_reg' => 1900,
                'id_regmf' => 1510,
                'doors' => 2,
            ),
            400 => 
            array (
                'id_reg' => 1901,
                'id_regmf' => 1511,
                'doors' => 4,
            ),
            401 => 
            array (
                'id_reg' => 1902,
                'id_regmf' => 1512,
                'doors' => 4,
            ),
            402 => 
            array (
                'id_reg' => 1903,
                'id_regmf' => 1514,
                'doors' => 5,
            ),
            403 => 
            array (
                'id_reg' => 1904,
                'id_regmf' => 1513,
                'doors' => 5,
            ),
            404 => 
            array (
                'id_reg' => 1905,
                'id_regmf' => 1516,
                'doors' => 5,
            ),
            405 => 
            array (
                'id_reg' => 1906,
                'id_regmf' => 1515,
                'doors' => 5,
            ),
            406 => 
            array (
                'id_reg' => 1907,
                'id_regmf' => 1517,
                'doors' => 4,
            ),
            407 => 
            array (
                'id_reg' => 1908,
                'id_regmf' => 1519,
                'doors' => 5,
            ),
            408 => 
            array (
                'id_reg' => 1909,
                'id_regmf' => 1518,
                'doors' => 5,
            ),
            409 => 
            array (
                'id_reg' => 1910,
                'id_regmf' => 1520,
                'doors' => 4,
            ),
            410 => 
            array (
                'id_reg' => 1911,
                'id_regmf' => 1521,
                'doors' => 3,
            ),
            411 => 
            array (
                'id_reg' => 1912,
                'id_regmf' => 1522,
                'doors' => 3,
            ),
            412 => 
            array (
                'id_reg' => 1913,
                'id_regmf' => 1523,
                'doors' => 2,
            ),
            413 => 
            array (
                'id_reg' => 1914,
                'id_regmf' => 1524,
                'doors' => 2,
            ),
            414 => 
            array (
                'id_reg' => 1915,
                'id_regmf' => 1526,
                'doors' => 5,
            ),
            415 => 
            array (
                'id_reg' => 1916,
                'id_regmf' => 1525,
                'doors' => 5,
            ),
            416 => 
            array (
                'id_reg' => 1917,
                'id_regmf' => 1528,
                'doors' => 5,
            ),
            417 => 
            array (
                'id_reg' => 1918,
                'id_regmf' => 1527,
                'doors' => 5,
            ),
            418 => 
            array (
                'id_reg' => 1919,
                'id_regmf' => 1529,
                'doors' => 2,
            ),
            419 => 
            array (
                'id_reg' => 1920,
                'id_regmf' => 1529,
                'doors' => 4,
            ),
            420 => 
            array (
                'id_reg' => 1921,
                'id_regmf' => 1530,
                'doors' => 4,
            ),
            421 => 
            array (
                'id_reg' => 1922,
                'id_regmf' => 1531,
                'doors' => 4,
            ),
            422 => 
            array (
                'id_reg' => 1923,
                'id_regmf' => 1533,
                'doors' => 4,
            ),
            423 => 
            array (
                'id_reg' => 1924,
                'id_regmf' => 1532,
                'doors' => 4,
            ),
            424 => 
            array (
                'id_reg' => 1925,
                'id_regmf' => 1534,
                'doors' => 4,
            ),
            425 => 
            array (
                'id_reg' => 1926,
                'id_regmf' => 1536,
                'doors' => 5,
            ),
            426 => 
            array (
                'id_reg' => 1927,
                'id_regmf' => 1535,
                'doors' => 5,
            ),
            427 => 
            array (
                'id_reg' => 1928,
                'id_regmf' => 1538,
                'doors' => 4,
            ),
            428 => 
            array (
                'id_reg' => 1929,
                'id_regmf' => 1539,
                'doors' => 4,
            ),
            429 => 
            array (
                'id_reg' => 1930,
                'id_regmf' => 1537,
                'doors' => 4,
            ),
            430 => 
            array (
                'id_reg' => 1931,
                'id_regmf' => 1541,
                'doors' => 2,
            ),
            431 => 
            array (
                'id_reg' => 1932,
                'id_regmf' => 1540,
                'doors' => 2,
            ),
            432 => 
            array (
                'id_reg' => 1933,
                'id_regmf' => 1543,
                'doors' => 4,
            ),
            433 => 
            array (
                'id_reg' => 1934,
                'id_regmf' => 1542,
                'doors' => 4,
            ),
            434 => 
            array (
                'id_reg' => 1935,
                'id_regmf' => 1544,
                'doors' => 4,
            ),
            435 => 
            array (
                'id_reg' => 1936,
                'id_regmf' => 1545,
                'doors' => 5,
            ),
            436 => 
            array (
                'id_reg' => 1937,
                'id_regmf' => 1546,
                'doors' => 5,
            ),
            437 => 
            array (
                'id_reg' => 1938,
                'id_regmf' => 1547,
                'doors' => 5,
            ),
            438 => 
            array (
                'id_reg' => 1939,
                'id_regmf' => 1548,
                'doors' => 5,
            ),
            439 => 
            array (
                'id_reg' => 1940,
                'id_regmf' => 1550,
                'doors' => 5,
            ),
            440 => 
            array (
                'id_reg' => 1941,
                'id_regmf' => 1549,
                'doors' => 5,
            ),
            441 => 
            array (
                'id_reg' => 1942,
                'id_regmf' => 1551,
                'doors' => 3,
            ),
            442 => 
            array (
                'id_reg' => 1943,
                'id_regmf' => 1552,
                'doors' => 3,
            ),
            443 => 
            array (
                'id_reg' => 1944,
                'id_regmf' => 1554,
                'doors' => 3,
            ),
            444 => 
            array (
                'id_reg' => 1945,
                'id_regmf' => 1553,
                'doors' => 3,
            ),
            445 => 
            array (
                'id_reg' => 1946,
                'id_regmf' => 1555,
                'doors' => 3,
            ),
            446 => 
            array (
                'id_reg' => 1947,
                'id_regmf' => 1556,
                'doors' => 3,
            ),
            447 => 
            array (
                'id_reg' => 1948,
                'id_regmf' => 1557,
                'doors' => 5,
            ),
            448 => 
            array (
                'id_reg' => 1949,
                'id_regmf' => 1558,
                'doors' => 3,
            ),
            449 => 
            array (
                'id_reg' => 1950,
                'id_regmf' => 1559,
                'doors' => 2,
            ),
            450 => 
            array (
                'id_reg' => 1951,
                'id_regmf' => 1559,
                'doors' => 3,
            ),
            451 => 
            array (
                'id_reg' => 1952,
                'id_regmf' => 1560,
                'doors' => 3,
            ),
            452 => 
            array (
                'id_reg' => 1953,
                'id_regmf' => 1561,
                'doors' => 3,
            ),
            453 => 
            array (
                'id_reg' => 1954,
                'id_regmf' => 1562,
                'doors' => 5,
            ),
            454 => 
            array (
                'id_reg' => 1955,
                'id_regmf' => 1563,
                'doors' => 4,
            ),
            455 => 
            array (
                'id_reg' => 1956,
                'id_regmf' => 1564,
                'doors' => 2,
            ),
            456 => 
            array (
                'id_reg' => 1957,
                'id_regmf' => 1566,
                'doors' => 3,
            ),
            457 => 
            array (
                'id_reg' => 1958,
                'id_regmf' => 1565,
                'doors' => 3,
            ),
            458 => 
            array (
                'id_reg' => 1959,
                'id_regmf' => 1567,
                'doors' => 2,
            ),
            459 => 
            array (
                'id_reg' => 1960,
                'id_regmf' => 1568,
                'doors' => 3,
            ),
            460 => 
            array (
                'id_reg' => 1961,
                'id_regmf' => 1569,
                'doors' => 5,
            ),
            461 => 
            array (
                'id_reg' => 1962,
                'id_regmf' => 1570,
                'doors' => 4,
            ),
            462 => 
            array (
                'id_reg' => 1963,
                'id_regmf' => 1571,
                'doors' => 6,
            ),
            463 => 
            array (
                'id_reg' => 1964,
                'id_regmf' => 1572,
                'doors' => 2,
            ),
            464 => 
            array (
                'id_reg' => 1965,
                'id_regmf' => 1572,
                'doors' => 3,
            ),
            465 => 
            array (
                'id_reg' => 1966,
                'id_regmf' => 1572,
                'doors' => 4,
            ),
            466 => 
            array (
                'id_reg' => 1967,
                'id_regmf' => 1573,
                'doors' => 4,
            ),
            467 => 
            array (
                'id_reg' => 1968,
                'id_regmf' => 1574,
                'doors' => 4,
            ),
            468 => 
            array (
                'id_reg' => 1969,
                'id_regmf' => 1575,
                'doors' => 2,
            ),
            469 => 
            array (
                'id_reg' => 1970,
                'id_regmf' => 1575,
                'doors' => 4,
            ),
            470 => 
            array (
                'id_reg' => 1971,
                'id_regmf' => 1576,
                'doors' => 3,
            ),
            471 => 
            array (
                'id_reg' => 1972,
                'id_regmf' => 1576,
                'doors' => 4,
            ),
            472 => 
            array (
                'id_reg' => 1973,
                'id_regmf' => 1577,
                'doors' => 4,
            ),
            473 => 
            array (
                'id_reg' => 1974,
                'id_regmf' => 1578,
                'doors' => 4,
            ),
            474 => 
            array (
                'id_reg' => 1975,
                'id_regmf' => 1579,
                'doors' => 4,
            ),
            475 => 
            array (
                'id_reg' => 1976,
                'id_regmf' => 1580,
                'doors' => 4,
            ),
            476 => 
            array (
                'id_reg' => 1977,
                'id_regmf' => 1581,
                'doors' => 3,
            ),
            477 => 
            array (
                'id_reg' => 1978,
                'id_regmf' => 1582,
                'doors' => 2,
            ),
            478 => 
            array (
                'id_reg' => 1979,
                'id_regmf' => 1582,
                'doors' => 4,
            ),
            479 => 
            array (
                'id_reg' => 1980,
                'id_regmf' => 1583,
                'doors' => 2,
            ),
            480 => 
            array (
                'id_reg' => 1981,
                'id_regmf' => 1584,
                'doors' => 2,
            ),
            481 => 
            array (
                'id_reg' => 1982,
                'id_regmf' => 1584,
                'doors' => 4,
            ),
            482 => 
            array (
                'id_reg' => 1983,
                'id_regmf' => 1585,
                'doors' => 5,
            ),
            483 => 
            array (
                'id_reg' => 1984,
                'id_regmf' => 1586,
                'doors' => 2,
            ),
            484 => 
            array (
                'id_reg' => 1985,
                'id_regmf' => 1587,
                'doors' => 2,
            ),
            485 => 
            array (
                'id_reg' => 1986,
                'id_regmf' => 1587,
                'doors' => 4,
            ),
            486 => 
            array (
                'id_reg' => 1987,
                'id_regmf' => 1588,
                'doors' => 2,
            ),
            487 => 
            array (
                'id_reg' => 1988,
                'id_regmf' => 1588,
                'doors' => 3,
            ),
            488 => 
            array (
                'id_reg' => 1989,
                'id_regmf' => 1588,
                'doors' => 4,
            ),
            489 => 
            array (
                'id_reg' => 1990,
                'id_regmf' => 1588,
                'doors' => 5,
            ),
            490 => 
            array (
                'id_reg' => 1991,
                'id_regmf' => 1589,
                'doors' => 3,
            ),
            491 => 
            array (
                'id_reg' => 1992,
                'id_regmf' => 1589,
                'doors' => 5,
            ),
            492 => 
            array (
                'id_reg' => 1993,
                'id_regmf' => 1589,
                'doors' => 6,
            ),
            493 => 
            array (
                'id_reg' => 1994,
                'id_regmf' => 1590,
                'doors' => 3,
            ),
            494 => 
            array (
                'id_reg' => 1995,
                'id_regmf' => 1591,
                'doors' => 0,
            ),
            495 => 
            array (
                'id_reg' => 1996,
                'id_regmf' => 1592,
                'doors' => 0,
            ),
            496 => 
            array (
                'id_reg' => 1997,
                'id_regmf' => 1593,
                'doors' => 2,
            ),
            497 => 
            array (
                'id_reg' => 1998,
                'id_regmf' => 1594,
                'doors' => 2,
            ),
            498 => 
            array (
                'id_reg' => 1999,
                'id_regmf' => 1595,
                'doors' => 2,
            ),
            499 => 
            array (
                'id_reg' => 2000,
                'id_regmf' => 1596,
                'doors' => 2,
            ),
        ));
        \DB::table('model_fuel_numdoors')->insert(array (
            0 => 
            array (
                'id_reg' => 2001,
                'id_regmf' => 1596,
                'doors' => 3,
            ),
            1 => 
            array (
                'id_reg' => 2002,
                'id_regmf' => 1596,
                'doors' => 4,
            ),
            2 => 
            array (
                'id_reg' => 2003,
                'id_regmf' => 1596,
                'doors' => 5,
            ),
            3 => 
            array (
                'id_reg' => 2004,
                'id_regmf' => 1598,
                'doors' => 2,
            ),
            4 => 
            array (
                'id_reg' => 2005,
                'id_regmf' => 1598,
                'doors' => 4,
            ),
            5 => 
            array (
                'id_reg' => 2006,
                'id_regmf' => 1598,
                'doors' => 5,
            ),
            6 => 
            array (
                'id_reg' => 2007,
                'id_regmf' => 1597,
                'doors' => 2,
            ),
            7 => 
            array (
                'id_reg' => 2008,
                'id_regmf' => 1597,
                'doors' => 4,
            ),
            8 => 
            array (
                'id_reg' => 2009,
                'id_regmf' => 1597,
                'doors' => 5,
            ),
            9 => 
            array (
                'id_reg' => 2010,
                'id_regmf' => 1600,
                'doors' => 2,
            ),
            10 => 
            array (
                'id_reg' => 2011,
                'id_regmf' => 1600,
                'doors' => 5,
            ),
            11 => 
            array (
                'id_reg' => 2012,
                'id_regmf' => 1599,
                'doors' => 2,
            ),
            12 => 
            array (
                'id_reg' => 2013,
                'id_regmf' => 1599,
                'doors' => 4,
            ),
            13 => 
            array (
                'id_reg' => 2014,
                'id_regmf' => 1599,
                'doors' => 5,
            ),
            14 => 
            array (
                'id_reg' => 2015,
                'id_regmf' => 1602,
                'doors' => 5,
            ),
            15 => 
            array (
                'id_reg' => 2016,
                'id_regmf' => 1601,
                'doors' => 2,
            ),
            16 => 
            array (
                'id_reg' => 2017,
                'id_regmf' => 1601,
                'doors' => 4,
            ),
            17 => 
            array (
                'id_reg' => 2018,
                'id_regmf' => 1601,
                'doors' => 5,
            ),
            18 => 
            array (
                'id_reg' => 2019,
                'id_regmf' => 1603,
                'doors' => 2,
            ),
            19 => 
            array (
                'id_reg' => 2020,
                'id_regmf' => 1603,
                'doors' => 4,
            ),
            20 => 
            array (
                'id_reg' => 2021,
                'id_regmf' => 1603,
                'doors' => 5,
            ),
            21 => 
            array (
                'id_reg' => 2022,
                'id_regmf' => 1604,
                'doors' => 2,
            ),
            22 => 
            array (
                'id_reg' => 2023,
                'id_regmf' => 1604,
                'doors' => 5,
            ),
            23 => 
            array (
                'id_reg' => 2024,
                'id_regmf' => 1605,
                'doors' => 2,
            ),
            24 => 
            array (
                'id_reg' => 2025,
                'id_regmf' => 1605,
                'doors' => 3,
            ),
            25 => 
            array (
                'id_reg' => 2026,
                'id_regmf' => 1605,
                'doors' => 5,
            ),
            26 => 
            array (
                'id_reg' => 2027,
                'id_regmf' => 1606,
                'doors' => 2,
            ),
            27 => 
            array (
                'id_reg' => 2028,
                'id_regmf' => 1607,
                'doors' => 2,
            ),
            28 => 
            array (
                'id_reg' => 2029,
                'id_regmf' => 1608,
                'doors' => 4,
            ),
            29 => 
            array (
                'id_reg' => 2030,
                'id_regmf' => 1609,
                'doors' => 4,
            ),
            30 => 
            array (
                'id_reg' => 2031,
                'id_regmf' => 1610,
                'doors' => 4,
            ),
            31 => 
            array (
                'id_reg' => 2032,
                'id_regmf' => 1611,
                'doors' => 2,
            ),
            32 => 
            array (
                'id_reg' => 2033,
                'id_regmf' => 1612,
                'doors' => 5,
            ),
            33 => 
            array (
                'id_reg' => 2034,
                'id_regmf' => 1613,
                'doors' => 5,
            ),
            34 => 
            array (
                'id_reg' => 2035,
                'id_regmf' => 1614,
                'doors' => 2,
            ),
            35 => 
            array (
                'id_reg' => 2036,
                'id_regmf' => 1614,
                'doors' => 3,
            ),
            36 => 
            array (
                'id_reg' => 2037,
                'id_regmf' => 1615,
                'doors' => 4,
            ),
            37 => 
            array (
                'id_reg' => 2038,
                'id_regmf' => 1616,
                'doors' => 4,
            ),
            38 => 
            array (
                'id_reg' => 2039,
                'id_regmf' => 1617,
                'doors' => 4,
            ),
            39 => 
            array (
                'id_reg' => 2040,
                'id_regmf' => 1618,
                'doors' => 4,
            ),
            40 => 
            array (
                'id_reg' => 2041,
                'id_regmf' => 1619,
                'doors' => 4,
            ),
            41 => 
            array (
                'id_reg' => 2042,
                'id_regmf' => 1620,
                'doors' => 4,
            ),
            42 => 
            array (
                'id_reg' => 2043,
                'id_regmf' => 1621,
                'doors' => 4,
            ),
            43 => 
            array (
                'id_reg' => 2044,
                'id_regmf' => 1623,
                'doors' => 4,
            ),
            44 => 
            array (
                'id_reg' => 2045,
                'id_regmf' => 1622,
                'doors' => 4,
            ),
            45 => 
            array (
                'id_reg' => 2046,
                'id_regmf' => 1624,
                'doors' => 4,
            ),
            46 => 
            array (
                'id_reg' => 2047,
                'id_regmf' => 1625,
                'doors' => 4,
            ),
            47 => 
            array (
                'id_reg' => 2048,
                'id_regmf' => 1626,
                'doors' => 4,
            ),
            48 => 
            array (
                'id_reg' => 2049,
                'id_regmf' => 1627,
                'doors' => 4,
            ),
            49 => 
            array (
                'id_reg' => 2050,
                'id_regmf' => 1629,
                'doors' => 4,
            ),
            50 => 
            array (
                'id_reg' => 2051,
                'id_regmf' => 1629,
                'doors' => 5,
            ),
            51 => 
            array (
                'id_reg' => 2052,
                'id_regmf' => 1628,
                'doors' => 4,
            ),
            52 => 
            array (
                'id_reg' => 2053,
                'id_regmf' => 1628,
                'doors' => 5,
            ),
            53 => 
            array (
                'id_reg' => 2054,
                'id_regmf' => 1630,
                'doors' => 4,
            ),
            54 => 
            array (
                'id_reg' => 2055,
                'id_regmf' => 1631,
                'doors' => 4,
            ),
            55 => 
            array (
                'id_reg' => 2056,
                'id_regmf' => 1633,
                'doors' => 4,
            ),
            56 => 
            array (
                'id_reg' => 2057,
                'id_regmf' => 1632,
                'doors' => 4,
            ),
            57 => 
            array (
                'id_reg' => 2058,
                'id_regmf' => 1632,
                'doors' => 5,
            ),
            58 => 
            array (
                'id_reg' => 2059,
                'id_regmf' => 1634,
                'doors' => 4,
            ),
            59 => 
            array (
                'id_reg' => 2060,
                'id_regmf' => 1635,
                'doors' => 2,
            ),
            60 => 
            array (
                'id_reg' => 2061,
                'id_regmf' => 1635,
                'doors' => 4,
            ),
            61 => 
            array (
                'id_reg' => 2062,
                'id_regmf' => 1636,
                'doors' => 4,
            ),
            62 => 
            array (
                'id_reg' => 2063,
                'id_regmf' => 1637,
                'doors' => 2,
            ),
            63 => 
            array (
                'id_reg' => 2064,
                'id_regmf' => 1637,
                'doors' => 4,
            ),
            64 => 
            array (
                'id_reg' => 2065,
                'id_regmf' => 1638,
                'doors' => 4,
            ),
            65 => 
            array (
                'id_reg' => 2066,
                'id_regmf' => 1639,
                'doors' => 4,
            ),
            66 => 
            array (
                'id_reg' => 2067,
                'id_regmf' => 1640,
                'doors' => 2,
            ),
            67 => 
            array (
                'id_reg' => 2068,
                'id_regmf' => 1641,
                'doors' => 2,
            ),
            68 => 
            array (
                'id_reg' => 2069,
                'id_regmf' => 1642,
                'doors' => 2,
            ),
            69 => 
            array (
                'id_reg' => 2070,
                'id_regmf' => 1643,
                'doors' => 2,
            ),
            70 => 
            array (
                'id_reg' => 2071,
                'id_regmf' => 1644,
                'doors' => 2,
            ),
            71 => 
            array (
                'id_reg' => 2072,
                'id_regmf' => 1645,
                'doors' => 2,
            ),
            72 => 
            array (
                'id_reg' => 2073,
                'id_regmf' => 1646,
                'doors' => 2,
            ),
            73 => 
            array (
                'id_reg' => 2074,
                'id_regmf' => 1647,
                'doors' => 3,
            ),
            74 => 
            array (
                'id_reg' => 2075,
                'id_regmf' => 1648,
                'doors' => 3,
            ),
            75 => 
            array (
                'id_reg' => 2076,
                'id_regmf' => 1649,
                'doors' => 0,
            ),
            76 => 
            array (
                'id_reg' => 2077,
                'id_regmf' => 1650,
                'doors' => 2,
            ),
            77 => 
            array (
                'id_reg' => 2078,
                'id_regmf' => 1651,
                'doors' => 2,
            ),
            78 => 
            array (
                'id_reg' => 2079,
                'id_regmf' => 1652,
                'doors' => 3,
            ),
            79 => 
            array (
                'id_reg' => 2080,
                'id_regmf' => 1653,
                'doors' => 2,
            ),
            80 => 
            array (
                'id_reg' => 2081,
                'id_regmf' => 1654,
                'doors' => 2,
            ),
            81 => 
            array (
                'id_reg' => 2082,
                'id_regmf' => 1654,
                'doors' => 3,
            ),
            82 => 
            array (
                'id_reg' => 2083,
                'id_regmf' => 1655,
                'doors' => 3,
            ),
            83 => 
            array (
                'id_reg' => 2084,
                'id_regmf' => 1656,
                'doors' => 3,
            ),
            84 => 
            array (
                'id_reg' => 2085,
                'id_regmf' => 1656,
                'doors' => 5,
            ),
            85 => 
            array (
                'id_reg' => 2086,
                'id_regmf' => 1657,
                'doors' => 3,
            ),
            86 => 
            array (
                'id_reg' => 2087,
                'id_regmf' => 1657,
                'doors' => 5,
            ),
            87 => 
            array (
                'id_reg' => 2088,
                'id_regmf' => 1658,
                'doors' => 3,
            ),
            88 => 
            array (
                'id_reg' => 2089,
                'id_regmf' => 1659,
                'doors' => 0,
            ),
            89 => 
            array (
                'id_reg' => 2090,
                'id_regmf' => 1660,
                'doors' => 2,
            ),
            90 => 
            array (
                'id_reg' => 2091,
                'id_regmf' => 1660,
                'doors' => 3,
            ),
            91 => 
            array (
                'id_reg' => 2092,
                'id_regmf' => 1661,
                'doors' => 3,
            ),
            92 => 
            array (
                'id_reg' => 2093,
                'id_regmf' => 1662,
                'doors' => 2,
            ),
            93 => 
            array (
                'id_reg' => 2094,
                'id_regmf' => 1664,
                'doors' => 5,
            ),
            94 => 
            array (
                'id_reg' => 2095,
                'id_regmf' => 1663,
                'doors' => 5,
            ),
            95 => 
            array (
                'id_reg' => 2096,
                'id_regmf' => 1666,
                'doors' => 5,
            ),
            96 => 
            array (
                'id_reg' => 2097,
                'id_regmf' => 1665,
                'doors' => 5,
            ),
            97 => 
            array (
                'id_reg' => 2098,
                'id_regmf' => 1668,
                'doors' => 5,
            ),
            98 => 
            array (
                'id_reg' => 2099,
                'id_regmf' => 1667,
                'doors' => 5,
            ),
            99 => 
            array (
                'id_reg' => 2100,
                'id_regmf' => 1669,
                'doors' => 5,
            ),
            100 => 
            array (
                'id_reg' => 2101,
                'id_regmf' => 1670,
                'doors' => 5,
            ),
            101 => 
            array (
                'id_reg' => 2102,
                'id_regmf' => 1672,
                'doors' => 5,
            ),
            102 => 
            array (
                'id_reg' => 2103,
                'id_regmf' => 1671,
                'doors' => 5,
            ),
            103 => 
            array (
                'id_reg' => 2104,
                'id_regmf' => 1674,
                'doors' => 5,
            ),
            104 => 
            array (
                'id_reg' => 2105,
                'id_regmf' => 1673,
                'doors' => 5,
            ),
            105 => 
            array (
                'id_reg' => 2106,
                'id_regmf' => 1676,
                'doors' => 5,
            ),
            106 => 
            array (
                'id_reg' => 2107,
                'id_regmf' => 1675,
                'doors' => 5,
            ),
            107 => 
            array (
                'id_reg' => 2108,
                'id_regmf' => 1677,
                'doors' => 5,
            ),
            108 => 
            array (
                'id_reg' => 2109,
                'id_regmf' => 1678,
                'doors' => 2,
            ),
            109 => 
            array (
                'id_reg' => 2110,
                'id_regmf' => 1679,
                'doors' => 2,
            ),
            110 => 
            array (
                'id_reg' => 2111,
                'id_regmf' => 1679,
                'doors' => 3,
            ),
            111 => 
            array (
                'id_reg' => 2112,
                'id_regmf' => 1680,
                'doors' => 3,
            ),
            112 => 
            array (
                'id_reg' => 2113,
                'id_regmf' => 1680,
                'doors' => 5,
            ),
            113 => 
            array (
                'id_reg' => 2114,
                'id_regmf' => 1681,
                'doors' => 2,
            ),
            114 => 
            array (
                'id_reg' => 2115,
                'id_regmf' => 1681,
                'doors' => 3,
            ),
            115 => 
            array (
                'id_reg' => 2116,
                'id_regmf' => 1681,
                'doors' => 5,
            ),
            116 => 
            array (
                'id_reg' => 2117,
                'id_regmf' => 1682,
                'doors' => 2,
            ),
            117 => 
            array (
                'id_reg' => 2118,
                'id_regmf' => 1682,
                'doors' => 6,
            ),
            118 => 
            array (
                'id_reg' => 2119,
                'id_regmf' => 1683,
                'doors' => 0,
            ),
            119 => 
            array (
                'id_reg' => 2120,
                'id_regmf' => 1683,
                'doors' => 2,
            ),
            120 => 
            array (
                'id_reg' => 2121,
                'id_regmf' => 1684,
                'doors' => 2,
            ),
            121 => 
            array (
                'id_reg' => 2122,
                'id_regmf' => 1685,
                'doors' => 0,
            ),
            122 => 
            array (
                'id_reg' => 2123,
                'id_regmf' => 1685,
                'doors' => 2,
            ),
            123 => 
            array (
                'id_reg' => 2124,
                'id_regmf' => 1686,
                'doors' => 0,
            ),
            124 => 
            array (
                'id_reg' => 2125,
                'id_regmf' => 1686,
                'doors' => 2,
            ),
            125 => 
            array (
                'id_reg' => 2126,
                'id_regmf' => 1687,
                'doors' => 2,
            ),
            126 => 
            array (
                'id_reg' => 2127,
                'id_regmf' => 1688,
                'doors' => 3,
            ),
            127 => 
            array (
                'id_reg' => 2128,
                'id_regmf' => 1689,
                'doors' => 2,
            ),
            128 => 
            array (
                'id_reg' => 2129,
                'id_regmf' => 1690,
                'doors' => 0,
            ),
            129 => 
            array (
                'id_reg' => 2130,
                'id_regmf' => 1691,
                'doors' => 0,
            ),
            130 => 
            array (
                'id_reg' => 2131,
                'id_regmf' => 1692,
                'doors' => 0,
            ),
            131 => 
            array (
                'id_reg' => 2132,
                'id_regmf' => 1695,
                'doors' => 0,
            ),
            132 => 
            array (
                'id_reg' => 2133,
                'id_regmf' => 1694,
                'doors' => 0,
            ),
            133 => 
            array (
                'id_reg' => 2134,
                'id_regmf' => 1693,
                'doors' => 0,
            ),
            134 => 
            array (
                'id_reg' => 2135,
                'id_regmf' => 1696,
                'doors' => 2,
            ),
            135 => 
            array (
                'id_reg' => 2136,
                'id_regmf' => 1697,
                'doors' => 0,
            ),
            136 => 
            array (
                'id_reg' => 2137,
                'id_regmf' => 1698,
                'doors' => 0,
            ),
            137 => 
            array (
                'id_reg' => 2138,
                'id_regmf' => 1699,
                'doors' => 2,
            ),
            138 => 
            array (
                'id_reg' => 2139,
                'id_regmf' => 1700,
                'doors' => 4,
            ),
            139 => 
            array (
                'id_reg' => 2140,
                'id_regmf' => 1701,
                'doors' => 0,
            ),
            140 => 
            array (
                'id_reg' => 2141,
                'id_regmf' => 1702,
                'doors' => 0,
            ),
            141 => 
            array (
                'id_reg' => 2142,
                'id_regmf' => 1703,
                'doors' => 0,
            ),
            142 => 
            array (
                'id_reg' => 2143,
                'id_regmf' => 1704,
                'doors' => 0,
            ),
            143 => 
            array (
                'id_reg' => 2144,
                'id_regmf' => 1705,
                'doors' => 0,
            ),
            144 => 
            array (
                'id_reg' => 2145,
                'id_regmf' => 1706,
                'doors' => 0,
            ),
            145 => 
            array (
                'id_reg' => 2146,
                'id_regmf' => 1707,
                'doors' => 0,
            ),
            146 => 
            array (
                'id_reg' => 2147,
                'id_regmf' => 1708,
                'doors' => 0,
            ),
            147 => 
            array (
                'id_reg' => 2148,
                'id_regmf' => 1709,
                'doors' => 0,
            ),
            148 => 
            array (
                'id_reg' => 2149,
                'id_regmf' => 1710,
                'doors' => 0,
            ),
            149 => 
            array (
                'id_reg' => 2150,
                'id_regmf' => 1711,
                'doors' => 3,
            ),
            150 => 
            array (
                'id_reg' => 2151,
                'id_regmf' => 1712,
                'doors' => 4,
            ),
            151 => 
            array (
                'id_reg' => 2152,
                'id_regmf' => 1713,
                'doors' => 2,
            ),
            152 => 
            array (
                'id_reg' => 2153,
                'id_regmf' => 1714,
                'doors' => 4,
            ),
            153 => 
            array (
                'id_reg' => 2154,
                'id_regmf' => 1714,
                'doors' => 5,
            ),
            154 => 
            array (
                'id_reg' => 2155,
                'id_regmf' => 1715,
                'doors' => 2,
            ),
            155 => 
            array (
                'id_reg' => 2156,
                'id_regmf' => 1716,
                'doors' => 4,
            ),
            156 => 
            array (
                'id_reg' => 2157,
                'id_regmf' => 1717,
                'doors' => 2,
            ),
            157 => 
            array (
                'id_reg' => 2158,
                'id_regmf' => 1717,
                'doors' => 4,
            ),
            158 => 
            array (
                'id_reg' => 2159,
                'id_regmf' => 1718,
                'doors' => 5,
            ),
            159 => 
            array (
                'id_reg' => 2160,
                'id_regmf' => 1719,
                'doors' => 2,
            ),
            160 => 
            array (
                'id_reg' => 2161,
                'id_regmf' => 1720,
                'doors' => 2,
            ),
            161 => 
            array (
                'id_reg' => 2162,
                'id_regmf' => 1720,
                'doors' => 4,
            ),
            162 => 
            array (
                'id_reg' => 2163,
                'id_regmf' => 1721,
                'doors' => 2,
            ),
            163 => 
            array (
                'id_reg' => 2164,
                'id_regmf' => 1721,
                'doors' => 4,
            ),
            164 => 
            array (
                'id_reg' => 2165,
                'id_regmf' => 1722,
                'doors' => 5,
            ),
            165 => 
            array (
                'id_reg' => 2166,
                'id_regmf' => 1723,
                'doors' => 4,
            ),
            166 => 
            array (
                'id_reg' => 2167,
                'id_regmf' => 1724,
                'doors' => 4,
            ),
            167 => 
            array (
                'id_reg' => 2168,
                'id_regmf' => 1725,
                'doors' => 4,
            ),
            168 => 
            array (
                'id_reg' => 2169,
                'id_regmf' => 1726,
                'doors' => 5,
            ),
            169 => 
            array (
                'id_reg' => 2170,
                'id_regmf' => 1727,
                'doors' => 4,
            ),
            170 => 
            array (
                'id_reg' => 2171,
                'id_regmf' => 1729,
                'doors' => 4,
            ),
            171 => 
            array (
                'id_reg' => 2172,
                'id_regmf' => 1729,
                'doors' => 5,
            ),
            172 => 
            array (
                'id_reg' => 2173,
                'id_regmf' => 1728,
                'doors' => 4,
            ),
            173 => 
            array (
                'id_reg' => 2174,
                'id_regmf' => 1728,
                'doors' => 5,
            ),
            174 => 
            array (
                'id_reg' => 2175,
                'id_regmf' => 1730,
                'doors' => 4,
            ),
            175 => 
            array (
                'id_reg' => 2176,
                'id_regmf' => 1731,
                'doors' => 5,
            ),
            176 => 
            array (
                'id_reg' => 2177,
                'id_regmf' => 1732,
                'doors' => 3,
            ),
            177 => 
            array (
                'id_reg' => 2178,
                'id_regmf' => 1732,
                'doors' => 5,
            ),
            178 => 
            array (
                'id_reg' => 2179,
                'id_regmf' => 1734,
                'doors' => 3,
            ),
            179 => 
            array (
                'id_reg' => 2180,
                'id_regmf' => 1733,
                'doors' => 3,
            ),
            180 => 
            array (
                'id_reg' => 2181,
                'id_regmf' => 1736,
                'doors' => 5,
            ),
            181 => 
            array (
                'id_reg' => 2182,
                'id_regmf' => 1735,
                'doors' => 5,
            ),
            182 => 
            array (
                'id_reg' => 2183,
                'id_regmf' => 1737,
                'doors' => 4,
            ),
            183 => 
            array (
                'id_reg' => 2184,
                'id_regmf' => 1737,
                'doors' => 5,
            ),
            184 => 
            array (
                'id_reg' => 2185,
                'id_regmf' => 1738,
                'doors' => 4,
            ),
            185 => 
            array (
                'id_reg' => 2186,
                'id_regmf' => 1739,
                'doors' => 4,
            ),
            186 => 
            array (
                'id_reg' => 2187,
                'id_regmf' => 1739,
                'doors' => 5,
            ),
            187 => 
            array (
                'id_reg' => 2188,
                'id_regmf' => 1741,
                'doors' => 5,
            ),
            188 => 
            array (
                'id_reg' => 2189,
                'id_regmf' => 1740,
                'doors' => 5,
            ),
            189 => 
            array (
                'id_reg' => 2190,
                'id_regmf' => 1742,
                'doors' => 0,
            ),
            190 => 
            array (
                'id_reg' => 2191,
                'id_regmf' => 1743,
                'doors' => 0,
            ),
            191 => 
            array (
                'id_reg' => 2192,
                'id_regmf' => 1744,
                'doors' => 0,
            ),
            192 => 
            array (
                'id_reg' => 2193,
                'id_regmf' => 1745,
                'doors' => 0,
            ),
            193 => 
            array (
                'id_reg' => 2194,
                'id_regmf' => 1746,
                'doors' => 0,
            ),
            194 => 
            array (
                'id_reg' => 2195,
                'id_regmf' => 1747,
                'doors' => 0,
            ),
            195 => 
            array (
                'id_reg' => 2196,
                'id_regmf' => 1748,
                'doors' => 4,
            ),
            196 => 
            array (
                'id_reg' => 2197,
                'id_regmf' => 1749,
                'doors' => 0,
            ),
            197 => 
            array (
                'id_reg' => 2198,
                'id_regmf' => 1750,
                'doors' => 2,
            ),
            198 => 
            array (
                'id_reg' => 2199,
                'id_regmf' => 1750,
                'doors' => 5,
            ),
            199 => 
            array (
                'id_reg' => 2200,
                'id_regmf' => 1751,
                'doors' => 2,
            ),
            200 => 
            array (
                'id_reg' => 2201,
                'id_regmf' => 1751,
                'doors' => 5,
            ),
            201 => 
            array (
                'id_reg' => 2202,
                'id_regmf' => 1752,
                'doors' => 0,
            ),
            202 => 
            array (
                'id_reg' => 2203,
                'id_regmf' => 1752,
                'doors' => 2,
            ),
            203 => 
            array (
                'id_reg' => 2204,
                'id_regmf' => 1753,
                'doors' => 4,
            ),
            204 => 
            array (
                'id_reg' => 2205,
                'id_regmf' => 1754,
                'doors' => 5,
            ),
            205 => 
            array (
                'id_reg' => 2206,
                'id_regmf' => 1755,
                'doors' => 5,
            ),
            206 => 
            array (
                'id_reg' => 2207,
                'id_regmf' => 1756,
                'doors' => 5,
            ),
            207 => 
            array (
                'id_reg' => 2208,
                'id_regmf' => 1758,
                'doors' => 4,
            ),
            208 => 
            array (
                'id_reg' => 2209,
                'id_regmf' => 1759,
                'doors' => 5,
            ),
            209 => 
            array (
                'id_reg' => 2210,
                'id_regmf' => 1760,
                'doors' => 4,
            ),
            210 => 
            array (
                'id_reg' => 2211,
                'id_regmf' => 1761,
                'doors' => 4,
            ),
            211 => 
            array (
                'id_reg' => 2212,
                'id_regmf' => 1762,
                'doors' => 4,
            ),
            212 => 
            array (
                'id_reg' => 2213,
                'id_regmf' => 1763,
                'doors' => 5,
            ),
            213 => 
            array (
                'id_reg' => 2214,
                'id_regmf' => 1764,
                'doors' => 5,
            ),
            214 => 
            array (
                'id_reg' => 2215,
                'id_regmf' => 1765,
                'doors' => 5,
            ),
            215 => 
            array (
                'id_reg' => 2216,
                'id_regmf' => 1766,
                'doors' => 4,
            ),
            216 => 
            array (
                'id_reg' => 2217,
                'id_regmf' => 1767,
                'doors' => 4,
            ),
            217 => 
            array (
                'id_reg' => 2218,
                'id_regmf' => 1757,
                'doors' => 3,
            ),
            218 => 
            array (
                'id_reg' => 2219,
                'id_regmf' => 1768,
                'doors' => 4,
            ),
            219 => 
            array (
                'id_reg' => 2220,
                'id_regmf' => 1768,
                'doors' => 5,
            ),
            220 => 
            array (
                'id_reg' => 2221,
                'id_regmf' => 1770,
                'doors' => 2,
            ),
            221 => 
            array (
                'id_reg' => 2222,
                'id_regmf' => 1770,
                'doors' => 3,
            ),
            222 => 
            array (
                'id_reg' => 2223,
                'id_regmf' => 1769,
                'doors' => 3,
            ),
            223 => 
            array (
                'id_reg' => 2224,
                'id_regmf' => 1771,
                'doors' => 4,
            ),
            224 => 
            array (
                'id_reg' => 2225,
                'id_regmf' => 1771,
                'doors' => 5,
            ),
            225 => 
            array (
                'id_reg' => 2226,
                'id_regmf' => 1772,
                'doors' => 4,
            ),
            226 => 
            array (
                'id_reg' => 2227,
                'id_regmf' => 1773,
                'doors' => 4,
            ),
            227 => 
            array (
                'id_reg' => 2228,
                'id_regmf' => 1774,
                'doors' => 2,
            ),
            228 => 
            array (
                'id_reg' => 2229,
                'id_regmf' => 1774,
                'doors' => 3,
            ),
            229 => 
            array (
                'id_reg' => 2230,
                'id_regmf' => 1774,
                'doors' => 5,
            ),
            230 => 
            array (
                'id_reg' => 2231,
                'id_regmf' => 1775,
                'doors' => 4,
            ),
            231 => 
            array (
                'id_reg' => 2232,
                'id_regmf' => 1776,
                'doors' => 5,
            ),
            232 => 
            array (
                'id_reg' => 2233,
                'id_regmf' => 1777,
                'doors' => 2,
            ),
            233 => 
            array (
                'id_reg' => 2234,
                'id_regmf' => 1778,
                'doors' => 0,
            ),
            234 => 
            array (
                'id_reg' => 2235,
                'id_regmf' => 1779,
                'doors' => 2,
            ),
            235 => 
            array (
                'id_reg' => 2236,
                'id_regmf' => 1780,
                'doors' => 2,
            ),
            236 => 
            array (
                'id_reg' => 2237,
                'id_regmf' => 1781,
                'doors' => 2,
            ),
            237 => 
            array (
                'id_reg' => 2238,
                'id_regmf' => 1782,
                'doors' => 2,
            ),
            238 => 
            array (
                'id_reg' => 2239,
                'id_regmf' => 1783,
                'doors' => 3,
            ),
            239 => 
            array (
                'id_reg' => 2240,
                'id_regmf' => 1784,
                'doors' => 2,
            ),
            240 => 
            array (
                'id_reg' => 2241,
                'id_regmf' => 1785,
                'doors' => 0,
            ),
            241 => 
            array (
                'id_reg' => 2242,
                'id_regmf' => 1786,
                'doors' => 2,
            ),
            242 => 
            array (
                'id_reg' => 2243,
                'id_regmf' => 1787,
                'doors' => 2,
            ),
            243 => 
            array (
                'id_reg' => 2244,
                'id_regmf' => 1788,
                'doors' => 4,
            ),
            244 => 
            array (
                'id_reg' => 2245,
                'id_regmf' => 1789,
                'doors' => 2,
            ),
            245 => 
            array (
                'id_reg' => 2246,
                'id_regmf' => 1790,
                'doors' => 2,
            ),
            246 => 
            array (
                'id_reg' => 2247,
                'id_regmf' => 1791,
                'doors' => 2,
            ),
            247 => 
            array (
                'id_reg' => 2248,
                'id_regmf' => 1792,
                'doors' => 2,
            ),
            248 => 
            array (
                'id_reg' => 2249,
                'id_regmf' => 1793,
                'doors' => 2,
            ),
            249 => 
            array (
                'id_reg' => 2250,
                'id_regmf' => 1794,
                'doors' => 2,
            ),
            250 => 
            array (
                'id_reg' => 2251,
                'id_regmf' => 1795,
                'doors' => 3,
            ),
            251 => 
            array (
                'id_reg' => 2252,
                'id_regmf' => 1796,
                'doors' => 4,
            ),
            252 => 
            array (
                'id_reg' => 2253,
                'id_regmf' => 1797,
                'doors' => 2,
            ),
            253 => 
            array (
                'id_reg' => 2254,
                'id_regmf' => 1799,
                'doors' => 4,
            ),
            254 => 
            array (
                'id_reg' => 2255,
                'id_regmf' => 1799,
                'doors' => 5,
            ),
            255 => 
            array (
                'id_reg' => 2256,
                'id_regmf' => 1798,
                'doors' => 4,
            ),
            256 => 
            array (
                'id_reg' => 2257,
                'id_regmf' => 1798,
                'doors' => 5,
            ),
            257 => 
            array (
                'id_reg' => 2258,
                'id_regmf' => 1800,
                'doors' => 3,
            ),
            258 => 
            array (
                'id_reg' => 2259,
                'id_regmf' => 1800,
                'doors' => 5,
            ),
            259 => 
            array (
                'id_reg' => 2260,
                'id_regmf' => 1802,
                'doors' => 3,
            ),
            260 => 
            array (
                'id_reg' => 2261,
                'id_regmf' => 1802,
                'doors' => 5,
            ),
            261 => 
            array (
                'id_reg' => 2262,
                'id_regmf' => 1801,
                'doors' => 5,
            ),
            262 => 
            array (
                'id_reg' => 2263,
                'id_regmf' => 1803,
                'doors' => 2,
            ),
            263 => 
            array (
                'id_reg' => 2264,
                'id_regmf' => 1804,
                'doors' => 2,
            ),
            264 => 
            array (
                'id_reg' => 2265,
                'id_regmf' => 1805,
                'doors' => 2,
            ),
            265 => 
            array (
                'id_reg' => 2266,
                'id_regmf' => 1806,
                'doors' => 2,
            ),
            266 => 
            array (
                'id_reg' => 2267,
                'id_regmf' => 1807,
                'doors' => 2,
            ),
            267 => 
            array (
                'id_reg' => 2268,
                'id_regmf' => 1807,
                'doors' => 5,
            ),
            268 => 
            array (
                'id_reg' => 2269,
                'id_regmf' => 1808,
                'doors' => 3,
            ),
            269 => 
            array (
                'id_reg' => 2270,
                'id_regmf' => 1810,
                'doors' => 2,
            ),
            270 => 
            array (
                'id_reg' => 2271,
                'id_regmf' => 1810,
                'doors' => 4,
            ),
            271 => 
            array (
                'id_reg' => 2272,
                'id_regmf' => 1810,
                'doors' => 5,
            ),
            272 => 
            array (
                'id_reg' => 2273,
                'id_regmf' => 1809,
                'doors' => 4,
            ),
            273 => 
            array (
                'id_reg' => 2274,
                'id_regmf' => 1809,
                'doors' => 5,
            ),
            274 => 
            array (
                'id_reg' => 2275,
                'id_regmf' => 1812,
                'doors' => 4,
            ),
            275 => 
            array (
                'id_reg' => 2276,
                'id_regmf' => 1812,
                'doors' => 5,
            ),
            276 => 
            array (
                'id_reg' => 2277,
                'id_regmf' => 1811,
                'doors' => 4,
            ),
            277 => 
            array (
                'id_reg' => 2278,
                'id_regmf' => 1811,
                'doors' => 5,
            ),
            278 => 
            array (
                'id_reg' => 2279,
                'id_regmf' => 1813,
                'doors' => 2,
            ),
            279 => 
            array (
                'id_reg' => 2280,
                'id_regmf' => 1814,
                'doors' => 5,
            ),
            280 => 
            array (
                'id_reg' => 2281,
                'id_regmf' => 1815,
                'doors' => 5,
            ),
            281 => 
            array (
                'id_reg' => 2282,
                'id_regmf' => 1817,
                'doors' => 5,
            ),
            282 => 
            array (
                'id_reg' => 2283,
                'id_regmf' => 1816,
                'doors' => 5,
            ),
            283 => 
            array (
                'id_reg' => 2284,
                'id_regmf' => 1818,
                'doors' => 4,
            ),
            284 => 
            array (
                'id_reg' => 2285,
                'id_regmf' => 1819,
                'doors' => 4,
            ),
            285 => 
            array (
                'id_reg' => 2286,
                'id_regmf' => 1820,
                'doors' => 2,
            ),
            286 => 
            array (
                'id_reg' => 2287,
                'id_regmf' => 1822,
                'doors' => 4,
            ),
            287 => 
            array (
                'id_reg' => 2288,
                'id_regmf' => 1822,
                'doors' => 5,
            ),
            288 => 
            array (
                'id_reg' => 2289,
                'id_regmf' => 1821,
                'doors' => 4,
            ),
            289 => 
            array (
                'id_reg' => 2290,
                'id_regmf' => 1821,
                'doors' => 5,
            ),
            290 => 
            array (
                'id_reg' => 2291,
                'id_regmf' => 1824,
                'doors' => 4,
            ),
            291 => 
            array (
                'id_reg' => 2292,
                'id_regmf' => 1823,
                'doors' => 4,
            ),
            292 => 
            array (
                'id_reg' => 2293,
                'id_regmf' => 1825,
                'doors' => 4,
            ),
            293 => 
            array (
                'id_reg' => 2294,
                'id_regmf' => 1826,
                'doors' => 5,
            ),
            294 => 
            array (
                'id_reg' => 2295,
                'id_regmf' => 1827,
                'doors' => 3,
            ),
            295 => 
            array (
                'id_reg' => 2296,
                'id_regmf' => 1828,
                'doors' => 3,
            ),
            296 => 
            array (
                'id_reg' => 2297,
                'id_regmf' => 1829,
                'doors' => 3,
            ),
            297 => 
            array (
                'id_reg' => 2298,
                'id_regmf' => 1829,
                'doors' => 5,
            ),
            298 => 
            array (
                'id_reg' => 2299,
                'id_regmf' => 1830,
                'doors' => 5,
            ),
            299 => 
            array (
                'id_reg' => 2300,
                'id_regmf' => 1831,
                'doors' => 3,
            ),
            300 => 
            array (
                'id_reg' => 2301,
                'id_regmf' => 1831,
                'doors' => 5,
            ),
            301 => 
            array (
                'id_reg' => 2302,
                'id_regmf' => 1833,
                'doors' => 5,
            ),
            302 => 
            array (
                'id_reg' => 2303,
                'id_regmf' => 1832,
                'doors' => 5,
            ),
            303 => 
            array (
                'id_reg' => 2304,
                'id_regmf' => 1835,
                'doors' => 2,
            ),
            304 => 
            array (
                'id_reg' => 2305,
                'id_regmf' => 1837,
                'doors' => 3,
            ),
            305 => 
            array (
                'id_reg' => 2306,
                'id_regmf' => 1837,
                'doors' => 5,
            ),
            306 => 
            array (
                'id_reg' => 2307,
                'id_regmf' => 1836,
                'doors' => 2,
            ),
            307 => 
            array (
                'id_reg' => 2308,
                'id_regmf' => 1836,
                'doors' => 3,
            ),
            308 => 
            array (
                'id_reg' => 2309,
                'id_regmf' => 1838,
                'doors' => 4,
            ),
            309 => 
            array (
                'id_reg' => 2310,
                'id_regmf' => 1834,
                'doors' => 3,
            ),
            310 => 
            array (
                'id_reg' => 2311,
                'id_regmf' => 1834,
                'doors' => 5,
            ),
            311 => 
            array (
                'id_reg' => 2312,
                'id_regmf' => 1840,
                'doors' => 3,
            ),
            312 => 
            array (
                'id_reg' => 2313,
                'id_regmf' => 1839,
                'doors' => 2,
            ),
            313 => 
            array (
                'id_reg' => 2314,
                'id_regmf' => 1839,
                'doors' => 3,
            ),
            314 => 
            array (
                'id_reg' => 2315,
                'id_regmf' => 1839,
                'doors' => 4,
            ),
            315 => 
            array (
                'id_reg' => 2316,
                'id_regmf' => 1839,
                'doors' => 5,
            ),
            316 => 
            array (
                'id_reg' => 2317,
                'id_regmf' => 1841,
                'doors' => 3,
            ),
            317 => 
            array (
                'id_reg' => 2318,
                'id_regmf' => 1841,
                'doors' => 5,
            ),
            318 => 
            array (
                'id_reg' => 2319,
                'id_regmf' => 1842,
                'doors' => 5,
            ),
            319 => 
            array (
                'id_reg' => 2320,
                'id_regmf' => 1843,
                'doors' => 5,
            ),
            320 => 
            array (
                'id_reg' => 2321,
                'id_regmf' => 1844,
                'doors' => 5,
            ),
            321 => 
            array (
                'id_reg' => 2322,
                'id_regmf' => 1845,
                'doors' => 3,
            ),
            322 => 
            array (
                'id_reg' => 2323,
                'id_regmf' => 1845,
                'doors' => 5,
            ),
            323 => 
            array (
                'id_reg' => 2324,
                'id_regmf' => 1846,
                'doors' => 3,
            ),
            324 => 
            array (
                'id_reg' => 2325,
                'id_regmf' => 1846,
                'doors' => 5,
            ),
            325 => 
            array (
                'id_reg' => 2326,
                'id_regmf' => 1847,
                'doors' => 2,
            ),
            326 => 
            array (
                'id_reg' => 2327,
                'id_regmf' => 1847,
                'doors' => 3,
            ),
            327 => 
            array (
                'id_reg' => 2328,
                'id_regmf' => 1847,
                'doors' => 5,
            ),
            328 => 
            array (
                'id_reg' => 2329,
                'id_regmf' => 1848,
                'doors' => 2,
            ),
            329 => 
            array (
                'id_reg' => 2330,
                'id_regmf' => 1848,
                'doors' => 3,
            ),
            330 => 
            array (
                'id_reg' => 2331,
                'id_regmf' => 1848,
                'doors' => 4,
            ),
            331 => 
            array (
                'id_reg' => 2332,
                'id_regmf' => 1848,
                'doors' => 5,
            ),
            332 => 
            array (
                'id_reg' => 2333,
                'id_regmf' => 1849,
                'doors' => 2,
            ),
            333 => 
            array (
                'id_reg' => 2334,
                'id_regmf' => 1849,
                'doors' => 3,
            ),
            334 => 
            array (
                'id_reg' => 2335,
                'id_regmf' => 1849,
                'doors' => 5,
            ),
            335 => 
            array (
                'id_reg' => 2336,
                'id_regmf' => 1850,
                'doors' => 2,
            ),
            336 => 
            array (
                'id_reg' => 2337,
                'id_regmf' => 1850,
                'doors' => 3,
            ),
            337 => 
            array (
                'id_reg' => 2338,
                'id_regmf' => 1850,
                'doors' => 5,
            ),
            338 => 
            array (
                'id_reg' => 2339,
                'id_regmf' => 1851,
                'doors' => 5,
            ),
            339 => 
            array (
                'id_reg' => 2340,
                'id_regmf' => 1852,
                'doors' => 5,
            ),
            340 => 
            array (
                'id_reg' => 2341,
                'id_regmf' => 1853,
                'doors' => 3,
            ),
            341 => 
            array (
                'id_reg' => 2342,
                'id_regmf' => 1853,
                'doors' => 5,
            ),
            342 => 
            array (
                'id_reg' => 2343,
                'id_regmf' => 1854,
                'doors' => 0,
            ),
            343 => 
            array (
                'id_reg' => 2344,
                'id_regmf' => 1855,
                'doors' => 0,
            ),
            344 => 
            array (
                'id_reg' => 2345,
                'id_regmf' => 1856,
                'doors' => 0,
            ),
            345 => 
            array (
                'id_reg' => 2346,
                'id_regmf' => 1857,
                'doors' => 0,
            ),
            346 => 
            array (
                'id_reg' => 2347,
                'id_regmf' => 1858,
                'doors' => 2,
            ),
            347 => 
            array (
                'id_reg' => 2348,
                'id_regmf' => 1859,
                'doors' => 2,
            ),
            348 => 
            array (
                'id_reg' => 2349,
                'id_regmf' => 1860,
                'doors' => 2,
            ),
            349 => 
            array (
                'id_reg' => 2350,
                'id_regmf' => 1861,
                'doors' => 5,
            ),
            350 => 
            array (
                'id_reg' => 2351,
                'id_regmf' => 1862,
                'doors' => 4,
            ),
            351 => 
            array (
                'id_reg' => 2352,
                'id_regmf' => 1863,
                'doors' => 4,
            ),
            352 => 
            array (
                'id_reg' => 2353,
                'id_regmf' => 1864,
                'doors' => 4,
            ),
            353 => 
            array (
                'id_reg' => 2354,
                'id_regmf' => 1865,
                'doors' => 4,
            ),
            354 => 
            array (
                'id_reg' => 2355,
                'id_regmf' => 1866,
                'doors' => 4,
            ),
            355 => 
            array (
                'id_reg' => 2356,
                'id_regmf' => 1867,
                'doors' => 4,
            ),
            356 => 
            array (
                'id_reg' => 2357,
                'id_regmf' => 1868,
                'doors' => 4,
            ),
            357 => 
            array (
                'id_reg' => 2358,
                'id_regmf' => 1869,
                'doors' => 4,
            ),
            358 => 
            array (
                'id_reg' => 2359,
                'id_regmf' => 1870,
                'doors' => 5,
            ),
            359 => 
            array (
                'id_reg' => 2360,
                'id_regmf' => 1871,
                'doors' => 4,
            ),
            360 => 
            array (
                'id_reg' => 2361,
                'id_regmf' => 1872,
                'doors' => 4,
            ),
            361 => 
            array (
                'id_reg' => 2362,
                'id_regmf' => 1873,
                'doors' => 4,
            ),
            362 => 
            array (
                'id_reg' => 2363,
                'id_regmf' => 1874,
                'doors' => 4,
            ),
            363 => 
            array (
                'id_reg' => 2364,
                'id_regmf' => 1875,
                'doors' => 4,
            ),
            364 => 
            array (
                'id_reg' => 2365,
                'id_regmf' => 1876,
                'doors' => 5,
            ),
            365 => 
            array (
                'id_reg' => 2366,
                'id_regmf' => 1877,
                'doors' => 5,
            ),
            366 => 
            array (
                'id_reg' => 2367,
                'id_regmf' => 1878,
                'doors' => 4,
            ),
            367 => 
            array (
                'id_reg' => 2368,
                'id_regmf' => 1878,
                'doors' => 5,
            ),
            368 => 
            array (
                'id_reg' => 2369,
                'id_regmf' => 1879,
                'doors' => 4,
            ),
            369 => 
            array (
                'id_reg' => 2370,
                'id_regmf' => 1880,
                'doors' => 4,
            ),
            370 => 
            array (
                'id_reg' => 2371,
                'id_regmf' => 1881,
                'doors' => 2,
            ),
            371 => 
            array (
                'id_reg' => 2372,
                'id_regmf' => 1881,
                'doors' => 4,
            ),
            372 => 
            array (
                'id_reg' => 2373,
                'id_regmf' => 1882,
                'doors' => 4,
            ),
            373 => 
            array (
                'id_reg' => 2374,
                'id_regmf' => 1882,
                'doors' => 5,
            ),
            374 => 
            array (
                'id_reg' => 2375,
                'id_regmf' => 1883,
                'doors' => 4,
            ),
            375 => 
            array (
                'id_reg' => 2376,
                'id_regmf' => 1884,
                'doors' => 2,
            ),
            376 => 
            array (
                'id_reg' => 2377,
                'id_regmf' => 1885,
                'doors' => 4,
            ),
            377 => 
            array (
                'id_reg' => 2378,
                'id_regmf' => 1886,
                'doors' => 2,
            ),
            378 => 
            array (
                'id_reg' => 2379,
                'id_regmf' => 1887,
                'doors' => 2,
            ),
            379 => 
            array (
                'id_reg' => 2380,
                'id_regmf' => 1888,
                'doors' => 2,
            ),
            380 => 
            array (
                'id_reg' => 2381,
                'id_regmf' => 1889,
                'doors' => 4,
            ),
            381 => 
            array (
                'id_reg' => 2382,
                'id_regmf' => 1890,
                'doors' => 4,
            ),
            382 => 
            array (
                'id_reg' => 2383,
                'id_regmf' => 1891,
                'doors' => 4,
            ),
            383 => 
            array (
                'id_reg' => 2384,
                'id_regmf' => 1892,
                'doors' => 4,
            ),
            384 => 
            array (
                'id_reg' => 2385,
                'id_regmf' => 1893,
                'doors' => 5,
            ),
            385 => 
            array (
                'id_reg' => 2386,
                'id_regmf' => 1894,
                'doors' => 5,
            ),
            386 => 
            array (
                'id_reg' => 2387,
                'id_regmf' => 1896,
                'doors' => 5,
            ),
            387 => 
            array (
                'id_reg' => 2388,
                'id_regmf' => 1895,
                'doors' => 5,
            ),
            388 => 
            array (
                'id_reg' => 2389,
                'id_regmf' => 1897,
                'doors' => 5,
            ),
            389 => 
            array (
                'id_reg' => 2390,
                'id_regmf' => 1898,
                'doors' => 5,
            ),
            390 => 
            array (
                'id_reg' => 2391,
                'id_regmf' => 1899,
                'doors' => 2,
            ),
            391 => 
            array (
                'id_reg' => 2392,
                'id_regmf' => 1900,
                'doors' => 2,
            ),
            392 => 
            array (
                'id_reg' => 2393,
                'id_regmf' => 1901,
                'doors' => 5,
            ),
            393 => 
            array (
                'id_reg' => 2394,
                'id_regmf' => 1902,
                'doors' => 5,
            ),
            394 => 
            array (
                'id_reg' => 2395,
                'id_regmf' => 1903,
                'doors' => 5,
            ),
            395 => 
            array (
                'id_reg' => 2396,
                'id_regmf' => 1904,
                'doors' => 5,
            ),
            396 => 
            array (
                'id_reg' => 2397,
                'id_regmf' => 1905,
                'doors' => 5,
            ),
            397 => 
            array (
                'id_reg' => 2398,
                'id_regmf' => 1906,
                'doors' => 2,
            ),
            398 => 
            array (
                'id_reg' => 2399,
                'id_regmf' => 1907,
                'doors' => 2,
            ),
            399 => 
            array (
                'id_reg' => 2400,
                'id_regmf' => 1908,
                'doors' => 2,
            ),
            400 => 
            array (
                'id_reg' => 2401,
                'id_regmf' => 1909,
                'doors' => 2,
            ),
            401 => 
            array (
                'id_reg' => 2402,
                'id_regmf' => 1910,
                'doors' => 2,
            ),
            402 => 
            array (
                'id_reg' => 2403,
                'id_regmf' => 1911,
                'doors' => 2,
            ),
            403 => 
            array (
                'id_reg' => 2404,
                'id_regmf' => 1912,
                'doors' => 2,
            ),
            404 => 
            array (
                'id_reg' => 2405,
                'id_regmf' => 1913,
                'doors' => 2,
            ),
            405 => 
            array (
                'id_reg' => 2406,
                'id_regmf' => 1914,
                'doors' => 3,
            ),
            406 => 
            array (
                'id_reg' => 2407,
                'id_regmf' => 1915,
                'doors' => 3,
            ),
            407 => 
            array (
                'id_reg' => 2408,
                'id_regmf' => 1916,
                'doors' => 2,
            ),
            408 => 
            array (
                'id_reg' => 2409,
                'id_regmf' => 1917,
                'doors' => 3,
            ),
            409 => 
            array (
                'id_reg' => 2410,
                'id_regmf' => 1918,
                'doors' => 3,
            ),
            410 => 
            array (
                'id_reg' => 2411,
                'id_regmf' => 1919,
                'doors' => 3,
            ),
            411 => 
            array (
                'id_reg' => 2412,
                'id_regmf' => 1920,
                'doors' => 3,
            ),
            412 => 
            array (
                'id_reg' => 2413,
                'id_regmf' => 1921,
                'doors' => 2,
            ),
            413 => 
            array (
                'id_reg' => 2414,
                'id_regmf' => 1922,
                'doors' => 3,
            ),
            414 => 
            array (
                'id_reg' => 2415,
                'id_regmf' => 1923,
                'doors' => 3,
            ),
            415 => 
            array (
                'id_reg' => 2416,
                'id_regmf' => 1924,
                'doors' => 3,
            ),
            416 => 
            array (
                'id_reg' => 2417,
                'id_regmf' => 1925,
                'doors' => 3,
            ),
            417 => 
            array (
                'id_reg' => 2418,
                'id_regmf' => 1926,
                'doors' => 3,
            ),
            418 => 
            array (
                'id_reg' => 2419,
                'id_regmf' => 1927,
                'doors' => 3,
            ),
            419 => 
            array (
                'id_reg' => 2420,
                'id_regmf' => 1928,
                'doors' => 3,
            ),
            420 => 
            array (
                'id_reg' => 2421,
                'id_regmf' => 1929,
                'doors' => 2,
            ),
            421 => 
            array (
                'id_reg' => 2422,
                'id_regmf' => 1931,
                'doors' => 3,
            ),
            422 => 
            array (
                'id_reg' => 2423,
                'id_regmf' => 1930,
                'doors' => 3,
            ),
            423 => 
            array (
                'id_reg' => 2424,
                'id_regmf' => 1932,
                'doors' => 2,
            ),
            424 => 
            array (
                'id_reg' => 2425,
                'id_regmf' => 1933,
                'doors' => 5,
            ),
            425 => 
            array (
                'id_reg' => 2426,
                'id_regmf' => 1934,
                'doors' => 4,
            ),
            426 => 
            array (
                'id_reg' => 2427,
                'id_regmf' => 1935,
                'doors' => 2,
            ),
            427 => 
            array (
                'id_reg' => 2428,
                'id_regmf' => 1935,
                'doors' => 4,
            ),
            428 => 
            array (
                'id_reg' => 2429,
                'id_regmf' => 1936,
                'doors' => 4,
            ),
            429 => 
            array (
                'id_reg' => 2430,
                'id_regmf' => 1937,
                'doors' => 2,
            ),
            430 => 
            array (
                'id_reg' => 2431,
                'id_regmf' => 1937,
                'doors' => 4,
            ),
            431 => 
            array (
                'id_reg' => 2432,
                'id_regmf' => 1938,
                'doors' => 4,
            ),
            432 => 
            array (
                'id_reg' => 2433,
                'id_regmf' => 1939,
                'doors' => 2,
            ),
            433 => 
            array (
                'id_reg' => 2434,
                'id_regmf' => 1940,
                'doors' => 4,
            ),
            434 => 
            array (
                'id_reg' => 2435,
                'id_regmf' => 1941,
                'doors' => 4,
            ),
            435 => 
            array (
                'id_reg' => 2436,
                'id_regmf' => 1942,
                'doors' => 4,
            ),
            436 => 
            array (
                'id_reg' => 2437,
                'id_regmf' => 1943,
                'doors' => 2,
            ),
            437 => 
            array (
                'id_reg' => 2438,
                'id_regmf' => 1944,
                'doors' => 2,
            ),
            438 => 
            array (
                'id_reg' => 2439,
                'id_regmf' => 1945,
                'doors' => 2,
            ),
            439 => 
            array (
                'id_reg' => 2440,
                'id_regmf' => 1946,
                'doors' => 5,
            ),
            440 => 
            array (
                'id_reg' => 2441,
                'id_regmf' => 1947,
                'doors' => 5,
            ),
            441 => 
            array (
                'id_reg' => 2442,
                'id_regmf' => 1948,
                'doors' => 5,
            ),
            442 => 
            array (
                'id_reg' => 2443,
                'id_regmf' => 1949,
                'doors' => 4,
            ),
            443 => 
            array (
                'id_reg' => 2444,
                'id_regmf' => 1950,
                'doors' => 4,
            ),
            444 => 
            array (
                'id_reg' => 2445,
                'id_regmf' => 1951,
                'doors' => 4,
            ),
            445 => 
            array (
                'id_reg' => 2446,
                'id_regmf' => 1952,
                'doors' => 4,
            ),
            446 => 
            array (
                'id_reg' => 2447,
                'id_regmf' => 1953,
                'doors' => 4,
            ),
            447 => 
            array (
                'id_reg' => 2448,
                'id_regmf' => 1954,
                'doors' => 4,
            ),
            448 => 
            array (
                'id_reg' => 2449,
                'id_regmf' => 1955,
                'doors' => 4,
            ),
            449 => 
            array (
                'id_reg' => 2450,
                'id_regmf' => 1956,
                'doors' => 0,
            ),
            450 => 
            array (
                'id_reg' => 2451,
                'id_regmf' => 1957,
                'doors' => 0,
            ),
            451 => 
            array (
                'id_reg' => 2452,
                'id_regmf' => 1958,
                'doors' => 0,
            ),
            452 => 
            array (
                'id_reg' => 2453,
                'id_regmf' => 1958,
                'doors' => 2,
            ),
            453 => 
            array (
                'id_reg' => 2454,
                'id_regmf' => 1959,
                'doors' => 2,
            ),
            454 => 
            array (
                'id_reg' => 2455,
                'id_regmf' => 1960,
                'doors' => 2,
            ),
            455 => 
            array (
                'id_reg' => 2456,
                'id_regmf' => 1961,
                'doors' => 2,
            ),
            456 => 
            array (
                'id_reg' => 2457,
                'id_regmf' => 1962,
                'doors' => 2,
            ),
            457 => 
            array (
                'id_reg' => 2458,
                'id_regmf' => 1963,
                'doors' => 2,
            ),
            458 => 
            array (
                'id_reg' => 2459,
                'id_regmf' => 1964,
                'doors' => 2,
            ),
            459 => 
            array (
                'id_reg' => 2460,
                'id_regmf' => 1965,
                'doors' => 2,
            ),
            460 => 
            array (
                'id_reg' => 2461,
                'id_regmf' => 1965,
                'doors' => 3,
            ),
            461 => 
            array (
                'id_reg' => 2462,
                'id_regmf' => 1966,
                'doors' => 2,
            ),
            462 => 
            array (
                'id_reg' => 2463,
                'id_regmf' => 1967,
                'doors' => 2,
            ),
            463 => 
            array (
                'id_reg' => 2464,
                'id_regmf' => 1968,
                'doors' => 2,
            ),
            464 => 
            array (
                'id_reg' => 2465,
                'id_regmf' => 1969,
                'doors' => 4,
            ),
            465 => 
            array (
                'id_reg' => 2466,
                'id_regmf' => 1970,
                'doors' => 4,
            ),
            466 => 
            array (
                'id_reg' => 2467,
                'id_regmf' => 1971,
                'doors' => 4,
            ),
            467 => 
            array (
                'id_reg' => 2468,
                'id_regmf' => 1972,
                'doors' => 4,
            ),
            468 => 
            array (
                'id_reg' => 2469,
                'id_regmf' => 1973,
                'doors' => 2,
            ),
            469 => 
            array (
                'id_reg' => 2470,
                'id_regmf' => 1973,
                'doors' => 4,
            ),
            470 => 
            array (
                'id_reg' => 2471,
                'id_regmf' => 1974,
                'doors' => 2,
            ),
            471 => 
            array (
                'id_reg' => 2472,
                'id_regmf' => 1976,
                'doors' => 3,
            ),
            472 => 
            array (
                'id_reg' => 2473,
                'id_regmf' => 1975,
                'doors' => 3,
            ),
            473 => 
            array (
                'id_reg' => 2474,
                'id_regmf' => 1978,
                'doors' => 3,
            ),
            474 => 
            array (
                'id_reg' => 2475,
                'id_regmf' => 1977,
                'doors' => 2,
            ),
            475 => 
            array (
                'id_reg' => 2476,
                'id_regmf' => 1979,
                'doors' => 2,
            ),
            476 => 
            array (
                'id_reg' => 2477,
                'id_regmf' => 1979,
                'doors' => 4,
            ),
            477 => 
            array (
                'id_reg' => 2478,
                'id_regmf' => 1980,
                'doors' => 2,
            ),
            478 => 
            array (
                'id_reg' => 2479,
                'id_regmf' => 1980,
                'doors' => 4,
            ),
            479 => 
            array (
                'id_reg' => 2480,
                'id_regmf' => 1980,
                'doors' => 5,
            ),
            480 => 
            array (
                'id_reg' => 2481,
                'id_regmf' => 1981,
                'doors' => 2,
            ),
            481 => 
            array (
                'id_reg' => 2482,
                'id_regmf' => 1981,
                'doors' => 4,
            ),
            482 => 
            array (
                'id_reg' => 2483,
                'id_regmf' => 1982,
                'doors' => 5,
            ),
            483 => 
            array (
                'id_reg' => 2484,
                'id_regmf' => 1983,
                'doors' => 2,
            ),
            484 => 
            array (
                'id_reg' => 2485,
                'id_regmf' => 1984,
                'doors' => 5,
            ),
            485 => 
            array (
                'id_reg' => 2486,
                'id_regmf' => 1985,
                'doors' => 2,
            ),
            486 => 
            array (
                'id_reg' => 2487,
                'id_regmf' => 1986,
                'doors' => 4,
            ),
            487 => 
            array (
                'id_reg' => 2488,
                'id_regmf' => 1987,
                'doors' => 2,
            ),
            488 => 
            array (
                'id_reg' => 2489,
                'id_regmf' => 1988,
                'doors' => 2,
            ),
            489 => 
            array (
                'id_reg' => 2490,
                'id_regmf' => 1989,
                'doors' => 2,
            ),
            490 => 
            array (
                'id_reg' => 2491,
                'id_regmf' => 1990,
                'doors' => 2,
            ),
            491 => 
            array (
                'id_reg' => 2492,
                'id_regmf' => 1991,
                'doors' => 2,
            ),
            492 => 
            array (
                'id_reg' => 2493,
                'id_regmf' => 1992,
                'doors' => 2,
            ),
            493 => 
            array (
                'id_reg' => 2494,
                'id_regmf' => 1993,
                'doors' => 2,
            ),
            494 => 
            array (
                'id_reg' => 2495,
                'id_regmf' => 1994,
                'doors' => 3,
            ),
            495 => 
            array (
                'id_reg' => 2496,
                'id_regmf' => 1995,
                'doors' => 3,
            ),
            496 => 
            array (
                'id_reg' => 2497,
                'id_regmf' => 1996,
                'doors' => 2,
            ),
            497 => 
            array (
                'id_reg' => 2498,
                'id_regmf' => 1997,
                'doors' => 2,
            ),
            498 => 
            array (
                'id_reg' => 2499,
                'id_regmf' => 1998,
                'doors' => 2,
            ),
            499 => 
            array (
                'id_reg' => 2500,
                'id_regmf' => 1999,
                'doors' => 2,
            ),
        ));
        \DB::table('model_fuel_numdoors')->insert(array (
            0 => 
            array (
                'id_reg' => 2501,
                'id_regmf' => 2000,
                'doors' => 2,
            ),
            1 => 
            array (
                'id_reg' => 2502,
                'id_regmf' => 2005,
                'doors' => 2,
            ),
            2 => 
            array (
                'id_reg' => 2503,
                'id_regmf' => 2006,
                'doors' => 2,
            ),
            3 => 
            array (
                'id_reg' => 2504,
                'id_regmf' => 2007,
                'doors' => 2,
            ),
            4 => 
            array (
                'id_reg' => 2505,
                'id_regmf' => 2001,
                'doors' => 4,
            ),
            5 => 
            array (
                'id_reg' => 2506,
                'id_regmf' => 2002,
                'doors' => 4,
            ),
            6 => 
            array (
                'id_reg' => 2507,
                'id_regmf' => 2003,
                'doors' => 4,
            ),
            7 => 
            array (
                'id_reg' => 2508,
                'id_regmf' => 2004,
                'doors' => 4,
            ),
            8 => 
            array (
                'id_reg' => 2509,
                'id_regmf' => 2008,
                'doors' => 2,
            ),
            9 => 
            array (
                'id_reg' => 2510,
                'id_regmf' => 2009,
                'doors' => 2,
            ),
            10 => 
            array (
                'id_reg' => 2511,
                'id_regmf' => 2010,
                'doors' => 2,
            ),
            11 => 
            array (
                'id_reg' => 2512,
                'id_regmf' => 2011,
                'doors' => 2,
            ),
            12 => 
            array (
                'id_reg' => 2513,
                'id_regmf' => 2012,
                'doors' => 4,
            ),
            13 => 
            array (
                'id_reg' => 2514,
                'id_regmf' => 2013,
                'doors' => 2,
            ),
            14 => 
            array (
                'id_reg' => 2515,
                'id_regmf' => 2013,
                'doors' => 4,
            ),
            15 => 
            array (
                'id_reg' => 2516,
                'id_regmf' => 2014,
                'doors' => 2,
            ),
            16 => 
            array (
                'id_reg' => 2517,
                'id_regmf' => 2015,
                'doors' => 2,
            ),
            17 => 
            array (
                'id_reg' => 2518,
                'id_regmf' => 2016,
                'doors' => 2,
            ),
            18 => 
            array (
                'id_reg' => 2519,
                'id_regmf' => 2017,
                'doors' => 2,
            ),
            19 => 
            array (
                'id_reg' => 2520,
                'id_regmf' => 2018,
                'doors' => 2,
            ),
            20 => 
            array (
                'id_reg' => 2521,
                'id_regmf' => 2020,
                'doors' => 5,
            ),
            21 => 
            array (
                'id_reg' => 2522,
                'id_regmf' => 2019,
                'doors' => 5,
            ),
            22 => 
            array (
                'id_reg' => 2523,
                'id_regmf' => 2021,
                'doors' => 2,
            ),
            23 => 
            array (
                'id_reg' => 2524,
                'id_regmf' => 2022,
                'doors' => 2,
            ),
            24 => 
            array (
                'id_reg' => 2525,
                'id_regmf' => 2023,
                'doors' => 4,
            ),
            25 => 
            array (
                'id_reg' => 2526,
                'id_regmf' => 2024,
                'doors' => 4,
            ),
            26 => 
            array (
                'id_reg' => 2527,
                'id_regmf' => 2025,
                'doors' => 4,
            ),
            27 => 
            array (
                'id_reg' => 2528,
                'id_regmf' => 2026,
                'doors' => 2,
            ),
            28 => 
            array (
                'id_reg' => 2529,
                'id_regmf' => 2027,
                'doors' => 2,
            ),
            29 => 
            array (
                'id_reg' => 2530,
                'id_regmf' => 2028,
                'doors' => 4,
            ),
            30 => 
            array (
                'id_reg' => 2531,
                'id_regmf' => 2029,
                'doors' => 3,
            ),
            31 => 
            array (
                'id_reg' => 2532,
                'id_regmf' => 2031,
                'doors' => 5,
            ),
            32 => 
            array (
                'id_reg' => 2533,
                'id_regmf' => 2030,
                'doors' => 5,
            ),
            33 => 
            array (
                'id_reg' => 2534,
                'id_regmf' => 2032,
                'doors' => 5,
            ),
            34 => 
            array (
                'id_reg' => 2535,
                'id_regmf' => 2033,
                'doors' => 2,
            ),
            35 => 
            array (
                'id_reg' => 2536,
                'id_regmf' => 2034,
                'doors' => 2,
            ),
            36 => 
            array (
                'id_reg' => 2537,
                'id_regmf' => 2035,
                'doors' => 3,
            ),
            37 => 
            array (
                'id_reg' => 2538,
                'id_regmf' => 2036,
                'doors' => 3,
            ),
            38 => 
            array (
                'id_reg' => 2539,
                'id_regmf' => 2037,
                'doors' => 2,
            ),
            39 => 
            array (
                'id_reg' => 2540,
                'id_regmf' => 2038,
                'doors' => 4,
            ),
            40 => 
            array (
                'id_reg' => 2541,
                'id_regmf' => 2039,
                'doors' => 5,
            ),
            41 => 
            array (
                'id_reg' => 2542,
                'id_regmf' => 2040,
                'doors' => 3,
            ),
            42 => 
            array (
                'id_reg' => 2543,
                'id_regmf' => 2040,
                'doors' => 5,
            ),
            43 => 
            array (
                'id_reg' => 2544,
                'id_regmf' => 2041,
                'doors' => 4,
            ),
            44 => 
            array (
                'id_reg' => 2545,
                'id_regmf' => 2042,
                'doors' => 4,
            ),
            45 => 
            array (
                'id_reg' => 2546,
                'id_regmf' => 2043,
                'doors' => 3,
            ),
            46 => 
            array (
                'id_reg' => 2547,
                'id_regmf' => 2044,
                'doors' => 4,
            ),
            47 => 
            array (
                'id_reg' => 2548,
                'id_regmf' => 2046,
                'doors' => 2,
            ),
            48 => 
            array (
                'id_reg' => 2549,
                'id_regmf' => 2046,
                'doors' => 4,
            ),
            49 => 
            array (
                'id_reg' => 2550,
                'id_regmf' => 2045,
                'doors' => 4,
            ),
            50 => 
            array (
                'id_reg' => 2551,
                'id_regmf' => 2047,
                'doors' => 4,
            ),
            51 => 
            array (
                'id_reg' => 2552,
                'id_regmf' => 2048,
                'doors' => 4,
            ),
            52 => 
            array (
                'id_reg' => 2553,
                'id_regmf' => 2050,
                'doors' => 5,
            ),
            53 => 
            array (
                'id_reg' => 2554,
                'id_regmf' => 2049,
                'doors' => 5,
            ),
            54 => 
            array (
                'id_reg' => 2555,
                'id_regmf' => 2052,
                'doors' => 5,
            ),
            55 => 
            array (
                'id_reg' => 2556,
                'id_regmf' => 2051,
                'doors' => 5,
            ),
            56 => 
            array (
                'id_reg' => 2557,
                'id_regmf' => 2053,
                'doors' => 5,
            ),
            57 => 
            array (
                'id_reg' => 2558,
                'id_regmf' => 2054,
                'doors' => 5,
            ),
            58 => 
            array (
                'id_reg' => 2559,
                'id_regmf' => 2055,
                'doors' => 5,
            ),
            59 => 
            array (
                'id_reg' => 2560,
                'id_regmf' => 2056,
                'doors' => 5,
            ),
            60 => 
            array (
                'id_reg' => 2561,
                'id_regmf' => 2057,
                'doors' => 2,
            ),
            61 => 
            array (
                'id_reg' => 2562,
                'id_regmf' => 2057,
                'doors' => 5,
            ),
            62 => 
            array (
                'id_reg' => 2563,
                'id_regmf' => 2058,
                'doors' => 4,
            ),
            63 => 
            array (
                'id_reg' => 2564,
                'id_regmf' => 2058,
                'doors' => 5,
            ),
            64 => 
            array (
                'id_reg' => 2565,
                'id_regmf' => 2059,
                'doors' => 5,
            ),
            65 => 
            array (
                'id_reg' => 2566,
                'id_regmf' => 2060,
                'doors' => 4,
            ),
            66 => 
            array (
                'id_reg' => 2567,
                'id_regmf' => 2061,
                'doors' => 5,
            ),
            67 => 
            array (
                'id_reg' => 2568,
                'id_regmf' => 2062,
                'doors' => 5,
            ),
            68 => 
            array (
                'id_reg' => 2569,
                'id_regmf' => 2063,
                'doors' => 2,
            ),
            69 => 
            array (
                'id_reg' => 2570,
                'id_regmf' => 2064,
                'doors' => 5,
            ),
            70 => 
            array (
                'id_reg' => 2571,
                'id_regmf' => 2066,
                'doors' => 5,
            ),
            71 => 
            array (
                'id_reg' => 2572,
                'id_regmf' => 2065,
                'doors' => 5,
            ),
            72 => 
            array (
                'id_reg' => 2573,
                'id_regmf' => 2067,
                'doors' => 3,
            ),
            73 => 
            array (
                'id_reg' => 2574,
                'id_regmf' => 2068,
                'doors' => 4,
            ),
            74 => 
            array (
                'id_reg' => 2575,
                'id_regmf' => 2069,
                'doors' => 2,
            ),
            75 => 
            array (
                'id_reg' => 2576,
                'id_regmf' => 2070,
                'doors' => 4,
            ),
            76 => 
            array (
                'id_reg' => 2577,
                'id_regmf' => 2071,
                'doors' => 2,
            ),
            77 => 
            array (
                'id_reg' => 2578,
                'id_regmf' => 2071,
                'doors' => 3,
            ),
            78 => 
            array (
                'id_reg' => 2579,
                'id_regmf' => 2072,
                'doors' => 2,
            ),
            79 => 
            array (
                'id_reg' => 2580,
                'id_regmf' => 2073,
                'doors' => 5,
            ),
            80 => 
            array (
                'id_reg' => 2581,
                'id_regmf' => 2074,
                'doors' => 4,
            ),
            81 => 
            array (
                'id_reg' => 2582,
                'id_regmf' => 2075,
                'doors' => 4,
            ),
            82 => 
            array (
                'id_reg' => 2583,
                'id_regmf' => 2077,
                'doors' => 0,
            ),
            83 => 
            array (
                'id_reg' => 2584,
                'id_regmf' => 2077,
                'doors' => 2,
            ),
            84 => 
            array (
                'id_reg' => 2585,
                'id_regmf' => 2077,
                'doors' => 3,
            ),
            85 => 
            array (
                'id_reg' => 2586,
                'id_regmf' => 2076,
                'doors' => 2,
            ),
            86 => 
            array (
                'id_reg' => 2587,
                'id_regmf' => 2076,
                'doors' => 3,
            ),
            87 => 
            array (
                'id_reg' => 2588,
                'id_regmf' => 2078,
                'doors' => 2,
            ),
            88 => 
            array (
                'id_reg' => 2589,
                'id_regmf' => 2079,
                'doors' => 2,
            ),
            89 => 
            array (
                'id_reg' => 2590,
                'id_regmf' => 2080,
                'doors' => 2,
            ),
            90 => 
            array (
                'id_reg' => 2591,
                'id_regmf' => 2081,
                'doors' => 2,
            ),
            91 => 
            array (
                'id_reg' => 2592,
                'id_regmf' => 2083,
                'doors' => 2,
            ),
            92 => 
            array (
                'id_reg' => 2593,
                'id_regmf' => 2082,
                'doors' => 3,
            ),
            93 => 
            array (
                'id_reg' => 2594,
                'id_regmf' => 2084,
                'doors' => 3,
            ),
            94 => 
            array (
                'id_reg' => 2595,
                'id_regmf' => 2085,
                'doors' => 0,
            ),
            95 => 
            array (
                'id_reg' => 2596,
                'id_regmf' => 2086,
                'doors' => 0,
            ),
            96 => 
            array (
                'id_reg' => 2597,
                'id_regmf' => 2087,
                'doors' => 0,
            ),
            97 => 
            array (
                'id_reg' => 2598,
                'id_regmf' => 2088,
                'doors' => 0,
            ),
            98 => 
            array (
                'id_reg' => 2599,
                'id_regmf' => 2089,
                'doors' => 0,
            ),
            99 => 
            array (
                'id_reg' => 2600,
                'id_regmf' => 2090,
                'doors' => 0,
            ),
            100 => 
            array (
                'id_reg' => 2601,
                'id_regmf' => 2091,
                'doors' => 3,
            ),
            101 => 
            array (
                'id_reg' => 2602,
                'id_regmf' => 2092,
                'doors' => 3,
            ),
            102 => 
            array (
                'id_reg' => 2603,
                'id_regmf' => 2093,
                'doors' => 0,
            ),
            103 => 
            array (
                'id_reg' => 2604,
                'id_regmf' => 2094,
                'doors' => 3,
            ),
            104 => 
            array (
                'id_reg' => 2605,
                'id_regmf' => 2095,
                'doors' => 0,
            ),
            105 => 
            array (
                'id_reg' => 2606,
                'id_regmf' => 2096,
                'doors' => 0,
            ),
            106 => 
            array (
                'id_reg' => 2607,
                'id_regmf' => 2096,
                'doors' => 3,
            ),
            107 => 
            array (
                'id_reg' => 2608,
                'id_regmf' => 2097,
                'doors' => 0,
            ),
            108 => 
            array (
                'id_reg' => 2609,
                'id_regmf' => 2098,
                'doors' => 4,
            ),
            109 => 
            array (
                'id_reg' => 2610,
                'id_regmf' => 2099,
                'doors' => 2,
            ),
            110 => 
            array (
                'id_reg' => 2611,
                'id_regmf' => 2099,
                'doors' => 4,
            ),
            111 => 
            array (
                'id_reg' => 2612,
                'id_regmf' => 2100,
                'doors' => 4,
            ),
            112 => 
            array (
                'id_reg' => 2613,
                'id_regmf' => 2102,
                'doors' => 4,
            ),
            113 => 
            array (
                'id_reg' => 2614,
                'id_regmf' => 2101,
                'doors' => 4,
            ),
            114 => 
            array (
                'id_reg' => 2615,
                'id_regmf' => 2103,
                'doors' => 4,
            ),
            115 => 
            array (
                'id_reg' => 2616,
                'id_regmf' => 2104,
                'doors' => 4,
            ),
            116 => 
            array (
                'id_reg' => 2617,
                'id_regmf' => 2106,
                'doors' => 2,
            ),
            117 => 
            array (
                'id_reg' => 2618,
                'id_regmf' => 2105,
                'doors' => 4,
            ),
            118 => 
            array (
                'id_reg' => 2619,
                'id_regmf' => 2107,
                'doors' => 4,
            ),
            119 => 
            array (
                'id_reg' => 2620,
                'id_regmf' => 2108,
                'doors' => 4,
            ),
            120 => 
            array (
                'id_reg' => 2621,
                'id_regmf' => 2109,
                'doors' => 4,
            ),
            121 => 
            array (
                'id_reg' => 2622,
                'id_regmf' => 2111,
                'doors' => 4,
            ),
            122 => 
            array (
                'id_reg' => 2623,
                'id_regmf' => 2110,
                'doors' => 4,
            ),
            123 => 
            array (
                'id_reg' => 2624,
                'id_regmf' => 2112,
                'doors' => 4,
            ),
            124 => 
            array (
                'id_reg' => 2625,
                'id_regmf' => 2113,
                'doors' => 4,
            ),
            125 => 
            array (
                'id_reg' => 2626,
                'id_regmf' => 2113,
                'doors' => 5,
            ),
            126 => 
            array (
                'id_reg' => 2627,
                'id_regmf' => 2114,
                'doors' => 4,
            ),
            127 => 
            array (
                'id_reg' => 2628,
                'id_regmf' => 2114,
                'doors' => 5,
            ),
            128 => 
            array (
                'id_reg' => 2629,
                'id_regmf' => 2115,
                'doors' => 2,
            ),
            129 => 
            array (
                'id_reg' => 2630,
                'id_regmf' => 2115,
                'doors' => 4,
            ),
            130 => 
            array (
                'id_reg' => 2631,
                'id_regmf' => 2115,
                'doors' => 5,
            ),
            131 => 
            array (
                'id_reg' => 2632,
                'id_regmf' => 2116,
                'doors' => 2,
            ),
            132 => 
            array (
                'id_reg' => 2633,
                'id_regmf' => 2117,
                'doors' => 3,
            ),
            133 => 
            array (
                'id_reg' => 2634,
                'id_regmf' => 2118,
                'doors' => 0,
            ),
            134 => 
            array (
                'id_reg' => 2635,
                'id_regmf' => 2118,
                'doors' => 2,
            ),
            135 => 
            array (
                'id_reg' => 2636,
                'id_regmf' => 2118,
                'doors' => 4,
            ),
            136 => 
            array (
                'id_reg' => 2637,
                'id_regmf' => 2118,
                'doors' => 5,
            ),
            137 => 
            array (
                'id_reg' => 2638,
                'id_regmf' => 2120,
                'doors' => 2,
            ),
            138 => 
            array (
                'id_reg' => 2639,
                'id_regmf' => 2120,
                'doors' => 5,
            ),
            139 => 
            array (
                'id_reg' => 2640,
                'id_regmf' => 2119,
                'doors' => 0,
            ),
            140 => 
            array (
                'id_reg' => 2641,
                'id_regmf' => 2119,
                'doors' => 2,
            ),
            141 => 
            array (
                'id_reg' => 2642,
                'id_regmf' => 2119,
                'doors' => 5,
            ),
            142 => 
            array (
                'id_reg' => 2643,
                'id_regmf' => 2121,
                'doors' => 5,
            ),
            143 => 
            array (
                'id_reg' => 2644,
                'id_regmf' => 2122,
                'doors' => 4,
            ),
            144 => 
            array (
                'id_reg' => 2645,
                'id_regmf' => 2123,
                'doors' => 2,
            ),
            145 => 
            array (
                'id_reg' => 2646,
                'id_regmf' => 2124,
                'doors' => 2,
            ),
            146 => 
            array (
                'id_reg' => 2647,
                'id_regmf' => 2126,
                'doors' => 4,
            ),
            147 => 
            array (
                'id_reg' => 2648,
                'id_regmf' => 2125,
                'doors' => 4,
            ),
            148 => 
            array (
                'id_reg' => 2649,
                'id_regmf' => 2127,
                'doors' => 4,
            ),
            149 => 
            array (
                'id_reg' => 2650,
                'id_regmf' => 2128,
                'doors' => 2,
            ),
            150 => 
            array (
                'id_reg' => 2651,
                'id_regmf' => 2128,
                'doors' => 4,
            ),
            151 => 
            array (
                'id_reg' => 2652,
                'id_regmf' => 2128,
                'doors' => 5,
            ),
            152 => 
            array (
                'id_reg' => 2653,
                'id_regmf' => 2129,
                'doors' => 2,
            ),
            153 => 
            array (
                'id_reg' => 2654,
                'id_regmf' => 2130,
                'doors' => 4,
            ),
            154 => 
            array (
                'id_reg' => 2655,
                'id_regmf' => 2131,
                'doors' => 4,
            ),
            155 => 
            array (
                'id_reg' => 2656,
                'id_regmf' => 2132,
                'doors' => 2,
            ),
            156 => 
            array (
                'id_reg' => 2657,
                'id_regmf' => 2133,
                'doors' => 4,
            ),
            157 => 
            array (
                'id_reg' => 2658,
                'id_regmf' => 2134,
                'doors' => 2,
            ),
            158 => 
            array (
                'id_reg' => 2659,
                'id_regmf' => 2135,
                'doors' => 4,
            ),
            159 => 
            array (
                'id_reg' => 2660,
                'id_regmf' => 2136,
                'doors' => 2,
            ),
            160 => 
            array (
                'id_reg' => 2661,
                'id_regmf' => 2136,
                'doors' => 4,
            ),
            161 => 
            array (
                'id_reg' => 2662,
                'id_regmf' => 2136,
                'doors' => 5,
            ),
            162 => 
            array (
                'id_reg' => 2663,
                'id_regmf' => 2137,
                'doors' => 2,
            ),
            163 => 
            array (
                'id_reg' => 2664,
                'id_regmf' => 2137,
                'doors' => 4,
            ),
            164 => 
            array (
                'id_reg' => 2665,
                'id_regmf' => 2137,
                'doors' => 5,
            ),
            165 => 
            array (
                'id_reg' => 2666,
                'id_regmf' => 2138,
                'doors' => 3,
            ),
            166 => 
            array (
                'id_reg' => 2667,
                'id_regmf' => 2138,
                'doors' => 5,
            ),
            167 => 
            array (
                'id_reg' => 2668,
                'id_regmf' => 2139,
                'doors' => 3,
            ),
            168 => 
            array (
                'id_reg' => 2669,
                'id_regmf' => 2139,
                'doors' => 5,
            ),
            169 => 
            array (
                'id_reg' => 2670,
                'id_regmf' => 2140,
                'doors' => 4,
            ),
            170 => 
            array (
                'id_reg' => 2671,
                'id_regmf' => 2141,
                'doors' => 4,
            ),
            171 => 
            array (
                'id_reg' => 2672,
                'id_regmf' => 2141,
                'doors' => 5,
            ),
            172 => 
            array (
                'id_reg' => 2673,
                'id_regmf' => 2142,
                'doors' => 3,
            ),
            173 => 
            array (
                'id_reg' => 2674,
                'id_regmf' => 2142,
                'doors' => 5,
            ),
            174 => 
            array (
                'id_reg' => 2675,
                'id_regmf' => 2143,
                'doors' => 4,
            ),
            175 => 
            array (
                'id_reg' => 2676,
                'id_regmf' => 2144,
                'doors' => 2,
            ),
            176 => 
            array (
                'id_reg' => 2677,
                'id_regmf' => 2145,
                'doors' => 2,
            ),
            177 => 
            array (
                'id_reg' => 2678,
                'id_regmf' => 2146,
                'doors' => 2,
            ),
            178 => 
            array (
                'id_reg' => 2679,
                'id_regmf' => 2147,
                'doors' => 4,
            ),
            179 => 
            array (
                'id_reg' => 2680,
                'id_regmf' => 2148,
                'doors' => 2,
            ),
            180 => 
            array (
                'id_reg' => 2681,
                'id_regmf' => 2148,
                'doors' => 4,
            ),
            181 => 
            array (
                'id_reg' => 2682,
                'id_regmf' => 2148,
                'doors' => 5,
            ),
            182 => 
            array (
                'id_reg' => 2683,
                'id_regmf' => 2149,
                'doors' => 4,
            ),
            183 => 
            array (
                'id_reg' => 2684,
                'id_regmf' => 2149,
                'doors' => 5,
            ),
            184 => 
            array (
                'id_reg' => 2685,
                'id_regmf' => 2149,
                'doors' => 6,
            ),
            185 => 
            array (
                'id_reg' => 2686,
                'id_regmf' => 2150,
                'doors' => 3,
            ),
            186 => 
            array (
                'id_reg' => 2687,
                'id_regmf' => 2150,
                'doors' => 5,
            ),
            187 => 
            array (
                'id_reg' => 2688,
                'id_regmf' => 2151,
                'doors' => 4,
            ),
            188 => 
            array (
                'id_reg' => 2689,
                'id_regmf' => 2152,
                'doors' => 4,
            ),
            189 => 
            array (
                'id_reg' => 2690,
                'id_regmf' => 2152,
                'doors' => 6,
            ),
            190 => 
            array (
                'id_reg' => 2691,
                'id_regmf' => 2153,
                'doors' => 4,
            ),
            191 => 
            array (
                'id_reg' => 2692,
                'id_regmf' => 2154,
                'doors' => 2,
            ),
            192 => 
            array (
                'id_reg' => 2693,
                'id_regmf' => 2155,
                'doors' => 4,
            ),
            193 => 
            array (
                'id_reg' => 2694,
                'id_regmf' => 2156,
                'doors' => 4,
            ),
            194 => 
            array (
                'id_reg' => 2695,
                'id_regmf' => 2157,
                'doors' => 2,
            ),
            195 => 
            array (
                'id_reg' => 2696,
                'id_regmf' => 2158,
                'doors' => 2,
            ),
            196 => 
            array (
                'id_reg' => 2697,
                'id_regmf' => 2158,
                'doors' => 4,
            ),
            197 => 
            array (
                'id_reg' => 2698,
                'id_regmf' => 2159,
                'doors' => 4,
            ),
            198 => 
            array (
                'id_reg' => 2699,
                'id_regmf' => 2160,
                'doors' => 2,
            ),
            199 => 
            array (
                'id_reg' => 2700,
                'id_regmf' => 2160,
                'doors' => 4,
            ),
            200 => 
            array (
                'id_reg' => 2701,
                'id_regmf' => 2160,
                'doors' => 5,
            ),
            201 => 
            array (
                'id_reg' => 2702,
                'id_regmf' => 2161,
                'doors' => 4,
            ),
            202 => 
            array (
                'id_reg' => 2703,
                'id_regmf' => 2161,
                'doors' => 5,
            ),
            203 => 
            array (
                'id_reg' => 2704,
                'id_regmf' => 2161,
                'doors' => 6,
            ),
            204 => 
            array (
                'id_reg' => 2705,
                'id_regmf' => 2163,
                'doors' => 4,
            ),
            205 => 
            array (
                'id_reg' => 2706,
                'id_regmf' => 2162,
                'doors' => 4,
            ),
            206 => 
            array (
                'id_reg' => 2707,
                'id_regmf' => 2164,
                'doors' => 5,
            ),
            207 => 
            array (
                'id_reg' => 2708,
                'id_regmf' => 2165,
                'doors' => 3,
            ),
            208 => 
            array (
                'id_reg' => 2709,
                'id_regmf' => 2165,
                'doors' => 5,
            ),
            209 => 
            array (
                'id_reg' => 2710,
                'id_regmf' => 2166,
                'doors' => 3,
            ),
            210 => 
            array (
                'id_reg' => 2711,
                'id_regmf' => 2167,
                'doors' => 3,
            ),
            211 => 
            array (
                'id_reg' => 2712,
                'id_regmf' => 2168,
                'doors' => 2,
            ),
            212 => 
            array (
                'id_reg' => 2713,
                'id_regmf' => 2169,
                'doors' => 4,
            ),
            213 => 
            array (
                'id_reg' => 2714,
                'id_regmf' => 2170,
                'doors' => 2,
            ),
            214 => 
            array (
                'id_reg' => 2715,
                'id_regmf' => 2171,
                'doors' => 4,
            ),
            215 => 
            array (
                'id_reg' => 2716,
                'id_regmf' => 2172,
                'doors' => 4,
            ),
            216 => 
            array (
                'id_reg' => 2717,
                'id_regmf' => 2174,
                'doors' => 2,
            ),
            217 => 
            array (
                'id_reg' => 2718,
                'id_regmf' => 2174,
                'doors' => 4,
            ),
            218 => 
            array (
                'id_reg' => 2719,
                'id_regmf' => 2174,
                'doors' => 5,
            ),
            219 => 
            array (
                'id_reg' => 2720,
                'id_regmf' => 2173,
                'doors' => 4,
            ),
            220 => 
            array (
                'id_reg' => 2721,
                'id_regmf' => 2173,
                'doors' => 5,
            ),
            221 => 
            array (
                'id_reg' => 2722,
                'id_regmf' => 2176,
                'doors' => 4,
            ),
            222 => 
            array (
                'id_reg' => 2723,
                'id_regmf' => 2175,
                'doors' => 4,
            ),
            223 => 
            array (
                'id_reg' => 2724,
                'id_regmf' => 2177,
                'doors' => 2,
            ),
            224 => 
            array (
                'id_reg' => 2725,
                'id_regmf' => 2179,
                'doors' => 2,
            ),
            225 => 
            array (
                'id_reg' => 2726,
                'id_regmf' => 2179,
                'doors' => 4,
            ),
            226 => 
            array (
                'id_reg' => 2727,
                'id_regmf' => 2178,
                'doors' => 4,
            ),
            227 => 
            array (
                'id_reg' => 2728,
                'id_regmf' => 2180,
                'doors' => 4,
            ),
            228 => 
            array (
                'id_reg' => 2729,
                'id_regmf' => 2181,
                'doors' => 4,
            ),
            229 => 
            array (
                'id_reg' => 2730,
                'id_regmf' => 2182,
                'doors' => 2,
            ),
            230 => 
            array (
                'id_reg' => 2731,
                'id_regmf' => 2183,
                'doors' => 3,
            ),
            231 => 
            array (
                'id_reg' => 2732,
                'id_regmf' => 2183,
                'doors' => 5,
            ),
            232 => 
            array (
                'id_reg' => 2733,
                'id_regmf' => 2184,
                'doors' => 3,
            ),
            233 => 
            array (
                'id_reg' => 2734,
                'id_regmf' => 2184,
                'doors' => 5,
            ),
            234 => 
            array (
                'id_reg' => 2735,
                'id_regmf' => 2185,
                'doors' => 3,
            ),
            235 => 
            array (
                'id_reg' => 2736,
                'id_regmf' => 2185,
                'doors' => 5,
            ),
            236 => 
            array (
                'id_reg' => 2737,
                'id_regmf' => 2186,
                'doors' => 0,
            ),
            237 => 
            array (
                'id_reg' => 2738,
                'id_regmf' => 2186,
                'doors' => 2,
            ),
            238 => 
            array (
                'id_reg' => 2739,
                'id_regmf' => 2186,
                'doors' => 5,
            ),
            239 => 
            array (
                'id_reg' => 2740,
                'id_regmf' => 2187,
                'doors' => 0,
            ),
            240 => 
            array (
                'id_reg' => 2741,
                'id_regmf' => 2187,
                'doors' => 2,
            ),
            241 => 
            array (
                'id_reg' => 2742,
                'id_regmf' => 2187,
                'doors' => 3,
            ),
            242 => 
            array (
                'id_reg' => 2743,
                'id_regmf' => 2187,
                'doors' => 4,
            ),
            243 => 
            array (
                'id_reg' => 2744,
                'id_regmf' => 2188,
                'doors' => 2,
            ),
            244 => 
            array (
                'id_reg' => 2745,
                'id_regmf' => 2188,
                'doors' => 4,
            ),
            245 => 
            array (
                'id_reg' => 2746,
                'id_regmf' => 2188,
                'doors' => 5,
            ),
            246 => 
            array (
                'id_reg' => 2747,
                'id_regmf' => 2189,
                'doors' => 2,
            ),
            247 => 
            array (
                'id_reg' => 2748,
                'id_regmf' => 2190,
                'doors' => 4,
            ),
            248 => 
            array (
                'id_reg' => 2749,
                'id_regmf' => 2191,
                'doors' => 4,
            ),
            249 => 
            array (
                'id_reg' => 2750,
                'id_regmf' => 2192,
                'doors' => 4,
            ),
            250 => 
            array (
                'id_reg' => 2751,
                'id_regmf' => 2193,
                'doors' => 3,
            ),
            251 => 
            array (
                'id_reg' => 2752,
                'id_regmf' => 2193,
                'doors' => 5,
            ),
            252 => 
            array (
                'id_reg' => 2753,
                'id_regmf' => 2194,
                'doors' => 2,
            ),
            253 => 
            array (
                'id_reg' => 2754,
                'id_regmf' => 2194,
                'doors' => 4,
            ),
            254 => 
            array (
                'id_reg' => 2755,
                'id_regmf' => 2195,
                'doors' => 4,
            ),
            255 => 
            array (
                'id_reg' => 2756,
                'id_regmf' => 2196,
                'doors' => 4,
            ),
            256 => 
            array (
                'id_reg' => 2757,
                'id_regmf' => 2197,
                'doors' => 2,
            ),
            257 => 
            array (
                'id_reg' => 2758,
                'id_regmf' => 2198,
                'doors' => 2,
            ),
            258 => 
            array (
                'id_reg' => 2759,
                'id_regmf' => 2198,
                'doors' => 5,
            ),
            259 => 
            array (
                'id_reg' => 2760,
                'id_regmf' => 2199,
                'doors' => 2,
            ),
            260 => 
            array (
                'id_reg' => 2761,
                'id_regmf' => 2200,
                'doors' => 2,
            ),
            261 => 
            array (
                'id_reg' => 2762,
                'id_regmf' => 2200,
                'doors' => 4,
            ),
            262 => 
            array (
                'id_reg' => 2763,
                'id_regmf' => 2201,
                'doors' => 2,
            ),
            263 => 
            array (
                'id_reg' => 2764,
                'id_regmf' => 2201,
                'doors' => 4,
            ),
            264 => 
            array (
                'id_reg' => 2765,
                'id_regmf' => 2202,
                'doors' => 2,
            ),
            265 => 
            array (
                'id_reg' => 2766,
                'id_regmf' => 2202,
                'doors' => 4,
            ),
            266 => 
            array (
                'id_reg' => 2767,
                'id_regmf' => 2203,
                'doors' => 2,
            ),
            267 => 
            array (
                'id_reg' => 2768,
                'id_regmf' => 2204,
                'doors' => 4,
            ),
            268 => 
            array (
                'id_reg' => 2769,
                'id_regmf' => 2205,
                'doors' => 2,
            ),
            269 => 
            array (
                'id_reg' => 2770,
                'id_regmf' => 2205,
                'doors' => 4,
            ),
            270 => 
            array (
                'id_reg' => 2771,
                'id_regmf' => 2206,
                'doors' => 2,
            ),
            271 => 
            array (
                'id_reg' => 2772,
                'id_regmf' => 2207,
                'doors' => 2,
            ),
            272 => 
            array (
                'id_reg' => 2773,
                'id_regmf' => 2207,
                'doors' => 4,
            ),
            273 => 
            array (
                'id_reg' => 2774,
                'id_regmf' => 2208,
                'doors' => 5,
            ),
            274 => 
            array (
                'id_reg' => 2775,
                'id_regmf' => 2209,
                'doors' => 0,
            ),
            275 => 
            array (
                'id_reg' => 2776,
                'id_regmf' => 2210,
                'doors' => 2,
            ),
            276 => 
            array (
                'id_reg' => 2777,
                'id_regmf' => 2211,
                'doors' => 2,
            ),
            277 => 
            array (
                'id_reg' => 2778,
                'id_regmf' => 2211,
                'doors' => 4,
            ),
            278 => 
            array (
                'id_reg' => 2779,
                'id_regmf' => 2212,
                'doors' => 4,
            ),
            279 => 
            array (
                'id_reg' => 2780,
                'id_regmf' => 2212,
                'doors' => 6,
            ),
            280 => 
            array (
                'id_reg' => 2781,
                'id_regmf' => 2213,
                'doors' => 2,
            ),
            281 => 
            array (
                'id_reg' => 2782,
                'id_regmf' => 2213,
                'doors' => 4,
            ),
            282 => 
            array (
                'id_reg' => 2783,
                'id_regmf' => 2213,
                'doors' => 6,
            ),
            283 => 
            array (
                'id_reg' => 2784,
                'id_regmf' => 2214,
                'doors' => 2,
            ),
            284 => 
            array (
                'id_reg' => 2785,
                'id_regmf' => 2214,
                'doors' => 4,
            ),
            285 => 
            array (
                'id_reg' => 2786,
                'id_regmf' => 2214,
                'doors' => 5,
            ),
            286 => 
            array (
                'id_reg' => 2787,
                'id_regmf' => 2215,
                'doors' => 2,
            ),
            287 => 
            array (
                'id_reg' => 2788,
                'id_regmf' => 2217,
                'doors' => 5,
            ),
            288 => 
            array (
                'id_reg' => 2789,
                'id_regmf' => 2216,
                'doors' => 5,
            ),
            289 => 
            array (
                'id_reg' => 2790,
                'id_regmf' => 2218,
                'doors' => 3,
            ),
            290 => 
            array (
                'id_reg' => 2791,
                'id_regmf' => 2220,
                'doors' => 2,
            ),
            291 => 
            array (
                'id_reg' => 2792,
                'id_regmf' => 2220,
                'doors' => 4,
            ),
            292 => 
            array (
                'id_reg' => 2793,
                'id_regmf' => 2220,
                'doors' => 5,
            ),
            293 => 
            array (
                'id_reg' => 2794,
                'id_regmf' => 2221,
                'doors' => 4,
            ),
            294 => 
            array (
                'id_reg' => 2795,
                'id_regmf' => 2221,
                'doors' => 5,
            ),
            295 => 
            array (
                'id_reg' => 2796,
                'id_regmf' => 2219,
                'doors' => 2,
            ),
            296 => 
            array (
                'id_reg' => 2797,
                'id_regmf' => 2219,
                'doors' => 4,
            ),
            297 => 
            array (
                'id_reg' => 2798,
                'id_regmf' => 2219,
                'doors' => 5,
            ),
            298 => 
            array (
                'id_reg' => 2799,
                'id_regmf' => 2222,
                'doors' => 4,
            ),
            299 => 
            array (
                'id_reg' => 2800,
                'id_regmf' => 2222,
                'doors' => 5,
            ),
            300 => 
            array (
                'id_reg' => 2801,
                'id_regmf' => 2222,
                'doors' => 6,
            ),
            301 => 
            array (
                'id_reg' => 2802,
                'id_regmf' => 2223,
                'doors' => 5,
            ),
            302 => 
            array (
                'id_reg' => 2803,
                'id_regmf' => 2224,
                'doors' => 2,
            ),
            303 => 
            array (
                'id_reg' => 2804,
                'id_regmf' => 2225,
                'doors' => 2,
            ),
            304 => 
            array (
                'id_reg' => 2805,
                'id_regmf' => 2227,
                'doors' => 4,
            ),
            305 => 
            array (
                'id_reg' => 2806,
                'id_regmf' => 2227,
                'doors' => 5,
            ),
            306 => 
            array (
                'id_reg' => 2807,
                'id_regmf' => 2226,
                'doors' => 4,
            ),
            307 => 
            array (
                'id_reg' => 2808,
                'id_regmf' => 2226,
                'doors' => 5,
            ),
            308 => 
            array (
                'id_reg' => 2809,
                'id_regmf' => 2229,
                'doors' => 3,
            ),
            309 => 
            array (
                'id_reg' => 2810,
                'id_regmf' => 2228,
                'doors' => 3,
            ),
            310 => 
            array (
                'id_reg' => 2811,
                'id_regmf' => 2231,
                'doors' => 4,
            ),
            311 => 
            array (
                'id_reg' => 2812,
                'id_regmf' => 2231,
                'doors' => 5,
            ),
            312 => 
            array (
                'id_reg' => 2813,
                'id_regmf' => 2230,
                'doors' => 4,
            ),
            313 => 
            array (
                'id_reg' => 2814,
                'id_regmf' => 2230,
                'doors' => 5,
            ),
            314 => 
            array (
                'id_reg' => 2815,
                'id_regmf' => 2233,
                'doors' => 4,
            ),
            315 => 
            array (
                'id_reg' => 2816,
                'id_regmf' => 2232,
                'doors' => 4,
            ),
            316 => 
            array (
                'id_reg' => 2817,
                'id_regmf' => 2236,
                'doors' => 4,
            ),
            317 => 
            array (
                'id_reg' => 2818,
                'id_regmf' => 2235,
                'doors' => 4,
            ),
            318 => 
            array (
                'id_reg' => 2819,
                'id_regmf' => 2235,
                'doors' => 5,
            ),
            319 => 
            array (
                'id_reg' => 2820,
                'id_regmf' => 2234,
                'doors' => 4,
            ),
            320 => 
            array (
                'id_reg' => 2821,
                'id_regmf' => 2234,
                'doors' => 5,
            ),
            321 => 
            array (
                'id_reg' => 2822,
                'id_regmf' => 2237,
                'doors' => 2,
            ),
            322 => 
            array (
                'id_reg' => 2823,
                'id_regmf' => 2237,
                'doors' => 3,
            ),
            323 => 
            array (
                'id_reg' => 2824,
                'id_regmf' => 2237,
                'doors' => 5,
            ),
            324 => 
            array (
                'id_reg' => 2825,
                'id_regmf' => 2238,
                'doors' => 2,
            ),
            325 => 
            array (
                'id_reg' => 2826,
                'id_regmf' => 2238,
                'doors' => 3,
            ),
            326 => 
            array (
                'id_reg' => 2827,
                'id_regmf' => 2238,
                'doors' => 5,
            ),
            327 => 
            array (
                'id_reg' => 2828,
                'id_regmf' => 2239,
                'doors' => 3,
            ),
            328 => 
            array (
                'id_reg' => 2829,
                'id_regmf' => 2239,
                'doors' => 5,
            ),
            329 => 
            array (
                'id_reg' => 2830,
                'id_regmf' => 2240,
                'doors' => 3,
            ),
            330 => 
            array (
                'id_reg' => 2831,
                'id_regmf' => 2241,
                'doors' => 2,
            ),
            331 => 
            array (
                'id_reg' => 2832,
                'id_regmf' => 2242,
                'doors' => 3,
            ),
            332 => 
            array (
                'id_reg' => 2833,
                'id_regmf' => 2242,
                'doors' => 5,
            ),
            333 => 
            array (
                'id_reg' => 2834,
                'id_regmf' => 2243,
                'doors' => 5,
            ),
            334 => 
            array (
                'id_reg' => 2835,
                'id_regmf' => 2244,
                'doors' => 5,
            ),
            335 => 
            array (
                'id_reg' => 2836,
                'id_regmf' => 2245,
                'doors' => 3,
            ),
            336 => 
            array (
                'id_reg' => 2837,
                'id_regmf' => 2245,
                'doors' => 5,
            ),
            337 => 
            array (
                'id_reg' => 2838,
                'id_regmf' => 2246,
                'doors' => 3,
            ),
            338 => 
            array (
                'id_reg' => 2839,
                'id_regmf' => 2246,
                'doors' => 5,
            ),
            339 => 
            array (
                'id_reg' => 2840,
                'id_regmf' => 2247,
                'doors' => 3,
            ),
            340 => 
            array (
                'id_reg' => 2841,
                'id_regmf' => 2247,
                'doors' => 5,
            ),
            341 => 
            array (
                'id_reg' => 2842,
                'id_regmf' => 2248,
                'doors' => 3,
            ),
            342 => 
            array (
                'id_reg' => 2843,
                'id_regmf' => 2248,
                'doors' => 5,
            ),
            343 => 
            array (
                'id_reg' => 2844,
                'id_regmf' => 2249,
                'doors' => 3,
            ),
            344 => 
            array (
                'id_reg' => 2845,
                'id_regmf' => 2249,
                'doors' => 5,
            ),
            345 => 
            array (
                'id_reg' => 2846,
                'id_regmf' => 2251,
                'doors' => 5,
            ),
            346 => 
            array (
                'id_reg' => 2847,
                'id_regmf' => 2250,
                'doors' => 5,
            ),
            347 => 
            array (
                'id_reg' => 2848,
                'id_regmf' => 2253,
                'doors' => 5,
            ),
            348 => 
            array (
                'id_reg' => 2849,
                'id_regmf' => 2252,
                'doors' => 5,
            ),
            349 => 
            array (
                'id_reg' => 2850,
                'id_regmf' => 2255,
                'doors' => 5,
            ),
            350 => 
            array (
                'id_reg' => 2851,
                'id_regmf' => 2254,
                'doors' => 5,
            ),
            351 => 
            array (
                'id_reg' => 2852,
                'id_regmf' => 2257,
                'doors' => 5,
            ),
            352 => 
            array (
                'id_reg' => 2853,
                'id_regmf' => 2258,
                'doors' => 5,
            ),
            353 => 
            array (
                'id_reg' => 2854,
                'id_regmf' => 2256,
                'doors' => 5,
            ),
            354 => 
            array (
                'id_reg' => 2855,
                'id_regmf' => 2261,
                'doors' => 5,
            ),
            355 => 
            array (
                'id_reg' => 2856,
                'id_regmf' => 2259,
                'doors' => 5,
            ),
            356 => 
            array (
                'id_reg' => 2857,
                'id_regmf' => 2260,
                'doors' => 5,
            ),
            357 => 
            array (
                'id_reg' => 2858,
                'id_regmf' => 2263,
                'doors' => 5,
            ),
            358 => 
            array (
                'id_reg' => 2859,
                'id_regmf' => 2262,
                'doors' => 5,
            ),
            359 => 
            array (
                'id_reg' => 2860,
                'id_regmf' => 2264,
                'doors' => 5,
            ),
            360 => 
            array (
                'id_reg' => 2861,
                'id_regmf' => 2265,
                'doors' => 5,
            ),
            361 => 
            array (
                'id_reg' => 2862,
                'id_regmf' => 2266,
                'doors' => 2,
            ),
            362 => 
            array (
                'id_reg' => 2863,
                'id_regmf' => 2266,
                'doors' => 5,
            ),
            363 => 
            array (
                'id_reg' => 2864,
                'id_regmf' => 2267,
                'doors' => 0,
            ),
            364 => 
            array (
                'id_reg' => 2865,
                'id_regmf' => 2268,
                'doors' => 2,
            ),
            365 => 
            array (
                'id_reg' => 2866,
                'id_regmf' => 2269,
                'doors' => 0,
            ),
            366 => 
            array (
                'id_reg' => 2867,
                'id_regmf' => 2269,
                'doors' => 2,
            ),
            367 => 
            array (
                'id_reg' => 2868,
                'id_regmf' => 2269,
                'doors' => 5,
            ),
            368 => 
            array (
                'id_reg' => 2869,
                'id_regmf' => 2270,
                'doors' => 0,
            ),
            369 => 
            array (
                'id_reg' => 2870,
                'id_regmf' => 2270,
                'doors' => 2,
            ),
            370 => 
            array (
                'id_reg' => 2871,
                'id_regmf' => 2271,
                'doors' => 2,
            ),
            371 => 
            array (
                'id_reg' => 2872,
                'id_regmf' => 2272,
                'doors' => 0,
            ),
            372 => 
            array (
                'id_reg' => 2873,
                'id_regmf' => 2273,
                'doors' => 5,
            ),
            373 => 
            array (
                'id_reg' => 2874,
                'id_regmf' => 2274,
                'doors' => 5,
            ),
            374 => 
            array (
                'id_reg' => 2875,
                'id_regmf' => 2275,
                'doors' => 2,
            ),
            375 => 
            array (
                'id_reg' => 2876,
                'id_regmf' => 2277,
                'doors' => 5,
            ),
            376 => 
            array (
                'id_reg' => 2877,
                'id_regmf' => 2276,
                'doors' => 5,
            ),
            377 => 
            array (
                'id_reg' => 2878,
                'id_regmf' => 2278,
                'doors' => 4,
            ),
            378 => 
            array (
                'id_reg' => 2879,
                'id_regmf' => 2279,
                'doors' => 0,
            ),
            379 => 
            array (
                'id_reg' => 2880,
                'id_regmf' => 2279,
                'doors' => 2,
            ),
            380 => 
            array (
                'id_reg' => 2881,
                'id_regmf' => 2279,
                'doors' => 5,
            ),
            381 => 
            array (
                'id_reg' => 2882,
                'id_regmf' => 2280,
                'doors' => 0,
            ),
            382 => 
            array (
                'id_reg' => 2883,
                'id_regmf' => 2280,
                'doors' => 5,
            ),
            383 => 
            array (
                'id_reg' => 2884,
                'id_regmf' => 2281,
                'doors' => 0,
            ),
            384 => 
            array (
                'id_reg' => 2885,
                'id_regmf' => 2281,
                'doors' => 2,
            ),
            385 => 
            array (
                'id_reg' => 2886,
                'id_regmf' => 2281,
                'doors' => 5,
            ),
            386 => 
            array (
                'id_reg' => 2887,
                'id_regmf' => 2282,
                'doors' => 2,
            ),
            387 => 
            array (
                'id_reg' => 2888,
                'id_regmf' => 2282,
                'doors' => 5,
            ),
            388 => 
            array (
                'id_reg' => 2889,
                'id_regmf' => 2283,
                'doors' => 5,
            ),
            389 => 
            array (
                'id_reg' => 2890,
                'id_regmf' => 2284,
                'doors' => 0,
            ),
            390 => 
            array (
                'id_reg' => 2891,
                'id_regmf' => 2284,
                'doors' => 2,
            ),
            391 => 
            array (
                'id_reg' => 2892,
                'id_regmf' => 2284,
                'doors' => 4,
            ),
            392 => 
            array (
                'id_reg' => 2893,
                'id_regmf' => 2284,
                'doors' => 5,
            ),
            393 => 
            array (
                'id_reg' => 2894,
                'id_regmf' => 2285,
                'doors' => 0,
            ),
            394 => 
            array (
                'id_reg' => 2895,
                'id_regmf' => 2285,
                'doors' => 2,
            ),
            395 => 
            array (
                'id_reg' => 2896,
                'id_regmf' => 2286,
                'doors' => 0,
            ),
            396 => 
            array (
                'id_reg' => 2897,
                'id_regmf' => 2286,
                'doors' => 2,
            ),
            397 => 
            array (
                'id_reg' => 2898,
                'id_regmf' => 2286,
                'doors' => 5,
            ),
            398 => 
            array (
                'id_reg' => 2899,
                'id_regmf' => 2287,
                'doors' => 0,
            ),
            399 => 
            array (
                'id_reg' => 2900,
                'id_regmf' => 2288,
                'doors' => 0,
            ),
            400 => 
            array (
                'id_reg' => 2901,
                'id_regmf' => 2289,
                'doors' => 0,
            ),
            401 => 
            array (
                'id_reg' => 2902,
                'id_regmf' => 2289,
                'doors' => 2,
            ),
            402 => 
            array (
                'id_reg' => 2903,
                'id_regmf' => 2290,
                'doors' => 0,
            ),
            403 => 
            array (
                'id_reg' => 2904,
                'id_regmf' => 2290,
                'doors' => 2,
            ),
            404 => 
            array (
                'id_reg' => 2905,
                'id_regmf' => 2290,
                'doors' => 5,
            ),
            405 => 
            array (
                'id_reg' => 2906,
                'id_regmf' => 2292,
                'doors' => 0,
            ),
            406 => 
            array (
                'id_reg' => 2907,
                'id_regmf' => 2292,
                'doors' => 5,
            ),
            407 => 
            array (
                'id_reg' => 2908,
                'id_regmf' => 2291,
                'doors' => 5,
            ),
            408 => 
            array (
                'id_reg' => 2909,
                'id_regmf' => 2293,
                'doors' => 0,
            ),
            409 => 
            array (
                'id_reg' => 2910,
                'id_regmf' => 2293,
                'doors' => 2,
            ),
            410 => 
            array (
                'id_reg' => 2911,
                'id_regmf' => 2293,
                'doors' => 5,
            ),
            411 => 
            array (
                'id_reg' => 2912,
                'id_regmf' => 2294,
                'doors' => 2,
            ),
            412 => 
            array (
                'id_reg' => 2913,
                'id_regmf' => 2294,
                'doors' => 5,
            ),
            413 => 
            array (
                'id_reg' => 2914,
                'id_regmf' => 2295,
                'doors' => 5,
            ),
            414 => 
            array (
                'id_reg' => 2915,
                'id_regmf' => 2296,
                'doors' => 5,
            ),
            415 => 
            array (
                'id_reg' => 2916,
                'id_regmf' => 2297,
                'doors' => 3,
            ),
            416 => 
            array (
                'id_reg' => 2917,
                'id_regmf' => 2298,
                'doors' => 2,
            ),
            417 => 
            array (
                'id_reg' => 2918,
                'id_regmf' => 2298,
                'doors' => 4,
            ),
            418 => 
            array (
                'id_reg' => 2919,
                'id_regmf' => 2298,
                'doors' => 5,
            ),
            419 => 
            array (
                'id_reg' => 2920,
                'id_regmf' => 2298,
                'doors' => 6,
            ),
            420 => 
            array (
                'id_reg' => 2921,
                'id_regmf' => 2300,
                'doors' => 5,
            ),
            421 => 
            array (
                'id_reg' => 2922,
                'id_regmf' => 2299,
                'doors' => 5,
            ),
            422 => 
            array (
                'id_reg' => 2923,
                'id_regmf' => 2301,
                'doors' => 2,
            ),
            423 => 
            array (
                'id_reg' => 2924,
                'id_regmf' => 2302,
                'doors' => 4,
            ),
            424 => 
            array (
                'id_reg' => 2925,
                'id_regmf' => 2303,
                'doors' => 4,
            ),
            425 => 
            array (
                'id_reg' => 2926,
                'id_regmf' => 2304,
                'doors' => 4,
            ),
            426 => 
            array (
                'id_reg' => 2927,
                'id_regmf' => 2305,
                'doors' => 4,
            ),
            427 => 
            array (
                'id_reg' => 2928,
                'id_regmf' => 2306,
                'doors' => 4,
            ),
            428 => 
            array (
                'id_reg' => 2929,
                'id_regmf' => 2307,
                'doors' => 4,
            ),
            429 => 
            array (
                'id_reg' => 2930,
                'id_regmf' => 2308,
                'doors' => 4,
            ),
            430 => 
            array (
                'id_reg' => 2931,
                'id_regmf' => 2309,
                'doors' => 4,
            ),
            431 => 
            array (
                'id_reg' => 2932,
                'id_regmf' => 2310,
                'doors' => 4,
            ),
            432 => 
            array (
                'id_reg' => 2933,
                'id_regmf' => 2311,
                'doors' => 4,
            ),
            433 => 
            array (
                'id_reg' => 2934,
                'id_regmf' => 2312,
                'doors' => 2,
            ),
            434 => 
            array (
                'id_reg' => 2935,
                'id_regmf' => 2312,
                'doors' => 4,
            ),
            435 => 
            array (
                'id_reg' => 2936,
                'id_regmf' => 2313,
                'doors' => 2,
            ),
            436 => 
            array (
                'id_reg' => 2937,
                'id_regmf' => 2313,
                'doors' => 4,
            ),
            437 => 
            array (
                'id_reg' => 2938,
                'id_regmf' => 2314,
                'doors' => 2,
            ),
            438 => 
            array (
                'id_reg' => 2939,
                'id_regmf' => 2314,
                'doors' => 4,
            ),
            439 => 
            array (
                'id_reg' => 2940,
                'id_regmf' => 2315,
                'doors' => 2,
            ),
            440 => 
            array (
                'id_reg' => 2941,
                'id_regmf' => 2316,
                'doors' => 2,
            ),
            441 => 
            array (
                'id_reg' => 2942,
                'id_regmf' => 2317,
                'doors' => 2,
            ),
            442 => 
            array (
                'id_reg' => 2943,
                'id_regmf' => 2318,
                'doors' => 2,
            ),
            443 => 
            array (
                'id_reg' => 2944,
                'id_regmf' => 2319,
                'doors' => 2,
            ),
            444 => 
            array (
                'id_reg' => 2945,
                'id_regmf' => 2320,
                'doors' => 2,
            ),
            445 => 
            array (
                'id_reg' => 2946,
                'id_regmf' => 2321,
                'doors' => 2,
            ),
            446 => 
            array (
                'id_reg' => 2947,
                'id_regmf' => 2322,
                'doors' => 2,
            ),
            447 => 
            array (
                'id_reg' => 2948,
                'id_regmf' => 2323,
                'doors' => 2,
            ),
            448 => 
            array (
                'id_reg' => 2949,
                'id_regmf' => 2324,
                'doors' => 2,
            ),
            449 => 
            array (
                'id_reg' => 2950,
                'id_regmf' => 2325,
                'doors' => 2,
            ),
            450 => 
            array (
                'id_reg' => 2951,
                'id_regmf' => 2326,
                'doors' => 2,
            ),
            451 => 
            array (
                'id_reg' => 2952,
                'id_regmf' => 2327,
                'doors' => 2,
            ),
            452 => 
            array (
                'id_reg' => 2953,
                'id_regmf' => 2329,
                'doors' => 2,
            ),
            453 => 
            array (
                'id_reg' => 2954,
                'id_regmf' => 2328,
                'doors' => 2,
            ),
            454 => 
            array (
                'id_reg' => 2955,
                'id_regmf' => 2330,
                'doors' => 2,
            ),
            455 => 
            array (
                'id_reg' => 2956,
                'id_regmf' => 2331,
                'doors' => 2,
            ),
            456 => 
            array (
                'id_reg' => 2957,
                'id_regmf' => 2332,
                'doors' => 2,
            ),
            457 => 
            array (
                'id_reg' => 2958,
                'id_regmf' => 2333,
                'doors' => 2,
            ),
            458 => 
            array (
                'id_reg' => 2959,
                'id_regmf' => 2334,
                'doors' => 2,
            ),
            459 => 
            array (
                'id_reg' => 2960,
                'id_regmf' => 2334,
                'doors' => 3,
            ),
            460 => 
            array (
                'id_reg' => 2961,
                'id_regmf' => 2334,
                'doors' => 4,
            ),
            461 => 
            array (
                'id_reg' => 2962,
                'id_regmf' => 2334,
                'doors' => 5,
            ),
            462 => 
            array (
                'id_reg' => 2963,
                'id_regmf' => 2335,
                'doors' => 2,
            ),
            463 => 
            array (
                'id_reg' => 2964,
                'id_regmf' => 2335,
                'doors' => 4,
            ),
            464 => 
            array (
                'id_reg' => 2965,
                'id_regmf' => 2335,
                'doors' => 5,
            ),
            465 => 
            array (
                'id_reg' => 2966,
                'id_regmf' => 2336,
                'doors' => 2,
            ),
            466 => 
            array (
                'id_reg' => 2967,
                'id_regmf' => 2336,
                'doors' => 4,
            ),
            467 => 
            array (
                'id_reg' => 2968,
                'id_regmf' => 2336,
                'doors' => 5,
            ),
            468 => 
            array (
                'id_reg' => 2969,
                'id_regmf' => 2338,
                'doors' => 2,
            ),
            469 => 
            array (
                'id_reg' => 2970,
                'id_regmf' => 2338,
                'doors' => 4,
            ),
            470 => 
            array (
                'id_reg' => 2971,
                'id_regmf' => 2338,
                'doors' => 5,
            ),
            471 => 
            array (
                'id_reg' => 2972,
                'id_regmf' => 2337,
                'doors' => 2,
            ),
            472 => 
            array (
                'id_reg' => 2973,
                'id_regmf' => 2337,
                'doors' => 4,
            ),
            473 => 
            array (
                'id_reg' => 2974,
                'id_regmf' => 2337,
                'doors' => 5,
            ),
            474 => 
            array (
                'id_reg' => 2975,
                'id_regmf' => 2339,
                'doors' => 0,
            ),
            475 => 
            array (
                'id_reg' => 2976,
                'id_regmf' => 2340,
                'doors' => 0,
            ),
            476 => 
            array (
                'id_reg' => 2977,
                'id_regmf' => 2340,
                'doors' => 2,
            ),
            477 => 
            array (
                'id_reg' => 2978,
                'id_regmf' => 2341,
                'doors' => 2,
            ),
            478 => 
            array (
                'id_reg' => 2979,
                'id_regmf' => 2341,
                'doors' => 5,
            ),
            479 => 
            array (
                'id_reg' => 2980,
                'id_regmf' => 2342,
                'doors' => 2,
            ),
            480 => 
            array (
                'id_reg' => 2981,
                'id_regmf' => 2342,
                'doors' => 5,
            ),
            481 => 
            array (
                'id_reg' => 2982,
                'id_regmf' => 2343,
                'doors' => 0,
            ),
            482 => 
            array (
                'id_reg' => 2983,
                'id_regmf' => 2343,
                'doors' => 2,
            ),
            483 => 
            array (
                'id_reg' => 2984,
                'id_regmf' => 2344,
                'doors' => 2,
            ),
            484 => 
            array (
                'id_reg' => 2985,
                'id_regmf' => 2344,
                'doors' => 5,
            ),
            485 => 
            array (
                'id_reg' => 2986,
                'id_regmf' => 2345,
                'doors' => 2,
            ),
            486 => 
            array (
                'id_reg' => 2987,
                'id_regmf' => 2347,
                'doors' => 2,
            ),
            487 => 
            array (
                'id_reg' => 2988,
                'id_regmf' => 2346,
                'doors' => 2,
            ),
            488 => 
            array (
                'id_reg' => 2989,
                'id_regmf' => 2349,
                'doors' => 4,
            ),
            489 => 
            array (
                'id_reg' => 2990,
                'id_regmf' => 2348,
                'doors' => 4,
            ),
            490 => 
            array (
                'id_reg' => 2991,
                'id_regmf' => 2348,
                'doors' => 5,
            ),
            491 => 
            array (
                'id_reg' => 2992,
                'id_regmf' => 2351,
                'doors' => 4,
            ),
            492 => 
            array (
                'id_reg' => 2993,
                'id_regmf' => 2351,
                'doors' => 5,
            ),
            493 => 
            array (
                'id_reg' => 2994,
                'id_regmf' => 2350,
                'doors' => 4,
            ),
            494 => 
            array (
                'id_reg' => 2995,
                'id_regmf' => 2350,
                'doors' => 5,
            ),
            495 => 
            array (
                'id_reg' => 2996,
                'id_regmf' => 2352,
                'doors' => 2,
            ),
            496 => 
            array (
                'id_reg' => 2997,
                'id_regmf' => 2353,
                'doors' => 4,
            ),
            497 => 
            array (
                'id_reg' => 2998,
                'id_regmf' => 2353,
                'doors' => 5,
            ),
            498 => 
            array (
                'id_reg' => 2999,
                'id_regmf' => 2354,
                'doors' => 4,
            ),
            499 => 
            array (
                'id_reg' => 3000,
                'id_regmf' => 2354,
                'doors' => 5,
            ),
        ));
        \DB::table('model_fuel_numdoors')->insert(array (
            0 => 
            array (
                'id_reg' => 3001,
                'id_regmf' => 2355,
                'doors' => 5,
            ),
            1 => 
            array (
                'id_reg' => 3002,
                'id_regmf' => 2356,
                'doors' => 2,
            ),
            2 => 
            array (
                'id_reg' => 3003,
                'id_regmf' => 2356,
                'doors' => 3,
            ),
            3 => 
            array (
                'id_reg' => 3004,
                'id_regmf' => 2357,
                'doors' => 4,
            ),
            4 => 
            array (
                'id_reg' => 3005,
                'id_regmf' => 2358,
                'doors' => 2,
            ),
            5 => 
            array (
                'id_reg' => 3006,
                'id_regmf' => 2359,
                'doors' => 4,
            ),
            6 => 
            array (
                'id_reg' => 3007,
                'id_regmf' => 2360,
                'doors' => 4,
            ),
            7 => 
            array (
                'id_reg' => 3008,
                'id_regmf' => 2361,
                'doors' => 5,
            ),
            8 => 
            array (
                'id_reg' => 3009,
                'id_regmf' => 2362,
                'doors' => 4,
            ),
            9 => 
            array (
                'id_reg' => 3010,
                'id_regmf' => 2363,
                'doors' => 4,
            ),
            10 => 
            array (
                'id_reg' => 3011,
                'id_regmf' => 2364,
                'doors' => 4,
            ),
            11 => 
            array (
                'id_reg' => 3012,
                'id_regmf' => 2365,
                'doors' => 2,
            ),
            12 => 
            array (
                'id_reg' => 3013,
                'id_regmf' => 2366,
                'doors' => 5,
            ),
            13 => 
            array (
                'id_reg' => 3014,
                'id_regmf' => 2367,
                'doors' => 4,
            ),
            14 => 
            array (
                'id_reg' => 3015,
                'id_regmf' => 2368,
                'doors' => 4,
            ),
            15 => 
            array (
                'id_reg' => 3016,
                'id_regmf' => 2369,
                'doors' => 2,
            ),
            16 => 
            array (
                'id_reg' => 3017,
                'id_regmf' => 2369,
                'doors' => 4,
            ),
            17 => 
            array (
                'id_reg' => 3018,
                'id_regmf' => 2369,
                'doors' => 5,
            ),
            18 => 
            array (
                'id_reg' => 3019,
                'id_regmf' => 2370,
                'doors' => 4,
            ),
            19 => 
            array (
                'id_reg' => 3020,
                'id_regmf' => 2371,
                'doors' => 4,
            ),
            20 => 
            array (
                'id_reg' => 3021,
                'id_regmf' => 2372,
                'doors' => 2,
            ),
            21 => 
            array (
                'id_reg' => 3022,
                'id_regmf' => 2372,
                'doors' => 4,
            ),
            22 => 
            array (
                'id_reg' => 3023,
                'id_regmf' => 2372,
                'doors' => 5,
            ),
            23 => 
            array (
                'id_reg' => 3024,
                'id_regmf' => 2373,
                'doors' => 4,
            ),
            24 => 
            array (
                'id_reg' => 3025,
                'id_regmf' => 2374,
                'doors' => 2,
            ),
            25 => 
            array (
                'id_reg' => 3026,
                'id_regmf' => 2375,
                'doors' => 2,
            ),
            26 => 
            array (
                'id_reg' => 3027,
                'id_regmf' => 2377,
                'doors' => 2,
            ),
            27 => 
            array (
                'id_reg' => 3028,
                'id_regmf' => 2376,
                'doors' => 2,
            ),
            28 => 
            array (
                'id_reg' => 3029,
                'id_regmf' => 2379,
                'doors' => 2,
            ),
            29 => 
            array (
                'id_reg' => 3030,
                'id_regmf' => 2378,
                'doors' => 2,
            ),
            30 => 
            array (
                'id_reg' => 3031,
                'id_regmf' => 2381,
                'doors' => 2,
            ),
            31 => 
            array (
                'id_reg' => 3032,
                'id_regmf' => 2380,
                'doors' => 2,
            ),
            32 => 
            array (
                'id_reg' => 3033,
                'id_regmf' => 2382,
                'doors' => 2,
            ),
            33 => 
            array (
                'id_reg' => 3034,
                'id_regmf' => 2382,
                'doors' => 4,
            ),
            34 => 
            array (
                'id_reg' => 3035,
                'id_regmf' => 2383,
                'doors' => 4,
            ),
            35 => 
            array (
                'id_reg' => 3036,
                'id_regmf' => 2384,
                'doors' => 2,
            ),
            36 => 
            array (
                'id_reg' => 3037,
                'id_regmf' => 2385,
                'doors' => 3,
            ),
            37 => 
            array (
                'id_reg' => 3038,
                'id_regmf' => 2386,
                'doors' => 3,
            ),
            38 => 
            array (
                'id_reg' => 3039,
                'id_regmf' => 2387,
                'doors' => 2,
            ),
            39 => 
            array (
                'id_reg' => 3040,
                'id_regmf' => 2388,
                'doors' => 2,
            ),
            40 => 
            array (
                'id_reg' => 3041,
                'id_regmf' => 2388,
                'doors' => 3,
            ),
            41 => 
            array (
                'id_reg' => 3042,
                'id_regmf' => 2389,
                'doors' => 2,
            ),
            42 => 
            array (
                'id_reg' => 3043,
                'id_regmf' => 2390,
                'doors' => 2,
            ),
            43 => 
            array (
                'id_reg' => 3044,
                'id_regmf' => 2391,
                'doors' => 2,
            ),
            44 => 
            array (
                'id_reg' => 3045,
                'id_regmf' => 2392,
                'doors' => 2,
            ),
            45 => 
            array (
                'id_reg' => 3046,
                'id_regmf' => 2393,
                'doors' => 2,
            ),
            46 => 
            array (
                'id_reg' => 3047,
                'id_regmf' => 2394,
                'doors' => 3,
            ),
            47 => 
            array (
                'id_reg' => 3048,
                'id_regmf' => 2395,
                'doors' => 3,
            ),
            48 => 
            array (
                'id_reg' => 3049,
                'id_regmf' => 2396,
                'doors' => 2,
            ),
            49 => 
            array (
                'id_reg' => 3050,
                'id_regmf' => 2397,
                'doors' => 2,
            ),
            50 => 
            array (
                'id_reg' => 3051,
                'id_regmf' => 2398,
                'doors' => 3,
            ),
            51 => 
            array (
                'id_reg' => 3052,
                'id_regmf' => 2399,
                'doors' => 2,
            ),
            52 => 
            array (
                'id_reg' => 3053,
                'id_regmf' => 2400,
                'doors' => 3,
            ),
            53 => 
            array (
                'id_reg' => 3054,
                'id_regmf' => 2401,
                'doors' => 3,
            ),
            54 => 
            array (
                'id_reg' => 3055,
                'id_regmf' => 2402,
                'doors' => 2,
            ),
            55 => 
            array (
                'id_reg' => 3056,
                'id_regmf' => 2403,
                'doors' => 4,
            ),
            56 => 
            array (
                'id_reg' => 3057,
                'id_regmf' => 2404,
                'doors' => 3,
            ),
            57 => 
            array (
                'id_reg' => 3058,
                'id_regmf' => 2405,
                'doors' => 2,
            ),
            58 => 
            array (
                'id_reg' => 3059,
                'id_regmf' => 2406,
                'doors' => 2,
            ),
            59 => 
            array (
                'id_reg' => 3060,
                'id_regmf' => 2407,
                'doors' => 2,
            ),
            60 => 
            array (
                'id_reg' => 3061,
                'id_regmf' => 2408,
                'doors' => 2,
            ),
            61 => 
            array (
                'id_reg' => 3062,
                'id_regmf' => 2409,
                'doors' => 2,
            ),
            62 => 
            array (
                'id_reg' => 3063,
                'id_regmf' => 2410,
                'doors' => 0,
            ),
            63 => 
            array (
                'id_reg' => 3064,
                'id_regmf' => 2410,
                'doors' => 3,
            ),
            64 => 
            array (
                'id_reg' => 3065,
                'id_regmf' => 2411,
                'doors' => 2,
            ),
            65 => 
            array (
                'id_reg' => 3066,
                'id_regmf' => 2412,
                'doors' => 2,
            ),
            66 => 
            array (
                'id_reg' => 3067,
                'id_regmf' => 2413,
                'doors' => 2,
            ),
            67 => 
            array (
                'id_reg' => 3068,
                'id_regmf' => 2414,
                'doors' => 2,
            ),
            68 => 
            array (
                'id_reg' => 3069,
                'id_regmf' => 2415,
                'doors' => 3,
            ),
            69 => 
            array (
                'id_reg' => 3070,
                'id_regmf' => 2416,
                'doors' => 3,
            ),
            70 => 
            array (
                'id_reg' => 3071,
                'id_regmf' => 2417,
                'doors' => 3,
            ),
            71 => 
            array (
                'id_reg' => 3072,
                'id_regmf' => 2418,
                'doors' => 3,
            ),
            72 => 
            array (
                'id_reg' => 3073,
                'id_regmf' => 2419,
                'doors' => 3,
            ),
            73 => 
            array (
                'id_reg' => 3074,
                'id_regmf' => 2420,
                'doors' => 0,
            ),
            74 => 
            array (
                'id_reg' => 3075,
                'id_regmf' => 2421,
                'doors' => 3,
            ),
            75 => 
            array (
                'id_reg' => 3076,
                'id_regmf' => 2422,
                'doors' => 0,
            ),
            76 => 
            array (
                'id_reg' => 3077,
                'id_regmf' => 2423,
                'doors' => 5,
            ),
            77 => 
            array (
                'id_reg' => 3078,
                'id_regmf' => 2424,
                'doors' => 5,
            ),
            78 => 
            array (
                'id_reg' => 3079,
                'id_regmf' => 2425,
                'doors' => 5,
            ),
            79 => 
            array (
                'id_reg' => 3080,
                'id_regmf' => 2426,
                'doors' => 2,
            ),
            80 => 
            array (
                'id_reg' => 3081,
                'id_regmf' => 2426,
                'doors' => 3,
            ),
            81 => 
            array (
                'id_reg' => 3082,
                'id_regmf' => 2426,
                'doors' => 4,
            ),
            82 => 
            array (
                'id_reg' => 3083,
                'id_regmf' => 2426,
                'doors' => 5,
            ),
            83 => 
            array (
                'id_reg' => 3084,
                'id_regmf' => 2426,
                'doors' => 6,
            ),
            84 => 
            array (
                'id_reg' => 3085,
                'id_regmf' => 2427,
                'doors' => 2,
            ),
            85 => 
            array (
                'id_reg' => 3086,
                'id_regmf' => 2427,
                'doors' => 3,
            ),
            86 => 
            array (
                'id_reg' => 3087,
                'id_regmf' => 2427,
                'doors' => 4,
            ),
            87 => 
            array (
                'id_reg' => 3088,
                'id_regmf' => 2427,
                'doors' => 5,
            ),
            88 => 
            array (
                'id_reg' => 3089,
                'id_regmf' => 2427,
                'doors' => 6,
            ),
            89 => 
            array (
                'id_reg' => 3090,
                'id_regmf' => 2429,
                'doors' => 3,
            ),
            90 => 
            array (
                'id_reg' => 3091,
                'id_regmf' => 2428,
                'doors' => 3,
            ),
            91 => 
            array (
                'id_reg' => 3092,
                'id_regmf' => 2430,
                'doors' => 0,
            ),
            92 => 
            array (
                'id_reg' => 3093,
                'id_regmf' => 2431,
                'doors' => 2,
            ),
            93 => 
            array (
                'id_reg' => 3094,
                'id_regmf' => 2431,
                'doors' => 3,
            ),
            94 => 
            array (
                'id_reg' => 3095,
                'id_regmf' => 2432,
                'doors' => 4,
            ),
            95 => 
            array (
                'id_reg' => 3096,
                'id_regmf' => 2433,
                'doors' => 5,
            ),
            96 => 
            array (
                'id_reg' => 3097,
                'id_regmf' => 2434,
                'doors' => 5,
            ),
            97 => 
            array (
                'id_reg' => 3098,
                'id_regmf' => 2435,
                'doors' => 2,
            ),
            98 => 
            array (
                'id_reg' => 3099,
                'id_regmf' => 2435,
                'doors' => 4,
            ),
            99 => 
            array (
                'id_reg' => 3100,
                'id_regmf' => 2436,
                'doors' => 2,
            ),
            100 => 
            array (
                'id_reg' => 3101,
                'id_regmf' => 2436,
                'doors' => 4,
            ),
            101 => 
            array (
                'id_reg' => 3102,
                'id_regmf' => 2437,
                'doors' => 2,
            ),
            102 => 
            array (
                'id_reg' => 3103,
                'id_regmf' => 2439,
                'doors' => 4,
            ),
            103 => 
            array (
                'id_reg' => 3104,
                'id_regmf' => 2439,
                'doors' => 5,
            ),
            104 => 
            array (
                'id_reg' => 3105,
                'id_regmf' => 2438,
                'doors' => 4,
            ),
            105 => 
            array (
                'id_reg' => 3106,
                'id_regmf' => 2438,
                'doors' => 5,
            ),
            106 => 
            array (
                'id_reg' => 3107,
                'id_regmf' => 2440,
                'doors' => 2,
            ),
            107 => 
            array (
                'id_reg' => 3108,
                'id_regmf' => 2442,
                'doors' => 2,
            ),
            108 => 
            array (
                'id_reg' => 3109,
                'id_regmf' => 2442,
                'doors' => 3,
            ),
            109 => 
            array (
                'id_reg' => 3110,
                'id_regmf' => 2442,
                'doors' => 5,
            ),
            110 => 
            array (
                'id_reg' => 3111,
                'id_regmf' => 2441,
                'doors' => 3,
            ),
            111 => 
            array (
                'id_reg' => 3112,
                'id_regmf' => 2441,
                'doors' => 5,
            ),
            112 => 
            array (
                'id_reg' => 3113,
                'id_regmf' => 2443,
                'doors' => 3,
            ),
            113 => 
            array (
                'id_reg' => 3114,
                'id_regmf' => 2443,
                'doors' => 4,
            ),
            114 => 
            array (
                'id_reg' => 3115,
                'id_regmf' => 2445,
                'doors' => 5,
            ),
            115 => 
            array (
                'id_reg' => 3116,
                'id_regmf' => 2444,
                'doors' => 5,
            ),
            116 => 
            array (
                'id_reg' => 3117,
                'id_regmf' => 2446,
                'doors' => 5,
            ),
            117 => 
            array (
                'id_reg' => 3118,
                'id_regmf' => 2447,
                'doors' => 4,
            ),
            118 => 
            array (
                'id_reg' => 3119,
                'id_regmf' => 2448,
                'doors' => 2,
            ),
            119 => 
            array (
                'id_reg' => 3120,
                'id_regmf' => 2449,
                'doors' => 3,
            ),
            120 => 
            array (
                'id_reg' => 3121,
                'id_regmf' => 2450,
                'doors' => 2,
            ),
            121 => 
            array (
                'id_reg' => 3122,
                'id_regmf' => 2450,
                'doors' => 3,
            ),
            122 => 
            array (
                'id_reg' => 3123,
                'id_regmf' => 2451,
                'doors' => 5,
            ),
            123 => 
            array (
                'id_reg' => 3124,
                'id_regmf' => 2452,
                'doors' => 4,
            ),
            124 => 
            array (
                'id_reg' => 3125,
                'id_regmf' => 2453,
                'doors' => 2,
            ),
            125 => 
            array (
                'id_reg' => 3126,
                'id_regmf' => 2454,
                'doors' => 4,
            ),
            126 => 
            array (
                'id_reg' => 3127,
                'id_regmf' => 2455,
                'doors' => 2,
            ),
            127 => 
            array (
                'id_reg' => 3128,
                'id_regmf' => 2455,
                'doors' => 4,
            ),
            128 => 
            array (
                'id_reg' => 3129,
                'id_regmf' => 2455,
                'doors' => 5,
            ),
            129 => 
            array (
                'id_reg' => 3130,
                'id_regmf' => 2456,
                'doors' => 4,
            ),
            130 => 
            array (
                'id_reg' => 3131,
                'id_regmf' => 2456,
                'doors' => 5,
            ),
            131 => 
            array (
                'id_reg' => 3132,
                'id_regmf' => 2458,
                'doors' => 5,
            ),
            132 => 
            array (
                'id_reg' => 3133,
                'id_regmf' => 2457,
                'doors' => 5,
            ),
            133 => 
            array (
                'id_reg' => 3134,
                'id_regmf' => 2459,
                'doors' => 3,
            ),
            134 => 
            array (
                'id_reg' => 3135,
                'id_regmf' => 2460,
                'doors' => 5,
            ),
            135 => 
            array (
                'id_reg' => 3136,
                'id_regmf' => 2461,
                'doors' => 5,
            ),
            136 => 
            array (
                'id_reg' => 3137,
                'id_regmf' => 2462,
                'doors' => 0,
            ),
            137 => 
            array (
                'id_reg' => 3138,
                'id_regmf' => 2463,
                'doors' => 2,
            ),
            138 => 
            array (
                'id_reg' => 3139,
                'id_regmf' => 2463,
                'doors' => 3,
            ),
            139 => 
            array (
                'id_reg' => 3140,
                'id_regmf' => 2463,
                'doors' => 4,
            ),
            140 => 
            array (
                'id_reg' => 3141,
                'id_regmf' => 2464,
                'doors' => 2,
            ),
            141 => 
            array (
                'id_reg' => 3142,
                'id_regmf' => 2464,
                'doors' => 4,
            ),
            142 => 
            array (
                'id_reg' => 3143,
                'id_regmf' => 2464,
                'doors' => 5,
            ),
            143 => 
            array (
                'id_reg' => 3144,
                'id_regmf' => 2465,
                'doors' => 4,
            ),
            144 => 
            array (
                'id_reg' => 3145,
                'id_regmf' => 2465,
                'doors' => 5,
            ),
            145 => 
            array (
                'id_reg' => 3146,
                'id_regmf' => 2467,
                'doors' => 4,
            ),
            146 => 
            array (
                'id_reg' => 3147,
                'id_regmf' => 2466,
                'doors' => 5,
            ),
            147 => 
            array (
                'id_reg' => 3148,
                'id_regmf' => 2469,
                'doors' => 3,
            ),
            148 => 
            array (
                'id_reg' => 3149,
                'id_regmf' => 2469,
                'doors' => 4,
            ),
            149 => 
            array (
                'id_reg' => 3150,
                'id_regmf' => 2469,
                'doors' => 5,
            ),
            150 => 
            array (
                'id_reg' => 3151,
                'id_regmf' => 2468,
                'doors' => 4,
            ),
            151 => 
            array (
                'id_reg' => 3152,
                'id_regmf' => 2468,
                'doors' => 5,
            ),
            152 => 
            array (
                'id_reg' => 3153,
                'id_regmf' => 2470,
                'doors' => 3,
            ),
            153 => 
            array (
                'id_reg' => 3154,
                'id_regmf' => 2470,
                'doors' => 4,
            ),
            154 => 
            array (
                'id_reg' => 3155,
                'id_regmf' => 2471,
                'doors' => 2,
            ),
            155 => 
            array (
                'id_reg' => 3156,
                'id_regmf' => 2471,
                'doors' => 3,
            ),
            156 => 
            array (
                'id_reg' => 3157,
                'id_regmf' => 2471,
                'doors' => 5,
            ),
            157 => 
            array (
                'id_reg' => 3158,
                'id_regmf' => 2472,
                'doors' => 3,
            ),
            158 => 
            array (
                'id_reg' => 3159,
                'id_regmf' => 2472,
                'doors' => 5,
            ),
            159 => 
            array (
                'id_reg' => 3160,
                'id_regmf' => 2473,
                'doors' => 3,
            ),
            160 => 
            array (
                'id_reg' => 3161,
                'id_regmf' => 2473,
                'doors' => 5,
            ),
            161 => 
            array (
                'id_reg' => 3162,
                'id_regmf' => 2475,
                'doors' => 5,
            ),
            162 => 
            array (
                'id_reg' => 3163,
                'id_regmf' => 2474,
                'doors' => 3,
            ),
            163 => 
            array (
                'id_reg' => 3164,
                'id_regmf' => 2474,
                'doors' => 5,
            ),
            164 => 
            array (
                'id_reg' => 3165,
                'id_regmf' => 2476,
                'doors' => 5,
            ),
            165 => 
            array (
                'id_reg' => 3166,
                'id_regmf' => 2477,
                'doors' => 5,
            ),
            166 => 
            array (
                'id_reg' => 3167,
                'id_regmf' => 2479,
                'doors' => 5,
            ),
            167 => 
            array (
                'id_reg' => 3168,
                'id_regmf' => 2478,
                'doors' => 5,
            ),
            168 => 
            array (
                'id_reg' => 3169,
                'id_regmf' => 2480,
                'doors' => 3,
            ),
            169 => 
            array (
                'id_reg' => 3170,
                'id_regmf' => 2480,
                'doors' => 5,
            ),
            170 => 
            array (
                'id_reg' => 3171,
                'id_regmf' => 2481,
                'doors' => 3,
            ),
            171 => 
            array (
                'id_reg' => 3172,
                'id_regmf' => 2481,
                'doors' => 5,
            ),
            172 => 
            array (
                'id_reg' => 3173,
                'id_regmf' => 2482,
                'doors' => 3,
            ),
            173 => 
            array (
                'id_reg' => 3174,
                'id_regmf' => 2483,
                'doors' => 2,
            ),
            174 => 
            array (
                'id_reg' => 3175,
                'id_regmf' => 2484,
                'doors' => 2,
            ),
            175 => 
            array (
                'id_reg' => 3176,
                'id_regmf' => 2484,
                'doors' => 4,
            ),
            176 => 
            array (
                'id_reg' => 3177,
                'id_regmf' => 2486,
                'doors' => 5,
            ),
            177 => 
            array (
                'id_reg' => 3178,
                'id_regmf' => 2485,
                'doors' => 5,
            ),
            178 => 
            array (
                'id_reg' => 3179,
                'id_regmf' => 2487,
                'doors' => 3,
            ),
            179 => 
            array (
                'id_reg' => 3180,
                'id_regmf' => 2489,
                'doors' => 5,
            ),
            180 => 
            array (
                'id_reg' => 3181,
                'id_regmf' => 2488,
                'doors' => 5,
            ),
            181 => 
            array (
                'id_reg' => 3182,
                'id_regmf' => 2491,
                'doors' => 4,
            ),
            182 => 
            array (
                'id_reg' => 3183,
                'id_regmf' => 2491,
                'doors' => 5,
            ),
            183 => 
            array (
                'id_reg' => 3184,
                'id_regmf' => 2490,
                'doors' => 4,
            ),
            184 => 
            array (
                'id_reg' => 3185,
                'id_regmf' => 2493,
                'doors' => 4,
            ),
            185 => 
            array (
                'id_reg' => 3186,
                'id_regmf' => 2492,
                'doors' => 5,
            ),
            186 => 
            array (
                'id_reg' => 3187,
                'id_regmf' => 2495,
                'doors' => 4,
            ),
            187 => 
            array (
                'id_reg' => 3188,
                'id_regmf' => 2494,
                'doors' => 4,
            ),
            188 => 
            array (
                'id_reg' => 3189,
                'id_regmf' => 2497,
                'doors' => 5,
            ),
            189 => 
            array (
                'id_reg' => 3190,
                'id_regmf' => 2496,
                'doors' => 5,
            ),
            190 => 
            array (
                'id_reg' => 3191,
                'id_regmf' => 2499,
                'doors' => 5,
            ),
            191 => 
            array (
                'id_reg' => 3192,
                'id_regmf' => 2498,
                'doors' => 5,
            ),
            192 => 
            array (
                'id_reg' => 3193,
                'id_regmf' => 2500,
                'doors' => 2,
            ),
            193 => 
            array (
                'id_reg' => 3194,
                'id_regmf' => 2501,
                'doors' => 3,
            ),
            194 => 
            array (
                'id_reg' => 3195,
                'id_regmf' => 2502,
                'doors' => 4,
            ),
            195 => 
            array (
                'id_reg' => 3196,
                'id_regmf' => 2503,
                'doors' => 4,
            ),
            196 => 
            array (
                'id_reg' => 3197,
                'id_regmf' => 2504,
                'doors' => 3,
            ),
            197 => 
            array (
                'id_reg' => 3198,
                'id_regmf' => 2505,
                'doors' => 0,
            ),
            198 => 
            array (
                'id_reg' => 3199,
                'id_regmf' => 2506,
                'doors' => 2,
            ),
            199 => 
            array (
                'id_reg' => 3200,
                'id_regmf' => 2507,
                'doors' => 5,
            ),
            200 => 
            array (
                'id_reg' => 3201,
                'id_regmf' => 2508,
                'doors' => 4,
            ),
            201 => 
            array (
                'id_reg' => 3202,
                'id_regmf' => 2510,
                'doors' => 2,
            ),
            202 => 
            array (
                'id_reg' => 3203,
                'id_regmf' => 2509,
                'doors' => 0,
            ),
            203 => 
            array (
                'id_reg' => 3204,
                'id_regmf' => 2511,
                'doors' => 0,
            ),
            204 => 
            array (
                'id_reg' => 3205,
                'id_regmf' => 2512,
                'doors' => 2,
            ),
            205 => 
            array (
                'id_reg' => 3206,
                'id_regmf' => 2513,
                'doors' => 2,
            ),
            206 => 
            array (
                'id_reg' => 3207,
                'id_regmf' => 2514,
                'doors' => 2,
            ),
            207 => 
            array (
                'id_reg' => 3208,
                'id_regmf' => 2515,
                'doors' => 2,
            ),
            208 => 
            array (
                'id_reg' => 3209,
                'id_regmf' => 2516,
                'doors' => 2,
            ),
            209 => 
            array (
                'id_reg' => 3210,
                'id_regmf' => 2517,
                'doors' => 4,
            ),
            210 => 
            array (
                'id_reg' => 3211,
                'id_regmf' => 2518,
                'doors' => 4,
            ),
            211 => 
            array (
                'id_reg' => 3212,
                'id_regmf' => 2519,
                'doors' => 4,
            ),
            212 => 
            array (
                'id_reg' => 3213,
                'id_regmf' => 2520,
                'doors' => 2,
            ),
            213 => 
            array (
                'id_reg' => 3214,
                'id_regmf' => 2521,
                'doors' => 4,
            ),
            214 => 
            array (
                'id_reg' => 3215,
                'id_regmf' => 2522,
                'doors' => 2,
            ),
            215 => 
            array (
                'id_reg' => 3216,
                'id_regmf' => 2523,
                'doors' => 2,
            ),
            216 => 
            array (
                'id_reg' => 3217,
                'id_regmf' => 2524,
                'doors' => 2,
            ),
            217 => 
            array (
                'id_reg' => 3218,
                'id_regmf' => 2524,
                'doors' => 3,
            ),
            218 => 
            array (
                'id_reg' => 3219,
                'id_regmf' => 2524,
                'doors' => 4,
            ),
            219 => 
            array (
                'id_reg' => 3220,
                'id_regmf' => 2525,
                'doors' => 4,
            ),
            220 => 
            array (
                'id_reg' => 3221,
                'id_regmf' => 2526,
                'doors' => 4,
            ),
            221 => 
            array (
                'id_reg' => 3222,
                'id_regmf' => 2527,
                'doors' => 5,
            ),
            222 => 
            array (
                'id_reg' => 3223,
                'id_regmf' => 2528,
                'doors' => 4,
            ),
            223 => 
            array (
                'id_reg' => 3224,
                'id_regmf' => 2529,
                'doors' => 0,
            ),
            224 => 
            array (
                'id_reg' => 3225,
                'id_regmf' => 2530,
                'doors' => 4,
            ),
            225 => 
            array (
                'id_reg' => 3226,
                'id_regmf' => 2531,
                'doors' => 2,
            ),
            226 => 
            array (
                'id_reg' => 3227,
                'id_regmf' => 2532,
                'doors' => 0,
            ),
            227 => 
            array (
                'id_reg' => 3228,
                'id_regmf' => 2533,
                'doors' => 2,
            ),
            228 => 
            array (
                'id_reg' => 3229,
                'id_regmf' => 2534,
                'doors' => 0,
            ),
            229 => 
            array (
                'id_reg' => 3230,
                'id_regmf' => 2535,
                'doors' => 0,
            ),
            230 => 
            array (
                'id_reg' => 3231,
                'id_regmf' => 2535,
                'doors' => 2,
            ),
            231 => 
            array (
                'id_reg' => 3232,
                'id_regmf' => 2536,
                'doors' => 0,
            ),
            232 => 
            array (
                'id_reg' => 3233,
                'id_regmf' => 2537,
                'doors' => 3,
            ),
            233 => 
            array (
                'id_reg' => 3234,
                'id_regmf' => 2538,
                'doors' => 0,
            ),
            234 => 
            array (
                'id_reg' => 3235,
                'id_regmf' => 2539,
                'doors' => 4,
            ),
            235 => 
            array (
                'id_reg' => 3236,
                'id_regmf' => 2540,
                'doors' => 2,
            ),
            236 => 
            array (
                'id_reg' => 3237,
                'id_regmf' => 2541,
                'doors' => 4,
            ),
            237 => 
            array (
                'id_reg' => 3238,
                'id_regmf' => 2542,
                'doors' => 0,
            ),
            238 => 
            array (
                'id_reg' => 3239,
                'id_regmf' => 2543,
                'doors' => 3,
            ),
            239 => 
            array (
                'id_reg' => 3240,
                'id_regmf' => 2544,
                'doors' => 2,
            ),
            240 => 
            array (
                'id_reg' => 3241,
                'id_regmf' => 2544,
                'doors' => 3,
            ),
            241 => 
            array (
                'id_reg' => 3242,
                'id_regmf' => 2545,
                'doors' => 2,
            ),
            242 => 
            array (
                'id_reg' => 3243,
                'id_regmf' => 2546,
                'doors' => 3,
            ),
            243 => 
            array (
                'id_reg' => 3244,
                'id_regmf' => 2547,
                'doors' => 3,
            ),
            244 => 
            array (
                'id_reg' => 3245,
                'id_regmf' => 2548,
                'doors' => 2,
            ),
            245 => 
            array (
                'id_reg' => 3246,
                'id_regmf' => 2548,
                'doors' => 3,
            ),
            246 => 
            array (
                'id_reg' => 3247,
                'id_regmf' => 2549,
                'doors' => 2,
            ),
            247 => 
            array (
                'id_reg' => 3248,
                'id_regmf' => 2549,
                'doors' => 3,
            ),
            248 => 
            array (
                'id_reg' => 3249,
                'id_regmf' => 2550,
                'doors' => 4,
            ),
            249 => 
            array (
                'id_reg' => 3250,
                'id_regmf' => 2551,
                'doors' => 4,
            ),
            250 => 
            array (
                'id_reg' => 3251,
                'id_regmf' => 2551,
                'doors' => 5,
            ),
            251 => 
            array (
                'id_reg' => 3252,
                'id_regmf' => 2552,
                'doors' => 5,
            ),
            252 => 
            array (
                'id_reg' => 3253,
                'id_regmf' => 2553,
                'doors' => 2,
            ),
            253 => 
            array (
                'id_reg' => 3254,
                'id_regmf' => 2554,
                'doors' => 4,
            ),
            254 => 
            array (
                'id_reg' => 3255,
                'id_regmf' => 2554,
                'doors' => 5,
            ),
            255 => 
            array (
                'id_reg' => 3256,
                'id_regmf' => 2555,
                'doors' => 4,
            ),
            256 => 
            array (
                'id_reg' => 3257,
                'id_regmf' => 2555,
                'doors' => 5,
            ),
            257 => 
            array (
                'id_reg' => 3258,
                'id_regmf' => 2556,
                'doors' => 2,
            ),
            258 => 
            array (
                'id_reg' => 3259,
                'id_regmf' => 2556,
                'doors' => 4,
            ),
            259 => 
            array (
                'id_reg' => 3260,
                'id_regmf' => 2557,
                'doors' => 2,
            ),
            260 => 
            array (
                'id_reg' => 3261,
                'id_regmf' => 2557,
                'doors' => 4,
            ),
            261 => 
            array (
                'id_reg' => 3262,
                'id_regmf' => 2558,
                'doors' => 4,
            ),
            262 => 
            array (
                'id_reg' => 3263,
                'id_regmf' => 2559,
                'doors' => 4,
            ),
            263 => 
            array (
                'id_reg' => 3264,
                'id_regmf' => 2560,
                'doors' => 4,
            ),
            264 => 
            array (
                'id_reg' => 3265,
                'id_regmf' => 2561,
                'doors' => 3,
            ),
            265 => 
            array (
                'id_reg' => 3266,
                'id_regmf' => 2561,
                'doors' => 4,
            ),
            266 => 
            array (
                'id_reg' => 3267,
                'id_regmf' => 2561,
                'doors' => 5,
            ),
            267 => 
            array (
                'id_reg' => 3268,
                'id_regmf' => 2562,
                'doors' => 5,
            ),
            268 => 
            array (
                'id_reg' => 3269,
                'id_regmf' => 2563,
                'doors' => 5,
            ),
            269 => 
            array (
                'id_reg' => 3270,
                'id_regmf' => 2564,
                'doors' => 5,
            ),
            270 => 
            array (
                'id_reg' => 3271,
                'id_regmf' => 2565,
                'doors' => 5,
            ),
            271 => 
            array (
                'id_reg' => 3272,
                'id_regmf' => 2566,
                'doors' => 5,
            ),
            272 => 
            array (
                'id_reg' => 3273,
                'id_regmf' => 2567,
                'doors' => 5,
            ),
            273 => 
            array (
                'id_reg' => 3274,
                'id_regmf' => 2568,
                'doors' => 4,
            ),
            274 => 
            array (
                'id_reg' => 3275,
                'id_regmf' => 2568,
                'doors' => 5,
            ),
            275 => 
            array (
                'id_reg' => 3276,
                'id_regmf' => 2568,
                'doors' => 6,
            ),
            276 => 
            array (
                'id_reg' => 3277,
                'id_regmf' => 2569,
                'doors' => 2,
            ),
            277 => 
            array (
                'id_reg' => 3278,
                'id_regmf' => 2570,
                'doors' => 5,
            ),
            278 => 
            array (
                'id_reg' => 3279,
                'id_regmf' => 2571,
                'doors' => 5,
            ),
            279 => 
            array (
                'id_reg' => 3280,
                'id_regmf' => 2572,
                'doors' => 5,
            ),
            280 => 
            array (
                'id_reg' => 3281,
                'id_regmf' => 2574,
                'doors' => 5,
            ),
            281 => 
            array (
                'id_reg' => 3282,
                'id_regmf' => 2573,
                'doors' => 5,
            ),
            282 => 
            array (
                'id_reg' => 3283,
                'id_regmf' => 2575,
                'doors' => 5,
            ),
            283 => 
            array (
                'id_reg' => 3284,
                'id_regmf' => 2576,
                'doors' => 0,
            ),
            284 => 
            array (
                'id_reg' => 3285,
                'id_regmf' => 2577,
                'doors' => 0,
            ),
            285 => 
            array (
                'id_reg' => 3286,
                'id_regmf' => 2577,
                'doors' => 2,
            ),
            286 => 
            array (
                'id_reg' => 3287,
                'id_regmf' => 2577,
                'doors' => 5,
            ),
            287 => 
            array (
                'id_reg' => 3288,
                'id_regmf' => 2578,
                'doors' => 0,
            ),
            288 => 
            array (
                'id_reg' => 3289,
                'id_regmf' => 2578,
                'doors' => 2,
            ),
            289 => 
            array (
                'id_reg' => 3290,
                'id_regmf' => 2578,
                'doors' => 5,
            ),
            290 => 
            array (
                'id_reg' => 3291,
                'id_regmf' => 2579,
                'doors' => 3,
            ),
            291 => 
            array (
                'id_reg' => 3292,
                'id_regmf' => 2580,
                'doors' => 2,
            ),
            292 => 
            array (
                'id_reg' => 3293,
                'id_regmf' => 2582,
                'doors' => 4,
            ),
            293 => 
            array (
                'id_reg' => 3294,
                'id_regmf' => 2581,
                'doors' => 4,
            ),
            294 => 
            array (
                'id_reg' => 3295,
                'id_regmf' => 2583,
                'doors' => 2,
            ),
            295 => 
            array (
                'id_reg' => 3296,
                'id_regmf' => 2584,
                'doors' => 4,
            ),
            296 => 
            array (
                'id_reg' => 3297,
                'id_regmf' => 2584,
                'doors' => 5,
            ),
            297 => 
            array (
                'id_reg' => 3298,
                'id_regmf' => 2585,
                'doors' => 5,
            ),
            298 => 
            array (
                'id_reg' => 3299,
                'id_regmf' => 2586,
                'doors' => 5,
            ),
            299 => 
            array (
                'id_reg' => 3300,
                'id_regmf' => 2587,
                'doors' => 5,
            ),
            300 => 
            array (
                'id_reg' => 3301,
                'id_regmf' => 2588,
                'doors' => 5,
            ),
            301 => 
            array (
                'id_reg' => 3302,
                'id_regmf' => 2589,
                'doors' => 2,
            ),
            302 => 
            array (
                'id_reg' => 3303,
                'id_regmf' => 2591,
                'doors' => 4,
            ),
            303 => 
            array (
                'id_reg' => 3304,
                'id_regmf' => 2591,
                'doors' => 5,
            ),
            304 => 
            array (
                'id_reg' => 3305,
                'id_regmf' => 2590,
                'doors' => 4,
            ),
            305 => 
            array (
                'id_reg' => 3306,
                'id_regmf' => 2590,
                'doors' => 5,
            ),
            306 => 
            array (
                'id_reg' => 3307,
                'id_regmf' => 2592,
                'doors' => 2,
            ),
            307 => 
            array (
                'id_reg' => 3308,
                'id_regmf' => 2593,
                'doors' => 2,
            ),
            308 => 
            array (
                'id_reg' => 3309,
                'id_regmf' => 2593,
                'doors' => 5,
            ),
            309 => 
            array (
                'id_reg' => 3310,
                'id_regmf' => 2594,
                'doors' => 2,
            ),
            310 => 
            array (
                'id_reg' => 3311,
                'id_regmf' => 2594,
                'doors' => 5,
            ),
            311 => 
            array (
                'id_reg' => 3312,
                'id_regmf' => 2595,
                'doors' => 4,
            ),
            312 => 
            array (
                'id_reg' => 3313,
                'id_regmf' => 2596,
                'doors' => 4,
            ),
            313 => 
            array (
                'id_reg' => 3314,
                'id_regmf' => 2597,
                'doors' => 4,
            ),
            314 => 
            array (
                'id_reg' => 3315,
                'id_regmf' => 2598,
                'doors' => 5,
            ),
            315 => 
            array (
                'id_reg' => 3316,
                'id_regmf' => 2599,
                'doors' => 4,
            ),
            316 => 
            array (
                'id_reg' => 3317,
                'id_regmf' => 2600,
                'doors' => 4,
            ),
            317 => 
            array (
                'id_reg' => 3318,
                'id_regmf' => 2601,
                'doors' => 5,
            ),
            318 => 
            array (
                'id_reg' => 3319,
                'id_regmf' => 2602,
                'doors' => 5,
            ),
            319 => 
            array (
                'id_reg' => 3320,
                'id_regmf' => 2603,
                'doors' => 2,
            ),
            320 => 
            array (
                'id_reg' => 3321,
                'id_regmf' => 2603,
                'doors' => 5,
            ),
            321 => 
            array (
                'id_reg' => 3322,
                'id_regmf' => 2604,
                'doors' => 2,
            ),
            322 => 
            array (
                'id_reg' => 3323,
                'id_regmf' => 2604,
                'doors' => 4,
            ),
            323 => 
            array (
                'id_reg' => 3324,
                'id_regmf' => 2605,
                'doors' => 2,
            ),
            324 => 
            array (
                'id_reg' => 3325,
                'id_regmf' => 2605,
                'doors' => 4,
            ),
            325 => 
            array (
                'id_reg' => 3326,
                'id_regmf' => 2606,
                'doors' => 2,
            ),
            326 => 
            array (
                'id_reg' => 3327,
                'id_regmf' => 2606,
                'doors' => 4,
            ),
            327 => 
            array (
                'id_reg' => 3328,
                'id_regmf' => 2607,
                'doors' => 2,
            ),
            328 => 
            array (
                'id_reg' => 3329,
                'id_regmf' => 2607,
                'doors' => 4,
            ),
            329 => 
            array (
                'id_reg' => 3330,
                'id_regmf' => 2608,
                'doors' => 2,
            ),
            330 => 
            array (
                'id_reg' => 3331,
                'id_regmf' => 2608,
                'doors' => 4,
            ),
            331 => 
            array (
                'id_reg' => 3332,
                'id_regmf' => 2609,
                'doors' => 2,
            ),
            332 => 
            array (
                'id_reg' => 3333,
                'id_regmf' => 2610,
                'doors' => 5,
            ),
            333 => 
            array (
                'id_reg' => 3334,
                'id_regmf' => 2610,
                'doors' => 6,
            ),
            334 => 
            array (
                'id_reg' => 3335,
                'id_regmf' => 2611,
                'doors' => 4,
            ),
            335 => 
            array (
                'id_reg' => 3336,
                'id_regmf' => 2611,
                'doors' => 5,
            ),
            336 => 
            array (
                'id_reg' => 3337,
                'id_regmf' => 2612,
                'doors' => 5,
            ),
            337 => 
            array (
                'id_reg' => 3338,
                'id_regmf' => 2612,
                'doors' => 6,
            ),
            338 => 
            array (
                'id_reg' => 3339,
                'id_regmf' => 2613,
                'doors' => 2,
            ),
            339 => 
            array (
                'id_reg' => 3340,
                'id_regmf' => 2613,
                'doors' => 4,
            ),
            340 => 
            array (
                'id_reg' => 3341,
                'id_regmf' => 2613,
                'doors' => 5,
            ),
            341 => 
            array (
                'id_reg' => 3342,
                'id_regmf' => 2615,
                'doors' => 3,
            ),
            342 => 
            array (
                'id_reg' => 3343,
                'id_regmf' => 2615,
                'doors' => 5,
            ),
            343 => 
            array (
                'id_reg' => 3344,
                'id_regmf' => 2614,
                'doors' => 5,
            ),
            344 => 
            array (
                'id_reg' => 3345,
                'id_regmf' => 2616,
                'doors' => 3,
            ),
            345 => 
            array (
                'id_reg' => 3346,
                'id_regmf' => 2616,
                'doors' => 5,
            ),
            346 => 
            array (
                'id_reg' => 3347,
                'id_regmf' => 2617,
                'doors' => 5,
            ),
            347 => 
            array (
                'id_reg' => 3348,
                'id_regmf' => 2619,
                'doors' => 2,
            ),
            348 => 
            array (
                'id_reg' => 3349,
                'id_regmf' => 2618,
                'doors' => 2,
            ),
            349 => 
            array (
                'id_reg' => 3350,
                'id_regmf' => 2618,
                'doors' => 4,
            ),
            350 => 
            array (
                'id_reg' => 3351,
                'id_regmf' => 2620,
                'doors' => 5,
            ),
            351 => 
            array (
                'id_reg' => 3352,
                'id_regmf' => 2621,
                'doors' => 5,
            ),
            352 => 
            array (
                'id_reg' => 3353,
                'id_regmf' => 2622,
                'doors' => 4,
            ),
            353 => 
            array (
                'id_reg' => 3354,
                'id_regmf' => 2622,
                'doors' => 5,
            ),
            354 => 
            array (
                'id_reg' => 3355,
                'id_regmf' => 2622,
                'doors' => 6,
            ),
            355 => 
            array (
                'id_reg' => 3356,
                'id_regmf' => 2624,
                'doors' => 3,
            ),
            356 => 
            array (
                'id_reg' => 3357,
                'id_regmf' => 2624,
                'doors' => 5,
            ),
            357 => 
            array (
                'id_reg' => 3358,
                'id_regmf' => 2623,
                'doors' => 3,
            ),
            358 => 
            array (
                'id_reg' => 3359,
                'id_regmf' => 2623,
                'doors' => 5,
            ),
            359 => 
            array (
                'id_reg' => 3360,
                'id_regmf' => 2625,
                'doors' => 4,
            ),
            360 => 
            array (
                'id_reg' => 3361,
                'id_regmf' => 2625,
                'doors' => 5,
            ),
            361 => 
            array (
                'id_reg' => 3362,
                'id_regmf' => 2626,
                'doors' => 5,
            ),
            362 => 
            array (
                'id_reg' => 3363,
                'id_regmf' => 2627,
                'doors' => 3,
            ),
            363 => 
            array (
                'id_reg' => 3364,
                'id_regmf' => 2628,
                'doors' => 2,
            ),
            364 => 
            array (
                'id_reg' => 3365,
                'id_regmf' => 2628,
                'doors' => 3,
            ),
            365 => 
            array (
                'id_reg' => 3366,
                'id_regmf' => 2630,
                'doors' => 4,
            ),
            366 => 
            array (
                'id_reg' => 3367,
                'id_regmf' => 2630,
                'doors' => 5,
            ),
            367 => 
            array (
                'id_reg' => 3368,
                'id_regmf' => 2629,
                'doors' => 4,
            ),
            368 => 
            array (
                'id_reg' => 3369,
                'id_regmf' => 2629,
                'doors' => 5,
            ),
            369 => 
            array (
                'id_reg' => 3370,
                'id_regmf' => 2631,
                'doors' => 2,
            ),
            370 => 
            array (
                'id_reg' => 3371,
                'id_regmf' => 2631,
                'doors' => 3,
            ),
            371 => 
            array (
                'id_reg' => 3372,
                'id_regmf' => 2632,
                'doors' => 2,
            ),
            372 => 
            array (
                'id_reg' => 3373,
                'id_regmf' => 2632,
                'doors' => 4,
            ),
            373 => 
            array (
                'id_reg' => 3374,
                'id_regmf' => 2633,
                'doors' => 4,
            ),
            374 => 
            array (
                'id_reg' => 3375,
                'id_regmf' => 2633,
                'doors' => 5,
            ),
            375 => 
            array (
                'id_reg' => 3376,
                'id_regmf' => 2634,
                'doors' => 3,
            ),
            376 => 
            array (
                'id_reg' => 3377,
                'id_regmf' => 2634,
                'doors' => 4,
            ),
            377 => 
            array (
                'id_reg' => 3378,
                'id_regmf' => 2634,
                'doors' => 5,
            ),
            378 => 
            array (
                'id_reg' => 3379,
                'id_regmf' => 2635,
                'doors' => 3,
            ),
            379 => 
            array (
                'id_reg' => 3380,
                'id_regmf' => 2635,
                'doors' => 4,
            ),
            380 => 
            array (
                'id_reg' => 3381,
                'id_regmf' => 2635,
                'doors' => 5,
            ),
            381 => 
            array (
                'id_reg' => 3382,
                'id_regmf' => 2636,
                'doors' => 4,
            ),
            382 => 
            array (
                'id_reg' => 3383,
                'id_regmf' => 2637,
                'doors' => 2,
            ),
            383 => 
            array (
                'id_reg' => 3384,
                'id_regmf' => 2637,
                'doors' => 3,
            ),
            384 => 
            array (
                'id_reg' => 3385,
                'id_regmf' => 2637,
                'doors' => 5,
            ),
            385 => 
            array (
                'id_reg' => 3386,
                'id_regmf' => 2638,
                'doors' => 3,
            ),
            386 => 
            array (
                'id_reg' => 3387,
                'id_regmf' => 2638,
                'doors' => 5,
            ),
            387 => 
            array (
                'id_reg' => 3388,
                'id_regmf' => 2640,
                'doors' => 4,
            ),
            388 => 
            array (
                'id_reg' => 3389,
                'id_regmf' => 2640,
                'doors' => 5,
            ),
            389 => 
            array (
                'id_reg' => 3390,
                'id_regmf' => 2639,
                'doors' => 5,
            ),
            390 => 
            array (
                'id_reg' => 3391,
                'id_regmf' => 2641,
                'doors' => 4,
            ),
            391 => 
            array (
                'id_reg' => 3392,
                'id_regmf' => 2642,
                'doors' => 2,
            ),
            392 => 
            array (
                'id_reg' => 3393,
                'id_regmf' => 2642,
                'doors' => 4,
            ),
            393 => 
            array (
                'id_reg' => 3394,
                'id_regmf' => 2642,
                'doors' => 5,
            ),
            394 => 
            array (
                'id_reg' => 3395,
                'id_regmf' => 2642,
                'doors' => 6,
            ),
            395 => 
            array (
                'id_reg' => 3396,
                'id_regmf' => 2643,
                'doors' => 2,
            ),
            396 => 
            array (
                'id_reg' => 3397,
                'id_regmf' => 2645,
                'doors' => 0,
            ),
            397 => 
            array (
                'id_reg' => 3398,
                'id_regmf' => 2645,
                'doors' => 5,
            ),
            398 => 
            array (
                'id_reg' => 3399,
                'id_regmf' => 2644,
                'doors' => 0,
            ),
            399 => 
            array (
                'id_reg' => 3400,
                'id_regmf' => 2644,
                'doors' => 5,
            ),
            400 => 
            array (
                'id_reg' => 3401,
                'id_regmf' => 2646,
                'doors' => 5,
            ),
            401 => 
            array (
                'id_reg' => 3402,
                'id_regmf' => 2647,
                'doors' => 5,
            ),
            402 => 
            array (
                'id_reg' => 3403,
                'id_regmf' => 2648,
                'doors' => 5,
            ),
            403 => 
            array (
                'id_reg' => 3404,
                'id_regmf' => 2650,
                'doors' => 5,
            ),
            404 => 
            array (
                'id_reg' => 3405,
                'id_regmf' => 2649,
                'doors' => 5,
            ),
            405 => 
            array (
                'id_reg' => 3406,
                'id_regmf' => 2651,
                'doors' => 2,
            ),
            406 => 
            array (
                'id_reg' => 3407,
                'id_regmf' => 2652,
                'doors' => 4,
            ),
            407 => 
            array (
                'id_reg' => 3408,
                'id_regmf' => 2653,
                'doors' => 2,
            ),
            408 => 
            array (
                'id_reg' => 3409,
                'id_regmf' => 2654,
                'doors' => 2,
            ),
            409 => 
            array (
                'id_reg' => 3410,
                'id_regmf' => 2655,
                'doors' => 5,
            ),
            410 => 
            array (
                'id_reg' => 3411,
                'id_regmf' => 2656,
                'doors' => 4,
            ),
            411 => 
            array (
                'id_reg' => 3412,
                'id_regmf' => 2657,
                'doors' => 0,
            ),
            412 => 
            array (
                'id_reg' => 3413,
                'id_regmf' => 2658,
                'doors' => 4,
            ),
            413 => 
            array (
                'id_reg' => 3414,
                'id_regmf' => 2659,
                'doors' => 4,
            ),
            414 => 
            array (
                'id_reg' => 3415,
                'id_regmf' => 2660,
                'doors' => 5,
            ),
            415 => 
            array (
                'id_reg' => 3416,
                'id_regmf' => 2661,
                'doors' => 2,
            ),
            416 => 
            array (
                'id_reg' => 3417,
                'id_regmf' => 2661,
                'doors' => 4,
            ),
            417 => 
            array (
                'id_reg' => 3418,
                'id_regmf' => 2662,
                'doors' => 2,
            ),
            418 => 
            array (
                'id_reg' => 3419,
                'id_regmf' => 2662,
                'doors' => 4,
            ),
            419 => 
            array (
                'id_reg' => 3420,
                'id_regmf' => 2662,
                'doors' => 5,
            ),
            420 => 
            array (
                'id_reg' => 3421,
                'id_regmf' => 2663,
                'doors' => 2,
            ),
            421 => 
            array (
                'id_reg' => 3422,
                'id_regmf' => 2663,
                'doors' => 4,
            ),
            422 => 
            array (
                'id_reg' => 3423,
                'id_regmf' => 2664,
                'doors' => 4,
            ),
            423 => 
            array (
                'id_reg' => 3424,
                'id_regmf' => 2665,
                'doors' => 2,
            ),
            424 => 
            array (
                'id_reg' => 3425,
                'id_regmf' => 2666,
                'doors' => 4,
            ),
            425 => 
            array (
                'id_reg' => 3426,
                'id_regmf' => 2667,
                'doors' => 2,
            ),
            426 => 
            array (
                'id_reg' => 3427,
                'id_regmf' => 2667,
                'doors' => 4,
            ),
            427 => 
            array (
                'id_reg' => 3428,
                'id_regmf' => 2668,
                'doors' => 4,
            ),
            428 => 
            array (
                'id_reg' => 3429,
                'id_regmf' => 2669,
                'doors' => 4,
            ),
            429 => 
            array (
                'id_reg' => 3430,
                'id_regmf' => 2670,
                'doors' => 3,
            ),
            430 => 
            array (
                'id_reg' => 3431,
                'id_regmf' => 2671,
                'doors' => 2,
            ),
            431 => 
            array (
                'id_reg' => 3432,
                'id_regmf' => 2672,
                'doors' => 2,
            ),
            432 => 
            array (
                'id_reg' => 3433,
                'id_regmf' => 2673,
                'doors' => 4,
            ),
            433 => 
            array (
                'id_reg' => 3434,
                'id_regmf' => 2674,
                'doors' => 3,
            ),
            434 => 
            array (
                'id_reg' => 3435,
                'id_regmf' => 2675,
                'doors' => 2,
            ),
            435 => 
            array (
                'id_reg' => 3436,
                'id_regmf' => 2676,
                'doors' => 3,
            ),
            436 => 
            array (
                'id_reg' => 3437,
                'id_regmf' => 2677,
                'doors' => 3,
            ),
            437 => 
            array (
                'id_reg' => 3438,
                'id_regmf' => 2678,
                'doors' => 4,
            ),
            438 => 
            array (
                'id_reg' => 3439,
                'id_regmf' => 2680,
                'doors' => 5,
            ),
            439 => 
            array (
                'id_reg' => 3440,
                'id_regmf' => 2679,
                'doors' => 5,
            ),
            440 => 
            array (
                'id_reg' => 3441,
                'id_regmf' => 2681,
                'doors' => 5,
            ),
            441 => 
            array (
                'id_reg' => 3442,
                'id_regmf' => 2682,
                'doors' => 5,
            ),
            442 => 
            array (
                'id_reg' => 3443,
                'id_regmf' => 2683,
                'doors' => 5,
            ),
            443 => 
            array (
                'id_reg' => 3444,
                'id_regmf' => 2685,
                'doors' => 2,
            ),
            444 => 
            array (
                'id_reg' => 3445,
                'id_regmf' => 2685,
                'doors' => 4,
            ),
            445 => 
            array (
                'id_reg' => 3446,
                'id_regmf' => 2685,
                'doors' => 5,
            ),
            446 => 
            array (
                'id_reg' => 3447,
                'id_regmf' => 2684,
                'doors' => 4,
            ),
            447 => 
            array (
                'id_reg' => 3448,
                'id_regmf' => 2684,
                'doors' => 5,
            ),
            448 => 
            array (
                'id_reg' => 3449,
                'id_regmf' => 2687,
                'doors' => 2,
            ),
            449 => 
            array (
                'id_reg' => 3450,
                'id_regmf' => 2686,
                'doors' => 2,
            ),
            450 => 
            array (
                'id_reg' => 3451,
                'id_regmf' => 2688,
                'doors' => 0,
            ),
            451 => 
            array (
                'id_reg' => 3452,
                'id_regmf' => 2689,
                'doors' => 2,
            ),
            452 => 
            array (
                'id_reg' => 3453,
                'id_regmf' => 2690,
                'doors' => 2,
            ),
            453 => 
            array (
                'id_reg' => 3454,
                'id_regmf' => 2691,
                'doors' => 3,
            ),
            454 => 
            array (
                'id_reg' => 3455,
                'id_regmf' => 2693,
                'doors' => 4,
            ),
            455 => 
            array (
                'id_reg' => 3456,
                'id_regmf' => 2692,
                'doors' => 4,
            ),
            456 => 
            array (
                'id_reg' => 3457,
                'id_regmf' => 2694,
                'doors' => 4,
            ),
            457 => 
            array (
                'id_reg' => 3458,
                'id_regmf' => 2695,
                'doors' => 2,
            ),
            458 => 
            array (
                'id_reg' => 3459,
                'id_regmf' => 2695,
                'doors' => 4,
            ),
            459 => 
            array (
                'id_reg' => 3460,
                'id_regmf' => 2695,
                'doors' => 5,
            ),
            460 => 
            array (
                'id_reg' => 3461,
                'id_regmf' => 2696,
                'doors' => 4,
            ),
            461 => 
            array (
                'id_reg' => 3462,
                'id_regmf' => 2697,
                'doors' => 3,
            ),
            462 => 
            array (
                'id_reg' => 3463,
                'id_regmf' => 2697,
                'doors' => 5,
            ),
            463 => 
            array (
                'id_reg' => 3464,
                'id_regmf' => 2698,
                'doors' => 3,
            ),
            464 => 
            array (
                'id_reg' => 3465,
                'id_regmf' => 2698,
                'doors' => 5,
            ),
            465 => 
            array (
                'id_reg' => 3466,
                'id_regmf' => 2699,
                'doors' => 2,
            ),
            466 => 
            array (
                'id_reg' => 3467,
                'id_regmf' => 2700,
                'doors' => 5,
            ),
            467 => 
            array (
                'id_reg' => 3468,
                'id_regmf' => 2701,
                'doors' => 4,
            ),
            468 => 
            array (
                'id_reg' => 3469,
                'id_regmf' => 2702,
                'doors' => 5,
            ),
            469 => 
            array (
                'id_reg' => 3470,
                'id_regmf' => 2703,
                'doors' => 4,
            ),
            470 => 
            array (
                'id_reg' => 3471,
                'id_regmf' => 2704,
                'doors' => 2,
            ),
            471 => 
            array (
                'id_reg' => 3472,
                'id_regmf' => 2704,
                'doors' => 3,
            ),
            472 => 
            array (
                'id_reg' => 3473,
                'id_regmf' => 2706,
                'doors' => 5,
            ),
            473 => 
            array (
                'id_reg' => 3474,
                'id_regmf' => 2707,
                'doors' => 5,
            ),
            474 => 
            array (
                'id_reg' => 3475,
                'id_regmf' => 2705,
                'doors' => 5,
            ),
            475 => 
            array (
                'id_reg' => 3476,
                'id_regmf' => 2709,
                'doors' => 5,
            ),
            476 => 
            array (
                'id_reg' => 3477,
                'id_regmf' => 2708,
                'doors' => 5,
            ),
            477 => 
            array (
                'id_reg' => 3478,
                'id_regmf' => 2711,
                'doors' => 3,
            ),
            478 => 
            array (
                'id_reg' => 3479,
                'id_regmf' => 2711,
                'doors' => 5,
            ),
            479 => 
            array (
                'id_reg' => 3480,
                'id_regmf' => 2710,
                'doors' => 3,
            ),
            480 => 
            array (
                'id_reg' => 3481,
                'id_regmf' => 2710,
                'doors' => 5,
            ),
            481 => 
            array (
                'id_reg' => 3482,
                'id_regmf' => 2712,
                'doors' => 2,
            ),
            482 => 
            array (
                'id_reg' => 3483,
                'id_regmf' => 2712,
                'doors' => 3,
            ),
            483 => 
            array (
                'id_reg' => 3484,
                'id_regmf' => 2713,
                'doors' => 2,
            ),
            484 => 
            array (
                'id_reg' => 3485,
                'id_regmf' => 2713,
                'doors' => 4,
            ),
            485 => 
            array (
                'id_reg' => 3486,
                'id_regmf' => 2713,
                'doors' => 5,
            ),
            486 => 
            array (
                'id_reg' => 3487,
                'id_regmf' => 2713,
                'doors' => 6,
            ),
            487 => 
            array (
                'id_reg' => 3488,
                'id_regmf' => 2714,
                'doors' => 2,
            ),
            488 => 
            array (
                'id_reg' => 3489,
                'id_regmf' => 2715,
                'doors' => 4,
            ),
            489 => 
            array (
                'id_reg' => 3490,
                'id_regmf' => 2715,
                'doors' => 5,
            ),
            490 => 
            array (
                'id_reg' => 3491,
                'id_regmf' => 2716,
                'doors' => 4,
            ),
            491 => 
            array (
                'id_reg' => 3492,
                'id_regmf' => 2716,
                'doors' => 5,
            ),
            492 => 
            array (
                'id_reg' => 3493,
                'id_regmf' => 2717,
                'doors' => 4,
            ),
            493 => 
            array (
                'id_reg' => 3494,
                'id_regmf' => 2718,
                'doors' => 2,
            ),
            494 => 
            array (
                'id_reg' => 3495,
                'id_regmf' => 2719,
                'doors' => 4,
            ),
            495 => 
            array (
                'id_reg' => 3496,
                'id_regmf' => 2720,
                'doors' => 2,
            ),
            496 => 
            array (
                'id_reg' => 3497,
                'id_regmf' => 2720,
                'doors' => 4,
            ),
            497 => 
            array (
                'id_reg' => 3498,
                'id_regmf' => 2720,
                'doors' => 5,
            ),
            498 => 
            array (
                'id_reg' => 3499,
                'id_regmf' => 2721,
                'doors' => 4,
            ),
            499 => 
            array (
                'id_reg' => 3500,
                'id_regmf' => 2723,
                'doors' => 5,
            ),
        ));
        \DB::table('model_fuel_numdoors')->insert(array (
            0 => 
            array (
                'id_reg' => 3501,
                'id_regmf' => 2722,
                'doors' => 5,
            ),
            1 => 
            array (
                'id_reg' => 3502,
                'id_regmf' => 2725,
                'doors' => 5,
            ),
            2 => 
            array (
                'id_reg' => 3503,
                'id_regmf' => 2724,
                'doors' => 5,
            ),
            3 => 
            array (
                'id_reg' => 3504,
                'id_regmf' => 2726,
                'doors' => 2,
            ),
            4 => 
            array (
                'id_reg' => 3505,
                'id_regmf' => 2727,
                'doors' => 2,
            ),
            5 => 
            array (
                'id_reg' => 3506,
                'id_regmf' => 2729,
                'doors' => 2,
            ),
            6 => 
            array (
                'id_reg' => 3507,
                'id_regmf' => 2729,
                'doors' => 3,
            ),
            7 => 
            array (
                'id_reg' => 3508,
                'id_regmf' => 2728,
                'doors' => 2,
            ),
            8 => 
            array (
                'id_reg' => 3509,
                'id_regmf' => 2730,
                'doors' => 2,
            ),
            9 => 
            array (
                'id_reg' => 3510,
                'id_regmf' => 2730,
                'doors' => 4,
            ),
            10 => 
            array (
                'id_reg' => 3511,
                'id_regmf' => 2730,
                'doors' => 5,
            ),
            11 => 
            array (
                'id_reg' => 3512,
                'id_regmf' => 2730,
                'doors' => 6,
            ),
            12 => 
            array (
                'id_reg' => 3513,
                'id_regmf' => 2732,
                'doors' => 5,
            ),
            13 => 
            array (
                'id_reg' => 3514,
                'id_regmf' => 2731,
                'doors' => 5,
            ),
            14 => 
            array (
                'id_reg' => 3515,
                'id_regmf' => 2733,
                'doors' => 5,
            ),
            15 => 
            array (
                'id_reg' => 3516,
                'id_regmf' => 2734,
                'doors' => 2,
            ),
            16 => 
            array (
                'id_reg' => 3517,
                'id_regmf' => 2735,
                'doors' => 2,
            ),
            17 => 
            array (
                'id_reg' => 3518,
                'id_regmf' => 2736,
                'doors' => 2,
            ),
            18 => 
            array (
                'id_reg' => 3519,
                'id_regmf' => 2737,
                'doors' => 4,
            ),
            19 => 
            array (
                'id_reg' => 3520,
                'id_regmf' => 2738,
                'doors' => 0,
            ),
            20 => 
            array (
                'id_reg' => 3521,
                'id_regmf' => 2739,
                'doors' => 5,
            ),
            21 => 
            array (
                'id_reg' => 3522,
                'id_regmf' => 2740,
                'doors' => 4,
            ),
            22 => 
            array (
                'id_reg' => 3523,
                'id_regmf' => 2741,
                'doors' => 2,
            ),
            23 => 
            array (
                'id_reg' => 3524,
                'id_regmf' => 2742,
                'doors' => 4,
            ),
            24 => 
            array (
                'id_reg' => 3525,
                'id_regmf' => 2743,
                'doors' => 0,
            ),
            25 => 
            array (
                'id_reg' => 3526,
                'id_regmf' => 2744,
                'doors' => 4,
            ),
            26 => 
            array (
                'id_reg' => 3527,
                'id_regmf' => 2745,
                'doors' => 4,
            ),
            27 => 
            array (
                'id_reg' => 3528,
                'id_regmf' => 2746,
                'doors' => 2,
            ),
            28 => 
            array (
                'id_reg' => 3529,
                'id_regmf' => 2747,
                'doors' => 2,
            ),
            29 => 
            array (
                'id_reg' => 3530,
                'id_regmf' => 2748,
                'doors' => 2,
            ),
            30 => 
            array (
                'id_reg' => 3531,
                'id_regmf' => 2749,
                'doors' => 2,
            ),
            31 => 
            array (
                'id_reg' => 3532,
                'id_regmf' => 2750,
                'doors' => 2,
            ),
            32 => 
            array (
                'id_reg' => 3533,
                'id_regmf' => 2751,
                'doors' => 2,
            ),
            33 => 
            array (
                'id_reg' => 3534,
                'id_regmf' => 2752,
                'doors' => 2,
            ),
            34 => 
            array (
                'id_reg' => 3535,
                'id_regmf' => 2753,
                'doors' => 2,
            ),
            35 => 
            array (
                'id_reg' => 3536,
                'id_regmf' => 2754,
                'doors' => 2,
            ),
            36 => 
            array (
                'id_reg' => 3537,
                'id_regmf' => 2754,
                'doors' => 3,
            ),
            37 => 
            array (
                'id_reg' => 3538,
                'id_regmf' => 2754,
                'doors' => 5,
            ),
            38 => 
            array (
                'id_reg' => 3539,
                'id_regmf' => 2755,
                'doors' => 2,
            ),
            39 => 
            array (
                'id_reg' => 3540,
                'id_regmf' => 2755,
                'doors' => 5,
            ),
            40 => 
            array (
                'id_reg' => 3541,
                'id_regmf' => 2756,
                'doors' => 2,
            ),
            41 => 
            array (
                'id_reg' => 3542,
                'id_regmf' => 2756,
                'doors' => 3,
            ),
            42 => 
            array (
                'id_reg' => 3543,
                'id_regmf' => 2756,
                'doors' => 5,
            ),
            43 => 
            array (
                'id_reg' => 3544,
                'id_regmf' => 2757,
                'doors' => 0,
            ),
            44 => 
            array (
                'id_reg' => 3545,
                'id_regmf' => 2757,
                'doors' => 2,
            ),
            45 => 
            array (
                'id_reg' => 3546,
                'id_regmf' => 2758,
                'doors' => 2,
            ),
            46 => 
            array (
                'id_reg' => 3547,
                'id_regmf' => 2759,
                'doors' => 2,
            ),
            47 => 
            array (
                'id_reg' => 3548,
                'id_regmf' => 2817,
                'doors' => 3,
            ),
            48 => 
            array (
                'id_reg' => 3549,
                'id_regmf' => 2818,
                'doors' => 3,
            ),
            49 => 
            array (
                'id_reg' => 3550,
                'id_regmf' => 2760,
                'doors' => 3,
            ),
            50 => 
            array (
                'id_reg' => 3551,
                'id_regmf' => 2760,
                'doors' => 5,
            ),
            51 => 
            array (
                'id_reg' => 3552,
                'id_regmf' => 2762,
                'doors' => 3,
            ),
            52 => 
            array (
                'id_reg' => 3553,
                'id_regmf' => 2762,
                'doors' => 5,
            ),
            53 => 
            array (
                'id_reg' => 3554,
                'id_regmf' => 2761,
                'doors' => 3,
            ),
            54 => 
            array (
                'id_reg' => 3555,
                'id_regmf' => 2761,
                'doors' => 5,
            ),
            55 => 
            array (
                'id_reg' => 3556,
                'id_regmf' => 2763,
                'doors' => 3,
            ),
            56 => 
            array (
                'id_reg' => 3557,
                'id_regmf' => 2763,
                'doors' => 5,
            ),
            57 => 
            array (
                'id_reg' => 3558,
                'id_regmf' => 2764,
                'doors' => 4,
            ),
            58 => 
            array (
                'id_reg' => 3559,
                'id_regmf' => 2765,
                'doors' => 4,
            ),
            59 => 
            array (
                'id_reg' => 3560,
                'id_regmf' => 2766,
                'doors' => 2,
            ),
            60 => 
            array (
                'id_reg' => 3561,
                'id_regmf' => 2767,
                'doors' => 2,
            ),
            61 => 
            array (
                'id_reg' => 3562,
                'id_regmf' => 2819,
                'doors' => 5,
            ),
            62 => 
            array (
                'id_reg' => 3563,
                'id_regmf' => 2820,
                'doors' => 5,
            ),
            63 => 
            array (
                'id_reg' => 3564,
                'id_regmf' => 2768,
                'doors' => 4,
            ),
            64 => 
            array (
                'id_reg' => 3565,
                'id_regmf' => 2769,
                'doors' => 2,
            ),
            65 => 
            array (
                'id_reg' => 3566,
                'id_regmf' => 2769,
                'doors' => 4,
            ),
            66 => 
            array (
                'id_reg' => 3567,
                'id_regmf' => 2770,
                'doors' => 2,
            ),
            67 => 
            array (
                'id_reg' => 3568,
                'id_regmf' => 2770,
                'doors' => 4,
            ),
            68 => 
            array (
                'id_reg' => 3569,
                'id_regmf' => 2770,
                'doors' => 5,
            ),
            69 => 
            array (
                'id_reg' => 3570,
                'id_regmf' => 2771,
                'doors' => 3,
            ),
            70 => 
            array (
                'id_reg' => 3571,
                'id_regmf' => 2771,
                'doors' => 4,
            ),
            71 => 
            array (
                'id_reg' => 3572,
                'id_regmf' => 2771,
                'doors' => 5,
            ),
            72 => 
            array (
                'id_reg' => 3573,
                'id_regmf' => 2772,
                'doors' => 2,
            ),
            73 => 
            array (
                'id_reg' => 3574,
                'id_regmf' => 2772,
                'doors' => 3,
            ),
            74 => 
            array (
                'id_reg' => 3575,
                'id_regmf' => 2772,
                'doors' => 4,
            ),
            75 => 
            array (
                'id_reg' => 3576,
                'id_regmf' => 2772,
                'doors' => 5,
            ),
            76 => 
            array (
                'id_reg' => 3577,
                'id_regmf' => 2774,
                'doors' => 3,
            ),
            77 => 
            array (
                'id_reg' => 3578,
                'id_regmf' => 2774,
                'doors' => 5,
            ),
            78 => 
            array (
                'id_reg' => 3579,
                'id_regmf' => 2773,
                'doors' => 3,
            ),
            79 => 
            array (
                'id_reg' => 3580,
                'id_regmf' => 2773,
                'doors' => 5,
            ),
            80 => 
            array (
                'id_reg' => 3581,
                'id_regmf' => 2776,
                'doors' => 3,
            ),
            81 => 
            array (
                'id_reg' => 3582,
                'id_regmf' => 2776,
                'doors' => 5,
            ),
            82 => 
            array (
                'id_reg' => 3583,
                'id_regmf' => 2775,
                'doors' => 3,
            ),
            83 => 
            array (
                'id_reg' => 3584,
                'id_regmf' => 2775,
                'doors' => 5,
            ),
            84 => 
            array (
                'id_reg' => 3585,
                'id_regmf' => 2777,
                'doors' => 2,
            ),
            85 => 
            array (
                'id_reg' => 3586,
                'id_regmf' => 2778,
                'doors' => 2,
            ),
            86 => 
            array (
                'id_reg' => 3587,
                'id_regmf' => 2781,
                'doors' => 3,
            ),
            87 => 
            array (
                'id_reg' => 3588,
                'id_regmf' => 2781,
                'doors' => 5,
            ),
            88 => 
            array (
                'id_reg' => 3589,
                'id_regmf' => 2780,
                'doors' => 3,
            ),
            89 => 
            array (
                'id_reg' => 3590,
                'id_regmf' => 2779,
                'doors' => 3,
            ),
            90 => 
            array (
                'id_reg' => 3591,
                'id_regmf' => 2779,
                'doors' => 5,
            ),
            91 => 
            array (
                'id_reg' => 3592,
                'id_regmf' => 2821,
                'doors' => 5,
            ),
            92 => 
            array (
                'id_reg' => 3593,
                'id_regmf' => 2822,
                'doors' => 5,
            ),
            93 => 
            array (
                'id_reg' => 3594,
                'id_regmf' => 2782,
                'doors' => 4,
            ),
            94 => 
            array (
                'id_reg' => 3595,
                'id_regmf' => 2783,
                'doors' => 4,
            ),
            95 => 
            array (
                'id_reg' => 3596,
                'id_regmf' => 2784,
                'doors' => 2,
            ),
            96 => 
            array (
                'id_reg' => 3597,
                'id_regmf' => 2786,
                'doors' => 2,
            ),
            97 => 
            array (
                'id_reg' => 3598,
                'id_regmf' => 2786,
                'doors' => 4,
            ),
            98 => 
            array (
                'id_reg' => 3599,
                'id_regmf' => 2785,
                'doors' => 4,
            ),
            99 => 
            array (
                'id_reg' => 3600,
                'id_regmf' => 2788,
                'doors' => 4,
            ),
            100 => 
            array (
                'id_reg' => 3601,
                'id_regmf' => 2788,
                'doors' => 5,
            ),
            101 => 
            array (
                'id_reg' => 3602,
                'id_regmf' => 2787,
                'doors' => 4,
            ),
            102 => 
            array (
                'id_reg' => 3603,
                'id_regmf' => 2787,
                'doors' => 5,
            ),
            103 => 
            array (
                'id_reg' => 3604,
                'id_regmf' => 2790,
                'doors' => 3,
            ),
            104 => 
            array (
                'id_reg' => 3605,
                'id_regmf' => 2790,
                'doors' => 5,
            ),
            105 => 
            array (
                'id_reg' => 3606,
                'id_regmf' => 2789,
                'doors' => 3,
            ),
            106 => 
            array (
                'id_reg' => 3607,
                'id_regmf' => 2789,
                'doors' => 5,
            ),
            107 => 
            array (
                'id_reg' => 3608,
                'id_regmf' => 2823,
                'doors' => 5,
            ),
            108 => 
            array (
                'id_reg' => 3609,
                'id_regmf' => 2824,
                'doors' => 5,
            ),
            109 => 
            array (
                'id_reg' => 3610,
                'id_regmf' => 2825,
                'doors' => 5,
            ),
            110 => 
            array (
                'id_reg' => 3611,
                'id_regmf' => 2791,
                'doors' => 4,
            ),
            111 => 
            array (
                'id_reg' => 3612,
                'id_regmf' => 2792,
                'doors' => 4,
            ),
            112 => 
            array (
                'id_reg' => 3613,
                'id_regmf' => 2793,
                'doors' => 4,
            ),
            113 => 
            array (
                'id_reg' => 3614,
                'id_regmf' => 2794,
                'doors' => 3,
            ),
            114 => 
            array (
                'id_reg' => 3615,
                'id_regmf' => 2794,
                'doors' => 4,
            ),
            115 => 
            array (
                'id_reg' => 3616,
                'id_regmf' => 2794,
                'doors' => 5,
            ),
            116 => 
            array (
                'id_reg' => 3617,
                'id_regmf' => 2795,
                'doors' => 2,
            ),
            117 => 
            array (
                'id_reg' => 3618,
                'id_regmf' => 2795,
                'doors' => 3,
            ),
            118 => 
            array (
                'id_reg' => 3619,
                'id_regmf' => 2795,
                'doors' => 4,
            ),
            119 => 
            array (
                'id_reg' => 3620,
                'id_regmf' => 2795,
                'doors' => 5,
            ),
            120 => 
            array (
                'id_reg' => 3621,
                'id_regmf' => 2796,
                'doors' => 4,
            ),
            121 => 
            array (
                'id_reg' => 3622,
                'id_regmf' => 2796,
                'doors' => 5,
            ),
            122 => 
            array (
                'id_reg' => 3623,
                'id_regmf' => 2797,
                'doors' => 4,
            ),
            123 => 
            array (
                'id_reg' => 3624,
                'id_regmf' => 2797,
                'doors' => 5,
            ),
            124 => 
            array (
                'id_reg' => 3625,
                'id_regmf' => 2798,
                'doors' => 4,
            ),
            125 => 
            array (
                'id_reg' => 3626,
                'id_regmf' => 2827,
                'doors' => 5,
            ),
            126 => 
            array (
                'id_reg' => 3627,
                'id_regmf' => 2826,
                'doors' => 5,
            ),
            127 => 
            array (
                'id_reg' => 3628,
                'id_regmf' => 2800,
                'doors' => 2,
            ),
            128 => 
            array (
                'id_reg' => 3629,
                'id_regmf' => 2800,
                'doors' => 3,
            ),
            129 => 
            array (
                'id_reg' => 3630,
                'id_regmf' => 2800,
                'doors' => 4,
            ),
            130 => 
            array (
                'id_reg' => 3631,
                'id_regmf' => 2800,
                'doors' => 5,
            ),
            131 => 
            array (
                'id_reg' => 3632,
                'id_regmf' => 2799,
                'doors' => 4,
            ),
            132 => 
            array (
                'id_reg' => 3633,
                'id_regmf' => 2799,
                'doors' => 5,
            ),
            133 => 
            array (
                'id_reg' => 3634,
                'id_regmf' => 2802,
                'doors' => 2,
            ),
            134 => 
            array (
                'id_reg' => 3635,
                'id_regmf' => 2801,
                'doors' => 2,
            ),
            135 => 
            array (
                'id_reg' => 3636,
                'id_regmf' => 2804,
                'doors' => 4,
            ),
            136 => 
            array (
                'id_reg' => 3637,
                'id_regmf' => 2804,
                'doors' => 5,
            ),
            137 => 
            array (
                'id_reg' => 3638,
                'id_regmf' => 2803,
                'doors' => 4,
            ),
            138 => 
            array (
                'id_reg' => 3639,
                'id_regmf' => 2803,
                'doors' => 5,
            ),
            139 => 
            array (
                'id_reg' => 3640,
                'id_regmf' => 2805,
                'doors' => 4,
            ),
            140 => 
            array (
                'id_reg' => 3641,
                'id_regmf' => 2805,
                'doors' => 5,
            ),
            141 => 
            array (
                'id_reg' => 3642,
                'id_regmf' => 2806,
                'doors' => 4,
            ),
            142 => 
            array (
                'id_reg' => 3643,
                'id_regmf' => 2806,
                'doors' => 5,
            ),
            143 => 
            array (
                'id_reg' => 3644,
                'id_regmf' => 2807,
                'doors' => 4,
            ),
            144 => 
            array (
                'id_reg' => 3645,
                'id_regmf' => 2808,
                'doors' => 4,
            ),
            145 => 
            array (
                'id_reg' => 3646,
                'id_regmf' => 2809,
                'doors' => 4,
            ),
            146 => 
            array (
                'id_reg' => 3647,
                'id_regmf' => 2810,
                'doors' => 4,
            ),
            147 => 
            array (
                'id_reg' => 3648,
                'id_regmf' => 2812,
                'doors' => 4,
            ),
            148 => 
            array (
                'id_reg' => 3649,
                'id_regmf' => 2811,
                'doors' => 4,
            ),
            149 => 
            array (
                'id_reg' => 3650,
                'id_regmf' => 2813,
                'doors' => 5,
            ),
            150 => 
            array (
                'id_reg' => 3651,
                'id_regmf' => 2814,
                'doors' => 5,
            ),
            151 => 
            array (
                'id_reg' => 3652,
                'id_regmf' => 2816,
                'doors' => 5,
            ),
            152 => 
            array (
                'id_reg' => 3653,
                'id_regmf' => 2815,
                'doors' => 5,
            ),
            153 => 
            array (
                'id_reg' => 3654,
                'id_regmf' => 2829,
                'doors' => 4,
            ),
            154 => 
            array (
                'id_reg' => 3655,
                'id_regmf' => 2829,
                'doors' => 5,
            ),
            155 => 
            array (
                'id_reg' => 3656,
                'id_regmf' => 2828,
                'doors' => 4,
            ),
            156 => 
            array (
                'id_reg' => 3657,
                'id_regmf' => 2828,
                'doors' => 5,
            ),
            157 => 
            array (
                'id_reg' => 3658,
                'id_regmf' => 2828,
                'doors' => 6,
            ),
            158 => 
            array (
                'id_reg' => 3659,
                'id_regmf' => 2831,
                'doors' => 5,
            ),
            159 => 
            array (
                'id_reg' => 3660,
                'id_regmf' => 2830,
                'doors' => 2,
            ),
            160 => 
            array (
                'id_reg' => 3661,
                'id_regmf' => 2830,
                'doors' => 4,
            ),
            161 => 
            array (
                'id_reg' => 3662,
                'id_regmf' => 2830,
                'doors' => 5,
            ),
            162 => 
            array (
                'id_reg' => 3663,
                'id_regmf' => 2833,
                'doors' => 2,
            ),
            163 => 
            array (
                'id_reg' => 3664,
                'id_regmf' => 2832,
                'doors' => 2,
            ),
            164 => 
            array (
                'id_reg' => 3665,
                'id_regmf' => 2835,
                'doors' => 5,
            ),
            165 => 
            array (
                'id_reg' => 3666,
                'id_regmf' => 2834,
                'doors' => 2,
            ),
            166 => 
            array (
                'id_reg' => 3667,
                'id_regmf' => 2834,
                'doors' => 4,
            ),
            167 => 
            array (
                'id_reg' => 3668,
                'id_regmf' => 2834,
                'doors' => 5,
            ),
            168 => 
            array (
                'id_reg' => 3669,
                'id_regmf' => 2834,
                'doors' => 6,
            ),
            169 => 
            array (
                'id_reg' => 3670,
                'id_regmf' => 2836,
                'doors' => 5,
            ),
            170 => 
            array (
                'id_reg' => 3671,
                'id_regmf' => 2838,
                'doors' => 2,
            ),
            171 => 
            array (
                'id_reg' => 3672,
                'id_regmf' => 2838,
                'doors' => 4,
            ),
            172 => 
            array (
                'id_reg' => 3673,
                'id_regmf' => 2837,
                'doors' => 2,
            ),
            173 => 
            array (
                'id_reg' => 3674,
                'id_regmf' => 2837,
                'doors' => 3,
            ),
            174 => 
            array (
                'id_reg' => 3675,
                'id_regmf' => 2837,
                'doors' => 4,
            ),
            175 => 
            array (
                'id_reg' => 3676,
                'id_regmf' => 2837,
                'doors' => 5,
            ),
            176 => 
            array (
                'id_reg' => 3677,
                'id_regmf' => 2839,
                'doors' => 5,
            ),
            177 => 
            array (
                'id_reg' => 3678,
                'id_regmf' => 2840,
                'doors' => 0,
            ),
            178 => 
            array (
                'id_reg' => 3679,
                'id_regmf' => 2841,
                'doors' => 3,
            ),
            179 => 
            array (
                'id_reg' => 3680,
                'id_regmf' => 2843,
                'doors' => 4,
            ),
            180 => 
            array (
                'id_reg' => 3681,
                'id_regmf' => 2843,
                'doors' => 5,
            ),
            181 => 
            array (
                'id_reg' => 3682,
                'id_regmf' => 2842,
                'doors' => 4,
            ),
            182 => 
            array (
                'id_reg' => 3683,
                'id_regmf' => 2842,
                'doors' => 5,
            ),
            183 => 
            array (
                'id_reg' => 3684,
                'id_regmf' => 2844,
                'doors' => 2,
            ),
            184 => 
            array (
                'id_reg' => 3685,
                'id_regmf' => 2845,
                'doors' => 2,
            ),
            185 => 
            array (
                'id_reg' => 3686,
                'id_regmf' => 2846,
                'doors' => 5,
            ),
            186 => 
            array (
                'id_reg' => 3687,
                'id_regmf' => 2847,
                'doors' => 0,
            ),
            187 => 
            array (
                'id_reg' => 3688,
                'id_regmf' => 2848,
                'doors' => 0,
            ),
            188 => 
            array (
                'id_reg' => 3689,
                'id_regmf' => 2849,
                'doors' => 0,
            ),
            189 => 
            array (
                'id_reg' => 3690,
                'id_regmf' => 2850,
                'doors' => 2,
            ),
            190 => 
            array (
                'id_reg' => 3691,
                'id_regmf' => 2851,
                'doors' => 2,
            ),
            191 => 
            array (
                'id_reg' => 3692,
                'id_regmf' => 2853,
                'doors' => 2,
            ),
            192 => 
            array (
                'id_reg' => 3693,
                'id_regmf' => 2853,
                'doors' => 3,
            ),
            193 => 
            array (
                'id_reg' => 3694,
                'id_regmf' => 2853,
                'doors' => 5,
            ),
            194 => 
            array (
                'id_reg' => 3695,
                'id_regmf' => 2854,
                'doors' => 2,
            ),
            195 => 
            array (
                'id_reg' => 3696,
                'id_regmf' => 2854,
                'doors' => 5,
            ),
            196 => 
            array (
                'id_reg' => 3697,
                'id_regmf' => 2852,
                'doors' => 2,
            ),
            197 => 
            array (
                'id_reg' => 3698,
                'id_regmf' => 2852,
                'doors' => 3,
            ),
            198 => 
            array (
                'id_reg' => 3699,
                'id_regmf' => 2852,
                'doors' => 5,
            ),
            199 => 
            array (
                'id_reg' => 3700,
                'id_regmf' => 2855,
                'doors' => 2,
            ),
            200 => 
            array (
                'id_reg' => 3701,
                'id_regmf' => 2855,
                'doors' => 3,
            ),
            201 => 
            array (
                'id_reg' => 3702,
                'id_regmf' => 2855,
                'doors' => 4,
            ),
            202 => 
            array (
                'id_reg' => 3703,
                'id_regmf' => 2855,
                'doors' => 5,
            ),
            203 => 
            array (
                'id_reg' => 3704,
                'id_regmf' => 2856,
                'doors' => 2,
            ),
            204 => 
            array (
                'id_reg' => 3705,
                'id_regmf' => 2857,
                'doors' => 3,
            ),
            205 => 
            array (
                'id_reg' => 3706,
                'id_regmf' => 2858,
                'doors' => 2,
            ),
            206 => 
            array (
                'id_reg' => 3707,
                'id_regmf' => 2859,
                'doors' => 2,
            ),
            207 => 
            array (
                'id_reg' => 3708,
                'id_regmf' => 2860,
                'doors' => 2,
            ),
            208 => 
            array (
                'id_reg' => 3709,
                'id_regmf' => 2861,
                'doors' => 2,
            ),
            209 => 
            array (
                'id_reg' => 3710,
                'id_regmf' => 2862,
                'doors' => 2,
            ),
            210 => 
            array (
                'id_reg' => 3711,
                'id_regmf' => 2863,
                'doors' => 4,
            ),
            211 => 
            array (
                'id_reg' => 3712,
                'id_regmf' => 2864,
                'doors' => 2,
            ),
            212 => 
            array (
                'id_reg' => 3713,
                'id_regmf' => 2865,
                'doors' => 2,
            ),
            213 => 
            array (
                'id_reg' => 3714,
                'id_regmf' => 2867,
                'doors' => 2,
            ),
            214 => 
            array (
                'id_reg' => 3715,
                'id_regmf' => 2866,
                'doors' => 2,
            ),
            215 => 
            array (
                'id_reg' => 3716,
                'id_regmf' => 2868,
                'doors' => 2,
            ),
            216 => 
            array (
                'id_reg' => 3717,
                'id_regmf' => 2869,
                'doors' => 5,
            ),
            217 => 
            array (
                'id_reg' => 3718,
                'id_regmf' => 2870,
                'doors' => 4,
            ),
            218 => 
            array (
                'id_reg' => 3719,
                'id_regmf' => 2871,
                'doors' => 4,
            ),
            219 => 
            array (
                'id_reg' => 3720,
                'id_regmf' => 2872,
                'doors' => 3,
            ),
            220 => 
            array (
                'id_reg' => 3721,
                'id_regmf' => 2873,
                'doors' => 4,
            ),
            221 => 
            array (
                'id_reg' => 3722,
                'id_regmf' => 2874,
                'doors' => 4,
            ),
            222 => 
            array (
                'id_reg' => 3723,
                'id_regmf' => 2875,
                'doors' => 2,
            ),
            223 => 
            array (
                'id_reg' => 3724,
                'id_regmf' => 2876,
                'doors' => 2,
            ),
            224 => 
            array (
                'id_reg' => 3725,
                'id_regmf' => 2877,
                'doors' => 2,
            ),
            225 => 
            array (
                'id_reg' => 3726,
                'id_regmf' => 2878,
                'doors' => 2,
            ),
            226 => 
            array (
                'id_reg' => 3727,
                'id_regmf' => 2879,
                'doors' => 4,
            ),
            227 => 
            array (
                'id_reg' => 3728,
                'id_regmf' => 2880,
                'doors' => 5,
            ),
            228 => 
            array (
                'id_reg' => 3729,
                'id_regmf' => 2881,
                'doors' => 5,
            ),
            229 => 
            array (
                'id_reg' => 3730,
                'id_regmf' => 2882,
                'doors' => 2,
            ),
            230 => 
            array (
                'id_reg' => 3731,
                'id_regmf' => 2882,
                'doors' => 4,
            ),
            231 => 
            array (
                'id_reg' => 3732,
                'id_regmf' => 2883,
                'doors' => 5,
            ),
            232 => 
            array (
                'id_reg' => 3733,
                'id_regmf' => 2884,
                'doors' => 5,
            ),
            233 => 
            array (
                'id_reg' => 3734,
                'id_regmf' => 2886,
                'doors' => 0,
            ),
            234 => 
            array (
                'id_reg' => 3735,
                'id_regmf' => 2885,
                'doors' => 0,
            ),
            235 => 
            array (
                'id_reg' => 3736,
                'id_regmf' => 2887,
                'doors' => 0,
            ),
            236 => 
            array (
                'id_reg' => 3737,
                'id_regmf' => 2888,
                'doors' => 0,
            ),
            237 => 
            array (
                'id_reg' => 3738,
                'id_regmf' => 2889,
                'doors' => 0,
            ),
            238 => 
            array (
                'id_reg' => 3739,
                'id_regmf' => 2890,
                'doors' => 0,
            ),
            239 => 
            array (
                'id_reg' => 3740,
                'id_regmf' => 2891,
                'doors' => 0,
            ),
            240 => 
            array (
                'id_reg' => 3741,
                'id_regmf' => 2892,
                'doors' => 0,
            ),
            241 => 
            array (
                'id_reg' => 3742,
                'id_regmf' => 2893,
                'doors' => 0,
            ),
            242 => 
            array (
                'id_reg' => 3743,
                'id_regmf' => 2894,
                'doors' => 0,
            ),
            243 => 
            array (
                'id_reg' => 3744,
                'id_regmf' => 2895,
                'doors' => 0,
            ),
            244 => 
            array (
                'id_reg' => 3745,
                'id_regmf' => 2896,
                'doors' => 0,
            ),
            245 => 
            array (
                'id_reg' => 3746,
                'id_regmf' => 2897,
                'doors' => 4,
            ),
            246 => 
            array (
                'id_reg' => 3747,
                'id_regmf' => 2898,
                'doors' => 5,
            ),
            247 => 
            array (
                'id_reg' => 3748,
                'id_regmf' => 2899,
                'doors' => 2,
            ),
            248 => 
            array (
                'id_reg' => 3749,
                'id_regmf' => 2900,
                'doors' => 4,
            ),
            249 => 
            array (
                'id_reg' => 3750,
                'id_regmf' => 2901,
                'doors' => 2,
            ),
            250 => 
            array (
                'id_reg' => 3751,
                'id_regmf' => 2902,
                'doors' => 4,
            ),
            251 => 
            array (
                'id_reg' => 3752,
                'id_regmf' => 2903,
                'doors' => 2,
            ),
            252 => 
            array (
                'id_reg' => 3753,
                'id_regmf' => 2904,
                'doors' => 2,
            ),
            253 => 
            array (
                'id_reg' => 3754,
                'id_regmf' => 2904,
                'doors' => 3,
            ),
            254 => 
            array (
                'id_reg' => 3755,
                'id_regmf' => 2905,
                'doors' => 4,
            ),
            255 => 
            array (
                'id_reg' => 3756,
                'id_regmf' => 2906,
                'doors' => 2,
            ),
            256 => 
            array (
                'id_reg' => 3757,
                'id_regmf' => 2906,
                'doors' => 4,
            ),
            257 => 
            array (
                'id_reg' => 3758,
                'id_regmf' => 2907,
                'doors' => 4,
            ),
            258 => 
            array (
                'id_reg' => 3759,
                'id_regmf' => 2908,
                'doors' => 2,
            ),
            259 => 
            array (
                'id_reg' => 3760,
                'id_regmf' => 2908,
                'doors' => 4,
            ),
            260 => 
            array (
                'id_reg' => 3761,
                'id_regmf' => 2909,
                'doors' => 5,
            ),
            261 => 
            array (
                'id_reg' => 3762,
                'id_regmf' => 2910,
                'doors' => 4,
            ),
            262 => 
            array (
                'id_reg' => 3763,
                'id_regmf' => 2911,
                'doors' => 3,
            ),
            263 => 
            array (
                'id_reg' => 3764,
                'id_regmf' => 2912,
                'doors' => 2,
            ),
            264 => 
            array (
                'id_reg' => 3765,
                'id_regmf' => 2912,
                'doors' => 3,
            ),
            265 => 
            array (
                'id_reg' => 3766,
                'id_regmf' => 2913,
                'doors' => 5,
            ),
            266 => 
            array (
                'id_reg' => 3767,
                'id_regmf' => 2914,
                'doors' => 4,
            ),
            267 => 
            array (
                'id_reg' => 3768,
                'id_regmf' => 2915,
                'doors' => 4,
            ),
            268 => 
            array (
                'id_reg' => 3769,
                'id_regmf' => 2916,
                'doors' => 2,
            ),
            269 => 
            array (
                'id_reg' => 3770,
                'id_regmf' => 2917,
                'doors' => 2,
            ),
            270 => 
            array (
                'id_reg' => 3771,
                'id_regmf' => 2918,
                'doors' => 2,
            ),
            271 => 
            array (
                'id_reg' => 3772,
                'id_regmf' => 2918,
                'doors' => 3,
            ),
            272 => 
            array (
                'id_reg' => 3773,
                'id_regmf' => 2919,
                'doors' => 2,
            ),
            273 => 
            array (
                'id_reg' => 3774,
                'id_regmf' => 2920,
                'doors' => 4,
            ),
            274 => 
            array (
                'id_reg' => 3775,
                'id_regmf' => 2921,
                'doors' => 5,
            ),
            275 => 
            array (
                'id_reg' => 3776,
                'id_regmf' => 2922,
                'doors' => 2,
            ),
            276 => 
            array (
                'id_reg' => 3777,
                'id_regmf' => 2922,
                'doors' => 3,
            ),
            277 => 
            array (
                'id_reg' => 3778,
                'id_regmf' => 2923,
                'doors' => 4,
            ),
            278 => 
            array (
                'id_reg' => 3779,
                'id_regmf' => 2924,
                'doors' => 5,
            ),
            279 => 
            array (
                'id_reg' => 3780,
                'id_regmf' => 2925,
                'doors' => 2,
            ),
            280 => 
            array (
                'id_reg' => 3781,
                'id_regmf' => 2926,
                'doors' => 2,
            ),
            281 => 
            array (
                'id_reg' => 3782,
                'id_regmf' => 2927,
                'doors' => 2,
            ),
            282 => 
            array (
                'id_reg' => 3783,
                'id_regmf' => 2928,
                'doors' => 2,
            ),
            283 => 
            array (
                'id_reg' => 3784,
                'id_regmf' => 2929,
                'doors' => 2,
            ),
            284 => 
            array (
                'id_reg' => 3785,
                'id_regmf' => 2930,
                'doors' => 2,
            ),
            285 => 
            array (
                'id_reg' => 3786,
                'id_regmf' => 2931,
                'doors' => 2,
            ),
            286 => 
            array (
                'id_reg' => 3787,
                'id_regmf' => 2932,
                'doors' => 2,
            ),
            287 => 
            array (
                'id_reg' => 3788,
                'id_regmf' => 2933,
                'doors' => 2,
            ),
            288 => 
            array (
                'id_reg' => 3789,
                'id_regmf' => 2934,
                'doors' => 2,
            ),
            289 => 
            array (
                'id_reg' => 3790,
                'id_regmf' => 2934,
                'doors' => 3,
            ),
            290 => 
            array (
                'id_reg' => 3791,
                'id_regmf' => 2935,
                'doors' => 2,
            ),
            291 => 
            array (
                'id_reg' => 3792,
                'id_regmf' => 2936,
                'doors' => 2,
            ),
            292 => 
            array (
                'id_reg' => 3793,
                'id_regmf' => 2937,
                'doors' => 2,
            ),
            293 => 
            array (
                'id_reg' => 3794,
                'id_regmf' => 2938,
                'doors' => 2,
            ),
            294 => 
            array (
                'id_reg' => 3795,
                'id_regmf' => 2939,
                'doors' => 3,
            ),
            295 => 
            array (
                'id_reg' => 3796,
                'id_regmf' => 2940,
                'doors' => 2,
            ),
            296 => 
            array (
                'id_reg' => 3797,
                'id_regmf' => 2940,
                'doors' => 3,
            ),
            297 => 
            array (
                'id_reg' => 3798,
                'id_regmf' => 2941,
                'doors' => 3,
            ),
            298 => 
            array (
                'id_reg' => 3799,
                'id_regmf' => 2942,
                'doors' => 2,
            ),
            299 => 
            array (
                'id_reg' => 3800,
                'id_regmf' => 2942,
                'doors' => 3,
            ),
            300 => 
            array (
                'id_reg' => 3801,
                'id_regmf' => 2943,
                'doors' => 2,
            ),
            301 => 
            array (
                'id_reg' => 3802,
                'id_regmf' => 2944,
                'doors' => 2,
            ),
            302 => 
            array (
                'id_reg' => 3803,
                'id_regmf' => 2947,
                'doors' => 5,
            ),
            303 => 
            array (
                'id_reg' => 3804,
                'id_regmf' => 2945,
                'doors' => 5,
            ),
            304 => 
            array (
                'id_reg' => 3805,
                'id_regmf' => 2946,
                'doors' => 5,
            ),
            305 => 
            array (
                'id_reg' => 3806,
                'id_regmf' => 2948,
                'doors' => 2,
            ),
            306 => 
            array (
                'id_reg' => 3807,
                'id_regmf' => 2948,
                'doors' => 3,
            ),
            307 => 
            array (
                'id_reg' => 3808,
                'id_regmf' => 2949,
                'doors' => 5,
            ),
            308 => 
            array (
                'id_reg' => 3809,
                'id_regmf' => 2950,
                'doors' => 5,
            ),
            309 => 
            array (
                'id_reg' => 3810,
                'id_regmf' => 2952,
                'doors' => 5,
            ),
            310 => 
            array (
                'id_reg' => 3811,
                'id_regmf' => 2953,
                'doors' => 5,
            ),
            311 => 
            array (
                'id_reg' => 3812,
                'id_regmf' => 2951,
                'doors' => 5,
            ),
            312 => 
            array (
                'id_reg' => 3813,
                'id_regmf' => 2954,
                'doors' => 0,
            ),
            313 => 
            array (
                'id_reg' => 3814,
                'id_regmf' => 2955,
                'doors' => 4,
            ),
            314 => 
            array (
                'id_reg' => 3815,
                'id_regmf' => 2956,
                'doors' => 3,
            ),
            315 => 
            array (
                'id_reg' => 3816,
                'id_regmf' => 2957,
                'doors' => 4,
            ),
            316 => 
            array (
                'id_reg' => 3817,
                'id_regmf' => 2957,
                'doors' => 5,
            ),
            317 => 
            array (
                'id_reg' => 3818,
                'id_regmf' => 2958,
                'doors' => 2,
            ),
            318 => 
            array (
                'id_reg' => 3819,
                'id_regmf' => 2958,
                'doors' => 3,
            ),
            319 => 
            array (
                'id_reg' => 3820,
                'id_regmf' => 2958,
                'doors' => 4,
            ),
            320 => 
            array (
                'id_reg' => 3821,
                'id_regmf' => 2958,
                'doors' => 5,
            ),
            321 => 
            array (
                'id_reg' => 3822,
                'id_regmf' => 2959,
                'doors' => 4,
            ),
            322 => 
            array (
                'id_reg' => 3823,
                'id_regmf' => 2959,
                'doors' => 5,
            ),
            323 => 
            array (
                'id_reg' => 3824,
                'id_regmf' => 2960,
                'doors' => 3,
            ),
            324 => 
            array (
                'id_reg' => 3825,
                'id_regmf' => 2961,
                'doors' => 5,
            ),
            325 => 
            array (
                'id_reg' => 3826,
                'id_regmf' => 2962,
                'doors' => 2,
            ),
            326 => 
            array (
                'id_reg' => 3827,
                'id_regmf' => 2963,
                'doors' => 3,
            ),
            327 => 
            array (
                'id_reg' => 3828,
                'id_regmf' => 2964,
                'doors' => 0,
            ),
            328 => 
            array (
                'id_reg' => 3829,
                'id_regmf' => 2964,
                'doors' => 2,
            ),
            329 => 
            array (
                'id_reg' => 3830,
                'id_regmf' => 2965,
                'doors' => 0,
            ),
            330 => 
            array (
                'id_reg' => 3831,
                'id_regmf' => 2966,
                'doors' => 0,
            ),
            331 => 
            array (
                'id_reg' => 3832,
                'id_regmf' => 2967,
                'doors' => 2,
            ),
            332 => 
            array (
                'id_reg' => 3833,
                'id_regmf' => 2968,
                'doors' => 4,
            ),
            333 => 
            array (
                'id_reg' => 3834,
                'id_regmf' => 2969,
                'doors' => 2,
            ),
            334 => 
            array (
                'id_reg' => 3835,
                'id_regmf' => 2970,
                'doors' => 2,
            ),
            335 => 
            array (
                'id_reg' => 3836,
                'id_regmf' => 2971,
                'doors' => 5,
            ),
            336 => 
            array (
                'id_reg' => 3837,
                'id_regmf' => 2972,
                'doors' => 0,
            ),
            337 => 
            array (
                'id_reg' => 3838,
                'id_regmf' => 2973,
                'doors' => 0,
            ),
            338 => 
            array (
                'id_reg' => 3839,
                'id_regmf' => 2974,
                'doors' => 2,
            ),
            339 => 
            array (
                'id_reg' => 3840,
                'id_regmf' => 2975,
                'doors' => 2,
            ),
            340 => 
            array (
                'id_reg' => 3841,
                'id_regmf' => 2976,
                'doors' => 2,
            ),
            341 => 
            array (
                'id_reg' => 3842,
                'id_regmf' => 2976,
                'doors' => 3,
            ),
            342 => 
            array (
                'id_reg' => 3843,
                'id_regmf' => 2986,
                'doors' => 4,
            ),
            343 => 
            array (
                'id_reg' => 3844,
                'id_regmf' => 2987,
                'doors' => 3,
            ),
            344 => 
            array (
                'id_reg' => 3845,
                'id_regmf' => 2987,
                'doors' => 5,
            ),
            345 => 
            array (
                'id_reg' => 3846,
                'id_regmf' => 2988,
                'doors' => 3,
            ),
            346 => 
            array (
                'id_reg' => 3847,
                'id_regmf' => 2988,
                'doors' => 5,
            ),
            347 => 
            array (
                'id_reg' => 3848,
                'id_regmf' => 2989,
                'doors' => 4,
            ),
            348 => 
            array (
                'id_reg' => 3849,
                'id_regmf' => 2989,
                'doors' => 5,
            ),
            349 => 
            array (
                'id_reg' => 3850,
                'id_regmf' => 2990,
                'doors' => 5,
            ),
            350 => 
            array (
                'id_reg' => 3851,
                'id_regmf' => 2991,
                'doors' => 3,
            ),
            351 => 
            array (
                'id_reg' => 3852,
                'id_regmf' => 2992,
                'doors' => 3,
            ),
            352 => 
            array (
                'id_reg' => 3853,
                'id_regmf' => 2992,
                'doors' => 5,
            ),
            353 => 
            array (
                'id_reg' => 3854,
                'id_regmf' => 2993,
                'doors' => 2,
            ),
            354 => 
            array (
                'id_reg' => 3855,
                'id_regmf' => 2994,
                'doors' => 4,
            ),
            355 => 
            array (
                'id_reg' => 3856,
                'id_regmf' => 2994,
                'doors' => 5,
            ),
            356 => 
            array (
                'id_reg' => 3857,
                'id_regmf' => 2995,
                'doors' => 4,
            ),
            357 => 
            array (
                'id_reg' => 3858,
                'id_regmf' => 2995,
                'doors' => 5,
            ),
            358 => 
            array (
                'id_reg' => 3859,
                'id_regmf' => 2997,
                'doors' => 5,
            ),
            359 => 
            array (
                'id_reg' => 3860,
                'id_regmf' => 2996,
                'doors' => 5,
            ),
            360 => 
            array (
                'id_reg' => 3861,
                'id_regmf' => 2998,
                'doors' => 4,
            ),
            361 => 
            array (
                'id_reg' => 3862,
                'id_regmf' => 2998,
                'doors' => 5,
            ),
            362 => 
            array (
                'id_reg' => 3863,
                'id_regmf' => 2999,
                'doors' => 4,
            ),
            363 => 
            array (
                'id_reg' => 3864,
                'id_regmf' => 2999,
                'doors' => 5,
            ),
            364 => 
            array (
                'id_reg' => 3865,
                'id_regmf' => 3000,
                'doors' => 5,
            ),
            365 => 
            array (
                'id_reg' => 3866,
                'id_regmf' => 3001,
                'doors' => 5,
            ),
            366 => 
            array (
                'id_reg' => 3867,
                'id_regmf' => 3003,
                'doors' => 5,
            ),
            367 => 
            array (
                'id_reg' => 3868,
                'id_regmf' => 3002,
                'doors' => 5,
            ),
            368 => 
            array (
                'id_reg' => 3869,
                'id_regmf' => 2977,
                'doors' => 3,
            ),
            369 => 
            array (
                'id_reg' => 3870,
                'id_regmf' => 2977,
                'doors' => 5,
            ),
            370 => 
            array (
                'id_reg' => 3871,
                'id_regmf' => 2978,
                'doors' => 4,
            ),
            371 => 
            array (
                'id_reg' => 3872,
                'id_regmf' => 2980,
                'doors' => 3,
            ),
            372 => 
            array (
                'id_reg' => 3873,
                'id_regmf' => 2980,
                'doors' => 5,
            ),
            373 => 
            array (
                'id_reg' => 3874,
                'id_regmf' => 2979,
                'doors' => 3,
            ),
            374 => 
            array (
                'id_reg' => 3875,
                'id_regmf' => 2979,
                'doors' => 5,
            ),
            375 => 
            array (
                'id_reg' => 3876,
                'id_regmf' => 2981,
                'doors' => 5,
            ),
            376 => 
            array (
                'id_reg' => 3877,
                'id_regmf' => 2982,
                'doors' => 4,
            ),
            377 => 
            array (
                'id_reg' => 3878,
                'id_regmf' => 2983,
                'doors' => 4,
            ),
            378 => 
            array (
                'id_reg' => 3879,
                'id_regmf' => 2985,
                'doors' => 4,
            ),
            379 => 
            array (
                'id_reg' => 3880,
                'id_regmf' => 2984,
                'doors' => 4,
            ),
            380 => 
            array (
                'id_reg' => 3881,
                'id_regmf' => 3004,
                'doors' => 2,
            ),
            381 => 
            array (
                'id_reg' => 3882,
                'id_regmf' => 3005,
                'doors' => 2,
            ),
            382 => 
            array (
                'id_reg' => 3883,
                'id_regmf' => 3005,
                'doors' => 3,
            ),
            383 => 
            array (
                'id_reg' => 3884,
                'id_regmf' => 3006,
                'doors' => 4,
            ),
            384 => 
            array (
                'id_reg' => 3885,
                'id_regmf' => 3008,
                'doors' => 3,
            ),
            385 => 
            array (
                'id_reg' => 3886,
                'id_regmf' => 3007,
                'doors' => 3,
            ),
            386 => 
            array (
                'id_reg' => 3887,
                'id_regmf' => 3009,
                'doors' => 2,
            ),
            387 => 
            array (
                'id_reg' => 3888,
                'id_regmf' => 3010,
                'doors' => 2,
            ),
            388 => 
            array (
                'id_reg' => 3889,
                'id_regmf' => 3010,
                'doors' => 5,
            ),
            389 => 
            array (
                'id_reg' => 3890,
                'id_regmf' => 3011,
                'doors' => 2,
            ),
            390 => 
            array (
                'id_reg' => 3891,
                'id_regmf' => 3011,
                'doors' => 4,
            ),
            391 => 
            array (
                'id_reg' => 3892,
                'id_regmf' => 3011,
                'doors' => 5,
            ),
            392 => 
            array (
                'id_reg' => 3893,
                'id_regmf' => 3012,
                'doors' => 2,
            ),
            393 => 
            array (
                'id_reg' => 3894,
                'id_regmf' => 3013,
                'doors' => 2,
            ),
            394 => 
            array (
                'id_reg' => 3895,
                'id_regmf' => 3013,
                'doors' => 4,
            ),
            395 => 
            array (
                'id_reg' => 3896,
                'id_regmf' => 3013,
                'doors' => 5,
            ),
            396 => 
            array (
                'id_reg' => 3897,
                'id_regmf' => 3014,
                'doors' => 2,
            ),
            397 => 
            array (
                'id_reg' => 3898,
                'id_regmf' => 3014,
                'doors' => 5,
            ),
            398 => 
            array (
                'id_reg' => 3899,
                'id_regmf' => 3016,
                'doors' => 5,
            ),
            399 => 
            array (
                'id_reg' => 3900,
                'id_regmf' => 3015,
                'doors' => 5,
            ),
            400 => 
            array (
                'id_reg' => 3901,
                'id_regmf' => 3017,
                'doors' => 2,
            ),
            401 => 
            array (
                'id_reg' => 3902,
                'id_regmf' => 3018,
                'doors' => 4,
            ),
            402 => 
            array (
                'id_reg' => 3903,
                'id_regmf' => 3019,
                'doors' => 4,
            ),
            403 => 
            array (
                'id_reg' => 3904,
                'id_regmf' => 3020,
                'doors' => 5,
            ),
            404 => 
            array (
                'id_reg' => 3905,
                'id_regmf' => 3022,
                'doors' => 5,
            ),
            405 => 
            array (
                'id_reg' => 3906,
                'id_regmf' => 3021,
                'doors' => 5,
            ),
            406 => 
            array (
                'id_reg' => 3907,
                'id_regmf' => 3023,
                'doors' => 4,
            ),
            407 => 
            array (
                'id_reg' => 3908,
                'id_regmf' => 3025,
                'doors' => 2,
            ),
            408 => 
            array (
                'id_reg' => 3909,
                'id_regmf' => 3025,
                'doors' => 3,
            ),
            409 => 
            array (
                'id_reg' => 3910,
                'id_regmf' => 3025,
                'doors' => 4,
            ),
            410 => 
            array (
                'id_reg' => 3911,
                'id_regmf' => 3024,
                'doors' => 2,
            ),
            411 => 
            array (
                'id_reg' => 3912,
                'id_regmf' => 3024,
                'doors' => 3,
            ),
            412 => 
            array (
                'id_reg' => 3913,
                'id_regmf' => 3024,
                'doors' => 4,
            ),
            413 => 
            array (
                'id_reg' => 3914,
                'id_regmf' => 3026,
                'doors' => 2,
            ),
            414 => 
            array (
                'id_reg' => 3915,
                'id_regmf' => 3029,
                'doors' => 4,
            ),
            415 => 
            array (
                'id_reg' => 3916,
                'id_regmf' => 3028,
                'doors' => 4,
            ),
            416 => 
            array (
                'id_reg' => 3917,
                'id_regmf' => 3027,
                'doors' => 4,
            ),
            417 => 
            array (
                'id_reg' => 3918,
                'id_regmf' => 3030,
                'doors' => 4,
            ),
            418 => 
            array (
                'id_reg' => 3919,
                'id_regmf' => 3031,
                'doors' => 3,
            ),
            419 => 
            array (
                'id_reg' => 3920,
                'id_regmf' => 3032,
                'doors' => 3,
            ),
            420 => 
            array (
                'id_reg' => 3921,
                'id_regmf' => 3033,
                'doors' => 2,
            ),
            421 => 
            array (
                'id_reg' => 3922,
                'id_regmf' => 3034,
                'doors' => 4,
            ),
            422 => 
            array (
                'id_reg' => 3923,
                'id_regmf' => 3035,
                'doors' => 5,
            ),
            423 => 
            array (
                'id_reg' => 3924,
                'id_regmf' => 3036,
                'doors' => 5,
            ),
            424 => 
            array (
                'id_reg' => 3925,
                'id_regmf' => 3037,
                'doors' => 5,
            ),
            425 => 
            array (
                'id_reg' => 3926,
                'id_regmf' => 3037,
                'doors' => 6,
            ),
            426 => 
            array (
                'id_reg' => 3927,
                'id_regmf' => 3039,
                'doors' => 5,
            ),
            427 => 
            array (
                'id_reg' => 3928,
                'id_regmf' => 3038,
                'doors' => 5,
            ),
            428 => 
            array (
                'id_reg' => 3929,
                'id_regmf' => 3040,
                'doors' => 2,
            ),
            429 => 
            array (
                'id_reg' => 3930,
                'id_regmf' => 3041,
                'doors' => 5,
            ),
            430 => 
            array (
                'id_reg' => 3931,
                'id_regmf' => 3042,
                'doors' => 5,
            ),
            431 => 
            array (
                'id_reg' => 3932,
                'id_regmf' => 3043,
                'doors' => 3,
            ),
            432 => 
            array (
                'id_reg' => 3933,
                'id_regmf' => 3044,
                'doors' => 3,
            ),
            433 => 
            array (
                'id_reg' => 3934,
                'id_regmf' => 3045,
                'doors' => 4,
            ),
            434 => 
            array (
                'id_reg' => 3935,
                'id_regmf' => 3047,
                'doors' => 5,
            ),
            435 => 
            array (
                'id_reg' => 3936,
                'id_regmf' => 3046,
                'doors' => 5,
            ),
            436 => 
            array (
                'id_reg' => 3937,
                'id_regmf' => 3048,
                'doors' => 5,
            ),
            437 => 
            array (
                'id_reg' => 3938,
                'id_regmf' => 3049,
                'doors' => 5,
            ),
            438 => 
            array (
                'id_reg' => 3939,
                'id_regmf' => 3050,
                'doors' => 2,
            ),
            439 => 
            array (
                'id_reg' => 3940,
                'id_regmf' => 3050,
                'doors' => 4,
            ),
            440 => 
            array (
                'id_reg' => 3941,
                'id_regmf' => 3052,
                'doors' => 2,
            ),
            441 => 
            array (
                'id_reg' => 3942,
                'id_regmf' => 3051,
                'doors' => 2,
            ),
            442 => 
            array (
                'id_reg' => 3943,
                'id_regmf' => 3053,
                'doors' => 4,
            ),
            443 => 
            array (
                'id_reg' => 3944,
                'id_regmf' => 3054,
                'doors' => 4,
            ),
            444 => 
            array (
                'id_reg' => 3945,
                'id_regmf' => 3055,
                'doors' => 2,
            ),
            445 => 
            array (
                'id_reg' => 3946,
                'id_regmf' => 3056,
                'doors' => 2,
            ),
            446 => 
            array (
                'id_reg' => 3947,
                'id_regmf' => 3056,
                'doors' => 4,
            ),
            447 => 
            array (
                'id_reg' => 3948,
                'id_regmf' => 3056,
                'doors' => 5,
            ),
            448 => 
            array (
                'id_reg' => 3949,
                'id_regmf' => 3058,
                'doors' => 5,
            ),
            449 => 
            array (
                'id_reg' => 3950,
                'id_regmf' => 3057,
                'doors' => 2,
            ),
            450 => 
            array (
                'id_reg' => 3951,
                'id_regmf' => 3057,
                'doors' => 3,
            ),
            451 => 
            array (
                'id_reg' => 3952,
                'id_regmf' => 3057,
                'doors' => 4,
            ),
            452 => 
            array (
                'id_reg' => 3953,
                'id_regmf' => 3057,
                'doors' => 5,
            ),
            453 => 
            array (
                'id_reg' => 3954,
                'id_regmf' => 3057,
                'doors' => 6,
            ),
            454 => 
            array (
                'id_reg' => 3955,
                'id_regmf' => 3059,
                'doors' => 2,
            ),
            455 => 
            array (
                'id_reg' => 3956,
                'id_regmf' => 3059,
                'doors' => 4,
            ),
            456 => 
            array (
                'id_reg' => 3957,
                'id_regmf' => 3061,
                'doors' => 2,
            ),
            457 => 
            array (
                'id_reg' => 3958,
                'id_regmf' => 3060,
                'doors' => 2,
            ),
            458 => 
            array (
                'id_reg' => 3959,
                'id_regmf' => 3063,
                'doors' => 5,
            ),
            459 => 
            array (
                'id_reg' => 3960,
                'id_regmf' => 3064,
                'doors' => 5,
            ),
            460 => 
            array (
                'id_reg' => 3961,
                'id_regmf' => 3062,
                'doors' => 5,
            ),
            461 => 
            array (
                'id_reg' => 3962,
                'id_regmf' => 3065,
                'doors' => 2,
            ),
            462 => 
            array (
                'id_reg' => 3963,
                'id_regmf' => 3066,
                'doors' => 5,
            ),
            463 => 
            array (
                'id_reg' => 3964,
                'id_regmf' => 3067,
                'doors' => 5,
            ),
            464 => 
            array (
                'id_reg' => 3965,
                'id_regmf' => 3068,
                'doors' => 4,
            ),
            465 => 
            array (
                'id_reg' => 3966,
                'id_regmf' => 3069,
                'doors' => 4,
            ),
            466 => 
            array (
                'id_reg' => 3967,
                'id_regmf' => 3070,
                'doors' => 4,
            ),
            467 => 
            array (
                'id_reg' => 3968,
                'id_regmf' => 3071,
                'doors' => 4,
            ),
            468 => 
            array (
                'id_reg' => 3969,
                'id_regmf' => 3072,
                'doors' => 2,
            ),
            469 => 
            array (
                'id_reg' => 3970,
                'id_regmf' => 3073,
                'doors' => 4,
            ),
            470 => 
            array (
                'id_reg' => 3971,
                'id_regmf' => 3074,
                'doors' => 4,
            ),
            471 => 
            array (
                'id_reg' => 3972,
                'id_regmf' => 3075,
                'doors' => 2,
            ),
            472 => 
            array (
                'id_reg' => 3973,
                'id_regmf' => 3076,
                'doors' => 5,
            ),
            473 => 
            array (
                'id_reg' => 3974,
                'id_regmf' => 3077,
                'doors' => 5,
            ),
            474 => 
            array (
                'id_reg' => 3975,
                'id_regmf' => 3078,
                'doors' => 2,
            ),
            475 => 
            array (
                'id_reg' => 3976,
                'id_regmf' => 3080,
                'doors' => 3,
            ),
            476 => 
            array (
                'id_reg' => 3977,
                'id_regmf' => 3080,
                'doors' => 5,
            ),
            477 => 
            array (
                'id_reg' => 3978,
                'id_regmf' => 3079,
                'doors' => 3,
            ),
            478 => 
            array (
                'id_reg' => 3979,
                'id_regmf' => 3079,
                'doors' => 5,
            ),
            479 => 
            array (
                'id_reg' => 3980,
                'id_regmf' => 3082,
                'doors' => 4,
            ),
            480 => 
            array (
                'id_reg' => 3981,
                'id_regmf' => 3081,
                'doors' => 4,
            ),
            481 => 
            array (
                'id_reg' => 3982,
                'id_regmf' => 3084,
                'doors' => 4,
            ),
            482 => 
            array (
                'id_reg' => 3983,
                'id_regmf' => 3083,
                'doors' => 4,
            ),
            483 => 
            array (
                'id_reg' => 3984,
                'id_regmf' => 3086,
                'doors' => 5,
            ),
            484 => 
            array (
                'id_reg' => 3985,
                'id_regmf' => 3085,
                'doors' => 5,
            ),
            485 => 
            array (
                'id_reg' => 3986,
                'id_regmf' => 3088,
                'doors' => 4,
            ),
            486 => 
            array (
                'id_reg' => 3987,
                'id_regmf' => 3087,
                'doors' => 4,
            ),
            487 => 
            array (
                'id_reg' => 3988,
                'id_regmf' => 3089,
                'doors' => 2,
            ),
            488 => 
            array (
                'id_reg' => 3989,
                'id_regmf' => 3090,
                'doors' => 2,
            ),
            489 => 
            array (
                'id_reg' => 3990,
                'id_regmf' => 3091,
                'doors' => 0,
            ),
            490 => 
            array (
                'id_reg' => 3991,
                'id_regmf' => 3091,
                'doors' => 2,
            ),
            491 => 
            array (
                'id_reg' => 3992,
                'id_regmf' => 3092,
                'doors' => 2,
            ),
            492 => 
            array (
                'id_reg' => 3993,
                'id_regmf' => 3093,
                'doors' => 5,
            ),
            493 => 
            array (
                'id_reg' => 3994,
                'id_regmf' => 3094,
                'doors' => 5,
            ),
            494 => 
            array (
                'id_reg' => 3995,
                'id_regmf' => 3095,
                'doors' => 4,
            ),
            495 => 
            array (
                'id_reg' => 3996,
                'id_regmf' => 3096,
                'doors' => 2,
            ),
            496 => 
            array (
                'id_reg' => 3997,
                'id_regmf' => 3097,
                'doors' => 5,
            ),
            497 => 
            array (
                'id_reg' => 3998,
                'id_regmf' => 3098,
                'doors' => 0,
            ),
            498 => 
            array (
                'id_reg' => 3999,
                'id_regmf' => 3099,
                'doors' => 0,
            ),
            499 => 
            array (
                'id_reg' => 4000,
                'id_regmf' => 3100,
                'doors' => 3,
            ),
        ));
        \DB::table('model_fuel_numdoors')->insert(array (
            0 => 
            array (
                'id_reg' => 4001,
                'id_regmf' => 3101,
                'doors' => 3,
            ),
            1 => 
            array (
                'id_reg' => 4002,
                'id_regmf' => 3102,
                'doors' => 3,
            ),
            2 => 
            array (
                'id_reg' => 4003,
                'id_regmf' => 3103,
                'doors' => 3,
            ),
            3 => 
            array (
                'id_reg' => 4004,
                'id_regmf' => 3104,
                'doors' => 2,
            ),
            4 => 
            array (
                'id_reg' => 4005,
                'id_regmf' => 3105,
                'doors' => 0,
            ),
            5 => 
            array (
                'id_reg' => 4006,
                'id_regmf' => 3106,
                'doors' => 2,
            ),
            6 => 
            array (
                'id_reg' => 4007,
                'id_regmf' => 3107,
                'doors' => 3,
            ),
            7 => 
            array (
                'id_reg' => 4008,
                'id_regmf' => 3108,
                'doors' => 3,
            ),
            8 => 
            array (
                'id_reg' => 4009,
                'id_regmf' => 3109,
                'doors' => 4,
            ),
            9 => 
            array (
                'id_reg' => 4010,
                'id_regmf' => 3110,
                'doors' => 4,
            ),
            10 => 
            array (
                'id_reg' => 4011,
                'id_regmf' => 3111,
                'doors' => 4,
            ),
            11 => 
            array (
                'id_reg' => 4012,
                'id_regmf' => 3112,
                'doors' => 2,
            ),
            12 => 
            array (
                'id_reg' => 4013,
                'id_regmf' => 3113,
                'doors' => 2,
            ),
            13 => 
            array (
                'id_reg' => 4014,
                'id_regmf' => 3113,
                'doors' => 4,
            ),
            14 => 
            array (
                'id_reg' => 4015,
                'id_regmf' => 3114,
                'doors' => 2,
            ),
            15 => 
            array (
                'id_reg' => 4016,
                'id_regmf' => 3115,
                'doors' => 2,
            ),
            16 => 
            array (
                'id_reg' => 4017,
                'id_regmf' => 3116,
                'doors' => 4,
            ),
            17 => 
            array (
                'id_reg' => 4018,
                'id_regmf' => 3117,
                'doors' => 4,
            ),
            18 => 
            array (
                'id_reg' => 4019,
                'id_regmf' => 3118,
                'doors' => 4,
            ),
            19 => 
            array (
                'id_reg' => 4020,
                'id_regmf' => 3119,
                'doors' => 2,
            ),
            20 => 
            array (
                'id_reg' => 4021,
                'id_regmf' => 3119,
                'doors' => 4,
            ),
            21 => 
            array (
                'id_reg' => 4022,
                'id_regmf' => 3120,
                'doors' => 4,
            ),
            22 => 
            array (
                'id_reg' => 4023,
                'id_regmf' => 3121,
                'doors' => 4,
            ),
            23 => 
            array (
                'id_reg' => 4024,
                'id_regmf' => 3122,
                'doors' => 4,
            ),
            24 => 
            array (
                'id_reg' => 4025,
                'id_regmf' => 3123,
                'doors' => 4,
            ),
            25 => 
            array (
                'id_reg' => 4026,
                'id_regmf' => 3124,
                'doors' => 2,
            ),
            26 => 
            array (
                'id_reg' => 4027,
                'id_regmf' => 3124,
                'doors' => 4,
            ),
            27 => 
            array (
                'id_reg' => 4028,
                'id_regmf' => 3125,
                'doors' => 4,
            ),
            28 => 
            array (
                'id_reg' => 4029,
                'id_regmf' => 3126,
                'doors' => 4,
            ),
            29 => 
            array (
                'id_reg' => 4030,
                'id_regmf' => 3127,
                'doors' => 4,
            ),
            30 => 
            array (
                'id_reg' => 4031,
                'id_regmf' => 3128,
                'doors' => 2,
            ),
            31 => 
            array (
                'id_reg' => 4032,
                'id_regmf' => 3128,
                'doors' => 4,
            ),
            32 => 
            array (
                'id_reg' => 4033,
                'id_regmf' => 3129,
                'doors' => 0,
            ),
            33 => 
            array (
                'id_reg' => 4034,
                'id_regmf' => 3136,
                'doors' => 4,
            ),
            34 => 
            array (
                'id_reg' => 4035,
                'id_regmf' => 3137,
                'doors' => 3,
            ),
            35 => 
            array (
                'id_reg' => 4036,
                'id_regmf' => 3137,
                'doors' => 5,
            ),
            36 => 
            array (
                'id_reg' => 4037,
                'id_regmf' => 3138,
                'doors' => 3,
            ),
            37 => 
            array (
                'id_reg' => 4038,
                'id_regmf' => 3138,
                'doors' => 5,
            ),
            38 => 
            array (
                'id_reg' => 4039,
                'id_regmf' => 3139,
                'doors' => 2,
            ),
            39 => 
            array (
                'id_reg' => 4040,
                'id_regmf' => 3139,
                'doors' => 3,
            ),
            40 => 
            array (
                'id_reg' => 4041,
                'id_regmf' => 3139,
                'doors' => 5,
            ),
            41 => 
            array (
                'id_reg' => 4042,
                'id_regmf' => 3140,
                'doors' => 5,
            ),
            42 => 
            array (
                'id_reg' => 4043,
                'id_regmf' => 3141,
                'doors' => 3,
            ),
            43 => 
            array (
                'id_reg' => 4044,
                'id_regmf' => 3164,
                'doors' => 4,
            ),
            44 => 
            array (
                'id_reg' => 4045,
                'id_regmf' => 3142,
                'doors' => 5,
            ),
            45 => 
            array (
                'id_reg' => 4046,
                'id_regmf' => 3143,
                'doors' => 4,
            ),
            46 => 
            array (
                'id_reg' => 4047,
                'id_regmf' => 3144,
                'doors' => 3,
            ),
            47 => 
            array (
                'id_reg' => 4048,
                'id_regmf' => 3144,
                'doors' => 5,
            ),
            48 => 
            array (
                'id_reg' => 4049,
                'id_regmf' => 3145,
                'doors' => 2,
            ),
            49 => 
            array (
                'id_reg' => 4050,
                'id_regmf' => 3145,
                'doors' => 3,
            ),
            50 => 
            array (
                'id_reg' => 4051,
                'id_regmf' => 3145,
                'doors' => 4,
            ),
            51 => 
            array (
                'id_reg' => 4052,
                'id_regmf' => 3145,
                'doors' => 5,
            ),
            52 => 
            array (
                'id_reg' => 4053,
                'id_regmf' => 3146,
                'doors' => 5,
            ),
            53 => 
            array (
                'id_reg' => 4054,
                'id_regmf' => 3147,
                'doors' => 2,
            ),
            54 => 
            array (
                'id_reg' => 4055,
                'id_regmf' => 3148,
                'doors' => 3,
            ),
            55 => 
            array (
                'id_reg' => 4056,
                'id_regmf' => 3148,
                'doors' => 5,
            ),
            56 => 
            array (
                'id_reg' => 4057,
                'id_regmf' => 3149,
                'doors' => 2,
            ),
            57 => 
            array (
                'id_reg' => 4058,
                'id_regmf' => 3149,
                'doors' => 3,
            ),
            58 => 
            array (
                'id_reg' => 4059,
                'id_regmf' => 3149,
                'doors' => 5,
            ),
            59 => 
            array (
                'id_reg' => 4060,
                'id_regmf' => 3165,
                'doors' => 4,
            ),
            60 => 
            array (
                'id_reg' => 4061,
                'id_regmf' => 3166,
                'doors' => 5,
            ),
            61 => 
            array (
                'id_reg' => 4062,
                'id_regmf' => 3130,
                'doors' => 3,
            ),
            62 => 
            array (
                'id_reg' => 4063,
                'id_regmf' => 3130,
                'doors' => 5,
            ),
            63 => 
            array (
                'id_reg' => 4064,
                'id_regmf' => 3131,
                'doors' => 3,
            ),
            64 => 
            array (
                'id_reg' => 4065,
                'id_regmf' => 3131,
                'doors' => 5,
            ),
            65 => 
            array (
                'id_reg' => 4066,
                'id_regmf' => 3167,
                'doors' => 5,
            ),
            66 => 
            array (
                'id_reg' => 4067,
                'id_regmf' => 3168,
                'doors' => 4,
            ),
            67 => 
            array (
                'id_reg' => 4068,
                'id_regmf' => 3168,
                'doors' => 5,
            ),
            68 => 
            array (
                'id_reg' => 4069,
                'id_regmf' => 3150,
                'doors' => 4,
            ),
            69 => 
            array (
                'id_reg' => 4070,
                'id_regmf' => 3150,
                'doors' => 5,
            ),
            70 => 
            array (
                'id_reg' => 4071,
                'id_regmf' => 3151,
                'doors' => 4,
            ),
            71 => 
            array (
                'id_reg' => 4072,
                'id_regmf' => 3151,
                'doors' => 5,
            ),
            72 => 
            array (
                'id_reg' => 4073,
                'id_regmf' => 3152,
                'doors' => 4,
            ),
            73 => 
            array (
                'id_reg' => 4074,
                'id_regmf' => 3153,
                'doors' => 5,
            ),
            74 => 
            array (
                'id_reg' => 4075,
                'id_regmf' => 3154,
                'doors' => 4,
            ),
            75 => 
            array (
                'id_reg' => 4076,
                'id_regmf' => 3154,
                'doors' => 5,
            ),
            76 => 
            array (
                'id_reg' => 4077,
                'id_regmf' => 3155,
                'doors' => 4,
            ),
            77 => 
            array (
                'id_reg' => 4078,
                'id_regmf' => 3155,
                'doors' => 5,
            ),
            78 => 
            array (
                'id_reg' => 4079,
                'id_regmf' => 3133,
                'doors' => 4,
            ),
            79 => 
            array (
                'id_reg' => 4080,
                'id_regmf' => 3133,
                'doors' => 5,
            ),
            80 => 
            array (
                'id_reg' => 4081,
                'id_regmf' => 3132,
                'doors' => 4,
            ),
            81 => 
            array (
                'id_reg' => 4082,
                'id_regmf' => 3132,
                'doors' => 5,
            ),
            82 => 
            array (
                'id_reg' => 4083,
                'id_regmf' => 3156,
                'doors' => 4,
            ),
            83 => 
            array (
                'id_reg' => 4084,
                'id_regmf' => 3158,
                'doors' => 4,
            ),
            84 => 
            array (
                'id_reg' => 4085,
                'id_regmf' => 3157,
                'doors' => 4,
            ),
            85 => 
            array (
                'id_reg' => 4086,
                'id_regmf' => 3159,
                'doors' => 4,
            ),
            86 => 
            array (
                'id_reg' => 4087,
                'id_regmf' => 3135,
                'doors' => 4,
            ),
            87 => 
            array (
                'id_reg' => 4088,
                'id_regmf' => 3135,
                'doors' => 5,
            ),
            88 => 
            array (
                'id_reg' => 4089,
                'id_regmf' => 3134,
                'doors' => 4,
            ),
            89 => 
            array (
                'id_reg' => 4090,
                'id_regmf' => 3134,
                'doors' => 5,
            ),
            90 => 
            array (
                'id_reg' => 4091,
                'id_regmf' => 3160,
                'doors' => 4,
            ),
            91 => 
            array (
                'id_reg' => 4092,
                'id_regmf' => 3160,
                'doors' => 5,
            ),
            92 => 
            array (
                'id_reg' => 4093,
                'id_regmf' => 3161,
                'doors' => 4,
            ),
            93 => 
            array (
                'id_reg' => 4094,
                'id_regmf' => 3162,
                'doors' => 2,
            ),
            94 => 
            array (
                'id_reg' => 4095,
                'id_regmf' => 3162,
                'doors' => 4,
            ),
            95 => 
            array (
                'id_reg' => 4096,
                'id_regmf' => 3163,
                'doors' => 2,
            ),
            96 => 
            array (
                'id_reg' => 4097,
                'id_regmf' => 3163,
                'doors' => 4,
            ),
            97 => 
            array (
                'id_reg' => 4098,
                'id_regmf' => 3163,
                'doors' => 5,
            ),
            98 => 
            array (
                'id_reg' => 4099,
                'id_regmf' => 3169,
                'doors' => 5,
            ),
            99 => 
            array (
                'id_reg' => 4100,
                'id_regmf' => 3170,
                'doors' => 4,
            ),
            100 => 
            array (
                'id_reg' => 4101,
                'id_regmf' => 3171,
                'doors' => 5,
            ),
            101 => 
            array (
                'id_reg' => 4102,
                'id_regmf' => 3172,
                'doors' => 3,
            ),
            102 => 
            array (
                'id_reg' => 4103,
                'id_regmf' => 3172,
                'doors' => 5,
            ),
            103 => 
            array (
                'id_reg' => 4104,
                'id_regmf' => 3173,
                'doors' => 2,
            ),
            104 => 
            array (
                'id_reg' => 4105,
                'id_regmf' => 3174,
                'doors' => 2,
            ),
            105 => 
            array (
                'id_reg' => 4106,
                'id_regmf' => 3175,
                'doors' => 3,
            ),
            106 => 
            array (
                'id_reg' => 4107,
                'id_regmf' => 3175,
                'doors' => 5,
            ),
            107 => 
            array (
                'id_reg' => 4108,
                'id_regmf' => 3176,
                'doors' => 3,
            ),
            108 => 
            array (
                'id_reg' => 4109,
                'id_regmf' => 3176,
                'doors' => 5,
            ),
            109 => 
            array (
                'id_reg' => 4110,
                'id_regmf' => 3177,
                'doors' => 4,
            ),
            110 => 
            array (
                'id_reg' => 4111,
                'id_regmf' => 3178,
                'doors' => 4,
            ),
            111 => 
            array (
                'id_reg' => 4112,
                'id_regmf' => 3179,
                'doors' => 4,
            ),
            112 => 
            array (
                'id_reg' => 4113,
                'id_regmf' => 3179,
                'doors' => 5,
            ),
            113 => 
            array (
                'id_reg' => 4114,
                'id_regmf' => 3180,
                'doors' => 4,
            ),
            114 => 
            array (
                'id_reg' => 4115,
                'id_regmf' => 3180,
                'doors' => 5,
            ),
            115 => 
            array (
                'id_reg' => 4116,
                'id_regmf' => 3181,
                'doors' => 2,
            ),
            116 => 
            array (
                'id_reg' => 4117,
                'id_regmf' => 3182,
                'doors' => 0,
            ),
            117 => 
            array (
                'id_reg' => 4118,
                'id_regmf' => 3182,
                'doors' => 2,
            ),
            118 => 
            array (
                'id_reg' => 4119,
                'id_regmf' => 3183,
                'doors' => 4,
            ),
            119 => 
            array (
                'id_reg' => 4120,
                'id_regmf' => 3184,
                'doors' => 4,
            ),
            120 => 
            array (
                'id_reg' => 4121,
                'id_regmf' => 3184,
                'doors' => 5,
            ),
            121 => 
            array (
                'id_reg' => 4122,
                'id_regmf' => 3185,
                'doors' => 4,
            ),
            122 => 
            array (
                'id_reg' => 4123,
                'id_regmf' => 3186,
                'doors' => 4,
            ),
            123 => 
            array (
                'id_reg' => 4124,
                'id_regmf' => 3188,
                'doors' => 3,
            ),
            124 => 
            array (
                'id_reg' => 4125,
                'id_regmf' => 3188,
                'doors' => 5,
            ),
            125 => 
            array (
                'id_reg' => 4126,
                'id_regmf' => 3187,
                'doors' => 3,
            ),
            126 => 
            array (
                'id_reg' => 4127,
                'id_regmf' => 3187,
                'doors' => 5,
            ),
            127 => 
            array (
                'id_reg' => 4128,
                'id_regmf' => 3189,
                'doors' => 2,
            ),
            128 => 
            array (
                'id_reg' => 4129,
                'id_regmf' => 3189,
                'doors' => 4,
            ),
            129 => 
            array (
                'id_reg' => 4130,
                'id_regmf' => 3189,
                'doors' => 5,
            ),
            130 => 
            array (
                'id_reg' => 4131,
                'id_regmf' => 3191,
                'doors' => 2,
            ),
            131 => 
            array (
                'id_reg' => 4132,
                'id_regmf' => 3191,
                'doors' => 3,
            ),
            132 => 
            array (
                'id_reg' => 4133,
                'id_regmf' => 3191,
                'doors' => 4,
            ),
            133 => 
            array (
                'id_reg' => 4134,
                'id_regmf' => 3191,
                'doors' => 5,
            ),
            134 => 
            array (
                'id_reg' => 4135,
                'id_regmf' => 3190,
                'doors' => 2,
            ),
            135 => 
            array (
                'id_reg' => 4136,
                'id_regmf' => 3190,
                'doors' => 3,
            ),
            136 => 
            array (
                'id_reg' => 4137,
                'id_regmf' => 3190,
                'doors' => 4,
            ),
            137 => 
            array (
                'id_reg' => 4138,
                'id_regmf' => 3190,
                'doors' => 5,
            ),
            138 => 
            array (
                'id_reg' => 4139,
                'id_regmf' => 3194,
                'doors' => 5,
            ),
            139 => 
            array (
                'id_reg' => 4140,
                'id_regmf' => 3193,
                'doors' => 5,
            ),
            140 => 
            array (
                'id_reg' => 4141,
                'id_regmf' => 3192,
                'doors' => 5,
            ),
            141 => 
            array (
                'id_reg' => 4142,
                'id_regmf' => 3195,
                'doors' => 5,
            ),
            142 => 
            array (
                'id_reg' => 4143,
                'id_regmf' => 3196,
                'doors' => 4,
            ),
            143 => 
            array (
                'id_reg' => 4144,
                'id_regmf' => 3196,
                'doors' => 5,
            ),
            144 => 
            array (
                'id_reg' => 4145,
                'id_regmf' => 3197,
                'doors' => 4,
            ),
            145 => 
            array (
                'id_reg' => 4146,
                'id_regmf' => 3197,
                'doors' => 5,
            ),
            146 => 
            array (
                'id_reg' => 4147,
                'id_regmf' => 3198,
                'doors' => 4,
            ),
            147 => 
            array (
                'id_reg' => 4148,
                'id_regmf' => 3198,
                'doors' => 5,
            ),
            148 => 
            array (
                'id_reg' => 4149,
                'id_regmf' => 3199,
                'doors' => 5,
            ),
            149 => 
            array (
                'id_reg' => 4150,
                'id_regmf' => 3203,
                'doors' => 2,
            ),
            150 => 
            array (
                'id_reg' => 4151,
                'id_regmf' => 3203,
                'doors' => 3,
            ),
            151 => 
            array (
                'id_reg' => 4152,
                'id_regmf' => 3203,
                'doors' => 4,
            ),
            152 => 
            array (
                'id_reg' => 4153,
                'id_regmf' => 3203,
                'doors' => 5,
            ),
            153 => 
            array (
                'id_reg' => 4154,
                'id_regmf' => 3204,
                'doors' => 4,
            ),
            154 => 
            array (
                'id_reg' => 4155,
                'id_regmf' => 3204,
                'doors' => 5,
            ),
            155 => 
            array (
                'id_reg' => 4156,
                'id_regmf' => 3200,
                'doors' => 2,
            ),
            156 => 
            array (
                'id_reg' => 4157,
                'id_regmf' => 3201,
                'doors' => 2,
            ),
            157 => 
            array (
                'id_reg' => 4158,
                'id_regmf' => 3202,
                'doors' => 3,
            ),
            158 => 
            array (
                'id_reg' => 4159,
                'id_regmf' => 3202,
                'doors' => 5,
            ),
            159 => 
            array (
                'id_reg' => 4160,
                'id_regmf' => 3205,
                'doors' => 2,
            ),
            160 => 
            array (
                'id_reg' => 4161,
                'id_regmf' => 3206,
                'doors' => 0,
            ),
            161 => 
            array (
                'id_reg' => 4162,
                'id_regmf' => 3207,
                'doors' => 2,
            ),
            162 => 
            array (
                'id_reg' => 4163,
                'id_regmf' => 3208,
                'doors' => 2,
            ),
            163 => 
            array (
                'id_reg' => 4164,
                'id_regmf' => 3209,
                'doors' => 0,
            ),
            164 => 
            array (
                'id_reg' => 4165,
                'id_regmf' => 3210,
                'doors' => 4,
            ),
            165 => 
            array (
                'id_reg' => 4166,
                'id_regmf' => 3211,
                'doors' => 2,
            ),
            166 => 
            array (
                'id_reg' => 4167,
                'id_regmf' => 3212,
                'doors' => 2,
            ),
            167 => 
            array (
                'id_reg' => 4168,
                'id_regmf' => 3212,
                'doors' => 3,
            ),
            168 => 
            array (
                'id_reg' => 4169,
                'id_regmf' => 3212,
                'doors' => 5,
            ),
            169 => 
            array (
                'id_reg' => 4170,
                'id_regmf' => 3217,
                'doors' => 2,
            ),
            170 => 
            array (
                'id_reg' => 4171,
                'id_regmf' => 3217,
                'doors' => 4,
            ),
            171 => 
            array (
                'id_reg' => 4172,
                'id_regmf' => 3218,
                'doors' => 3,
            ),
            172 => 
            array (
                'id_reg' => 4173,
                'id_regmf' => 3218,
                'doors' => 4,
            ),
            173 => 
            array (
                'id_reg' => 4174,
                'id_regmf' => 3218,
                'doors' => 5,
            ),
            174 => 
            array (
                'id_reg' => 4175,
                'id_regmf' => 3213,
                'doors' => 3,
            ),
            175 => 
            array (
                'id_reg' => 4176,
                'id_regmf' => 3214,
                'doors' => 3,
            ),
            176 => 
            array (
                'id_reg' => 4177,
                'id_regmf' => 3216,
                'doors' => 5,
            ),
            177 => 
            array (
                'id_reg' => 4178,
                'id_regmf' => 3215,
                'doors' => 5,
            ),
            178 => 
            array (
                'id_reg' => 4179,
                'id_regmf' => 3219,
                'doors' => 5,
            ),
            179 => 
            array (
                'id_reg' => 4180,
                'id_regmf' => 3220,
                'doors' => 2,
            ),
            180 => 
            array (
                'id_reg' => 4181,
                'id_regmf' => 3220,
                'doors' => 5,
            ),
            181 => 
            array (
                'id_reg' => 4182,
                'id_regmf' => 3222,
                'doors' => 2,
            ),
            182 => 
            array (
                'id_reg' => 4183,
                'id_regmf' => 3222,
                'doors' => 3,
            ),
            183 => 
            array (
                'id_reg' => 4184,
                'id_regmf' => 3222,
                'doors' => 5,
            ),
            184 => 
            array (
                'id_reg' => 4185,
                'id_regmf' => 3221,
                'doors' => 2,
            ),
            185 => 
            array (
                'id_reg' => 4186,
                'id_regmf' => 3221,
                'doors' => 3,
            ),
            186 => 
            array (
                'id_reg' => 4187,
                'id_regmf' => 3221,
                'doors' => 4,
            ),
            187 => 
            array (
                'id_reg' => 4188,
                'id_regmf' => 3221,
                'doors' => 5,
            ),
            188 => 
            array (
                'id_reg' => 4189,
                'id_regmf' => 3223,
                'doors' => 5,
            ),
            189 => 
            array (
                'id_reg' => 4190,
                'id_regmf' => 3224,
                'doors' => 4,
            ),
            190 => 
            array (
                'id_reg' => 4191,
                'id_regmf' => 3225,
                'doors' => 2,
            ),
            191 => 
            array (
                'id_reg' => 4192,
                'id_regmf' => 3226,
                'doors' => 4,
            ),
            192 => 
            array (
                'id_reg' => 4193,
                'id_regmf' => 3227,
                'doors' => 4,
            ),
            193 => 
            array (
                'id_reg' => 4194,
                'id_regmf' => 3228,
                'doors' => 2,
            ),
            194 => 
            array (
                'id_reg' => 4195,
                'id_regmf' => 3229,
                'doors' => 5,
            ),
            195 => 
            array (
                'id_reg' => 4196,
                'id_regmf' => 3230,
                'doors' => 0,
            ),
            196 => 
            array (
                'id_reg' => 4197,
                'id_regmf' => 3230,
                'doors' => 2,
            ),
            197 => 
            array (
                'id_reg' => 4198,
                'id_regmf' => 3230,
                'doors' => 5,
            ),
            198 => 
            array (
                'id_reg' => 4199,
                'id_regmf' => 3231,
                'doors' => 0,
            ),
            199 => 
            array (
                'id_reg' => 4200,
                'id_regmf' => 3231,
                'doors' => 2,
            ),
            200 => 
            array (
                'id_reg' => 4201,
                'id_regmf' => 3232,
                'doors' => 0,
            ),
            201 => 
            array (
                'id_reg' => 4202,
                'id_regmf' => 3233,
                'doors' => 2,
            ),
            202 => 
            array (
                'id_reg' => 4203,
                'id_regmf' => 3234,
                'doors' => 0,
            ),
            203 => 
            array (
                'id_reg' => 4204,
                'id_regmf' => 3235,
                'doors' => 2,
            ),
            204 => 
            array (
                'id_reg' => 4205,
                'id_regmf' => 3248,
                'doors' => 2,
            ),
            205 => 
            array (
                'id_reg' => 4206,
                'id_regmf' => 3237,
                'doors' => 2,
            ),
            206 => 
            array (
                'id_reg' => 4207,
                'id_regmf' => 3237,
                'doors' => 4,
            ),
            207 => 
            array (
                'id_reg' => 4208,
                'id_regmf' => 3237,
                'doors' => 5,
            ),
            208 => 
            array (
                'id_reg' => 4209,
                'id_regmf' => 3236,
                'doors' => 5,
            ),
            209 => 
            array (
                'id_reg' => 4210,
                'id_regmf' => 3238,
                'doors' => 2,
            ),
            210 => 
            array (
                'id_reg' => 4211,
                'id_regmf' => 3238,
                'doors' => 3,
            ),
            211 => 
            array (
                'id_reg' => 4212,
                'id_regmf' => 3238,
                'doors' => 4,
            ),
            212 => 
            array (
                'id_reg' => 4213,
                'id_regmf' => 3238,
                'doors' => 5,
            ),
            213 => 
            array (
                'id_reg' => 4214,
                'id_regmf' => 3239,
                'doors' => 3,
            ),
            214 => 
            array (
                'id_reg' => 4215,
                'id_regmf' => 3240,
                'doors' => 4,
            ),
            215 => 
            array (
                'id_reg' => 4216,
                'id_regmf' => 3240,
                'doors' => 5,
            ),
            216 => 
            array (
                'id_reg' => 4217,
                'id_regmf' => 3241,
                'doors' => 4,
            ),
            217 => 
            array (
                'id_reg' => 4218,
                'id_regmf' => 3241,
                'doors' => 5,
            ),
            218 => 
            array (
                'id_reg' => 4219,
                'id_regmf' => 3242,
                'doors' => 4,
            ),
            219 => 
            array (
                'id_reg' => 4220,
                'id_regmf' => 3243,
                'doors' => 4,
            ),
            220 => 
            array (
                'id_reg' => 4221,
                'id_regmf' => 3244,
                'doors' => 3,
            ),
            221 => 
            array (
                'id_reg' => 4222,
                'id_regmf' => 3249,
                'doors' => 4,
            ),
            222 => 
            array (
                'id_reg' => 4223,
                'id_regmf' => 3249,
                'doors' => 5,
            ),
            223 => 
            array (
                'id_reg' => 4224,
                'id_regmf' => 3250,
                'doors' => 2,
            ),
            224 => 
            array (
                'id_reg' => 4225,
                'id_regmf' => 3250,
                'doors' => 4,
            ),
            225 => 
            array (
                'id_reg' => 4226,
                'id_regmf' => 3250,
                'doors' => 5,
            ),
            226 => 
            array (
                'id_reg' => 4227,
                'id_regmf' => 3251,
                'doors' => 4,
            ),
            227 => 
            array (
                'id_reg' => 4228,
                'id_regmf' => 3252,
                'doors' => 4,
            ),
            228 => 
            array (
                'id_reg' => 4229,
                'id_regmf' => 3253,
                'doors' => 4,
            ),
            229 => 
            array (
                'id_reg' => 4230,
                'id_regmf' => 3245,
                'doors' => 2,
            ),
            230 => 
            array (
                'id_reg' => 4231,
                'id_regmf' => 3246,
                'doors' => 4,
            ),
            231 => 
            array (
                'id_reg' => 4232,
                'id_regmf' => 3247,
                'doors' => 2,
            ),
            232 => 
            array (
                'id_reg' => 4233,
                'id_regmf' => 3247,
                'doors' => 4,
            ),
            233 => 
            array (
                'id_reg' => 4234,
                'id_regmf' => 3254,
                'doors' => 3,
            ),
            234 => 
            array (
                'id_reg' => 4235,
                'id_regmf' => 3256,
                'doors' => 5,
            ),
            235 => 
            array (
                'id_reg' => 4236,
                'id_regmf' => 3255,
                'doors' => 5,
            ),
            236 => 
            array (
                'id_reg' => 4237,
                'id_regmf' => 3257,
                'doors' => 5,
            ),
            237 => 
            array (
                'id_reg' => 4238,
                'id_regmf' => 3258,
                'doors' => 5,
            ),
            238 => 
            array (
                'id_reg' => 4239,
                'id_regmf' => 3259,
                'doors' => 3,
            ),
            239 => 
            array (
                'id_reg' => 4240,
                'id_regmf' => 3259,
                'doors' => 5,
            ),
            240 => 
            array (
                'id_reg' => 4241,
                'id_regmf' => 3260,
                'doors' => 2,
            ),
            241 => 
            array (
                'id_reg' => 4242,
                'id_regmf' => 3260,
                'doors' => 3,
            ),
            242 => 
            array (
                'id_reg' => 4243,
                'id_regmf' => 3261,
                'doors' => 4,
            ),
            243 => 
            array (
                'id_reg' => 4244,
                'id_regmf' => 3262,
                'doors' => 4,
            ),
            244 => 
            array (
                'id_reg' => 4245,
                'id_regmf' => 3264,
                'doors' => 5,
            ),
            245 => 
            array (
                'id_reg' => 4246,
                'id_regmf' => 3263,
                'doors' => 5,
            ),
            246 => 
            array (
                'id_reg' => 4247,
                'id_regmf' => 3265,
                'doors' => 4,
            ),
            247 => 
            array (
                'id_reg' => 4248,
                'id_regmf' => 3266,
                'doors' => 4,
            ),
            248 => 
            array (
                'id_reg' => 4249,
                'id_regmf' => 3267,
                'doors' => 3,
            ),
            249 => 
            array (
                'id_reg' => 4250,
                'id_regmf' => 3267,
                'doors' => 5,
            ),
            250 => 
            array (
                'id_reg' => 4251,
                'id_regmf' => 3268,
                'doors' => 3,
            ),
            251 => 
            array (
                'id_reg' => 4252,
                'id_regmf' => 3268,
                'doors' => 5,
            ),
            252 => 
            array (
                'id_reg' => 4253,
                'id_regmf' => 3269,
                'doors' => 5,
            ),
            253 => 
            array (
                'id_reg' => 4254,
                'id_regmf' => 3270,
                'doors' => 3,
            ),
            254 => 
            array (
                'id_reg' => 4255,
                'id_regmf' => 3272,
                'doors' => 3,
            ),
            255 => 
            array (
                'id_reg' => 4256,
                'id_regmf' => 3272,
                'doors' => 5,
            ),
            256 => 
            array (
                'id_reg' => 4257,
                'id_regmf' => 3271,
                'doors' => 5,
            ),
            257 => 
            array (
                'id_reg' => 4258,
                'id_regmf' => 3274,
                'doors' => 2,
            ),
            258 => 
            array (
                'id_reg' => 4259,
                'id_regmf' => 3274,
                'doors' => 4,
            ),
            259 => 
            array (
                'id_reg' => 4260,
                'id_regmf' => 3274,
                'doors' => 5,
            ),
            260 => 
            array (
                'id_reg' => 4261,
                'id_regmf' => 3273,
                'doors' => 2,
            ),
            261 => 
            array (
                'id_reg' => 4262,
                'id_regmf' => 3273,
                'doors' => 5,
            ),
            262 => 
            array (
                'id_reg' => 4263,
                'id_regmf' => 3276,
                'doors' => 3,
            ),
            263 => 
            array (
                'id_reg' => 4264,
                'id_regmf' => 3276,
                'doors' => 4,
            ),
            264 => 
            array (
                'id_reg' => 4265,
                'id_regmf' => 3275,
                'doors' => 3,
            ),
            265 => 
            array (
                'id_reg' => 4266,
                'id_regmf' => 3275,
                'doors' => 4,
            ),
            266 => 
            array (
                'id_reg' => 4267,
                'id_regmf' => 3277,
                'doors' => 3,
            ),
            267 => 
            array (
                'id_reg' => 4268,
                'id_regmf' => 3278,
                'doors' => 0,
            ),
            268 => 
            array (
                'id_reg' => 4269,
                'id_regmf' => 3279,
                'doors' => 0,
            ),
            269 => 
            array (
                'id_reg' => 4270,
                'id_regmf' => 3280,
                'doors' => 0,
            ),
            270 => 
            array (
                'id_reg' => 4271,
                'id_regmf' => 3281,
                'doors' => 0,
            ),
            271 => 
            array (
                'id_reg' => 4272,
                'id_regmf' => 3282,
                'doors' => 0,
            ),
            272 => 
            array (
                'id_reg' => 4273,
                'id_regmf' => 3283,
                'doors' => 0,
            ),
            273 => 
            array (
                'id_reg' => 4274,
                'id_regmf' => 3284,
                'doors' => 0,
            ),
            274 => 
            array (
                'id_reg' => 4275,
                'id_regmf' => 3286,
                'doors' => 0,
            ),
            275 => 
            array (
                'id_reg' => 4276,
                'id_regmf' => 3285,
                'doors' => 0,
            ),
            276 => 
            array (
                'id_reg' => 4277,
                'id_regmf' => 3287,
                'doors' => 2,
            ),
            277 => 
            array (
                'id_reg' => 4278,
                'id_regmf' => 3288,
                'doors' => 0,
            ),
            278 => 
            array (
                'id_reg' => 4279,
                'id_regmf' => 3289,
                'doors' => 0,
            ),
            279 => 
            array (
                'id_reg' => 4280,
                'id_regmf' => 3290,
                'doors' => 5,
            ),
            280 => 
            array (
                'id_reg' => 4281,
                'id_regmf' => 3292,
                'doors' => 4,
            ),
            281 => 
            array (
                'id_reg' => 4282,
                'id_regmf' => 3291,
                'doors' => 5,
            ),
            282 => 
            array (
                'id_reg' => 4283,
                'id_regmf' => 3293,
                'doors' => 5,
            ),
            283 => 
            array (
                'id_reg' => 4284,
                'id_regmf' => 3296,
                'doors' => 4,
            ),
            284 => 
            array (
                'id_reg' => 4285,
                'id_regmf' => 3297,
                'doors' => 5,
            ),
            285 => 
            array (
                'id_reg' => 4286,
                'id_regmf' => 3298,
                'doors' => 4,
            ),
            286 => 
            array (
                'id_reg' => 4287,
                'id_regmf' => 3299,
                'doors' => 4,
            ),
            287 => 
            array (
                'id_reg' => 4288,
                'id_regmf' => 3299,
                'doors' => 5,
            ),
            288 => 
            array (
                'id_reg' => 4289,
                'id_regmf' => 3300,
                'doors' => 4,
            ),
            289 => 
            array (
                'id_reg' => 4290,
                'id_regmf' => 3300,
                'doors' => 5,
            ),
            290 => 
            array (
                'id_reg' => 4291,
                'id_regmf' => 3294,
                'doors' => 4,
            ),
            291 => 
            array (
                'id_reg' => 4292,
                'id_regmf' => 3295,
                'doors' => 4,
            ),
            292 => 
            array (
                'id_reg' => 4293,
                'id_regmf' => 3301,
                'doors' => 4,
            ),
            293 => 
            array (
                'id_reg' => 4294,
                'id_regmf' => 3302,
                'doors' => 4,
            ),
            294 => 
            array (
                'id_reg' => 4295,
                'id_regmf' => 3303,
                'doors' => 4,
            ),
            295 => 
            array (
                'id_reg' => 4296,
                'id_regmf' => 3304,
                'doors' => 2,
            ),
            296 => 
            array (
                'id_reg' => 4297,
                'id_regmf' => 3305,
                'doors' => 4,
            ),
            297 => 
            array (
                'id_reg' => 4298,
                'id_regmf' => 3306,
                'doors' => 4,
            ),
            298 => 
            array (
                'id_reg' => 4299,
                'id_regmf' => 3306,
                'doors' => 5,
            ),
            299 => 
            array (
                'id_reg' => 4300,
                'id_regmf' => 3307,
                'doors' => 2,
            ),
            300 => 
            array (
                'id_reg' => 4301,
                'id_regmf' => 3308,
                'doors' => 2,
            ),
            301 => 
            array (
                'id_reg' => 4302,
                'id_regmf' => 3309,
                'doors' => 2,
            ),
            302 => 
            array (
                'id_reg' => 4303,
                'id_regmf' => 3309,
                'doors' => 4,
            ),
            303 => 
            array (
                'id_reg' => 4304,
                'id_regmf' => 3310,
                'doors' => 2,
            ),
            304 => 
            array (
                'id_reg' => 4305,
                'id_regmf' => 3311,
                'doors' => 3,
            ),
            305 => 
            array (
                'id_reg' => 4306,
                'id_regmf' => 3311,
                'doors' => 5,
            ),
            306 => 
            array (
                'id_reg' => 4307,
                'id_regmf' => 3312,
                'doors' => 5,
            ),
            307 => 
            array (
                'id_reg' => 4308,
                'id_regmf' => 3313,
                'doors' => 5,
            ),
            308 => 
            array (
                'id_reg' => 4309,
                'id_regmf' => 3315,
                'doors' => 5,
            ),
            309 => 
            array (
                'id_reg' => 4310,
                'id_regmf' => 3314,
                'doors' => 5,
            ),
            310 => 
            array (
                'id_reg' => 4311,
                'id_regmf' => 3316,
                'doors' => 4,
            ),
            311 => 
            array (
                'id_reg' => 4312,
                'id_regmf' => 3317,
                'doors' => 2,
            ),
            312 => 
            array (
                'id_reg' => 4313,
                'id_regmf' => 3318,
                'doors' => 5,
            ),
            313 => 
            array (
                'id_reg' => 4314,
                'id_regmf' => 3319,
                'doors' => 5,
            ),
            314 => 
            array (
                'id_reg' => 4315,
                'id_regmf' => 3321,
                'doors' => 2,
            ),
            315 => 
            array (
                'id_reg' => 4316,
                'id_regmf' => 3320,
                'doors' => 2,
            ),
            316 => 
            array (
                'id_reg' => 4317,
                'id_regmf' => 3322,
                'doors' => 3,
            ),
            317 => 
            array (
                'id_reg' => 4318,
                'id_regmf' => 3323,
                'doors' => 3,
            ),
            318 => 
            array (
                'id_reg' => 4319,
                'id_regmf' => 3324,
                'doors' => 0,
            ),
            319 => 
            array (
                'id_reg' => 4320,
                'id_regmf' => 3326,
                'doors' => 5,
            ),
            320 => 
            array (
                'id_reg' => 4321,
                'id_regmf' => 3325,
                'doors' => 5,
            ),
            321 => 
            array (
                'id_reg' => 4322,
                'id_regmf' => 3329,
                'doors' => 2,
            ),
            322 => 
            array (
                'id_reg' => 4323,
                'id_regmf' => 3327,
                'doors' => 2,
            ),
            323 => 
            array (
                'id_reg' => 4324,
                'id_regmf' => 3328,
                'doors' => 2,
            ),
            324 => 
            array (
                'id_reg' => 4325,
                'id_regmf' => 3331,
                'doors' => 3,
            ),
            325 => 
            array (
                'id_reg' => 4326,
                'id_regmf' => 3332,
                'doors' => 3,
            ),
            326 => 
            array (
                'id_reg' => 4327,
                'id_regmf' => 3330,
                'doors' => 3,
            ),
            327 => 
            array (
                'id_reg' => 4328,
                'id_regmf' => 3333,
                'doors' => 2,
            ),
            328 => 
            array (
                'id_reg' => 4329,
                'id_regmf' => 3334,
                'doors' => 3,
            ),
            329 => 
            array (
                'id_reg' => 4330,
                'id_regmf' => 3335,
                'doors' => 0,
            ),
            330 => 
            array (
                'id_reg' => 4331,
                'id_regmf' => 3336,
                'doors' => 0,
            ),
            331 => 
            array (
                'id_reg' => 4332,
                'id_regmf' => 3337,
                'doors' => 2,
            ),
            332 => 
            array (
                'id_reg' => 4333,
                'id_regmf' => 3338,
                'doors' => 4,
            ),
            333 => 
            array (
                'id_reg' => 4334,
                'id_regmf' => 3338,
                'doors' => 5,
            ),
            334 => 
            array (
                'id_reg' => 4335,
                'id_regmf' => 3339,
                'doors' => 5,
            ),
            335 => 
            array (
                'id_reg' => 4336,
                'id_regmf' => 3341,
                'doors' => 5,
            ),
            336 => 
            array (
                'id_reg' => 4337,
                'id_regmf' => 3340,
                'doors' => 5,
            ),
            337 => 
            array (
                'id_reg' => 4338,
                'id_regmf' => 3343,
                'doors' => 2,
            ),
            338 => 
            array (
                'id_reg' => 4339,
                'id_regmf' => 3343,
                'doors' => 4,
            ),
            339 => 
            array (
                'id_reg' => 4340,
                'id_regmf' => 3342,
                'doors' => 2,
            ),
            340 => 
            array (
                'id_reg' => 4341,
                'id_regmf' => 3342,
                'doors' => 4,
            ),
            341 => 
            array (
                'id_reg' => 4342,
                'id_regmf' => 3344,
                'doors' => 2,
            ),
            342 => 
            array (
                'id_reg' => 4343,
                'id_regmf' => 3344,
                'doors' => 5,
            ),
            343 => 
            array (
                'id_reg' => 4344,
                'id_regmf' => 3345,
                'doors' => 2,
            ),
            344 => 
            array (
                'id_reg' => 4345,
                'id_regmf' => 3345,
                'doors' => 5,
            ),
            345 => 
            array (
                'id_reg' => 4346,
                'id_regmf' => 3347,
                'doors' => 4,
            ),
            346 => 
            array (
                'id_reg' => 4347,
                'id_regmf' => 3346,
                'doors' => 4,
            ),
            347 => 
            array (
                'id_reg' => 4348,
                'id_regmf' => 3348,
                'doors' => 5,
            ),
            348 => 
            array (
                'id_reg' => 4349,
                'id_regmf' => 3349,
                'doors' => 5,
            ),
            349 => 
            array (
                'id_reg' => 4350,
                'id_regmf' => 3350,
                'doors' => 4,
            ),
            350 => 
            array (
                'id_reg' => 4351,
                'id_regmf' => 3351,
                'doors' => 4,
            ),
            351 => 
            array (
                'id_reg' => 4352,
                'id_regmf' => 3352,
                'doors' => 5,
            ),
            352 => 
            array (
                'id_reg' => 4353,
                'id_regmf' => 3353,
                'doors' => 5,
            ),
            353 => 
            array (
                'id_reg' => 4354,
                'id_regmf' => 3354,
                'doors' => 2,
            ),
            354 => 
            array (
                'id_reg' => 4355,
                'id_regmf' => 3356,
                'doors' => 5,
            ),
            355 => 
            array (
                'id_reg' => 4356,
                'id_regmf' => 3355,
                'doors' => 2,
            ),
            356 => 
            array (
                'id_reg' => 4357,
                'id_regmf' => 3355,
                'doors' => 5,
            ),
            357 => 
            array (
                'id_reg' => 4358,
                'id_regmf' => 3357,
                'doors' => 3,
            ),
            358 => 
            array (
                'id_reg' => 4359,
                'id_regmf' => 3357,
                'doors' => 5,
            ),
            359 => 
            array (
                'id_reg' => 4360,
                'id_regmf' => 3358,
                'doors' => 3,
            ),
            360 => 
            array (
                'id_reg' => 4361,
                'id_regmf' => 3359,
                'doors' => 5,
            ),
            361 => 
            array (
                'id_reg' => 4362,
                'id_regmf' => 3361,
                'doors' => 5,
            ),
            362 => 
            array (
                'id_reg' => 4363,
                'id_regmf' => 3360,
                'doors' => 5,
            ),
            363 => 
            array (
                'id_reg' => 4364,
                'id_regmf' => 3362,
                'doors' => 4,
            ),
            364 => 
            array (
                'id_reg' => 4365,
                'id_regmf' => 3364,
                'doors' => 5,
            ),
            365 => 
            array (
                'id_reg' => 4366,
                'id_regmf' => 3363,
                'doors' => 5,
            ),
            366 => 
            array (
                'id_reg' => 4367,
                'id_regmf' => 3365,
                'doors' => 5,
            ),
            367 => 
            array (
                'id_reg' => 4368,
                'id_regmf' => 3366,
                'doors' => 5,
            ),
            368 => 
            array (
                'id_reg' => 4369,
                'id_regmf' => 3367,
                'doors' => 4,
            ),
            369 => 
            array (
                'id_reg' => 4370,
                'id_regmf' => 3368,
                'doors' => 5,
            ),
            370 => 
            array (
                'id_reg' => 4371,
                'id_regmf' => 3369,
                'doors' => 5,
            ),
            371 => 
            array (
                'id_reg' => 4372,
                'id_regmf' => 3370,
                'doors' => 5,
            ),
            372 => 
            array (
                'id_reg' => 4373,
                'id_regmf' => 3371,
                'doors' => 5,
            ),
            373 => 
            array (
                'id_reg' => 4374,
                'id_regmf' => 3373,
                'doors' => 5,
            ),
            374 => 
            array (
                'id_reg' => 4375,
                'id_regmf' => 3372,
                'doors' => 5,
            ),
            375 => 
            array (
                'id_reg' => 4376,
                'id_regmf' => 3374,
                'doors' => 2,
            ),
            376 => 
            array (
                'id_reg' => 4377,
                'id_regmf' => 3375,
                'doors' => 5,
            ),
            377 => 
            array (
                'id_reg' => 4378,
                'id_regmf' => 3376,
                'doors' => 2,
            ),
            378 => 
            array (
                'id_reg' => 4379,
                'id_regmf' => 3377,
                'doors' => 0,
            ),
            379 => 
            array (
                'id_reg' => 4380,
                'id_regmf' => 3378,
                'doors' => 4,
            ),
            380 => 
            array (
                'id_reg' => 4381,
                'id_regmf' => 3379,
                'doors' => 4,
            ),
            381 => 
            array (
                'id_reg' => 4382,
                'id_regmf' => 3380,
                'doors' => 2,
            ),
            382 => 
            array (
                'id_reg' => 4383,
                'id_regmf' => 3381,
                'doors' => 4,
            ),
            383 => 
            array (
                'id_reg' => 4384,
                'id_regmf' => 3382,
                'doors' => 4,
            ),
            384 => 
            array (
                'id_reg' => 4385,
                'id_regmf' => 3383,
                'doors' => 4,
            ),
            385 => 
            array (
                'id_reg' => 4386,
                'id_regmf' => 3384,
                'doors' => 4,
            ),
            386 => 
            array (
                'id_reg' => 4387,
                'id_regmf' => 3385,
                'doors' => 2,
            ),
            387 => 
            array (
                'id_reg' => 4388,
                'id_regmf' => 3386,
                'doors' => 4,
            ),
            388 => 
            array (
                'id_reg' => 4389,
                'id_regmf' => 3387,
                'doors' => 2,
            ),
            389 => 
            array (
                'id_reg' => 4390,
                'id_regmf' => 3388,
                'doors' => 3,
            ),
            390 => 
            array (
                'id_reg' => 4391,
                'id_regmf' => 3389,
                'doors' => 4,
            ),
            391 => 
            array (
                'id_reg' => 4392,
                'id_regmf' => 3390,
                'doors' => 3,
            ),
            392 => 
            array (
                'id_reg' => 4393,
                'id_regmf' => 3391,
                'doors' => 2,
            ),
            393 => 
            array (
                'id_reg' => 4394,
                'id_regmf' => 3392,
                'doors' => 3,
            ),
            394 => 
            array (
                'id_reg' => 4395,
                'id_regmf' => 3393,
                'doors' => 5,
            ),
            395 => 
            array (
                'id_reg' => 4396,
                'id_regmf' => 3394,
                'doors' => 3,
            ),
            396 => 
            array (
                'id_reg' => 4397,
                'id_regmf' => 3394,
                'doors' => 4,
            ),
            397 => 
            array (
                'id_reg' => 4398,
                'id_regmf' => 3395,
                'doors' => 3,
            ),
            398 => 
            array (
                'id_reg' => 4399,
                'id_regmf' => 3395,
                'doors' => 4,
            ),
            399 => 
            array (
                'id_reg' => 4400,
                'id_regmf' => 3397,
                'doors' => 5,
            ),
            400 => 
            array (
                'id_reg' => 4401,
                'id_regmf' => 3396,
                'doors' => 5,
            ),
            401 => 
            array (
                'id_reg' => 4402,
                'id_regmf' => 3398,
                'doors' => 5,
            ),
            402 => 
            array (
                'id_reg' => 4403,
                'id_regmf' => 3399,
                'doors' => 2,
            ),
            403 => 
            array (
                'id_reg' => 4404,
                'id_regmf' => 3399,
                'doors' => 4,
            ),
            404 => 
            array (
                'id_reg' => 4405,
                'id_regmf' => 3399,
                'doors' => 5,
            ),
            405 => 
            array (
                'id_reg' => 4406,
                'id_regmf' => 3400,
                'doors' => 5,
            ),
            406 => 
            array (
                'id_reg' => 4407,
                'id_regmf' => 3401,
                'doors' => 3,
            ),
            407 => 
            array (
                'id_reg' => 4408,
                'id_regmf' => 3401,
                'doors' => 5,
            ),
            408 => 
            array (
                'id_reg' => 4409,
                'id_regmf' => 3402,
                'doors' => 4,
            ),
            409 => 
            array (
                'id_reg' => 4410,
                'id_regmf' => 3402,
                'doors' => 5,
            ),
            410 => 
            array (
                'id_reg' => 4411,
                'id_regmf' => 3403,
                'doors' => 4,
            ),
            411 => 
            array (
                'id_reg' => 4412,
                'id_regmf' => 3403,
                'doors' => 5,
            ),
            412 => 
            array (
                'id_reg' => 4413,
                'id_regmf' => 3404,
                'doors' => 3,
            ),
            413 => 
            array (
                'id_reg' => 4414,
                'id_regmf' => 3404,
                'doors' => 4,
            ),
            414 => 
            array (
                'id_reg' => 4415,
                'id_regmf' => 3404,
                'doors' => 5,
            ),
            415 => 
            array (
                'id_reg' => 4416,
                'id_regmf' => 3405,
                'doors' => 5,
            ),
            416 => 
            array (
                'id_reg' => 4417,
                'id_regmf' => 3406,
                'doors' => 4,
            ),
            417 => 
            array (
                'id_reg' => 4418,
                'id_regmf' => 3407,
                'doors' => 5,
            ),
            418 => 
            array (
                'id_reg' => 4419,
                'id_regmf' => 3408,
                'doors' => 3,
            ),
            419 => 
            array (
                'id_reg' => 4420,
                'id_regmf' => 3411,
                'doors' => 5,
            ),
            420 => 
            array (
                'id_reg' => 4421,
                'id_regmf' => 3410,
                'doors' => 5,
            ),
            421 => 
            array (
                'id_reg' => 4422,
                'id_regmf' => 3409,
                'doors' => 5,
            ),
            422 => 
            array (
                'id_reg' => 4423,
                'id_regmf' => 3412,
                'doors' => 3,
            ),
            423 => 
            array (
                'id_reg' => 4424,
                'id_regmf' => 3412,
                'doors' => 5,
            ),
            424 => 
            array (
                'id_reg' => 4425,
                'id_regmf' => 3413,
                'doors' => 5,
            ),
            425 => 
            array (
                'id_reg' => 4426,
                'id_regmf' => 3414,
                'doors' => 2,
            ),
            426 => 
            array (
                'id_reg' => 4427,
                'id_regmf' => 3415,
                'doors' => 5,
            ),
            427 => 
            array (
                'id_reg' => 4428,
                'id_regmf' => 3416,
                'doors' => 5,
            ),
            428 => 
            array (
                'id_reg' => 4429,
                'id_regmf' => 3417,
                'doors' => 5,
            ),
            429 => 
            array (
                'id_reg' => 4430,
                'id_regmf' => 3418,
                'doors' => 4,
            ),
            430 => 
            array (
                'id_reg' => 4431,
                'id_regmf' => 3419,
                'doors' => 3,
            ),
            431 => 
            array (
                'id_reg' => 4432,
                'id_regmf' => 3420,
                'doors' => 4,
            ),
            432 => 
            array (
                'id_reg' => 4433,
                'id_regmf' => 3420,
                'doors' => 5,
            ),
            433 => 
            array (
                'id_reg' => 4434,
                'id_regmf' => 3421,
                'doors' => 2,
            ),
            434 => 
            array (
                'id_reg' => 4435,
                'id_regmf' => 3424,
                'doors' => 5,
            ),
            435 => 
            array (
                'id_reg' => 4436,
                'id_regmf' => 3422,
                'doors' => 5,
            ),
            436 => 
            array (
                'id_reg' => 4437,
                'id_regmf' => 3423,
                'doors' => 5,
            ),
            437 => 
            array (
                'id_reg' => 4438,
                'id_regmf' => 3426,
                'doors' => 4,
            ),
            438 => 
            array (
                'id_reg' => 4439,
                'id_regmf' => 3425,
                'doors' => 3,
            ),
            439 => 
            array (
                'id_reg' => 4440,
                'id_regmf' => 3427,
                'doors' => 2,
            ),
            440 => 
            array (
                'id_reg' => 4441,
                'id_regmf' => 3428,
                'doors' => 2,
            ),
            441 => 
            array (
                'id_reg' => 4442,
                'id_regmf' => 3429,
                'doors' => 4,
            ),
            442 => 
            array (
                'id_reg' => 4443,
                'id_regmf' => 3430,
                'doors' => 2,
            ),
            443 => 
            array (
                'id_reg' => 4444,
                'id_regmf' => 3431,
                'doors' => 2,
            ),
            444 => 
            array (
                'id_reg' => 4445,
                'id_regmf' => 3432,
                'doors' => 2,
            ),
            445 => 
            array (
                'id_reg' => 4446,
                'id_regmf' => 3433,
                'doors' => 0,
            ),
            446 => 
            array (
                'id_reg' => 4447,
                'id_regmf' => 3434,
                'doors' => 0,
            ),
            447 => 
            array (
                'id_reg' => 4448,
                'id_regmf' => 3435,
                'doors' => 4,
            ),
            448 => 
            array (
                'id_reg' => 4449,
                'id_regmf' => 3436,
                'doors' => 3,
            ),
            449 => 
            array (
                'id_reg' => 4450,
                'id_regmf' => 3436,
                'doors' => 5,
            ),
            450 => 
            array (
                'id_reg' => 4451,
                'id_regmf' => 3438,
                'doors' => 3,
            ),
            451 => 
            array (
                'id_reg' => 4452,
                'id_regmf' => 3438,
                'doors' => 4,
            ),
            452 => 
            array (
                'id_reg' => 4453,
                'id_regmf' => 3438,
                'doors' => 5,
            ),
            453 => 
            array (
                'id_reg' => 4454,
                'id_regmf' => 3439,
                'doors' => 5,
            ),
            454 => 
            array (
                'id_reg' => 4455,
                'id_regmf' => 3437,
                'doors' => 4,
            ),
            455 => 
            array (
                'id_reg' => 4456,
                'id_regmf' => 3437,
                'doors' => 5,
            ),
            456 => 
            array (
                'id_reg' => 4457,
                'id_regmf' => 3440,
                'doors' => 2,
            ),
            457 => 
            array (
                'id_reg' => 4458,
                'id_regmf' => 3441,
                'doors' => 4,
            ),
            458 => 
            array (
                'id_reg' => 4459,
                'id_regmf' => 3441,
                'doors' => 5,
            ),
            459 => 
            array (
                'id_reg' => 4460,
                'id_regmf' => 3442,
                'doors' => 5,
            ),
            460 => 
            array (
                'id_reg' => 4461,
                'id_regmf' => 3443,
                'doors' => 3,
            ),
            461 => 
            array (
                'id_reg' => 4462,
                'id_regmf' => 3444,
                'doors' => 2,
            ),
            462 => 
            array (
                'id_reg' => 4463,
                'id_regmf' => 3445,
                'doors' => 5,
            ),
            463 => 
            array (
                'id_reg' => 4464,
                'id_regmf' => 3446,
                'doors' => 3,
            ),
            464 => 
            array (
                'id_reg' => 4465,
                'id_regmf' => 3446,
                'doors' => 5,
            ),
            465 => 
            array (
                'id_reg' => 4466,
                'id_regmf' => 3447,
                'doors' => 3,
            ),
            466 => 
            array (
                'id_reg' => 4467,
                'id_regmf' => 3447,
                'doors' => 5,
            ),
            467 => 
            array (
                'id_reg' => 4468,
                'id_regmf' => 3449,
                'doors' => 3,
            ),
            468 => 
            array (
                'id_reg' => 4469,
                'id_regmf' => 3449,
                'doors' => 5,
            ),
            469 => 
            array (
                'id_reg' => 4470,
                'id_regmf' => 3448,
                'doors' => 5,
            ),
            470 => 
            array (
                'id_reg' => 4471,
                'id_regmf' => 3450,
                'doors' => 3,
            ),
            471 => 
            array (
                'id_reg' => 4472,
                'id_regmf' => 3451,
                'doors' => 2,
            ),
            472 => 
            array (
                'id_reg' => 4473,
                'id_regmf' => 3451,
                'doors' => 3,
            ),
            473 => 
            array (
                'id_reg' => 4474,
                'id_regmf' => 3452,
                'doors' => 4,
            ),
            474 => 
            array (
                'id_reg' => 4475,
                'id_regmf' => 3453,
                'doors' => 2,
            ),
            475 => 
            array (
                'id_reg' => 4476,
                'id_regmf' => 3454,
                'doors' => 5,
            ),
            476 => 
            array (
                'id_reg' => 4477,
                'id_regmf' => 3455,
                'doors' => 4,
            ),
            477 => 
            array (
                'id_reg' => 4478,
                'id_regmf' => 3455,
                'doors' => 5,
            ),
            478 => 
            array (
                'id_reg' => 4479,
                'id_regmf' => 3456,
                'doors' => 2,
            ),
            479 => 
            array (
                'id_reg' => 4480,
                'id_regmf' => 3457,
                'doors' => 2,
            ),
            480 => 
            array (
                'id_reg' => 4481,
                'id_regmf' => 3458,
                'doors' => 5,
            ),
            481 => 
            array (
                'id_reg' => 4482,
                'id_regmf' => 3459,
                'doors' => 2,
            ),
            482 => 
            array (
                'id_reg' => 4483,
                'id_regmf' => 3459,
                'doors' => 3,
            ),
            483 => 
            array (
                'id_reg' => 4484,
                'id_regmf' => 3460,
                'doors' => 2,
            ),
            484 => 
            array (
                'id_reg' => 4485,
                'id_regmf' => 3460,
                'doors' => 3,
            ),
            485 => 
            array (
                'id_reg' => 4486,
                'id_regmf' => 3461,
                'doors' => 5,
            ),
            486 => 
            array (
                'id_reg' => 4487,
                'id_regmf' => 3462,
                'doors' => 3,
            ),
            487 => 
            array (
                'id_reg' => 4488,
                'id_regmf' => 3463,
                'doors' => 3,
            ),
            488 => 
            array (
                'id_reg' => 4489,
                'id_regmf' => 3464,
                'doors' => 3,
            ),
            489 => 
            array (
                'id_reg' => 4490,
                'id_regmf' => 3465,
                'doors' => 5,
            ),
            490 => 
            array (
                'id_reg' => 4491,
                'id_regmf' => 3466,
                'doors' => 5,
            ),
            491 => 
            array (
                'id_reg' => 4492,
                'id_regmf' => 3467,
                'doors' => 5,
            ),
            492 => 
            array (
                'id_reg' => 4493,
                'id_regmf' => 3468,
                'doors' => 3,
            ),
            493 => 
            array (
                'id_reg' => 4494,
                'id_regmf' => 3468,
                'doors' => 5,
            ),
            494 => 
            array (
                'id_reg' => 4495,
                'id_regmf' => 3469,
                'doors' => 2,
            ),
            495 => 
            array (
                'id_reg' => 4496,
                'id_regmf' => 3469,
                'doors' => 3,
            ),
            496 => 
            array (
                'id_reg' => 4497,
                'id_regmf' => 3469,
                'doors' => 4,
            ),
            497 => 
            array (
                'id_reg' => 4498,
                'id_regmf' => 3469,
                'doors' => 5,
            ),
            498 => 
            array (
                'id_reg' => 4499,
                'id_regmf' => 3471,
                'doors' => 5,
            ),
            499 => 
            array (
                'id_reg' => 4500,
                'id_regmf' => 3470,
                'doors' => 5,
            ),
        ));
        \DB::table('model_fuel_numdoors')->insert(array (
            0 => 
            array (
                'id_reg' => 4501,
                'id_regmf' => 3473,
                'doors' => 5,
            ),
            1 => 
            array (
                'id_reg' => 4502,
                'id_regmf' => 3472,
                'doors' => 5,
            ),
            2 => 
            array (
                'id_reg' => 4503,
                'id_regmf' => 3475,
                'doors' => 2,
            ),
            3 => 
            array (
                'id_reg' => 4504,
                'id_regmf' => 3475,
                'doors' => 3,
            ),
            4 => 
            array (
                'id_reg' => 4505,
                'id_regmf' => 3475,
                'doors' => 5,
            ),
            5 => 
            array (
                'id_reg' => 4506,
                'id_regmf' => 3474,
                'doors' => 3,
            ),
            6 => 
            array (
                'id_reg' => 4507,
                'id_regmf' => 3474,
                'doors' => 5,
            ),
            7 => 
            array (
                'id_reg' => 4508,
                'id_regmf' => 3476,
                'doors' => 5,
            ),
            8 => 
            array (
                'id_reg' => 4509,
                'id_regmf' => 3477,
                'doors' => 3,
            ),
            9 => 
            array (
                'id_reg' => 4510,
                'id_regmf' => 3478,
                'doors' => 3,
            ),
            10 => 
            array (
                'id_reg' => 4511,
                'id_regmf' => 3478,
                'doors' => 5,
            ),
            11 => 
            array (
                'id_reg' => 4512,
                'id_regmf' => 3479,
                'doors' => 5,
            ),
            12 => 
            array (
                'id_reg' => 4513,
                'id_regmf' => 3480,
                'doors' => 5,
            ),
            13 => 
            array (
                'id_reg' => 4514,
                'id_regmf' => 3481,
                'doors' => 3,
            ),
            14 => 
            array (
                'id_reg' => 4515,
                'id_regmf' => 3482,
                'doors' => 5,
            ),
            15 => 
            array (
                'id_reg' => 4516,
                'id_regmf' => 3483,
                'doors' => 2,
            ),
            16 => 
            array (
                'id_reg' => 4517,
                'id_regmf' => 3483,
                'doors' => 4,
            ),
            17 => 
            array (
                'id_reg' => 4518,
                'id_regmf' => 3484,
                'doors' => 3,
            ),
            18 => 
            array (
                'id_reg' => 4519,
                'id_regmf' => 3485,
                'doors' => 5,
            ),
            19 => 
            array (
                'id_reg' => 4520,
                'id_regmf' => 3486,
                'doors' => 3,
            ),
            20 => 
            array (
                'id_reg' => 4521,
                'id_regmf' => 3487,
                'doors' => 5,
            ),
            21 => 
            array (
                'id_reg' => 4522,
                'id_regmf' => 3488,
                'doors' => 5,
            ),
            22 => 
            array (
                'id_reg' => 4523,
                'id_regmf' => 3489,
                'doors' => 5,
            ),
            23 => 
            array (
                'id_reg' => 4524,
                'id_regmf' => 3490,
                'doors' => 5,
            ),
            24 => 
            array (
                'id_reg' => 4525,
                'id_regmf' => 3491,
                'doors' => 5,
            ),
            25 => 
            array (
                'id_reg' => 4526,
                'id_regmf' => 3492,
                'doors' => 2,
            ),
            26 => 
            array (
                'id_reg' => 4527,
                'id_regmf' => 3493,
                'doors' => 2,
            ),
            27 => 
            array (
                'id_reg' => 4528,
                'id_regmf' => 3493,
                'doors' => 3,
            ),
            28 => 
            array (
                'id_reg' => 4529,
                'id_regmf' => 3495,
                'doors' => 4,
            ),
            29 => 
            array (
                'id_reg' => 4530,
                'id_regmf' => 3494,
                'doors' => 4,
            ),
            30 => 
            array (
                'id_reg' => 4531,
                'id_regmf' => 3496,
                'doors' => 3,
            ),
            31 => 
            array (
                'id_reg' => 4532,
                'id_regmf' => 3498,
                'doors' => 4,
            ),
            32 => 
            array (
                'id_reg' => 4533,
                'id_regmf' => 3497,
                'doors' => 4,
            ),
            33 => 
            array (
                'id_reg' => 4534,
                'id_regmf' => 3499,
                'doors' => 2,
            ),
            34 => 
            array (
                'id_reg' => 4535,
                'id_regmf' => 3500,
                'doors' => 3,
            ),
            35 => 
            array (
                'id_reg' => 4536,
                'id_regmf' => 3501,
                'doors' => 3,
            ),
            36 => 
            array (
                'id_reg' => 4537,
                'id_regmf' => 3502,
                'doors' => 3,
            ),
            37 => 
            array (
                'id_reg' => 4538,
                'id_regmf' => 3503,
                'doors' => 3,
            ),
            38 => 
            array (
                'id_reg' => 4539,
                'id_regmf' => 3504,
                'doors' => 3,
            ),
            39 => 
            array (
                'id_reg' => 4540,
                'id_regmf' => 3505,
                'doors' => 2,
            ),
            40 => 
            array (
                'id_reg' => 4541,
                'id_regmf' => 3506,
                'doors' => 5,
            ),
            41 => 
            array (
                'id_reg' => 4542,
                'id_regmf' => 3507,
                'doors' => 5,
            ),
            42 => 
            array (
                'id_reg' => 4543,
                'id_regmf' => 3509,
                'doors' => 5,
            ),
            43 => 
            array (
                'id_reg' => 4544,
                'id_regmf' => 3508,
                'doors' => 5,
            ),
            44 => 
            array (
                'id_reg' => 4545,
                'id_regmf' => 3511,
                'doors' => 5,
            ),
            45 => 
            array (
                'id_reg' => 4546,
                'id_regmf' => 3510,
                'doors' => 5,
            ),
            46 => 
            array (
                'id_reg' => 4547,
                'id_regmf' => 3512,
                'doors' => 5,
            ),
            47 => 
            array (
                'id_reg' => 4548,
                'id_regmf' => 3513,
                'doors' => 5,
            ),
            48 => 
            array (
                'id_reg' => 4549,
                'id_regmf' => 3514,
                'doors' => 5,
            ),
            49 => 
            array (
                'id_reg' => 4550,
                'id_regmf' => 3515,
                'doors' => 5,
            ),
            50 => 
            array (
                'id_reg' => 4551,
                'id_regmf' => 3516,
                'doors' => 2,
            ),
            51 => 
            array (
                'id_reg' => 4552,
                'id_regmf' => 3516,
                'doors' => 3,
            ),
            52 => 
            array (
                'id_reg' => 4553,
                'id_regmf' => 3516,
                'doors' => 4,
            ),
            53 => 
            array (
                'id_reg' => 4554,
                'id_regmf' => 3517,
                'doors' => 2,
            ),
            54 => 
            array (
                'id_reg' => 4555,
                'id_regmf' => 3518,
                'doors' => 5,
            ),
            55 => 
            array (
                'id_reg' => 4556,
                'id_regmf' => 3519,
                'doors' => 5,
            ),
            56 => 
            array (
                'id_reg' => 4557,
                'id_regmf' => 3520,
                'doors' => 2,
            ),
            57 => 
            array (
                'id_reg' => 4558,
                'id_regmf' => 3520,
                'doors' => 4,
            ),
            58 => 
            array (
                'id_reg' => 4559,
                'id_regmf' => 3521,
                'doors' => 0,
            ),
            59 => 
            array (
                'id_reg' => 4560,
                'id_regmf' => 3522,
                'doors' => 0,
            ),
            60 => 
            array (
                'id_reg' => 4561,
                'id_regmf' => 3523,
                'doors' => 0,
            ),
            61 => 
            array (
                'id_reg' => 4562,
                'id_regmf' => 3524,
                'doors' => 0,
            ),
            62 => 
            array (
                'id_reg' => 4563,
                'id_regmf' => 3525,
                'doors' => 0,
            ),
            63 => 
            array (
                'id_reg' => 4564,
                'id_regmf' => 3526,
                'doors' => 0,
            ),
            64 => 
            array (
                'id_reg' => 4565,
                'id_regmf' => 3527,
                'doors' => 0,
            ),
            65 => 
            array (
                'id_reg' => 4566,
                'id_regmf' => 3528,
                'doors' => 3,
            ),
            66 => 
            array (
                'id_reg' => 4567,
                'id_regmf' => 3529,
                'doors' => 0,
            ),
            67 => 
            array (
                'id_reg' => 4568,
                'id_regmf' => 3530,
                'doors' => 0,
            ),
            68 => 
            array (
                'id_reg' => 4569,
                'id_regmf' => 3531,
                'doors' => 0,
            ),
            69 => 
            array (
                'id_reg' => 4570,
                'id_regmf' => 3532,
                'doors' => 0,
            ),
            70 => 
            array (
                'id_reg' => 4571,
                'id_regmf' => 3533,
                'doors' => 0,
            ),
            71 => 
            array (
                'id_reg' => 4572,
                'id_regmf' => 3534,
                'doors' => 0,
            ),
            72 => 
            array (
                'id_reg' => 4573,
                'id_regmf' => 3535,
                'doors' => 0,
            ),
            73 => 
            array (
                'id_reg' => 4574,
                'id_regmf' => 3536,
                'doors' => 0,
            ),
            74 => 
            array (
                'id_reg' => 4575,
                'id_regmf' => 3537,
                'doors' => 0,
            ),
            75 => 
            array (
                'id_reg' => 4576,
                'id_regmf' => 3538,
                'doors' => 2,
            ),
            76 => 
            array (
                'id_reg' => 4577,
                'id_regmf' => 3539,
                'doors' => 0,
            ),
            77 => 
            array (
                'id_reg' => 4578,
                'id_regmf' => 3540,
                'doors' => 3,
            ),
            78 => 
            array (
                'id_reg' => 4579,
                'id_regmf' => 3541,
                'doors' => 3,
            ),
            79 => 
            array (
                'id_reg' => 4580,
                'id_regmf' => 3542,
                'doors' => 3,
            ),
            80 => 
            array (
                'id_reg' => 4581,
                'id_regmf' => 3543,
                'doors' => 2,
            ),
            81 => 
            array (
                'id_reg' => 4582,
                'id_regmf' => 3544,
                'doors' => 3,
            ),
            82 => 
            array (
                'id_reg' => 4583,
                'id_regmf' => 3545,
                'doors' => 5,
            ),
            83 => 
            array (
                'id_reg' => 4584,
                'id_regmf' => 3546,
                'doors' => 5,
            ),
            84 => 
            array (
                'id_reg' => 4585,
                'id_regmf' => 3547,
                'doors' => 2,
            ),
            85 => 
            array (
                'id_reg' => 4586,
                'id_regmf' => 3548,
                'doors' => 0,
            ),
            86 => 
            array (
                'id_reg' => 4587,
                'id_regmf' => 3549,
                'doors' => 0,
            ),
            87 => 
            array (
                'id_reg' => 4588,
                'id_regmf' => 3550,
                'doors' => 0,
            ),
            88 => 
            array (
                'id_reg' => 4589,
                'id_regmf' => 3551,
                'doors' => 0,
            ),
            89 => 
            array (
                'id_reg' => 4590,
                'id_regmf' => 3552,
                'doors' => 0,
            ),
            90 => 
            array (
                'id_reg' => 4591,
                'id_regmf' => 3553,
                'doors' => 0,
            ),
            91 => 
            array (
                'id_reg' => 4592,
                'id_regmf' => 3554,
                'doors' => 0,
            ),
            92 => 
            array (
                'id_reg' => 4593,
                'id_regmf' => 3555,
                'doors' => 0,
            ),
            93 => 
            array (
                'id_reg' => 4594,
                'id_regmf' => 3556,
                'doors' => 0,
            ),
            94 => 
            array (
                'id_reg' => 4595,
                'id_regmf' => 3557,
                'doors' => 0,
            ),
            95 => 
            array (
                'id_reg' => 4596,
                'id_regmf' => 3558,
                'doors' => 0,
            ),
            96 => 
            array (
                'id_reg' => 4597,
                'id_regmf' => 3559,
                'doors' => 0,
            ),
            97 => 
            array (
                'id_reg' => 4598,
                'id_regmf' => 3560,
                'doors' => 0,
            ),
            98 => 
            array (
                'id_reg' => 4599,
                'id_regmf' => 3561,
                'doors' => 0,
            ),
            99 => 
            array (
                'id_reg' => 4600,
                'id_regmf' => 3562,
                'doors' => 3,
            ),
            100 => 
            array (
                'id_reg' => 4601,
                'id_regmf' => 3563,
                'doors' => 0,
            ),
            101 => 
            array (
                'id_reg' => 4602,
                'id_regmf' => 3564,
                'doors' => 0,
            ),
            102 => 
            array (
                'id_reg' => 4603,
                'id_regmf' => 3565,
                'doors' => 0,
            ),
            103 => 
            array (
                'id_reg' => 4604,
                'id_regmf' => 3567,
                'doors' => 0,
            ),
            104 => 
            array (
                'id_reg' => 4605,
                'id_regmf' => 3566,
                'doors' => 0,
            ),
            105 => 
            array (
                'id_reg' => 4606,
                'id_regmf' => 3569,
                'doors' => 0,
            ),
            106 => 
            array (
                'id_reg' => 4607,
                'id_regmf' => 3568,
                'doors' => 0,
            ),
            107 => 
            array (
                'id_reg' => 4608,
                'id_regmf' => 3571,
                'doors' => 0,
            ),
            108 => 
            array (
                'id_reg' => 4609,
                'id_regmf' => 3570,
                'doors' => 0,
            ),
            109 => 
            array (
                'id_reg' => 4610,
                'id_regmf' => 3572,
                'doors' => 0,
            ),
            110 => 
            array (
                'id_reg' => 4611,
                'id_regmf' => 3573,
                'doors' => 3,
            ),
            111 => 
            array (
                'id_reg' => 4612,
                'id_regmf' => 3574,
                'doors' => 3,
            ),
            112 => 
            array (
                'id_reg' => 4613,
                'id_regmf' => 3577,
                'doors' => 4,
            ),
            113 => 
            array (
                'id_reg' => 4614,
                'id_regmf' => 3575,
                'doors' => 5,
            ),
            114 => 
            array (
                'id_reg' => 4615,
                'id_regmf' => 3576,
                'doors' => 4,
            ),
            115 => 
            array (
                'id_reg' => 4616,
                'id_regmf' => 3576,
                'doors' => 5,
            ),
            116 => 
            array (
                'id_reg' => 4617,
                'id_regmf' => 3578,
                'doors' => 5,
            ),
            117 => 
            array (
                'id_reg' => 4618,
                'id_regmf' => 3579,
                'doors' => 5,
            ),
            118 => 
            array (
                'id_reg' => 4619,
                'id_regmf' => 3580,
                'doors' => 4,
            ),
            119 => 
            array (
                'id_reg' => 4620,
                'id_regmf' => 3582,
                'doors' => 5,
            ),
            120 => 
            array (
                'id_reg' => 4621,
                'id_regmf' => 3581,
                'doors' => 5,
            ),
            121 => 
            array (
                'id_reg' => 4622,
                'id_regmf' => 3584,
                'doors' => 2,
            ),
            122 => 
            array (
                'id_reg' => 4623,
                'id_regmf' => 3584,
                'doors' => 4,
            ),
            123 => 
            array (
                'id_reg' => 4624,
                'id_regmf' => 3584,
                'doors' => 5,
            ),
            124 => 
            array (
                'id_reg' => 4625,
                'id_regmf' => 3583,
                'doors' => 4,
            ),
            125 => 
            array (
                'id_reg' => 4626,
                'id_regmf' => 3585,
                'doors' => 4,
            ),
            126 => 
            array (
                'id_reg' => 4627,
                'id_regmf' => 3585,
                'doors' => 5,
            ),
            127 => 
            array (
                'id_reg' => 4628,
                'id_regmf' => 3586,
                'doors' => 4,
            ),
            128 => 
            array (
                'id_reg' => 4629,
                'id_regmf' => 3586,
                'doors' => 5,
            ),
            129 => 
            array (
                'id_reg' => 4630,
                'id_regmf' => 3587,
                'doors' => 4,
            ),
            130 => 
            array (
                'id_reg' => 4631,
                'id_regmf' => 3587,
                'doors' => 5,
            ),
            131 => 
            array (
                'id_reg' => 4632,
                'id_regmf' => 3588,
                'doors' => 4,
            ),
            132 => 
            array (
                'id_reg' => 4633,
                'id_regmf' => 3588,
                'doors' => 5,
            ),
            133 => 
            array (
                'id_reg' => 4634,
                'id_regmf' => 3589,
                'doors' => 4,
            ),
            134 => 
            array (
                'id_reg' => 4635,
                'id_regmf' => 3590,
                'doors' => 4,
            ),
            135 => 
            array (
                'id_reg' => 4636,
                'id_regmf' => 3591,
                'doors' => 4,
            ),
            136 => 
            array (
                'id_reg' => 4637,
                'id_regmf' => 3592,
                'doors' => 2,
            ),
            137 => 
            array (
                'id_reg' => 4638,
                'id_regmf' => 3592,
                'doors' => 4,
            ),
            138 => 
            array (
                'id_reg' => 4639,
                'id_regmf' => 3593,
                'doors' => 2,
            ),
            139 => 
            array (
                'id_reg' => 4640,
                'id_regmf' => 3593,
                'doors' => 4,
            ),
            140 => 
            array (
                'id_reg' => 4641,
                'id_regmf' => 3594,
                'doors' => 2,
            ),
            141 => 
            array (
                'id_reg' => 4642,
                'id_regmf' => 3594,
                'doors' => 4,
            ),
            142 => 
            array (
                'id_reg' => 4643,
                'id_regmf' => 3595,
                'doors' => 2,
            ),
            143 => 
            array (
                'id_reg' => 4644,
                'id_regmf' => 3596,
                'doors' => 2,
            ),
            144 => 
            array (
                'id_reg' => 4645,
                'id_regmf' => 3597,
                'doors' => 2,
            ),
            145 => 
            array (
                'id_reg' => 4646,
                'id_regmf' => 3599,
                'doors' => 5,
            ),
            146 => 
            array (
                'id_reg' => 4647,
                'id_regmf' => 3598,
                'doors' => 5,
            ),
            147 => 
            array (
                'id_reg' => 4648,
                'id_regmf' => 3600,
                'doors' => 4,
            ),
            148 => 
            array (
                'id_reg' => 4649,
                'id_regmf' => 3601,
                'doors' => 3,
            ),
            149 => 
            array (
                'id_reg' => 4650,
                'id_regmf' => 3602,
                'doors' => 5,
            ),
            150 => 
            array (
                'id_reg' => 4651,
                'id_regmf' => 3603,
                'doors' => 4,
            ),
            151 => 
            array (
                'id_reg' => 4652,
                'id_regmf' => 3604,
                'doors' => 2,
            ),
            152 => 
            array (
                'id_reg' => 4653,
                'id_regmf' => 3606,
                'doors' => 4,
            ),
            153 => 
            array (
                'id_reg' => 4654,
                'id_regmf' => 3606,
                'doors' => 5,
            ),
            154 => 
            array (
                'id_reg' => 4655,
                'id_regmf' => 3605,
                'doors' => 2,
            ),
            155 => 
            array (
                'id_reg' => 4656,
                'id_regmf' => 3605,
                'doors' => 4,
            ),
            156 => 
            array (
                'id_reg' => 4657,
                'id_regmf' => 3605,
                'doors' => 5,
            ),
            157 => 
            array (
                'id_reg' => 4658,
                'id_regmf' => 3607,
                'doors' => 5,
            ),
            158 => 
            array (
                'id_reg' => 4659,
                'id_regmf' => 3608,
                'doors' => 5,
            ),
            159 => 
            array (
                'id_reg' => 4660,
                'id_regmf' => 3609,
                'doors' => 2,
            ),
            160 => 
            array (
                'id_reg' => 4661,
                'id_regmf' => 3609,
                'doors' => 4,
            ),
            161 => 
            array (
                'id_reg' => 4662,
                'id_regmf' => 3609,
                'doors' => 5,
            ),
            162 => 
            array (
                'id_reg' => 4663,
                'id_regmf' => 3610,
                'doors' => 2,
            ),
            163 => 
            array (
                'id_reg' => 4664,
                'id_regmf' => 3610,
                'doors' => 4,
            ),
            164 => 
            array (
                'id_reg' => 4665,
                'id_regmf' => 3611,
                'doors' => 5,
            ),
            165 => 
            array (
                'id_reg' => 4666,
                'id_regmf' => 3612,
                'doors' => 3,
            ),
            166 => 
            array (
                'id_reg' => 4667,
                'id_regmf' => 3613,
                'doors' => 3,
            ),
            167 => 
            array (
                'id_reg' => 4668,
                'id_regmf' => 3614,
                'doors' => 5,
            ),
            168 => 
            array (
                'id_reg' => 4669,
                'id_regmf' => 3615,
                'doors' => 5,
            ),
            169 => 
            array (
                'id_reg' => 4670,
                'id_regmf' => 3617,
                'doors' => 5,
            ),
            170 => 
            array (
                'id_reg' => 4671,
                'id_regmf' => 3616,
                'doors' => 5,
            ),
            171 => 
            array (
                'id_reg' => 4672,
                'id_regmf' => 3619,
                'doors' => 4,
            ),
            172 => 
            array (
                'id_reg' => 4673,
                'id_regmf' => 3618,
                'doors' => 4,
            ),
            173 => 
            array (
                'id_reg' => 4674,
                'id_regmf' => 3618,
                'doors' => 5,
            ),
            174 => 
            array (
                'id_reg' => 4675,
                'id_regmf' => 3620,
                'doors' => 4,
            ),
            175 => 
            array (
                'id_reg' => 4676,
                'id_regmf' => 3621,
                'doors' => 5,
            ),
            176 => 
            array (
                'id_reg' => 4677,
                'id_regmf' => 3622,
                'doors' => 5,
            ),
            177 => 
            array (
                'id_reg' => 4678,
                'id_regmf' => 3623,
                'doors' => 4,
            ),
            178 => 
            array (
                'id_reg' => 4679,
                'id_regmf' => 3624,
                'doors' => 2,
            ),
            179 => 
            array (
                'id_reg' => 4680,
                'id_regmf' => 3625,
                'doors' => 2,
            ),
            180 => 
            array (
                'id_reg' => 4681,
                'id_regmf' => 3626,
                'doors' => 2,
            ),
            181 => 
            array (
                'id_reg' => 4682,
                'id_regmf' => 3627,
                'doors' => 5,
            ),
            182 => 
            array (
                'id_reg' => 4683,
                'id_regmf' => 3628,
                'doors' => 5,
            ),
            183 => 
            array (
                'id_reg' => 4684,
                'id_regmf' => 3629,
                'doors' => 5,
            ),
            184 => 
            array (
                'id_reg' => 4685,
                'id_regmf' => 3630,
                'doors' => 3,
            ),
            185 => 
            array (
                'id_reg' => 4686,
                'id_regmf' => 3630,
                'doors' => 4,
            ),
            186 => 
            array (
                'id_reg' => 4687,
                'id_regmf' => 3630,
                'doors' => 5,
            ),
            187 => 
            array (
                'id_reg' => 4688,
                'id_regmf' => 3631,
                'doors' => 5,
            ),
            188 => 
            array (
                'id_reg' => 4689,
                'id_regmf' => 3632,
                'doors' => 5,
            ),
            189 => 
            array (
                'id_reg' => 4690,
                'id_regmf' => 3632,
                'doors' => 6,
            ),
            190 => 
            array (
                'id_reg' => 4691,
                'id_regmf' => 3633,
                'doors' => 5,
            ),
            191 => 
            array (
                'id_reg' => 4692,
                'id_regmf' => 3633,
                'doors' => 6,
            ),
            192 => 
            array (
                'id_reg' => 4693,
                'id_regmf' => 3634,
                'doors' => 5,
            ),
            193 => 
            array (
                'id_reg' => 4694,
                'id_regmf' => 3635,
                'doors' => 5,
            ),
            194 => 
            array (
                'id_reg' => 4695,
                'id_regmf' => 3636,
                'doors' => 5,
            ),
            195 => 
            array (
                'id_reg' => 4696,
                'id_regmf' => 3637,
                'doors' => 5,
            ),
            196 => 
            array (
                'id_reg' => 4697,
                'id_regmf' => 3638,
                'doors' => 2,
            ),
            197 => 
            array (
                'id_reg' => 4698,
                'id_regmf' => 3639,
                'doors' => 5,
            ),
            198 => 
            array (
                'id_reg' => 4699,
                'id_regmf' => 3641,
                'doors' => 3,
            ),
            199 => 
            array (
                'id_reg' => 4700,
                'id_regmf' => 3641,
                'doors' => 5,
            ),
            200 => 
            array (
                'id_reg' => 4701,
                'id_regmf' => 3640,
                'doors' => 5,
            ),
            201 => 
            array (
                'id_reg' => 4702,
                'id_regmf' => 3642,
                'doors' => 2,
            ),
            202 => 
            array (
                'id_reg' => 4703,
                'id_regmf' => 3642,
                'doors' => 3,
            ),
            203 => 
            array (
                'id_reg' => 4704,
                'id_regmf' => 3643,
                'doors' => 5,
            ),
            204 => 
            array (
                'id_reg' => 4705,
                'id_regmf' => 3644,
                'doors' => 5,
            ),
            205 => 
            array (
                'id_reg' => 4706,
                'id_regmf' => 3645,
                'doors' => 4,
            ),
            206 => 
            array (
                'id_reg' => 4707,
                'id_regmf' => 3646,
                'doors' => 2,
            ),
            207 => 
            array (
                'id_reg' => 4708,
                'id_regmf' => 3646,
                'doors' => 4,
            ),
            208 => 
            array (
                'id_reg' => 4709,
                'id_regmf' => 3647,
                'doors' => 2,
            ),
            209 => 
            array (
                'id_reg' => 4710,
                'id_regmf' => 3647,
                'doors' => 3,
            ),
            210 => 
            array (
                'id_reg' => 4711,
                'id_regmf' => 3647,
                'doors' => 4,
            ),
            211 => 
            array (
                'id_reg' => 4712,
                'id_regmf' => 3647,
                'doors' => 5,
            ),
            212 => 
            array (
                'id_reg' => 4713,
                'id_regmf' => 3648,
                'doors' => 4,
            ),
            213 => 
            array (
                'id_reg' => 4714,
                'id_regmf' => 3649,
                'doors' => 2,
            ),
            214 => 
            array (
                'id_reg' => 4715,
                'id_regmf' => 3650,
                'doors' => 4,
            ),
            215 => 
            array (
                'id_reg' => 4716,
                'id_regmf' => 3651,
                'doors' => 5,
            ),
            216 => 
            array (
                'id_reg' => 4717,
                'id_regmf' => 3652,
                'doors' => 5,
            ),
            217 => 
            array (
                'id_reg' => 4718,
                'id_regmf' => 3653,
                'doors' => 5,
            ),
            218 => 
            array (
                'id_reg' => 4719,
                'id_regmf' => 3655,
                'doors' => 5,
            ),
            219 => 
            array (
                'id_reg' => 4720,
                'id_regmf' => 3654,
                'doors' => 5,
            ),
            220 => 
            array (
                'id_reg' => 4721,
                'id_regmf' => 3656,
                'doors' => 3,
            ),
            221 => 
            array (
                'id_reg' => 4722,
                'id_regmf' => 3656,
                'doors' => 4,
            ),
            222 => 
            array (
                'id_reg' => 4723,
                'id_regmf' => 3657,
                'doors' => 5,
            ),
            223 => 
            array (
                'id_reg' => 4724,
                'id_regmf' => 3658,
                'doors' => 2,
            ),
            224 => 
            array (
                'id_reg' => 4725,
                'id_regmf' => 3659,
                'doors' => 2,
            ),
            225 => 
            array (
                'id_reg' => 4726,
                'id_regmf' => 3660,
                'doors' => 1,
            ),
            226 => 
            array (
                'id_reg' => 4727,
                'id_regmf' => 3661,
                'doors' => 0,
            ),
            227 => 
            array (
                'id_reg' => 4728,
                'id_regmf' => 3662,
                'doors' => 0,
            ),
            228 => 
            array (
                'id_reg' => 4729,
                'id_regmf' => 3663,
                'doors' => 0,
            ),
            229 => 
            array (
                'id_reg' => 4730,
                'id_regmf' => 3665,
                'doors' => 2,
            ),
            230 => 
            array (
                'id_reg' => 4731,
                'id_regmf' => 3664,
                'doors' => 4,
            ),
            231 => 
            array (
                'id_reg' => 4732,
                'id_regmf' => 3666,
                'doors' => 4,
            ),
            232 => 
            array (
                'id_reg' => 4733,
                'id_regmf' => 3667,
                'doors' => 4,
            ),
            233 => 
            array (
                'id_reg' => 4734,
                'id_regmf' => 3668,
                'doors' => 3,
            ),
            234 => 
            array (
                'id_reg' => 4735,
                'id_regmf' => 3669,
                'doors' => 2,
            ),
            235 => 
            array (
                'id_reg' => 4736,
                'id_regmf' => 3670,
                'doors' => 2,
            ),
            236 => 
            array (
                'id_reg' => 4737,
                'id_regmf' => 3671,
                'doors' => 2,
            ),
            237 => 
            array (
                'id_reg' => 4738,
                'id_regmf' => 3672,
                'doors' => 2,
            ),
            238 => 
            array (
                'id_reg' => 4739,
                'id_regmf' => 3673,
                'doors' => 2,
            ),
            239 => 
            array (
                'id_reg' => 4740,
                'id_regmf' => 3674,
                'doors' => 2,
            ),
            240 => 
            array (
                'id_reg' => 4741,
                'id_regmf' => 3675,
                'doors' => 2,
            ),
            241 => 
            array (
                'id_reg' => 4742,
                'id_regmf' => 3676,
                'doors' => 2,
            ),
            242 => 
            array (
                'id_reg' => 4743,
                'id_regmf' => 3677,
                'doors' => 2,
            ),
            243 => 
            array (
                'id_reg' => 4744,
                'id_regmf' => 3678,
                'doors' => 2,
            ),
            244 => 
            array (
                'id_reg' => 4745,
                'id_regmf' => 3679,
                'doors' => 2,
            ),
            245 => 
            array (
                'id_reg' => 4746,
                'id_regmf' => 3680,
                'doors' => 0,
            ),
            246 => 
            array (
                'id_reg' => 4747,
                'id_regmf' => 3681,
                'doors' => 0,
            ),
            247 => 
            array (
                'id_reg' => 4748,
                'id_regmf' => 3682,
                'doors' => 2,
            ),
            248 => 
            array (
                'id_reg' => 4749,
                'id_regmf' => 3683,
                'doors' => 2,
            ),
            249 => 
            array (
                'id_reg' => 4750,
                'id_regmf' => 3684,
                'doors' => 2,
            ),
            250 => 
            array (
                'id_reg' => 4751,
                'id_regmf' => 3685,
                'doors' => 2,
            ),
            251 => 
            array (
                'id_reg' => 4752,
                'id_regmf' => 3686,
                'doors' => 2,
            ),
            252 => 
            array (
                'id_reg' => 4753,
                'id_regmf' => 3687,
                'doors' => 2,
            ),
            253 => 
            array (
                'id_reg' => 4754,
                'id_regmf' => 3688,
                'doors' => 2,
            ),
            254 => 
            array (
                'id_reg' => 4755,
                'id_regmf' => 3689,
                'doors' => 2,
            ),
            255 => 
            array (
                'id_reg' => 4756,
                'id_regmf' => 3690,
                'doors' => 2,
            ),
            256 => 
            array (
                'id_reg' => 4757,
                'id_regmf' => 3691,
                'doors' => 4,
            ),
            257 => 
            array (
                'id_reg' => 4758,
                'id_regmf' => 3692,
                'doors' => 4,
            ),
            258 => 
            array (
                'id_reg' => 4759,
                'id_regmf' => 3693,
                'doors' => 2,
            ),
            259 => 
            array (
                'id_reg' => 4760,
                'id_regmf' => 3693,
                'doors' => 4,
            ),
            260 => 
            array (
                'id_reg' => 4761,
                'id_regmf' => 3694,
                'doors' => 4,
            ),
            261 => 
            array (
                'id_reg' => 4762,
                'id_regmf' => 3695,
                'doors' => 4,
            ),
            262 => 
            array (
                'id_reg' => 4763,
                'id_regmf' => 3696,
                'doors' => 2,
            ),
            263 => 
            array (
                'id_reg' => 4764,
                'id_regmf' => 3698,
                'doors' => 2,
            ),
            264 => 
            array (
                'id_reg' => 4765,
                'id_regmf' => 3698,
                'doors' => 3,
            ),
            265 => 
            array (
                'id_reg' => 4766,
                'id_regmf' => 3697,
                'doors' => 3,
            ),
            266 => 
            array (
                'id_reg' => 4767,
                'id_regmf' => 3700,
                'doors' => 2,
            ),
            267 => 
            array (
                'id_reg' => 4768,
                'id_regmf' => 3700,
                'doors' => 3,
            ),
            268 => 
            array (
                'id_reg' => 4769,
                'id_regmf' => 3699,
                'doors' => 3,
            ),
            269 => 
            array (
                'id_reg' => 4770,
                'id_regmf' => 3699,
                'doors' => 4,
            ),
            270 => 
            array (
                'id_reg' => 4771,
                'id_regmf' => 3701,
                'doors' => 3,
            ),
            271 => 
            array (
                'id_reg' => 4772,
                'id_regmf' => 3702,
                'doors' => 3,
            ),
            272 => 
            array (
                'id_reg' => 4773,
                'id_regmf' => 3703,
                'doors' => 0,
            ),
            273 => 
            array (
                'id_reg' => 4774,
                'id_regmf' => 3704,
                'doors' => 0,
            ),
            274 => 
            array (
                'id_reg' => 4775,
                'id_regmf' => 3705,
                'doors' => 5,
            ),
            275 => 
            array (
                'id_reg' => 4776,
                'id_regmf' => 3706,
                'doors' => 3,
            ),
            276 => 
            array (
                'id_reg' => 4777,
                'id_regmf' => 3707,
                'doors' => 2,
            ),
            277 => 
            array (
                'id_reg' => 4778,
                'id_regmf' => 3707,
                'doors' => 5,
            ),
            278 => 
            array (
                'id_reg' => 4779,
                'id_regmf' => 3708,
                'doors' => 3,
            ),
            279 => 
            array (
                'id_reg' => 4780,
                'id_regmf' => 3710,
                'doors' => 5,
            ),
            280 => 
            array (
                'id_reg' => 4781,
                'id_regmf' => 3709,
                'doors' => 5,
            ),
            281 => 
            array (
                'id_reg' => 4782,
                'id_regmf' => 3712,
                'doors' => 2,
            ),
            282 => 
            array (
                'id_reg' => 4783,
                'id_regmf' => 3712,
                'doors' => 5,
            ),
            283 => 
            array (
                'id_reg' => 4784,
                'id_regmf' => 3711,
                'doors' => 5,
            ),
            284 => 
            array (
                'id_reg' => 4785,
                'id_regmf' => 3713,
                'doors' => 3,
            ),
            285 => 
            array (
                'id_reg' => 4786,
                'id_regmf' => 3714,
                'doors' => 4,
            ),
            286 => 
            array (
                'id_reg' => 4787,
                'id_regmf' => 3715,
                'doors' => 4,
            ),
            287 => 
            array (
                'id_reg' => 4788,
                'id_regmf' => 3716,
                'doors' => 2,
            ),
            288 => 
            array (
                'id_reg' => 4789,
                'id_regmf' => 3717,
                'doors' => 5,
            ),
            289 => 
            array (
                'id_reg' => 4790,
                'id_regmf' => 3718,
                'doors' => 2,
            ),
            290 => 
            array (
                'id_reg' => 4791,
                'id_regmf' => 3719,
                'doors' => 5,
            ),
            291 => 
            array (
                'id_reg' => 4792,
                'id_regmf' => 3720,
                'doors' => 3,
            ),
            292 => 
            array (
                'id_reg' => 4793,
                'id_regmf' => 3721,
                'doors' => 2,
            ),
            293 => 
            array (
                'id_reg' => 4794,
                'id_regmf' => 3722,
                'doors' => 4,
            ),
            294 => 
            array (
                'id_reg' => 4795,
                'id_regmf' => 3723,
                'doors' => 4,
            ),
            295 => 
            array (
                'id_reg' => 4796,
                'id_regmf' => 3724,
                'doors' => 5,
            ),
            296 => 
            array (
                'id_reg' => 4797,
                'id_regmf' => 3725,
                'doors' => 4,
            ),
            297 => 
            array (
                'id_reg' => 4798,
                'id_regmf' => 3726,
                'doors' => 4,
            ),
            298 => 
            array (
                'id_reg' => 4799,
                'id_regmf' => 3727,
                'doors' => 5,
            ),
            299 => 
            array (
                'id_reg' => 4800,
                'id_regmf' => 3728,
                'doors' => 5,
            ),
            300 => 
            array (
                'id_reg' => 4801,
                'id_regmf' => 3729,
                'doors' => 3,
            ),
            301 => 
            array (
                'id_reg' => 4802,
                'id_regmf' => 3730,
                'doors' => 3,
            ),
            302 => 
            array (
                'id_reg' => 4803,
                'id_regmf' => 3731,
                'doors' => 3,
            ),
            303 => 
            array (
                'id_reg' => 4804,
                'id_regmf' => 3732,
                'doors' => 4,
            ),
            304 => 
            array (
                'id_reg' => 4805,
                'id_regmf' => 3732,
                'doors' => 5,
            ),
            305 => 
            array (
                'id_reg' => 4806,
                'id_regmf' => 3733,
                'doors' => 0,
            ),
            306 => 
            array (
                'id_reg' => 4807,
                'id_regmf' => 3734,
                'doors' => 2,
            ),
            307 => 
            array (
                'id_reg' => 4808,
                'id_regmf' => 3735,
                'doors' => 1,
            ),
            308 => 
            array (
                'id_reg' => 4809,
                'id_regmf' => 3736,
                'doors' => 1,
            ),
            309 => 
            array (
                'id_reg' => 4810,
                'id_regmf' => 3737,
                'doors' => 1,
            ),
            310 => 
            array (
                'id_reg' => 4811,
                'id_regmf' => 3739,
                'doors' => 3,
            ),
            311 => 
            array (
                'id_reg' => 4812,
                'id_regmf' => 3738,
                'doors' => 3,
            ),
            312 => 
            array (
                'id_reg' => 4813,
                'id_regmf' => 3738,
                'doors' => 5,
            ),
            313 => 
            array (
                'id_reg' => 4814,
                'id_regmf' => 3740,
                'doors' => 2,
            ),
            314 => 
            array (
                'id_reg' => 4815,
                'id_regmf' => 3741,
                'doors' => 0,
            ),
            315 => 
            array (
                'id_reg' => 4816,
                'id_regmf' => 3746,
                'doors' => 4,
            ),
            316 => 
            array (
                'id_reg' => 4817,
                'id_regmf' => 3747,
                'doors' => 2,
            ),
            317 => 
            array (
                'id_reg' => 4818,
                'id_regmf' => 3742,
                'doors' => 2,
            ),
            318 => 
            array (
                'id_reg' => 4819,
                'id_regmf' => 3743,
                'doors' => 4,
            ),
            319 => 
            array (
                'id_reg' => 4820,
                'id_regmf' => 3744,
                'doors' => 2,
            ),
            320 => 
            array (
                'id_reg' => 4821,
                'id_regmf' => 3744,
                'doors' => 4,
            ),
            321 => 
            array (
                'id_reg' => 4822,
                'id_regmf' => 3745,
                'doors' => 4,
            ),
            322 => 
            array (
                'id_reg' => 4823,
                'id_regmf' => 3748,
                'doors' => 2,
            ),
            323 => 
            array (
                'id_reg' => 4824,
                'id_regmf' => 3748,
                'doors' => 4,
            ),
            324 => 
            array (
                'id_reg' => 4825,
                'id_regmf' => 3749,
                'doors' => 2,
            ),
            325 => 
            array (
                'id_reg' => 4826,
                'id_regmf' => 3750,
                'doors' => 2,
            ),
            326 => 
            array (
                'id_reg' => 4827,
                'id_regmf' => 3750,
                'doors' => 3,
            ),
            327 => 
            array (
                'id_reg' => 4828,
                'id_regmf' => 3751,
                'doors' => 2,
            ),
            328 => 
            array (
                'id_reg' => 4829,
                'id_regmf' => 3751,
                'doors' => 3,
            ),
            329 => 
            array (
                'id_reg' => 4830,
                'id_regmf' => 3752,
                'doors' => 5,
            ),
            330 => 
            array (
                'id_reg' => 4831,
                'id_regmf' => 3753,
                'doors' => 4,
            ),
            331 => 
            array (
                'id_reg' => 4832,
                'id_regmf' => 3753,
                'doors' => 5,
            ),
            332 => 
            array (
                'id_reg' => 4833,
                'id_regmf' => 3754,
                'doors' => 4,
            ),
            333 => 
            array (
                'id_reg' => 4834,
                'id_regmf' => 3754,
                'doors' => 5,
            ),
            334 => 
            array (
                'id_reg' => 4835,
                'id_regmf' => 3755,
                'doors' => 3,
            ),
            335 => 
            array (
                'id_reg' => 4836,
                'id_regmf' => 3755,
                'doors' => 5,
            ),
            336 => 
            array (
                'id_reg' => 4837,
                'id_regmf' => 3756,
                'doors' => 4,
            ),
            337 => 
            array (
                'id_reg' => 4838,
                'id_regmf' => 3758,
                'doors' => 4,
            ),
            338 => 
            array (
                'id_reg' => 4839,
                'id_regmf' => 3758,
                'doors' => 5,
            ),
            339 => 
            array (
                'id_reg' => 4840,
                'id_regmf' => 3758,
                'doors' => 6,
            ),
            340 => 
            array (
                'id_reg' => 4841,
                'id_regmf' => 3759,
                'doors' => 2,
            ),
            341 => 
            array (
                'id_reg' => 4842,
                'id_regmf' => 3759,
                'doors' => 4,
            ),
            342 => 
            array (
                'id_reg' => 4843,
                'id_regmf' => 3759,
                'doors' => 5,
            ),
            343 => 
            array (
                'id_reg' => 4844,
                'id_regmf' => 3759,
                'doors' => 6,
            ),
            344 => 
            array (
                'id_reg' => 4845,
                'id_regmf' => 3757,
                'doors' => 2,
            ),
            345 => 
            array (
                'id_reg' => 4846,
                'id_regmf' => 3757,
                'doors' => 4,
            ),
            346 => 
            array (
                'id_reg' => 4847,
                'id_regmf' => 3757,
                'doors' => 5,
            ),
            347 => 
            array (
                'id_reg' => 4848,
                'id_regmf' => 3757,
                'doors' => 6,
            ),
            348 => 
            array (
                'id_reg' => 4849,
                'id_regmf' => 3761,
                'doors' => 4,
            ),
            349 => 
            array (
                'id_reg' => 4850,
                'id_regmf' => 3761,
                'doors' => 5,
            ),
            350 => 
            array (
                'id_reg' => 4851,
                'id_regmf' => 3762,
                'doors' => 4,
            ),
            351 => 
            array (
                'id_reg' => 4852,
                'id_regmf' => 3762,
                'doors' => 5,
            ),
            352 => 
            array (
                'id_reg' => 4853,
                'id_regmf' => 3760,
                'doors' => 4,
            ),
            353 => 
            array (
                'id_reg' => 4854,
                'id_regmf' => 3760,
                'doors' => 5,
            ),
            354 => 
            array (
                'id_reg' => 4855,
                'id_regmf' => 3764,
                'doors' => 4,
            ),
            355 => 
            array (
                'id_reg' => 4856,
                'id_regmf' => 3763,
                'doors' => 4,
            ),
            356 => 
            array (
                'id_reg' => 4857,
                'id_regmf' => 3763,
                'doors' => 5,
            ),
            357 => 
            array (
                'id_reg' => 4858,
                'id_regmf' => 3766,
                'doors' => 3,
            ),
            358 => 
            array (
                'id_reg' => 4859,
                'id_regmf' => 3766,
                'doors' => 4,
            ),
            359 => 
            array (
                'id_reg' => 4860,
                'id_regmf' => 3765,
                'doors' => 3,
            ),
            360 => 
            array (
                'id_reg' => 4861,
                'id_regmf' => 3765,
                'doors' => 4,
            ),
            361 => 
            array (
                'id_reg' => 4862,
                'id_regmf' => 3765,
                'doors' => 5,
            ),
            362 => 
            array (
                'id_reg' => 4863,
                'id_regmf' => 3768,
                'doors' => 4,
            ),
            363 => 
            array (
                'id_reg' => 4864,
                'id_regmf' => 3767,
                'doors' => 4,
            ),
            364 => 
            array (
                'id_reg' => 4865,
                'id_regmf' => 3769,
                'doors' => 2,
            ),
            365 => 
            array (
                'id_reg' => 4866,
                'id_regmf' => 3771,
                'doors' => 4,
            ),
            366 => 
            array (
                'id_reg' => 4867,
                'id_regmf' => 3771,
                'doors' => 5,
            ),
            367 => 
            array (
                'id_reg' => 4868,
                'id_regmf' => 3770,
                'doors' => 4,
            ),
            368 => 
            array (
                'id_reg' => 4869,
                'id_regmf' => 3770,
                'doors' => 5,
            ),
            369 => 
            array (
                'id_reg' => 4870,
                'id_regmf' => 3772,
                'doors' => 3,
            ),
            370 => 
            array (
                'id_reg' => 4871,
                'id_regmf' => 3773,
                'doors' => 2,
            ),
            371 => 
            array (
                'id_reg' => 4872,
                'id_regmf' => 3773,
                'doors' => 4,
            ),
            372 => 
            array (
                'id_reg' => 4873,
                'id_regmf' => 3773,
                'doors' => 5,
            ),
            373 => 
            array (
                'id_reg' => 4874,
                'id_regmf' => 3774,
                'doors' => 5,
            ),
            374 => 
            array (
                'id_reg' => 4875,
                'id_regmf' => 3776,
                'doors' => 5,
            ),
            375 => 
            array (
                'id_reg' => 4876,
                'id_regmf' => 3775,
                'doors' => 5,
            ),
            376 => 
            array (
                'id_reg' => 4877,
                'id_regmf' => 3777,
                'doors' => 3,
            ),
            377 => 
            array (
                'id_reg' => 4878,
                'id_regmf' => 3777,
                'doors' => 5,
            ),
            378 => 
            array (
                'id_reg' => 4879,
                'id_regmf' => 3778,
                'doors' => 3,
            ),
            379 => 
            array (
                'id_reg' => 4880,
                'id_regmf' => 3778,
                'doors' => 5,
            ),
            380 => 
            array (
                'id_reg' => 4881,
                'id_regmf' => 3779,
                'doors' => 5,
            ),
            381 => 
            array (
                'id_reg' => 4882,
                'id_regmf' => 3780,
                'doors' => 5,
            ),
            382 => 
            array (
                'id_reg' => 4883,
                'id_regmf' => 3781,
                'doors' => 2,
            ),
            383 => 
            array (
                'id_reg' => 4884,
                'id_regmf' => 3781,
                'doors' => 4,
            ),
            384 => 
            array (
                'id_reg' => 4885,
                'id_regmf' => 3783,
                'doors' => 2,
            ),
            385 => 
            array (
                'id_reg' => 4886,
                'id_regmf' => 3782,
                'doors' => 2,
            ),
            386 => 
            array (
                'id_reg' => 4887,
                'id_regmf' => 3784,
                'doors' => 2,
            ),
            387 => 
            array (
                'id_reg' => 4888,
                'id_regmf' => 3785,
                'doors' => 4,
            ),
            388 => 
            array (
                'id_reg' => 4889,
                'id_regmf' => 3787,
                'doors' => 3,
            ),
            389 => 
            array (
                'id_reg' => 4890,
                'id_regmf' => 3786,
                'doors' => 3,
            ),
            390 => 
            array (
                'id_reg' => 4891,
                'id_regmf' => 3788,
                'doors' => 4,
            ),
            391 => 
            array (
                'id_reg' => 4892,
                'id_regmf' => 3789,
                'doors' => 2,
            ),
            392 => 
            array (
                'id_reg' => 4893,
                'id_regmf' => 3790,
                'doors' => 5,
            ),
            393 => 
            array (
                'id_reg' => 4894,
                'id_regmf' => 3792,
                'doors' => 5,
            ),
            394 => 
            array (
                'id_reg' => 4895,
                'id_regmf' => 3791,
                'doors' => 5,
            ),
            395 => 
            array (
                'id_reg' => 4896,
                'id_regmf' => 3793,
                'doors' => 5,
            ),
            396 => 
            array (
                'id_reg' => 4897,
                'id_regmf' => 3794,
                'doors' => 5,
            ),
            397 => 
            array (
                'id_reg' => 4898,
                'id_regmf' => 3796,
                'doors' => 5,
            ),
            398 => 
            array (
                'id_reg' => 4899,
                'id_regmf' => 3795,
                'doors' => 5,
            ),
            399 => 
            array (
                'id_reg' => 4900,
                'id_regmf' => 3797,
                'doors' => 5,
            ),
            400 => 
            array (
                'id_reg' => 4901,
                'id_regmf' => 3798,
                'doors' => 4,
            ),
            401 => 
            array (
                'id_reg' => 4902,
                'id_regmf' => 3799,
                'doors' => 2,
            ),
            402 => 
            array (
                'id_reg' => 4903,
                'id_regmf' => 3800,
                'doors' => 2,
            ),
            403 => 
            array (
                'id_reg' => 4904,
                'id_regmf' => 3801,
                'doors' => 2,
            ),
            404 => 
            array (
                'id_reg' => 4905,
                'id_regmf' => 3802,
                'doors' => 4,
            ),
            405 => 
            array (
                'id_reg' => 4906,
                'id_regmf' => 3803,
                'doors' => 4,
            ),
            406 => 
            array (
                'id_reg' => 4907,
                'id_regmf' => 3805,
                'doors' => 5,
            ),
            407 => 
            array (
                'id_reg' => 4908,
                'id_regmf' => 3804,
                'doors' => 2,
            ),
            408 => 
            array (
                'id_reg' => 4909,
                'id_regmf' => 3804,
                'doors' => 4,
            ),
            409 => 
            array (
                'id_reg' => 4910,
                'id_regmf' => 3804,
                'doors' => 5,
            ),
            410 => 
            array (
                'id_reg' => 4911,
                'id_regmf' => 3806,
                'doors' => 2,
            ),
            411 => 
            array (
                'id_reg' => 4912,
                'id_regmf' => 3807,
                'doors' => 2,
            ),
            412 => 
            array (
                'id_reg' => 4913,
                'id_regmf' => 3809,
                'doors' => 3,
            ),
            413 => 
            array (
                'id_reg' => 4914,
                'id_regmf' => 3808,
                'doors' => 3,
            ),
            414 => 
            array (
                'id_reg' => 4915,
                'id_regmf' => 3810,
                'doors' => 4,
            ),
            415 => 
            array (
                'id_reg' => 4916,
                'id_regmf' => 3812,
                'doors' => 4,
            ),
            416 => 
            array (
                'id_reg' => 4917,
                'id_regmf' => 3812,
                'doors' => 5,
            ),
            417 => 
            array (
                'id_reg' => 4918,
                'id_regmf' => 3811,
                'doors' => 4,
            ),
            418 => 
            array (
                'id_reg' => 4919,
                'id_regmf' => 3811,
                'doors' => 5,
            ),
            419 => 
            array (
                'id_reg' => 4920,
                'id_regmf' => 3813,
                'doors' => 5,
            ),
            420 => 
            array (
                'id_reg' => 4921,
                'id_regmf' => 3815,
                'doors' => 5,
            ),
            421 => 
            array (
                'id_reg' => 4922,
                'id_regmf' => 3814,
                'doors' => 5,
            ),
            422 => 
            array (
                'id_reg' => 4923,
                'id_regmf' => 3817,
                'doors' => 4,
            ),
            423 => 
            array (
                'id_reg' => 4924,
                'id_regmf' => 3816,
                'doors' => 4,
            ),
            424 => 
            array (
                'id_reg' => 4925,
                'id_regmf' => 3819,
                'doors' => 4,
            ),
            425 => 
            array (
                'id_reg' => 4926,
                'id_regmf' => 3818,
                'doors' => 4,
            ),
            426 => 
            array (
                'id_reg' => 4927,
                'id_regmf' => 3820,
                'doors' => 5,
            ),
            427 => 
            array (
                'id_reg' => 4928,
                'id_regmf' => 3821,
                'doors' => 3,
            ),
            428 => 
            array (
                'id_reg' => 4929,
                'id_regmf' => 3822,
                'doors' => 5,
            ),
            429 => 
            array (
                'id_reg' => 4930,
                'id_regmf' => 3823,
                'doors' => 2,
            ),
            430 => 
            array (
                'id_reg' => 4931,
                'id_regmf' => 3823,
                'doors' => 4,
            ),
            431 => 
            array (
                'id_reg' => 4932,
                'id_regmf' => 3825,
                'doors' => 4,
            ),
            432 => 
            array (
                'id_reg' => 4933,
                'id_regmf' => 3824,
                'doors' => 4,
            ),
            433 => 
            array (
                'id_reg' => 4934,
                'id_regmf' => 3827,
                'doors' => 5,
            ),
            434 => 
            array (
                'id_reg' => 4935,
                'id_regmf' => 3826,
                'doors' => 5,
            ),
            435 => 
            array (
                'id_reg' => 4936,
                'id_regmf' => 3828,
                'doors' => 2,
            ),
            436 => 
            array (
                'id_reg' => 4937,
                'id_regmf' => 3829,
                'doors' => 2,
            ),
            437 => 
            array (
                'id_reg' => 4938,
                'id_regmf' => 3830,
                'doors' => 2,
            ),
            438 => 
            array (
                'id_reg' => 4939,
                'id_regmf' => 3831,
                'doors' => 2,
            ),
            439 => 
            array (
                'id_reg' => 4940,
                'id_regmf' => 3832,
                'doors' => 3,
            ),
            440 => 
            array (
                'id_reg' => 4941,
                'id_regmf' => 3833,
                'doors' => 4,
            ),
            441 => 
            array (
                'id_reg' => 4942,
                'id_regmf' => 3833,
                'doors' => 5,
            ),
            442 => 
            array (
                'id_reg' => 4943,
                'id_regmf' => 3834,
                'doors' => 4,
            ),
            443 => 
            array (
                'id_reg' => 4944,
                'id_regmf' => 3834,
                'doors' => 5,
            ),
            444 => 
            array (
                'id_reg' => 4945,
                'id_regmf' => 3835,
                'doors' => 2,
            ),
            445 => 
            array (
                'id_reg' => 4946,
                'id_regmf' => 3836,
                'doors' => 4,
            ),
            446 => 
            array (
                'id_reg' => 4947,
                'id_regmf' => 3837,
                'doors' => 5,
            ),
            447 => 
            array (
                'id_reg' => 4948,
                'id_regmf' => 3839,
                'doors' => 2,
            ),
            448 => 
            array (
                'id_reg' => 4949,
                'id_regmf' => 3839,
                'doors' => 4,
            ),
            449 => 
            array (
                'id_reg' => 4950,
                'id_regmf' => 3839,
                'doors' => 5,
            ),
            450 => 
            array (
                'id_reg' => 4951,
                'id_regmf' => 3838,
                'doors' => 2,
            ),
            451 => 
            array (
                'id_reg' => 4952,
                'id_regmf' => 3838,
                'doors' => 4,
            ),
            452 => 
            array (
                'id_reg' => 4953,
                'id_regmf' => 3838,
                'doors' => 5,
            ),
            453 => 
            array (
                'id_reg' => 4954,
                'id_regmf' => 3840,
                'doors' => 2,
            ),
            454 => 
            array (
                'id_reg' => 4955,
                'id_regmf' => 3840,
                'doors' => 4,
            ),
            455 => 
            array (
                'id_reg' => 4956,
                'id_regmf' => 3842,
                'doors' => 3,
            ),
            456 => 
            array (
                'id_reg' => 4957,
                'id_regmf' => 3842,
                'doors' => 5,
            ),
            457 => 
            array (
                'id_reg' => 4958,
                'id_regmf' => 3841,
                'doors' => 5,
            ),
            458 => 
            array (
                'id_reg' => 4959,
                'id_regmf' => 3843,
                'doors' => 3,
            ),
            459 => 
            array (
                'id_reg' => 4960,
                'id_regmf' => 3843,
                'doors' => 5,
            ),
            460 => 
            array (
                'id_reg' => 4961,
                'id_regmf' => 3844,
                'doors' => 4,
            ),
            461 => 
            array (
                'id_reg' => 4962,
                'id_regmf' => 3845,
                'doors' => 4,
            ),
            462 => 
            array (
                'id_reg' => 4963,
                'id_regmf' => 3846,
                'doors' => 4,
            ),
            463 => 
            array (
                'id_reg' => 4964,
                'id_regmf' => 3847,
                'doors' => 4,
            ),
            464 => 
            array (
                'id_reg' => 4965,
                'id_regmf' => 3848,
                'doors' => 2,
            ),
            465 => 
            array (
                'id_reg' => 4966,
                'id_regmf' => 3849,
                'doors' => 0,
            ),
            466 => 
            array (
                'id_reg' => 4967,
                'id_regmf' => 3851,
                'doors' => 2,
            ),
            467 => 
            array (
                'id_reg' => 4968,
                'id_regmf' => 3852,
                'doors' => 2,
            ),
            468 => 
            array (
                'id_reg' => 4969,
                'id_regmf' => 3853,
                'doors' => 2,
            ),
            469 => 
            array (
                'id_reg' => 4970,
                'id_regmf' => 3854,
                'doors' => 4,
            ),
            470 => 
            array (
                'id_reg' => 4971,
                'id_regmf' => 3855,
                'doors' => 3,
            ),
            471 => 
            array (
                'id_reg' => 4972,
                'id_regmf' => 3855,
                'doors' => 5,
            ),
            472 => 
            array (
                'id_reg' => 4973,
                'id_regmf' => 3856,
                'doors' => 4,
            ),
            473 => 
            array (
                'id_reg' => 4974,
                'id_regmf' => 3857,
                'doors' => 4,
            ),
            474 => 
            array (
                'id_reg' => 4975,
                'id_regmf' => 3857,
                'doors' => 5,
            ),
            475 => 
            array (
                'id_reg' => 4976,
                'id_regmf' => 3858,
                'doors' => 4,
            ),
            476 => 
            array (
                'id_reg' => 4977,
                'id_regmf' => 3858,
                'doors' => 5,
            ),
            477 => 
            array (
                'id_reg' => 4978,
                'id_regmf' => 3859,
                'doors' => 2,
            ),
            478 => 
            array (
                'id_reg' => 4979,
                'id_regmf' => 3860,
                'doors' => 4,
            ),
            479 => 
            array (
                'id_reg' => 4980,
                'id_regmf' => 3861,
                'doors' => 4,
            ),
            480 => 
            array (
                'id_reg' => 4981,
                'id_regmf' => 3863,
                'doors' => 5,
            ),
            481 => 
            array (
                'id_reg' => 4982,
                'id_regmf' => 3862,
                'doors' => 5,
            ),
            482 => 
            array (
                'id_reg' => 4983,
                'id_regmf' => 3864,
                'doors' => 2,
            ),
            483 => 
            array (
                'id_reg' => 4984,
                'id_regmf' => 3865,
                'doors' => 4,
            ),
            484 => 
            array (
                'id_reg' => 4985,
                'id_regmf' => 3866,
                'doors' => 5,
            ),
            485 => 
            array (
                'id_reg' => 4986,
                'id_regmf' => 3867,
                'doors' => 4,
            ),
            486 => 
            array (
                'id_reg' => 4987,
                'id_regmf' => 3869,
                'doors' => 3,
            ),
            487 => 
            array (
                'id_reg' => 4988,
                'id_regmf' => 3869,
                'doors' => 4,
            ),
            488 => 
            array (
                'id_reg' => 4989,
                'id_regmf' => 3869,
                'doors' => 5,
            ),
            489 => 
            array (
                'id_reg' => 4990,
                'id_regmf' => 3868,
                'doors' => 4,
            ),
            490 => 
            array (
                'id_reg' => 4991,
                'id_regmf' => 3868,
                'doors' => 5,
            ),
            491 => 
            array (
                'id_reg' => 4992,
                'id_regmf' => 3870,
                'doors' => 3,
            ),
            492 => 
            array (
                'id_reg' => 4993,
                'id_regmf' => 3871,
                'doors' => 5,
            ),
            493 => 
            array (
                'id_reg' => 4994,
                'id_regmf' => 3872,
                'doors' => 3,
            ),
            494 => 
            array (
                'id_reg' => 4995,
                'id_regmf' => 3872,
                'doors' => 4,
            ),
            495 => 
            array (
                'id_reg' => 4996,
                'id_regmf' => 3872,
                'doors' => 5,
            ),
            496 => 
            array (
                'id_reg' => 4997,
                'id_regmf' => 3874,
                'doors' => 5,
            ),
            497 => 
            array (
                'id_reg' => 4998,
                'id_regmf' => 3873,
                'doors' => 5,
            ),
            498 => 
            array (
                'id_reg' => 4999,
                'id_regmf' => 3876,
                'doors' => 4,
            ),
            499 => 
            array (
                'id_reg' => 5000,
                'id_regmf' => 3875,
                'doors' => 4,
            ),
        ));
        \DB::table('model_fuel_numdoors')->insert(array (
            0 => 
            array (
                'id_reg' => 5001,
                'id_regmf' => 3877,
                'doors' => 3,
            ),
            1 => 
            array (
                'id_reg' => 5002,
                'id_regmf' => 3878,
                'doors' => 3,
            ),
            2 => 
            array (
                'id_reg' => 5003,
                'id_regmf' => 3850,
                'doors' => 2,
            ),
            3 => 
            array (
                'id_reg' => 5004,
                'id_regmf' => 3879,
                'doors' => 4,
            ),
            4 => 
            array (
                'id_reg' => 5005,
                'id_regmf' => 3879,
                'doors' => 5,
            ),
            5 => 
            array (
                'id_reg' => 5006,
                'id_regmf' => 3880,
                'doors' => 4,
            ),
            6 => 
            array (
                'id_reg' => 5007,
                'id_regmf' => 3880,
                'doors' => 5,
            ),
            7 => 
            array (
                'id_reg' => 5008,
                'id_regmf' => 3882,
                'doors' => 4,
            ),
            8 => 
            array (
                'id_reg' => 5009,
                'id_regmf' => 3881,
                'doors' => 4,
            ),
            9 => 
            array (
                'id_reg' => 5010,
                'id_regmf' => 3883,
                'doors' => 5,
            ),
            10 => 
            array (
                'id_reg' => 5011,
                'id_regmf' => 3884,
                'doors' => 4,
            ),
            11 => 
            array (
                'id_reg' => 5012,
                'id_regmf' => 3884,
                'doors' => 5,
            ),
            12 => 
            array (
                'id_reg' => 5013,
                'id_regmf' => 3885,
                'doors' => 4,
            ),
            13 => 
            array (
                'id_reg' => 5014,
                'id_regmf' => 3886,
                'doors' => 2,
            ),
            14 => 
            array (
                'id_reg' => 5015,
                'id_regmf' => 3887,
                'doors' => 2,
            ),
            15 => 
            array (
                'id_reg' => 5016,
                'id_regmf' => 3888,
                'doors' => 4,
            ),
            16 => 
            array (
                'id_reg' => 5017,
                'id_regmf' => 3888,
                'doors' => 5,
            ),
            17 => 
            array (
                'id_reg' => 5018,
                'id_regmf' => 3889,
                'doors' => 4,
            ),
            18 => 
            array (
                'id_reg' => 5019,
                'id_regmf' => 3889,
                'doors' => 5,
            ),
            19 => 
            array (
                'id_reg' => 5020,
                'id_regmf' => 3890,
                'doors' => 4,
            ),
            20 => 
            array (
                'id_reg' => 5021,
                'id_regmf' => 3890,
                'doors' => 5,
            ),
            21 => 
            array (
                'id_reg' => 5022,
                'id_regmf' => 3891,
                'doors' => 4,
            ),
            22 => 
            array (
                'id_reg' => 5023,
                'id_regmf' => 3891,
                'doors' => 5,
            ),
            23 => 
            array (
                'id_reg' => 5024,
                'id_regmf' => 3892,
                'doors' => 5,
            ),
            24 => 
            array (
                'id_reg' => 5025,
                'id_regmf' => 3894,
                'doors' => 4,
            ),
            25 => 
            array (
                'id_reg' => 5026,
                'id_regmf' => 3894,
                'doors' => 5,
            ),
            26 => 
            array (
                'id_reg' => 5027,
                'id_regmf' => 3893,
                'doors' => 4,
            ),
            27 => 
            array (
                'id_reg' => 5028,
                'id_regmf' => 3895,
                'doors' => 2,
            ),
            28 => 
            array (
                'id_reg' => 5029,
                'id_regmf' => 3895,
                'doors' => 5,
            ),
            29 => 
            array (
                'id_reg' => 5030,
                'id_regmf' => 3898,
                'doors' => 3,
            ),
            30 => 
            array (
                'id_reg' => 5031,
                'id_regmf' => 3897,
                'doors' => 3,
            ),
            31 => 
            array (
                'id_reg' => 5032,
                'id_regmf' => 3896,
                'doors' => 3,
            ),
            32 => 
            array (
                'id_reg' => 5033,
                'id_regmf' => 3899,
                'doors' => 6,
            ),
            33 => 
            array (
                'id_reg' => 5034,
                'id_regmf' => 3901,
                'doors' => 2,
            ),
            34 => 
            array (
                'id_reg' => 5035,
                'id_regmf' => 3900,
                'doors' => 2,
            ),
            35 => 
            array (
                'id_reg' => 5036,
                'id_regmf' => 3902,
                'doors' => 4,
            ),
            36 => 
            array (
                'id_reg' => 5037,
                'id_regmf' => 3902,
                'doors' => 5,
            ),
            37 => 
            array (
                'id_reg' => 5038,
                'id_regmf' => 3903,
                'doors' => 2,
            ),
            38 => 
            array (
                'id_reg' => 5039,
                'id_regmf' => 3904,
                'doors' => 2,
            ),
            39 => 
            array (
                'id_reg' => 5040,
                'id_regmf' => 3905,
                'doors' => 4,
            ),
            40 => 
            array (
                'id_reg' => 5041,
                'id_regmf' => 3906,
                'doors' => 4,
            ),
            41 => 
            array (
                'id_reg' => 5042,
                'id_regmf' => 3907,
                'doors' => 2,
            ),
            42 => 
            array (
                'id_reg' => 5043,
                'id_regmf' => 3908,
                'doors' => 4,
            ),
            43 => 
            array (
                'id_reg' => 5044,
                'id_regmf' => 3910,
                'doors' => 4,
            ),
            44 => 
            array (
                'id_reg' => 5045,
                'id_regmf' => 3909,
                'doors' => 4,
            ),
            45 => 
            array (
                'id_reg' => 5046,
                'id_regmf' => 3911,
                'doors' => 4,
            ),
            46 => 
            array (
                'id_reg' => 5047,
                'id_regmf' => 3913,
                'doors' => 4,
            ),
            47 => 
            array (
                'id_reg' => 5048,
                'id_regmf' => 3914,
                'doors' => 4,
            ),
            48 => 
            array (
                'id_reg' => 5049,
                'id_regmf' => 3912,
                'doors' => 4,
            ),
            49 => 
            array (
                'id_reg' => 5050,
                'id_regmf' => 3916,
                'doors' => 4,
            ),
            50 => 
            array (
                'id_reg' => 5051,
                'id_regmf' => 3915,
                'doors' => 4,
            ),
            51 => 
            array (
                'id_reg' => 5052,
                'id_regmf' => 3918,
                'doors' => 4,
            ),
            52 => 
            array (
                'id_reg' => 5053,
                'id_regmf' => 3917,
                'doors' => 4,
            ),
            53 => 
            array (
                'id_reg' => 5054,
                'id_regmf' => 3919,
                'doors' => 4,
            ),
            54 => 
            array (
                'id_reg' => 5055,
                'id_regmf' => 3922,
                'doors' => 4,
            ),
            55 => 
            array (
                'id_reg' => 5056,
                'id_regmf' => 3921,
                'doors' => 4,
            ),
            56 => 
            array (
                'id_reg' => 5057,
                'id_regmf' => 3920,
                'doors' => 4,
            ),
            57 => 
            array (
                'id_reg' => 5058,
                'id_regmf' => 3924,
                'doors' => 4,
            ),
            58 => 
            array (
                'id_reg' => 5059,
                'id_regmf' => 3923,
                'doors' => 4,
            ),
            59 => 
            array (
                'id_reg' => 5060,
                'id_regmf' => 3925,
                'doors' => 4,
            ),
            60 => 
            array (
                'id_reg' => 5061,
                'id_regmf' => 3927,
                'doors' => 5,
            ),
            61 => 
            array (
                'id_reg' => 5062,
                'id_regmf' => 3926,
                'doors' => 5,
            ),
            62 => 
            array (
                'id_reg' => 5063,
                'id_regmf' => 3929,
                'doors' => 5,
            ),
            63 => 
            array (
                'id_reg' => 5064,
                'id_regmf' => 3928,
                'doors' => 5,
            ),
            64 => 
            array (
                'id_reg' => 5065,
                'id_regmf' => 3930,
                'doors' => 5,
            ),
            65 => 
            array (
                'id_reg' => 5066,
                'id_regmf' => 3932,
                'doors' => 5,
            ),
            66 => 
            array (
                'id_reg' => 5067,
                'id_regmf' => 3931,
                'doors' => 5,
            ),
            67 => 
            array (
                'id_reg' => 5068,
                'id_regmf' => 3935,
                'doors' => 5,
            ),
            68 => 
            array (
                'id_reg' => 5069,
                'id_regmf' => 3934,
                'doors' => 5,
            ),
            69 => 
            array (
                'id_reg' => 5070,
                'id_regmf' => 3933,
                'doors' => 5,
            ),
            70 => 
            array (
                'id_reg' => 5071,
                'id_regmf' => 3937,
                'doors' => 5,
            ),
            71 => 
            array (
                'id_reg' => 5072,
                'id_regmf' => 3936,
                'doors' => 5,
            ),
            72 => 
            array (
                'id_reg' => 5073,
                'id_regmf' => 3939,
                'doors' => 5,
            ),
            73 => 
            array (
                'id_reg' => 5074,
                'id_regmf' => 3938,
                'doors' => 5,
            ),
            74 => 
            array (
                'id_reg' => 5075,
                'id_regmf' => 3940,
                'doors' => 5,
            ),
            75 => 
            array (
                'id_reg' => 5076,
                'id_regmf' => 3941,
                'doors' => 5,
            ),
            76 => 
            array (
                'id_reg' => 5077,
                'id_regmf' => 3942,
                'doors' => 5,
            ),
            77 => 
            array (
                'id_reg' => 5078,
                'id_regmf' => 3945,
                'doors' => 5,
            ),
            78 => 
            array (
                'id_reg' => 5079,
                'id_regmf' => 3943,
                'doors' => 5,
            ),
            79 => 
            array (
                'id_reg' => 5080,
                'id_regmf' => 3944,
                'doors' => 5,
            ),
            80 => 
            array (
                'id_reg' => 5081,
                'id_regmf' => 3947,
                'doors' => 5,
            ),
            81 => 
            array (
                'id_reg' => 5082,
                'id_regmf' => 3946,
                'doors' => 5,
            ),
            82 => 
            array (
                'id_reg' => 5083,
                'id_regmf' => 3948,
                'doors' => 5,
            ),
            83 => 
            array (
                'id_reg' => 5084,
                'id_regmf' => 3949,
                'doors' => 5,
            ),
            84 => 
            array (
                'id_reg' => 5085,
                'id_regmf' => 3951,
                'doors' => 5,
            ),
            85 => 
            array (
                'id_reg' => 5086,
                'id_regmf' => 3950,
                'doors' => 5,
            ),
            86 => 
            array (
                'id_reg' => 5087,
                'id_regmf' => 3952,
                'doors' => 5,
            ),
            87 => 
            array (
                'id_reg' => 5088,
                'id_regmf' => 3954,
                'doors' => 5,
            ),
            88 => 
            array (
                'id_reg' => 5089,
                'id_regmf' => 3953,
                'doors' => 5,
            ),
            89 => 
            array (
                'id_reg' => 5090,
                'id_regmf' => 3955,
                'doors' => 0,
            ),
            90 => 
            array (
                'id_reg' => 5091,
                'id_regmf' => 3956,
                'doors' => 4,
            ),
            91 => 
            array (
                'id_reg' => 5092,
                'id_regmf' => 3957,
                'doors' => 4,
            ),
            92 => 
            array (
                'id_reg' => 5093,
                'id_regmf' => 3957,
                'doors' => 5,
            ),
            93 => 
            array (
                'id_reg' => 5094,
                'id_regmf' => 3958,
                'doors' => 2,
            ),
            94 => 
            array (
                'id_reg' => 5095,
                'id_regmf' => 3959,
                'doors' => 3,
            ),
            95 => 
            array (
                'id_reg' => 5096,
                'id_regmf' => 3960,
                'doors' => 0,
            ),
            96 => 
            array (
                'id_reg' => 5097,
                'id_regmf' => 3961,
                'doors' => 0,
            ),
            97 => 
            array (
                'id_reg' => 5098,
                'id_regmf' => 3962,
                'doors' => 0,
            ),
            98 => 
            array (
                'id_reg' => 5099,
                'id_regmf' => 3963,
                'doors' => 0,
            ),
            99 => 
            array (
                'id_reg' => 5100,
                'id_regmf' => 3963,
                'doors' => 2,
            ),
            100 => 
            array (
                'id_reg' => 5101,
                'id_regmf' => 3964,
                'doors' => 2,
            ),
            101 => 
            array (
                'id_reg' => 5102,
                'id_regmf' => 3965,
                'doors' => 0,
            ),
            102 => 
            array (
                'id_reg' => 5103,
                'id_regmf' => 3966,
                'doors' => 0,
            ),
            103 => 
            array (
                'id_reg' => 5104,
                'id_regmf' => 3966,
                'doors' => 2,
            ),
            104 => 
            array (
                'id_reg' => 5105,
                'id_regmf' => 3967,
                'doors' => 2,
            ),
            105 => 
            array (
                'id_reg' => 5106,
                'id_regmf' => 3968,
                'doors' => 2,
            ),
            106 => 
            array (
                'id_reg' => 5107,
                'id_regmf' => 3969,
                'doors' => 0,
            ),
            107 => 
            array (
                'id_reg' => 5108,
                'id_regmf' => 3970,
                'doors' => 0,
            ),
            108 => 
            array (
                'id_reg' => 5109,
                'id_regmf' => 3972,
                'doors' => 2,
            ),
            109 => 
            array (
                'id_reg' => 5110,
                'id_regmf' => 3972,
                'doors' => 3,
            ),
            110 => 
            array (
                'id_reg' => 5111,
                'id_regmf' => 3971,
                'doors' => 2,
            ),
            111 => 
            array (
                'id_reg' => 5112,
                'id_regmf' => 3973,
                'doors' => 2,
            ),
            112 => 
            array (
                'id_reg' => 5113,
                'id_regmf' => 3974,
                'doors' => 2,
            ),
            113 => 
            array (
                'id_reg' => 5114,
                'id_regmf' => 3975,
                'doors' => 2,
            ),
            114 => 
            array (
                'id_reg' => 5115,
                'id_regmf' => 3976,
                'doors' => 4,
            ),
            115 => 
            array (
                'id_reg' => 5116,
                'id_regmf' => 3977,
                'doors' => 0,
            ),
            116 => 
            array (
                'id_reg' => 5117,
                'id_regmf' => 3978,
                'doors' => 0,
            ),
            117 => 
            array (
                'id_reg' => 5118,
                'id_regmf' => 3979,
                'doors' => 0,
            ),
            118 => 
            array (
                'id_reg' => 5119,
                'id_regmf' => 3980,
                'doors' => 0,
            ),
            119 => 
            array (
                'id_reg' => 5120,
                'id_regmf' => 3981,
                'doors' => 2,
            ),
            120 => 
            array (
                'id_reg' => 5121,
                'id_regmf' => 3982,
                'doors' => 2,
            ),
            121 => 
            array (
                'id_reg' => 5122,
                'id_regmf' => 3983,
                'doors' => 0,
            ),
            122 => 
            array (
                'id_reg' => 5123,
                'id_regmf' => 3985,
                'doors' => 0,
            ),
            123 => 
            array (
                'id_reg' => 5124,
                'id_regmf' => 3984,
                'doors' => 0,
            ),
            124 => 
            array (
                'id_reg' => 5125,
                'id_regmf' => 3986,
                'doors' => 0,
            ),
            125 => 
            array (
                'id_reg' => 5126,
                'id_regmf' => 3987,
                'doors' => 0,
            ),
            126 => 
            array (
                'id_reg' => 5127,
                'id_regmf' => 3988,
                'doors' => 0,
            ),
            127 => 
            array (
                'id_reg' => 5128,
                'id_regmf' => 3989,
                'doors' => 0,
            ),
            128 => 
            array (
                'id_reg' => 5129,
                'id_regmf' => 3990,
                'doors' => 0,
            ),
            129 => 
            array (
                'id_reg' => 5130,
                'id_regmf' => 3991,
                'doors' => 2,
            ),
            130 => 
            array (
                'id_reg' => 5131,
                'id_regmf' => 3992,
                'doors' => 0,
            ),
            131 => 
            array (
                'id_reg' => 5132,
                'id_regmf' => 3993,
                'doors' => 0,
            ),
            132 => 
            array (
                'id_reg' => 5133,
                'id_regmf' => 3994,
                'doors' => 0,
            ),
            133 => 
            array (
                'id_reg' => 5134,
                'id_regmf' => 3995,
                'doors' => 3,
            ),
            134 => 
            array (
                'id_reg' => 5135,
                'id_regmf' => 3996,
                'doors' => 3,
            ),
            135 => 
            array (
                'id_reg' => 5136,
                'id_regmf' => 3997,
                'doors' => 3,
            ),
            136 => 
            array (
                'id_reg' => 5137,
                'id_regmf' => 3998,
                'doors' => 5,
            ),
            137 => 
            array (
                'id_reg' => 5138,
                'id_regmf' => 3999,
                'doors' => 0,
            ),
            138 => 
            array (
                'id_reg' => 5139,
                'id_regmf' => 4000,
                'doors' => 3,
            ),
            139 => 
            array (
                'id_reg' => 5140,
                'id_regmf' => 4001,
                'doors' => 5,
            ),
            140 => 
            array (
                'id_reg' => 5141,
                'id_regmf' => 4002,
                'doors' => 0,
            ),
            141 => 
            array (
                'id_reg' => 5142,
                'id_regmf' => 4003,
                'doors' => 0,
            ),
            142 => 
            array (
                'id_reg' => 5143,
                'id_regmf' => 4004,
                'doors' => 0,
            ),
            143 => 
            array (
                'id_reg' => 5144,
                'id_regmf' => 4005,
                'doors' => 0,
            ),
            144 => 
            array (
                'id_reg' => 5145,
                'id_regmf' => 4006,
                'doors' => 2,
            ),
            145 => 
            array (
                'id_reg' => 5146,
                'id_regmf' => 4006,
                'doors' => 4,
            ),
        ));
        
        
    }
}