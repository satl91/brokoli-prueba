<?php

use Illuminate\Database\Seeder;

class AutomobileDoorsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('automobile_doors')->delete();
        
        \DB::table('automobile_doors')->insert(array (
            0 => 
            array (
                'id_doors' => 1,
                'int_numdoors' => 2,
            ),
            1 => 
            array (
                'id_doors' => 2,
                'int_numdoors' => 3,
            ),
            2 => 
            array (
                'id_doors' => 3,
                'int_numdoors' => 4,
            ),
            3 => 
            array (
                'id_doors' => 4,
                'int_numdoors' => 5,
            ),
        ));
        
        
    }
}