<?php

use Illuminate\Database\Seeder;

class AutomobileModelsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('automobile_models')->delete();
        
        \DB::table('automobile_models')->insert(array (
            0 => 
            array (
                'id_model' => 1,
                'id_manufacturer' => 1,
                'str_description' => '124 spider',
                'str_description_slug' => '124-spider',
                'bol_active' => 1,
            ),
            1 => 
            array (
                'id_model' => 2,
                'id_manufacturer' => 1,
                'str_description' => '500',
                'str_description_slug' => '500',
                'bol_active' => 1,
            ),
            2 => 
            array (
                'id_model' => 3,
                'id_manufacturer' => 1,
                'str_description' => '500c',
                'str_description_slug' => '500c',
                'bol_active' => 1,
            ),
            3 => 
            array (
                'id_model' => 4,
                'id_manufacturer' => 1,
                'str_description' => '595',
                'str_description_slug' => '595',
                'bol_active' => 1,
            ),
            4 => 
            array (
                'id_model' => 5,
                'id_manufacturer' => 1,
                'str_description' => '595c',
                'str_description_slug' => '595c',
                'bol_active' => 1,
            ),
            5 => 
            array (
                'id_model' => 6,
                'id_manufacturer' => 1,
                'str_description' => '695',
                'str_description_slug' => '695',
                'bol_active' => 1,
            ),
            6 => 
            array (
                'id_model' => 7,
                'id_manufacturer' => 1,
                'str_description' => 'punto',
                'str_description_slug' => 'punto',
                'bol_active' => 1,
            ),
            7 => 
            array (
                'id_model' => 8,
                'id_manufacturer' => 1,
                'str_description' => 'punto evo',
                'str_description_slug' => 'punto-evo',
                'bol_active' => 1,
            ),
            8 => 
            array (
                'id_model' => 9,
                'id_manufacturer' => 2,
                'str_description' => 'cobra',
                'str_description_slug' => 'cobra',
                'bol_active' => 1,
            ),
            9 => 
            array (
                'id_model' => 10,
                'id_manufacturer' => 2,
                'str_description' => 'two litres',
                'str_description_slug' => 'two-litres',
                'bol_active' => 1,
            ),
            10 => 
            array (
                'id_model' => 11,
                'id_manufacturer' => 3,
                'str_description' => 'off',
                'str_description_slug' => 'off',
                'bol_active' => 1,
            ),
            11 => 
            array (
                'id_model' => 12,
                'id_manufacturer' => 4,
                'str_description' => 'integra',
                'str_description_slug' => 'integra',
                'bol_active' => 1,
            ),
            12 => 
            array (
                'id_model' => 13,
                'id_manufacturer' => 4,
                'str_description' => 'legend',
                'str_description_slug' => 'legend',
                'bol_active' => 1,
            ),
            13 => 
            array (
                'id_model' => 14,
                'id_manufacturer' => 4,
                'str_description' => 'mdx',
                'str_description_slug' => 'mdx',
                'bol_active' => 1,
            ),
            14 => 
            array (
                'id_model' => 15,
                'id_manufacturer' => 4,
                'str_description' => 'rsx',
                'str_description_slug' => 'rsx',
                'bol_active' => 1,
            ),
            15 => 
            array (
                'id_model' => 16,
                'id_manufacturer' => 4,
                'str_description' => 'tl',
                'str_description_slug' => 'tl',
                'bol_active' => 1,
            ),
            16 => 
            array (
                'id_model' => 17,
                'id_manufacturer' => 4,
                'str_description' => 'tsx',
                'str_description_slug' => 'tsx',
                'bol_active' => 1,
            ),
            17 => 
            array (
                'id_model' => 18,
                'id_manufacturer' => 4,
                'str_description' => 'zdx',
                'str_description_slug' => 'zdx',
                'bol_active' => 1,
            ),
            18 => 
            array (
                'id_model' => 19,
                'id_manufacturer' => 5,
                'str_description' => 'trumpf junior',
                'str_description_slug' => 'trumpf-junior',
                'bol_active' => 1,
            ),
            19 => 
            array (
                'id_model' => 20,
                'id_manufacturer' => 6,
                'str_description' => 'go kart 125',
                'str_description_slug' => 'go-kart-125',
                'bol_active' => 1,
            ),
            20 => 
            array (
                'id_model' => 21,
                'id_manufacturer' => 7,
                'str_description' => 'cube',
                'str_description_slug' => 'cube',
                'bol_active' => 1,
            ),
            21 => 
            array (
                'id_model' => 22,
                'id_manufacturer' => 8,
                'str_description' => '300',
                'str_description_slug' => '300',
                'bol_active' => 1,
            ),
            22 => 
            array (
                'id_model' => 23,
                'id_manufacturer' => 8,
                'str_description' => '400',
                'str_description_slug' => '400',
                'bol_active' => 1,
            ),
            23 => 
            array (
                'id_model' => 24,
                'id_manufacturer' => 8,
                'str_description' => '500',
                'str_description_slug' => '500',
                'bol_active' => 1,
            ),
            24 => 
            array (
                'id_model' => 25,
                'id_manufacturer' => 8,
                'str_description' => '500-4',
                'str_description_slug' => '500-4',
                'bol_active' => 1,
            ),
            25 => 
            array (
                'id_model' => 26,
                'id_manufacturer' => 8,
                'str_description' => '500-5',
                'str_description_slug' => '500-5',
                'bol_active' => 1,
            ),
            26 => 
            array (
                'id_model' => 27,
                'id_manufacturer' => 8,
                'str_description' => '600',
                'str_description_slug' => '600',
                'bol_active' => 1,
            ),
            27 => 
            array (
                'id_model' => 28,
                'id_manufacturer' => 8,
                'str_description' => 'a 721',
                'str_description_slug' => 'a-721',
                'bol_active' => 1,
            ),
            28 => 
            array (
                'id_model' => 29,
                'id_manufacturer' => 8,
                'str_description' => 'a 741',
                'str_description_slug' => 'a-741',
                'bol_active' => 1,
            ),
            29 => 
            array (
                'id_model' => 30,
                'id_manufacturer' => 8,
                'str_description' => 'a 751',
                'str_description_slug' => 'a-751',
                'bol_active' => 1,
            ),
            30 => 
            array (
                'id_model' => 31,
                'id_manufacturer' => 8,
                'str_description' => 'a-540',
                'str_description_slug' => 'a-540',
                'bol_active' => 1,
            ),
            31 => 
            array (
                'id_model' => 32,
                'id_manufacturer' => 8,
                'str_description' => 'city',
                'str_description_slug' => 'city',
                'bol_active' => 1,
            ),
            32 => 
            array (
                'id_model' => 33,
                'id_manufacturer' => 8,
                'str_description' => 'coupe',
                'str_description_slug' => 'coupe',
                'bol_active' => 1,
            ),
            33 => 
            array (
                'id_model' => 34,
                'id_manufacturer' => 8,
                'str_description' => 'crossline',
                'str_description_slug' => 'crossline',
                'bol_active' => 1,
            ),
            34 => 
            array (
                'id_model' => 35,
                'id_manufacturer' => 8,
                'str_description' => 'crossover',
                'str_description_slug' => 'crossover',
                'bol_active' => 1,
            ),
            35 => 
            array (
                'id_model' => 36,
                'id_manufacturer' => 8,
                'str_description' => 'furgon van',
                'str_description_slug' => 'furgon-van',
                'bol_active' => 1,
            ),
            36 => 
            array (
                'id_model' => 37,
                'id_manufacturer' => 8,
                'str_description' => 'gto',
                'str_description_slug' => 'gto',
                'bol_active' => 1,
            ),
            37 => 
            array (
                'id_model' => 38,
                'id_manufacturer' => 8,
                'str_description' => 'mac',
                'str_description_slug' => 'mac',
                'bol_active' => 1,
            ),
            38 => 
            array (
                'id_model' => 39,
                'id_manufacturer' => 8,
                'str_description' => 'mega',
                'str_description_slug' => 'mega',
                'bol_active' => 1,
            ),
            39 => 
            array (
                'id_model' => 40,
                'id_manufacturer' => 8,
                'str_description' => 'minauto',
                'str_description_slug' => 'minauto',
                'bol_active' => 1,
            ),
            40 => 
            array (
                'id_model' => 41,
                'id_manufacturer' => 8,
                'str_description' => 'minivan',
                'str_description_slug' => 'minivan',
                'bol_active' => 1,
            ),
            41 => 
            array (
                'id_model' => 42,
                'id_manufacturer' => 8,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            42 => 
            array (
                'id_model' => 43,
                'id_manufacturer' => 8,
                'str_description' => 'roadline',
                'str_description_slug' => 'roadline',
                'bol_active' => 1,
            ),
            43 => 
            array (
                'id_model' => 44,
                'id_manufacturer' => 8,
                'str_description' => 'scouty',
                'str_description_slug' => 'scouty',
                'bol_active' => 1,
            ),
            44 => 
            array (
                'id_model' => 45,
                'id_manufacturer' => 8,
                'str_description' => 'vision',
                'str_description_slug' => 'vision',
                'bol_active' => 1,
            ),
            45 => 
            array (
                'id_model' => 46,
                'id_manufacturer' => 9,
                'str_description' => 'condor',
                'str_description_slug' => 'condor',
                'bol_active' => 1,
            ),
            46 => 
            array (
                'id_model' => 47,
                'id_manufacturer' => 10,
                'str_description' => '2141',
                'str_description_slug' => '2141',
                'bol_active' => 1,
            ),
            47 => 
            array (
                'id_model' => 48,
                'id_manufacturer' => 10,
                'str_description' => 'moskrich',
                'str_description_slug' => 'moskrich',
                'bol_active' => 1,
            ),
            48 => 
            array (
                'id_model' => 49,
                'id_manufacturer' => 10,
                'str_description' => 'sl',
                'str_description_slug' => 'sl',
                'bol_active' => 1,
            ),
            49 => 
            array (
                'id_model' => 50,
                'id_manufacturer' => 11,
                'str_description' => '4c',
                'str_description_slug' => '4c',
                'bol_active' => 1,
            ),
            50 => 
            array (
                'id_model' => 51,
                'id_manufacturer' => 11,
                'str_description' => '8c',
                'str_description_slug' => '8c',
                'bol_active' => 1,
            ),
            51 => 
            array (
                'id_model' => 52,
                'id_manufacturer' => 11,
                'str_description' => '33',
                'str_description_slug' => '33',
                'bol_active' => 1,
            ),
            52 => 
            array (
                'id_model' => 53,
                'id_manufacturer' => 11,
                'str_description' => '75',
                'str_description_slug' => '75',
                'bol_active' => 1,
            ),
            53 => 
            array (
                'id_model' => 54,
                'id_manufacturer' => 11,
                'str_description' => '90',
                'str_description_slug' => '90',
                'bol_active' => 1,
            ),
            54 => 
            array (
                'id_model' => 55,
                'id_manufacturer' => 11,
                'str_description' => '155',
                'str_description_slug' => '155',
                'bol_active' => 1,
            ),
            55 => 
            array (
                'id_model' => 56,
                'id_manufacturer' => 11,
                'str_description' => '164',
                'str_description_slug' => '164',
                'bol_active' => 1,
            ),
            56 => 
            array (
                'id_model' => 57,
                'id_manufacturer' => 11,
                'str_description' => '1750',
                'str_description_slug' => '1750',
                'bol_active' => 1,
            ),
            57 => 
            array (
                'id_model' => 58,
                'id_manufacturer' => 11,
                'str_description' => '1900',
                'str_description_slug' => '1900',
                'bol_active' => 1,
            ),
            58 => 
            array (
                'id_model' => 59,
                'id_manufacturer' => 11,
                'str_description' => '2000',
                'str_description_slug' => '2000',
                'bol_active' => 1,
            ),
            59 => 
            array (
                'id_model' => 60,
                'id_manufacturer' => 11,
                'str_description' => 'alfa 6',
                'str_description_slug' => 'alfa-6',
                'bol_active' => 1,
            ),
            60 => 
            array (
                'id_model' => 61,
                'id_manufacturer' => 11,
                'str_description' => 'alfasud',
                'str_description_slug' => 'alfasud',
                'bol_active' => 1,
            ),
            61 => 
            array (
                'id_model' => 62,
                'id_manufacturer' => 11,
                'str_description' => 'alfetta',
                'str_description_slug' => 'alfetta',
                'bol_active' => 1,
            ),
            62 => 
            array (
                'id_model' => 63,
                'id_manufacturer' => 11,
                'str_description' => 'arna',
                'str_description_slug' => 'arna',
                'bol_active' => 1,
            ),
            63 => 
            array (
                'id_model' => 64,
                'id_manufacturer' => 11,
                'str_description' => 'crosswagon',
                'str_description_slug' => 'crosswagon',
                'bol_active' => 1,
            ),
            64 => 
            array (
                'id_model' => 65,
                'id_manufacturer' => 11,
                'str_description' => 'f12',
                'str_description_slug' => 'f12',
                'bol_active' => 1,
            ),
            65 => 
            array (
                'id_model' => 66,
                'id_manufacturer' => 11,
                'str_description' => 'giulia',
                'str_description_slug' => 'giulia',
                'bol_active' => 1,
            ),
            66 => 
            array (
                'id_model' => 67,
                'id_manufacturer' => 11,
                'str_description' => 'gtv',
                'str_description_slug' => 'gtv',
                'bol_active' => 1,
            ),
            67 => 
            array (
                'id_model' => 68,
                'id_manufacturer' => 11,
                'str_description' => 'montreal',
                'str_description_slug' => 'montreal',
                'bol_active' => 1,
            ),
            68 => 
            array (
                'id_model' => 69,
                'id_manufacturer' => 11,
                'str_description' => 'ravello',
                'str_description_slug' => 'ravello',
                'bol_active' => 1,
            ),
            69 => 
            array (
                'id_model' => 70,
                'id_manufacturer' => 11,
                'str_description' => 'spider',
                'str_description_slug' => 'spider',
                'bol_active' => 1,
            ),
            70 => 
            array (
                'id_model' => 71,
                'id_manufacturer' => 11,
                'str_description' => 'sprint',
                'str_description_slug' => 'sprint',
                'bol_active' => 1,
            ),
            71 => 
            array (
                'id_model' => 72,
                'id_manufacturer' => 11,
                'str_description' => 'sz',
                'str_description_slug' => 'sz',
                'bol_active' => 1,
            ),
            72 => 
            array (
                'id_model' => 73,
                'id_manufacturer' => 12,
                'str_description' => 'a610',
                'str_description_slug' => 'a610',
                'bol_active' => 1,
            ),
            73 => 
            array (
                'id_model' => 74,
                'id_manufacturer' => 13,
                'str_description' => 'speed',
                'str_description_slug' => 'speed',
                'bol_active' => 1,
            ),
            74 => 
            array (
                'id_model' => 75,
                'id_manufacturer' => 13,
                'str_description' => 'td21',
                'str_description_slug' => 'td21',
                'bol_active' => 1,
            ),
            75 => 
            array (
                'id_model' => 76,
                'id_manufacturer' => 14,
                'str_description' => 'eagle',
                'str_description_slug' => 'eagle',
                'bol_active' => 1,
            ),
            76 => 
            array (
                'id_model' => 77,
                'id_manufacturer' => 14,
                'str_description' => 'javelin',
                'str_description_slug' => 'javelin',
                'bol_active' => 1,
            ),
            77 => 
            array (
                'id_model' => 78,
                'id_manufacturer' => 14,
                'str_description' => 'jeep',
                'str_description_slug' => 'jeep',
                'bol_active' => 1,
            ),
            78 => 
            array (
                'id_model' => 79,
                'id_manufacturer' => 14,
                'str_description' => 'pacer',
                'str_description_slug' => 'pacer',
                'bol_active' => 1,
            ),
            79 => 
            array (
                'id_model' => 80,
                'id_manufacturer' => 14,
                'str_description' => 'renegade',
                'str_description_slug' => 'renegade',
                'bol_active' => 1,
            ),
            80 => 
            array (
                'id_model' => 81,
                'id_manufacturer' => 14,
                'str_description' => 'the cub',
                'str_description_slug' => 'the-cub',
                'bol_active' => 1,
            ),
            81 => 
            array (
                'id_model' => 82,
                'id_manufacturer' => 14,
                'str_description' => 'wrangler',
                'str_description_slug' => 'wrangler',
                'bol_active' => 1,
            ),
            82 => 
            array (
                'id_model' => 83,
                'id_manufacturer' => 15,
                'str_description' => 'm3',
                'str_description_slug' => 'm3',
                'bol_active' => 1,
            ),
            83 => 
            array (
                'id_model' => 84,
                'id_manufacturer' => 16,
                'str_description' => 'f-100',
                'str_description_slug' => 'f-100',
                'bol_active' => 1,
            ),
            84 => 
            array (
                'id_model' => 85,
                'id_manufacturer' => 17,
                'str_description' => 'prowler',
                'str_description_slug' => 'prowler',
                'bol_active' => 1,
            ),
            85 => 
            array (
                'id_model' => 86,
                'id_manufacturer' => 17,
                'str_description' => 'wildcat',
                'str_description_slug' => 'wildcat',
                'bol_active' => 1,
            ),
            86 => 
            array (
                'id_model' => 87,
                'id_manufacturer' => 18,
                'str_description' => '750',
                'str_description_slug' => '750',
                'bol_active' => 1,
            ),
            87 => 
            array (
                'id_model' => 88,
                'id_manufacturer' => 18,
                'str_description' => '1030',
                'str_description_slug' => '1030',
                'bol_active' => 1,
            ),
            88 => 
            array (
                'id_model' => 89,
                'id_manufacturer' => 18,
                'str_description' => 'avenger 8x8',
                'str_description_slug' => 'avenger-8x8',
                'bol_active' => 1,
            ),
            89 => 
            array (
                'id_model' => 90,
                'id_manufacturer' => 18,
                'str_description' => 'bigfoot 6x6',
                'str_description_slug' => 'bigfoot-6x6',
                'bol_active' => 1,
            ),
            90 => 
            array (
                'id_model' => 91,
                'id_manufacturer' => 18,
                'str_description' => 'conquest 6x6',
                'str_description_slug' => 'conquest-6x6',
                'bol_active' => 1,
            ),
            91 => 
            array (
                'id_model' => 92,
                'id_manufacturer' => 18,
                'str_description' => 'conquest 8x8',
                'str_description_slug' => 'conquest-8x8',
                'bol_active' => 1,
            ),
            92 => 
            array (
                'id_model' => 93,
                'id_manufacturer' => 18,
                'str_description' => 'frontier 6x6',
                'str_description_slug' => 'frontier-6x6',
                'bol_active' => 1,
            ),
            93 => 
            array (
                'id_model' => 94,
                'id_manufacturer' => 18,
                'str_description' => 'frontier 8x8',
                'str_description_slug' => 'frontier-8x8',
                'bol_active' => 1,
            ),
            94 => 
            array (
                'id_model' => 95,
                'id_manufacturer' => 18,
                'str_description' => 'response 8x8',
                'str_description_slug' => 'response-8x8',
                'bol_active' => 1,
            ),
            95 => 
            array (
                'id_model' => 96,
                'id_manufacturer' => 18,
                'str_description' => 'vanguard 6x6',
                'str_description_slug' => 'vanguard-6x6',
                'bol_active' => 1,
            ),
            96 => 
            array (
                'id_model' => 97,
                'id_manufacturer' => 19,
                'str_description' => 'sapphire',
                'str_description_slug' => 'sapphire',
                'bol_active' => 1,
            ),
            97 => 
            array (
                'id_model' => 98,
                'id_manufacturer' => 20,
                'str_description' => '10',
                'str_description_slug' => '10',
                'bol_active' => 1,
            ),
            98 => 
            array (
                'id_model' => 99,
                'id_manufacturer' => 20,
                'str_description' => '243',
                'str_description_slug' => '243',
                'bol_active' => 1,
            ),
            99 => 
            array (
                'id_model' => 100,
                'id_manufacturer' => 20,
                'str_description' => '244',
                'str_description_slug' => '244',
                'bol_active' => 1,
            ),
            100 => 
            array (
                'id_model' => 101,
                'id_manufacturer' => 20,
                'str_description' => '320',
                'str_description_slug' => '320',
                'bol_active' => 1,
            ),
            101 => 
            array (
                'id_model' => 102,
                'id_manufacturer' => 20,
                'str_description' => 'dacia',
                'str_description_slug' => 'dacia',
                'bol_active' => 1,
            ),
            102 => 
            array (
                'id_model' => 103,
                'id_manufacturer' => 20,
                'str_description' => 'expander',
                'str_description_slug' => 'expander',
                'bol_active' => 1,
            ),
            103 => 
            array (
                'id_model' => 104,
                'id_manufacturer' => 21,
                'str_description' => '20',
                'str_description_slug' => '20',
                'bol_active' => 1,
            ),
            104 => 
            array (
                'id_model' => 105,
                'id_manufacturer' => 22,
                'str_description' => 'hi-topic',
                'str_description_slug' => 'hi-topic',
                'bol_active' => 1,
            ),
            105 => 
            array (
                'id_model' => 106,
                'id_manufacturer' => 22,
                'str_description' => 'retona',
                'str_description_slug' => 'retona',
                'bol_active' => 1,
            ),
            106 => 
            array (
                'id_model' => 107,
                'id_manufacturer' => 22,
                'str_description' => 'rocsta',
                'str_description_slug' => 'rocsta',
                'bol_active' => 1,
            ),
            107 => 
            array (
                'id_model' => 108,
                'id_manufacturer' => 23,
                'str_description' => 'shire van',
                'str_description_slug' => 'shire-van',
                'bol_active' => 1,
            ),
            108 => 
            array (
                'id_model' => 109,
                'id_manufacturer' => 24,
                'str_description' => 'cygnet',
                'str_description_slug' => 'cygnet',
                'bol_active' => 1,
            ),
            109 => 
            array (
                'id_model' => 110,
                'id_manufacturer' => 24,
                'str_description' => 'db',
                'str_description_slug' => 'db',
                'bol_active' => 1,
            ),
            110 => 
            array (
                'id_model' => 111,
                'id_manufacturer' => 24,
                'str_description' => 'db2',
                'str_description_slug' => 'db2',
                'bol_active' => 1,
            ),
            111 => 
            array (
                'id_model' => 112,
                'id_manufacturer' => 24,
                'str_description' => 'db5',
                'str_description_slug' => 'db5',
                'bol_active' => 1,
            ),
            112 => 
            array (
                'id_model' => 113,
                'id_manufacturer' => 24,
                'str_description' => 'db6',
                'str_description_slug' => 'db6',
                'bol_active' => 1,
            ),
            113 => 
            array (
                'id_model' => 114,
                'id_manufacturer' => 24,
                'str_description' => 'db7',
                'str_description_slug' => 'db7',
                'bol_active' => 1,
            ),
            114 => 
            array (
                'id_model' => 115,
                'id_manufacturer' => 24,
                'str_description' => 'db9',
                'str_description_slug' => 'db9',
                'bol_active' => 1,
            ),
            115 => 
            array (
                'id_model' => 116,
                'id_manufacturer' => 24,
                'str_description' => 'dbs',
                'str_description_slug' => 'dbs',
                'bol_active' => 1,
            ),
            116 => 
            array (
                'id_model' => 117,
                'id_manufacturer' => 24,
                'str_description' => 'lagonda',
                'str_description_slug' => 'lagonda',
                'bol_active' => 1,
            ),
            117 => 
            array (
                'id_model' => 118,
                'id_manufacturer' => 24,
                'str_description' => 'one 77',
                'str_description_slug' => 'one-77',
                'bol_active' => 1,
            ),
            118 => 
            array (
                'id_model' => 119,
                'id_manufacturer' => 24,
                'str_description' => 'rapide',
                'str_description_slug' => 'rapide',
                'bol_active' => 1,
            ),
            119 => 
            array (
                'id_model' => 120,
                'id_manufacturer' => 24,
                'str_description' => 'v12 vantage',
                'str_description_slug' => 'v12-vantage',
                'bol_active' => 1,
            ),
            120 => 
            array (
                'id_model' => 121,
                'id_manufacturer' => 24,
                'str_description' => 'v12 zagato',
                'str_description_slug' => 'v12-zagato',
                'bol_active' => 1,
            ),
            121 => 
            array (
                'id_model' => 122,
                'id_manufacturer' => 24,
                'str_description' => 'v8',
                'str_description_slug' => 'v8',
                'bol_active' => 1,
            ),
            122 => 
            array (
                'id_model' => 123,
                'id_manufacturer' => 24,
                'str_description' => 'v8 vantage',
                'str_description_slug' => 'v8-vantage',
                'bol_active' => 1,
            ),
            123 => 
            array (
                'id_model' => 124,
                'id_manufacturer' => 24,
                'str_description' => 'vanquish',
                'str_description_slug' => 'vanquish',
                'bol_active' => 1,
            ),
            124 => 
            array (
                'id_model' => 125,
                'id_manufacturer' => 24,
                'str_description' => 'vantage',
                'str_description_slug' => 'vantage',
                'bol_active' => 1,
            ),
            125 => 
            array (
                'id_model' => 126,
                'id_manufacturer' => 24,
                'str_description' => 'virage',
                'str_description_slug' => 'virage',
                'bol_active' => 1,
            ),
            126 => 
            array (
                'id_model' => 127,
                'id_manufacturer' => 24,
                'str_description' => 'volante',
                'str_description_slug' => 'volante',
                'bol_active' => 1,
            ),
            127 => 
            array (
                'id_model' => 128,
                'id_manufacturer' => 24,
                'str_description' => 'zagato',
                'str_description_slug' => 'zagato',
                'bol_active' => 1,
            ),
            128 => 
            array (
                'id_model' => 129,
                'id_manufacturer' => 25,
                'str_description' => '8-100',
                'str_description_slug' => '8-100',
                'bol_active' => 1,
            ),
            129 => 
            array (
                'id_model' => 130,
                'id_manufacturer' => 25,
                'str_description' => '850',
                'str_description_slug' => '850',
                'bol_active' => 1,
            ),
            130 => 
            array (
                'id_model' => 131,
                'id_manufacturer' => 25,
                'str_description' => '852',
                'str_description_slug' => '852',
                'bol_active' => 1,
            ),
            131 => 
            array (
                'id_model' => 132,
                'id_manufacturer' => 26,
                'str_description' => '90',
                'str_description_slug' => '90',
                'bol_active' => 1,
            ),
            132 => 
            array (
                'id_model' => 133,
                'id_manufacturer' => 26,
                'str_description' => '200',
                'str_description_slug' => '200',
                'bol_active' => 1,
            ),
            133 => 
            array (
                'id_model' => 134,
                'id_manufacturer' => 26,
                'str_description' => '500',
                'str_description_slug' => '500',
                'bol_active' => 1,
            ),
            134 => 
            array (
                'id_model' => 135,
                'id_manufacturer' => 26,
                'str_description' => '4000',
                'str_description_slug' => '4000',
                'bol_active' => 1,
            ),
            135 => 
            array (
                'id_model' => 136,
                'id_manufacturer' => 26,
                'str_description' => 'a1',
                'str_description_slug' => 'a1',
                'bol_active' => 1,
            ),
            136 => 
            array (
                'id_model' => 137,
                'id_manufacturer' => 26,
                'str_description' => 'a1 sportback',
                'str_description_slug' => 'a1-sportback',
                'bol_active' => 1,
            ),
            137 => 
            array (
                'id_model' => 138,
                'id_manufacturer' => 26,
                'str_description' => 'a2',
                'str_description_slug' => 'a2',
                'bol_active' => 1,
            ),
            138 => 
            array (
                'id_model' => 139,
                'id_manufacturer' => 26,
                'str_description' => 'a4 allroad quattro',
                'str_description_slug' => 'a4-allroad-quattro',
                'bol_active' => 1,
            ),
            139 => 
            array (
                'id_model' => 140,
                'id_manufacturer' => 26,
                'str_description' => 'a5 sportback',
                'str_description_slug' => 'a5-sportback',
                'bol_active' => 1,
            ),
            140 => 
            array (
                'id_model' => 141,
                'id_manufacturer' => 26,
                'str_description' => 'a6 allroad quattro',
                'str_description_slug' => 'a6-allroad-quattro',
                'bol_active' => 1,
            ),
            141 => 
            array (
                'id_model' => 142,
                'id_manufacturer' => 26,
                'str_description' => 'a7 sportback',
                'str_description_slug' => 'a7-sportback',
                'bol_active' => 1,
            ),
            142 => 
            array (
                'id_model' => 143,
                'id_manufacturer' => 26,
                'str_description' => 'a8',
                'str_description_slug' => 'a8',
                'bol_active' => 1,
            ),
            143 => 
            array (
                'id_model' => 144,
                'id_manufacturer' => 26,
                'str_description' => 'cabriolet',
                'str_description_slug' => 'cabriolet',
                'bol_active' => 1,
            ),
            144 => 
            array (
                'id_model' => 145,
                'id_manufacturer' => 26,
                'str_description' => 'coupe',
                'str_description_slug' => 'coupe',
                'bol_active' => 1,
            ),
            145 => 
            array (
                'id_model' => 146,
                'id_manufacturer' => 26,
                'str_description' => 'q2',
                'str_description_slug' => 'q2',
                'bol_active' => 1,
            ),
            146 => 
            array (
                'id_model' => 147,
                'id_manufacturer' => 26,
                'str_description' => 'q3',
                'str_description_slug' => 'q3',
                'bol_active' => 1,
            ),
            147 => 
            array (
                'id_model' => 148,
                'id_manufacturer' => 26,
                'str_description' => 'q7',
                'str_description_slug' => 'q7',
                'bol_active' => 1,
            ),
            148 => 
            array (
                'id_model' => 149,
                'id_manufacturer' => 26,
                'str_description' => 'quattro',
                'str_description_slug' => 'quattro',
                'bol_active' => 1,
            ),
            149 => 
            array (
                'id_model' => 150,
                'id_manufacturer' => 26,
                'str_description' => 'r8',
                'str_description_slug' => 'r8',
                'bol_active' => 1,
            ),
            150 => 
            array (
                'id_model' => 151,
                'id_manufacturer' => 26,
                'str_description' => 'rs q3',
                'str_description_slug' => 'rs-q3',
                'bol_active' => 1,
            ),
            151 => 
            array (
                'id_model' => 152,
                'id_manufacturer' => 26,
                'str_description' => 'rs2',
                'str_description_slug' => 'rs2',
                'bol_active' => 1,
            ),
            152 => 
            array (
                'id_model' => 153,
                'id_manufacturer' => 26,
                'str_description' => 'rs3 sportback',
                'str_description_slug' => 'rs3-sportback',
                'bol_active' => 1,
            ),
            153 => 
            array (
                'id_model' => 154,
                'id_manufacturer' => 26,
                'str_description' => 'rs4',
                'str_description_slug' => 'rs4',
                'bol_active' => 1,
            ),
            154 => 
            array (
                'id_model' => 155,
                'id_manufacturer' => 26,
                'str_description' => 'rs5',
                'str_description_slug' => 'rs5',
                'bol_active' => 1,
            ),
            155 => 
            array (
                'id_model' => 156,
                'id_manufacturer' => 26,
                'str_description' => 'rs6',
                'str_description_slug' => 'rs6',
                'bol_active' => 1,
            ),
            156 => 
            array (
                'id_model' => 157,
                'id_manufacturer' => 26,
                'str_description' => 'rs7 sportback',
                'str_description_slug' => 'rs7-sportback',
                'bol_active' => 1,
            ),
            157 => 
            array (
                'id_model' => 158,
                'id_manufacturer' => 26,
                'str_description' => 's1',
                'str_description_slug' => 's1',
                'bol_active' => 1,
            ),
            158 => 
            array (
                'id_model' => 159,
                'id_manufacturer' => 26,
                'str_description' => 's1 sportback',
                'str_description_slug' => 's1-sportback',
                'bol_active' => 1,
            ),
            159 => 
            array (
                'id_model' => 160,
                'id_manufacturer' => 26,
                'str_description' => 's2',
                'str_description_slug' => 's2',
                'bol_active' => 1,
            ),
            160 => 
            array (
                'id_model' => 161,
                'id_manufacturer' => 26,
                'str_description' => 's3 sportback',
                'str_description_slug' => 's3-sportback',
                'bol_active' => 1,
            ),
            161 => 
            array (
                'id_model' => 162,
                'id_manufacturer' => 26,
                'str_description' => 's4',
                'str_description_slug' => 's4',
                'bol_active' => 1,
            ),
            162 => 
            array (
                'id_model' => 163,
                'id_manufacturer' => 26,
                'str_description' => 's5',
                'str_description_slug' => 's5',
                'bol_active' => 1,
            ),
            163 => 
            array (
                'id_model' => 164,
                'id_manufacturer' => 26,
                'str_description' => 's5 sportback',
                'str_description_slug' => 's5-sportback',
                'bol_active' => 1,
            ),
            164 => 
            array (
                'id_model' => 165,
                'id_manufacturer' => 26,
                'str_description' => 's6',
                'str_description_slug' => 's6',
                'bol_active' => 1,
            ),
            165 => 
            array (
                'id_model' => 166,
                'id_manufacturer' => 26,
                'str_description' => 's7 sportback',
                'str_description_slug' => 's7-sportback',
                'bol_active' => 1,
            ),
            166 => 
            array (
                'id_model' => 167,
                'id_manufacturer' => 26,
                'str_description' => 's8',
                'str_description_slug' => 's8',
                'bol_active' => 1,
            ),
            167 => 
            array (
                'id_model' => 168,
                'id_manufacturer' => 26,
                'str_description' => 'sport quattro',
                'str_description_slug' => 'sport-quattro',
                'bol_active' => 1,
            ),
            168 => 
            array (
                'id_model' => 169,
                'id_manufacturer' => 26,
                'str_description' => 'sq5',
                'str_description_slug' => 'sq5',
                'bol_active' => 1,
            ),
            169 => 
            array (
                'id_model' => 170,
                'id_manufacturer' => 26,
                'str_description' => 'sq7',
                'str_description_slug' => 'sq7',
                'bol_active' => 1,
            ),
            170 => 
            array (
                'id_model' => 171,
                'id_manufacturer' => 26,
                'str_description' => 'tt rs',
                'str_description_slug' => 'tt-rs',
                'bol_active' => 1,
            ),
            171 => 
            array (
                'id_model' => 172,
                'id_manufacturer' => 26,
                'str_description' => 'tts',
                'str_description_slug' => 'tts',
                'bol_active' => 1,
            ),
            172 => 
            array (
                'id_model' => 173,
                'id_manufacturer' => 26,
                'str_description' => 'v8',
                'str_description_slug' => 'v8',
                'bol_active' => 1,
            ),
            173 => 
            array (
                'id_model' => 174,
                'id_manufacturer' => 27,
                'str_description' => 'm50',
                'str_description_slug' => 'm50',
                'bol_active' => 1,
            ),
            174 => 
            array (
                'id_model' => 175,
                'id_manufacturer' => 27,
                'str_description' => 'ptv',
                'str_description_slug' => 'ptv',
                'bol_active' => 1,
            ),
            175 => 
            array (
                'id_model' => 176,
                'id_manufacturer' => 28,
                'str_description' => '8',
                'str_description_slug' => '8',
                'bol_active' => 1,
            ),
            176 => 
            array (
                'id_model' => 177,
                'id_manufacturer' => 28,
                'str_description' => '10',
                'str_description_slug' => '10',
                'bol_active' => 1,
            ),
            177 => 
            array (
                'id_model' => 178,
                'id_manufacturer' => 28,
                'str_description' => '1000',
                'str_description_slug' => '1000',
                'bol_active' => 1,
            ),
            178 => 
            array (
                'id_model' => 179,
                'id_manufacturer' => 28,
                'str_description' => '1100',
                'str_description_slug' => '1100',
                'bol_active' => 1,
            ),
            179 => 
            array (
                'id_model' => 180,
                'id_manufacturer' => 28,
                'str_description' => '1300',
                'str_description_slug' => '1300',
                'bol_active' => 1,
            ),
            180 => 
            array (
                'id_model' => 181,
                'id_manufacturer' => 28,
                'str_description' => 'a 110',
                'str_description_slug' => 'a-110',
                'bol_active' => 1,
            ),
            181 => 
            array (
                'id_model' => 182,
                'id_manufacturer' => 28,
                'str_description' => 'a 135',
                'str_description_slug' => 'a-135',
                'bol_active' => 1,
            ),
            182 => 
            array (
                'id_model' => 183,
                'id_manufacturer' => 28,
                'str_description' => 'a 30',
                'str_description_slug' => 'a-30',
                'bol_active' => 1,
            ),
            183 => 
            array (
                'id_model' => 184,
                'id_manufacturer' => 28,
                'str_description' => 'a 35',
                'str_description_slug' => 'a-35',
                'bol_active' => 1,
            ),
            184 => 
            array (
                'id_model' => 185,
                'id_manufacturer' => 28,
                'str_description' => 'a 40',
                'str_description_slug' => 'a-40',
                'bol_active' => 1,
            ),
            185 => 
            array (
                'id_model' => 186,
                'id_manufacturer' => 28,
                'str_description' => 'a 50',
                'str_description_slug' => 'a-50',
                'bol_active' => 1,
            ),
            186 => 
            array (
                'id_model' => 187,
                'id_manufacturer' => 28,
                'str_description' => 'a 60',
                'str_description_slug' => 'a-60',
                'bol_active' => 1,
            ),
            187 => 
            array (
                'id_model' => 188,
                'id_manufacturer' => 28,
                'str_description' => 'a 70',
                'str_description_slug' => 'a-70',
                'bol_active' => 1,
            ),
            188 => 
            array (
                'id_model' => 189,
                'id_manufacturer' => 28,
                'str_description' => 'a 95',
                'str_description_slug' => 'a-95',
                'bol_active' => 1,
            ),
            189 => 
            array (
                'id_model' => 190,
                'id_manufacturer' => 28,
                'str_description' => 'a 99',
                'str_description_slug' => 'a-99',
                'bol_active' => 1,
            ),
            190 => 
            array (
                'id_model' => 191,
                'id_manufacturer' => 28,
                'str_description' => 'caravan',
                'str_description_slug' => 'caravan',
                'bol_active' => 1,
            ),
            191 => 
            array (
                'id_model' => 192,
                'id_manufacturer' => 28,
                'str_description' => 'champ',
                'str_description_slug' => 'champ',
                'bol_active' => 1,
            ),
            192 => 
            array (
                'id_model' => 193,
                'id_manufacturer' => 28,
                'str_description' => 'hire car',
                'str_description_slug' => 'hire-car',
                'bol_active' => 1,
            ),
            193 => 
            array (
                'id_model' => 194,
                'id_manufacturer' => 28,
                'str_description' => 'maestro',
                'str_description_slug' => 'maestro',
                'bol_active' => 1,
            ),
            194 => 
            array (
                'id_model' => 195,
                'id_manufacturer' => 28,
                'str_description' => 'maxi 2',
                'str_description_slug' => 'maxi-2',
                'bol_active' => 1,
            ),
            195 => 
            array (
                'id_model' => 196,
                'id_manufacturer' => 28,
                'str_description' => 'mini',
                'str_description_slug' => 'mini',
                'bol_active' => 1,
            ),
            196 => 
            array (
                'id_model' => 197,
                'id_manufacturer' => 28,
                'str_description' => 'seven tourer',
                'str_description_slug' => 'seven-tourer',
                'bol_active' => 1,
            ),
            197 => 
            array (
                'id_model' => 198,
                'id_manufacturer' => 28,
                'str_description' => 'sherpa',
                'str_description_slug' => 'sherpa',
                'bol_active' => 1,
            ),
            198 => 
            array (
                'id_model' => 199,
                'id_manufacturer' => 28,
                'str_description' => 'sprit',
                'str_description_slug' => 'sprit',
                'bol_active' => 1,
            ),
            199 => 
            array (
                'id_model' => 200,
                'id_manufacturer' => 28,
                'str_description' => 'taxi',
                'str_description_slug' => 'taxi',
                'bol_active' => 1,
            ),
            200 => 
            array (
                'id_model' => 201,
                'id_manufacturer' => 28,
                'str_description' => 'vanden plas',
                'str_description_slug' => 'vanden-plas',
                'bol_active' => 1,
            ),
            201 => 
            array (
                'id_model' => 202,
                'id_manufacturer' => 28,
                'str_description' => 'victoria',
                'str_description_slug' => 'victoria',
                'bol_active' => 1,
            ),
            202 => 
            array (
                'id_model' => 203,
                'id_manufacturer' => 29,
                'str_description' => '100',
                'str_description_slug' => '100',
                'bol_active' => 1,
            ),
            203 => 
            array (
                'id_model' => 204,
                'id_manufacturer' => 29,
                'str_description' => '3000',
                'str_description_slug' => '3000',
                'bol_active' => 1,
            ),
            204 => 
            array (
                'id_model' => 205,
                'id_manufacturer' => 29,
                'str_description' => 'sprit',
                'str_description_slug' => 'sprit',
                'bol_active' => 1,
            ),
            205 => 
            array (
                'id_model' => 206,
                'id_manufacturer' => 30,
                'str_description' => 'mini',
                'str_description_slug' => 'mini',
                'bol_active' => 1,
            ),
            206 => 
            array (
                'id_model' => 207,
                'id_manufacturer' => 31,
                'str_description' => 'a111',
                'str_description_slug' => 'a111',
                'bol_active' => 1,
            ),
            207 => 
            array (
                'id_model' => 208,
                'id_manufacturer' => 31,
                'str_description' => 'a112',
                'str_description_slug' => 'a112',
                'bol_active' => 1,
            ),
            208 => 
            array (
                'id_model' => 209,
                'id_manufacturer' => 31,
                'str_description' => 'bianchina',
                'str_description_slug' => 'bianchina',
                'bol_active' => 1,
            ),
            209 => 
            array (
                'id_model' => 210,
                'id_manufacturer' => 31,
                'str_description' => 'y10',
                'str_description_slug' => 'y10',
                'bol_active' => 1,
            ),
            210 => 
            array (
                'id_model' => 211,
                'id_manufacturer' => 32,
                'str_description' => 'a3',
                'str_description_slug' => 'a3',
                'bol_active' => 1,
            ),
            211 => 
            array (
                'id_model' => 212,
                'id_manufacturer' => 33,
                'str_description' => '1000',
                'str_description_slug' => '1000',
                'bol_active' => 1,
            ),
            212 => 
            array (
                'id_model' => 213,
                'id_manufacturer' => 33,
                'str_description' => '1100',
                'str_description_slug' => '1100',
                'bol_active' => 1,
            ),
            213 => 
            array (
                'id_model' => 214,
                'id_manufacturer' => 33,
                'str_description' => '1200',
                'str_description_slug' => '1200',
                'bol_active' => 1,
            ),
            214 => 
            array (
                'id_model' => 215,
                'id_manufacturer' => 33,
                'str_description' => '1250',
                'str_description_slug' => '1250',
                'bol_active' => 1,
            ),
            215 => 
            array (
                'id_model' => 216,
                'id_manufacturer' => 33,
                'str_description' => '1300',
                'str_description_slug' => '1300',
                'bol_active' => 1,
            ),
            216 => 
            array (
                'id_model' => 217,
                'id_manufacturer' => 33,
                'str_description' => '1500',
                'str_description_slug' => '1500',
                'bol_active' => 1,
            ),
            217 => 
            array (
                'id_model' => 218,
                'id_manufacturer' => 33,
                'str_description' => '1600',
                'str_description_slug' => '1600',
                'bol_active' => 1,
            ),
            218 => 
            array (
                'id_model' => 219,
                'id_manufacturer' => 33,
                'str_description' => '1601',
                'str_description_slug' => '1601',
                'bol_active' => 1,
            ),
            219 => 
            array (
                'id_model' => 220,
                'id_manufacturer' => 33,
                'str_description' => '1602',
                'str_description_slug' => '1602',
                'bol_active' => 1,
            ),
            220 => 
            array (
                'id_model' => 221,
                'id_manufacturer' => 33,
                'str_description' => '1603',
                'str_description_slug' => '1603',
                'bol_active' => 1,
            ),
            221 => 
            array (
                'id_model' => 222,
                'id_manufacturer' => 33,
                'str_description' => '2000',
                'str_description_slug' => '2000',
                'bol_active' => 1,
            ),
            222 => 
            array (
                'id_model' => 223,
                'id_manufacturer' => 33,
                'str_description' => '2001',
                'str_description_slug' => '2001',
                'bol_active' => 1,
            ),
            223 => 
            array (
                'id_model' => 224,
                'id_manufacturer' => 33,
                'str_description' => '2002',
                'str_description_slug' => '2002',
                'bol_active' => 1,
            ),
            224 => 
            array (
                'id_model' => 225,
                'id_manufacturer' => 33,
                'str_description' => '2003',
                'str_description_slug' => '2003',
                'bol_active' => 1,
            ),
            225 => 
            array (
                'id_model' => 226,
                'id_manufacturer' => 33,
                'str_description' => '2500',
                'str_description_slug' => '2500',
                'bol_active' => 1,
            ),
            226 => 
            array (
                'id_model' => 227,
                'id_manufacturer' => 33,
                'str_description' => '2600',
                'str_description_slug' => '2600',
                'bol_active' => 1,
            ),
            227 => 
            array (
                'id_model' => 228,
                'id_manufacturer' => 33,
                'str_description' => '2601',
                'str_description_slug' => '2601',
                'bol_active' => 1,
            ),
            228 => 
            array (
                'id_model' => 229,
                'id_manufacturer' => 33,
                'str_description' => '2602',
                'str_description_slug' => '2602',
                'bol_active' => 1,
            ),
            229 => 
            array (
                'id_model' => 230,
                'id_manufacturer' => 33,
                'str_description' => '2603',
                'str_description_slug' => '2603',
                'bol_active' => 1,
            ),
            230 => 
            array (
                'id_model' => 231,
                'id_manufacturer' => 33,
                'str_description' => '3500',
                'str_description_slug' => '3500',
                'bol_active' => 1,
            ),
            231 => 
            array (
                'id_model' => 232,
                'id_manufacturer' => 33,
                'str_description' => '3601',
                'str_description_slug' => '3601',
                'bol_active' => 1,
            ),
            232 => 
            array (
                'id_model' => 233,
                'id_manufacturer' => 33,
                'str_description' => '3602',
                'str_description_slug' => '3602',
                'bol_active' => 1,
            ),
            233 => 
            array (
                'id_model' => 234,
                'id_manufacturer' => 33,
                'str_description' => '3603',
                'str_description_slug' => '3603',
                'bol_active' => 1,
            ),
            234 => 
            array (
                'id_model' => 235,
                'id_manufacturer' => 33,
                'str_description' => '3604',
                'str_description_slug' => '3604',
                'bol_active' => 1,
            ),
            235 => 
            array (
                'id_model' => 236,
                'id_manufacturer' => 33,
                'str_description' => '4000',
                'str_description_slug' => '4000',
                'bol_active' => 1,
            ),
            236 => 
            array (
                'id_model' => 237,
                'id_manufacturer' => 33,
                'str_description' => '30603',
                'str_description_slug' => '30603',
                'bol_active' => 1,
            ),
            237 => 
            array (
                'id_model' => 238,
                'id_manufacturer' => 33,
                'str_description' => 'a-2001',
                'str_description_slug' => 'a-2001',
                'bol_active' => 1,
            ),
            238 => 
            array (
                'id_model' => 239,
                'id_manufacturer' => 33,
                'str_description' => 'a-2002',
                'str_description_slug' => 'a-2002',
                'bol_active' => 1,
            ),
            239 => 
            array (
                'id_model' => 240,
                'id_manufacturer' => 33,
                'str_description' => 'furg.-am',
                'str_description_slug' => 'furg.-am',
                'bol_active' => 1,
            ),
            240 => 
            array (
                'id_model' => 241,
                'id_manufacturer' => 33,
                'str_description' => 'siata',
                'str_description_slug' => 'siata',
                'bol_active' => 1,
            ),
            241 => 
            array (
                'id_model' => 242,
                'id_manufacturer' => 34,
                'str_description' => 'carbone',
                'str_description_slug' => 'carbone',
                'bol_active' => 1,
            ),
            242 => 
            array (
                'id_model' => 243,
                'id_manufacturer' => 34,
                'str_description' => 'crossbone',
                'str_description_slug' => 'crossbone',
                'bol_active' => 1,
            ),
            243 => 
            array (
                'id_model' => 244,
                'id_manufacturer' => 35,
                'str_description' => 'beep',
                'str_description_slug' => 'beep',
                'bol_active' => 1,
            ),
            244 => 
            array (
                'id_model' => 245,
                'id_manufacturer' => 35,
                'str_description' => 'kaboom',
                'str_description_slug' => 'kaboom',
                'bol_active' => 1,
            ),
            245 => 
            array (
                'id_model' => 246,
                'id_manufacturer' => 35,
                'str_description' => 'roadster',
                'str_description_slug' => 'roadster',
                'bol_active' => 1,
            ),
            246 => 
            array (
                'id_model' => 247,
                'id_manufacturer' => 36,
                'str_description' => '150',
                'str_description_slug' => '150',
                'bol_active' => 1,
            ),
            247 => 
            array (
                'id_model' => 248,
                'id_manufacturer' => 36,
                'str_description' => '175',
                'str_description_slug' => '175',
                'bol_active' => 1,
            ),
            248 => 
            array (
                'id_model' => 249,
                'id_manufacturer' => 36,
                'str_description' => '250',
                'str_description_slug' => '250',
                'bol_active' => 1,
            ),
            249 => 
            array (
                'id_model' => 250,
                'id_manufacturer' => 36,
                'str_description' => '1100',
                'str_description_slug' => '1100',
                'bol_active' => 1,
            ),
            250 => 
            array (
                'id_model' => 251,
                'id_manufacturer' => 37,
                'str_description' => 'babieca',
                'str_description_slug' => 'babieca',
                'bol_active' => 1,
            ),
            251 => 
            array (
                'id_model' => 252,
                'id_manufacturer' => 38,
                'str_description' => 'taigah',
                'str_description_slug' => 'taigah',
                'bol_active' => 1,
            ),
            252 => 
            array (
                'id_model' => 253,
                'id_manufacturer' => 38,
                'str_description' => 'tulos',
                'str_description_slug' => 'tulos',
                'bol_active' => 1,
            ),
            253 => 
            array (
                'id_model' => 254,
                'id_manufacturer' => 39,
                'str_description' => 'tempo trax',
                'str_description_slug' => 'tempo-trax',
                'bol_active' => 1,
            ),
            254 => 
            array (
                'id_model' => 255,
                'id_manufacturer' => 40,
                'str_description' => 'gkt',
                'str_description_slug' => 'gkt',
                'bol_active' => 1,
            ),
            255 => 
            array (
                'id_model' => 256,
                'id_manufacturer' => 41,
                'str_description' => 'vintage',
                'str_description_slug' => 'vintage',
                'bol_active' => 1,
            ),
            256 => 
            array (
                'id_model' => 257,
                'id_manufacturer' => 42,
                'str_description' => 'by-e',
                'str_description_slug' => 'by-e',
                'bol_active' => 1,
            ),
            257 => 
            array (
                'id_model' => 258,
                'id_manufacturer' => 43,
                'str_description' => '4000',
                'str_description_slug' => '4000',
                'bol_active' => 1,
            ),
            258 => 
            array (
                'id_model' => 259,
                'id_manufacturer' => 43,
                'str_description' => 'saeta',
                'str_description_slug' => 'saeta',
                'bol_active' => 1,
            ),
            259 => 
            array (
                'id_model' => 260,
                'id_manufacturer' => 44,
                'str_description' => 'serie cf',
                'str_description_slug' => 'serie-cf',
                'bol_active' => 1,
            ),
            260 => 
            array (
                'id_model' => 261,
                'id_manufacturer' => 45,
                'str_description' => 'pony',
                'str_description_slug' => 'pony',
                'bol_active' => 1,
            ),
            261 => 
            array (
                'id_model' => 262,
                'id_manufacturer' => 46,
                'str_description' => 'bellier',
                'str_description_slug' => 'bellier',
                'bol_active' => 1,
            ),
            262 => 
            array (
                'id_model' => 263,
                'id_manufacturer' => 46,
                'str_description' => 'divane',
                'str_description_slug' => 'divane',
                'bol_active' => 1,
            ),
            263 => 
            array (
                'id_model' => 264,
                'id_manufacturer' => 46,
                'str_description' => 'jade',
                'str_description_slug' => 'jade',
                'bol_active' => 1,
            ),
            264 => 
            array (
                'id_model' => 265,
                'id_manufacturer' => 46,
                'str_description' => 'opale',
                'str_description_slug' => 'opale',
                'bol_active' => 1,
            ),
            265 => 
            array (
                'id_model' => 266,
                'id_manufacturer' => 46,
                'str_description' => 'vx 550',
                'str_description_slug' => 'vx-550',
                'bol_active' => 1,
            ),
            266 => 
            array (
                'id_model' => 267,
                'id_manufacturer' => 47,
                'str_description' => 'sole kiki',
                'str_description_slug' => 'sole-kiki',
                'bol_active' => 1,
            ),
            267 => 
            array (
                'id_model' => 268,
                'id_manufacturer' => 48,
                'str_description' => '3 1/2',
                'str_description_slug' => '3-1/2',
                'bol_active' => 1,
            ),
            268 => 
            array (
                'id_model' => 269,
                'id_manufacturer' => 48,
                'str_description' => '4 1/4',
                'str_description_slug' => '4-1/4',
                'bol_active' => 1,
            ),
            269 => 
            array (
                'id_model' => 270,
                'id_manufacturer' => 48,
                'str_description' => 'arnage',
                'str_description_slug' => 'arnage',
                'bol_active' => 1,
            ),
            270 => 
            array (
                'id_model' => 271,
                'id_manufacturer' => 48,
                'str_description' => 'azure',
                'str_description_slug' => 'azure',
                'bol_active' => 1,
            ),
            271 => 
            array (
                'id_model' => 272,
                'id_manufacturer' => 48,
                'str_description' => 'bentayga',
                'str_description_slug' => 'bentayga',
                'bol_active' => 1,
            ),
            272 => 
            array (
                'id_model' => 273,
                'id_manufacturer' => 48,
                'str_description' => 'brooklands',
                'str_description_slug' => 'brooklands',
                'bol_active' => 1,
            ),
            273 => 
            array (
                'id_model' => 274,
                'id_manufacturer' => 48,
                'str_description' => 'continental',
                'str_description_slug' => 'continental',
                'bol_active' => 1,
            ),
            274 => 
            array (
                'id_model' => 275,
                'id_manufacturer' => 48,
                'str_description' => 'eight',
                'str_description_slug' => 'eight',
                'bol_active' => 1,
            ),
            275 => 
            array (
                'id_model' => 276,
                'id_manufacturer' => 48,
                'str_description' => 'flying spur',
                'str_description_slug' => 'flying-spur',
                'bol_active' => 1,
            ),
            276 => 
            array (
                'id_model' => 277,
                'id_manufacturer' => 48,
                'str_description' => 'freestone & webb',
                'str_description_slug' => 'freestone-&-webb',
                'bol_active' => 1,
            ),
            277 => 
            array (
                'id_model' => 278,
                'id_manufacturer' => 48,
                'str_description' => 'mk 6',
                'str_description_slug' => 'mk-6',
                'bol_active' => 1,
            ),
            278 => 
            array (
                'id_model' => 279,
                'id_manufacturer' => 48,
                'str_description' => 'mulsanne',
                'str_description_slug' => 'mulsanne',
                'bol_active' => 1,
            ),
            279 => 
            array (
                'id_model' => 280,
                'id_manufacturer' => 48,
                'str_description' => 'mulsanne s',
                'str_description_slug' => 'mulsanne-s',
                'bol_active' => 1,
            ),
            280 => 
            array (
                'id_model' => 281,
                'id_manufacturer' => 48,
                'str_description' => 's1',
                'str_description_slug' => 's1',
                'bol_active' => 1,
            ),
            281 => 
            array (
                'id_model' => 282,
                'id_manufacturer' => 48,
                'str_description' => 's2',
                'str_description_slug' => 's2',
                'bol_active' => 1,
            ),
            282 => 
            array (
                'id_model' => 283,
                'id_manufacturer' => 48,
                'str_description' => 's3',
                'str_description_slug' => 's3',
                'bol_active' => 1,
            ),
            283 => 
            array (
                'id_model' => 284,
                'id_manufacturer' => 48,
                'str_description' => 'saloon',
                'str_description_slug' => 'saloon',
                'bol_active' => 1,
            ),
            284 => 
            array (
                'id_model' => 285,
                'id_manufacturer' => 48,
                'str_description' => 'serie t',
                'str_description_slug' => 'serie-t',
                'bol_active' => 1,
            ),
            285 => 
            array (
                'id_model' => 286,
                'id_manufacturer' => 48,
                'str_description' => 'turbo r',
                'str_description_slug' => 'turbo-r',
                'bol_active' => 1,
            ),
            286 => 
            array (
                'id_model' => 287,
                'id_manufacturer' => 48,
                'str_description' => 'turbo s',
                'str_description_slug' => 'turbo-s',
                'bol_active' => 1,
            ),
            287 => 
            array (
                'id_model' => 288,
                'id_manufacturer' => 48,
                'str_description' => 'type r',
                'str_description_slug' => 'type-r',
                'bol_active' => 1,
            ),
            288 => 
            array (
                'id_model' => 289,
                'id_manufacturer' => 49,
                'str_description' => 'freeclimber',
                'str_description_slug' => 'freeclimber',
                'bol_active' => 1,
            ),
            289 => 
            array (
                'id_model' => 290,
                'id_manufacturer' => 49,
                'str_description' => 'ritmo',
                'str_description_slug' => 'ritmo',
                'bol_active' => 1,
            ),
            290 => 
            array (
                'id_model' => 291,
                'id_manufacturer' => 49,
                'str_description' => 'x 1/9',
                'str_description_slug' => 'x-1/9',
                'bol_active' => 1,
            ),
            291 => 
            array (
                'id_model' => 292,
                'id_manufacturer' => 50,
                'str_description' => 'serie 100',
                'str_description_slug' => 'serie-100',
                'bol_active' => 1,
            ),
            292 => 
            array (
                'id_model' => 293,
                'id_manufacturer' => 50,
                'str_description' => 'serie 200 a',
                'str_description_slug' => 'serie-200-a',
                'bol_active' => 1,
            ),
            293 => 
            array (
                'id_model' => 294,
                'id_manufacturer' => 50,
                'str_description' => 'serie 200 c',
                'str_description_slug' => 'serie-200-c',
                'bol_active' => 1,
            ),
            294 => 
            array (
                'id_model' => 295,
                'id_manufacturer' => 50,
                'str_description' => 'serie 200 f',
                'str_description_slug' => 'serie-200-f',
                'bol_active' => 1,
            ),
            295 => 
            array (
                'id_model' => 296,
                'id_manufacturer' => 50,
                'str_description' => 'serie 200 i',
                'str_description_slug' => 'serie-200-i',
                'bol_active' => 1,
            ),
            296 => 
            array (
                'id_model' => 297,
                'id_manufacturer' => 51,
                'str_description' => 'golia',
                'str_description_slug' => 'golia',
                'bol_active' => 1,
            ),
            297 => 
            array (
                'id_model' => 298,
                'id_manufacturer' => 52,
                'str_description' => 'riley elf',
                'str_description_slug' => 'riley-elf',
                'bol_active' => 1,
            ),
            298 => 
            array (
                'id_model' => 299,
                'id_manufacturer' => 53,
                'str_description' => '3.0',
                'str_description_slug' => '3.0',
                'bol_active' => 1,
            ),
            299 => 
            array (
                'id_model' => 300,
                'id_manufacturer' => 53,
                'str_description' => '315',
                'str_description_slug' => '315',
                'bol_active' => 1,
            ),
            300 => 
            array (
                'id_model' => 301,
                'id_manufacturer' => 53,
                'str_description' => '501',
                'str_description_slug' => '501',
                'bol_active' => 1,
            ),
            301 => 
            array (
                'id_model' => 302,
                'id_manufacturer' => 53,
                'str_description' => '502',
                'str_description_slug' => '502',
                'bol_active' => 1,
            ),
            302 => 
            array (
                'id_model' => 303,
                'id_manufacturer' => 53,
                'str_description' => '600',
                'str_description_slug' => '600',
                'bol_active' => 1,
            ),
            303 => 
            array (
                'id_model' => 304,
                'id_manufacturer' => 53,
                'str_description' => '700',
                'str_description_slug' => '700',
                'bol_active' => 1,
            ),
            304 => 
            array (
                'id_model' => 305,
                'id_manufacturer' => 53,
                'str_description' => '1502',
                'str_description_slug' => '1502',
                'bol_active' => 1,
            ),
            305 => 
            array (
                'id_model' => 306,
                'id_manufacturer' => 53,
                'str_description' => '1600',
                'str_description_slug' => '1600',
                'bol_active' => 1,
            ),
            306 => 
            array (
                'id_model' => 307,
                'id_manufacturer' => 53,
                'str_description' => '1602',
                'str_description_slug' => '1602',
                'bol_active' => 1,
            ),
            307 => 
            array (
                'id_model' => 308,
                'id_manufacturer' => 53,
                'str_description' => '1800',
                'str_description_slug' => '1800',
                'bol_active' => 1,
            ),
            308 => 
            array (
                'id_model' => 309,
                'id_manufacturer' => 53,
                'str_description' => '1802',
                'str_description_slug' => '1802',
                'bol_active' => 1,
            ),
            309 => 
            array (
                'id_model' => 310,
                'id_manufacturer' => 53,
                'str_description' => '2000',
                'str_description_slug' => '2000',
                'bol_active' => 1,
            ),
            310 => 
            array (
                'id_model' => 311,
                'id_manufacturer' => 53,
                'str_description' => '2002',
                'str_description_slug' => '2002',
                'bol_active' => 1,
            ),
            311 => 
            array (
                'id_model' => 312,
                'id_manufacturer' => 53,
                'str_description' => '2500',
                'str_description_slug' => '2500',
                'bol_active' => 1,
            ),
            312 => 
            array (
                'id_model' => 313,
                'id_manufacturer' => 53,
                'str_description' => '2800',
                'str_description_slug' => '2800',
                'bol_active' => 1,
            ),
            313 => 
            array (
                'id_model' => 314,
                'id_manufacturer' => 53,
                'str_description' => 'i3',
                'str_description_slug' => 'i3',
                'bol_active' => 1,
            ),
            314 => 
            array (
                'id_model' => 315,
                'id_manufacturer' => 53,
                'str_description' => 'i8',
                'str_description_slug' => 'i8',
                'bol_active' => 1,
            ),
            315 => 
            array (
                'id_model' => 316,
                'id_manufacturer' => 53,
                'str_description' => 'isetta',
                'str_description_slug' => 'isetta',
                'bol_active' => 1,
            ),
            316 => 
            array (
                'id_model' => 317,
                'id_manufacturer' => 53,
                'str_description' => 'm1',
                'str_description_slug' => 'm1',
                'bol_active' => 1,
            ),
            317 => 
            array (
                'id_model' => 318,
                'id_manufacturer' => 53,
                'str_description' => 'serie 2',
                'str_description_slug' => 'serie-2',
                'bol_active' => 1,
            ),
            318 => 
            array (
                'id_model' => 319,
                'id_manufacturer' => 53,
                'str_description' => 'serie 4',
                'str_description_slug' => 'serie-4',
                'bol_active' => 1,
            ),
            319 => 
            array (
                'id_model' => 320,
                'id_manufacturer' => 53,
                'str_description' => 'serie 8',
                'str_description_slug' => 'serie-8',
                'bol_active' => 1,
            ),
            320 => 
            array (
                'id_model' => 321,
                'id_manufacturer' => 53,
                'str_description' => 'x4',
                'str_description_slug' => 'x4',
                'bol_active' => 1,
            ),
            321 => 
            array (
                'id_model' => 322,
                'id_manufacturer' => 53,
                'str_description' => 'x6',
                'str_description_slug' => 'x6',
                'bol_active' => 1,
            ),
            322 => 
            array (
                'id_model' => 323,
                'id_manufacturer' => 53,
                'str_description' => 'z1',
                'str_description_slug' => 'z1',
                'bol_active' => 1,
            ),
            323 => 
            array (
                'id_model' => 324,
                'id_manufacturer' => 53,
                'str_description' => 'z8',
                'str_description_slug' => 'z8',
                'bol_active' => 1,
            ),
            324 => 
            array (
                'id_model' => 325,
                'id_manufacturer' => 54,
                'str_description' => 'nv',
                'str_description_slug' => 'nv',
                'bol_active' => 1,
            ),
            325 => 
            array (
                'id_model' => 326,
                'id_manufacturer' => 55,
                'str_description' => 'isabella',
                'str_description_slug' => 'isabella',
                'bol_active' => 1,
            ),
            326 => 
            array (
                'id_model' => 327,
                'id_manufacturer' => 56,
                'str_description' => 'tgr 35',
                'str_description_slug' => 'tgr-35',
                'bol_active' => 1,
            ),
            327 => 
            array (
                'id_model' => 328,
                'id_manufacturer' => 56,
                'str_description' => 'xtreme',
                'str_description_slug' => 'xtreme',
                'bol_active' => 1,
            ),
            328 => 
            array (
                'id_model' => 329,
                'id_manufacturer' => 57,
                'str_description' => 'mgb',
                'str_description_slug' => 'mgb',
                'bol_active' => 1,
            ),
            329 => 
            array (
                'id_model' => 330,
                'id_manufacturer' => 57,
                'str_description' => 'midget',
                'str_description_slug' => 'midget',
                'bol_active' => 1,
            ),
            330 => 
            array (
                'id_model' => 331,
                'id_manufacturer' => 58,
                'str_description' => 'giba',
                'str_description_slug' => 'giba',
                'bol_active' => 1,
            ),
            331 => 
            array (
                'id_model' => 332,
                'id_manufacturer' => 58,
                'str_description' => 'way',
                'str_description_slug' => 'way',
                'bol_active' => 1,
            ),
            332 => 
            array (
                'id_model' => 333,
                'id_manufacturer' => 59,
                'str_description' => 'scout',
                'str_description_slug' => 'scout',
                'bol_active' => 1,
            ),
            333 => 
            array (
                'id_model' => 334,
                'id_manufacturer' => 59,
                'str_description' => 'tw 34-10',
                'str_description_slug' => 'tw-34-10',
                'bol_active' => 1,
            ),
            334 => 
            array (
                'id_model' => 335,
                'id_manufacturer' => 60,
                'str_description' => 'asac',
                'str_description_slug' => 'asac',
                'bol_active' => 1,
            ),
            335 => 
            array (
                'id_model' => 336,
                'id_manufacturer' => 61,
                'str_description' => 'eb',
                'str_description_slug' => 'eb',
                'bol_active' => 1,
            ),
            336 => 
            array (
                'id_model' => 337,
                'id_manufacturer' => 61,
                'str_description' => 't 57',
                'str_description_slug' => 't-57',
                'bol_active' => 1,
            ),
            337 => 
            array (
                'id_model' => 338,
                'id_manufacturer' => 61,
                'str_description' => 'veyron',
                'str_description_slug' => 'veyron',
                'bol_active' => 1,
            ),
            338 => 
            array (
                'id_model' => 339,
                'id_manufacturer' => 62,
                'str_description' => '250',
                'str_description_slug' => '250',
                'bol_active' => 1,
            ),
            339 => 
            array (
                'id_model' => 340,
                'id_manufacturer' => 62,
                'str_description' => '500',
                'str_description_slug' => '500',
                'bol_active' => 1,
            ),
            340 => 
            array (
                'id_model' => 341,
                'id_manufacturer' => 62,
                'str_description' => 'classic',
                'str_description_slug' => 'classic',
                'bol_active' => 1,
            ),
            341 => 
            array (
                'id_model' => 342,
                'id_manufacturer' => 63,
                'str_description' => 'way',
                'str_description_slug' => 'way',
                'bol_active' => 1,
            ),
            342 => 
            array (
                'id_model' => 343,
                'id_manufacturer' => 64,
                'str_description' => '8',
                'str_description_slug' => '8',
                'bol_active' => 1,
            ),
            343 => 
            array (
                'id_model' => 344,
                'id_manufacturer' => 64,
                'str_description' => '27',
                'str_description_slug' => '27',
                'bol_active' => 1,
            ),
            344 => 
            array (
                'id_model' => 345,
                'id_manufacturer' => 64,
                'str_description' => '40',
                'str_description_slug' => '40',
                'bol_active' => 1,
            ),
            345 => 
            array (
                'id_model' => 346,
                'id_manufacturer' => 64,
                'str_description' => '41',
                'str_description_slug' => '41',
                'bol_active' => 1,
            ),
            346 => 
            array (
                'id_model' => 347,
                'id_manufacturer' => 64,
                'str_description' => '50',
                'str_description_slug' => '50',
                'bol_active' => 1,
            ),
            347 => 
            array (
                'id_model' => 348,
                'id_manufacturer' => 64,
                'str_description' => '51',
                'str_description_slug' => '51',
                'bol_active' => 1,
            ),
            348 => 
            array (
                'id_model' => 349,
                'id_manufacturer' => 64,
                'str_description' => 'abadal',
                'str_description_slug' => 'abadal',
                'bol_active' => 1,
            ),
            349 => 
            array (
                'id_model' => 350,
                'id_manufacturer' => 64,
                'str_description' => 'century',
                'str_description_slug' => 'century',
                'bol_active' => 1,
            ),
            350 => 
            array (
                'id_model' => 351,
                'id_manufacturer' => 64,
                'str_description' => 'cs',
                'str_description_slug' => 'cs',
                'bol_active' => 1,
            ),
            351 => 
            array (
                'id_model' => 352,
                'id_manufacturer' => 64,
                'str_description' => 'electra',
                'str_description_slug' => 'electra',
                'bol_active' => 1,
            ),
            352 => 
            array (
                'id_model' => 353,
                'id_manufacturer' => 64,
                'str_description' => 'enclave',
                'str_description_slug' => 'enclave',
                'bol_active' => 1,
            ),
            353 => 
            array (
                'id_model' => 354,
                'id_manufacturer' => 64,
                'str_description' => 'le sabre',
                'str_description_slug' => 'le-sabre',
                'bol_active' => 1,
            ),
            354 => 
            array (
                'id_model' => 355,
                'id_manufacturer' => 64,
                'str_description' => 'marquette',
                'str_description_slug' => 'marquette',
                'bol_active' => 1,
            ),
            355 => 
            array (
                'id_model' => 356,
                'id_manufacturer' => 64,
                'str_description' => 'master six',
                'str_description_slug' => 'master-six',
                'bol_active' => 1,
            ),
            356 => 
            array (
                'id_model' => 357,
                'id_manufacturer' => 64,
                'str_description' => 'model b',
                'str_description_slug' => 'model-b',
                'bol_active' => 1,
            ),
            357 => 
            array (
                'id_model' => 358,
                'id_manufacturer' => 64,
                'str_description' => 'park avenue',
                'str_description_slug' => 'park-avenue',
                'bol_active' => 1,
            ),
            358 => 
            array (
                'id_model' => 359,
                'id_manufacturer' => 64,
                'str_description' => 'phaeton',
                'str_description_slug' => 'phaeton',
                'bol_active' => 1,
            ),
            359 => 
            array (
                'id_model' => 360,
                'id_manufacturer' => 64,
                'str_description' => 'reatla',
                'str_description_slug' => 'reatla',
                'bol_active' => 1,
            ),
            360 => 
            array (
                'id_model' => 361,
                'id_manufacturer' => 64,
                'str_description' => 'reatta',
                'str_description_slug' => 'reatta',
                'bol_active' => 1,
            ),
            361 => 
            array (
                'id_model' => 362,
                'id_manufacturer' => 64,
                'str_description' => 'regal',
                'str_description_slug' => 'regal',
                'bol_active' => 1,
            ),
            362 => 
            array (
                'id_model' => 363,
                'id_manufacturer' => 64,
                'str_description' => 'rendez vous',
                'str_description_slug' => 'rendez-vous',
                'bol_active' => 1,
            ),
            363 => 
            array (
                'id_model' => 364,
                'id_manufacturer' => 64,
                'str_description' => 'riviera',
                'str_description_slug' => 'riviera',
                'bol_active' => 1,
            ),
            364 => 
            array (
                'id_model' => 365,
                'id_manufacturer' => 64,
                'str_description' => 'roadmaster',
                'str_description_slug' => 'roadmaster',
                'bol_active' => 1,
            ),
            365 => 
            array (
                'id_model' => 366,
                'id_manufacturer' => 64,
                'str_description' => 'skyhawk',
                'str_description_slug' => 'skyhawk',
                'bol_active' => 1,
            ),
            366 => 
            array (
                'id_model' => 367,
                'id_manufacturer' => 64,
                'str_description' => 'skylark',
                'str_description_slug' => 'skylark',
                'bol_active' => 1,
            ),
            367 => 
            array (
                'id_model' => 368,
                'id_manufacturer' => 64,
                'str_description' => 'special',
                'str_description_slug' => 'special',
                'bol_active' => 1,
            ),
            368 => 
            array (
                'id_model' => 369,
                'id_manufacturer' => 65,
                'str_description' => 'buomo',
                'str_description_slug' => 'buomo',
                'bol_active' => 1,
            ),
            369 => 
            array (
                'id_model' => 370,
                'id_manufacturer' => 66,
                'str_description' => 'fa',
                'str_description_slug' => 'fa',
                'bol_active' => 1,
            ),
            370 => 
            array (
                'id_model' => 371,
                'id_manufacturer' => 67,
                'str_description' => 'e6',
                'str_description_slug' => 'e6',
                'bol_active' => 1,
            ),
            371 => 
            array (
                'id_model' => 372,
                'id_manufacturer' => 67,
                'str_description' => 'f3',
                'str_description_slug' => 'f3',
                'bol_active' => 1,
            ),
            372 => 
            array (
                'id_model' => 373,
                'id_manufacturer' => 68,
                'str_description' => '28k',
                'str_description_slug' => '28k',
                'bol_active' => 1,
            ),
            373 => 
            array (
                'id_model' => 374,
                'id_manufacturer' => 69,
                'str_description' => '51',
                'str_description_slug' => '51',
                'bol_active' => 1,
            ),
            374 => 
            array (
                'id_model' => 375,
                'id_manufacturer' => 69,
                'str_description' => 'allante',
                'str_description_slug' => 'allante',
                'bol_active' => 1,
            ),
            375 => 
            array (
                'id_model' => 376,
                'id_manufacturer' => 69,
                'str_description' => 'ats',
                'str_description_slug' => 'ats',
                'bol_active' => 1,
            ),
            376 => 
            array (
                'id_model' => 377,
                'id_manufacturer' => 69,
                'str_description' => 'bls',
                'str_description_slug' => 'bls',
                'bol_active' => 1,
            ),
            377 => 
            array (
                'id_model' => 378,
                'id_manufacturer' => 69,
                'str_description' => 'brougham',
                'str_description_slug' => 'brougham',
                'bol_active' => 1,
            ),
            378 => 
            array (
                'id_model' => 379,
                'id_manufacturer' => 69,
                'str_description' => 'calais',
                'str_description_slug' => 'calais',
                'bol_active' => 1,
            ),
            379 => 
            array (
                'id_model' => 380,
                'id_manufacturer' => 69,
                'str_description' => 'catera',
                'str_description_slug' => 'catera',
                'bol_active' => 1,
            ),
            380 => 
            array (
                'id_model' => 381,
                'id_manufacturer' => 69,
                'str_description' => 'cimarron',
                'str_description_slug' => 'cimarron',
                'bol_active' => 1,
            ),
            381 => 
            array (
                'id_model' => 382,
                'id_manufacturer' => 69,
                'str_description' => 'convertible',
                'str_description_slug' => 'convertible',
                'bol_active' => 1,
            ),
            382 => 
            array (
                'id_model' => 383,
                'id_manufacturer' => 69,
                'str_description' => 'cts',
                'str_description_slug' => 'cts',
                'bol_active' => 1,
            ),
            383 => 
            array (
                'id_model' => 384,
                'id_manufacturer' => 69,
                'str_description' => 'cts-v',
                'str_description_slug' => 'cts-v',
                'bol_active' => 1,
            ),
            384 => 
            array (
                'id_model' => 385,
                'id_manufacturer' => 69,
                'str_description' => 'deville',
                'str_description_slug' => 'deville',
                'bol_active' => 1,
            ),
            385 => 
            array (
                'id_model' => 386,
                'id_manufacturer' => 69,
                'str_description' => 'dts',
                'str_description_slug' => 'dts',
                'bol_active' => 1,
            ),
            386 => 
            array (
                'id_model' => 387,
                'id_manufacturer' => 69,
                'str_description' => 'eldorado',
                'str_description_slug' => 'eldorado',
                'bol_active' => 1,
            ),
            387 => 
            array (
                'id_model' => 388,
                'id_manufacturer' => 69,
                'str_description' => 'escalade',
                'str_description_slug' => 'escalade',
                'bol_active' => 1,
            ),
            388 => 
            array (
                'id_model' => 389,
                'id_manufacturer' => 69,
                'str_description' => 'fleetwood',
                'str_description_slug' => 'fleetwood',
                'bol_active' => 1,
            ),
            389 => 
            array (
                'id_model' => 390,
                'id_manufacturer' => 69,
                'str_description' => 'lasalle',
                'str_description_slug' => 'lasalle',
                'bol_active' => 1,
            ),
            390 => 
            array (
                'id_model' => 391,
                'id_manufacturer' => 69,
                'str_description' => 'quick silver',
                'str_description_slug' => 'quick-silver',
                'bol_active' => 1,
            ),
            391 => 
            array (
                'id_model' => 392,
                'id_manufacturer' => 69,
                'str_description' => 'serie 62',
                'str_description_slug' => 'serie-62',
                'bol_active' => 1,
            ),
            392 => 
            array (
                'id_model' => 393,
                'id_manufacturer' => 69,
                'str_description' => 'seville',
                'str_description_slug' => 'seville',
                'bol_active' => 1,
            ),
            393 => 
            array (
                'id_model' => 394,
                'id_manufacturer' => 69,
                'str_description' => 'seville opera',
                'str_description_slug' => 'seville-opera',
                'bol_active' => 1,
            ),
            394 => 
            array (
                'id_model' => 395,
                'id_manufacturer' => 69,
                'str_description' => 'srx',
                'str_description_slug' => 'srx',
                'bol_active' => 1,
            ),
            395 => 
            array (
                'id_model' => 396,
                'id_manufacturer' => 69,
                'str_description' => 'statesman',
                'str_description_slug' => 'statesman',
                'bol_active' => 1,
            ),
            396 => 
            array (
                'id_model' => 397,
                'id_manufacturer' => 69,
                'str_description' => 'sts',
                'str_description_slug' => 'sts',
                'bol_active' => 1,
            ),
            397 => 
            array (
                'id_model' => 398,
                'id_manufacturer' => 69,
                'str_description' => 'v16',
                'str_description_slug' => 'v16',
                'bol_active' => 1,
            ),
            398 => 
            array (
                'id_model' => 399,
                'id_manufacturer' => 69,
                'str_description' => 'xlr',
                'str_description_slug' => 'xlr',
                'bol_active' => 1,
            ),
            399 => 
            array (
                'id_model' => 400,
                'id_manufacturer' => 70,
                'str_description' => 't-rex',
                'str_description_slug' => 't-rex',
                'bol_active' => 1,
            ),
            400 => 
            array (
                'id_model' => 401,
                'id_manufacturer' => 71,
                'str_description' => 'commander',
                'str_description_slug' => 'commander',
                'bol_active' => 1,
            ),
            401 => 
            array (
                'id_model' => 402,
                'id_manufacturer' => 71,
                'str_description' => 'maverick',
                'str_description_slug' => 'maverick',
                'bol_active' => 1,
            ),
            402 => 
            array (
                'id_model' => 403,
                'id_manufacturer' => 72,
                'str_description' => 'fx4',
                'str_description_slug' => 'fx4',
                'bol_active' => 1,
            ),
            403 => 
            array (
                'id_model' => 404,
                'id_manufacturer' => 72,
                'str_description' => 'taxi hire car',
                'str_description_slug' => 'taxi-hire-car',
                'bol_active' => 1,
            ),
            404 => 
            array (
                'id_model' => 405,
                'id_manufacturer' => 73,
                'str_description' => 'gtr',
                'str_description_slug' => 'gtr',
                'bol_active' => 1,
            ),
            405 => 
            array (
                'id_model' => 406,
                'id_manufacturer' => 74,
                'str_description' => 'one',
                'str_description_slug' => 'one',
                'bol_active' => 1,
            ),
            406 => 
            array (
                'id_model' => 407,
                'id_manufacturer' => 75,
                'str_description' => 'sulky',
                'str_description_slug' => 'sulky',
                'bol_active' => 1,
            ),
            407 => 
            array (
                'id_model' => 408,
                'id_manufacturer' => 75,
                'str_description' => 'sulkycar',
                'str_description_slug' => 'sulkycar',
                'bol_active' => 1,
            ),
            408 => 
            array (
                'id_model' => 409,
                'id_manufacturer' => 75,
                'str_description' => 'ydea',
                'str_description_slug' => 'ydea',
                'bol_active' => 1,
            ),
            409 => 
            array (
                'id_model' => 410,
                'id_manufacturer' => 76,
                'str_description' => '21',
                'str_description_slug' => '21',
                'bol_active' => 1,
            ),
            410 => 
            array (
                'id_model' => 411,
                'id_manufacturer' => 76,
                'str_description' => 'seven',
                'str_description_slug' => 'seven',
                'bol_active' => 1,
            ),
            411 => 
            array (
                'id_model' => 412,
                'id_manufacturer' => 77,
                'str_description' => 'slb2',
                'str_description_slug' => 'slb2',
                'bol_active' => 1,
            ),
            412 => 
            array (
                'id_model' => 413,
                'id_manufacturer' => 78,
                'str_description' => 'barooder',
                'str_description_slug' => 'barooder',
                'bol_active' => 1,
            ),
            413 => 
            array (
                'id_model' => 414,
                'id_manufacturer' => 78,
                'str_description' => 'ch 26',
                'str_description_slug' => 'ch-26',
                'bol_active' => 1,
            ),
            414 => 
            array (
                'id_model' => 415,
                'id_manufacturer' => 78,
                'str_description' => 'cjf',
                'str_description_slug' => 'cjf',
                'bol_active' => 1,
            ),
            415 => 
            array (
                'id_model' => 416,
                'id_manufacturer' => 78,
                'str_description' => 'media',
                'str_description_slug' => 'media',
                'bol_active' => 1,
            ),
            416 => 
            array (
                'id_model' => 417,
                'id_manufacturer' => 78,
                'str_description' => 'speedino',
                'str_description_slug' => 'speedino',
                'bol_active' => 1,
            ),
            417 => 
            array (
                'id_model' => 418,
                'id_manufacturer' => 78,
                'str_description' => 'stella',
                'str_description_slug' => 'stella',
                'bol_active' => 1,
            ),
            418 => 
            array (
                'id_model' => 419,
                'id_manufacturer' => 79,
                'str_description' => '3100',
                'str_description_slug' => '3100',
                'bol_active' => 1,
            ),
            419 => 
            array (
                'id_model' => 420,
                'id_manufacturer' => 79,
                'str_description' => 'alero',
                'str_description_slug' => 'alero',
                'bol_active' => 1,
            ),
            420 => 
            array (
                'id_model' => 421,
                'id_manufacturer' => 79,
                'str_description' => 'apache 31',
                'str_description_slug' => 'apache-31',
                'bol_active' => 1,
            ),
            421 => 
            array (
                'id_model' => 422,
                'id_manufacturer' => 79,
                'str_description' => 'astra',
                'str_description_slug' => 'astra',
                'bol_active' => 1,
            ),
            422 => 
            array (
                'id_model' => 423,
                'id_manufacturer' => 79,
                'str_description' => 'astro',
                'str_description_slug' => 'astro',
                'bol_active' => 1,
            ),
            423 => 
            array (
                'id_model' => 424,
                'id_manufacturer' => 79,
                'str_description' => 'avalanche',
                'str_description_slug' => 'avalanche',
                'bol_active' => 1,
            ),
            424 => 
            array (
                'id_model' => 425,
                'id_manufacturer' => 79,
                'str_description' => 'beauville',
                'str_description_slug' => 'beauville',
                'bol_active' => 1,
            ),
            425 => 
            array (
                'id_model' => 426,
                'id_manufacturer' => 79,
                'str_description' => 'bel air',
                'str_description_slug' => 'bel-air',
                'bol_active' => 1,
            ),
            426 => 
            array (
                'id_model' => 427,
                'id_manufacturer' => 79,
                'str_description' => 'beretta',
                'str_description_slug' => 'beretta',
                'bol_active' => 1,
            ),
            427 => 
            array (
                'id_model' => 428,
                'id_manufacturer' => 79,
                'str_description' => 'blazer',
                'str_description_slug' => 'blazer',
                'bol_active' => 1,
            ),
            428 => 
            array (
                'id_model' => 429,
                'id_manufacturer' => 79,
                'str_description' => 'c',
                'str_description_slug' => 'c',
                'bol_active' => 1,
            ),
            429 => 
            array (
                'id_model' => 430,
                'id_manufacturer' => 79,
                'str_description' => 'camaro',
                'str_description_slug' => 'camaro',
                'bol_active' => 1,
            ),
            430 => 
            array (
                'id_model' => 431,
                'id_manufacturer' => 79,
                'str_description' => 'caprice',
                'str_description_slug' => 'caprice',
                'bol_active' => 1,
            ),
            431 => 
            array (
                'id_model' => 432,
                'id_manufacturer' => 79,
                'str_description' => 'captiva sport',
                'str_description_slug' => 'captiva-sport',
                'bol_active' => 1,
            ),
            432 => 
            array (
                'id_model' => 433,
                'id_manufacturer' => 79,
                'str_description' => 'cavalier',
                'str_description_slug' => 'cavalier',
                'bol_active' => 1,
            ),
            433 => 
            array (
                'id_model' => 434,
                'id_manufacturer' => 79,
                'str_description' => 'celebrity',
                'str_description_slug' => 'celebrity',
                'bol_active' => 1,
            ),
            434 => 
            array (
                'id_model' => 435,
                'id_manufacturer' => 79,
                'str_description' => 'century',
                'str_description_slug' => 'century',
                'bol_active' => 1,
            ),
            435 => 
            array (
                'id_model' => 436,
                'id_manufacturer' => 79,
                'str_description' => 'champion',
                'str_description_slug' => 'champion',
                'bol_active' => 1,
            ),
            436 => 
            array (
                'id_model' => 437,
                'id_manufacturer' => 79,
                'str_description' => 'chevelle',
                'str_description_slug' => 'chevelle',
                'bol_active' => 1,
            ),
            437 => 
            array (
                'id_model' => 438,
                'id_manufacturer' => 79,
                'str_description' => 'chevette',
                'str_description_slug' => 'chevette',
                'bol_active' => 1,
            ),
            438 => 
            array (
                'id_model' => 439,
                'id_manufacturer' => 79,
                'str_description' => 'chevy 3600',
                'str_description_slug' => 'chevy-3600',
                'bol_active' => 1,
            ),
            439 => 
            array (
                'id_model' => 440,
                'id_manufacturer' => 79,
                'str_description' => 'chevy ii',
                'str_description_slug' => 'chevy-ii',
                'bol_active' => 1,
            ),
            440 => 
            array (
                'id_model' => 441,
                'id_manufacturer' => 79,
                'str_description' => 'chevy monza',
                'str_description_slug' => 'chevy-monza',
                'bol_active' => 1,
            ),
            441 => 
            array (
                'id_model' => 442,
                'id_manufacturer' => 79,
                'str_description' => 'chevy van',
                'str_description_slug' => 'chevy-van',
                'bol_active' => 1,
            ),
            442 => 
            array (
                'id_model' => 443,
                'id_manufacturer' => 79,
                'str_description' => 'cheyenne',
                'str_description_slug' => 'cheyenne',
                'bol_active' => 1,
            ),
            443 => 
            array (
                'id_model' => 444,
                'id_manufacturer' => 79,
                'str_description' => 'citation',
                'str_description_slug' => 'citation',
                'bol_active' => 1,
            ),
            444 => 
            array (
                'id_model' => 445,
                'id_manufacturer' => 79,
                'str_description' => 'coach',
                'str_description_slug' => 'coach',
                'bol_active' => 1,
            ),
            445 => 
            array (
                'id_model' => 446,
                'id_manufacturer' => 79,
                'str_description' => 'cobalt',
                'str_description_slug' => 'cobalt',
                'bol_active' => 1,
            ),
            446 => 
            array (
                'id_model' => 447,
                'id_manufacturer' => 79,
                'str_description' => 'cobra',
                'str_description_slug' => 'cobra',
                'bol_active' => 1,
            ),
            447 => 
            array (
                'id_model' => 448,
                'id_manufacturer' => 79,
                'str_description' => 'colorado',
                'str_description_slug' => 'colorado',
                'bol_active' => 1,
            ),
            448 => 
            array (
                'id_model' => 449,
                'id_manufacturer' => 79,
                'str_description' => 'confederate',
                'str_description_slug' => 'confederate',
                'bol_active' => 1,
            ),
            449 => 
            array (
                'id_model' => 450,
                'id_manufacturer' => 79,
                'str_description' => 'corsica',
                'str_description_slug' => 'corsica',
                'bol_active' => 1,
            ),
            450 => 
            array (
                'id_model' => 451,
                'id_manufacturer' => 79,
                'str_description' => 'corvair',
                'str_description_slug' => 'corvair',
                'bol_active' => 1,
            ),
            451 => 
            array (
                'id_model' => 452,
                'id_manufacturer' => 79,
                'str_description' => 'corvette',
                'str_description_slug' => 'corvette',
                'bol_active' => 1,
            ),
            452 => 
            array (
                'id_model' => 453,
                'id_manufacturer' => 79,
                'str_description' => 'custom',
                'str_description_slug' => 'custom',
                'bol_active' => 1,
            ),
            453 => 
            array (
                'id_model' => 454,
                'id_manufacturer' => 79,
                'str_description' => 'daytona',
                'str_description_slug' => 'daytona',
                'bol_active' => 1,
            ),
            454 => 
            array (
                'id_model' => 455,
                'id_manufacturer' => 79,
                'str_description' => 'desande',
                'str_description_slug' => 'desande',
                'bol_active' => 1,
            ),
            455 => 
            array (
                'id_model' => 456,
                'id_manufacturer' => 79,
                'str_description' => 'el camino',
                'str_description_slug' => 'el-camino',
                'bol_active' => 1,
            ),
            456 => 
            array (
                'id_model' => 457,
                'id_manufacturer' => 79,
                'str_description' => 'equinox',
                'str_description_slug' => 'equinox',
                'bol_active' => 1,
            ),
            457 => 
            array (
                'id_model' => 458,
                'id_manufacturer' => 79,
                'str_description' => 'evanda',
                'str_description_slug' => 'evanda',
                'bol_active' => 1,
            ),
            458 => 
            array (
                'id_model' => 459,
                'id_manufacturer' => 79,
                'str_description' => 'explorer',
                'str_description_slug' => 'explorer',
                'bol_active' => 1,
            ),
            459 => 
            array (
                'id_model' => 460,
                'id_manufacturer' => 79,
                'str_description' => 'express explorer',
                'str_description_slug' => 'express-explorer',
                'bol_active' => 1,
            ),
            460 => 
            array (
                'id_model' => 461,
                'id_manufacturer' => 79,
                'str_description' => 'express van 1500',
                'str_description_slug' => 'express-van-1500',
                'bol_active' => 1,
            ),
            461 => 
            array (
                'id_model' => 462,
                'id_manufacturer' => 79,
                'str_description' => 'fleetline aero',
                'str_description_slug' => 'fleetline-aero',
                'bol_active' => 1,
            ),
            462 => 
            array (
                'id_model' => 463,
                'id_manufacturer' => 79,
                'str_description' => 'fleetmaster',
                'str_description_slug' => 'fleetmaster',
                'bol_active' => 1,
            ),
            463 => 
            array (
                'id_model' => 464,
                'id_manufacturer' => 79,
                'str_description' => 'geo metro',
                'str_description_slug' => 'geo-metro',
                'bol_active' => 1,
            ),
            464 => 
            array (
                'id_model' => 465,
                'id_manufacturer' => 79,
                'str_description' => 'geo storm',
                'str_description_slug' => 'geo-storm',
                'bol_active' => 1,
            ),
            465 => 
            array (
                'id_model' => 466,
                'id_manufacturer' => 79,
                'str_description' => 'geo tracker',
                'str_description_slug' => 'geo-tracker',
                'bol_active' => 1,
            ),
            466 => 
            array (
                'id_model' => 467,
                'id_manufacturer' => 79,
                'str_description' => 'gp 3100',
                'str_description_slug' => 'gp-3100',
                'bol_active' => 1,
            ),
            467 => 
            array (
                'id_model' => 468,
                'id_manufacturer' => 79,
                'str_description' => 'hhr',
                'str_description_slug' => 'hhr',
                'bol_active' => 1,
            ),
            468 => 
            array (
                'id_model' => 469,
                'id_manufacturer' => 79,
                'str_description' => 'impala',
                'str_description_slug' => 'impala',
                'bol_active' => 1,
            ),
            469 => 
            array (
                'id_model' => 470,
                'id_manufacturer' => 79,
                'str_description' => 'independence',
                'str_description_slug' => 'independence',
                'bol_active' => 1,
            ),
            470 => 
            array (
                'id_model' => 471,
                'id_manufacturer' => 79,
                'str_description' => 'internacional',
                'str_description_slug' => 'internacional',
                'bol_active' => 1,
            ),
            471 => 
            array (
                'id_model' => 472,
                'id_manufacturer' => 79,
                'str_description' => 'k-14',
                'str_description_slug' => 'k-14',
                'bol_active' => 1,
            ),
            472 => 
            array (
                'id_model' => 473,
                'id_manufacturer' => 79,
                'str_description' => 'k-20',
                'str_description_slug' => 'k-20',
                'bol_active' => 1,
            ),
            473 => 
            array (
                'id_model' => 474,
                'id_manufacturer' => 79,
                'str_description' => 'lq',
                'str_description_slug' => 'lq',
                'bol_active' => 1,
            ),
            474 => 
            array (
                'id_model' => 475,
                'id_manufacturer' => 79,
                'str_description' => 'lumina',
                'str_description_slug' => 'lumina',
                'bol_active' => 1,
            ),
            475 => 
            array (
                'id_model' => 476,
                'id_manufacturer' => 79,
                'str_description' => 'luv',
                'str_description_slug' => 'luv',
                'bol_active' => 1,
            ),
            476 => 
            array (
                'id_model' => 477,
                'id_manufacturer' => 79,
                'str_description' => 'malibu',
                'str_description_slug' => 'malibu',
                'bol_active' => 1,
            ),
            477 => 
            array (
                'id_model' => 478,
                'id_manufacturer' => 79,
                'str_description' => 'master sedan',
                'str_description_slug' => 'master-sedan',
                'bol_active' => 1,
            ),
            478 => 
            array (
                'id_model' => 479,
                'id_manufacturer' => 79,
                'str_description' => 'master series',
                'str_description_slug' => 'master-series',
                'bol_active' => 1,
            ),
            479 => 
            array (
                'id_model' => 480,
                'id_manufacturer' => 79,
                'str_description' => 'montecarlo',
                'str_description_slug' => 'montecarlo',
                'bol_active' => 1,
            ),
            480 => 
            array (
                'id_model' => 481,
                'id_manufacturer' => 79,
                'str_description' => 'monza',
                'str_description_slug' => 'monza',
                'bol_active' => 1,
            ),
            481 => 
            array (
                'id_model' => 482,
                'id_manufacturer' => 79,
                'str_description' => 'national ab',
                'str_description_slug' => 'national-ab',
                'bol_active' => 1,
            ),
            482 => 
            array (
                'id_model' => 483,
                'id_manufacturer' => 79,
                'str_description' => 'nova',
                'str_description_slug' => 'nova',
                'bol_active' => 1,
            ),
            483 => 
            array (
                'id_model' => 484,
                'id_manufacturer' => 79,
                'str_description' => 'orlando',
                'str_description_slug' => 'orlando',
                'bol_active' => 1,
            ),
            484 => 
            array (
                'id_model' => 485,
                'id_manufacturer' => 79,
                'str_description' => 'phaeton',
                'str_description_slug' => 'phaeton',
                'bol_active' => 1,
            ),
            485 => 
            array (
                'id_model' => 486,
                'id_manufacturer' => 79,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            486 => 
            array (
                'id_model' => 487,
                'id_manufacturer' => 79,
                'str_description' => 'sedan',
                'str_description_slug' => 'sedan',
                'bol_active' => 1,
            ),
            487 => 
            array (
                'id_model' => 488,
                'id_manufacturer' => 79,
                'str_description' => 'series v',
                'str_description_slug' => 'series-v',
                'bol_active' => 1,
            ),
            488 => 
            array (
                'id_model' => 489,
                'id_manufacturer' => 79,
                'str_description' => 'silverado',
                'str_description_slug' => 'silverado',
                'bol_active' => 1,
            ),
            489 => 
            array (
                'id_model' => 490,
                'id_manufacturer' => 79,
                'str_description' => 'sonic',
                'str_description_slug' => 'sonic',
                'bol_active' => 1,
            ),
            490 => 
            array (
                'id_model' => 491,
                'id_manufacturer' => 79,
                'str_description' => 'special deluxe',
                'str_description_slug' => 'special-deluxe',
                'bol_active' => 1,
            ),
            491 => 
            array (
                'id_model' => 492,
                'id_manufacturer' => 79,
                'str_description' => 'sprint',
                'str_description_slug' => 'sprint',
                'bol_active' => 1,
            ),
            492 => 
            array (
                'id_model' => 493,
                'id_manufacturer' => 79,
                'str_description' => 'ssr',
                'str_description_slug' => 'ssr',
                'bol_active' => 1,
            ),
            493 => 
            array (
                'id_model' => 494,
                'id_manufacturer' => 79,
                'str_description' => 'standard series',
                'str_description_slug' => 'standard-series',
                'bol_active' => 1,
            ),
            494 => 
            array (
                'id_model' => 495,
                'id_manufacturer' => 79,
                'str_description' => 'starcraft',
                'str_description_slug' => 'starcraft',
                'bol_active' => 1,
            ),
            495 => 
            array (
                'id_model' => 496,
                'id_manufacturer' => 79,
                'str_description' => 'stepvan',
                'str_description_slug' => 'stepvan',
                'bol_active' => 1,
            ),
            496 => 
            array (
                'id_model' => 497,
                'id_manufacturer' => 79,
                'str_description' => 'styline deluxe',
                'str_description_slug' => 'styline-deluxe',
                'bol_active' => 1,
            ),
            497 => 
            array (
                'id_model' => 498,
                'id_manufacturer' => 79,
                'str_description' => 'suburban',
                'str_description_slug' => 'suburban',
                'bol_active' => 1,
            ),
            498 => 
            array (
                'id_model' => 499,
                'id_manufacturer' => 79,
                'str_description' => 'tahoe',
                'str_description_slug' => 'tahoe',
                'bol_active' => 1,
            ),
            499 => 
            array (
                'id_model' => 500,
                'id_manufacturer' => 79,
                'str_description' => 'tracker',
                'str_description_slug' => 'tracker',
                'bol_active' => 1,
            ),
        ));
        \DB::table('automobile_models')->insert(array (
            0 => 
            array (
                'id_model' => 501,
                'id_manufacturer' => 79,
                'str_description' => 'trailblazer',
                'str_description_slug' => 'trailblazer',
                'bol_active' => 1,
            ),
            1 => 
            array (
                'id_model' => 502,
                'id_manufacturer' => 79,
                'str_description' => 'trans sport',
                'str_description_slug' => 'trans-sport',
                'bol_active' => 1,
            ),
            2 => 
            array (
                'id_model' => 503,
                'id_manufacturer' => 79,
                'str_description' => 'traverse',
                'str_description_slug' => 'traverse',
                'bol_active' => 1,
            ),
            3 => 
            array (
                'id_model' => 504,
                'id_manufacturer' => 79,
                'str_description' => 'trax',
                'str_description_slug' => 'trax',
                'bol_active' => 1,
            ),
            4 => 
            array (
                'id_model' => 505,
                'id_manufacturer' => 79,
                'str_description' => 'typhoon',
                'str_description_slug' => 'typhoon',
                'bol_active' => 1,
            ),
            5 => 
            array (
                'id_model' => 506,
                'id_manufacturer' => 79,
                'str_description' => 'van 20',
                'str_description_slug' => 'van-20',
                'bol_active' => 1,
            ),
            6 => 
            array (
                'id_model' => 507,
                'id_manufacturer' => 79,
                'str_description' => 'vandura',
                'str_description_slug' => 'vandura',
                'bol_active' => 1,
            ),
            7 => 
            array (
                'id_model' => 508,
                'id_manufacturer' => 79,
                'str_description' => 'venture',
                'str_description_slug' => 'venture',
                'bol_active' => 1,
            ),
            8 => 
            array (
                'id_model' => 509,
                'id_manufacturer' => 79,
                'str_description' => 'vitara',
                'str_description_slug' => 'vitara',
                'bol_active' => 1,
            ),
            9 => 
            array (
                'id_model' => 510,
                'id_manufacturer' => 79,
                'str_description' => 'yukon',
                'str_description_slug' => 'yukon',
                'bol_active' => 1,
            ),
            10 => 
            array (
                'id_model' => 511,
                'id_manufacturer' => 80,
                'str_description' => 'xy 300',
                'str_description_slug' => 'xy-300',
                'bol_active' => 1,
            ),
            11 => 
            array (
                'id_model' => 512,
                'id_manufacturer' => 81,
                'str_description' => '2 l',
                'str_description_slug' => '2-l',
                'bol_active' => 1,
            ),
            12 => 
            array (
                'id_model' => 513,
                'id_manufacturer' => 81,
                'str_description' => '75',
                'str_description_slug' => '75',
                'bol_active' => 1,
            ),
            13 => 
            array (
                'id_model' => 514,
                'id_manufacturer' => 81,
                'str_description' => '150',
                'str_description_slug' => '150',
                'bol_active' => 1,
            ),
            14 => 
            array (
                'id_model' => 515,
                'id_manufacturer' => 81,
                'str_description' => '180',
                'str_description_slug' => '180',
                'bol_active' => 1,
            ),
            15 => 
            array (
                'id_model' => 516,
                'id_manufacturer' => 81,
                'str_description' => '200',
                'str_description_slug' => '200',
                'bol_active' => 1,
            ),
            16 => 
            array (
                'id_model' => 517,
                'id_manufacturer' => 81,
                'str_description' => '200 s',
                'str_description_slug' => '200-s',
                'bol_active' => 1,
            ),
            17 => 
            array (
                'id_model' => 518,
                'id_manufacturer' => 81,
                'str_description' => '300 c',
                'str_description_slug' => '300-c',
                'bol_active' => 1,
            ),
            18 => 
            array (
                'id_model' => 519,
                'id_manufacturer' => 81,
                'str_description' => '300 m',
                'str_description_slug' => '300-m',
                'bol_active' => 1,
            ),
            19 => 
            array (
                'id_model' => 520,
                'id_manufacturer' => 81,
                'str_description' => '1309',
                'str_description_slug' => '1309',
                'bol_active' => 1,
            ),
            20 => 
            array (
                'id_model' => 521,
                'id_manufacturer' => 81,
                'str_description' => 'aspen',
                'str_description_slug' => 'aspen',
                'bol_active' => 1,
            ),
            21 => 
            array (
                'id_model' => 522,
                'id_manufacturer' => 81,
                'str_description' => 'caravan',
                'str_description_slug' => 'caravan',
                'bol_active' => 1,
            ),
            22 => 
            array (
                'id_model' => 523,
                'id_manufacturer' => 81,
                'str_description' => 'cirrus',
                'str_description_slug' => 'cirrus',
                'bol_active' => 1,
            ),
            23 => 
            array (
                'id_model' => 524,
                'id_manufacturer' => 81,
                'str_description' => 'concorde',
                'str_description_slug' => 'concorde',
                'bol_active' => 1,
            ),
            24 => 
            array (
                'id_model' => 525,
                'id_manufacturer' => 81,
                'str_description' => 'crossfire',
                'str_description_slug' => 'crossfire',
                'bol_active' => 1,
            ),
            25 => 
            array (
                'id_model' => 526,
                'id_manufacturer' => 81,
                'str_description' => 'daytona shelby',
                'str_description_slug' => 'daytona-shelby',
                'bol_active' => 1,
            ),
            26 => 
            array (
                'id_model' => 527,
                'id_manufacturer' => 81,
                'str_description' => 'dynasty',
                'str_description_slug' => 'dynasty',
                'bol_active' => 1,
            ),
            27 => 
            array (
                'id_model' => 528,
                'id_manufacturer' => 81,
                'str_description' => 'es',
                'str_description_slug' => 'es',
                'bol_active' => 1,
            ),
            28 => 
            array (
                'id_model' => 529,
                'id_manufacturer' => 81,
                'str_description' => 'grand caravan',
                'str_description_slug' => 'grand-caravan',
                'bol_active' => 1,
            ),
            29 => 
            array (
                'id_model' => 530,
                'id_manufacturer' => 81,
                'str_description' => 'grand voyager',
                'str_description_slug' => 'grand-voyager',
                'bol_active' => 1,
            ),
            30 => 
            array (
                'id_model' => 531,
                'id_manufacturer' => 81,
                'str_description' => 'gs',
                'str_description_slug' => 'gs',
                'bol_active' => 1,
            ),
            31 => 
            array (
                'id_model' => 532,
                'id_manufacturer' => 81,
                'str_description' => 'imperial',
                'str_description_slug' => 'imperial',
                'bol_active' => 1,
            ),
            32 => 
            array (
                'id_model' => 533,
                'id_manufacturer' => 81,
                'str_description' => 'lebaron',
                'str_description_slug' => 'lebaron',
                'bol_active' => 1,
            ),
            33 => 
            array (
                'id_model' => 534,
                'id_manufacturer' => 81,
                'str_description' => 'lhs',
                'str_description_slug' => 'lhs',
                'bol_active' => 1,
            ),
            34 => 
            array (
                'id_model' => 535,
                'id_manufacturer' => 81,
                'str_description' => 'neon',
                'str_description_slug' => 'neon',
                'bol_active' => 1,
            ),
            35 => 
            array (
                'id_model' => 536,
                'id_manufacturer' => 81,
                'str_description' => 'new yorker',
                'str_description_slug' => 'new-yorker',
                'bol_active' => 1,
            ),
            36 => 
            array (
                'id_model' => 537,
                'id_manufacturer' => 81,
                'str_description' => 'newport',
                'str_description_slug' => 'newport',
                'bol_active' => 1,
            ),
            37 => 
            array (
                'id_model' => 538,
                'id_manufacturer' => 81,
                'str_description' => 'pacifica',
                'str_description_slug' => 'pacifica',
                'bol_active' => 1,
            ),
            38 => 
            array (
                'id_model' => 539,
                'id_manufacturer' => 81,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            39 => 
            array (
                'id_model' => 540,
                'id_manufacturer' => 81,
                'str_description' => 'pt cruiser',
                'str_description_slug' => 'pt-cruiser',
                'bol_active' => 1,
            ),
            40 => 
            array (
                'id_model' => 541,
                'id_manufacturer' => 81,
                'str_description' => 'royal',
                'str_description_slug' => 'royal',
                'bol_active' => 1,
            ),
            41 => 
            array (
                'id_model' => 542,
                'id_manufacturer' => 81,
                'str_description' => 'saratoga',
                'str_description_slug' => 'saratoga',
                'bol_active' => 1,
            ),
            42 => 
            array (
                'id_model' => 543,
                'id_manufacturer' => 81,
                'str_description' => 'sebring',
                'str_description_slug' => 'sebring',
                'bol_active' => 1,
            ),
            43 => 
            array (
                'id_model' => 544,
                'id_manufacturer' => 81,
                'str_description' => 'sedan',
                'str_description_slug' => 'sedan',
                'bol_active' => 1,
            ),
            44 => 
            array (
                'id_model' => 545,
                'id_manufacturer' => 81,
                'str_description' => 'six',
                'str_description_slug' => 'six',
                'bol_active' => 1,
            ),
            45 => 
            array (
                'id_model' => 546,
                'id_manufacturer' => 81,
                'str_description' => 'spirit',
                'str_description_slug' => 'spirit',
                'bol_active' => 1,
            ),
            46 => 
            array (
                'id_model' => 547,
                'id_manufacturer' => 81,
                'str_description' => 'stratus',
                'str_description_slug' => 'stratus',
                'bol_active' => 1,
            ),
            47 => 
            array (
                'id_model' => 548,
                'id_manufacturer' => 81,
                'str_description' => 'talon',
                'str_description_slug' => 'talon',
                'bol_active' => 1,
            ),
            48 => 
            array (
                'id_model' => 549,
                'id_manufacturer' => 81,
                'str_description' => 'tc',
                'str_description_slug' => 'tc',
                'bol_active' => 1,
            ),
            49 => 
            array (
                'id_model' => 550,
                'id_manufacturer' => 81,
                'str_description' => 'town & country',
                'str_description_slug' => 'town-&-country',
                'bol_active' => 1,
            ),
            50 => 
            array (
                'id_model' => 551,
                'id_manufacturer' => 81,
                'str_description' => 'viper',
                'str_description_slug' => 'viper',
                'bol_active' => 1,
            ),
            51 => 
            array (
                'id_model' => 552,
                'id_manufacturer' => 81,
                'str_description' => 'vision',
                'str_description_slug' => 'vision',
                'bol_active' => 1,
            ),
            52 => 
            array (
                'id_model' => 553,
                'id_manufacturer' => 81,
                'str_description' => 'voyager',
                'str_description_slug' => 'voyager',
                'bol_active' => 1,
            ),
            53 => 
            array (
                'id_model' => 554,
                'id_manufacturer' => 81,
                'str_description' => 'windsor',
                'str_description_slug' => 'windsor',
                'bol_active' => 1,
            ),
            54 => 
            array (
                'id_model' => 555,
                'id_manufacturer' => 82,
                'str_description' => '2 cv',
                'str_description_slug' => '2-cv',
                'bol_active' => 1,
            ),
            55 => 
            array (
                'id_model' => 556,
                'id_manufacturer' => 82,
                'str_description' => '5 cv',
                'str_description_slug' => '5-cv',
                'bol_active' => 1,
            ),
            56 => 
            array (
                'id_model' => 557,
                'id_manufacturer' => 82,
                'str_description' => '11',
                'str_description_slug' => '11',
                'bol_active' => 1,
            ),
            57 => 
            array (
                'id_model' => 558,
                'id_manufacturer' => 82,
                'str_description' => '15',
                'str_description_slug' => '15',
                'bol_active' => 1,
            ),
            58 => 
            array (
                'id_model' => 559,
                'id_manufacturer' => 82,
                'str_description' => 'ac4',
                'str_description_slug' => 'ac4',
                'bol_active' => 1,
            ),
            59 => 
            array (
                'id_model' => 560,
                'id_manufacturer' => 82,
                'str_description' => 'ak',
                'str_description_slug' => 'ak',
                'bol_active' => 1,
            ),
            60 => 
            array (
                'id_model' => 561,
                'id_manufacturer' => 82,
                'str_description' => 'aks',
                'str_description_slug' => 'aks',
                'bol_active' => 1,
            ),
            61 => 
            array (
                'id_model' => 562,
                'id_manufacturer' => 82,
                'str_description' => 'amb',
                'str_description_slug' => 'amb',
                'bol_active' => 1,
            ),
            62 => 
            array (
                'id_model' => 563,
                'id_manufacturer' => 82,
                'str_description' => 'ami',
                'str_description_slug' => 'ami',
                'bol_active' => 1,
            ),
            63 => 
            array (
                'id_model' => 564,
                'id_manufacturer' => 82,
                'str_description' => 'ax',
                'str_description_slug' => 'ax',
                'bol_active' => 1,
            ),
            64 => 
            array (
                'id_model' => 565,
                'id_manufacturer' => 82,
                'str_description' => 'axel',
                'str_description_slug' => 'axel',
                'bol_active' => 1,
            ),
            65 => 
            array (
                'id_model' => 566,
                'id_manufacturer' => 82,
                'str_description' => 'b 11',
                'str_description_slug' => 'b-11',
                'bol_active' => 1,
            ),
            66 => 
            array (
                'id_model' => 567,
                'id_manufacturer' => 82,
                'str_description' => 'b 12',
                'str_description_slug' => 'b-12',
                'bol_active' => 1,
            ),
            67 => 
            array (
                'id_model' => 568,
                'id_manufacturer' => 82,
                'str_description' => 'b14',
                'str_description_slug' => 'b14',
                'bol_active' => 1,
            ),
            68 => 
            array (
                'id_model' => 569,
                'id_manufacturer' => 82,
                'str_description' => 'berlingo first',
                'str_description_slug' => 'berlingo-first',
                'bol_active' => 1,
            ),
            69 => 
            array (
                'id_model' => 570,
                'id_manufacturer' => 82,
                'str_description' => 'bx',
                'str_description_slug' => 'bx',
                'bol_active' => 1,
            ),
            70 => 
            array (
                'id_model' => 571,
                'id_manufacturer' => 82,
                'str_description' => 'c 35',
                'str_description_slug' => 'c-35',
                'bol_active' => 1,
            ),
            71 => 
            array (
                'id_model' => 572,
                'id_manufacturer' => 82,
                'str_description' => 'c-15',
                'str_description_slug' => 'c-15',
                'bol_active' => 1,
            ),
            72 => 
            array (
                'id_model' => 573,
                'id_manufacturer' => 82,
                'str_description' => 'c-25',
                'str_description_slug' => 'c-25',
                'bol_active' => 1,
            ),
            73 => 
            array (
                'id_model' => 574,
                'id_manufacturer' => 82,
                'str_description' => 'c-4',
                'str_description_slug' => 'c-4',
                'bol_active' => 1,
            ),
            74 => 
            array (
                'id_model' => 575,
                'id_manufacturer' => 82,
                'str_description' => 'c-6',
                'str_description_slug' => 'c-6',
                'bol_active' => 1,
            ),
            75 => 
            array (
                'id_model' => 576,
                'id_manufacturer' => 82,
                'str_description' => 'c-8',
                'str_description_slug' => 'c-8',
                'bol_active' => 1,
            ),
            76 => 
            array (
                'id_model' => 577,
                'id_manufacturer' => 82,
                'str_description' => 'c-crosser',
                'str_description_slug' => 'c-crosser',
                'bol_active' => 1,
            ),
            77 => 
            array (
                'id_model' => 578,
                'id_manufacturer' => 82,
                'str_description' => 'c-elysee',
                'str_description_slug' => 'c-elysee',
                'bol_active' => 1,
            ),
            78 => 
            array (
                'id_model' => 579,
                'id_manufacturer' => 82,
                'str_description' => 'c-zero',
                'str_description_slug' => 'c-zero',
                'bol_active' => 1,
            ),
            79 => 
            array (
                'id_model' => 580,
                'id_manufacturer' => 82,
                'str_description' => 'c1',
                'str_description_slug' => 'c1',
                'bol_active' => 1,
            ),
            80 => 
            array (
                'id_model' => 581,
                'id_manufacturer' => 82,
                'str_description' => 'c3 picasso',
                'str_description_slug' => 'c3-picasso',
                'bol_active' => 1,
            ),
            81 => 
            array (
                'id_model' => 582,
                'id_manufacturer' => 82,
                'str_description' => 'c3 pluriel',
                'str_description_slug' => 'c3-pluriel',
                'bol_active' => 1,
            ),
            82 => 
            array (
                'id_model' => 583,
                'id_manufacturer' => 82,
                'str_description' => 'c4 aircross',
                'str_description_slug' => 'c4-aircross',
                'bol_active' => 1,
            ),
            83 => 
            array (
                'id_model' => 584,
                'id_manufacturer' => 82,
                'str_description' => 'c4 cactus',
                'str_description_slug' => 'c4-cactus',
                'bol_active' => 1,
            ),
            84 => 
            array (
                'id_model' => 585,
                'id_manufacturer' => 82,
                'str_description' => 'c4 grand picasso',
                'str_description_slug' => 'c4-grand-picasso',
                'bol_active' => 1,
            ),
            85 => 
            array (
                'id_model' => 586,
                'id_manufacturer' => 82,
                'str_description' => 'c4 picasso 7pz',
                'str_description_slug' => 'c4-picasso-7pz',
                'bol_active' => 1,
            ),
            86 => 
            array (
                'id_model' => 587,
                'id_manufacturer' => 82,
                'str_description' => 'c4g',
                'str_description_slug' => 'c4g',
                'bol_active' => 1,
            ),
            87 => 
            array (
                'id_model' => 588,
                'id_manufacturer' => 82,
                'str_description' => 'c6',
                'str_description_slug' => 'c6',
                'bol_active' => 1,
            ),
            88 => 
            array (
                'id_model' => 589,
                'id_manufacturer' => 82,
                'str_description' => 'c8',
                'str_description_slug' => 'c8',
                'bol_active' => 1,
            ),
            89 => 
            array (
                'id_model' => 590,
                'id_manufacturer' => 82,
                'str_description' => 'cx',
                'str_description_slug' => 'cx',
                'bol_active' => 1,
            ),
            90 => 
            array (
                'id_model' => 591,
                'id_manufacturer' => 82,
                'str_description' => 'ds3',
                'str_description_slug' => 'ds3',
                'bol_active' => 1,
            ),
            91 => 
            array (
                'id_model' => 592,
                'id_manufacturer' => 82,
                'str_description' => 'ds4',
                'str_description_slug' => 'ds4',
                'bol_active' => 1,
            ),
            92 => 
            array (
                'id_model' => 593,
                'id_manufacturer' => 82,
                'str_description' => 'ds5',
                'str_description_slug' => 'ds5',
                'bol_active' => 1,
            ),
            93 => 
            array (
                'id_model' => 594,
                'id_manufacturer' => 82,
                'str_description' => 'dsuper',
                'str_description_slug' => 'dsuper',
                'bol_active' => 1,
            ),
            94 => 
            array (
                'id_model' => 595,
                'id_manufacturer' => 82,
                'str_description' => 'dyane 6',
                'str_description_slug' => 'dyane-6',
                'bol_active' => 1,
            ),
            95 => 
            array (
                'id_model' => 596,
                'id_manufacturer' => 82,
                'str_description' => 'dynam',
                'str_description_slug' => 'dynam',
                'bol_active' => 1,
            ),
            96 => 
            array (
                'id_model' => 597,
                'id_manufacturer' => 82,
                'str_description' => 'e-mahari',
                'str_description_slug' => 'e-mahari',
                'bol_active' => 1,
            ),
            97 => 
            array (
                'id_model' => 598,
                'id_manufacturer' => 82,
                'str_description' => 'evasion',
                'str_description_slug' => 'evasion',
                'bol_active' => 1,
            ),
            98 => 
            array (
                'id_model' => 599,
                'id_manufacturer' => 82,
                'str_description' => 'gs',
                'str_description_slug' => 'gs',
                'bol_active' => 1,
            ),
            99 => 
            array (
                'id_model' => 600,
                'id_manufacturer' => 82,
                'str_description' => 'gsa',
                'str_description_slug' => 'gsa',
                'bol_active' => 1,
            ),
            100 => 
            array (
                'id_model' => 601,
                'id_manufacturer' => 82,
                'str_description' => 'h',
                'str_description_slug' => 'h',
                'bol_active' => 1,
            ),
            101 => 
            array (
                'id_model' => 602,
                'id_manufacturer' => 82,
                'str_description' => 'hy',
                'str_description_slug' => 'hy',
                'bol_active' => 1,
            ),
            102 => 
            array (
                'id_model' => 603,
                'id_manufacturer' => 82,
                'str_description' => 'jumper',
                'str_description_slug' => 'jumper',
                'bol_active' => 1,
            ),
            103 => 
            array (
                'id_model' => 604,
                'id_manufacturer' => 82,
                'str_description' => 'jumpy',
                'str_description_slug' => 'jumpy',
                'bol_active' => 1,
            ),
            104 => 
            array (
                'id_model' => 605,
                'id_manufacturer' => 82,
                'str_description' => 'lna',
                'str_description_slug' => 'lna',
                'bol_active' => 1,
            ),
            105 => 
            array (
                'id_model' => 606,
                'id_manufacturer' => 82,
                'str_description' => 'mehari',
                'str_description_slug' => 'mehari',
                'bol_active' => 1,
            ),
            106 => 
            array (
                'id_model' => 607,
                'id_manufacturer' => 82,
                'str_description' => 'nemo',
                'str_description_slug' => 'nemo',
                'bol_active' => 1,
            ),
            107 => 
            array (
                'id_model' => 608,
                'id_manufacturer' => 82,
                'str_description' => 'relay',
                'str_description_slug' => 'relay',
                'bol_active' => 1,
            ),
            108 => 
            array (
                'id_model' => 609,
                'id_manufacturer' => 82,
                'str_description' => 'rosalie',
                'str_description_slug' => 'rosalie',
                'bol_active' => 1,
            ),
            109 => 
            array (
                'id_model' => 610,
                'id_manufacturer' => 82,
                'str_description' => 'sm',
                'str_description_slug' => 'sm',
                'bol_active' => 1,
            ),
            110 => 
            array (
                'id_model' => 611,
                'id_manufacturer' => 82,
                'str_description' => 'spacetourer',
                'str_description_slug' => 'spacetourer',
                'bol_active' => 1,
            ),
            111 => 
            array (
                'id_model' => 612,
                'id_manufacturer' => 82,
                'str_description' => 'tipo a',
                'str_description_slug' => 'tipo-a',
                'bol_active' => 1,
            ),
            112 => 
            array (
                'id_model' => 613,
                'id_manufacturer' => 82,
                'str_description' => 'torpedo',
                'str_description_slug' => 'torpedo',
                'bol_active' => 1,
            ),
            113 => 
            array (
                'id_model' => 614,
                'id_manufacturer' => 82,
                'str_description' => 'traccion 7b',
                'str_description_slug' => 'traccion-7b',
                'bol_active' => 1,
            ),
            114 => 
            array (
                'id_model' => 615,
                'id_manufacturer' => 82,
                'str_description' => 'visa',
                'str_description_slug' => 'visa',
                'bol_active' => 1,
            ),
            115 => 
            array (
                'id_model' => 616,
                'id_manufacturer' => 82,
                'str_description' => 'xm',
                'str_description_slug' => 'xm',
                'bol_active' => 1,
            ),
            116 => 
            array (
                'id_model' => 617,
                'id_manufacturer' => 83,
                'str_description' => 'serie ii',
                'str_description_slug' => 'serie-ii',
                'bol_active' => 1,
            ),
            117 => 
            array (
                'id_model' => 618,
                'id_manufacturer' => 83,
                'str_description' => 'serie iii',
                'str_description_slug' => 'serie-iii',
                'bol_active' => 1,
            ),
            118 => 
            array (
                'id_model' => 619,
                'id_manufacturer' => 84,
                'str_description' => 'buggy',
                'str_description_slug' => 'buggy',
                'bol_active' => 1,
            ),
            119 => 
            array (
                'id_model' => 620,
                'id_manufacturer' => 85,
                'str_description' => 'carryall',
                'str_description_slug' => 'carryall',
                'bol_active' => 1,
            ),
            120 => 
            array (
                'id_model' => 621,
                'id_manufacturer' => 85,
                'str_description' => 'ds player',
                'str_description_slug' => 'ds-player',
                'bol_active' => 1,
            ),
            121 => 
            array (
                'id_model' => 622,
                'id_manufacturer' => 85,
                'str_description' => 'iq system',
                'str_description_slug' => 'iq-system',
                'bol_active' => 1,
            ),
            122 => 
            array (
                'id_model' => 623,
                'id_manufacturer' => 85,
                'str_description' => 'powerdrive',
                'str_description_slug' => 'powerdrive',
                'bol_active' => 1,
            ),
            123 => 
            array (
                'id_model' => 624,
                'id_manufacturer' => 85,
                'str_description' => 'precedent',
                'str_description_slug' => 'precedent',
                'bol_active' => 1,
            ),
            124 => 
            array (
                'id_model' => 625,
                'id_manufacturer' => 85,
                'str_description' => 'run a bout',
                'str_description_slug' => 'run-a-bout',
                'bol_active' => 1,
            ),
            125 => 
            array (
                'id_model' => 626,
                'id_manufacturer' => 85,
                'str_description' => 'tranquility',
                'str_description_slug' => 'tranquility',
                'bol_active' => 1,
            ),
            126 => 
            array (
                'id_model' => 627,
                'id_manufacturer' => 85,
                'str_description' => 'villager',
                'str_description_slug' => 'villager',
                'bol_active' => 1,
            ),
            127 => 
            array (
                'id_model' => 628,
                'id_manufacturer' => 86,
                'str_description' => 'ribot',
                'str_description_slug' => 'ribot',
                'bol_active' => 1,
            ),
            128 => 
            array (
                'id_model' => 629,
                'id_manufacturer' => 87,
                'str_description' => 'cr',
                'str_description_slug' => 'cr',
                'bol_active' => 1,
            ),
            129 => 
            array (
                'id_model' => 630,
                'id_manufacturer' => 87,
                'str_description' => 'cross rider',
                'str_description_slug' => 'cross-rider',
                'bol_active' => 1,
            ),
            130 => 
            array (
                'id_model' => 631,
                'id_manufacturer' => 87,
                'str_description' => 'hdg',
                'str_description_slug' => 'hdg',
                'bol_active' => 1,
            ),
            131 => 
            array (
                'id_model' => 632,
                'id_manufacturer' => 87,
                'str_description' => 'hunting',
                'str_description_slug' => 'hunting',
                'bol_active' => 1,
            ),
            132 => 
            array (
                'id_model' => 633,
                'id_manufacturer' => 87,
                'str_description' => 's1',
                'str_description_slug' => 's1',
                'bol_active' => 1,
            ),
            133 => 
            array (
                'id_model' => 634,
                'id_manufacturer' => 87,
                'str_description' => 's1 x-oly',
                'str_description_slug' => 's1-x-oly',
                'bol_active' => 1,
            ),
            134 => 
            array (
                'id_model' => 635,
                'id_manufacturer' => 87,
                'str_description' => 's1-25',
                'str_description_slug' => 's1-25',
                'bol_active' => 1,
            ),
            135 => 
            array (
                'id_model' => 636,
                'id_manufacturer' => 87,
                'str_description' => 't-bus',
                'str_description_slug' => 't-bus',
                'bol_active' => 1,
            ),
            136 => 
            array (
                'id_model' => 637,
                'id_manufacturer' => 87,
                'str_description' => 'toyrider',
                'str_description_slug' => 'toyrider',
                'bol_active' => 1,
            ),
            137 => 
            array (
                'id_model' => 638,
                'id_manufacturer' => 87,
                'str_description' => 'toytruck',
                'str_description_slug' => 'toytruck',
                'bol_active' => 1,
            ),
            138 => 
            array (
                'id_model' => 639,
                'id_manufacturer' => 87,
                'str_description' => 'xtamy',
                'str_description_slug' => 'xtamy',
                'bol_active' => 1,
            ),
            139 => 
            array (
                'id_model' => 640,
                'id_manufacturer' => 88,
                'str_description' => 'van',
                'str_description_slug' => 'van',
                'bol_active' => 1,
            ),
            140 => 
            array (
                'id_model' => 641,
                'id_manufacturer' => 89,
                'str_description' => 'c6',
                'str_description_slug' => 'c6',
                'bol_active' => 1,
            ),
            141 => 
            array (
                'id_model' => 642,
                'id_manufacturer' => 90,
                'str_description' => 'scartt',
                'str_description_slug' => 'scartt',
                'bol_active' => 1,
            ),
            142 => 
            array (
                'id_model' => 643,
                'id_manufacturer' => 91,
                'str_description' => 'hauler',
                'str_description_slug' => 'hauler',
                'bol_active' => 1,
            ),
            143 => 
            array (
                'id_model' => 644,
                'id_manufacturer' => 91,
                'str_description' => 'hawk',
                'str_description_slug' => 'hawk',
                'bol_active' => 1,
            ),
            144 => 
            array (
                'id_model' => 645,
                'id_manufacturer' => 91,
                'str_description' => 'shuttle',
                'str_description_slug' => 'shuttle',
                'bol_active' => 1,
            ),
            145 => 
            array (
                'id_model' => 646,
                'id_manufacturer' => 91,
                'str_description' => 'turf truckster',
                'str_description_slug' => 'turf-truckster',
                'bol_active' => 1,
            ),
            146 => 
            array (
                'id_model' => 647,
                'id_manufacturer' => 92,
                'str_description' => '1307',
                'str_description_slug' => '1307',
                'bol_active' => 1,
            ),
            147 => 
            array (
                'id_model' => 648,
                'id_manufacturer' => 92,
                'str_description' => 'contac',
                'str_description_slug' => 'contac',
                'bol_active' => 1,
            ),
            148 => 
            array (
                'id_model' => 649,
                'id_manufacturer' => 92,
                'str_description' => 'dokker',
                'str_description_slug' => 'dokker',
                'bol_active' => 1,
            ),
            149 => 
            array (
                'id_model' => 650,
                'id_manufacturer' => 92,
                'str_description' => 'dokker stepway',
                'str_description_slug' => 'dokker-stepway',
                'bol_active' => 1,
            ),
            150 => 
            array (
                'id_model' => 651,
                'id_manufacturer' => 92,
                'str_description' => 'duster',
                'str_description_slug' => 'duster',
                'bol_active' => 1,
            ),
            151 => 
            array (
                'id_model' => 652,
                'id_manufacturer' => 92,
                'str_description' => 'lodgy',
                'str_description_slug' => 'lodgy',
                'bol_active' => 1,
            ),
            152 => 
            array (
                'id_model' => 653,
                'id_manufacturer' => 92,
                'str_description' => 'lodgy stepway',
                'str_description_slug' => 'lodgy-stepway',
                'bol_active' => 1,
            ),
            153 => 
            array (
                'id_model' => 654,
                'id_manufacturer' => 92,
                'str_description' => 'logan',
                'str_description_slug' => 'logan',
                'bol_active' => 1,
            ),
            154 => 
            array (
                'id_model' => 655,
                'id_manufacturer' => 92,
                'str_description' => 'sandero',
                'str_description_slug' => 'sandero',
                'bol_active' => 1,
            ),
            155 => 
            array (
                'id_model' => 656,
                'id_manufacturer' => 92,
                'str_description' => 'sandero stepway',
                'str_description_slug' => 'sandero-stepway',
                'bol_active' => 1,
            ),
            156 => 
            array (
                'id_model' => 657,
                'id_manufacturer' => 92,
                'str_description' => 'super nova',
                'str_description_slug' => 'super-nova',
                'bol_active' => 1,
            ),
            157 => 
            array (
                'id_model' => 658,
                'id_manufacturer' => 93,
                'str_description' => 'aranos',
                'str_description_slug' => 'aranos',
                'bol_active' => 1,
            ),
            158 => 
            array (
                'id_model' => 659,
                'id_manufacturer' => 93,
                'str_description' => 'compact',
                'str_description_slug' => 'compact',
                'bol_active' => 1,
            ),
            159 => 
            array (
                'id_model' => 660,
                'id_manufacturer' => 93,
                'str_description' => 'damas',
                'str_description_slug' => 'damas',
                'bol_active' => 1,
            ),
            160 => 
            array (
                'id_model' => 661,
                'id_manufacturer' => 93,
                'str_description' => 'espero',
                'str_description_slug' => 'espero',
                'bol_active' => 1,
            ),
            161 => 
            array (
                'id_model' => 662,
                'id_manufacturer' => 93,
                'str_description' => 'evanda',
                'str_description_slug' => 'evanda',
                'bol_active' => 1,
            ),
            162 => 
            array (
                'id_model' => 663,
                'id_manufacturer' => 93,
                'str_description' => 'kalos',
                'str_description_slug' => 'kalos',
                'bol_active' => 1,
            ),
            163 => 
            array (
                'id_model' => 664,
                'id_manufacturer' => 93,
                'str_description' => 'korando kj',
                'str_description_slug' => 'korando-kj',
                'bol_active' => 1,
            ),
            164 => 
            array (
                'id_model' => 665,
                'id_manufacturer' => 93,
                'str_description' => 'lacetti',
                'str_description_slug' => 'lacetti',
                'bol_active' => 1,
            ),
            165 => 
            array (
                'id_model' => 666,
                'id_manufacturer' => 93,
                'str_description' => 'lanos',
                'str_description_slug' => 'lanos',
                'bol_active' => 1,
            ),
            166 => 
            array (
                'id_model' => 667,
                'id_manufacturer' => 93,
                'str_description' => 'leganza',
                'str_description_slug' => 'leganza',
                'bol_active' => 1,
            ),
            167 => 
            array (
                'id_model' => 668,
                'id_manufacturer' => 93,
                'str_description' => 'lublin 3',
                'str_description_slug' => 'lublin-3',
                'bol_active' => 1,
            ),
            168 => 
            array (
                'id_model' => 669,
                'id_manufacturer' => 93,
                'str_description' => 'matiz',
                'str_description_slug' => 'matiz',
                'bol_active' => 1,
            ),
            169 => 
            array (
                'id_model' => 670,
                'id_manufacturer' => 93,
                'str_description' => 'nexia',
                'str_description_slug' => 'nexia',
                'bol_active' => 1,
            ),
            170 => 
            array (
                'id_model' => 671,
                'id_manufacturer' => 93,
                'str_description' => 'nubira',
                'str_description_slug' => 'nubira',
                'bol_active' => 1,
            ),
            171 => 
            array (
                'id_model' => 672,
                'id_manufacturer' => 93,
                'str_description' => 'rezzo',
                'str_description_slug' => 'rezzo',
                'bol_active' => 1,
            ),
            172 => 
            array (
                'id_model' => 673,
                'id_manufacturer' => 93,
                'str_description' => 'tacuma',
                'str_description_slug' => 'tacuma',
                'bol_active' => 1,
            ),
            173 => 
            array (
                'id_model' => 674,
                'id_manufacturer' => 93,
                'str_description' => 'tico',
                'str_description_slug' => 'tico',
                'bol_active' => 1,
            ),
            174 => 
            array (
                'id_model' => 675,
                'id_manufacturer' => 94,
                'str_description' => '46',
                'str_description_slug' => '46',
                'bol_active' => 1,
            ),
            175 => 
            array (
                'id_model' => 676,
                'id_manufacturer' => 94,
                'str_description' => '55',
                'str_description_slug' => '55',
                'bol_active' => 1,
            ),
            176 => 
            array (
                'id_model' => 677,
                'id_manufacturer' => 94,
                'str_description' => 'freight rover',
                'str_description_slug' => 'freight-rover',
                'bol_active' => 1,
            ),
            177 => 
            array (
                'id_model' => 678,
                'id_manufacturer' => 94,
                'str_description' => 'va 200',
                'str_description_slug' => 'va-200',
                'bol_active' => 1,
            ),
            178 => 
            array (
                'id_model' => 679,
                'id_manufacturer' => 94,
                'str_description' => 'va 400',
                'str_description_slug' => 'va-400',
                'bol_active' => 1,
            ),
            179 => 
            array (
                'id_model' => 680,
                'id_manufacturer' => 94,
                'str_description' => 'vd 400',
                'str_description_slug' => 'vd-400',
                'bol_active' => 1,
            ),
            180 => 
            array (
                'id_model' => 681,
                'id_manufacturer' => 94,
                'str_description' => 'vh 400',
                'str_description_slug' => 'vh-400',
                'bol_active' => 1,
            ),
            181 => 
            array (
                'id_model' => 682,
                'id_manufacturer' => 94,
                'str_description' => 'vh 431',
                'str_description_slug' => 'vh-431',
                'bol_active' => 1,
            ),
            182 => 
            array (
                'id_model' => 683,
                'id_manufacturer' => 94,
                'str_description' => 'vs 200',
                'str_description_slug' => 'vs-200',
                'bol_active' => 1,
            ),
            183 => 
            array (
                'id_model' => 684,
                'id_manufacturer' => 94,
                'str_description' => 'vs 400',
                'str_description_slug' => 'vs-400',
                'bol_active' => 1,
            ),
            184 => 
            array (
                'id_model' => 685,
                'id_manufacturer' => 94,
                'str_description' => 'vx 400',
                'str_description_slug' => 'vx-400',
                'bol_active' => 1,
            ),
            185 => 
            array (
                'id_model' => 686,
                'id_manufacturer' => 94,
                'str_description' => 'ya 126',
                'str_description_slug' => 'ya-126',
                'bol_active' => 1,
            ),
            186 => 
            array (
                'id_model' => 687,
                'id_manufacturer' => 95,
                'str_description' => 'applause',
                'str_description_slug' => 'applause',
                'bol_active' => 1,
            ),
            187 => 
            array (
                'id_model' => 688,
                'id_manufacturer' => 95,
                'str_description' => 'charade',
                'str_description_slug' => 'charade',
                'bol_active' => 1,
            ),
            188 => 
            array (
                'id_model' => 689,
                'id_manufacturer' => 95,
                'str_description' => 'charmant',
                'str_description_slug' => 'charmant',
                'bol_active' => 1,
            ),
            189 => 
            array (
                'id_model' => 690,
                'id_manufacturer' => 95,
                'str_description' => 'copen',
                'str_description_slug' => 'copen',
                'bol_active' => 1,
            ),
            190 => 
            array (
                'id_model' => 691,
                'id_manufacturer' => 95,
                'str_description' => 'cuore',
                'str_description_slug' => 'cuore',
                'bol_active' => 1,
            ),
            191 => 
            array (
                'id_model' => 692,
                'id_manufacturer' => 95,
                'str_description' => 'delta',
                'str_description_slug' => 'delta',
                'bol_active' => 1,
            ),
            192 => 
            array (
                'id_model' => 693,
                'id_manufacturer' => 95,
                'str_description' => 'domino',
                'str_description_slug' => 'domino',
                'bol_active' => 1,
            ),
            193 => 
            array (
                'id_model' => 694,
                'id_manufacturer' => 95,
                'str_description' => 'f20',
                'str_description_slug' => 'f20',
                'bol_active' => 1,
            ),
            194 => 
            array (
                'id_model' => 695,
                'id_manufacturer' => 95,
                'str_description' => 'f50',
                'str_description_slug' => 'f50',
                'bol_active' => 1,
            ),
            195 => 
            array (
                'id_model' => 696,
                'id_manufacturer' => 95,
                'str_description' => 'f60',
                'str_description_slug' => 'f60',
                'bol_active' => 1,
            ),
            196 => 
            array (
                'id_model' => 697,
                'id_manufacturer' => 95,
                'str_description' => 'feroza',
                'str_description_slug' => 'feroza',
                'bol_active' => 1,
            ),
            197 => 
            array (
                'id_model' => 698,
                'id_manufacturer' => 95,
                'str_description' => 'fourtrak',
                'str_description_slug' => 'fourtrak',
                'bol_active' => 1,
            ),
            198 => 
            array (
                'id_model' => 699,
                'id_manufacturer' => 95,
                'str_description' => 'gran move',
                'str_description_slug' => 'gran-move',
                'bol_active' => 1,
            ),
            199 => 
            array (
                'id_model' => 700,
                'id_manufacturer' => 95,
                'str_description' => 'hijet',
                'str_description_slug' => 'hijet',
                'bol_active' => 1,
            ),
            200 => 
            array (
                'id_model' => 701,
                'id_manufacturer' => 95,
                'str_description' => 'materia',
                'str_description_slug' => 'materia',
                'bol_active' => 1,
            ),
            201 => 
            array (
                'id_model' => 702,
                'id_manufacturer' => 95,
                'str_description' => 'move',
                'str_description_slug' => 'move',
                'bol_active' => 1,
            ),
            202 => 
            array (
                'id_model' => 703,
                'id_manufacturer' => 95,
                'str_description' => 'rocky',
                'str_description_slug' => 'rocky',
                'bol_active' => 1,
            ),
            203 => 
            array (
                'id_model' => 704,
                'id_manufacturer' => 95,
                'str_description' => 'sirion',
                'str_description_slug' => 'sirion',
                'bol_active' => 1,
            ),
            204 => 
            array (
                'id_model' => 705,
                'id_manufacturer' => 95,
                'str_description' => 'taft',
                'str_description_slug' => 'taft',
                'bol_active' => 1,
            ),
            205 => 
            array (
                'id_model' => 706,
                'id_manufacturer' => 95,
                'str_description' => 'terios',
                'str_description_slug' => 'terios',
                'bol_active' => 1,
            ),
            206 => 
            array (
                'id_model' => 707,
                'id_manufacturer' => 95,
                'str_description' => 'trevis',
                'str_description_slug' => 'trevis',
                'bol_active' => 1,
            ),
            207 => 
            array (
                'id_model' => 708,
                'id_manufacturer' => 95,
                'str_description' => 'venom',
                'str_description_slug' => 'venom',
                'bol_active' => 1,
            ),
            208 => 
            array (
                'id_model' => 709,
                'id_manufacturer' => 95,
                'str_description' => 'wildcat',
                'str_description_slug' => 'wildcat',
                'bol_active' => 1,
            ),
            209 => 
            array (
                'id_model' => 710,
                'id_manufacturer' => 95,
                'str_description' => 'yrv',
                'str_description_slug' => 'yrv',
                'bol_active' => 1,
            ),
            210 => 
            array (
                'id_model' => 711,
                'id_manufacturer' => 96,
                'str_description' => 'db',
                'str_description_slug' => 'db',
                'bol_active' => 1,
            ),
            211 => 
            array (
                'id_model' => 712,
                'id_manufacturer' => 96,
                'str_description' => 'limousine',
                'str_description_slug' => 'limousine',
                'bol_active' => 1,
            ),
            212 => 
            array (
                'id_model' => 713,
                'id_manufacturer' => 96,
                'str_description' => 'majestic',
                'str_description_slug' => 'majestic',
                'bol_active' => 1,
            ),
            213 => 
            array (
                'id_model' => 714,
                'id_manufacturer' => 96,
                'str_description' => 'sp',
                'str_description_slug' => 'sp',
                'bol_active' => 1,
            ),
            214 => 
            array (
                'id_model' => 715,
                'id_manufacturer' => 96,
                'str_description' => 'special sports',
                'str_description_slug' => 'special-sports',
                'bol_active' => 1,
            ),
            215 => 
            array (
                'id_model' => 716,
                'id_manufacturer' => 97,
                'str_description' => '200',
                'str_description_slug' => '200',
                'bol_active' => 1,
            ),
            216 => 
            array (
                'id_model' => 717,
                'id_manufacturer' => 97,
                'str_description' => '210',
                'str_description_slug' => '210',
                'bol_active' => 1,
            ),
            217 => 
            array (
                'id_model' => 718,
                'id_manufacturer' => 97,
                'str_description' => '220',
                'str_description_slug' => '220',
                'bol_active' => 1,
            ),
            218 => 
            array (
                'id_model' => 719,
                'id_manufacturer' => 97,
                'str_description' => '240',
                'str_description_slug' => '240',
                'bol_active' => 1,
            ),
            219 => 
            array (
                'id_model' => 720,
                'id_manufacturer' => 97,
                'str_description' => '260',
                'str_description_slug' => '260',
                'bol_active' => 1,
            ),
            220 => 
            array (
                'id_model' => 721,
                'id_manufacturer' => 97,
                'str_description' => '280',
                'str_description_slug' => '280',
                'bol_active' => 1,
            ),
            221 => 
            array (
                'id_model' => 722,
                'id_manufacturer' => 97,
                'str_description' => '1500',
                'str_description_slug' => '1500',
                'bol_active' => 1,
            ),
            222 => 
            array (
                'id_model' => 723,
                'id_manufacturer' => 97,
                'str_description' => 'bluebird',
                'str_description_slug' => 'bluebird',
                'bol_active' => 1,
            ),
            223 => 
            array (
                'id_model' => 724,
                'id_manufacturer' => 97,
                'str_description' => 'cedric',
                'str_description_slug' => 'cedric',
                'bol_active' => 1,
            ),
            224 => 
            array (
                'id_model' => 725,
                'id_manufacturer' => 97,
                'str_description' => 'cherry',
                'str_description_slug' => 'cherry',
                'bol_active' => 1,
            ),
            225 => 
            array (
                'id_model' => 726,
                'id_manufacturer' => 97,
                'str_description' => 'patrol',
                'str_description_slug' => 'patrol',
                'bol_active' => 1,
            ),
            226 => 
            array (
                'id_model' => 727,
                'id_manufacturer' => 97,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            227 => 
            array (
                'id_model' => 728,
                'id_manufacturer' => 97,
                'str_description' => 'roadster',
                'str_description_slug' => 'roadster',
                'bol_active' => 1,
            ),
            228 => 
            array (
                'id_model' => 729,
                'id_manufacturer' => 97,
                'str_description' => 'stanza',
                'str_description_slug' => 'stanza',
                'bol_active' => 1,
            ),
            229 => 
            array (
                'id_model' => 730,
                'id_manufacturer' => 97,
                'str_description' => 'sunny',
                'str_description_slug' => 'sunny',
                'bol_active' => 1,
            ),
            230 => 
            array (
                'id_model' => 731,
                'id_manufacturer' => 97,
                'str_description' => 'urvan',
                'str_description_slug' => 'urvan',
                'bol_active' => 1,
            ),
            231 => 
            array (
                'id_model' => 732,
                'id_manufacturer' => 97,
                'str_description' => 'violet',
                'str_description_slug' => 'violet',
                'bol_active' => 1,
            ),
            232 => 
            array (
                'id_model' => 733,
                'id_manufacturer' => 98,
                'str_description' => 'rush',
                'str_description_slug' => 'rush',
                'bol_active' => 1,
            ),
            233 => 
            array (
                'id_model' => 734,
                'id_manufacturer' => 98,
                'str_description' => 'tojeiro',
                'str_description_slug' => 'tojeiro',
                'bol_active' => 1,
            ),
            234 => 
            array (
                'id_model' => 735,
                'id_manufacturer' => 99,
                'str_description' => 're',
                'str_description_slug' => 're',
                'bol_active' => 1,
            ),
            235 => 
            array (
                'id_model' => 736,
                'id_manufacturer' => 99,
                'str_description' => 'rider',
                'str_description_slug' => 'rider',
                'bol_active' => 1,
            ),
            236 => 
            array (
                'id_model' => 737,
                'id_manufacturer' => 99,
                'str_description' => 'rider max',
                'str_description_slug' => 'rider-max',
                'bol_active' => 1,
            ),
            237 => 
            array (
                'id_model' => 738,
                'id_manufacturer' => 100,
                'str_description' => 'guara',
                'str_description_slug' => 'guara',
                'bol_active' => 1,
            ),
            238 => 
            array (
                'id_model' => 739,
                'id_manufacturer' => 100,
                'str_description' => 'longchamp',
                'str_description_slug' => 'longchamp',
                'bol_active' => 1,
            ),
            239 => 
            array (
                'id_model' => 740,
                'id_manufacturer' => 100,
                'str_description' => 'mangusta',
                'str_description_slug' => 'mangusta',
                'bol_active' => 1,
            ),
            240 => 
            array (
                'id_model' => 741,
                'id_manufacturer' => 100,
                'str_description' => 'pantera',
                'str_description_slug' => 'pantera',
                'bol_active' => 1,
            ),
            241 => 
            array (
                'id_model' => 742,
                'id_manufacturer' => 100,
                'str_description' => 'vallelunga',
                'str_description_slug' => 'vallelunga',
                'bol_active' => 1,
            ),
            242 => 
            array (
                'id_model' => 743,
                'id_manufacturer' => 101,
                'str_description' => 'dr',
                'str_description_slug' => 'dr',
                'bol_active' => 1,
            ),
            243 => 
            array (
                'id_model' => 744,
                'id_manufacturer' => 102,
                'str_description' => '135',
                'str_description_slug' => '135',
                'bol_active' => 1,
            ),
            244 => 
            array (
                'id_model' => 745,
                'id_manufacturer' => 103,
                'str_description' => 'dmc',
                'str_description_slug' => 'dmc',
                'bol_active' => 1,
            ),
            245 => 
            array (
                'id_model' => 746,
                'id_manufacturer' => 104,
                'str_description' => 'custom',
                'str_description_slug' => 'custom',
                'bol_active' => 1,
            ),
            246 => 
            array (
                'id_model' => 747,
                'id_manufacturer' => 104,
                'str_description' => 'firedome',
                'str_description_slug' => 'firedome',
                'bol_active' => 1,
            ),
            247 => 
            array (
                'id_model' => 748,
                'id_manufacturer' => 104,
                'str_description' => 'roadster',
                'str_description_slug' => 'roadster',
                'bol_active' => 1,
            ),
            248 => 
            array (
                'id_model' => 749,
                'id_manufacturer' => 104,
                'str_description' => 'sedan',
                'str_description_slug' => 'sedan',
                'bol_active' => 1,
            ),
            249 => 
            array (
                'id_model' => 750,
                'id_manufacturer' => 105,
                'str_description' => '250',
                'str_description_slug' => '250',
                'bol_active' => 1,
            ),
            250 => 
            array (
                'id_model' => 751,
                'id_manufacturer' => 106,
                'str_description' => 'discoverer',
                'str_description_slug' => 'discoverer',
                'bol_active' => 1,
            ),
            251 => 
            array (
                'id_model' => 752,
                'id_manufacturer' => 107,
                'str_description' => 'auriga',
                'str_description_slug' => 'auriga',
                'bol_active' => 1,
            ),
            252 => 
            array (
                'id_model' => 753,
                'id_manufacturer' => 107,
                'str_description' => 'jolly',
                'str_description_slug' => 'jolly',
                'bol_active' => 1,
            ),
            253 => 
            array (
                'id_model' => 754,
                'id_manufacturer' => 107,
                'str_description' => 'lux',
                'str_description_slug' => 'lux',
                'bol_active' => 1,
            ),
            254 => 
            array (
                'id_model' => 755,
                'id_manufacturer' => 107,
                'str_description' => 'nilo',
                'str_description_slug' => 'nilo',
                'bol_active' => 1,
            ),
            255 => 
            array (
                'id_model' => 756,
                'id_manufacturer' => 107,
                'str_description' => 'pilos',
                'str_description_slug' => 'pilos',
                'bol_active' => 1,
            ),
            256 => 
            array (
                'id_model' => 757,
                'id_manufacturer' => 107,
                'str_description' => 'smile truck',
                'str_description_slug' => 'smile-truck',
                'bol_active' => 1,
            ),
            257 => 
            array (
                'id_model' => 758,
                'id_manufacturer' => 107,
                'str_description' => 'tarento',
                'str_description_slug' => 'tarento',
                'bol_active' => 1,
            ),
            258 => 
            array (
                'id_model' => 759,
                'id_manufacturer' => 108,
                'str_description' => '1000',
                'str_description_slug' => '1000',
                'bol_active' => 1,
            ),
            259 => 
            array (
                'id_model' => 760,
                'id_manufacturer' => 108,
                'str_description' => 'f',
                'str_description_slug' => 'f',
                'bol_active' => 1,
            ),
            260 => 
            array (
                'id_model' => 761,
                'id_manufacturer' => 108,
                'str_description' => 'f12',
                'str_description_slug' => 'f12',
                'bol_active' => 1,
            ),
            261 => 
            array (
                'id_model' => 762,
                'id_manufacturer' => 108,
                'str_description' => 'f7',
                'str_description_slug' => 'f7',
                'bol_active' => 1,
            ),
            262 => 
            array (
                'id_model' => 763,
                'id_manufacturer' => 108,
                'str_description' => 'munga',
                'str_description_slug' => 'munga',
                'bol_active' => 1,
            ),
            263 => 
            array (
                'id_model' => 764,
                'id_manufacturer' => 109,
                'str_description' => '126',
                'str_description_slug' => '126',
                'bol_active' => 1,
            ),
            264 => 
            array (
                'id_model' => 765,
                'id_manufacturer' => 109,
                'str_description' => '600',
                'str_description_slug' => '600',
                'bol_active' => 1,
            ),
            265 => 
            array (
                'id_model' => 766,
                'id_manufacturer' => 109,
                'str_description' => 'aries',
                'str_description_slug' => 'aries',
                'bol_active' => 1,
            ),
            266 => 
            array (
                'id_model' => 767,
                'id_manufacturer' => 109,
                'str_description' => 'aspen',
                'str_description_slug' => 'aspen',
                'bol_active' => 1,
            ),
            267 => 
            array (
                'id_model' => 768,
                'id_manufacturer' => 109,
                'str_description' => 'avenger',
                'str_description_slug' => 'avenger',
                'bol_active' => 1,
            ),
            268 => 
            array (
                'id_model' => 769,
                'id_manufacturer' => 109,
                'str_description' => 'b 350',
                'str_description_slug' => 'b-350',
                'bol_active' => 1,
            ),
            269 => 
            array (
                'id_model' => 770,
                'id_manufacturer' => 109,
                'str_description' => 'brothers',
                'str_description_slug' => 'brothers',
                'bol_active' => 1,
            ),
            270 => 
            array (
                'id_model' => 771,
                'id_manufacturer' => 109,
                'str_description' => 'caliber',
                'str_description_slug' => 'caliber',
                'bol_active' => 1,
            ),
            271 => 
            array (
                'id_model' => 772,
                'id_manufacturer' => 109,
                'str_description' => 'caravan',
                'str_description_slug' => 'caravan',
                'bol_active' => 1,
            ),
            272 => 
            array (
                'id_model' => 773,
                'id_manufacturer' => 109,
                'str_description' => 'challenger',
                'str_description_slug' => 'challenger',
                'bol_active' => 1,
            ),
            273 => 
            array (
                'id_model' => 774,
                'id_manufacturer' => 109,
                'str_description' => 'charger',
                'str_description_slug' => 'charger',
                'bol_active' => 1,
            ),
            274 => 
            array (
                'id_model' => 775,
                'id_manufacturer' => 109,
                'str_description' => 'coronet',
                'str_description_slug' => 'coronet',
                'bol_active' => 1,
            ),
            275 => 
            array (
                'id_model' => 776,
                'id_manufacturer' => 109,
                'str_description' => 'custom',
                'str_description_slug' => 'custom',
                'bol_active' => 1,
            ),
            276 => 
            array (
                'id_model' => 777,
                'id_manufacturer' => 109,
                'str_description' => 'd',
                'str_description_slug' => 'd',
                'bol_active' => 1,
            ),
            277 => 
            array (
                'id_model' => 778,
                'id_manufacturer' => 109,
                'str_description' => 'dakota',
                'str_description_slug' => 'dakota',
                'bol_active' => 1,
            ),
            278 => 
            array (
                'id_model' => 779,
                'id_manufacturer' => 109,
                'str_description' => 'dart',
                'str_description_slug' => 'dart',
                'bol_active' => 1,
            ),
            279 => 
            array (
                'id_model' => 780,
                'id_manufacturer' => 109,
                'str_description' => 'daytona',
                'str_description_slug' => 'daytona',
                'bol_active' => 1,
            ),
            280 => 
            array (
                'id_model' => 781,
                'id_manufacturer' => 109,
                'str_description' => 'durango',
                'str_description_slug' => 'durango',
                'bol_active' => 1,
            ),
            281 => 
            array (
                'id_model' => 782,
                'id_manufacturer' => 109,
                'str_description' => 'grand caravan',
                'str_description_slug' => 'grand-caravan',
                'bol_active' => 1,
            ),
            282 => 
            array (
                'id_model' => 783,
                'id_manufacturer' => 109,
                'str_description' => 'gt',
                'str_description_slug' => 'gt',
                'bol_active' => 1,
            ),
            283 => 
            array (
                'id_model' => 784,
                'id_manufacturer' => 109,
                'str_description' => 'intrepid',
                'str_description_slug' => 'intrepid',
                'bol_active' => 1,
            ),
            284 => 
            array (
                'id_model' => 785,
                'id_manufacturer' => 109,
                'str_description' => 'journey',
                'str_description_slug' => 'journey',
                'bol_active' => 1,
            ),
            285 => 
            array (
                'id_model' => 786,
                'id_manufacturer' => 109,
                'str_description' => 'k 31',
                'str_description_slug' => 'k-31',
                'bol_active' => 1,
            ),
            286 => 
            array (
                'id_model' => 787,
                'id_manufacturer' => 109,
                'str_description' => 'kingsway',
                'str_description_slug' => 'kingsway',
                'bol_active' => 1,
            ),
            287 => 
            array (
                'id_model' => 788,
                'id_manufacturer' => 109,
                'str_description' => 'm',
                'str_description_slug' => 'm',
                'bol_active' => 1,
            ),
            288 => 
            array (
                'id_model' => 789,
                'id_manufacturer' => 109,
                'str_description' => 'magnum',
                'str_description_slug' => 'magnum',
                'bol_active' => 1,
            ),
            289 => 
            array (
                'id_model' => 790,
                'id_manufacturer' => 109,
                'str_description' => 'max',
                'str_description_slug' => 'max',
                'bol_active' => 1,
            ),
            290 => 
            array (
                'id_model' => 791,
                'id_manufacturer' => 109,
                'str_description' => 'mini ram van',
                'str_description_slug' => 'mini-ram-van',
                'bol_active' => 1,
            ),
            291 => 
            array (
                'id_model' => 792,
                'id_manufacturer' => 109,
                'str_description' => 'model 30',
                'str_description_slug' => 'model-30',
                'bol_active' => 1,
            ),
            292 => 
            array (
                'id_model' => 793,
                'id_manufacturer' => 109,
                'str_description' => 'monaco',
                'str_description_slug' => 'monaco',
                'bol_active' => 1,
            ),
            293 => 
            array (
                'id_model' => 794,
                'id_manufacturer' => 109,
                'str_description' => 'neon',
                'str_description_slug' => 'neon',
                'bol_active' => 1,
            ),
            294 => 
            array (
                'id_model' => 795,
                'id_manufacturer' => 109,
                'str_description' => 'nitro',
                'str_description_slug' => 'nitro',
                'bol_active' => 1,
            ),
            295 => 
            array (
                'id_model' => 796,
                'id_manufacturer' => 109,
                'str_description' => 'omni',
                'str_description_slug' => 'omni',
                'bol_active' => 1,
            ),
            296 => 
            array (
                'id_model' => 797,
                'id_manufacturer' => 109,
                'str_description' => 'polara',
                'str_description_slug' => 'polara',
                'bol_active' => 1,
            ),
            297 => 
            array (
                'id_model' => 798,
                'id_manufacturer' => 109,
                'str_description' => 'ram',
                'str_description_slug' => 'ram',
                'bol_active' => 1,
            ),
            298 => 
            array (
                'id_model' => 799,
                'id_manufacturer' => 109,
                'str_description' => 'ram 1500',
                'str_description_slug' => 'ram-1500',
                'bol_active' => 1,
            ),
            299 => 
            array (
                'id_model' => 800,
                'id_manufacturer' => 109,
                'str_description' => 'ram 2500',
                'str_description_slug' => 'ram-2500',
                'bol_active' => 1,
            ),
            300 => 
            array (
                'id_model' => 801,
                'id_manufacturer' => 109,
                'str_description' => 'ram 3500',
                'str_description_slug' => 'ram-3500',
                'bol_active' => 1,
            ),
            301 => 
            array (
                'id_model' => 802,
                'id_manufacturer' => 109,
                'str_description' => 'ram van',
                'str_description_slug' => 'ram-van',
                'bol_active' => 1,
            ),
            302 => 
            array (
                'id_model' => 803,
                'id_manufacturer' => 109,
                'str_description' => 'ranger',
                'str_description_slug' => 'ranger',
                'bol_active' => 1,
            ),
            303 => 
            array (
                'id_model' => 804,
                'id_manufacturer' => 109,
                'str_description' => 'shadow',
                'str_description_slug' => 'shadow',
                'bol_active' => 1,
            ),
            304 => 
            array (
                'id_model' => 805,
                'id_manufacturer' => 109,
                'str_description' => 'stealth',
                'str_description_slug' => 'stealth',
                'bol_active' => 1,
            ),
            305 => 
            array (
                'id_model' => 806,
                'id_manufacturer' => 109,
                'str_description' => 'stratus',
                'str_description_slug' => 'stratus',
                'bol_active' => 1,
            ),
            306 => 
            array (
                'id_model' => 807,
                'id_manufacturer' => 109,
                'str_description' => 'valiant',
                'str_description_slug' => 'valiant',
                'bol_active' => 1,
            ),
            307 => 
            array (
                'id_model' => 808,
                'id_manufacturer' => 109,
                'str_description' => 'viper',
                'str_description_slug' => 'viper',
                'bol_active' => 1,
            ),
            308 => 
            array (
                'id_model' => 809,
                'id_manufacturer' => 109,
                'str_description' => 'w 150',
                'str_description_slug' => 'w-150',
                'bol_active' => 1,
            ),
            309 => 
            array (
                'id_model' => 810,
                'id_manufacturer' => 109,
                'str_description' => 'w 200',
                'str_description_slug' => 'w-200',
                'bol_active' => 1,
            ),
            310 => 
            array (
                'id_model' => 811,
                'id_manufacturer' => 109,
                'str_description' => 'wc 51',
                'str_description_slug' => 'wc-51',
                'bol_active' => 1,
            ),
            311 => 
            array (
                'id_model' => 812,
                'id_manufacturer' => 109,
                'str_description' => 'wc 57',
                'str_description_slug' => 'wc-57',
                'bol_active' => 1,
            ),
            312 => 
            array (
                'id_model' => 813,
                'id_manufacturer' => 110,
                'str_description' => 'beepo pony',
                'str_description_slug' => 'beepo-pony',
                'bol_active' => 1,
            ),
            313 => 
            array (
                'id_model' => 814,
                'id_manufacturer' => 110,
                'str_description' => 'go kart',
                'str_description_slug' => 'go-kart',
                'bol_active' => 1,
            ),
            314 => 
            array (
                'id_model' => 815,
                'id_manufacturer' => 110,
                'str_description' => 'k01',
                'str_description_slug' => 'k01',
                'bol_active' => 1,
            ),
            315 => 
            array (
                'id_model' => 816,
                'id_manufacturer' => 110,
                'str_description' => 'k02',
                'str_description_slug' => 'k02',
                'bol_active' => 1,
            ),
            316 => 
            array (
                'id_model' => 817,
                'id_manufacturer' => 110,
                'str_description' => 'k05',
                'str_description_slug' => 'k05',
                'bol_active' => 1,
            ),
            317 => 
            array (
                'id_model' => 818,
                'id_manufacturer' => 110,
                'str_description' => 'k07',
                'str_description_slug' => 'k07',
                'bol_active' => 1,
            ),
            318 => 
            array (
                'id_model' => 819,
                'id_manufacturer' => 111,
                'str_description' => 'super eight',
                'str_description_slug' => 'super-eight',
                'bol_active' => 1,
            ),
            319 => 
            array (
                'id_model' => 820,
                'id_manufacturer' => 112,
                'str_description' => 'diablo',
                'str_description_slug' => 'diablo',
                'bol_active' => 1,
            ),
            320 => 
            array (
                'id_model' => 821,
                'id_manufacturer' => 112,
                'str_description' => 'oxokart',
                'str_description_slug' => 'oxokart',
                'bol_active' => 1,
            ),
            321 => 
            array (
                'id_model' => 822,
                'id_manufacturer' => 112,
                'str_description' => 'saffary',
                'str_description_slug' => 'saffary',
                'bol_active' => 1,
            ),
            322 => 
            array (
                'id_model' => 823,
                'id_manufacturer' => 112,
                'str_description' => 'sahara',
                'str_description_slug' => 'sahara',
                'bol_active' => 1,
            ),
            323 => 
            array (
                'id_model' => 824,
                'id_manufacturer' => 113,
                'str_description' => 'first',
                'str_description_slug' => 'first',
                'bol_active' => 1,
            ),
            324 => 
            array (
                'id_model' => 825,
                'id_manufacturer' => 114,
                'str_description' => 'k',
                'str_description_slug' => 'k',
                'bol_active' => 1,
            ),
            325 => 
            array (
                'id_model' => 826,
                'id_manufacturer' => 115,
                'str_description' => 'gtv',
                'str_description_slug' => 'gtv',
                'bol_active' => 1,
            ),
            326 => 
            array (
                'id_model' => 827,
                'id_manufacturer' => 116,
                'str_description' => 'farmer',
                'str_description_slug' => 'farmer',
                'bol_active' => 1,
            ),
            327 => 
            array (
                'id_model' => 828,
                'id_manufacturer' => 116,
                'str_description' => 'ufo',
                'str_description_slug' => 'ufo',
                'bol_active' => 1,
            ),
            328 => 
            array (
                'id_model' => 829,
                'id_manufacturer' => 117,
                'str_description' => 'freedom',
                'str_description_slug' => 'freedom',
                'bol_active' => 1,
            ),
            329 => 
            array (
                'id_model' => 830,
                'id_manufacturer' => 117,
                'str_description' => 'gxt',
                'str_description_slug' => 'gxt',
                'bol_active' => 1,
            ),
            330 => 
            array (
                'id_model' => 831,
                'id_manufacturer' => 117,
                'str_description' => 'mpt',
                'str_description_slug' => 'mpt',
                'bol_active' => 1,
            ),
            331 => 
            array (
                'id_model' => 832,
                'id_manufacturer' => 117,
                'str_description' => 'pc',
                'str_description_slug' => 'pc',
                'bol_active' => 1,
            ),
            332 => 
            array (
                'id_model' => 833,
                'id_manufacturer' => 117,
                'str_description' => 'rxv',
                'str_description_slug' => 'rxv',
                'bol_active' => 1,
            ),
            333 => 
            array (
                'id_model' => 834,
                'id_manufacturer' => 117,
                'str_description' => 'shuttle',
                'str_description_slug' => 'shuttle',
                'bol_active' => 1,
            ),
            334 => 
            array (
                'id_model' => 835,
                'id_manufacturer' => 117,
                'str_description' => 'txt',
                'str_description_slug' => 'txt',
                'bol_active' => 1,
            ),
            335 => 
            array (
                'id_model' => 836,
                'id_manufacturer' => 118,
                'str_description' => 'premier',
                'str_description_slug' => 'premier',
                'bol_active' => 1,
            ),
            336 => 
            array (
                'id_model' => 837,
                'id_manufacturer' => 118,
                'str_description' => 'ss coupe',
                'str_description_slug' => 'ss-coupe',
                'bol_active' => 1,
            ),
            337 => 
            array (
                'id_model' => 838,
                'id_manufacturer' => 118,
                'str_description' => 'summit dl',
                'str_description_slug' => 'summit-dl',
                'bol_active' => 1,
            ),
            338 => 
            array (
                'id_model' => 839,
                'id_manufacturer' => 118,
                'str_description' => 'talon',
                'str_description_slug' => 'talon',
                'bol_active' => 1,
            ),
            339 => 
            array (
                'id_model' => 840,
                'id_manufacturer' => 118,
                'str_description' => 'vision',
                'str_description_slug' => 'vision',
                'bol_active' => 1,
            ),
            340 => 
            array (
                'id_model' => 841,
                'id_manufacturer' => 118,
                'str_description' => 'wheel drive',
                'str_description_slug' => 'wheel-drive',
                'bol_active' => 1,
            ),
            341 => 
            array (
                'id_model' => 842,
                'id_manufacturer' => 119,
                'str_description' => 'b 150',
                'str_description_slug' => 'b-150',
                'bol_active' => 1,
            ),
            342 => 
            array (
                'id_model' => 843,
                'id_manufacturer' => 119,
                'str_description' => 'c 150',
                'str_description_slug' => 'c-150',
                'bol_active' => 1,
            ),
            343 => 
            array (
                'id_model' => 844,
                'id_manufacturer' => 119,
                'str_description' => 'd 150',
                'str_description_slug' => 'd-150',
                'bol_active' => 1,
            ),
            344 => 
            array (
                'id_model' => 845,
                'id_manufacturer' => 119,
                'str_description' => 'd 151',
                'str_description_slug' => 'd-151',
                'bol_active' => 1,
            ),
            345 => 
            array (
                'id_model' => 846,
                'id_manufacturer' => 119,
                'str_description' => 'd 153',
                'str_description_slug' => 'd-153',
                'bol_active' => 1,
            ),
            346 => 
            array (
                'id_model' => 847,
                'id_manufacturer' => 119,
                'str_description' => 'd 154',
                'str_description_slug' => 'd-154',
                'bol_active' => 1,
            ),
            347 => 
            array (
                'id_model' => 848,
                'id_manufacturer' => 119,
                'str_description' => 'e 35',
                'str_description_slug' => 'e-35',
                'bol_active' => 1,
            ),
            348 => 
            array (
                'id_model' => 849,
                'id_manufacturer' => 119,
                'str_description' => 'f 100',
                'str_description_slug' => 'f-100',
                'bol_active' => 1,
            ),
            349 => 
            array (
                'id_model' => 850,
                'id_manufacturer' => 119,
                'str_description' => 'f 108',
                'str_description_slug' => 'f-108',
                'bol_active' => 1,
            ),
            350 => 
            array (
                'id_model' => 851,
                'id_manufacturer' => 119,
                'str_description' => 'f 260',
                'str_description_slug' => 'f-260',
                'bol_active' => 1,
            ),
            351 => 
            array (
                'id_model' => 852,
                'id_manufacturer' => 119,
                'str_description' => 'f 275',
                'str_description_slug' => 'f-275',
                'bol_active' => 1,
            ),
            352 => 
            array (
                'id_model' => 853,
                'id_manufacturer' => 119,
                'str_description' => 'f 350',
                'str_description_slug' => 'f-350',
                'bol_active' => 1,
            ),
            353 => 
            array (
                'id_model' => 854,
                'id_manufacturer' => 119,
                'str_description' => 'jeep',
                'str_description_slug' => 'jeep',
                'bol_active' => 1,
            ),
            354 => 
            array (
                'id_model' => 855,
                'id_manufacturer' => 119,
                'str_description' => 'l',
                'str_description_slug' => 'l',
                'bol_active' => 1,
            ),
            355 => 
            array (
                'id_model' => 856,
                'id_manufacturer' => 119,
                'str_description' => 'l 35',
                'str_description_slug' => 'l-35',
                'bol_active' => 1,
            ),
            356 => 
            array (
                'id_model' => 857,
                'id_manufacturer' => 119,
                'str_description' => 'siata',
                'str_description_slug' => 'siata',
                'bol_active' => 1,
            ),
            357 => 
            array (
                'id_model' => 858,
                'id_manufacturer' => 119,
                'str_description' => 'trade',
                'str_description_slug' => 'trade',
                'bol_active' => 1,
            ),
            358 => 
            array (
                'id_model' => 859,
                'id_manufacturer' => 120,
                'str_description' => 'pelican',
                'str_description_slug' => 'pelican',
                'bol_active' => 1,
            ),
            359 => 
            array (
                'id_model' => 860,
                'id_manufacturer' => 120,
                'str_description' => 'simply city',
                'str_description_slug' => 'simply-city',
                'bol_active' => 1,
            ),
            360 => 
            array (
                'id_model' => 861,
                'id_manufacturer' => 121,
                'str_description' => 'gasolone',
                'str_description_slug' => 'gasolone',
                'bol_active' => 1,
            ),
            361 => 
            array (
                'id_model' => 862,
                'id_manufacturer' => 121,
                'str_description' => 'maranello',
                'str_description_slug' => 'maranello',
                'bol_active' => 1,
            ),
            362 => 
            array (
                'id_model' => 863,
                'id_manufacturer' => 122,
                'str_description' => 'agora',
                'str_description_slug' => 'agora',
                'bol_active' => 1,
            ),
            363 => 
            array (
                'id_model' => 864,
                'id_manufacturer' => 122,
                'str_description' => 'ce',
                'str_description_slug' => 'ce',
                'bol_active' => 1,
            ),
            364 => 
            array (
                'id_model' => 865,
                'id_manufacturer' => 122,
                'str_description' => 'spacia',
                'str_description_slug' => 'spacia',
                'bol_active' => 1,
            ),
            365 => 
            array (
                'id_model' => 866,
                'id_manufacturer' => 123,
                'str_description' => 'super six',
                'str_description_slug' => 'super-six',
                'bol_active' => 1,
            ),
            366 => 
            array (
                'id_model' => 867,
                'id_manufacturer' => 124,
                'str_description' => 'biro',
                'str_description_slug' => 'biro',
                'bol_active' => 1,
            ),
            367 => 
            array (
                'id_model' => 868,
                'id_manufacturer' => 125,
                'str_description' => 'txa',
                'str_description_slug' => 'txa',
                'bol_active' => 1,
            ),
            368 => 
            array (
                'id_model' => 869,
                'id_manufacturer' => 126,
                'str_description' => 'te 1200a',
                'str_description_slug' => 'te-1200a',
                'bol_active' => 1,
            ),
            369 => 
            array (
                'id_model' => 870,
                'id_manufacturer' => 126,
                'str_description' => 'te 2000a',
                'str_description_slug' => 'te-2000a',
                'bol_active' => 1,
            ),
            370 => 
            array (
                'id_model' => 871,
                'id_manufacturer' => 126,
                'str_description' => 'te 600a',
                'str_description_slug' => 'te-600a',
                'bol_active' => 1,
            ),
            371 => 
            array (
                'id_model' => 872,
                'id_manufacturer' => 127,
                'str_description' => 'phaeton',
                'str_description_slug' => 'phaeton',
                'bol_active' => 1,
            ),
            372 => 
            array (
                'id_model' => 873,
                'id_manufacturer' => 127,
                'str_description' => 'v',
                'str_description_slug' => 'v',
                'bol_active' => 1,
            ),
            373 => 
            array (
                'id_model' => 874,
                'id_manufacturer' => 128,
                'str_description' => 'jolly',
                'str_description_slug' => 'jolly',
                'bol_active' => 1,
            ),
            374 => 
            array (
                'id_model' => 875,
                'id_manufacturer' => 128,
                'str_description' => 'smile',
                'str_description_slug' => 'smile',
                'bol_active' => 1,
            ),
            375 => 
            array (
                'id_model' => 876,
                'id_manufacturer' => 128,
                'str_description' => 'taxi',
                'str_description_slug' => 'taxi',
                'bol_active' => 1,
            ),
            376 => 
            array (
                'id_model' => 877,
                'id_manufacturer' => 128,
                'str_description' => 'truck',
                'str_description_slug' => 'truck',
                'bol_active' => 1,
            ),
            377 => 
            array (
                'id_model' => 878,
                'id_manufacturer' => 129,
                'str_description' => 'facellia',
                'str_description_slug' => 'facellia',
                'bol_active' => 1,
            ),
            378 => 
            array (
                'id_model' => 879,
                'id_manufacturer' => 130,
                'str_description' => '166',
                'str_description_slug' => '166',
                'bol_active' => 1,
            ),
            379 => 
            array (
                'id_model' => 880,
                'id_manufacturer' => 130,
                'str_description' => '208',
                'str_description_slug' => '208',
                'bol_active' => 1,
            ),
            380 => 
            array (
                'id_model' => 881,
                'id_manufacturer' => 130,
                'str_description' => '212',
                'str_description_slug' => '212',
                'bol_active' => 1,
            ),
            381 => 
            array (
                'id_model' => 882,
                'id_manufacturer' => 130,
                'str_description' => '246',
                'str_description_slug' => '246',
                'bol_active' => 1,
            ),
            382 => 
            array (
                'id_model' => 883,
                'id_manufacturer' => 130,
                'str_description' => '250',
                'str_description_slug' => '250',
                'bol_active' => 1,
            ),
            383 => 
            array (
                'id_model' => 884,
                'id_manufacturer' => 130,
                'str_description' => '275',
                'str_description_slug' => '275',
                'bol_active' => 1,
            ),
            384 => 
            array (
                'id_model' => 885,
                'id_manufacturer' => 130,
                'str_description' => '288',
                'str_description_slug' => '288',
                'bol_active' => 1,
            ),
            385 => 
            array (
                'id_model' => 886,
                'id_manufacturer' => 130,
                'str_description' => '308',
                'str_description_slug' => '308',
                'bol_active' => 1,
            ),
            386 => 
            array (
                'id_model' => 887,
                'id_manufacturer' => 130,
                'str_description' => '328',
                'str_description_slug' => '328',
                'bol_active' => 1,
            ),
            387 => 
            array (
                'id_model' => 888,
                'id_manufacturer' => 130,
                'str_description' => '330',
                'str_description_slug' => '330',
                'bol_active' => 1,
            ),
            388 => 
            array (
                'id_model' => 889,
                'id_manufacturer' => 130,
                'str_description' => '340',
                'str_description_slug' => '340',
                'bol_active' => 1,
            ),
            389 => 
            array (
                'id_model' => 890,
                'id_manufacturer' => 130,
                'str_description' => '348',
                'str_description_slug' => '348',
                'bol_active' => 1,
            ),
            390 => 
            array (
                'id_model' => 891,
                'id_manufacturer' => 130,
                'str_description' => '355 f1',
                'str_description_slug' => '355-f1',
                'bol_active' => 1,
            ),
            391 => 
            array (
                'id_model' => 892,
                'id_manufacturer' => 130,
                'str_description' => '360',
                'str_description_slug' => '360',
                'bol_active' => 1,
            ),
            392 => 
            array (
                'id_model' => 893,
                'id_manufacturer' => 130,
                'str_description' => '365',
                'str_description_slug' => '365',
                'bol_active' => 1,
            ),
            393 => 
            array (
                'id_model' => 894,
                'id_manufacturer' => 130,
                'str_description' => '375',
                'str_description_slug' => '375',
                'bol_active' => 1,
            ),
            394 => 
            array (
                'id_model' => 895,
                'id_manufacturer' => 130,
                'str_description' => '400',
                'str_description_slug' => '400',
                'bol_active' => 1,
            ),
            395 => 
            array (
                'id_model' => 896,
                'id_manufacturer' => 130,
                'str_description' => '412',
                'str_description_slug' => '412',
                'bol_active' => 1,
            ),
            396 => 
            array (
                'id_model' => 897,
                'id_manufacturer' => 130,
                'str_description' => '456',
                'str_description_slug' => '456',
                'bol_active' => 1,
            ),
            397 => 
            array (
                'id_model' => 898,
                'id_manufacturer' => 130,
                'str_description' => '458',
                'str_description_slug' => '458',
                'bol_active' => 1,
            ),
            398 => 
            array (
                'id_model' => 899,
                'id_manufacturer' => 130,
                'str_description' => '488',
                'str_description_slug' => '488',
                'bol_active' => 1,
            ),
            399 => 
            array (
                'id_model' => 900,
                'id_manufacturer' => 130,
                'str_description' => '500',
                'str_description_slug' => '500',
                'bol_active' => 1,
            ),
            400 => 
            array (
                'id_model' => 901,
                'id_manufacturer' => 130,
                'str_description' => '512',
                'str_description_slug' => '512',
                'bol_active' => 1,
            ),
            401 => 
            array (
                'id_model' => 902,
                'id_manufacturer' => 130,
                'str_description' => '550',
                'str_description_slug' => '550',
                'bol_active' => 1,
            ),
            402 => 
            array (
                'id_model' => 903,
                'id_manufacturer' => 130,
                'str_description' => '575',
                'str_description_slug' => '575',
                'bol_active' => 1,
            ),
            403 => 
            array (
                'id_model' => 904,
                'id_manufacturer' => 130,
                'str_description' => '599',
                'str_description_slug' => '599',
                'bol_active' => 1,
            ),
            404 => 
            array (
                'id_model' => 905,
                'id_manufacturer' => 130,
                'str_description' => '612',
                'str_description_slug' => '612',
                'bol_active' => 1,
            ),
            405 => 
            array (
                'id_model' => 906,
                'id_manufacturer' => 130,
                'str_description' => 'california',
                'str_description_slug' => 'california',
                'bol_active' => 1,
            ),
            406 => 
            array (
                'id_model' => 907,
                'id_manufacturer' => 130,
                'str_description' => 'enzo',
                'str_description_slug' => 'enzo',
                'bol_active' => 1,
            ),
            407 => 
            array (
                'id_model' => 908,
                'id_manufacturer' => 130,
                'str_description' => 'f-355',
                'str_description_slug' => 'f-355',
                'bol_active' => 1,
            ),
            408 => 
            array (
                'id_model' => 909,
                'id_manufacturer' => 130,
                'str_description' => 'f-40',
                'str_description_slug' => 'f-40',
                'bol_active' => 1,
            ),
            409 => 
            array (
                'id_model' => 910,
                'id_manufacturer' => 130,
                'str_description' => 'f-430',
                'str_description_slug' => 'f-430',
                'bol_active' => 1,
            ),
            410 => 
            array (
                'id_model' => 911,
                'id_manufacturer' => 130,
                'str_description' => 'f-50',
                'str_description_slug' => 'f-50',
                'bol_active' => 1,
            ),
            411 => 
            array (
                'id_model' => 912,
                'id_manufacturer' => 130,
                'str_description' => 'f-512',
                'str_description_slug' => 'f-512',
                'bol_active' => 1,
            ),
            412 => 
            array (
                'id_model' => 913,
                'id_manufacturer' => 130,
                'str_description' => 'f12',
                'str_description_slug' => 'f12',
                'bol_active' => 1,
            ),
            413 => 
            array (
                'id_model' => 914,
                'id_manufacturer' => 130,
                'str_description' => 'ff',
                'str_description_slug' => 'ff',
                'bol_active' => 1,
            ),
            414 => 
            array (
                'id_model' => 915,
                'id_manufacturer' => 130,
                'str_description' => 'gtc4lusso',
                'str_description_slug' => 'gtc4lusso',
                'bol_active' => 1,
            ),
            415 => 
            array (
                'id_model' => 916,
                'id_manufacturer' => 130,
                'str_description' => 'laferrari',
                'str_description_slug' => 'laferrari',
                'bol_active' => 1,
            ),
            416 => 
            array (
                'id_model' => 917,
                'id_manufacturer' => 130,
                'str_description' => 'mondial',
                'str_description_slug' => 'mondial',
                'bol_active' => 1,
            ),
            417 => 
            array (
                'id_model' => 918,
                'id_manufacturer' => 130,
                'str_description' => 'mondial t',
                'str_description_slug' => 'mondial-t',
                'bol_active' => 1,
            ),
            418 => 
            array (
                'id_model' => 919,
                'id_manufacturer' => 130,
                'str_description' => 'superamerica',
                'str_description_slug' => 'superamerica',
                'bol_active' => 1,
            ),
            419 => 
            array (
                'id_model' => 920,
                'id_manufacturer' => 130,
                'str_description' => 'testarossa',
                'str_description_slug' => 'testarossa',
                'bol_active' => 1,
            ),
            420 => 
            array (
                'id_model' => 921,
                'id_manufacturer' => 131,
                'str_description' => '60',
                'str_description_slug' => '60',
                'bol_active' => 1,
            ),
            421 => 
            array (
                'id_model' => 922,
                'id_manufacturer' => 131,
                'str_description' => '65',
                'str_description_slug' => '65',
                'bol_active' => 1,
            ),
            422 => 
            array (
                'id_model' => 923,
                'id_manufacturer' => 131,
                'str_description' => '124',
                'str_description_slug' => '124',
                'bol_active' => 1,
            ),
            423 => 
            array (
                'id_model' => 924,
                'id_manufacturer' => 131,
                'str_description' => '124 spider',
                'str_description_slug' => '124-spider',
                'bol_active' => 1,
            ),
            424 => 
            array (
                'id_model' => 925,
                'id_manufacturer' => 131,
                'str_description' => '125',
                'str_description_slug' => '125',
                'bol_active' => 1,
            ),
            425 => 
            array (
                'id_model' => 926,
                'id_manufacturer' => 131,
                'str_description' => '126',
                'str_description_slug' => '126',
                'bol_active' => 1,
            ),
            426 => 
            array (
                'id_model' => 927,
                'id_manufacturer' => 131,
                'str_description' => '127',
                'str_description_slug' => '127',
                'bol_active' => 1,
            ),
            427 => 
            array (
                'id_model' => 928,
                'id_manufacturer' => 131,
                'str_description' => '128',
                'str_description_slug' => '128',
                'bol_active' => 1,
            ),
            428 => 
            array (
                'id_model' => 929,
                'id_manufacturer' => 131,
                'str_description' => '130',
                'str_description_slug' => '130',
                'bol_active' => 1,
            ),
            429 => 
            array (
                'id_model' => 930,
                'id_manufacturer' => 131,
                'str_description' => '131',
                'str_description_slug' => '131',
                'bol_active' => 1,
            ),
            430 => 
            array (
                'id_model' => 931,
                'id_manufacturer' => 131,
                'str_description' => '132',
                'str_description_slug' => '132',
                'bol_active' => 1,
            ),
            431 => 
            array (
                'id_model' => 932,
                'id_manufacturer' => 131,
                'str_description' => '238',
                'str_description_slug' => '238',
                'bol_active' => 1,
            ),
            432 => 
            array (
                'id_model' => 933,
                'id_manufacturer' => 131,
                'str_description' => '242',
                'str_description_slug' => '242',
                'bol_active' => 1,
            ),
            433 => 
            array (
                'id_model' => 934,
                'id_manufacturer' => 131,
                'str_description' => '500c',
                'str_description_slug' => '500c',
                'bol_active' => 1,
            ),
            434 => 
            array (
                'id_model' => 935,
                'id_manufacturer' => 131,
                'str_description' => '500l',
                'str_description_slug' => '500l',
                'bol_active' => 1,
            ),
            435 => 
            array (
                'id_model' => 936,
                'id_manufacturer' => 131,
                'str_description' => '500l living',
                'str_description_slug' => '500l-living',
                'bol_active' => 1,
            ),
            436 => 
            array (
                'id_model' => 937,
                'id_manufacturer' => 131,
                'str_description' => '500x',
                'str_description_slug' => '500x',
                'bol_active' => 1,
            ),
            437 => 
            array (
                'id_model' => 938,
                'id_manufacturer' => 131,
                'str_description' => '501',
                'str_description_slug' => '501',
                'bol_active' => 1,
            ),
            438 => 
            array (
                'id_model' => 939,
                'id_manufacturer' => 131,
                'str_description' => '503',
                'str_description_slug' => '503',
                'bol_active' => 1,
            ),
            439 => 
            array (
                'id_model' => 940,
                'id_manufacturer' => 131,
                'str_description' => '508 balilla',
                'str_description_slug' => '508-balilla',
                'bol_active' => 1,
            ),
            440 => 
            array (
                'id_model' => 941,
                'id_manufacturer' => 131,
                'str_description' => '509',
                'str_description_slug' => '509',
                'bol_active' => 1,
            ),
            441 => 
            array (
                'id_model' => 942,
                'id_manufacturer' => 131,
                'str_description' => '514',
                'str_description_slug' => '514',
                'bol_active' => 1,
            ),
            442 => 
            array (
                'id_model' => 943,
                'id_manufacturer' => 131,
                'str_description' => '600',
                'str_description_slug' => '600',
                'bol_active' => 1,
            ),
            443 => 
            array (
                'id_model' => 944,
                'id_manufacturer' => 131,
                'str_description' => '616',
                'str_description_slug' => '616',
                'bol_active' => 1,
            ),
            444 => 
            array (
                'id_model' => 945,
                'id_manufacturer' => 131,
                'str_description' => '618',
                'str_description_slug' => '618',
                'bol_active' => 1,
            ),
            445 => 
            array (
                'id_model' => 946,
                'id_manufacturer' => 131,
                'str_description' => '625',
                'str_description_slug' => '625',
                'bol_active' => 1,
            ),
            446 => 
            array (
                'id_model' => 947,
                'id_manufacturer' => 131,
                'str_description' => '850',
                'str_description_slug' => '850',
                'bol_active' => 1,
            ),
            447 => 
            array (
                'id_model' => 948,
                'id_manufacturer' => 131,
                'str_description' => '900 t',
                'str_description_slug' => '900-t',
                'bol_active' => 1,
            ),
            448 => 
            array (
                'id_model' => 949,
                'id_manufacturer' => 131,
                'str_description' => '1100',
                'str_description_slug' => '1100',
                'bol_active' => 1,
            ),
            449 => 
            array (
                'id_model' => 950,
                'id_manufacturer' => 131,
                'str_description' => '1200',
                'str_description_slug' => '1200',
                'bol_active' => 1,
            ),
            450 => 
            array (
                'id_model' => 951,
                'id_manufacturer' => 131,
                'str_description' => '1300',
                'str_description_slug' => '1300',
                'bol_active' => 1,
            ),
            451 => 
            array (
                'id_model' => 952,
                'id_manufacturer' => 131,
                'str_description' => '1400',
                'str_description_slug' => '1400',
                'bol_active' => 1,
            ),
            452 => 
            array (
                'id_model' => 953,
                'id_manufacturer' => 131,
                'str_description' => '1500',
                'str_description_slug' => '1500',
                'bol_active' => 1,
            ),
            453 => 
            array (
                'id_model' => 954,
                'id_manufacturer' => 131,
                'str_description' => '1900',
                'str_description_slug' => '1900',
                'bol_active' => 1,
            ),
            454 => 
            array (
                'id_model' => 955,
                'id_manufacturer' => 131,
                'str_description' => '2300',
                'str_description_slug' => '2300',
                'bol_active' => 1,
            ),
            455 => 
            array (
                'id_model' => 956,
                'id_manufacturer' => 131,
                'str_description' => 'albea',
                'str_description_slug' => 'albea',
                'bol_active' => 1,
            ),
            456 => 
            array (
                'id_model' => 957,
                'id_manufacturer' => 131,
                'str_description' => 'argenta',
                'str_description_slug' => 'argenta',
                'bol_active' => 1,
            ),
            457 => 
            array (
                'id_model' => 958,
                'id_manufacturer' => 131,
                'str_description' => 'barchetta',
                'str_description_slug' => 'barchetta',
                'bol_active' => 1,
            ),
            458 => 
            array (
                'id_model' => 959,
                'id_manufacturer' => 131,
                'str_description' => 'campagnola',
                'str_description_slug' => 'campagnola',
                'bol_active' => 1,
            ),
            459 => 
            array (
                'id_model' => 960,
                'id_manufacturer' => 131,
                'str_description' => 'cinquecento',
                'str_description_slug' => 'cinquecento',
                'bol_active' => 1,
            ),
            460 => 
            array (
                'id_model' => 961,
                'id_manufacturer' => 131,
                'str_description' => 'coupe',
                'str_description_slug' => 'coupe',
                'bol_active' => 1,
            ),
            461 => 
            array (
                'id_model' => 962,
                'id_manufacturer' => 131,
                'str_description' => 'croma',
                'str_description_slug' => 'croma',
                'bol_active' => 1,
            ),
            462 => 
            array (
                'id_model' => 963,
                'id_manufacturer' => 131,
                'str_description' => 'd 25',
                'str_description_slug' => 'd-25',
                'bol_active' => 1,
            ),
            463 => 
            array (
                'id_model' => 964,
                'id_manufacturer' => 131,
                'str_description' => 'dino',
                'str_description_slug' => 'dino',
                'bol_active' => 1,
            ),
            464 => 
            array (
                'id_model' => 965,
                'id_manufacturer' => 131,
                'str_description' => 'doblo cargo',
                'str_description_slug' => 'doblo-cargo',
                'bol_active' => 1,
            ),
            465 => 
            array (
                'id_model' => 966,
                'id_manufacturer' => 131,
                'str_description' => 'duna',
                'str_description_slug' => 'duna',
                'bol_active' => 1,
            ),
            466 => 
            array (
                'id_model' => 967,
                'id_manufacturer' => 131,
                'str_description' => 'f 13',
                'str_description_slug' => 'f-13',
                'bol_active' => 1,
            ),
            467 => 
            array (
                'id_model' => 968,
                'id_manufacturer' => 131,
                'str_description' => 'f8',
                'str_description_slug' => 'f8',
                'bol_active' => 1,
            ),
            468 => 
            array (
                'id_model' => 969,
                'id_manufacturer' => 131,
                'str_description' => 'fiorino',
                'str_description_slug' => 'fiorino',
                'bol_active' => 1,
            ),
            469 => 
            array (
                'id_model' => 970,
                'id_manufacturer' => 131,
                'str_description' => 'freemont',
                'str_description_slug' => 'freemont',
                'bol_active' => 1,
            ),
            470 => 
            array (
                'id_model' => 971,
                'id_manufacturer' => 131,
                'str_description' => 'fullback',
                'str_description_slug' => 'fullback',
                'bol_active' => 1,
            ),
            471 => 
            array (
                'id_model' => 972,
                'id_manufacturer' => 131,
                'str_description' => 'idea',
                'str_description_slug' => 'idea',
                'bol_active' => 1,
            ),
            472 => 
            array (
                'id_model' => 973,
                'id_manufacturer' => 131,
                'str_description' => 'linea',
                'str_description_slug' => 'linea',
                'bol_active' => 1,
            ),
            473 => 
            array (
                'id_model' => 974,
                'id_manufacturer' => 131,
                'str_description' => 'marea',
                'str_description_slug' => 'marea',
                'bol_active' => 1,
            ),
            474 => 
            array (
                'id_model' => 975,
                'id_manufacturer' => 131,
                'str_description' => 'multipla',
                'str_description_slug' => 'multipla',
                'bol_active' => 1,
            ),
            475 => 
            array (
                'id_model' => 976,
                'id_manufacturer' => 131,
                'str_description' => 'palio',
                'str_description_slug' => 'palio',
                'bol_active' => 1,
            ),
            476 => 
            array (
                'id_model' => 977,
                'id_manufacturer' => 131,
                'str_description' => 'punto evo',
                'str_description_slug' => 'punto-evo',
                'bol_active' => 1,
            ),
            477 => 
            array (
                'id_model' => 978,
                'id_manufacturer' => 131,
                'str_description' => 'qubo',
                'str_description_slug' => 'qubo',
                'bol_active' => 1,
            ),
            478 => 
            array (
                'id_model' => 979,
                'id_manufacturer' => 131,
                'str_description' => 'regata',
                'str_description_slug' => 'regata',
                'bol_active' => 1,
            ),
            479 => 
            array (
                'id_model' => 980,
                'id_manufacturer' => 131,
                'str_description' => 'ritmo',
                'str_description_slug' => 'ritmo',
                'bol_active' => 1,
            ),
            480 => 
            array (
                'id_model' => 981,
                'id_manufacturer' => 131,
                'str_description' => 'sedici',
                'str_description_slug' => 'sedici',
                'bol_active' => 1,
            ),
            481 => 
            array (
                'id_model' => 982,
                'id_manufacturer' => 131,
                'str_description' => 'serie cx',
                'str_description_slug' => 'serie-cx',
                'bol_active' => 1,
            ),
            482 => 
            array (
                'id_model' => 983,
                'id_manufacturer' => 131,
                'str_description' => 'spider',
                'str_description_slug' => 'spider',
                'bol_active' => 1,
            ),
            483 => 
            array (
                'id_model' => 984,
                'id_manufacturer' => 131,
                'str_description' => 'strada',
                'str_description_slug' => 'strada',
                'bol_active' => 1,
            ),
            484 => 
            array (
                'id_model' => 985,
                'id_manufacturer' => 131,
                'str_description' => 'talento',
                'str_description_slug' => 'talento',
                'bol_active' => 1,
            ),
            485 => 
            array (
                'id_model' => 986,
                'id_manufacturer' => 131,
                'str_description' => 'tempra',
                'str_description_slug' => 'tempra',
                'bol_active' => 1,
            ),
            486 => 
            array (
                'id_model' => 987,
                'id_manufacturer' => 131,
                'str_description' => 'tipo',
                'str_description_slug' => 'tipo',
                'bol_active' => 1,
            ),
            487 => 
            array (
                'id_model' => 988,
                'id_manufacturer' => 131,
                'str_description' => 'topolino',
                'str_description_slug' => 'topolino',
                'bol_active' => 1,
            ),
            488 => 
            array (
                'id_model' => 989,
                'id_manufacturer' => 131,
                'str_description' => 'ulysse',
                'str_description_slug' => 'ulysse',
                'bol_active' => 1,
            ),
            489 => 
            array (
                'id_model' => 990,
                'id_manufacturer' => 131,
                'str_description' => 'uno',
                'str_description_slug' => 'uno',
                'bol_active' => 1,
            ),
            490 => 
            array (
                'id_model' => 991,
                'id_manufacturer' => 131,
                'str_description' => 'x 1/9',
                'str_description_slug' => 'x-1/9',
                'bol_active' => 1,
            ),
            491 => 
            array (
                'id_model' => 992,
                'id_manufacturer' => 132,
                'str_description' => '30.12',
                'str_description_slug' => '30.12',
                'bol_active' => 1,
            ),
            492 => 
            array (
                'id_model' => 993,
                'id_manufacturer' => 132,
                'str_description' => '30.8',
                'str_description_slug' => '30.8',
                'bol_active' => 1,
            ),
            493 => 
            array (
                'id_model' => 994,
                'id_manufacturer' => 132,
                'str_description' => '35.10',
                'str_description_slug' => '35.10',
                'bol_active' => 1,
            ),
            494 => 
            array (
                'id_model' => 995,
                'id_manufacturer' => 132,
                'str_description' => '35.12',
                'str_description_slug' => '35.12',
                'bol_active' => 1,
            ),
            495 => 
            array (
                'id_model' => 996,
                'id_manufacturer' => 132,
                'str_description' => '35.8',
                'str_description_slug' => '35.8',
                'bol_active' => 1,
            ),
            496 => 
            array (
                'id_model' => 997,
                'id_manufacturer' => 132,
                'str_description' => '40.35',
                'str_description_slug' => '40.35',
                'bol_active' => 1,
            ),
            497 => 
            array (
                'id_model' => 998,
                'id_manufacturer' => 132,
                'str_description' => '40.50',
                'str_description_slug' => '40.50',
                'bol_active' => 1,
            ),
            498 => 
            array (
                'id_model' => 999,
                'id_manufacturer' => 132,
                'str_description' => '40.8',
                'str_description_slug' => '40.8',
                'bol_active' => 1,
            ),
            499 => 
            array (
                'id_model' => 1000,
                'id_manufacturer' => 132,
                'str_description' => '49.10',
                'str_description_slug' => '49.10',
                'bol_active' => 1,
            ),
        ));
        \DB::table('automobile_models')->insert(array (
            0 => 
            array (
                'id_model' => 1001,
                'id_manufacturer' => 132,
                'str_description' => '60.11',
                'str_description_slug' => '60.11',
                'bol_active' => 1,
            ),
            1 => 
            array (
                'id_model' => 1002,
                'id_manufacturer' => 132,
                'str_description' => 'cx 40',
                'str_description_slug' => 'cx-40',
                'bol_active' => 1,
            ),
            2 => 
            array (
                'id_model' => 1003,
                'id_manufacturer' => 132,
                'str_description' => 'ecovip',
                'str_description_slug' => 'ecovip',
                'bol_active' => 1,
            ),
            3 => 
            array (
                'id_model' => 1004,
                'id_manufacturer' => 132,
                'str_description' => 'turbodaily',
                'str_description_slug' => 'turbodaily',
                'bol_active' => 1,
            ),
            4 => 
            array (
                'id_model' => 1005,
                'id_manufacturer' => 133,
                'str_description' => 'newark',
                'str_description_slug' => 'newark',
                'bol_active' => 1,
            ),
            5 => 
            array (
                'id_model' => 1006,
                'id_manufacturer' => 134,
                'str_description' => '17 m',
                'str_description_slug' => '17-m',
                'bol_active' => 1,
            ),
            6 => 
            array (
                'id_model' => 1007,
                'id_manufacturer' => 134,
                'str_description' => 'a',
                'str_description_slug' => 'a',
                'bol_active' => 1,
            ),
            7 => 
            array (
                'id_model' => 1008,
                'id_manufacturer' => 134,
                'str_description' => 'aa',
                'str_description_slug' => 'aa',
                'bol_active' => 1,
            ),
            8 => 
            array (
                'id_model' => 1009,
                'id_manufacturer' => 134,
                'str_description' => 'aerostar',
                'str_description_slug' => 'aerostar',
                'bol_active' => 1,
            ),
            9 => 
            array (
                'id_model' => 1010,
                'id_manufacturer' => 134,
                'str_description' => 'anglia',
                'str_description_slug' => 'anglia',
                'bol_active' => 1,
            ),
            10 => 
            array (
                'id_model' => 1011,
                'id_manufacturer' => 134,
                'str_description' => 'aspire',
                'str_description_slug' => 'aspire',
                'bol_active' => 1,
            ),
            11 => 
            array (
                'id_model' => 1012,
                'id_manufacturer' => 134,
                'str_description' => 'b',
                'str_description_slug' => 'b',
                'bol_active' => 1,
            ),
            12 => 
            array (
                'id_model' => 1013,
                'id_manufacturer' => 134,
                'str_description' => 'b-max',
                'str_description_slug' => 'b-max',
                'bol_active' => 1,
            ),
            13 => 
            array (
                'id_model' => 1014,
                'id_manufacturer' => 134,
                'str_description' => 'branton',
                'str_description_slug' => 'branton',
                'bol_active' => 1,
            ),
            14 => 
            array (
                'id_model' => 1015,
                'id_manufacturer' => 134,
                'str_description' => 'bronco',
                'str_description_slug' => 'bronco',
                'bol_active' => 1,
            ),
            15 => 
            array (
                'id_model' => 1016,
                'id_manufacturer' => 134,
                'str_description' => 'bronco ii',
                'str_description_slug' => 'bronco-ii',
                'bol_active' => 1,
            ),
            16 => 
            array (
                'id_model' => 1017,
                'id_manufacturer' => 134,
                'str_description' => 'c-max',
                'str_description_slug' => 'c-max',
                'bol_active' => 1,
            ),
            17 => 
            array (
                'id_model' => 1018,
                'id_manufacturer' => 134,
                'str_description' => 'capri',
                'str_description_slug' => 'capri',
                'bol_active' => 1,
            ),
            18 => 
            array (
                'id_model' => 1019,
                'id_manufacturer' => 134,
                'str_description' => 'cobra',
                'str_description_slug' => 'cobra',
                'bol_active' => 1,
            ),
            19 => 
            array (
                'id_model' => 1020,
                'id_manufacturer' => 134,
                'str_description' => 'connect',
                'str_description_slug' => 'connect',
                'bol_active' => 1,
            ),
            20 => 
            array (
                'id_model' => 1021,
                'id_manufacturer' => 134,
                'str_description' => 'conquistador',
                'str_description_slug' => 'conquistador',
                'bol_active' => 1,
            ),
            21 => 
            array (
                'id_model' => 1022,
                'id_manufacturer' => 134,
                'str_description' => 'consul',
                'str_description_slug' => 'consul',
                'bol_active' => 1,
            ),
            22 => 
            array (
                'id_model' => 1023,
                'id_manufacturer' => 134,
                'str_description' => 'contour',
                'str_description_slug' => 'contour',
                'bol_active' => 1,
            ),
            23 => 
            array (
                'id_model' => 1024,
                'id_manufacturer' => 134,
                'str_description' => 'corsair',
                'str_description_slug' => 'corsair',
                'bol_active' => 1,
            ),
            24 => 
            array (
                'id_model' => 1025,
                'id_manufacturer' => 134,
                'str_description' => 'cortina',
                'str_description_slug' => 'cortina',
                'bol_active' => 1,
            ),
            25 => 
            array (
                'id_model' => 1026,
                'id_manufacturer' => 134,
                'str_description' => 'cougar',
                'str_description_slug' => 'cougar',
                'bol_active' => 1,
            ),
            26 => 
            array (
                'id_model' => 1027,
                'id_manufacturer' => 134,
                'str_description' => 'coupe club',
                'str_description_slug' => 'coupe-club',
                'bol_active' => 1,
            ),
            27 => 
            array (
                'id_model' => 1028,
                'id_manufacturer' => 134,
                'str_description' => 'courier',
                'str_description_slug' => 'courier',
                'bol_active' => 1,
            ),
            28 => 
            array (
                'id_model' => 1029,
                'id_manufacturer' => 134,
                'str_description' => 'crestline',
                'str_description_slug' => 'crestline',
                'bol_active' => 1,
            ),
            29 => 
            array (
                'id_model' => 1030,
                'id_manufacturer' => 134,
                'str_description' => 'crown victoria',
                'str_description_slug' => 'crown-victoria',
                'bol_active' => 1,
            ),
            30 => 
            array (
                'id_model' => 1031,
                'id_manufacturer' => 134,
                'str_description' => 'custom',
                'str_description_slug' => 'custom',
                'bol_active' => 1,
            ),
            31 => 
            array (
                'id_model' => 1032,
                'id_manufacturer' => 134,
                'str_description' => 'custom tourneo',
                'str_description_slug' => 'custom-tourneo',
                'bol_active' => 1,
            ),
            32 => 
            array (
                'id_model' => 1033,
                'id_manufacturer' => 134,
                'str_description' => 'e 50',
                'str_description_slug' => 'e-50',
                'bol_active' => 1,
            ),
            33 => 
            array (
                'id_model' => 1034,
                'id_manufacturer' => 134,
                'str_description' => 'econoline',
                'str_description_slug' => 'econoline',
                'bol_active' => 1,
            ),
            34 => 
            array (
                'id_model' => 1035,
                'id_manufacturer' => 134,
                'str_description' => 'econoline 150',
                'str_description_slug' => 'econoline-150',
                'bol_active' => 1,
            ),
            35 => 
            array (
                'id_model' => 1036,
                'id_manufacturer' => 134,
                'str_description' => 'econoline 240',
                'str_description_slug' => 'econoline-240',
                'bol_active' => 1,
            ),
            36 => 
            array (
                'id_model' => 1037,
                'id_manufacturer' => 134,
                'str_description' => 'econoline 350',
                'str_description_slug' => 'econoline-350',
                'bol_active' => 1,
            ),
            37 => 
            array (
                'id_model' => 1038,
                'id_manufacturer' => 134,
                'str_description' => 'econovan',
                'str_description_slug' => 'econovan',
                'bol_active' => 1,
            ),
            38 => 
            array (
                'id_model' => 1039,
                'id_manufacturer' => 134,
                'str_description' => 'ecosport',
                'str_description_slug' => 'ecosport',
                'bol_active' => 1,
            ),
            39 => 
            array (
                'id_model' => 1040,
                'id_manufacturer' => 134,
                'str_description' => 'edge',
                'str_description_slug' => 'edge',
                'bol_active' => 1,
            ),
            40 => 
            array (
                'id_model' => 1041,
                'id_manufacturer' => 134,
                'str_description' => 'escape',
                'str_description_slug' => 'escape',
                'bol_active' => 1,
            ),
            41 => 
            array (
                'id_model' => 1042,
                'id_manufacturer' => 134,
                'str_description' => 'escort',
                'str_description_slug' => 'escort',
                'bol_active' => 1,
            ),
            42 => 
            array (
                'id_model' => 1043,
                'id_manufacturer' => 134,
                'str_description' => 'excursion',
                'str_description_slug' => 'excursion',
                'bol_active' => 1,
            ),
            43 => 
            array (
                'id_model' => 1044,
                'id_manufacturer' => 134,
                'str_description' => 'expedition',
                'str_description_slug' => 'expedition',
                'bol_active' => 1,
            ),
            44 => 
            array (
                'id_model' => 1045,
                'id_manufacturer' => 134,
                'str_description' => 'explorer',
                'str_description_slug' => 'explorer',
                'bol_active' => 1,
            ),
            45 => 
            array (
                'id_model' => 1046,
                'id_manufacturer' => 134,
                'str_description' => 'f 100',
                'str_description_slug' => 'f-100',
                'bol_active' => 1,
            ),
            46 => 
            array (
                'id_model' => 1047,
                'id_manufacturer' => 134,
                'str_description' => 'f 150',
                'str_description_slug' => 'f-150',
                'bol_active' => 1,
            ),
            47 => 
            array (
                'id_model' => 1048,
                'id_manufacturer' => 134,
                'str_description' => 'f 250',
                'str_description_slug' => 'f-250',
                'bol_active' => 1,
            ),
            48 => 
            array (
                'id_model' => 1049,
                'id_manufacturer' => 134,
                'str_description' => 'f 350',
                'str_description_slug' => 'f-350',
                'bol_active' => 1,
            ),
            49 => 
            array (
                'id_model' => 1050,
                'id_manufacturer' => 134,
                'str_description' => 'fairlane 500',
                'str_description_slug' => 'fairlane-500',
                'bol_active' => 1,
            ),
            50 => 
            array (
                'id_model' => 1051,
                'id_manufacturer' => 134,
                'str_description' => 'fairmont',
                'str_description_slug' => 'fairmont',
                'bol_active' => 1,
            ),
            51 => 
            array (
                'id_model' => 1052,
                'id_manufacturer' => 134,
                'str_description' => 'falcon',
                'str_description_slug' => 'falcon',
                'bol_active' => 1,
            ),
            52 => 
            array (
                'id_model' => 1053,
                'id_manufacturer' => 134,
                'str_description' => 'festiva',
                'str_description_slug' => 'festiva',
                'bol_active' => 1,
            ),
            53 => 
            array (
                'id_model' => 1054,
                'id_manufacturer' => 134,
                'str_description' => 'fiesta',
                'str_description_slug' => 'fiesta',
                'bol_active' => 1,
            ),
            54 => 
            array (
                'id_model' => 1055,
                'id_manufacturer' => 134,
                'str_description' => 'five hundred',
                'str_description_slug' => 'five-hundred',
                'bol_active' => 1,
            ),
            55 => 
            array (
                'id_model' => 1056,
                'id_manufacturer' => 134,
                'str_description' => 'flex',
                'str_description_slug' => 'flex',
                'bol_active' => 1,
            ),
            56 => 
            array (
                'id_model' => 1057,
                'id_manufacturer' => 134,
                'str_description' => 'focus',
                'str_description_slug' => 'focus',
                'bol_active' => 1,
            ),
            57 => 
            array (
                'id_model' => 1058,
                'id_manufacturer' => 134,
                'str_description' => 'focus c-max',
                'str_description_slug' => 'focus-c-max',
                'bol_active' => 1,
            ),
            58 => 
            array (
                'id_model' => 1059,
                'id_manufacturer' => 134,
                'str_description' => 'focus cc',
                'str_description_slug' => 'focus-cc',
                'bol_active' => 1,
            ),
            59 => 
            array (
                'id_model' => 1060,
                'id_manufacturer' => 134,
                'str_description' => 'freda',
                'str_description_slug' => 'freda',
                'bol_active' => 1,
            ),
            60 => 
            array (
                'id_model' => 1061,
                'id_manufacturer' => 134,
                'str_description' => 'fusion',
                'str_description_slug' => 'fusion',
                'bol_active' => 1,
            ),
            61 => 
            array (
                'id_model' => 1062,
                'id_manufacturer' => 134,
                'str_description' => 'galaxie',
                'str_description_slug' => 'galaxie',
                'bol_active' => 1,
            ),
            62 => 
            array (
                'id_model' => 1063,
                'id_manufacturer' => 134,
                'str_description' => 'galaxy',
                'str_description_slug' => 'galaxy',
                'bol_active' => 1,
            ),
            63 => 
            array (
                'id_model' => 1064,
                'id_manufacturer' => 134,
                'str_description' => 'gpw',
                'str_description_slug' => 'gpw',
                'bol_active' => 1,
            ),
            64 => 
            array (
                'id_model' => 1065,
                'id_manufacturer' => 134,
                'str_description' => 'gran torino',
                'str_description_slug' => 'gran-torino',
                'bol_active' => 1,
            ),
            65 => 
            array (
                'id_model' => 1066,
                'id_manufacturer' => 134,
                'str_description' => 'granada',
                'str_description_slug' => 'granada',
                'bol_active' => 1,
            ),
            66 => 
            array (
                'id_model' => 1067,
                'id_manufacturer' => 134,
                'str_description' => 'grand c-max',
                'str_description_slug' => 'grand-c-max',
                'bol_active' => 1,
            ),
            67 => 
            array (
                'id_model' => 1068,
                'id_manufacturer' => 134,
                'str_description' => 'gt',
                'str_description_slug' => 'gt',
                'bol_active' => 1,
            ),
            68 => 
            array (
                'id_model' => 1069,
                'id_manufacturer' => 134,
                'str_description' => 'gt40',
                'str_description_slug' => 'gt40',
                'bol_active' => 1,
            ),
            69 => 
            array (
                'id_model' => 1070,
                'id_manufacturer' => 134,
                'str_description' => 'hornet',
                'str_description_slug' => 'hornet',
                'bol_active' => 1,
            ),
            70 => 
            array (
                'id_model' => 1071,
                'id_manufacturer' => 134,
                'str_description' => 'ka',
                'str_description_slug' => 'ka',
                'bol_active' => 1,
            ),
            71 => 
            array (
                'id_model' => 1072,
                'id_manufacturer' => 134,
                'str_description' => 'ka+',
                'str_description_slug' => 'ka+',
                'bol_active' => 1,
            ),
            72 => 
            array (
                'id_model' => 1073,
                'id_manufacturer' => 134,
                'str_description' => 'kuga',
                'str_description_slug' => 'kuga',
                'bol_active' => 1,
            ),
            73 => 
            array (
                'id_model' => 1074,
                'id_manufacturer' => 134,
                'str_description' => 'landau',
                'str_description_slug' => 'landau',
                'bol_active' => 1,
            ),
            74 => 
            array (
                'id_model' => 1075,
                'id_manufacturer' => 134,
                'str_description' => 'laser',
                'str_description_slug' => 'laser',
                'bol_active' => 1,
            ),
            75 => 
            array (
                'id_model' => 1076,
                'id_manufacturer' => 134,
                'str_description' => 'lobo',
                'str_description_slug' => 'lobo',
                'bol_active' => 1,
            ),
            76 => 
            array (
                'id_model' => 1077,
                'id_manufacturer' => 134,
                'str_description' => 'ltd',
                'str_description_slug' => 'ltd',
                'bol_active' => 1,
            ),
            77 => 
            array (
                'id_model' => 1078,
                'id_manufacturer' => 134,
                'str_description' => 'ltd crown victoria',
                'str_description_slug' => 'ltd-crown-victoria',
                'bol_active' => 1,
            ),
            78 => 
            array (
                'id_model' => 1079,
                'id_manufacturer' => 134,
                'str_description' => 'mamut',
                'str_description_slug' => 'mamut',
                'bol_active' => 1,
            ),
            79 => 
            array (
                'id_model' => 1080,
                'id_manufacturer' => 134,
                'str_description' => 'mark viii',
                'str_description_slug' => 'mark-viii',
                'bol_active' => 1,
            ),
            80 => 
            array (
                'id_model' => 1081,
                'id_manufacturer' => 134,
                'str_description' => 'maverick',
                'str_description_slug' => 'maverick',
                'bol_active' => 1,
            ),
            81 => 
            array (
                'id_model' => 1082,
                'id_manufacturer' => 134,
                'str_description' => 'model 40',
                'str_description_slug' => 'model-40',
                'bol_active' => 1,
            ),
            82 => 
            array (
                'id_model' => 1083,
                'id_manufacturer' => 134,
                'str_description' => 'mondeo',
                'str_description_slug' => 'mondeo',
                'bol_active' => 1,
            ),
            83 => 
            array (
                'id_model' => 1084,
                'id_manufacturer' => 134,
                'str_description' => 'mustang',
                'str_description_slug' => 'mustang',
                'bol_active' => 1,
            ),
            84 => 
            array (
                'id_model' => 1085,
                'id_manufacturer' => 134,
                'str_description' => 'navigator',
                'str_description_slug' => 'navigator',
                'bol_active' => 1,
            ),
            85 => 
            array (
                'id_model' => 1086,
                'id_manufacturer' => 134,
                'str_description' => 'orion',
                'str_description_slug' => 'orion',
                'bol_active' => 1,
            ),
            86 => 
            array (
                'id_model' => 1087,
                'id_manufacturer' => 134,
                'str_description' => 'phaeton',
                'str_description_slug' => 'phaeton',
                'bol_active' => 1,
            ),
            87 => 
            array (
                'id_model' => 1088,
                'id_manufacturer' => 134,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            88 => 
            array (
                'id_model' => 1089,
                'id_manufacturer' => 134,
                'str_description' => 'prefect',
                'str_description_slug' => 'prefect',
                'bol_active' => 1,
            ),
            89 => 
            array (
                'id_model' => 1090,
                'id_manufacturer' => 134,
                'str_description' => 'probe',
                'str_description_slug' => 'probe',
                'bol_active' => 1,
            ),
            90 => 
            array (
                'id_model' => 1091,
                'id_manufacturer' => 134,
                'str_description' => 'puma',
                'str_description_slug' => 'puma',
                'bol_active' => 1,
            ),
            91 => 
            array (
                'id_model' => 1092,
                'id_manufacturer' => 134,
                'str_description' => 'ranchero',
                'str_description_slug' => 'ranchero',
                'bol_active' => 1,
            ),
            92 => 
            array (
                'id_model' => 1093,
                'id_manufacturer' => 134,
                'str_description' => 'ranger',
                'str_description_slug' => 'ranger',
                'bol_active' => 1,
            ),
            93 => 
            array (
                'id_model' => 1094,
                'id_manufacturer' => 134,
                'str_description' => 'rs 200',
                'str_description_slug' => 'rs-200',
                'bol_active' => 1,
            ),
            94 => 
            array (
                'id_model' => 1095,
                'id_manufacturer' => 134,
                'str_description' => 's-max',
                'str_description_slug' => 's-max',
                'bol_active' => 1,
            ),
            95 => 
            array (
                'id_model' => 1096,
                'id_manufacturer' => 134,
                'str_description' => 'scorpio',
                'str_description_slug' => 'scorpio',
                'bol_active' => 1,
            ),
            96 => 
            array (
                'id_model' => 1097,
                'id_manufacturer' => 134,
                'str_description' => 'sierra',
                'str_description_slug' => 'sierra',
                'bol_active' => 1,
            ),
            97 => 
            array (
                'id_model' => 1098,
                'id_manufacturer' => 134,
                'str_description' => 'super deluxe',
                'str_description_slug' => 'super-deluxe',
                'bol_active' => 1,
            ),
            98 => 
            array (
                'id_model' => 1099,
                'id_manufacturer' => 134,
                'str_description' => 't',
                'str_description_slug' => 't',
                'bol_active' => 1,
            ),
            99 => 
            array (
                'id_model' => 1100,
                'id_manufacturer' => 134,
                'str_description' => 'taunus',
                'str_description_slug' => 'taunus',
                'bol_active' => 1,
            ),
            100 => 
            array (
                'id_model' => 1101,
                'id_manufacturer' => 134,
                'str_description' => 'taurus',
                'str_description_slug' => 'taurus',
                'bol_active' => 1,
            ),
            101 => 
            array (
                'id_model' => 1102,
                'id_manufacturer' => 134,
                'str_description' => 'tempo',
                'str_description_slug' => 'tempo',
                'bol_active' => 1,
            ),
            102 => 
            array (
                'id_model' => 1103,
                'id_manufacturer' => 134,
                'str_description' => 'thunderbird',
                'str_description_slug' => 'thunderbird',
                'bol_active' => 1,
            ),
            103 => 
            array (
                'id_model' => 1104,
                'id_manufacturer' => 134,
                'str_description' => 'topaz',
                'str_description_slug' => 'topaz',
                'bol_active' => 1,
            ),
            104 => 
            array (
                'id_model' => 1105,
                'id_manufacturer' => 134,
                'str_description' => 'tourneo connect',
                'str_description_slug' => 'tourneo-connect',
                'bol_active' => 1,
            ),
            105 => 
            array (
                'id_model' => 1106,
                'id_manufacturer' => 134,
                'str_description' => 'tourneo courier',
                'str_description_slug' => 'tourneo-courier',
                'bol_active' => 1,
            ),
            106 => 
            array (
                'id_model' => 1107,
                'id_manufacturer' => 134,
                'str_description' => 'transit',
                'str_description_slug' => 'transit',
                'bol_active' => 1,
            ),
            107 => 
            array (
                'id_model' => 1108,
                'id_manufacturer' => 134,
                'str_description' => 'transit 2',
                'str_description_slug' => 'transit-2',
                'bol_active' => 1,
            ),
            108 => 
            array (
                'id_model' => 1109,
                'id_manufacturer' => 134,
                'str_description' => 'transit courier',
                'str_description_slug' => 'transit-courier',
                'bol_active' => 1,
            ),
            109 => 
            array (
                'id_model' => 1110,
                'id_manufacturer' => 134,
                'str_description' => 'tt',
                'str_description_slug' => 'tt',
                'bol_active' => 1,
            ),
            110 => 
            array (
                'id_model' => 1111,
                'id_manufacturer' => 134,
                'str_description' => 'v8',
                'str_description_slug' => 'v8',
                'bol_active' => 1,
            ),
            111 => 
            array (
                'id_model' => 1112,
                'id_manufacturer' => 134,
                'str_description' => 'vedette',
                'str_description_slug' => 'vedette',
                'bol_active' => 1,
            ),
            112 => 
            array (
                'id_model' => 1113,
                'id_manufacturer' => 134,
                'str_description' => 'windstar',
                'str_description_slug' => 'windstar',
                'bol_active' => 1,
            ),
            113 => 
            array (
                'id_model' => 1114,
                'id_manufacturer' => 134,
                'str_description' => 'y',
                'str_description_slug' => 'y',
                'bol_active' => 1,
            ),
            114 => 
            array (
                'id_model' => 1115,
                'id_manufacturer' => 134,
                'str_description' => 'zephyr',
                'str_description_slug' => 'zephyr',
                'bol_active' => 1,
            ),
            115 => 
            array (
                'id_model' => 1116,
                'id_manufacturer' => 134,
                'str_description' => 'zodiac',
                'str_description_slug' => 'zodiac',
                'bol_active' => 1,
            ),
            116 => 
            array (
                'id_model' => 1117,
                'id_manufacturer' => 135,
                'str_description' => 'rr99',
                'str_description_slug' => 'rr99',
                'bol_active' => 1,
            ),
            117 => 
            array (
                'id_model' => 1118,
                'id_manufacturer' => 136,
                'str_description' => '650',
                'str_description_slug' => '650',
                'bol_active' => 1,
            ),
            118 => 
            array (
                'id_model' => 1119,
                'id_manufacturer' => 137,
                'str_description' => '125',
                'str_description_slug' => '125',
                'bol_active' => 1,
            ),
            119 => 
            array (
                'id_model' => 1120,
                'id_manufacturer' => 137,
                'str_description' => 'polonez',
                'str_description_slug' => 'polonez',
                'bol_active' => 1,
            ),
            120 => 
            array (
                'id_model' => 1121,
                'id_manufacturer' => 138,
                'str_description' => 'exceed',
                'str_description_slug' => 'exceed',
                'bol_active' => 1,
            ),
            121 => 
            array (
                'id_model' => 1122,
                'id_manufacturer' => 138,
                'str_description' => 'innovation',
                'str_description_slug' => 'innovation',
                'bol_active' => 1,
            ),
            122 => 
            array (
                'id_model' => 1123,
                'id_manufacturer' => 138,
                'str_description' => 'santamo',
                'str_description_slug' => 'santamo',
                'bol_active' => 1,
            ),
            123 => 
            array (
                'id_model' => 1124,
                'id_manufacturer' => 138,
                'str_description' => 'super exceed',
                'str_description_slug' => 'super-exceed',
                'bol_active' => 1,
            ),
            124 => 
            array (
                'id_model' => 1125,
                'id_manufacturer' => 139,
                'str_description' => 'golf car',
                'str_description_slug' => 'golf-car',
                'bol_active' => 1,
            ),
            125 => 
            array (
                'id_model' => 1126,
                'id_manufacturer' => 139,
                'str_description' => 'lsv',
                'str_description_slug' => 'lsv',
                'bol_active' => 1,
            ),
            126 => 
            array (
                'id_model' => 1127,
                'id_manufacturer' => 139,
                'str_description' => 'monaco',
                'str_description_slug' => 'monaco',
                'bol_active' => 1,
            ),
            127 => 
            array (
                'id_model' => 1128,
                'id_manufacturer' => 140,
                'str_description' => 'gateau',
                'str_description_slug' => 'gateau',
                'bol_active' => 1,
            ),
            128 => 
            array (
                'id_model' => 1129,
                'id_manufacturer' => 140,
                'str_description' => 'la vision',
                'str_description_slug' => 'la-vision',
                'bol_active' => 1,
            ),
            129 => 
            array (
                'id_model' => 1130,
                'id_manufacturer' => 141,
                'str_description' => '14',
                'str_description_slug' => '14',
                'bol_active' => 1,
            ),
            130 => 
            array (
                'id_model' => 1131,
                'id_manufacturer' => 141,
                'str_description' => '67',
                'str_description_slug' => '67',
                'bol_active' => 1,
            ),
            131 => 
            array (
                'id_model' => 1132,
                'id_manufacturer' => 141,
                'str_description' => '2752',
                'str_description_slug' => '2752',
                'bol_active' => 1,
            ),
            132 => 
            array (
                'id_model' => 1133,
                'id_manufacturer' => 141,
                'str_description' => 'gacela',
                'str_description_slug' => 'gacela',
                'bol_active' => 1,
            ),
            133 => 
            array (
                'id_model' => 1134,
                'id_manufacturer' => 141,
                'str_description' => 'gazelle',
                'str_description_slug' => 'gazelle',
                'bol_active' => 1,
            ),
            134 => 
            array (
                'id_model' => 1135,
                'id_manufacturer' => 142,
                'str_description' => 'e2',
                'str_description_slug' => 'e2',
                'bol_active' => 1,
            ),
            135 => 
            array (
                'id_model' => 1136,
                'id_manufacturer' => 142,
                'str_description' => 'e4',
                'str_description_slug' => 'e4',
                'bol_active' => 1,
            ),
            136 => 
            array (
                'id_model' => 1137,
                'id_manufacturer' => 142,
                'str_description' => 'el',
                'str_description_slug' => 'el',
                'bol_active' => 1,
            ),
            137 => 
            array (
                'id_model' => 1138,
                'id_manufacturer' => 142,
                'str_description' => 'es',
                'str_description_slug' => 'es',
                'bol_active' => 1,
            ),
            138 => 
            array (
                'id_model' => 1139,
                'id_manufacturer' => 143,
                'str_description' => 'metro',
                'str_description_slug' => 'metro',
                'bol_active' => 1,
            ),
            139 => 
            array (
                'id_model' => 1140,
                'id_manufacturer' => 143,
                'str_description' => 'storm',
                'str_description_slug' => 'storm',
                'bol_active' => 1,
            ),
            140 => 
            array (
                'id_model' => 1141,
                'id_manufacturer' => 143,
                'str_description' => 'tracker',
                'str_description_slug' => 'tracker',
                'bol_active' => 1,
            ),
            141 => 
            array (
                'id_model' => 1142,
                'id_manufacturer' => 144,
                'str_description' => 'g20',
                'str_description_slug' => 'g20',
                'bol_active' => 1,
            ),
            142 => 
            array (
                'id_model' => 1143,
                'id_manufacturer' => 145,
                'str_description' => 'ginko',
                'str_description_slug' => 'ginko',
                'bol_active' => 1,
            ),
            143 => 
            array (
                'id_model' => 1144,
                'id_manufacturer' => 146,
                'str_description' => '102-22',
                'str_description_slug' => '102-22',
                'bol_active' => 1,
            ),
            144 => 
            array (
                'id_model' => 1145,
                'id_manufacturer' => 146,
                'str_description' => 'acadia',
                'str_description_slug' => 'acadia',
                'bol_active' => 1,
            ),
            145 => 
            array (
                'id_model' => 1146,
                'id_manufacturer' => 146,
                'str_description' => 'cobra',
                'str_description_slug' => 'cobra',
                'bol_active' => 1,
            ),
            146 => 
            array (
                'id_model' => 1147,
                'id_manufacturer' => 146,
                'str_description' => 'envoy',
                'str_description_slug' => 'envoy',
                'bol_active' => 1,
            ),
            147 => 
            array (
                'id_model' => 1148,
                'id_manufacturer' => 146,
                'str_description' => 'gerrinch',
                'str_description_slug' => 'gerrinch',
                'bol_active' => 1,
            ),
            148 => 
            array (
                'id_model' => 1149,
                'id_manufacturer' => 146,
                'str_description' => 'jimmy',
                'str_description_slug' => 'jimmy',
                'bol_active' => 1,
            ),
            149 => 
            array (
                'id_model' => 1150,
                'id_manufacturer' => 146,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            150 => 
            array (
                'id_model' => 1151,
                'id_manufacturer' => 146,
                'str_description' => 'safari',
                'str_description_slug' => 'safari',
                'bol_active' => 1,
            ),
            151 => 
            array (
                'id_model' => 1152,
                'id_manufacturer' => 146,
                'str_description' => 'savana',
                'str_description_slug' => 'savana',
                'bol_active' => 1,
            ),
            152 => 
            array (
                'id_model' => 1153,
                'id_manufacturer' => 146,
                'str_description' => 'sierra',
                'str_description_slug' => 'sierra',
                'bol_active' => 1,
            ),
            153 => 
            array (
                'id_model' => 1154,
                'id_manufacturer' => 146,
                'str_description' => 'silverado',
                'str_description_slug' => 'silverado',
                'bol_active' => 1,
            ),
            154 => 
            array (
                'id_model' => 1155,
                'id_manufacturer' => 146,
                'str_description' => 'sonoma',
                'str_description_slug' => 'sonoma',
                'bol_active' => 1,
            ),
            155 => 
            array (
                'id_model' => 1156,
                'id_manufacturer' => 146,
                'str_description' => 'suburban',
                'str_description_slug' => 'suburban',
                'bol_active' => 1,
            ),
            156 => 
            array (
                'id_model' => 1157,
                'id_manufacturer' => 146,
                'str_description' => 'syclone',
                'str_description_slug' => 'syclone',
                'bol_active' => 1,
            ),
            157 => 
            array (
                'id_model' => 1158,
                'id_manufacturer' => 146,
                'str_description' => 'tracker',
                'str_description_slug' => 'tracker',
                'bol_active' => 1,
            ),
            158 => 
            array (
                'id_model' => 1159,
                'id_manufacturer' => 146,
                'str_description' => 'typhoon',
                'str_description_slug' => 'typhoon',
                'bol_active' => 1,
            ),
            159 => 
            array (
                'id_model' => 1160,
                'id_manufacturer' => 146,
                'str_description' => 'vandura',
                'str_description_slug' => 'vandura',
                'bol_active' => 1,
            ),
            160 => 
            array (
                'id_model' => 1161,
                'id_manufacturer' => 146,
                'str_description' => 'yukon',
                'str_description_slug' => 'yukon',
                'bol_active' => 1,
            ),
            161 => 
            array (
                'id_model' => 1162,
                'id_manufacturer' => 147,
                'str_description' => 'midi',
                'str_description_slug' => 'midi',
                'bol_active' => 1,
            ),
            162 => 
            array (
                'id_model' => 1163,
                'id_manufacturer' => 147,
                'str_description' => 'rascal',
                'str_description_slug' => 'rascal',
                'bol_active' => 1,
            ),
            163 => 
            array (
                'id_model' => 1164,
                'id_manufacturer' => 148,
                'str_description' => 'buggy',
                'str_description_slug' => 'buggy',
                'bol_active' => 1,
            ),
            164 => 
            array (
                'id_model' => 1165,
                'id_manufacturer' => 149,
                'str_description' => 'ruf',
                'str_description_slug' => 'ruf',
                'bol_active' => 1,
            ),
            165 => 
            array (
                'id_model' => 1166,
                'id_manufacturer' => 150,
                'str_description' => 'goggomobil',
                'str_description_slug' => 'goggomobil',
                'bol_active' => 1,
            ),
            166 => 
            array (
                'id_model' => 1167,
                'id_manufacturer' => 151,
                'str_description' => 'gk',
                'str_description_slug' => 'gk',
                'bol_active' => 1,
            ),
            167 => 
            array (
                'id_model' => 1168,
                'id_manufacturer' => 152,
                'str_description' => 'roadster',
                'str_description_slug' => 'roadster',
                'bol_active' => 1,
            ),
            168 => 
            array (
                'id_model' => 1169,
                'id_manufacturer' => 153,
                'str_description' => 'azro',
                'str_description_slug' => 'azro',
                'bol_active' => 1,
            ),
            169 => 
            array (
                'id_model' => 1170,
                'id_manufacturer' => 153,
                'str_description' => 'g3',
                'str_description_slug' => 'g3',
                'bol_active' => 1,
            ),
            170 => 
            array (
                'id_model' => 1171,
                'id_manufacturer' => 153,
                'str_description' => 'urbaplus',
                'str_description_slug' => 'urbaplus',
                'bol_active' => 1,
            ),
            171 => 
            array (
                'id_model' => 1172,
                'id_manufacturer' => 154,
                'str_description' => 'sedan',
                'str_description_slug' => 'sedan',
                'bol_active' => 1,
            ),
            172 => 
            array (
                'id_model' => 1173,
                'id_manufacturer' => 155,
                'str_description' => 'amica luna',
                'str_description_slug' => 'amica-luna',
                'bol_active' => 1,
            ),
            173 => 
            array (
                'id_model' => 1174,
                'id_manufacturer' => 155,
                'str_description' => 'eke',
                'str_description_slug' => 'eke',
                'bol_active' => 1,
            ),
            174 => 
            array (
                'id_model' => 1175,
                'id_manufacturer' => 155,
                'str_description' => 'sonique',
                'str_description_slug' => 'sonique',
                'bol_active' => 1,
            ),
            175 => 
            array (
                'id_model' => 1176,
                'id_manufacturer' => 156,
                'str_description' => 'bravo 4',
                'str_description_slug' => 'bravo-4',
                'bol_active' => 1,
            ),
            176 => 
            array (
                'id_model' => 1177,
                'id_manufacturer' => 157,
                'str_description' => 'stormy',
                'str_description_slug' => 'stormy',
                'bol_active' => 1,
            ),
            177 => 
            array (
                'id_model' => 1178,
                'id_manufacturer' => 157,
                'str_description' => 'xyjk',
                'str_description_slug' => 'xyjk',
                'bol_active' => 1,
            ),
            178 => 
            array (
                'id_model' => 1179,
                'id_manufacturer' => 157,
                'str_description' => 'xykd',
                'str_description_slug' => 'xykd',
                'bol_active' => 1,
            ),
            179 => 
            array (
                'id_model' => 1180,
                'id_manufacturer' => 158,
                'str_description' => 'x 12',
                'str_description_slug' => 'x-12',
                'bol_active' => 1,
            ),
            180 => 
            array (
                'id_model' => 1181,
                'id_manufacturer' => 159,
                'str_description' => 'husky',
                'str_description_slug' => 'husky',
                'bol_active' => 1,
            ),
            181 => 
            array (
                'id_model' => 1182,
                'id_manufacturer' => 159,
                'str_description' => 'minx',
                'str_description_slug' => 'minx',
                'bol_active' => 1,
            ),
            182 => 
            array (
                'id_model' => 1183,
                'id_manufacturer' => 159,
                'str_description' => 'station wagon',
                'str_description_slug' => 'station-wagon',
                'bol_active' => 1,
            ),
            183 => 
            array (
                'id_model' => 1184,
                'id_manufacturer' => 160,
                'str_description' => 'mallorca',
                'str_description_slug' => 'mallorca',
                'bol_active' => 1,
            ),
            184 => 
            array (
                'id_model' => 1185,
                'id_manufacturer' => 161,
                'str_description' => 'doble phaeton',
                'str_description_slug' => 'doble-phaeton',
                'bol_active' => 1,
            ),
            185 => 
            array (
                'id_model' => 1186,
                'id_manufacturer' => 161,
                'str_description' => 'h6',
                'str_description_slug' => 'h6',
                'bol_active' => 1,
            ),
            186 => 
            array (
                'id_model' => 1187,
                'id_manufacturer' => 161,
                'str_description' => 'k6',
                'str_description_slug' => 'k6',
                'bol_active' => 1,
            ),
            187 => 
            array (
                'id_model' => 1188,
                'id_manufacturer' => 161,
                'str_description' => 't49',
                'str_description_slug' => 't49',
                'bol_active' => 1,
            ),
            188 => 
            array (
                'id_model' => 1189,
                'id_manufacturer' => 162,
                'str_description' => 'dl',
                'str_description_slug' => 'dl',
                'bol_active' => 1,
            ),
            189 => 
            array (
                'id_model' => 1190,
                'id_manufacturer' => 163,
                'str_description' => 'commodore',
                'str_description_slug' => 'commodore',
                'bol_active' => 1,
            ),
            190 => 
            array (
                'id_model' => 1191,
                'id_manufacturer' => 163,
                'str_description' => 'jackaroo',
                'str_description_slug' => 'jackaroo',
                'bol_active' => 1,
            ),
            191 => 
            array (
                'id_model' => 1192,
                'id_manufacturer' => 164,
                'str_description' => 'city',
                'str_description_slug' => 'city',
                'bol_active' => 1,
            ),
            192 => 
            array (
                'id_model' => 1193,
                'id_manufacturer' => 164,
                'str_description' => 'civic tourer',
                'str_description_slug' => 'civic-tourer',
                'bol_active' => 1,
            ),
            193 => 
            array (
                'id_model' => 1194,
                'id_manufacturer' => 164,
                'str_description' => 'cr-z',
                'str_description_slug' => 'cr-z',
                'bol_active' => 1,
            ),
            194 => 
            array (
                'id_model' => 1195,
                'id_manufacturer' => 164,
                'str_description' => 'crosstour',
                'str_description_slug' => 'crosstour',
                'bol_active' => 1,
            ),
            195 => 
            array (
                'id_model' => 1196,
                'id_manufacturer' => 164,
                'str_description' => 'element',
                'str_description_slug' => 'element',
                'bol_active' => 1,
            ),
            196 => 
            array (
                'id_model' => 1197,
                'id_manufacturer' => 164,
                'str_description' => 'fit',
                'str_description_slug' => 'fit',
                'bol_active' => 1,
            ),
            197 => 
            array (
                'id_model' => 1198,
                'id_manufacturer' => 164,
                'str_description' => 'insight',
                'str_description_slug' => 'insight',
                'bol_active' => 1,
            ),
            198 => 
            array (
                'id_model' => 1199,
                'id_manufacturer' => 164,
                'str_description' => 'integra',
                'str_description_slug' => 'integra',
                'bol_active' => 1,
            ),
            199 => 
            array (
                'id_model' => 1200,
                'id_manufacturer' => 164,
                'str_description' => 'jazz atv',
                'str_description_slug' => 'jazz-atv',
                'bol_active' => 1,
            ),
            200 => 
            array (
                'id_model' => 1201,
                'id_manufacturer' => 164,
                'str_description' => 'legend',
                'str_description_slug' => 'legend',
                'bol_active' => 1,
            ),
            201 => 
            array (
                'id_model' => 1202,
                'id_manufacturer' => 164,
                'str_description' => 'logo',
                'str_description_slug' => 'logo',
                'bol_active' => 1,
            ),
            202 => 
            array (
                'id_model' => 1203,
                'id_manufacturer' => 164,
                'str_description' => 'monpal',
                'str_description_slug' => 'monpal',
                'bol_active' => 1,
            ),
            203 => 
            array (
                'id_model' => 1204,
                'id_manufacturer' => 164,
                'str_description' => 'nsx',
                'str_description_slug' => 'nsx',
                'bol_active' => 1,
            ),
            204 => 
            array (
                'id_model' => 1205,
                'id_manufacturer' => 164,
                'str_description' => 'odyssey',
                'str_description_slug' => 'odyssey',
                'bol_active' => 1,
            ),
            205 => 
            array (
                'id_model' => 1206,
                'id_manufacturer' => 164,
                'str_description' => 'odyssey-shuttle',
                'str_description_slug' => 'odyssey-shuttle',
                'bol_active' => 1,
            ),
            206 => 
            array (
                'id_model' => 1207,
                'id_manufacturer' => 164,
                'str_description' => 'pilot',
                'str_description_slug' => 'pilot',
                'bol_active' => 1,
            ),
            207 => 
            array (
                'id_model' => 1208,
                'id_manufacturer' => 164,
                'str_description' => 'quint integra',
                'str_description_slug' => 'quint-integra',
                'bol_active' => 1,
            ),
            208 => 
            array (
                'id_model' => 1209,
                'id_manufacturer' => 164,
                'str_description' => 'quintet',
                'str_description_slug' => 'quintet',
                'bol_active' => 1,
            ),
            209 => 
            array (
                'id_model' => 1210,
                'id_manufacturer' => 164,
                'str_description' => 'ridgeline',
                'str_description_slug' => 'ridgeline',
                'bol_active' => 1,
            ),
            210 => 
            array (
                'id_model' => 1211,
                'id_manufacturer' => 164,
                'str_description' => 's 800',
                'str_description_slug' => 's-800',
                'bol_active' => 1,
            ),
            211 => 
            array (
                'id_model' => 1212,
                'id_manufacturer' => 164,
                'str_description' => 'shuttle',
                'str_description_slug' => 'shuttle',
                'bol_active' => 1,
            ),
            212 => 
            array (
                'id_model' => 1213,
                'id_manufacturer' => 164,
                'str_description' => 'stepwgn',
                'str_description_slug' => 'stepwgn',
                'bol_active' => 1,
            ),
            213 => 
            array (
                'id_model' => 1214,
                'id_manufacturer' => 164,
                'str_description' => 'stream',
                'str_description_slug' => 'stream',
                'bol_active' => 1,
            ),
            214 => 
            array (
                'id_model' => 1215,
                'id_manufacturer' => 164,
                'str_description' => 'tn',
                'str_description_slug' => 'tn',
                'bol_active' => 1,
            ),
            215 => 
            array (
                'id_model' => 1216,
                'id_manufacturer' => 164,
                'str_description' => 'tn acty',
                'str_description_slug' => 'tn-acty',
                'bol_active' => 1,
            ),
            216 => 
            array (
                'id_model' => 1217,
                'id_manufacturer' => 164,
                'str_description' => 'vigor',
                'str_description_slug' => 'vigor',
                'bol_active' => 1,
            ),
            217 => 
            array (
                'id_model' => 1218,
                'id_manufacturer' => 164,
                'str_description' => 'z',
                'str_description_slug' => 'z',
                'bol_active' => 1,
            ),
            218 => 
            array (
                'id_model' => 1219,
                'id_manufacturer' => 165,
                'str_description' => 'golf car',
                'str_description_slug' => 'golf-car',
                'bol_active' => 1,
            ),
            219 => 
            array (
                'id_model' => 1220,
                'id_manufacturer' => 166,
                'str_description' => 'hs 400',
                'str_description_slug' => 'hs-400',
                'bol_active' => 1,
            ),
            220 => 
            array (
                'id_model' => 1221,
                'id_manufacturer' => 166,
                'str_description' => 'hs 500',
                'str_description_slug' => 'hs-500',
                'bol_active' => 1,
            ),
            221 => 
            array (
                'id_model' => 1222,
                'id_manufacturer' => 166,
                'str_description' => 'hs 700',
                'str_description_slug' => 'hs-700',
                'bol_active' => 1,
            ),
            222 => 
            array (
                'id_model' => 1223,
                'id_manufacturer' => 166,
                'str_description' => 'hs 800',
                'str_description_slug' => 'hs-800',
                'bol_active' => 1,
            ),
            223 => 
            array (
                'id_model' => 1224,
                'id_manufacturer' => 167,
                'str_description' => 'deluxe eight',
                'str_description_slug' => 'deluxe-eight',
                'bol_active' => 1,
            ),
            224 => 
            array (
                'id_model' => 1225,
                'id_manufacturer' => 167,
                'str_description' => 'u-5',
                'str_description_slug' => 'u-5',
                'bol_active' => 1,
            ),
            225 => 
            array (
                'id_model' => 1226,
                'id_manufacturer' => 168,
                'str_description' => 'hawk',
                'str_description_slug' => 'hawk',
                'bol_active' => 1,
            ),
            226 => 
            array (
                'id_model' => 1227,
                'id_manufacturer' => 168,
                'str_description' => 'sceptre',
                'str_description_slug' => 'sceptre',
                'bol_active' => 1,
            ),
            227 => 
            array (
                'id_model' => 1228,
                'id_manufacturer' => 168,
                'str_description' => 'super seven',
                'str_description_slug' => 'super-seven',
                'bol_active' => 1,
            ),
            228 => 
            array (
                'id_model' => 1229,
                'id_manufacturer' => 168,
                'str_description' => 'super snipe',
                'str_description_slug' => 'super-snipe',
                'bol_active' => 1,
            ),
            229 => 
            array (
                'id_model' => 1230,
                'id_manufacturer' => 169,
                'str_description' => 'concord',
                'str_description_slug' => 'concord',
                'bol_active' => 1,
            ),
            230 => 
            array (
                'id_model' => 1231,
                'id_manufacturer' => 169,
                'str_description' => 'eagle',
                'str_description_slug' => 'eagle',
                'bol_active' => 1,
            ),
            231 => 
            array (
                'id_model' => 1232,
                'id_manufacturer' => 169,
                'str_description' => 'h1',
                'str_description_slug' => 'h1',
                'bol_active' => 1,
            ),
            232 => 
            array (
                'id_model' => 1233,
                'id_manufacturer' => 169,
                'str_description' => 'h2',
                'str_description_slug' => 'h2',
                'bol_active' => 1,
            ),
            233 => 
            array (
                'id_model' => 1234,
                'id_manufacturer' => 169,
                'str_description' => 'h3',
                'str_description_slug' => 'h3',
                'bol_active' => 1,
            ),
            234 => 
            array (
                'id_model' => 1235,
                'id_manufacturer' => 169,
                'str_description' => 'h3t',
                'str_description_slug' => 'h3t',
                'bol_active' => 1,
            ),
            235 => 
            array (
                'id_model' => 1236,
                'id_manufacturer' => 169,
                'str_description' => 'renegade',
                'str_description_slug' => 'renegade',
                'bol_active' => 1,
            ),
            236 => 
            array (
                'id_model' => 1237,
                'id_manufacturer' => 170,
                'str_description' => 'albaycin',
                'str_description_slug' => 'albaycin',
                'bol_active' => 1,
            ),
            237 => 
            array (
                'id_model' => 1238,
                'id_manufacturer' => 170,
                'str_description' => 'grand albaycin',
                'str_description_slug' => 'grand-albaycin',
                'bol_active' => 1,
            ),
            238 => 
            array (
                'id_model' => 1239,
                'id_manufacturer' => 170,
                'str_description' => 't2',
                'str_description_slug' => 't2',
                'bol_active' => 1,
            ),
            239 => 
            array (
                'id_model' => 1240,
                'id_manufacturer' => 171,
                'str_description' => 'centennial',
                'str_description_slug' => 'centennial',
                'bol_active' => 1,
            ),
            240 => 
            array (
                'id_model' => 1241,
                'id_manufacturer' => 171,
                'str_description' => 'elantra',
                'str_description_slug' => 'elantra',
                'bol_active' => 1,
            ),
            241 => 
            array (
                'id_model' => 1242,
                'id_manufacturer' => 171,
                'str_description' => 'equus',
                'str_description_slug' => 'equus',
                'bol_active' => 1,
            ),
            242 => 
            array (
                'id_model' => 1243,
                'id_manufacturer' => 171,
                'str_description' => 'excel',
                'str_description_slug' => 'excel',
                'bol_active' => 1,
            ),
            243 => 
            array (
                'id_model' => 1244,
                'id_manufacturer' => 171,
                'str_description' => 'gandeur',
                'str_description_slug' => 'gandeur',
                'bol_active' => 1,
            ),
            244 => 
            array (
                'id_model' => 1245,
                'id_manufacturer' => 171,
                'str_description' => 'genesis',
                'str_description_slug' => 'genesis',
                'bol_active' => 1,
            ),
            245 => 
            array (
                'id_model' => 1246,
                'id_manufacturer' => 171,
                'str_description' => 'grace',
                'str_description_slug' => 'grace',
                'bol_active' => 1,
            ),
            246 => 
            array (
                'id_model' => 1247,
                'id_manufacturer' => 171,
                'str_description' => 'grand santa fe',
                'str_description_slug' => 'grand-santa-fe',
                'bol_active' => 1,
            ),
            247 => 
            array (
                'id_model' => 1248,
                'id_manufacturer' => 171,
                'str_description' => 'grandeur',
                'str_description_slug' => 'grandeur',
                'bol_active' => 1,
            ),
            248 => 
            array (
                'id_model' => 1249,
                'id_manufacturer' => 171,
                'str_description' => 'h 200',
                'str_description_slug' => 'h-200',
                'bol_active' => 1,
            ),
            249 => 
            array (
                'id_model' => 1250,
                'id_manufacturer' => 171,
                'str_description' => 'h 350',
                'str_description_slug' => 'h-350',
                'bol_active' => 1,
            ),
            250 => 
            array (
                'id_model' => 1251,
                'id_manufacturer' => 171,
                'str_description' => 'h-1',
                'str_description_slug' => 'h-1',
                'bol_active' => 1,
            ),
            251 => 
            array (
                'id_model' => 1252,
                'id_manufacturer' => 171,
                'str_description' => 'h-1 travel',
                'str_description_slug' => 'h-1-travel',
                'bol_active' => 1,
            ),
            252 => 
            array (
                'id_model' => 1253,
                'id_manufacturer' => 171,
                'str_description' => 'h-1 van',
                'str_description_slug' => 'h-1-van',
                'bol_active' => 1,
            ),
            253 => 
            array (
                'id_model' => 1254,
                'id_manufacturer' => 171,
                'str_description' => 'h-100',
                'str_description_slug' => 'h-100',
                'bol_active' => 1,
            ),
            254 => 
            array (
                'id_model' => 1255,
                'id_manufacturer' => 171,
                'str_description' => 'i10',
                'str_description_slug' => 'i10',
                'bol_active' => 1,
            ),
            255 => 
            array (
                'id_model' => 1256,
                'id_manufacturer' => 171,
                'str_description' => 'i20',
                'str_description_slug' => 'i20',
                'bol_active' => 1,
            ),
            256 => 
            array (
                'id_model' => 1257,
                'id_manufacturer' => 171,
                'str_description' => 'i20 active',
                'str_description_slug' => 'i20-active',
                'bol_active' => 1,
            ),
            257 => 
            array (
                'id_model' => 1258,
                'id_manufacturer' => 171,
                'str_description' => 'i40',
                'str_description_slug' => 'i40',
                'bol_active' => 1,
            ),
            258 => 
            array (
                'id_model' => 1259,
                'id_manufacturer' => 171,
                'str_description' => 'i800',
                'str_description_slug' => 'i800',
                'bol_active' => 1,
            ),
            259 => 
            array (
                'id_model' => 1260,
                'id_manufacturer' => 171,
                'str_description' => 'i800 van',
                'str_description_slug' => 'i800-van',
                'bol_active' => 1,
            ),
            260 => 
            array (
                'id_model' => 1261,
                'id_manufacturer' => 171,
                'str_description' => 'ioniq',
                'str_description_slug' => 'ioniq',
                'bol_active' => 1,
            ),
            261 => 
            array (
                'id_model' => 1262,
                'id_manufacturer' => 171,
                'str_description' => 'ix 20',
                'str_description_slug' => 'ix-20',
                'bol_active' => 1,
            ),
            262 => 
            array (
                'id_model' => 1263,
                'id_manufacturer' => 171,
                'str_description' => 'ix 35',
                'str_description_slug' => 'ix-35',
                'bol_active' => 1,
            ),
            263 => 
            array (
                'id_model' => 1264,
                'id_manufacturer' => 171,
                'str_description' => 'ix 55',
                'str_description_slug' => 'ix-55',
                'bol_active' => 1,
            ),
            264 => 
            array (
                'id_model' => 1265,
                'id_manufacturer' => 171,
                'str_description' => 'marcia',
                'str_description_slug' => 'marcia',
                'bol_active' => 1,
            ),
            265 => 
            array (
                'id_model' => 1266,
                'id_manufacturer' => 171,
                'str_description' => 'pony',
                'str_description_slug' => 'pony',
                'bol_active' => 1,
            ),
            266 => 
            array (
                'id_model' => 1267,
                'id_manufacturer' => 171,
                'str_description' => 'porter',
                'str_description_slug' => 'porter',
                'bol_active' => 1,
            ),
            267 => 
            array (
                'id_model' => 1268,
                'id_manufacturer' => 171,
                'str_description' => 'satellite',
                'str_description_slug' => 'satellite',
                'bol_active' => 1,
            ),
            268 => 
            array (
                'id_model' => 1269,
                'id_manufacturer' => 171,
                'str_description' => 'scoupe',
                'str_description_slug' => 'scoupe',
                'bol_active' => 1,
            ),
            269 => 
            array (
                'id_model' => 1270,
                'id_manufacturer' => 171,
                'str_description' => 'solaris',
                'str_description_slug' => 'solaris',
                'bol_active' => 1,
            ),
            270 => 
            array (
                'id_model' => 1271,
                'id_manufacturer' => 171,
                'str_description' => 'stellar',
                'str_description_slug' => 'stellar',
                'bol_active' => 1,
            ),
            271 => 
            array (
                'id_model' => 1272,
                'id_manufacturer' => 171,
                'str_description' => 'terracan',
                'str_description_slug' => 'terracan',
                'bol_active' => 1,
            ),
            272 => 
            array (
                'id_model' => 1273,
                'id_manufacturer' => 171,
                'str_description' => 'trajet',
                'str_description_slug' => 'trajet',
                'bol_active' => 1,
            ),
            273 => 
            array (
                'id_model' => 1274,
                'id_manufacturer' => 171,
                'str_description' => 'veloster',
                'str_description_slug' => 'veloster',
                'bol_active' => 1,
            ),
            274 => 
            array (
                'id_model' => 1275,
                'id_manufacturer' => 171,
                'str_description' => 'veracruz',
                'str_description_slug' => 'veracruz',
                'bol_active' => 1,
            ),
            275 => 
            array (
                'id_model' => 1276,
                'id_manufacturer' => 171,
                'str_description' => 'xg',
                'str_description_slug' => 'xg',
                'bol_active' => 1,
            ),
            276 => 
            array (
                'id_model' => 1277,
                'id_manufacturer' => 172,
                'str_description' => '1900',
                'str_description_slug' => '1900',
                'bol_active' => 1,
            ),
            277 => 
            array (
                'id_model' => 1278,
                'id_manufacturer' => 172,
                'str_description' => '2000',
                'str_description_slug' => '2000',
                'bol_active' => 1,
            ),
            278 => 
            array (
                'id_model' => 1279,
                'id_manufacturer' => 173,
                'str_description' => 'm 25',
                'str_description_slug' => 'm-25',
                'bol_active' => 1,
            ),
            279 => 
            array (
                'id_model' => 1280,
                'id_manufacturer' => 174,
                'str_description' => 'ec 2006',
                'str_description_slug' => 'ec-2006',
                'bol_active' => 1,
            ),
            280 => 
            array (
                'id_model' => 1281,
                'id_manufacturer' => 175,
                'str_description' => 'ex',
                'str_description_slug' => 'ex',
                'bol_active' => 1,
            ),
            281 => 
            array (
                'id_model' => 1282,
                'id_manufacturer' => 175,
                'str_description' => 'fx',
                'str_description_slug' => 'fx',
                'bol_active' => 1,
            ),
            282 => 
            array (
                'id_model' => 1283,
                'id_manufacturer' => 175,
                'str_description' => 'g',
                'str_description_slug' => 'g',
                'bol_active' => 1,
            ),
            283 => 
            array (
                'id_model' => 1284,
                'id_manufacturer' => 175,
                'str_description' => 'i',
                'str_description_slug' => 'i',
                'bol_active' => 1,
            ),
            284 => 
            array (
                'id_model' => 1285,
                'id_manufacturer' => 175,
                'str_description' => 'm',
                'str_description_slug' => 'm',
                'bol_active' => 1,
            ),
            285 => 
            array (
                'id_model' => 1286,
                'id_manufacturer' => 175,
                'str_description' => 'q',
                'str_description_slug' => 'q',
                'bol_active' => 1,
            ),
            286 => 
            array (
                'id_model' => 1287,
                'id_manufacturer' => 175,
                'str_description' => 'q30',
                'str_description_slug' => 'q30',
                'bol_active' => 1,
            ),
            287 => 
            array (
                'id_model' => 1288,
                'id_manufacturer' => 175,
                'str_description' => 'q50',
                'str_description_slug' => 'q50',
                'bol_active' => 1,
            ),
            288 => 
            array (
                'id_model' => 1289,
                'id_manufacturer' => 175,
                'str_description' => 'q60',
                'str_description_slug' => 'q60',
                'bol_active' => 1,
            ),
            289 => 
            array (
                'id_model' => 1290,
                'id_manufacturer' => 175,
                'str_description' => 'q70',
                'str_description_slug' => 'q70',
                'bol_active' => 1,
            ),
            290 => 
            array (
                'id_model' => 1291,
                'id_manufacturer' => 175,
                'str_description' => 'qx',
                'str_description_slug' => 'qx',
                'bol_active' => 1,
            ),
            291 => 
            array (
                'id_model' => 1292,
                'id_manufacturer' => 175,
                'str_description' => 'qx30',
                'str_description_slug' => 'qx30',
                'bol_active' => 1,
            ),
            292 => 
            array (
                'id_model' => 1293,
                'id_manufacturer' => 175,
                'str_description' => 'qx50',
                'str_description_slug' => 'qx50',
                'bol_active' => 1,
            ),
            293 => 
            array (
                'id_model' => 1294,
                'id_manufacturer' => 175,
                'str_description' => 'qx70',
                'str_description_slug' => 'qx70',
                'bol_active' => 1,
            ),
            294 => 
            array (
                'id_model' => 1295,
                'id_manufacturer' => 176,
                'str_description' => '90',
                'str_description_slug' => '90',
                'bol_active' => 1,
            ),
            295 => 
            array (
                'id_model' => 1296,
                'id_manufacturer' => 176,
                'str_description' => '950',
                'str_description_slug' => '950',
                'bol_active' => 1,
            ),
            296 => 
            array (
                'id_model' => 1297,
                'id_manufacturer' => 176,
                'str_description' => '990',
                'str_description_slug' => '990',
                'bol_active' => 1,
            ),
            297 => 
            array (
                'id_model' => 1298,
                'id_manufacturer' => 176,
                'str_description' => 'de tomaso',
                'str_description_slug' => 'de-tomaso',
                'bol_active' => 1,
            ),
            298 => 
            array (
                'id_model' => 1299,
                'id_manufacturer' => 176,
                'str_description' => 'elba',
                'str_description_slug' => 'elba',
                'bol_active' => 1,
            ),
            299 => 
            array (
                'id_model' => 1300,
                'id_manufacturer' => 176,
                'str_description' => 'mini',
                'str_description_slug' => 'mini',
                'bol_active' => 1,
            ),
            300 => 
            array (
                'id_model' => 1301,
                'id_manufacturer' => 176,
                'str_description' => 'minitre',
                'str_description_slug' => 'minitre',
                'bol_active' => 1,
            ),
            301 => 
            array (
                'id_model' => 1302,
                'id_manufacturer' => 177,
                'str_description' => 'serie mpv',
                'str_description_slug' => 'serie-mpv',
                'bol_active' => 1,
            ),
            302 => 
            array (
                'id_model' => 1303,
                'id_manufacturer' => 178,
                'str_description' => 'keko',
                'str_description_slug' => 'keko',
                'bol_active' => 1,
            ),
            303 => 
            array (
                'id_model' => 1304,
                'id_manufacturer' => 179,
                'str_description' => 's',
                'str_description_slug' => 's',
                'bol_active' => 1,
            ),
            304 => 
            array (
                'id_model' => 1305,
                'id_manufacturer' => 179,
                'str_description' => 'scout ii',
                'str_description_slug' => 'scout-ii',
                'bol_active' => 1,
            ),
            305 => 
            array (
                'id_model' => 1306,
                'id_manufacturer' => 179,
                'str_description' => 'serie 1000',
                'str_description_slug' => 'serie-1000',
                'bol_active' => 1,
            ),
            306 => 
            array (
                'id_model' => 1307,
                'id_manufacturer' => 180,
                'str_description' => 'amigo',
                'str_description_slug' => 'amigo',
                'bol_active' => 1,
            ),
            307 => 
            array (
                'id_model' => 1308,
                'id_manufacturer' => 180,
                'str_description' => 'axiom',
                'str_description_slug' => 'axiom',
                'bol_active' => 1,
            ),
            308 => 
            array (
                'id_model' => 1309,
                'id_manufacturer' => 180,
                'str_description' => 'bellett',
                'str_description_slug' => 'bellett',
                'bol_active' => 1,
            ),
            309 => 
            array (
                'id_model' => 1310,
                'id_manufacturer' => 180,
                'str_description' => 'bighorn',
                'str_description_slug' => 'bighorn',
                'bol_active' => 1,
            ),
            310 => 
            array (
                'id_model' => 1311,
                'id_manufacturer' => 180,
                'str_description' => 'campo',
                'str_description_slug' => 'campo',
                'bol_active' => 1,
            ),
            311 => 
            array (
                'id_model' => 1312,
                'id_manufacturer' => 180,
                'str_description' => 'caribe',
                'str_description_slug' => 'caribe',
                'bol_active' => 1,
            ),
            312 => 
            array (
                'id_model' => 1313,
                'id_manufacturer' => 180,
                'str_description' => 'dmax',
                'str_description_slug' => 'dmax',
                'bol_active' => 1,
            ),
            313 => 
            array (
                'id_model' => 1314,
                'id_manufacturer' => 180,
                'str_description' => 'gemini',
                'str_description_slug' => 'gemini',
                'bol_active' => 1,
            ),
            314 => 
            array (
                'id_model' => 1315,
                'id_manufacturer' => 180,
                'str_description' => 'i mark',
                'str_description_slug' => 'i-mark',
                'bol_active' => 1,
            ),
            315 => 
            array (
                'id_model' => 1316,
                'id_manufacturer' => 180,
                'str_description' => 'midi',
                'str_description_slug' => 'midi',
                'bol_active' => 1,
            ),
            316 => 
            array (
                'id_model' => 1317,
                'id_manufacturer' => 180,
                'str_description' => 'piazza',
                'str_description_slug' => 'piazza',
                'bol_active' => 1,
            ),
            317 => 
            array (
                'id_model' => 1318,
                'id_manufacturer' => 180,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            318 => 
            array (
                'id_model' => 1319,
                'id_manufacturer' => 180,
                'str_description' => 'rodeo',
                'str_description_slug' => 'rodeo',
                'bol_active' => 1,
            ),
            319 => 
            array (
                'id_model' => 1320,
                'id_manufacturer' => 180,
                'str_description' => 'serie k',
                'str_description_slug' => 'serie-k',
                'bol_active' => 1,
            ),
            320 => 
            array (
                'id_model' => 1321,
                'id_manufacturer' => 180,
                'str_description' => 'serie n',
                'str_description_slug' => 'serie-n',
                'bol_active' => 1,
            ),
            321 => 
            array (
                'id_model' => 1322,
                'id_manufacturer' => 180,
                'str_description' => 'trooper',
                'str_description_slug' => 'trooper',
                'bol_active' => 1,
            ),
            322 => 
            array (
                'id_model' => 1323,
                'id_manufacturer' => 180,
                'str_description' => 'vehicross',
                'str_description_slug' => 'vehicross',
                'bol_active' => 1,
            ),
            323 => 
            array (
                'id_model' => 1324,
                'id_manufacturer' => 181,
                'str_description' => 'attiva nev',
                'str_description_slug' => 'attiva-nev',
                'bol_active' => 1,
            ),
            324 => 
            array (
                'id_model' => 1325,
                'id_manufacturer' => 181,
                'str_description' => 'fleet',
                'str_description_slug' => 'fleet',
                'bol_active' => 1,
            ),
            325 => 
            array (
                'id_model' => 1326,
                'id_manufacturer' => 181,
                'str_description' => 't2',
                'str_description_slug' => 't2',
                'bol_active' => 1,
            ),
            326 => 
            array (
                'id_model' => 1327,
                'id_manufacturer' => 181,
                'str_description' => 't3',
                'str_description_slug' => 't3',
                'bol_active' => 1,
            ),
            327 => 
            array (
                'id_model' => 1328,
                'id_manufacturer' => 182,
                'str_description' => 'comai ribot 35',
                'str_description_slug' => 'comai-ribot-35',
                'bol_active' => 1,
            ),
            328 => 
            array (
                'id_model' => 1329,
                'id_manufacturer' => 182,
                'str_description' => 'daily',
                'str_description_slug' => 'daily',
                'bol_active' => 1,
            ),
            329 => 
            array (
                'id_model' => 1330,
                'id_manufacturer' => 182,
                'str_description' => 'daily *',
                'str_description_slug' => 'daily-*',
                'bol_active' => 1,
            ),
            330 => 
            array (
                'id_model' => 1331,
                'id_manufacturer' => 182,
                'str_description' => 'daily 2011',
                'str_description_slug' => 'daily-2011',
                'bol_active' => 1,
            ),
            331 => 
            array (
                'id_model' => 1332,
                'id_manufacturer' => 182,
                'str_description' => 'daily 2014',
                'str_description_slug' => 'daily-2014',
                'bol_active' => 1,
            ),
            332 => 
            array (
                'id_model' => 1333,
                'id_manufacturer' => 182,
                'str_description' => 'daily city',
                'str_description_slug' => 'daily-city',
                'bol_active' => 1,
            ),
            333 => 
            array (
                'id_model' => 1334,
                'id_manufacturer' => 182,
                'str_description' => 'duty',
                'str_description_slug' => 'duty',
                'bol_active' => 1,
            ),
            334 => 
            array (
                'id_model' => 1335,
                'id_manufacturer' => 182,
                'str_description' => 'massif',
                'str_description_slug' => 'massif',
                'bol_active' => 1,
            ),
            335 => 
            array (
                'id_model' => 1336,
                'id_manufacturer' => 182,
                'str_description' => 'turbodaily',
                'str_description_slug' => 'turbodaily',
                'bol_active' => 1,
            ),
            336 => 
            array (
                'id_model' => 1337,
                'id_manufacturer' => 183,
                'str_description' => 'jeep',
                'str_description_slug' => 'jeep',
                'bol_active' => 1,
            ),
            337 => 
            array (
                'id_model' => 1338,
                'id_manufacturer' => 184,
                'str_description' => '240',
                'str_description_slug' => '240',
                'bol_active' => 1,
            ),
            338 => 
            array (
                'id_model' => 1339,
                'id_manufacturer' => 184,
                'str_description' => '420',
                'str_description_slug' => '420',
                'bol_active' => 1,
            ),
            339 => 
            array (
                'id_model' => 1340,
                'id_manufacturer' => 184,
                'str_description' => 'daimler',
                'str_description_slug' => 'daimler',
                'bol_active' => 1,
            ),
            340 => 
            array (
                'id_model' => 1341,
                'id_manufacturer' => 184,
                'str_description' => 'e type',
                'str_description_slug' => 'e-type',
                'bol_active' => 1,
            ),
            341 => 
            array (
                'id_model' => 1342,
                'id_manufacturer' => 184,
                'str_description' => 'f pace',
                'str_description_slug' => 'f-pace',
                'bol_active' => 1,
            ),
            342 => 
            array (
                'id_model' => 1343,
                'id_manufacturer' => 184,
                'str_description' => 'f type',
                'str_description_slug' => 'f-type',
                'bol_active' => 1,
            ),
            343 => 
            array (
                'id_model' => 1344,
                'id_manufacturer' => 184,
                'str_description' => 'mark ii',
                'str_description_slug' => 'mark-ii',
                'bol_active' => 1,
            ),
            344 => 
            array (
                'id_model' => 1345,
                'id_manufacturer' => 184,
                'str_description' => 'mark ix',
                'str_description_slug' => 'mark-ix',
                'bol_active' => 1,
            ),
            345 => 
            array (
                'id_model' => 1346,
                'id_manufacturer' => 184,
                'str_description' => 'mark vii',
                'str_description_slug' => 'mark-vii',
                'bol_active' => 1,
            ),
            346 => 
            array (
                'id_model' => 1347,
                'id_manufacturer' => 184,
                'str_description' => 'mk ii',
                'str_description_slug' => 'mk-ii',
                'bol_active' => 1,
            ),
            347 => 
            array (
                'id_model' => 1348,
                'id_manufacturer' => 184,
                'str_description' => 'mk iii',
                'str_description_slug' => 'mk-iii',
                'bol_active' => 1,
            ),
            348 => 
            array (
                'id_model' => 1349,
                'id_manufacturer' => 184,
                'str_description' => 'mk v',
                'str_description_slug' => 'mk-v',
                'bol_active' => 1,
            ),
            349 => 
            array (
                'id_model' => 1350,
                'id_manufacturer' => 184,
                'str_description' => 'mk x',
                'str_description_slug' => 'mk-x',
                'bol_active' => 1,
            ),
            350 => 
            array (
                'id_model' => 1351,
                'id_manufacturer' => 184,
                'str_description' => 's type',
                'str_description_slug' => 's-type',
                'bol_active' => 1,
            ),
            351 => 
            array (
                'id_model' => 1352,
                'id_manufacturer' => 184,
                'str_description' => 'saloon',
                'str_description_slug' => 'saloon',
                'bol_active' => 1,
            ),
            352 => 
            array (
                'id_model' => 1353,
                'id_manufacturer' => 184,
                'str_description' => 'sovereign',
                'str_description_slug' => 'sovereign',
                'bol_active' => 1,
            ),
            353 => 
            array (
                'id_model' => 1354,
                'id_manufacturer' => 184,
                'str_description' => 'ss',
                'str_description_slug' => 'ss',
                'bol_active' => 1,
            ),
            354 => 
            array (
                'id_model' => 1355,
                'id_manufacturer' => 184,
                'str_description' => 'ss 1',
                'str_description_slug' => 'ss-1',
                'bol_active' => 1,
            ),
            355 => 
            array (
                'id_model' => 1356,
                'id_manufacturer' => 184,
                'str_description' => 'x type',
                'str_description_slug' => 'x-type',
                'bol_active' => 1,
            ),
            356 => 
            array (
                'id_model' => 1357,
                'id_manufacturer' => 184,
                'str_description' => 'xe',
                'str_description_slug' => 'xe',
                'bol_active' => 1,
            ),
            357 => 
            array (
                'id_model' => 1358,
                'id_manufacturer' => 184,
                'str_description' => 'xf',
                'str_description_slug' => 'xf',
                'bol_active' => 1,
            ),
            358 => 
            array (
                'id_model' => 1359,
                'id_manufacturer' => 184,
                'str_description' => 'xj series',
                'str_description_slug' => 'xj-series',
                'bol_active' => 1,
            ),
            359 => 
            array (
                'id_model' => 1360,
                'id_manufacturer' => 184,
                'str_description' => 'xj12',
                'str_description_slug' => 'xj12',
                'bol_active' => 1,
            ),
            360 => 
            array (
                'id_model' => 1361,
                'id_manufacturer' => 184,
                'str_description' => 'xj6',
                'str_description_slug' => 'xj6',
                'bol_active' => 1,
            ),
            361 => 
            array (
                'id_model' => 1362,
                'id_manufacturer' => 184,
                'str_description' => 'xj8',
                'str_description_slug' => 'xj8',
                'bol_active' => 1,
            ),
            362 => 
            array (
                'id_model' => 1363,
                'id_manufacturer' => 184,
                'str_description' => 'xjr',
                'str_description_slug' => 'xjr',
                'bol_active' => 1,
            ),
            363 => 
            array (
                'id_model' => 1364,
                'id_manufacturer' => 184,
                'str_description' => 'xjr-15',
                'str_description_slug' => 'xjr-15',
                'bol_active' => 1,
            ),
            364 => 
            array (
                'id_model' => 1365,
                'id_manufacturer' => 184,
                'str_description' => 'xjr-s',
                'str_description_slug' => 'xjr-s',
                'bol_active' => 1,
            ),
            365 => 
            array (
                'id_model' => 1366,
                'id_manufacturer' => 184,
                'str_description' => 'xjs',
                'str_description_slug' => 'xjs',
                'bol_active' => 1,
            ),
            366 => 
            array (
                'id_model' => 1367,
                'id_manufacturer' => 184,
                'str_description' => 'xk',
                'str_description_slug' => 'xk',
                'bol_active' => 1,
            ),
            367 => 
            array (
                'id_model' => 1368,
                'id_manufacturer' => 184,
                'str_description' => 'xk series',
                'str_description_slug' => 'xk-series',
                'bol_active' => 1,
            ),
            368 => 
            array (
                'id_model' => 1369,
                'id_manufacturer' => 184,
                'str_description' => 'xke',
                'str_description_slug' => 'xke',
                'bol_active' => 1,
            ),
            369 => 
            array (
                'id_model' => 1370,
                'id_manufacturer' => 184,
                'str_description' => 'xkr',
                'str_description_slug' => 'xkr',
                'bol_active' => 1,
            ),
            370 => 
            array (
                'id_model' => 1371,
                'id_manufacturer' => 185,
                'str_description' => 'abaca',
                'str_description_slug' => 'abaca',
                'bol_active' => 1,
            ),
            371 => 
            array (
                'id_model' => 1372,
                'id_manufacturer' => 185,
                'str_description' => 'albizia',
                'str_description_slug' => 'albizia',
                'bol_active' => 1,
            ),
            372 => 
            array (
                'id_model' => 1373,
                'id_manufacturer' => 185,
                'str_description' => 'aloes',
                'str_description_slug' => 'aloes',
                'bol_active' => 1,
            ),
            373 => 
            array (
                'id_model' => 1374,
                'id_manufacturer' => 185,
                'str_description' => 'eke',
                'str_description_slug' => 'eke',
                'bol_active' => 1,
            ),
            374 => 
            array (
                'id_model' => 1375,
                'id_manufacturer' => 185,
                'str_description' => 'orane',
                'str_description_slug' => 'orane',
                'bol_active' => 1,
            ),
            375 => 
            array (
                'id_model' => 1376,
                'id_manufacturer' => 185,
                'str_description' => 'roxsy',
                'str_description_slug' => 'roxsy',
                'bol_active' => 1,
            ),
            376 => 
            array (
                'id_model' => 1377,
                'id_manufacturer' => 185,
                'str_description' => 'simpa',
                'str_description_slug' => 'simpa',
                'bol_active' => 1,
            ),
            377 => 
            array (
                'id_model' => 1378,
                'id_manufacturer' => 185,
                'str_description' => 'titane',
                'str_description_slug' => 'titane',
                'bol_active' => 1,
            ),
            378 => 
            array (
                'id_model' => 1379,
                'id_manufacturer' => 185,
                'str_description' => 'xheos',
                'str_description_slug' => 'xheos',
                'bol_active' => 1,
            ),
            379 => 
            array (
                'id_model' => 1380,
                'id_manufacturer' => 186,
                'str_description' => 'cherokee',
                'str_description_slug' => 'cherokee',
                'bol_active' => 1,
            ),
            380 => 
            array (
                'id_model' => 1381,
                'id_manufacturer' => 186,
                'str_description' => 'cj-5',
                'str_description_slug' => 'cj-5',
                'bol_active' => 1,
            ),
            381 => 
            array (
                'id_model' => 1382,
                'id_manufacturer' => 186,
                'str_description' => 'cj-7',
                'str_description_slug' => 'cj-7',
                'bol_active' => 1,
            ),
            382 => 
            array (
                'id_model' => 1383,
                'id_manufacturer' => 186,
                'str_description' => 'cj-8',
                'str_description_slug' => 'cj-8',
                'bol_active' => 1,
            ),
            383 => 
            array (
                'id_model' => 1384,
                'id_manufacturer' => 186,
                'str_description' => 'comanche',
                'str_description_slug' => 'comanche',
                'bol_active' => 1,
            ),
            384 => 
            array (
                'id_model' => 1385,
                'id_manufacturer' => 186,
                'str_description' => 'commander',
                'str_description_slug' => 'commander',
                'bol_active' => 1,
            ),
            385 => 
            array (
                'id_model' => 1386,
                'id_manufacturer' => 186,
                'str_description' => 'compass',
                'str_description_slug' => 'compass',
                'bol_active' => 1,
            ),
            386 => 
            array (
                'id_model' => 1387,
                'id_manufacturer' => 186,
                'str_description' => 'grand cherokee',
                'str_description_slug' => 'grand-cherokee',
                'bol_active' => 1,
            ),
            387 => 
            array (
                'id_model' => 1388,
                'id_manufacturer' => 186,
                'str_description' => 'grand wagoneer',
                'str_description_slug' => 'grand-wagoneer',
                'bol_active' => 1,
            ),
            388 => 
            array (
                'id_model' => 1389,
                'id_manufacturer' => 186,
                'str_description' => 'liberty',
                'str_description_slug' => 'liberty',
                'bol_active' => 1,
            ),
            389 => 
            array (
                'id_model' => 1390,
                'id_manufacturer' => 186,
                'str_description' => 'patriot',
                'str_description_slug' => 'patriot',
                'bol_active' => 1,
            ),
            390 => 
            array (
                'id_model' => 1391,
                'id_manufacturer' => 186,
                'str_description' => 'renegade',
                'str_description_slug' => 'renegade',
                'bol_active' => 1,
            ),
            391 => 
            array (
                'id_model' => 1392,
                'id_manufacturer' => 186,
                'str_description' => 'wagoneer',
                'str_description_slug' => 'wagoneer',
                'bol_active' => 1,
            ),
            392 => 
            array (
                'id_model' => 1393,
                'id_manufacturer' => 186,
                'str_description' => 'willys',
                'str_description_slug' => 'willys',
                'bol_active' => 1,
            ),
            393 => 
            array (
                'id_model' => 1394,
                'id_manufacturer' => 186,
                'str_description' => 'wrangler',
                'str_description_slug' => 'wrangler',
                'bol_active' => 1,
            ),
            394 => 
            array (
                'id_model' => 1395,
                'id_manufacturer' => 187,
                'str_description' => 'campeador',
                'str_description_slug' => 'campeador',
                'bol_active' => 1,
            ),
            395 => 
            array (
                'id_model' => 1396,
                'id_manufacturer' => 187,
                'str_description' => 'cj',
                'str_description_slug' => 'cj',
                'bol_active' => 1,
            ),
            396 => 
            array (
                'id_model' => 1397,
                'id_manufacturer' => 187,
                'str_description' => 'comando',
                'str_description_slug' => 'comando',
                'bol_active' => 1,
            ),
            397 => 
            array (
                'id_model' => 1398,
                'id_manufacturer' => 187,
                'str_description' => 'willys',
                'str_description_slug' => 'willys',
                'bol_active' => 1,
            ),
            398 => 
            array (
                'id_model' => 1399,
                'id_manufacturer' => 188,
                'str_description' => 'healey',
                'str_description_slug' => 'healey',
                'bol_active' => 1,
            ),
            399 => 
            array (
                'id_model' => 1400,
                'id_manufacturer' => 188,
                'str_description' => 'interceptor',
                'str_description_slug' => 'interceptor',
                'bol_active' => 1,
            ),
            400 => 
            array (
                'id_model' => 1401,
                'id_manufacturer' => 189,
                'str_description' => 'mbg',
                'str_description_slug' => 'mbg',
                'bol_active' => 1,
            ),
            401 => 
            array (
                'id_model' => 1402,
                'id_manufacturer' => 190,
                'str_description' => 'gk',
                'str_description_slug' => 'gk',
                'bol_active' => 1,
            ),
            402 => 
            array (
                'id_model' => 1403,
                'id_manufacturer' => 190,
                'str_description' => 'hf',
                'str_description_slug' => 'hf',
                'bol_active' => 1,
            ),
            403 => 
            array (
                'id_model' => 1404,
                'id_manufacturer' => 190,
                'str_description' => 'joc 600',
                'str_description_slug' => 'joc-600',
                'bol_active' => 1,
            ),
            404 => 
            array (
                'id_model' => 1405,
                'id_manufacturer' => 191,
                'str_description' => 'gator',
                'str_description_slug' => 'gator',
                'bol_active' => 1,
            ),
            405 => 
            array (
                'id_model' => 1406,
                'id_manufacturer' => 192,
                'str_description' => 'jupiter',
                'str_description_slug' => 'jupiter',
                'bol_active' => 1,
            ),
            406 => 
            array (
                'id_model' => 1407,
                'id_manufacturer' => 193,
                'str_description' => 'lince',
                'str_description_slug' => 'lince',
                'bol_active' => 1,
            ),
            407 => 
            array (
                'id_model' => 1408,
                'id_manufacturer' => 193,
                'str_description' => 'matador',
                'str_description_slug' => 'matador',
                'bol_active' => 1,
            ),
            408 => 
            array (
                'id_model' => 1409,
                'id_manufacturer' => 194,
                'str_description' => 'cj-5',
                'str_description_slug' => 'cj-5',
                'bol_active' => 1,
            ),
            409 => 
            array (
                'id_model' => 1410,
                'id_manufacturer' => 194,
                'str_description' => 'manhattan',
                'str_description_slug' => 'manhattan',
                'bol_active' => 1,
            ),
            410 => 
            array (
                'id_model' => 1411,
                'id_manufacturer' => 195,
                'str_description' => 'kd 150',
                'str_description_slug' => 'kd-150',
                'bol_active' => 1,
            ),
            411 => 
            array (
                'id_model' => 1412,
                'id_manufacturer' => 195,
                'str_description' => 'kd 250',
                'str_description_slug' => 'kd-250',
                'bol_active' => 1,
            ),
            412 => 
            array (
                'id_model' => 1413,
                'id_manufacturer' => 196,
                'str_description' => 'jipy',
                'str_description_slug' => 'jipy',
                'bol_active' => 1,
            ),
            413 => 
            array (
                'id_model' => 1414,
                'id_manufacturer' => 197,
                'str_description' => 'ab-7',
                'str_description_slug' => 'ab-7',
                'bol_active' => 1,
            ),
            414 => 
            array (
                'id_model' => 1415,
                'id_manufacturer' => 197,
                'str_description' => 'hawk ii',
                'str_description_slug' => 'hawk-ii',
                'bol_active' => 1,
            ),
            415 => 
            array (
                'id_model' => 1416,
                'id_manufacturer' => 198,
                'str_description' => 'kaf 300',
                'str_description_slug' => 'kaf-300',
                'bol_active' => 1,
            ),
            416 => 
            array (
                'id_model' => 1417,
                'id_manufacturer' => 198,
                'str_description' => 'kaf 620',
                'str_description_slug' => 'kaf-620',
                'bol_active' => 1,
            ),
            417 => 
            array (
                'id_model' => 1418,
                'id_manufacturer' => 198,
                'str_description' => 'mule',
                'str_description_slug' => 'mule',
                'bol_active' => 1,
            ),
            418 => 
            array (
                'id_model' => 1419,
                'id_manufacturer' => 198,
                'str_description' => 'teryx',
                'str_description_slug' => 'teryx',
                'bol_active' => 1,
            ),
            419 => 
            array (
                'id_model' => 1420,
                'id_manufacturer' => 199,
                'str_description' => 'el-jet 2',
                'str_description_slug' => 'el-jet-2',
                'bol_active' => 1,
            ),
            420 => 
            array (
                'id_model' => 1421,
                'id_manufacturer' => 200,
                'str_description' => 'besta',
                'str_description_slug' => 'besta',
                'bol_active' => 1,
            ),
            421 => 
            array (
                'id_model' => 1422,
                'id_manufacturer' => 200,
                'str_description' => 'ceres',
                'str_description_slug' => 'ceres',
                'bol_active' => 1,
            ),
            422 => 
            array (
                'id_model' => 1423,
                'id_manufacturer' => 200,
                'str_description' => 'clarus',
                'str_description_slug' => 'clarus',
                'bol_active' => 1,
            ),
            423 => 
            array (
                'id_model' => 1424,
                'id_manufacturer' => 200,
                'str_description' => 'elan',
                'str_description_slug' => 'elan',
                'bol_active' => 1,
            ),
            424 => 
            array (
                'id_model' => 1425,
                'id_manufacturer' => 200,
                'str_description' => 'forte',
                'str_description_slug' => 'forte',
                'bol_active' => 1,
            ),
            425 => 
            array (
                'id_model' => 1426,
                'id_manufacturer' => 200,
                'str_description' => 'frontier',
                'str_description_slug' => 'frontier',
                'bol_active' => 1,
            ),
            426 => 
            array (
                'id_model' => 1427,
                'id_manufacturer' => 200,
                'str_description' => 'joice',
                'str_description_slug' => 'joice',
                'bol_active' => 1,
            ),
            427 => 
            array (
                'id_model' => 1428,
                'id_manufacturer' => 200,
                'str_description' => 'k 2700',
                'str_description_slug' => 'k-2700',
                'bol_active' => 1,
            ),
            428 => 
            array (
                'id_model' => 1429,
                'id_manufacturer' => 200,
                'str_description' => 'k2500',
                'str_description_slug' => 'k2500',
                'bol_active' => 1,
            ),
            429 => 
            array (
                'id_model' => 1430,
                'id_manufacturer' => 200,
                'str_description' => 'k2900',
                'str_description_slug' => 'k2900',
                'bol_active' => 1,
            ),
            430 => 
            array (
                'id_model' => 1431,
                'id_manufacturer' => 200,
                'str_description' => 'leo',
                'str_description_slug' => 'leo',
                'bol_active' => 1,
            ),
            431 => 
            array (
                'id_model' => 1432,
                'id_manufacturer' => 200,
                'str_description' => 'magentis',
                'str_description_slug' => 'magentis',
                'bol_active' => 1,
            ),
            432 => 
            array (
                'id_model' => 1433,
                'id_manufacturer' => 200,
                'str_description' => 'mentor',
                'str_description_slug' => 'mentor',
                'bol_active' => 1,
            ),
            433 => 
            array (
                'id_model' => 1434,
                'id_manufacturer' => 200,
                'str_description' => 'niro',
                'str_description_slug' => 'niro',
                'bol_active' => 1,
            ),
            434 => 
            array (
                'id_model' => 1435,
                'id_manufacturer' => 200,
                'str_description' => 'opirus',
                'str_description_slug' => 'opirus',
                'bol_active' => 1,
            ),
            435 => 
            array (
                'id_model' => 1436,
                'id_manufacturer' => 200,
                'str_description' => 'optima',
                'str_description_slug' => 'optima',
                'bol_active' => 1,
            ),
            436 => 
            array (
                'id_model' => 1437,
                'id_manufacturer' => 200,
                'str_description' => 'pregio',
                'str_description_slug' => 'pregio',
                'bol_active' => 1,
            ),
            437 => 
            array (
                'id_model' => 1438,
                'id_manufacturer' => 200,
                'str_description' => 'pride',
                'str_description_slug' => 'pride',
                'bol_active' => 1,
            ),
            438 => 
            array (
                'id_model' => 1439,
                'id_manufacturer' => 200,
                'str_description' => 'retona',
                'str_description_slug' => 'retona',
                'bol_active' => 1,
            ),
            439 => 
            array (
                'id_model' => 1440,
                'id_manufacturer' => 200,
                'str_description' => 'sedona',
                'str_description_slug' => 'sedona',
                'bol_active' => 1,
            ),
            440 => 
            array (
                'id_model' => 1441,
                'id_manufacturer' => 200,
                'str_description' => 'sephia',
                'str_description_slug' => 'sephia',
                'bol_active' => 1,
            ),
            441 => 
            array (
                'id_model' => 1442,
                'id_manufacturer' => 200,
                'str_description' => 'sephia ii',
                'str_description_slug' => 'sephia-ii',
                'bol_active' => 1,
            ),
            442 => 
            array (
                'id_model' => 1443,
                'id_manufacturer' => 200,
                'str_description' => 'spectra',
                'str_description_slug' => 'spectra',
                'bol_active' => 1,
            ),
            443 => 
            array (
                'id_model' => 1444,
                'id_manufacturer' => 200,
                'str_description' => 'venga',
                'str_description_slug' => 'venga',
                'bol_active' => 1,
            ),
            444 => 
            array (
                'id_model' => 1445,
                'id_manufacturer' => 201,
                'str_description' => 'cyclone',
                'str_description_slug' => 'cyclone',
                'bol_active' => 1,
            ),
            445 => 
            array (
                'id_model' => 1446,
                'id_manufacturer' => 201,
                'str_description' => 'jeep',
                'str_description_slug' => 'jeep',
                'bol_active' => 1,
            ),
            446 => 
            array (
                'id_model' => 1447,
                'id_manufacturer' => 201,
                'str_description' => 'racer',
                'str_description_slug' => 'racer',
                'bol_active' => 1,
            ),
            447 => 
            array (
                'id_model' => 1448,
                'id_manufacturer' => 201,
                'str_description' => 'runmaster',
                'str_description_slug' => 'runmaster',
                'bol_active' => 1,
            ),
            448 => 
            array (
                'id_model' => 1449,
                'id_manufacturer' => 201,
                'str_description' => 'sahara',
                'str_description_slug' => 'sahara',
                'bol_active' => 1,
            ),
            449 => 
            array (
                'id_model' => 1450,
                'id_manufacturer' => 201,
                'str_description' => 'shark',
                'str_description_slug' => 'shark',
                'bol_active' => 1,
            ),
            450 => 
            array (
                'id_model' => 1451,
                'id_manufacturer' => 202,
                'str_description' => 'sedan',
                'str_description_slug' => 'sedan',
                'bol_active' => 1,
            ),
            451 => 
            array (
                'id_model' => 1452,
                'id_manufacturer' => 203,
                'str_description' => 'x-bow',
                'str_description_slug' => 'x-bow',
                'bol_active' => 1,
            ),
            452 => 
            array (
                'id_model' => 1453,
                'id_manufacturer' => 204,
                'str_description' => 'convoy',
                'str_description_slug' => 'convoy',
                'bol_active' => 1,
            ),
            453 => 
            array (
                'id_model' => 1454,
                'id_manufacturer' => 204,
                'str_description' => 'maxus',
                'str_description_slug' => 'maxus',
                'bol_active' => 1,
            ),
            454 => 
            array (
                'id_model' => 1455,
                'id_manufacturer' => 204,
                'str_description' => 'va',
                'str_description_slug' => 'va',
                'bol_active' => 1,
            ),
            455 => 
            array (
                'id_model' => 1456,
                'id_manufacturer' => 204,
                'str_description' => 'vd',
                'str_description_slug' => 'vd',
                'bol_active' => 1,
            ),
            456 => 
            array (
                'id_model' => 1457,
                'id_manufacturer' => 204,
                'str_description' => 'vh',
                'str_description_slug' => 'vh',
                'bol_active' => 1,
            ),
            457 => 
            array (
                'id_model' => 1458,
                'id_manufacturer' => 204,
                'str_description' => 'vs',
                'str_description_slug' => 'vs',
                'bol_active' => 1,
            ),
            458 => 
            array (
                'id_model' => 1459,
                'id_manufacturer' => 204,
                'str_description' => 'vx',
                'str_description_slug' => 'vx',
                'bol_active' => 1,
            ),
            459 => 
            array (
                'id_model' => 1460,
                'id_manufacturer' => 205,
                'str_description' => '214',
                'str_description_slug' => '214',
                'bol_active' => 1,
            ),
            460 => 
            array (
                'id_model' => 1461,
                'id_manufacturer' => 205,
                'str_description' => '1300',
                'str_description_slug' => '1300',
                'bol_active' => 1,
            ),
            461 => 
            array (
                'id_model' => 1462,
                'id_manufacturer' => 205,
                'str_description' => '1500',
                'str_description_slug' => '1500',
                'bol_active' => 1,
            ),
            462 => 
            array (
                'id_model' => 1463,
                'id_manufacturer' => 205,
                'str_description' => '2101',
                'str_description_slug' => '2101',
                'bol_active' => 1,
            ),
            463 => 
            array (
                'id_model' => 1464,
                'id_manufacturer' => 205,
                'str_description' => '2102',
                'str_description_slug' => '2102',
                'bol_active' => 1,
            ),
            464 => 
            array (
                'id_model' => 1465,
                'id_manufacturer' => 205,
                'str_description' => '2103',
                'str_description_slug' => '2103',
                'bol_active' => 1,
            ),
            465 => 
            array (
                'id_model' => 1466,
                'id_manufacturer' => 205,
                'str_description' => '2104',
                'str_description_slug' => '2104',
                'bol_active' => 1,
            ),
            466 => 
            array (
                'id_model' => 1467,
                'id_manufacturer' => 205,
                'str_description' => '2105',
                'str_description_slug' => '2105',
                'bol_active' => 1,
            ),
            467 => 
            array (
                'id_model' => 1468,
                'id_manufacturer' => 205,
                'str_description' => '2106',
                'str_description_slug' => '2106',
                'bol_active' => 1,
            ),
            468 => 
            array (
                'id_model' => 1469,
                'id_manufacturer' => 205,
                'str_description' => '2107',
                'str_description_slug' => '2107',
                'bol_active' => 1,
            ),
            469 => 
            array (
                'id_model' => 1470,
                'id_manufacturer' => 205,
                'str_description' => '2108',
                'str_description_slug' => '2108',
                'bol_active' => 1,
            ),
            470 => 
            array (
                'id_model' => 1471,
                'id_manufacturer' => 205,
                'str_description' => 'kalina',
                'str_description_slug' => 'kalina',
                'bol_active' => 1,
            ),
            471 => 
            array (
                'id_model' => 1472,
                'id_manufacturer' => 205,
                'str_description' => 'niva',
                'str_description_slug' => 'niva',
                'bol_active' => 1,
            ),
            472 => 
            array (
                'id_model' => 1473,
                'id_manufacturer' => 205,
                'str_description' => 'priora',
                'str_description_slug' => 'priora',
                'bol_active' => 1,
            ),
            473 => 
            array (
                'id_model' => 1474,
                'id_manufacturer' => 205,
                'str_description' => 'riva',
                'str_description_slug' => 'riva',
                'bol_active' => 1,
            ),
            474 => 
            array (
                'id_model' => 1475,
                'id_manufacturer' => 205,
                'str_description' => 'sagona',
                'str_description_slug' => 'sagona',
                'bol_active' => 1,
            ),
            475 => 
            array (
                'id_model' => 1476,
                'id_manufacturer' => 205,
                'str_description' => 'samara',
                'str_description_slug' => 'samara',
                'bol_active' => 1,
            ),
            476 => 
            array (
                'id_model' => 1477,
                'id_manufacturer' => 205,
                'str_description' => 'stawara 2110',
                'str_description_slug' => 'stawara-2110',
                'bol_active' => 1,
            ),
            477 => 
            array (
                'id_model' => 1478,
                'id_manufacturer' => 205,
                'str_description' => 'stawara 2111',
                'str_description_slug' => 'stawara-2111',
                'bol_active' => 1,
            ),
            478 => 
            array (
                'id_model' => 1479,
                'id_manufacturer' => 206,
                'str_description' => 'drophead',
                'str_description_slug' => 'drophead',
                'bol_active' => 1,
            ),
            479 => 
            array (
                'id_model' => 1480,
                'id_manufacturer' => 207,
                'str_description' => 'buggy',
                'str_description_slug' => 'buggy',
                'bol_active' => 1,
            ),
            480 => 
            array (
                'id_model' => 1481,
                'id_manufacturer' => 208,
                'str_description' => '350 gt',
                'str_description_slug' => '350-gt',
                'bol_active' => 1,
            ),
            481 => 
            array (
                'id_model' => 1482,
                'id_manufacturer' => 208,
                'str_description' => 'aventador',
                'str_description_slug' => 'aventador',
                'bol_active' => 1,
            ),
            482 => 
            array (
                'id_model' => 1483,
                'id_manufacturer' => 208,
                'str_description' => 'countach',
                'str_description_slug' => 'countach',
                'bol_active' => 1,
            ),
            483 => 
            array (
                'id_model' => 1484,
                'id_manufacturer' => 208,
                'str_description' => 'diablo',
                'str_description_slug' => 'diablo',
                'bol_active' => 1,
            ),
            484 => 
            array (
                'id_model' => 1485,
                'id_manufacturer' => 208,
                'str_description' => 'espada',
                'str_description_slug' => 'espada',
                'bol_active' => 1,
            ),
            485 => 
            array (
                'id_model' => 1486,
                'id_manufacturer' => 208,
                'str_description' => 'gallardo',
                'str_description_slug' => 'gallardo',
                'bol_active' => 1,
            ),
            486 => 
            array (
                'id_model' => 1487,
                'id_manufacturer' => 208,
                'str_description' => 'golf car',
                'str_description_slug' => 'golf-car',
                'bol_active' => 1,
            ),
            487 => 
            array (
                'id_model' => 1488,
                'id_manufacturer' => 208,
                'str_description' => 'huracan',
                'str_description_slug' => 'huracan',
                'bol_active' => 1,
            ),
            488 => 
            array (
                'id_model' => 1489,
                'id_manufacturer' => 208,
                'str_description' => 'jalpa',
                'str_description_slug' => 'jalpa',
                'bol_active' => 1,
            ),
            489 => 
            array (
                'id_model' => 1490,
                'id_manufacturer' => 208,
                'str_description' => 'lm 002',
                'str_description_slug' => 'lm-002',
                'bol_active' => 1,
            ),
            490 => 
            array (
                'id_model' => 1491,
                'id_manufacturer' => 208,
                'str_description' => 'miura',
                'str_description_slug' => 'miura',
                'bol_active' => 1,
            ),
            491 => 
            array (
                'id_model' => 1492,
                'id_manufacturer' => 208,
                'str_description' => 'murcielago',
                'str_description_slug' => 'murcielago',
                'bol_active' => 1,
            ),
            492 => 
            array (
                'id_model' => 1493,
                'id_manufacturer' => 208,
                'str_description' => 'sesto elemento',
                'str_description_slug' => 'sesto-elemento',
                'bol_active' => 1,
            ),
            493 => 
            array (
                'id_model' => 1494,
                'id_manufacturer' => 208,
                'str_description' => 'urraco',
                'str_description_slug' => 'urraco',
                'bol_active' => 1,
            ),
            494 => 
            array (
                'id_model' => 1495,
                'id_manufacturer' => 209,
                'str_description' => '037',
                'str_description_slug' => '037',
                'bol_active' => 1,
            ),
            495 => 
            array (
                'id_model' => 1496,
                'id_manufacturer' => 209,
                'str_description' => '2000',
                'str_description_slug' => '2000',
                'bol_active' => 1,
            ),
            496 => 
            array (
                'id_model' => 1497,
                'id_manufacturer' => 209,
                'str_description' => 'a112',
                'str_description_slug' => 'a112',
                'bol_active' => 1,
            ),
            497 => 
            array (
                'id_model' => 1498,
                'id_manufacturer' => 209,
                'str_description' => 'artena',
                'str_description_slug' => 'artena',
                'bol_active' => 1,
            ),
            498 => 
            array (
                'id_model' => 1499,
                'id_manufacturer' => 209,
                'str_description' => 'beta',
                'str_description_slug' => 'beta',
                'bol_active' => 1,
            ),
            499 => 
            array (
                'id_model' => 1500,
                'id_manufacturer' => 209,
                'str_description' => 'dedra',
                'str_description_slug' => 'dedra',
                'bol_active' => 1,
            ),
        ));
        \DB::table('automobile_models')->insert(array (
            0 => 
            array (
                'id_model' => 1501,
                'id_manufacturer' => 209,
                'str_description' => 'delta',
                'str_description_slug' => 'delta',
                'bol_active' => 1,
            ),
            1 => 
            array (
                'id_model' => 1502,
                'id_manufacturer' => 209,
                'str_description' => 'flaminia',
                'str_description_slug' => 'flaminia',
                'bol_active' => 1,
            ),
            2 => 
            array (
                'id_model' => 1503,
                'id_manufacturer' => 209,
                'str_description' => 'flavia',
                'str_description_slug' => 'flavia',
                'bol_active' => 1,
            ),
            3 => 
            array (
                'id_model' => 1504,
                'id_manufacturer' => 209,
                'str_description' => 'flavia coupe',
                'str_description_slug' => 'flavia-coupe',
                'bol_active' => 1,
            ),
            4 => 
            array (
                'id_model' => 1505,
                'id_manufacturer' => 209,
                'str_description' => 'fulvia',
                'str_description_slug' => 'fulvia',
                'bol_active' => 1,
            ),
            5 => 
            array (
                'id_model' => 1506,
                'id_manufacturer' => 209,
                'str_description' => 'gamma',
                'str_description_slug' => 'gamma',
                'bol_active' => 1,
            ),
            6 => 
            array (
                'id_model' => 1507,
                'id_manufacturer' => 209,
                'str_description' => 'hpe',
                'str_description_slug' => 'hpe',
                'bol_active' => 1,
            ),
            7 => 
            array (
                'id_model' => 1508,
                'id_manufacturer' => 209,
                'str_description' => 'k',
                'str_description_slug' => 'k',
                'bol_active' => 1,
            ),
            8 => 
            array (
                'id_model' => 1509,
                'id_manufacturer' => 209,
                'str_description' => 'lybra',
                'str_description_slug' => 'lybra',
                'bol_active' => 1,
            ),
            9 => 
            array (
                'id_model' => 1510,
                'id_manufacturer' => 209,
                'str_description' => 'montecarlo',
                'str_description_slug' => 'montecarlo',
                'bol_active' => 1,
            ),
            10 => 
            array (
                'id_model' => 1511,
                'id_manufacturer' => 209,
                'str_description' => 'musa',
                'str_description_slug' => 'musa',
                'bol_active' => 1,
            ),
            11 => 
            array (
                'id_model' => 1512,
                'id_manufacturer' => 209,
                'str_description' => 'phedra',
                'str_description_slug' => 'phedra',
                'bol_active' => 1,
            ),
            12 => 
            array (
                'id_model' => 1513,
                'id_manufacturer' => 209,
                'str_description' => 'prisma',
                'str_description_slug' => 'prisma',
                'bol_active' => 1,
            ),
            13 => 
            array (
                'id_model' => 1514,
                'id_manufacturer' => 209,
                'str_description' => 'stratos',
                'str_description_slug' => 'stratos',
                'bol_active' => 1,
            ),
            14 => 
            array (
                'id_model' => 1515,
                'id_manufacturer' => 209,
                'str_description' => 'thema',
                'str_description_slug' => 'thema',
                'bol_active' => 1,
            ),
            15 => 
            array (
                'id_model' => 1516,
                'id_manufacturer' => 209,
                'str_description' => 'thesis',
                'str_description_slug' => 'thesis',
                'bol_active' => 1,
            ),
            16 => 
            array (
                'id_model' => 1517,
                'id_manufacturer' => 209,
                'str_description' => 'trevi',
                'str_description_slug' => 'trevi',
                'bol_active' => 1,
            ),
            17 => 
            array (
                'id_model' => 1518,
                'id_manufacturer' => 209,
                'str_description' => 'voyager',
                'str_description_slug' => 'voyager',
                'bol_active' => 1,
            ),
            18 => 
            array (
                'id_model' => 1519,
                'id_manufacturer' => 209,
                'str_description' => 'y',
                'str_description_slug' => 'y',
                'bol_active' => 1,
            ),
            19 => 
            array (
                'id_model' => 1520,
                'id_manufacturer' => 209,
                'str_description' => 'y10',
                'str_description_slug' => 'y10',
                'bol_active' => 1,
            ),
            20 => 
            array (
                'id_model' => 1521,
                'id_manufacturer' => 209,
                'str_description' => 'ypsilon',
                'str_description_slug' => 'ypsilon',
                'bol_active' => 1,
            ),
            21 => 
            array (
                'id_model' => 1522,
                'id_manufacturer' => 209,
                'str_description' => 'z',
                'str_description_slug' => 'z',
                'bol_active' => 1,
            ),
            22 => 
            array (
                'id_model' => 1523,
                'id_manufacturer' => 210,
                'str_description' => '90',
                'str_description_slug' => '90',
                'bol_active' => 1,
            ),
            23 => 
            array (
                'id_model' => 1524,
                'id_manufacturer' => 210,
                'str_description' => '101 fc',
                'str_description_slug' => '101-fc',
                'bol_active' => 1,
            ),
            24 => 
            array (
                'id_model' => 1525,
                'id_manufacturer' => 210,
                'str_description' => '110',
                'str_description_slug' => '110',
                'bol_active' => 1,
            ),
            25 => 
            array (
                'id_model' => 1526,
                'id_manufacturer' => 210,
                'str_description' => '800',
                'str_description_slug' => '800',
                'bol_active' => 1,
            ),
            26 => 
            array (
                'id_model' => 1527,
                'id_manufacturer' => 210,
                'str_description' => 'defender',
                'str_description_slug' => 'defender',
                'bol_active' => 1,
            ),
            27 => 
            array (
                'id_model' => 1528,
                'id_manufacturer' => 210,
                'str_description' => 'discovery',
                'str_description_slug' => 'discovery',
                'bol_active' => 1,
            ),
            28 => 
            array (
                'id_model' => 1529,
                'id_manufacturer' => 210,
                'str_description' => 'discovery sport',
                'str_description_slug' => 'discovery-sport',
                'bol_active' => 1,
            ),
            29 => 
            array (
                'id_model' => 1530,
                'id_manufacturer' => 210,
                'str_description' => 'freelander',
                'str_description_slug' => 'freelander',
                'bol_active' => 1,
            ),
            30 => 
            array (
                'id_model' => 1531,
                'id_manufacturer' => 210,
                'str_description' => 'range rover',
                'str_description_slug' => 'range-rover',
                'bol_active' => 1,
            ),
            31 => 
            array (
                'id_model' => 1532,
                'id_manufacturer' => 210,
                'str_description' => 'range rover evoque',
                'str_description_slug' => 'range-rover-evoque',
                'bol_active' => 1,
            ),
            32 => 
            array (
                'id_model' => 1533,
                'id_manufacturer' => 210,
                'str_description' => 'range rover sport',
                'str_description_slug' => 'range-rover-sport',
                'bol_active' => 1,
            ),
            33 => 
            array (
                'id_model' => 1534,
                'id_manufacturer' => 210,
                'str_description' => 'serie i',
                'str_description_slug' => 'serie-i',
                'bol_active' => 1,
            ),
            34 => 
            array (
                'id_model' => 1535,
                'id_manufacturer' => 211,
                'str_description' => 'lqy041',
                'str_description_slug' => 'lqy041',
                'bol_active' => 1,
            ),
            35 => 
            array (
                'id_model' => 1536,
                'id_manufacturer' => 211,
                'str_description' => 'lqy081an',
                'str_description_slug' => 'lqy081an',
                'bol_active' => 1,
            ),
            36 => 
            array (
                'id_model' => 1537,
                'id_manufacturer' => 211,
                'str_description' => 'lqy121a',
                'str_description_slug' => 'lqy121a',
                'bol_active' => 1,
            ),
            37 => 
            array (
                'id_model' => 1538,
                'id_manufacturer' => 212,
                'str_description' => 'camel',
                'str_description_slug' => 'camel',
                'bol_active' => 1,
            ),
            38 => 
            array (
                'id_model' => 1539,
                'id_manufacturer' => 213,
                'str_description' => 'c',
                'str_description_slug' => 'c',
                'bol_active' => 1,
            ),
            39 => 
            array (
                'id_model' => 1540,
                'id_manufacturer' => 213,
                'str_description' => 'torpedo',
                'str_description_slug' => 'torpedo',
                'bol_active' => 1,
            ),
            40 => 
            array (
                'id_model' => 1541,
                'id_manufacturer' => 214,
                'str_description' => 'rodeo',
                'str_description_slug' => 'rodeo',
                'bol_active' => 1,
            ),
            41 => 
            array (
                'id_model' => 1542,
                'id_manufacturer' => 215,
                'str_description' => 'ct 200 h',
                'str_description_slug' => 'ct-200-h',
                'bol_active' => 1,
            ),
            42 => 
            array (
                'id_model' => 1543,
                'id_manufacturer' => 215,
                'str_description' => 'es 250',
                'str_description_slug' => 'es-250',
                'bol_active' => 1,
            ),
            43 => 
            array (
                'id_model' => 1544,
                'id_manufacturer' => 215,
                'str_description' => 'es 300',
                'str_description_slug' => 'es-300',
                'bol_active' => 1,
            ),
            44 => 
            array (
                'id_model' => 1545,
                'id_manufacturer' => 215,
                'str_description' => 'es 330',
                'str_description_slug' => 'es-330',
                'bol_active' => 1,
            ),
            45 => 
            array (
                'id_model' => 1546,
                'id_manufacturer' => 215,
                'str_description' => 'es 350',
                'str_description_slug' => 'es-350',
                'bol_active' => 1,
            ),
            46 => 
            array (
                'id_model' => 1547,
                'id_manufacturer' => 215,
                'str_description' => 'ex 350',
                'str_description_slug' => 'ex-350',
                'bol_active' => 1,
            ),
            47 => 
            array (
                'id_model' => 1548,
                'id_manufacturer' => 215,
                'str_description' => 'gs 250',
                'str_description_slug' => 'gs-250',
                'bol_active' => 1,
            ),
            48 => 
            array (
                'id_model' => 1549,
                'id_manufacturer' => 215,
                'str_description' => 'gs 300',
                'str_description_slug' => 'gs-300',
                'bol_active' => 1,
            ),
            49 => 
            array (
                'id_model' => 1550,
                'id_manufacturer' => 215,
                'str_description' => 'gs 300 h',
                'str_description_slug' => 'gs-300-h',
                'bol_active' => 1,
            ),
            50 => 
            array (
                'id_model' => 1551,
                'id_manufacturer' => 215,
                'str_description' => 'gs 350',
                'str_description_slug' => 'gs-350',
                'bol_active' => 1,
            ),
            51 => 
            array (
                'id_model' => 1552,
                'id_manufacturer' => 215,
                'str_description' => 'gs 400',
                'str_description_slug' => 'gs-400',
                'bol_active' => 1,
            ),
            52 => 
            array (
                'id_model' => 1553,
                'id_manufacturer' => 215,
                'str_description' => 'gs 430',
                'str_description_slug' => 'gs-430',
                'bol_active' => 1,
            ),
            53 => 
            array (
                'id_model' => 1554,
                'id_manufacturer' => 215,
                'str_description' => 'gs 450 h',
                'str_description_slug' => 'gs-450-h',
                'bol_active' => 1,
            ),
            54 => 
            array (
                'id_model' => 1555,
                'id_manufacturer' => 215,
                'str_description' => 'gs 460',
                'str_description_slug' => 'gs-460',
                'bol_active' => 1,
            ),
            55 => 
            array (
                'id_model' => 1556,
                'id_manufacturer' => 215,
                'str_description' => 'gs f',
                'str_description_slug' => 'gs-f',
                'bol_active' => 1,
            ),
            56 => 
            array (
                'id_model' => 1557,
                'id_manufacturer' => 215,
                'str_description' => 'gx 460',
                'str_description_slug' => 'gx-460',
                'bol_active' => 1,
            ),
            57 => 
            array (
                'id_model' => 1558,
                'id_manufacturer' => 215,
                'str_description' => 'gx 470',
                'str_description_slug' => 'gx-470',
                'bol_active' => 1,
            ),
            58 => 
            array (
                'id_model' => 1559,
                'id_manufacturer' => 215,
                'str_description' => 'is 200',
                'str_description_slug' => 'is-200',
                'bol_active' => 1,
            ),
            59 => 
            array (
                'id_model' => 1560,
                'id_manufacturer' => 215,
                'str_description' => 'is 200d',
                'str_description_slug' => 'is-200d',
                'bol_active' => 1,
            ),
            60 => 
            array (
                'id_model' => 1561,
                'id_manufacturer' => 215,
                'str_description' => 'is 220d',
                'str_description_slug' => 'is-220d',
                'bol_active' => 1,
            ),
            61 => 
            array (
                'id_model' => 1562,
                'id_manufacturer' => 215,
                'str_description' => 'is 250',
                'str_description_slug' => 'is-250',
                'bol_active' => 1,
            ),
            62 => 
            array (
                'id_model' => 1563,
                'id_manufacturer' => 215,
                'str_description' => 'is 300',
                'str_description_slug' => 'is-300',
                'bol_active' => 1,
            ),
            63 => 
            array (
                'id_model' => 1564,
                'id_manufacturer' => 215,
                'str_description' => 'is 300 h',
                'str_description_slug' => 'is-300-h',
                'bol_active' => 1,
            ),
            64 => 
            array (
                'id_model' => 1565,
                'id_manufacturer' => 215,
                'str_description' => 'is 350',
                'str_description_slug' => 'is-350',
                'bol_active' => 1,
            ),
            65 => 
            array (
                'id_model' => 1566,
                'id_manufacturer' => 215,
                'str_description' => 'is f',
                'str_description_slug' => 'is-f',
                'bol_active' => 1,
            ),
            66 => 
            array (
                'id_model' => 1567,
                'id_manufacturer' => 215,
                'str_description' => 'lc 500',
                'str_description_slug' => 'lc-500',
                'bol_active' => 1,
            ),
            67 => 
            array (
                'id_model' => 1568,
                'id_manufacturer' => 215,
                'str_description' => 'lc 500 h',
                'str_description_slug' => 'lc-500-h',
                'bol_active' => 1,
            ),
            68 => 
            array (
                'id_model' => 1569,
                'id_manufacturer' => 215,
                'str_description' => 'lfa',
                'str_description_slug' => 'lfa',
                'bol_active' => 1,
            ),
            69 => 
            array (
                'id_model' => 1570,
                'id_manufacturer' => 215,
                'str_description' => 'ls 400',
                'str_description_slug' => 'ls-400',
                'bol_active' => 1,
            ),
            70 => 
            array (
                'id_model' => 1571,
                'id_manufacturer' => 215,
                'str_description' => 'ls 430',
                'str_description_slug' => 'ls-430',
                'bol_active' => 1,
            ),
            71 => 
            array (
                'id_model' => 1572,
                'id_manufacturer' => 215,
                'str_description' => 'ls 460',
                'str_description_slug' => 'ls-460',
                'bol_active' => 1,
            ),
            72 => 
            array (
                'id_model' => 1573,
                'id_manufacturer' => 215,
                'str_description' => 'ls 600 h',
                'str_description_slug' => 'ls-600-h',
                'bol_active' => 1,
            ),
            73 => 
            array (
                'id_model' => 1574,
                'id_manufacturer' => 215,
                'str_description' => 'lx 450',
                'str_description_slug' => 'lx-450',
                'bol_active' => 1,
            ),
            74 => 
            array (
                'id_model' => 1575,
                'id_manufacturer' => 215,
                'str_description' => 'lx 470',
                'str_description_slug' => 'lx-470',
                'bol_active' => 1,
            ),
            75 => 
            array (
                'id_model' => 1576,
                'id_manufacturer' => 215,
                'str_description' => 'lx 570',
                'str_description_slug' => 'lx-570',
                'bol_active' => 1,
            ),
            76 => 
            array (
                'id_model' => 1577,
                'id_manufacturer' => 215,
                'str_description' => 'nx 200 t',
                'str_description_slug' => 'nx-200-t',
                'bol_active' => 1,
            ),
            77 => 
            array (
                'id_model' => 1578,
                'id_manufacturer' => 215,
                'str_description' => 'nx 300 h',
                'str_description_slug' => 'nx-300-h',
                'bol_active' => 1,
            ),
            78 => 
            array (
                'id_model' => 1579,
                'id_manufacturer' => 215,
                'str_description' => 'rc 300 h',
                'str_description_slug' => 'rc-300-h',
                'bol_active' => 1,
            ),
            79 => 
            array (
                'id_model' => 1580,
                'id_manufacturer' => 215,
                'str_description' => 'rc f',
                'str_description_slug' => 'rc-f',
                'bol_active' => 1,
            ),
            80 => 
            array (
                'id_model' => 1581,
                'id_manufacturer' => 215,
                'str_description' => 'rx 300',
                'str_description_slug' => 'rx-300',
                'bol_active' => 1,
            ),
            81 => 
            array (
                'id_model' => 1582,
                'id_manufacturer' => 215,
                'str_description' => 'rx 330',
                'str_description_slug' => 'rx-330',
                'bol_active' => 1,
            ),
            82 => 
            array (
                'id_model' => 1583,
                'id_manufacturer' => 215,
                'str_description' => 'rx 350',
                'str_description_slug' => 'rx-350',
                'bol_active' => 1,
            ),
            83 => 
            array (
                'id_model' => 1584,
                'id_manufacturer' => 215,
                'str_description' => 'rx 400 h',
                'str_description_slug' => 'rx-400-h',
                'bol_active' => 1,
            ),
            84 => 
            array (
                'id_model' => 1585,
                'id_manufacturer' => 215,
                'str_description' => 'rx 450 h',
                'str_description_slug' => 'rx-450-h',
                'bol_active' => 1,
            ),
            85 => 
            array (
                'id_model' => 1586,
                'id_manufacturer' => 215,
                'str_description' => 'sc 300',
                'str_description_slug' => 'sc-300',
                'bol_active' => 1,
            ),
            86 => 
            array (
                'id_model' => 1587,
                'id_manufacturer' => 215,
                'str_description' => 'sc 400',
                'str_description_slug' => 'sc-400',
                'bol_active' => 1,
            ),
            87 => 
            array (
                'id_model' => 1588,
                'id_manufacturer' => 215,
                'str_description' => 'sc 430',
                'str_description_slug' => 'sc-430',
                'bol_active' => 1,
            ),
            88 => 
            array (
                'id_model' => 1589,
                'id_manufacturer' => 216,
                'str_description' => '1.3',
                'str_description_slug' => '1.3',
                'bol_active' => 1,
            ),
            89 => 
            array (
                'id_model' => 1590,
                'id_manufacturer' => 216,
                'str_description' => 'mg-b',
                'str_description_slug' => 'mg-b',
                'bol_active' => 1,
            ),
            90 => 
            array (
                'id_model' => 1591,
                'id_manufacturer' => 216,
                'str_description' => 'mini',
                'str_description_slug' => 'mini',
                'bol_active' => 1,
            ),
            91 => 
            array (
                'id_model' => 1592,
                'id_manufacturer' => 216,
                'str_description' => 'tr-6',
                'str_description_slug' => 'tr-6',
                'bol_active' => 1,
            ),
            92 => 
            array (
                'id_model' => 1593,
                'id_manufacturer' => 216,
                'str_description' => 'va',
                'str_description_slug' => 'va',
                'bol_active' => 1,
            ),
            93 => 
            array (
                'id_model' => 1594,
                'id_manufacturer' => 217,
                'str_description' => 'ambra',
                'str_description_slug' => 'ambra',
                'bol_active' => 1,
            ),
            94 => 
            array (
                'id_model' => 1595,
                'id_manufacturer' => 217,
                'str_description' => 'be sun',
                'str_description_slug' => 'be-sun',
                'bol_active' => 1,
            ),
            95 => 
            array (
                'id_model' => 1596,
                'id_manufacturer' => 217,
                'str_description' => 'be truck',
                'str_description_slug' => 'be-truck',
                'bol_active' => 1,
            ),
            96 => 
            array (
                'id_model' => 1597,
                'id_manufacturer' => 217,
                'str_description' => 'be two',
                'str_description_slug' => 'be-two',
                'bol_active' => 1,
            ),
            97 => 
            array (
                'id_model' => 1598,
                'id_manufacturer' => 217,
                'str_description' => 'be up',
                'str_description_slug' => 'be-up',
                'bol_active' => 1,
            ),
            98 => 
            array (
                'id_model' => 1599,
                'id_manufacturer' => 217,
                'str_description' => 'due',
                'str_description_slug' => 'due',
                'bol_active' => 1,
            ),
            99 => 
            array (
                'id_model' => 1600,
                'id_manufacturer' => 217,
                'str_description' => 'flex',
                'str_description_slug' => 'flex',
                'bol_active' => 1,
            ),
            100 => 
            array (
                'id_model' => 1601,
                'id_manufacturer' => 217,
                'str_description' => 'ixo',
                'str_description_slug' => 'ixo',
                'bol_active' => 1,
            ),
            101 => 
            array (
                'id_model' => 1602,
                'id_manufacturer' => 217,
                'str_description' => 'js',
                'str_description_slug' => 'js',
                'bol_active' => 1,
            ),
            102 => 
            array (
                'id_model' => 1603,
                'id_manufacturer' => 217,
                'str_description' => 'nova',
                'str_description_slug' => 'nova',
                'bol_active' => 1,
            ),
            103 => 
            array (
                'id_model' => 1604,
                'id_manufacturer' => 217,
                'str_description' => 'optima 2',
                'str_description_slug' => 'optima-2',
                'bol_active' => 1,
            ),
            104 => 
            array (
                'id_model' => 1605,
                'id_manufacturer' => 217,
                'str_description' => 'optima 4',
                'str_description_slug' => 'optima-4',
                'bol_active' => 1,
            ),
            105 => 
            array (
                'id_model' => 1606,
                'id_manufacturer' => 217,
                'str_description' => 'optimax',
                'str_description_slug' => 'optimax',
                'bol_active' => 1,
            ),
            106 => 
            array (
                'id_model' => 1607,
                'id_manufacturer' => 217,
                'str_description' => 'prima',
                'str_description_slug' => 'prima',
                'bol_active' => 1,
            ),
            107 => 
            array (
                'id_model' => 1608,
                'id_manufacturer' => 217,
                'str_description' => 'x-pro',
                'str_description_slug' => 'x-pro',
                'bol_active' => 1,
            ),
            108 => 
            array (
                'id_model' => 1609,
                'id_manufacturer' => 217,
                'str_description' => 'x-too',
                'str_description_slug' => 'x-too',
                'bol_active' => 1,
            ),
            109 => 
            array (
                'id_model' => 1610,
                'id_manufacturer' => 217,
                'str_description' => 'zenit',
                'str_description_slug' => 'zenit',
                'bol_active' => 1,
            ),
            110 => 
            array (
                'id_model' => 1611,
                'id_manufacturer' => 218,
                'str_description' => 'aviator',
                'str_description_slug' => 'aviator',
                'bol_active' => 1,
            ),
            111 => 
            array (
                'id_model' => 1612,
                'id_manufacturer' => 218,
                'str_description' => 'brunn victoria',
                'str_description_slug' => 'brunn-victoria',
                'bol_active' => 1,
            ),
            112 => 
            array (
                'id_model' => 1613,
                'id_manufacturer' => 218,
                'str_description' => 'capri',
                'str_description_slug' => 'capri',
                'bol_active' => 1,
            ),
            113 => 
            array (
                'id_model' => 1614,
                'id_manufacturer' => 218,
                'str_description' => 'cartier',
                'str_description_slug' => 'cartier',
                'bol_active' => 1,
            ),
            114 => 
            array (
                'id_model' => 1615,
                'id_manufacturer' => 218,
                'str_description' => 'continental',
                'str_description_slug' => 'continental',
                'bol_active' => 1,
            ),
            115 => 
            array (
                'id_model' => 1616,
                'id_manufacturer' => 218,
                'str_description' => 'cougar',
                'str_description_slug' => 'cougar',
                'bol_active' => 1,
            ),
            116 => 
            array (
                'id_model' => 1617,
                'id_manufacturer' => 218,
                'str_description' => 'deville',
                'str_description_slug' => 'deville',
                'bol_active' => 1,
            ),
            117 => 
            array (
                'id_model' => 1618,
                'id_manufacturer' => 218,
                'str_description' => 'gran marquis',
                'str_description_slug' => 'gran-marquis',
                'bol_active' => 1,
            ),
            118 => 
            array (
                'id_model' => 1619,
                'id_manufacturer' => 218,
                'str_description' => 'l',
                'str_description_slug' => 'l',
                'bol_active' => 1,
            ),
            119 => 
            array (
                'id_model' => 1620,
                'id_manufacturer' => 218,
                'str_description' => 'ls',
                'str_description_slug' => 'ls',
                'bol_active' => 1,
            ),
            120 => 
            array (
                'id_model' => 1621,
                'id_manufacturer' => 218,
                'str_description' => 'mark iv',
                'str_description_slug' => 'mark-iv',
                'bol_active' => 1,
            ),
            121 => 
            array (
                'id_model' => 1622,
                'id_manufacturer' => 218,
                'str_description' => 'mark vii',
                'str_description_slug' => 'mark-vii',
                'bol_active' => 1,
            ),
            122 => 
            array (
                'id_model' => 1623,
                'id_manufacturer' => 218,
                'str_description' => 'mark viii',
                'str_description_slug' => 'mark-viii',
                'bol_active' => 1,
            ),
            123 => 
            array (
                'id_model' => 1624,
                'id_manufacturer' => 218,
                'str_description' => 'mkt',
                'str_description_slug' => 'mkt',
                'bol_active' => 1,
            ),
            124 => 
            array (
                'id_model' => 1625,
                'id_manufacturer' => 218,
                'str_description' => 'mkx',
                'str_description_slug' => 'mkx',
                'bol_active' => 1,
            ),
            125 => 
            array (
                'id_model' => 1626,
                'id_manufacturer' => 218,
                'str_description' => 'navigator',
                'str_description_slug' => 'navigator',
                'bol_active' => 1,
            ),
            126 => 
            array (
                'id_model' => 1627,
                'id_manufacturer' => 218,
                'str_description' => 'oel series',
                'str_description_slug' => 'oel-series',
                'bol_active' => 1,
            ),
            127 => 
            array (
                'id_model' => 1628,
                'id_manufacturer' => 218,
                'str_description' => 'sable',
                'str_description_slug' => 'sable',
                'bol_active' => 1,
            ),
            128 => 
            array (
                'id_model' => 1629,
                'id_manufacturer' => 218,
                'str_description' => 'st tropez',
                'str_description_slug' => 'st-tropez',
                'bol_active' => 1,
            ),
            129 => 
            array (
                'id_model' => 1630,
                'id_manufacturer' => 218,
                'str_description' => 'stageway',
                'str_description_slug' => 'stageway',
                'bol_active' => 1,
            ),
            130 => 
            array (
                'id_model' => 1631,
                'id_manufacturer' => 218,
                'str_description' => 'town car',
                'str_description_slug' => 'town-car',
                'bol_active' => 1,
            ),
            131 => 
            array (
                'id_model' => 1632,
                'id_manufacturer' => 218,
                'str_description' => 'versailles',
                'str_description_slug' => 'versailles',
                'bol_active' => 1,
            ),
            132 => 
            array (
                'id_model' => 1633,
                'id_manufacturer' => 218,
                'str_description' => 'zephyr',
                'str_description_slug' => 'zephyr',
                'bol_active' => 1,
            ),
            133 => 
            array (
                'id_model' => 1634,
                'id_manufacturer' => 219,
                'str_description' => 'carry',
                'str_description_slug' => 'carry',
                'bol_active' => 1,
            ),
            134 => 
            array (
                'id_model' => 1635,
                'id_manufacturer' => 219,
                'str_description' => 'cuv',
                'str_description_slug' => 'cuv',
                'bol_active' => 1,
            ),
            135 => 
            array (
                'id_model' => 1636,
                'id_manufacturer' => 220,
                'str_description' => 'little4',
                'str_description_slug' => 'little4',
                'bol_active' => 1,
            ),
            136 => 
            array (
                'id_model' => 1637,
                'id_manufacturer' => 221,
                'str_description' => 'lp',
                'str_description_slug' => 'lp',
                'bol_active' => 1,
            ),
            137 => 
            array (
                'id_model' => 1638,
                'id_manufacturer' => 222,
                'str_description' => '340 r',
                'str_description_slug' => '340-r',
                'bol_active' => 1,
            ),
            138 => 
            array (
                'id_model' => 1639,
                'id_manufacturer' => 222,
                'str_description' => 'elan',
                'str_description_slug' => 'elan',
                'bol_active' => 1,
            ),
            139 => 
            array (
                'id_model' => 1640,
                'id_manufacturer' => 222,
                'str_description' => 'elise',
                'str_description_slug' => 'elise',
                'bol_active' => 1,
            ),
            140 => 
            array (
                'id_model' => 1641,
                'id_manufacturer' => 222,
                'str_description' => 'esprit',
                'str_description_slug' => 'esprit',
                'bol_active' => 1,
            ),
            141 => 
            array (
                'id_model' => 1642,
                'id_manufacturer' => 222,
                'str_description' => 'europa',
                'str_description_slug' => 'europa',
                'bol_active' => 1,
            ),
            142 => 
            array (
                'id_model' => 1643,
                'id_manufacturer' => 222,
                'str_description' => 'evora',
                'str_description_slug' => 'evora',
                'bol_active' => 1,
            ),
            143 => 
            array (
                'id_model' => 1644,
                'id_manufacturer' => 222,
                'str_description' => 'excel',
                'str_description_slug' => 'excel',
                'bol_active' => 1,
            ),
            144 => 
            array (
                'id_model' => 1645,
                'id_manufacturer' => 222,
                'str_description' => 'exige',
                'str_description_slug' => 'exige',
                'bol_active' => 1,
            ),
            145 => 
            array (
                'id_model' => 1646,
                'id_manufacturer' => 222,
                'str_description' => 'seven',
                'str_description_slug' => 'seven',
                'bol_active' => 1,
            ),
            146 => 
            array (
                'id_model' => 1647,
                'id_manufacturer' => 223,
                'str_description' => 'coventry',
                'str_description_slug' => 'coventry',
                'bol_active' => 1,
            ),
            147 => 
            array (
                'id_model' => 1648,
                'id_manufacturer' => 223,
                'str_description' => 'fairway',
                'str_description_slug' => 'fairway',
                'bol_active' => 1,
            ),
            148 => 
            array (
                'id_model' => 1649,
                'id_manufacturer' => 223,
                'str_description' => 'fx4',
                'str_description_slug' => 'fx4',
                'bol_active' => 1,
            ),
            149 => 
            array (
                'id_model' => 1650,
                'id_manufacturer' => 223,
                'str_description' => 'taxi',
                'str_description_slug' => 'taxi',
                'bol_active' => 1,
            ),
            150 => 
            array (
                'id_model' => 1651,
                'id_manufacturer' => 224,
                'str_description' => 'bolero',
                'str_description_slug' => 'bolero',
                'bol_active' => 1,
            ),
            151 => 
            array (
                'id_model' => 1652,
                'id_manufacturer' => 224,
                'str_description' => 'cj',
                'str_description_slug' => 'cj',
                'bol_active' => 1,
            ),
            152 => 
            array (
                'id_model' => 1653,
                'id_manufacturer' => 224,
                'str_description' => 'cj 340',
                'str_description_slug' => 'cj-340',
                'bol_active' => 1,
            ),
            153 => 
            array (
                'id_model' => 1654,
                'id_manufacturer' => 224,
                'str_description' => 'cj 540',
                'str_description_slug' => 'cj-540',
                'bol_active' => 1,
            ),
            154 => 
            array (
                'id_model' => 1655,
                'id_manufacturer' => 224,
                'str_description' => 'genio',
                'str_description_slug' => 'genio',
                'bol_active' => 1,
            ),
            155 => 
            array (
                'id_model' => 1656,
                'id_manufacturer' => 224,
                'str_description' => 'goa',
                'str_description_slug' => 'goa',
                'bol_active' => 1,
            ),
            156 => 
            array (
                'id_model' => 1657,
                'id_manufacturer' => 224,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            157 => 
            array (
                'id_model' => 1658,
                'id_manufacturer' => 224,
                'str_description' => 'quanto',
                'str_description_slug' => 'quanto',
                'bol_active' => 1,
            ),
            158 => 
            array (
                'id_model' => 1659,
                'id_manufacturer' => 224,
                'str_description' => 'thar',
                'str_description_slug' => 'thar',
                'bol_active' => 1,
            ),
            159 => 
            array (
                'id_model' => 1660,
                'id_manufacturer' => 224,
                'str_description' => 'xuv 500',
                'str_description_slug' => 'xuv-500',
                'bol_active' => 1,
            ),
            160 => 
            array (
                'id_model' => 1661,
                'id_manufacturer' => 225,
                'str_description' => 'l 2000',
                'str_description_slug' => 'l-2000',
                'bol_active' => 1,
            ),
            161 => 
            array (
                'id_model' => 1662,
                'id_manufacturer' => 226,
                'str_description' => 'diana',
                'str_description_slug' => 'diana',
                'bol_active' => 1,
            ),
            162 => 
            array (
                'id_model' => 1663,
                'id_manufacturer' => 226,
                'str_description' => 'generosa',
                'str_description_slug' => 'generosa',
                'bol_active' => 1,
            ),
            163 => 
            array (
                'id_model' => 1664,
                'id_manufacturer' => 227,
                'str_description' => 'gt',
                'str_description_slug' => 'gt',
                'bol_active' => 1,
            ),
            164 => 
            array (
                'id_model' => 1665,
                'id_manufacturer' => 227,
                'str_description' => 'lm',
                'str_description_slug' => 'lm',
                'bol_active' => 1,
            ),
            165 => 
            array (
                'id_model' => 1666,
                'id_manufacturer' => 227,
                'str_description' => 'mantara',
                'str_description_slug' => 'mantara',
                'bol_active' => 1,
            ),
            166 => 
            array (
                'id_model' => 1667,
                'id_manufacturer' => 227,
                'str_description' => 'mantis',
                'str_description_slug' => 'mantis',
                'bol_active' => 1,
            ),
            167 => 
            array (
                'id_model' => 1668,
                'id_manufacturer' => 227,
                'str_description' => 'mantula',
                'str_description_slug' => 'mantula',
                'bol_active' => 1,
            ),
            168 => 
            array (
                'id_model' => 1669,
                'id_manufacturer' => 227,
                'str_description' => 'rush',
                'str_description_slug' => 'rush',
                'bol_active' => 1,
            ),
            169 => 
            array (
                'id_model' => 1670,
                'id_manufacturer' => 228,
                'str_description' => '325',
                'str_description_slug' => '325',
                'bol_active' => 1,
            ),
            170 => 
            array (
                'id_model' => 1671,
                'id_manufacturer' => 228,
                'str_description' => 'dxt',
                'str_description_slug' => 'dxt',
                'bol_active' => 1,
            ),
            171 => 
            array (
                'id_model' => 1672,
                'id_manufacturer' => 229,
                'str_description' => 'cabrio',
                'str_description_slug' => 'cabrio',
                'bol_active' => 1,
            ),
            172 => 
            array (
                'id_model' => 1673,
                'id_manufacturer' => 229,
                'str_description' => 'roadster',
                'str_description_slug' => 'roadster',
                'bol_active' => 1,
            ),
            173 => 
            array (
                'id_model' => 1674,
                'id_manufacturer' => 230,
                'str_description' => '222',
                'str_description_slug' => '222',
                'bol_active' => 1,
            ),
            174 => 
            array (
                'id_model' => 1675,
                'id_manufacturer' => 230,
                'str_description' => '224',
                'str_description_slug' => '224',
                'bol_active' => 1,
            ),
            175 => 
            array (
                'id_model' => 1676,
                'id_manufacturer' => 230,
                'str_description' => '228',
                'str_description_slug' => '228',
                'bol_active' => 1,
            ),
            176 => 
            array (
                'id_model' => 1677,
                'id_manufacturer' => 230,
                'str_description' => '420',
                'str_description_slug' => '420',
                'bol_active' => 1,
            ),
            177 => 
            array (
                'id_model' => 1678,
                'id_manufacturer' => 230,
                'str_description' => '422',
                'str_description_slug' => '422',
                'bol_active' => 1,
            ),
            178 => 
            array (
                'id_model' => 1679,
                'id_manufacturer' => 230,
                'str_description' => '425',
                'str_description_slug' => '425',
                'bol_active' => 1,
            ),
            179 => 
            array (
                'id_model' => 1680,
                'id_manufacturer' => 230,
                'str_description' => '430',
                'str_description_slug' => '430',
                'bol_active' => 1,
            ),
            180 => 
            array (
                'id_model' => 1681,
                'id_manufacturer' => 230,
                'str_description' => '3200 gt',
                'str_description_slug' => '3200-gt',
                'bol_active' => 1,
            ),
            181 => 
            array (
                'id_model' => 1682,
                'id_manufacturer' => 230,
                'str_description' => '3500 gt',
                'str_description_slug' => '3500-gt',
                'bol_active' => 1,
            ),
            182 => 
            array (
                'id_model' => 1683,
                'id_manufacturer' => 230,
                'str_description' => '3700 gt',
                'str_description_slug' => '3700-gt',
                'bol_active' => 1,
            ),
            183 => 
            array (
                'id_model' => 1684,
                'id_manufacturer' => 230,
                'str_description' => 'biturbo',
                'str_description_slug' => 'biturbo',
                'bol_active' => 1,
            ),
            184 => 
            array (
                'id_model' => 1685,
                'id_manufacturer' => 230,
                'str_description' => 'biturbo spyder',
                'str_description_slug' => 'biturbo-spyder',
                'bol_active' => 1,
            ),
            185 => 
            array (
                'id_model' => 1686,
                'id_manufacturer' => 230,
                'str_description' => 'bora',
                'str_description_slug' => 'bora',
                'bol_active' => 1,
            ),
            186 => 
            array (
                'id_model' => 1687,
                'id_manufacturer' => 230,
                'str_description' => 'coupe',
                'str_description_slug' => 'coupe',
                'bol_active' => 1,
            ),
            187 => 
            array (
                'id_model' => 1688,
                'id_manufacturer' => 230,
                'str_description' => 'ghibli',
                'str_description_slug' => 'ghibli',
                'bol_active' => 1,
            ),
            188 => 
            array (
                'id_model' => 1689,
                'id_manufacturer' => 230,
                'str_description' => 'grancabrio',
                'str_description_slug' => 'grancabrio',
                'bol_active' => 1,
            ),
            189 => 
            array (
                'id_model' => 1690,
                'id_manufacturer' => 230,
                'str_description' => 'granturismo',
                'str_description_slug' => 'granturismo',
                'bol_active' => 1,
            ),
            190 => 
            array (
                'id_model' => 1691,
                'id_manufacturer' => 230,
                'str_description' => 'indy',
                'str_description_slug' => 'indy',
                'bol_active' => 1,
            ),
            191 => 
            array (
                'id_model' => 1692,
                'id_manufacturer' => 230,
                'str_description' => 'khamsin',
                'str_description_slug' => 'khamsin',
                'bol_active' => 1,
            ),
            192 => 
            array (
                'id_model' => 1693,
                'id_manufacturer' => 230,
                'str_description' => 'kylami',
                'str_description_slug' => 'kylami',
                'bol_active' => 1,
            ),
            193 => 
            array (
                'id_model' => 1694,
                'id_manufacturer' => 230,
                'str_description' => 'levante',
                'str_description_slug' => 'levante',
                'bol_active' => 1,
            ),
            194 => 
            array (
                'id_model' => 1695,
                'id_manufacturer' => 230,
                'str_description' => 'merak',
                'str_description_slug' => 'merak',
                'bol_active' => 1,
            ),
            195 => 
            array (
                'id_model' => 1696,
                'id_manufacturer' => 230,
                'str_description' => 'mistral',
                'str_description_slug' => 'mistral',
                'bol_active' => 1,
            ),
            196 => 
            array (
                'id_model' => 1697,
                'id_manufacturer' => 230,
                'str_description' => 'quattroporte',
                'str_description_slug' => 'quattroporte',
                'bol_active' => 1,
            ),
            197 => 
            array (
                'id_model' => 1698,
                'id_manufacturer' => 230,
                'str_description' => 'royale',
                'str_description_slug' => 'royale',
                'bol_active' => 1,
            ),
            198 => 
            array (
                'id_model' => 1699,
                'id_manufacturer' => 230,
                'str_description' => 'shamal',
                'str_description_slug' => 'shamal',
                'bol_active' => 1,
            ),
            199 => 
            array (
                'id_model' => 1700,
                'id_manufacturer' => 230,
                'str_description' => 'spyder',
                'str_description_slug' => 'spyder',
                'bol_active' => 1,
            ),
            200 => 
            array (
                'id_model' => 1701,
                'id_manufacturer' => 231,
                'str_description' => 'alsace',
                'str_description_slug' => 'alsace',
                'bol_active' => 1,
            ),
            201 => 
            array (
                'id_model' => 1702,
                'id_manufacturer' => 232,
                'str_description' => 'bagheera',
                'str_description_slug' => 'bagheera',
                'bol_active' => 1,
            ),
            202 => 
            array (
                'id_model' => 1703,
                'id_manufacturer' => 232,
                'str_description' => 'espace',
                'str_description_slug' => 'espace',
                'bol_active' => 1,
            ),
            203 => 
            array (
                'id_model' => 1704,
                'id_manufacturer' => 232,
                'str_description' => 'j 11',
                'str_description_slug' => 'j-11',
                'bol_active' => 1,
            ),
            204 => 
            array (
                'id_model' => 1705,
                'id_manufacturer' => 232,
                'str_description' => 'jet 6',
                'str_description_slug' => 'jet-6',
                'bol_active' => 1,
            ),
            205 => 
            array (
                'id_model' => 1706,
                'id_manufacturer' => 232,
                'str_description' => 'm530',
                'str_description_slug' => 'm530',
                'bol_active' => 1,
            ),
            206 => 
            array (
                'id_model' => 1707,
                'id_manufacturer' => 232,
                'str_description' => 'murena',
                'str_description_slug' => 'murena',
                'bol_active' => 1,
            ),
            207 => 
            array (
                'id_model' => 1708,
                'id_manufacturer' => 232,
                'str_description' => 'rancho',
                'str_description_slug' => 'rancho',
                'bol_active' => 1,
            ),
            208 => 
            array (
                'id_model' => 1709,
                'id_manufacturer' => 232,
                'str_description' => 'sports jet',
                'str_description_slug' => 'sports-jet',
                'bol_active' => 1,
            ),
            209 => 
            array (
                'id_model' => 1710,
                'id_manufacturer' => 233,
                'str_description' => 'maybach',
                'str_description_slug' => 'maybach',
                'bol_active' => 1,
            ),
            210 => 
            array (
                'id_model' => 1711,
                'id_manufacturer' => 234,
                'str_description' => '121',
                'str_description_slug' => '121',
                'bol_active' => 1,
            ),
            211 => 
            array (
                'id_model' => 1712,
                'id_manufacturer' => 234,
                'str_description' => '616',
                'str_description_slug' => '616',
                'bol_active' => 1,
            ),
            212 => 
            array (
                'id_model' => 1713,
                'id_manufacturer' => 234,
                'str_description' => '808',
                'str_description_slug' => '808',
                'bol_active' => 1,
            ),
            213 => 
            array (
                'id_model' => 1714,
                'id_manufacturer' => 234,
                'str_description' => '818',
                'str_description_slug' => '818',
                'bol_active' => 1,
            ),
            214 => 
            array (
                'id_model' => 1715,
                'id_manufacturer' => 234,
                'str_description' => '929',
                'str_description_slug' => '929',
                'bol_active' => 1,
            ),
            215 => 
            array (
                'id_model' => 1716,
                'id_manufacturer' => 234,
                'str_description' => 'b',
                'str_description_slug' => 'b',
                'bol_active' => 1,
            ),
            216 => 
            array (
                'id_model' => 1717,
                'id_manufacturer' => 234,
                'str_description' => 'bongo',
                'str_description_slug' => 'bongo',
                'bol_active' => 1,
            ),
            217 => 
            array (
                'id_model' => 1718,
                'id_manufacturer' => 234,
                'str_description' => 'bt-50',
                'str_description_slug' => 'bt-50',
                'bol_active' => 1,
            ),
            218 => 
            array (
                'id_model' => 1719,
                'id_manufacturer' => 234,
                'str_description' => 'cx-3',
                'str_description_slug' => 'cx-3',
                'bol_active' => 1,
            ),
            219 => 
            array (
                'id_model' => 1720,
                'id_manufacturer' => 234,
                'str_description' => 'cx-5',
                'str_description_slug' => 'cx-5',
                'bol_active' => 1,
            ),
            220 => 
            array (
                'id_model' => 1721,
                'id_manufacturer' => 234,
                'str_description' => 'cx-9',
                'str_description_slug' => 'cx-9',
                'bol_active' => 1,
            ),
            221 => 
            array (
                'id_model' => 1722,
                'id_manufacturer' => 234,
                'str_description' => 'demio',
                'str_description_slug' => 'demio',
                'bol_active' => 1,
            ),
            222 => 
            array (
                'id_model' => 1723,
                'id_manufacturer' => 234,
                'str_description' => 'e 2000',
                'str_description_slug' => 'e-2000',
                'bol_active' => 1,
            ),
            223 => 
            array (
                'id_model' => 1724,
                'id_manufacturer' => 234,
                'str_description' => 'e 2200',
                'str_description_slug' => 'e-2200',
                'bol_active' => 1,
            ),
            224 => 
            array (
                'id_model' => 1725,
                'id_manufacturer' => 234,
                'str_description' => 'econovan',
                'str_description_slug' => 'econovan',
                'bol_active' => 1,
            ),
            225 => 
            array (
                'id_model' => 1726,
                'id_manufacturer' => 234,
                'str_description' => 'kca bus',
                'str_description_slug' => 'kca-bus',
                'bol_active' => 1,
            ),
            226 => 
            array (
                'id_model' => 1727,
                'id_manufacturer' => 234,
                'str_description' => 'mpv',
                'str_description_slug' => 'mpv',
                'bol_active' => 1,
            ),
            227 => 
            array (
                'id_model' => 1728,
                'id_manufacturer' => 234,
                'str_description' => 'mx-6',
                'str_description_slug' => 'mx-6',
                'bol_active' => 1,
            ),
            228 => 
            array (
                'id_model' => 1729,
                'id_manufacturer' => 234,
                'str_description' => 'navajo',
                'str_description_slug' => 'navajo',
                'bol_active' => 1,
            ),
            229 => 
            array (
                'id_model' => 1730,
                'id_manufacturer' => 234,
                'str_description' => 'premacy',
                'str_description_slug' => 'premacy',
                'bol_active' => 1,
            ),
            230 => 
            array (
                'id_model' => 1731,
                'id_manufacturer' => 234,
                'str_description' => 'probe',
                'str_description_slug' => 'probe',
                'bol_active' => 1,
            ),
            231 => 
            array (
                'id_model' => 1732,
                'id_manufacturer' => 234,
                'str_description' => 'protege',
                'str_description_slug' => 'protege',
                'bol_active' => 1,
            ),
            232 => 
            array (
                'id_model' => 1733,
                'id_manufacturer' => 234,
                'str_description' => 'rx-2',
                'str_description_slug' => 'rx-2',
                'bol_active' => 1,
            ),
            233 => 
            array (
                'id_model' => 1734,
                'id_manufacturer' => 234,
                'str_description' => 'rx-4',
                'str_description_slug' => 'rx-4',
                'bol_active' => 1,
            ),
            234 => 
            array (
                'id_model' => 1735,
                'id_manufacturer' => 234,
                'str_description' => 'rx-7',
                'str_description_slug' => 'rx-7',
                'bol_active' => 1,
            ),
            235 => 
            array (
                'id_model' => 1736,
                'id_manufacturer' => 234,
                'str_description' => 't 2500',
                'str_description_slug' => 't-2500',
                'bol_active' => 1,
            ),
            236 => 
            array (
                'id_model' => 1737,
                'id_manufacturer' => 234,
                'str_description' => 'tribute',
                'str_description_slug' => 'tribute',
                'bol_active' => 1,
            ),
            237 => 
            array (
                'id_model' => 1738,
                'id_manufacturer' => 234,
                'str_description' => 'xedos 6',
                'str_description_slug' => 'xedos-6',
                'bol_active' => 1,
            ),
            238 => 
            array (
                'id_model' => 1739,
                'id_manufacturer' => 234,
                'str_description' => 'xedos 9',
                'str_description_slug' => 'xedos-9',
                'bol_active' => 1,
            ),
            239 => 
            array (
                'id_model' => 1740,
                'id_manufacturer' => 235,
                'str_description' => 'smart',
                'str_description_slug' => 'smart',
                'bol_active' => 1,
            ),
            240 => 
            array (
                'id_model' => 1741,
                'id_manufacturer' => 236,
                'str_description' => '650s',
                'str_description_slug' => '650s',
                'bol_active' => 1,
            ),
            241 => 
            array (
                'id_model' => 1742,
                'id_manufacturer' => 236,
                'str_description' => 'f1',
                'str_description_slug' => 'f1',
                'bol_active' => 1,
            ),
            242 => 
            array (
                'id_model' => 1743,
                'id_manufacturer' => 236,
                'str_description' => 'mp4-12c',
                'str_description_slug' => 'mp4-12c',
                'bol_active' => 1,
            ),
            243 => 
            array (
                'id_model' => 1744,
                'id_manufacturer' => 236,
                'str_description' => 'p1',
                'str_description_slug' => 'p1',
                'bol_active' => 1,
            ),
            244 => 
            array (
                'id_model' => 1745,
                'id_manufacturer' => 237,
                'str_description' => 'club',
                'str_description_slug' => 'club',
                'bol_active' => 1,
            ),
            245 => 
            array (
                'id_model' => 1746,
                'id_manufacturer' => 237,
                'str_description' => 'tjaeffer',
                'str_description_slug' => 'tjaeffer',
                'bol_active' => 1,
            ),
            246 => 
            array (
                'id_model' => 1747,
                'id_manufacturer' => 238,
                'str_description' => 'mt 1100',
                'str_description_slug' => 'mt-1100',
                'bol_active' => 1,
            ),
            247 => 
            array (
                'id_model' => 1748,
                'id_manufacturer' => 238,
                'str_description' => 'mt 800',
                'str_description_slug' => 'mt-800',
                'bol_active' => 1,
            ),
            248 => 
            array (
                'id_model' => 1749,
                'id_manufacturer' => 239,
                'str_description' => 'bardena',
                'str_description_slug' => 'bardena',
                'bol_active' => 1,
            ),
            249 => 
            array (
                'id_model' => 1750,
                'id_manufacturer' => 239,
                'str_description' => 'rhino',
                'str_description_slug' => 'rhino',
                'bol_active' => 1,
            ),
            250 => 
            array (
                'id_model' => 1751,
                'id_manufacturer' => 240,
                'str_description' => '343',
                'str_description_slug' => '343',
                'bol_active' => 1,
            ),
            251 => 
            array (
                'id_model' => 1752,
                'id_manufacturer' => 240,
                'str_description' => '563',
                'str_description_slug' => '563',
                'bol_active' => 1,
            ),
            252 => 
            array (
                'id_model' => 1753,
                'id_manufacturer' => 240,
                'str_description' => '627',
                'str_description_slug' => '627',
                'bol_active' => 1,
            ),
            253 => 
            array (
                'id_model' => 1754,
                'id_manufacturer' => 240,
                'str_description' => '663',
                'str_description_slug' => '663',
                'bol_active' => 1,
            ),
            254 => 
            array (
                'id_model' => 1755,
                'id_manufacturer' => 240,
                'str_description' => '866',
                'str_description_slug' => '866',
                'bol_active' => 1,
            ),
            255 => 
            array (
                'id_model' => 1756,
                'id_manufacturer' => 240,
                'str_description' => '943',
                'str_description_slug' => '943',
                'bol_active' => 1,
            ),
            256 => 
            array (
                'id_model' => 1757,
                'id_manufacturer' => 240,
                'str_description' => '945',
                'str_description_slug' => '945',
                'bol_active' => 1,
            ),
            257 => 
            array (
                'id_model' => 1758,
                'id_manufacturer' => 240,
                'str_description' => 'lektro',
                'str_description_slug' => 'lektro',
                'bol_active' => 1,
            ),
            258 => 
            array (
                'id_model' => 1759,
                'id_manufacturer' => 240,
                'str_description' => 'sxm',
                'str_description_slug' => 'sxm',
                'bol_active' => 1,
            ),
            259 => 
            array (
                'id_model' => 1760,
                'id_manufacturer' => 241,
            'str_description' => '170 (136)',
            'str_description_slug' => '170-(136)',
                'bol_active' => 1,
            ),
            260 => 
            array (
                'id_model' => 1761,
                'id_manufacturer' => 241,
                'str_description' => '170 v',
                'str_description_slug' => '170-v',
                'bol_active' => 1,
            ),
            261 => 
            array (
                'id_model' => 1762,
                'id_manufacturer' => 241,
            'str_description' => '180 (120)',
            'str_description_slug' => '180-(120)',
                'bol_active' => 1,
            ),
            262 => 
            array (
                'id_model' => 1763,
                'id_manufacturer' => 241,
            'str_description' => '190 (110)',
            'str_description_slug' => '190-(110)',
                'bol_active' => 1,
            ),
            263 => 
            array (
                'id_model' => 1764,
                'id_manufacturer' => 241,
            'str_description' => '190 (121)',
            'str_description_slug' => '190-(121)',
                'bol_active' => 1,
            ),
            264 => 
            array (
                'id_model' => 1765,
                'id_manufacturer' => 241,
            'str_description' => '190 (201)',
            'str_description_slug' => '190-(201)',
                'bol_active' => 1,
            ),
            265 => 
            array (
                'id_model' => 1766,
                'id_manufacturer' => 241,
            'str_description' => '200 (110)',
            'str_description_slug' => '200-(110)',
                'bol_active' => 1,
            ),
            266 => 
            array (
                'id_model' => 1767,
                'id_manufacturer' => 241,
            'str_description' => '200 (115)',
            'str_description_slug' => '200-(115)',
                'bol_active' => 1,
            ),
            267 => 
            array (
                'id_model' => 1768,
                'id_manufacturer' => 241,
            'str_description' => '200 (123)',
            'str_description_slug' => '200-(123)',
                'bol_active' => 1,
            ),
            268 => 
            array (
                'id_model' => 1769,
                'id_manufacturer' => 241,
            'str_description' => '200 (124)',
            'str_description_slug' => '200-(124)',
                'bol_active' => 1,
            ),
            269 => 
            array (
                'id_model' => 1770,
                'id_manufacturer' => 241,
            'str_description' => '200 (21)',
            'str_description_slug' => '200-(21)',
                'bol_active' => 1,
            ),
            270 => 
            array (
                'id_model' => 1771,
                'id_manufacturer' => 241,
            'str_description' => '200 ge (463)',
            'str_description_slug' => '200-ge-(463)',
                'bol_active' => 1,
            ),
            271 => 
            array (
                'id_model' => 1772,
                'id_manufacturer' => 241,
                'str_description' => '207',
                'str_description_slug' => '207',
                'bol_active' => 1,
            ),
            272 => 
            array (
                'id_model' => 1773,
                'id_manufacturer' => 241,
                'str_description' => '208',
                'str_description_slug' => '208',
                'bol_active' => 1,
            ),
            273 => 
            array (
                'id_model' => 1774,
                'id_manufacturer' => 241,
                'str_description' => '209',
                'str_description_slug' => '209',
                'bol_active' => 1,
            ),
            274 => 
            array (
                'id_model' => 1775,
                'id_manufacturer' => 241,
            'str_description' => '220 (105)',
            'str_description_slug' => '220-(105)',
                'bol_active' => 1,
            ),
            275 => 
            array (
                'id_model' => 1776,
                'id_manufacturer' => 241,
            'str_description' => '220 (111)',
            'str_description_slug' => '220-(111)',
                'bol_active' => 1,
            ),
            276 => 
            array (
                'id_model' => 1777,
                'id_manufacturer' => 241,
            'str_description' => '220 (115)',
            'str_description_slug' => '220-(115)',
                'bol_active' => 1,
            ),
            277 => 
            array (
                'id_model' => 1778,
                'id_manufacturer' => 241,
            'str_description' => '220 (123)',
            'str_description_slug' => '220-(123)',
                'bol_active' => 1,
            ),
            278 => 
            array (
                'id_model' => 1779,
                'id_manufacturer' => 241,
            'str_description' => '220 (124)',
            'str_description_slug' => '220-(124)',
                'bol_active' => 1,
            ),
            279 => 
            array (
                'id_model' => 1780,
                'id_manufacturer' => 241,
            'str_description' => '220 (128)',
            'str_description_slug' => '220-(128)',
                'bol_active' => 1,
            ),
            280 => 
            array (
                'id_model' => 1781,
                'id_manufacturer' => 241,
            'str_description' => '220 (180)',
            'str_description_slug' => '220-(180)',
                'bol_active' => 1,
            ),
            281 => 
            array (
                'id_model' => 1782,
                'id_manufacturer' => 241,
            'str_description' => '220 (187)',
            'str_description_slug' => '220-(187)',
                'bol_active' => 1,
            ),
            282 => 
            array (
                'id_model' => 1783,
                'id_manufacturer' => 241,
                'str_description' => '220 a',
                'str_description_slug' => '220-a',
                'bol_active' => 1,
            ),
            283 => 
            array (
                'id_model' => 1784,
                'id_manufacturer' => 241,
            'str_description' => '230 (111)',
            'str_description_slug' => '230-(111)',
                'bol_active' => 1,
            ),
            284 => 
            array (
                'id_model' => 1785,
                'id_manufacturer' => 241,
            'str_description' => '230 (113)',
            'str_description_slug' => '230-(113)',
                'bol_active' => 1,
            ),
            285 => 
            array (
                'id_model' => 1786,
                'id_manufacturer' => 241,
            'str_description' => '230 (114)',
            'str_description_slug' => '230-(114)',
                'bol_active' => 1,
            ),
            286 => 
            array (
                'id_model' => 1787,
                'id_manufacturer' => 241,
            'str_description' => '230 (123)',
            'str_description_slug' => '230-(123)',
                'bol_active' => 1,
            ),
            287 => 
            array (
                'id_model' => 1788,
                'id_manufacturer' => 241,
            'str_description' => '230 (124)',
            'str_description_slug' => '230-(124)',
                'bol_active' => 1,
            ),
            288 => 
            array (
                'id_model' => 1789,
                'id_manufacturer' => 241,
            'str_description' => '230 g (460)',
            'str_description_slug' => '230-g-(460)',
                'bol_active' => 1,
            ),
            289 => 
            array (
                'id_model' => 1790,
                'id_manufacturer' => 241,
            'str_description' => '230 ge (461)',
            'str_description_slug' => '230-ge-(461)',
                'bol_active' => 1,
            ),
            290 => 
            array (
                'id_model' => 1791,
                'id_manufacturer' => 241,
            'str_description' => '240 (115)',
            'str_description_slug' => '240-(115)',
                'bol_active' => 1,
            ),
            291 => 
            array (
                'id_model' => 1792,
                'id_manufacturer' => 241,
            'str_description' => '240 (123)',
            'str_description_slug' => '240-(123)',
                'bol_active' => 1,
            ),
            292 => 
            array (
                'id_model' => 1793,
                'id_manufacturer' => 241,
            'str_description' => '240 gd (460)',
            'str_description_slug' => '240-gd-(460)',
                'bol_active' => 1,
            ),
            293 => 
            array (
                'id_model' => 1794,
                'id_manufacturer' => 241,
            'str_description' => '250 (108)',
            'str_description_slug' => '250-(108)',
                'bol_active' => 1,
            ),
            294 => 
            array (
                'id_model' => 1795,
                'id_manufacturer' => 241,
            'str_description' => '250 (111)',
            'str_description_slug' => '250-(111)',
                'bol_active' => 1,
            ),
            295 => 
            array (
                'id_model' => 1796,
                'id_manufacturer' => 241,
            'str_description' => '250 (113)',
            'str_description_slug' => '250-(113)',
                'bol_active' => 1,
            ),
            296 => 
            array (
                'id_model' => 1797,
                'id_manufacturer' => 241,
            'str_description' => '250 (114)',
            'str_description_slug' => '250-(114)',
                'bol_active' => 1,
            ),
            297 => 
            array (
                'id_model' => 1798,
                'id_manufacturer' => 241,
            'str_description' => '250 (123)',
            'str_description_slug' => '250-(123)',
                'bol_active' => 1,
            ),
            298 => 
            array (
                'id_model' => 1799,
                'id_manufacturer' => 241,
            'str_description' => '250 (124)',
            'str_description_slug' => '250-(124)',
                'bol_active' => 1,
            ),
            299 => 
            array (
                'id_model' => 1800,
                'id_manufacturer' => 241,
            'str_description' => '250 gd (463)',
            'str_description_slug' => '250-gd-(463)',
                'bol_active' => 1,
            ),
            300 => 
            array (
                'id_model' => 1801,
                'id_manufacturer' => 241,
            'str_description' => '260 (123)',
            'str_description_slug' => '260-(123)',
                'bol_active' => 1,
            ),
            301 => 
            array (
                'id_model' => 1802,
                'id_manufacturer' => 241,
            'str_description' => '260 (124)',
            'str_description_slug' => '260-(124)',
                'bol_active' => 1,
            ),
            302 => 
            array (
                'id_model' => 1803,
                'id_manufacturer' => 241,
            'str_description' => '260 (126)',
            'str_description_slug' => '260-(126)',
                'bol_active' => 1,
            ),
            303 => 
            array (
                'id_model' => 1804,
                'id_manufacturer' => 241,
            'str_description' => '280 (107)',
            'str_description_slug' => '280-(107)',
                'bol_active' => 1,
            ),
            304 => 
            array (
                'id_model' => 1805,
                'id_manufacturer' => 241,
            'str_description' => '280 (108)',
            'str_description_slug' => '280-(108)',
                'bol_active' => 1,
            ),
            305 => 
            array (
                'id_model' => 1806,
                'id_manufacturer' => 241,
            'str_description' => '280 (111)',
            'str_description_slug' => '280-(111)',
                'bol_active' => 1,
            ),
            306 => 
            array (
                'id_model' => 1807,
                'id_manufacturer' => 241,
            'str_description' => '280 (114)',
            'str_description_slug' => '280-(114)',
                'bol_active' => 1,
            ),
            307 => 
            array (
                'id_model' => 1808,
                'id_manufacturer' => 241,
            'str_description' => '280 (116)',
            'str_description_slug' => '280-(116)',
                'bol_active' => 1,
            ),
            308 => 
            array (
                'id_model' => 1809,
                'id_manufacturer' => 241,
            'str_description' => '280 (123)',
            'str_description_slug' => '280-(123)',
                'bol_active' => 1,
            ),
            309 => 
            array (
                'id_model' => 1810,
                'id_manufacturer' => 241,
            'str_description' => '280 (124)',
            'str_description_slug' => '280-(124)',
                'bol_active' => 1,
            ),
            310 => 
            array (
                'id_model' => 1811,
                'id_manufacturer' => 241,
            'str_description' => '280 (126)',
            'str_description_slug' => '280-(126)',
                'bol_active' => 1,
            ),
            311 => 
            array (
                'id_model' => 1812,
                'id_manufacturer' => 241,
            'str_description' => '280 ge (460)',
            'str_description_slug' => '280-ge-(460)',
                'bol_active' => 1,
            ),
            312 => 
            array (
                'id_model' => 1813,
                'id_manufacturer' => 241,
            'str_description' => '290 gd (460)',
            'str_description_slug' => '290-gd-(460)',
                'bol_active' => 1,
            ),
            313 => 
            array (
                'id_model' => 1814,
                'id_manufacturer' => 241,
            'str_description' => '290 gd (461)',
            'str_description_slug' => '290-gd-(461)',
                'bol_active' => 1,
            ),
            314 => 
            array (
                'id_model' => 1815,
                'id_manufacturer' => 241,
            'str_description' => '300 (107)',
            'str_description_slug' => '300-(107)',
                'bol_active' => 1,
            ),
            315 => 
            array (
                'id_model' => 1816,
                'id_manufacturer' => 241,
            'str_description' => '300 (109)',
            'str_description_slug' => '300-(109)',
                'bol_active' => 1,
            ),
            316 => 
            array (
                'id_model' => 1817,
                'id_manufacturer' => 241,
            'str_description' => '300 (112)',
            'str_description_slug' => '300-(112)',
                'bol_active' => 1,
            ),
            317 => 
            array (
                'id_model' => 1818,
                'id_manufacturer' => 241,
            'str_description' => '300 (114)',
            'str_description_slug' => '300-(114)',
                'bol_active' => 1,
            ),
            318 => 
            array (
                'id_model' => 1819,
                'id_manufacturer' => 241,
            'str_description' => '300 (123)',
            'str_description_slug' => '300-(123)',
                'bol_active' => 1,
            ),
            319 => 
            array (
                'id_model' => 1820,
                'id_manufacturer' => 241,
            'str_description' => '300 (124)',
            'str_description_slug' => '300-(124)',
                'bol_active' => 1,
            ),
            320 => 
            array (
                'id_model' => 1821,
                'id_manufacturer' => 241,
            'str_description' => '300 (126)',
            'str_description_slug' => '300-(126)',
                'bol_active' => 1,
            ),
            321 => 
            array (
                'id_model' => 1822,
                'id_manufacturer' => 241,
            'str_description' => '300 (129)',
            'str_description_slug' => '300-(129)',
                'bol_active' => 1,
            ),
            322 => 
            array (
                'id_model' => 1823,
                'id_manufacturer' => 241,
            'str_description' => '300 (140)',
            'str_description_slug' => '300-(140)',
                'bol_active' => 1,
            ),
            323 => 
            array (
                'id_model' => 1824,
                'id_manufacturer' => 241,
            'str_description' => '300 (186)',
            'str_description_slug' => '300-(186)',
                'bol_active' => 1,
            ),
            324 => 
            array (
                'id_model' => 1825,
                'id_manufacturer' => 241,
            'str_description' => '300 (189)',
            'str_description_slug' => '300-(189)',
                'bol_active' => 1,
            ),
            325 => 
            array (
                'id_model' => 1826,
                'id_manufacturer' => 241,
            'str_description' => '300 (198)',
            'str_description_slug' => '300-(198)',
                'bol_active' => 1,
            ),
            326 => 
            array (
                'id_model' => 1827,
                'id_manufacturer' => 241,
            'str_description' => '300 gd (460)',
            'str_description_slug' => '300-gd-(460)',
                'bol_active' => 1,
            ),
            327 => 
            array (
                'id_model' => 1828,
                'id_manufacturer' => 241,
            'str_description' => '300 gd (463)',
            'str_description_slug' => '300-gd-(463)',
                'bol_active' => 1,
            ),
            328 => 
            array (
                'id_model' => 1829,
                'id_manufacturer' => 241,
            'str_description' => '300 ge (463)',
            'str_description_slug' => '300-ge-(463)',
                'bol_active' => 1,
            ),
            329 => 
            array (
                'id_model' => 1830,
                'id_manufacturer' => 241,
                'str_description' => '307',
                'str_description_slug' => '307',
                'bol_active' => 1,
            ),
            330 => 
            array (
                'id_model' => 1831,
                'id_manufacturer' => 241,
                'str_description' => '309',
                'str_description_slug' => '309',
                'bol_active' => 1,
            ),
            331 => 
            array (
                'id_model' => 1832,
                'id_manufacturer' => 241,
            'str_description' => '320 (124)',
            'str_description_slug' => '320-(124)',
                'bol_active' => 1,
            ),
            332 => 
            array (
                'id_model' => 1833,
                'id_manufacturer' => 241,
            'str_description' => '350 (107)',
            'str_description_slug' => '350-(107)',
                'bol_active' => 1,
            ),
            333 => 
            array (
                'id_model' => 1834,
                'id_manufacturer' => 241,
            'str_description' => '350 (116)',
            'str_description_slug' => '350-(116)',
                'bol_active' => 1,
            ),
            334 => 
            array (
                'id_model' => 1835,
                'id_manufacturer' => 241,
            'str_description' => '350 (126)',
            'str_description_slug' => '350-(126)',
                'bol_active' => 1,
            ),
            335 => 
            array (
                'id_model' => 1836,
                'id_manufacturer' => 241,
            'str_description' => '350 (129)',
            'str_description_slug' => '350-(129)',
                'bol_active' => 1,
            ),
            336 => 
            array (
                'id_model' => 1837,
                'id_manufacturer' => 241,
            'str_description' => '350 gdt (463)',
            'str_description_slug' => '350-gdt-(463)',
                'bol_active' => 1,
            ),
            337 => 
            array (
                'id_model' => 1838,
                'id_manufacturer' => 241,
            'str_description' => '380 (107)',
            'str_description_slug' => '380-(107)',
                'bol_active' => 1,
            ),
            338 => 
            array (
                'id_model' => 1839,
                'id_manufacturer' => 241,
            'str_description' => '380 (126)',
            'str_description_slug' => '380-(126)',
                'bol_active' => 1,
            ),
            339 => 
            array (
                'id_model' => 1840,
                'id_manufacturer' => 241,
            'str_description' => '400 (124)',
            'str_description_slug' => '400-(124)',
                'bol_active' => 1,
            ),
            340 => 
            array (
                'id_model' => 1841,
                'id_manufacturer' => 241,
            'str_description' => '400 (140)',
            'str_description_slug' => '400-(140)',
                'bol_active' => 1,
            ),
            341 => 
            array (
                'id_model' => 1842,
                'id_manufacturer' => 241,
                'str_description' => '407 d',
                'str_description_slug' => '407-d',
                'bol_active' => 1,
            ),
            342 => 
            array (
                'id_model' => 1843,
                'id_manufacturer' => 241,
                'str_description' => '409 d',
                'str_description_slug' => '409-d',
                'bol_active' => 1,
            ),
            343 => 
            array (
                'id_model' => 1844,
                'id_manufacturer' => 241,
            'str_description' => '420 (107)',
            'str_description_slug' => '420-(107)',
                'bol_active' => 1,
            ),
            344 => 
            array (
                'id_model' => 1845,
                'id_manufacturer' => 241,
            'str_description' => '420 (126)',
            'str_description_slug' => '420-(126)',
                'bol_active' => 1,
            ),
            345 => 
            array (
                'id_model' => 1846,
                'id_manufacturer' => 241,
            'str_description' => '450 (107)',
            'str_description_slug' => '450-(107)',
                'bol_active' => 1,
            ),
            346 => 
            array (
                'id_model' => 1847,
                'id_manufacturer' => 241,
            'str_description' => '450 (116)',
            'str_description_slug' => '450-(116)',
                'bol_active' => 1,
            ),
            347 => 
            array (
                'id_model' => 1848,
                'id_manufacturer' => 241,
            'str_description' => '500 (107)',
            'str_description_slug' => '500-(107)',
                'bol_active' => 1,
            ),
            348 => 
            array (
                'id_model' => 1849,
                'id_manufacturer' => 241,
            'str_description' => '500 (124)',
            'str_description_slug' => '500-(124)',
                'bol_active' => 1,
            ),
            349 => 
            array (
                'id_model' => 1850,
                'id_manufacturer' => 241,
            'str_description' => '500 (126)',
            'str_description_slug' => '500-(126)',
                'bol_active' => 1,
            ),
            350 => 
            array (
                'id_model' => 1851,
                'id_manufacturer' => 241,
            'str_description' => '500 (129)',
            'str_description_slug' => '500-(129)',
                'bol_active' => 1,
            ),
            351 => 
            array (
                'id_model' => 1852,
                'id_manufacturer' => 241,
            'str_description' => '500 (140)',
            'str_description_slug' => '500-(140)',
                'bol_active' => 1,
            ),
            352 => 
            array (
                'id_model' => 1853,
                'id_manufacturer' => 241,
            'str_description' => '500 ge (463)',
            'str_description_slug' => '500-ge-(463)',
                'bol_active' => 1,
            ),
            353 => 
            array (
                'id_model' => 1854,
                'id_manufacturer' => 241,
                'str_description' => '508 d',
                'str_description_slug' => '508-d',
                'bol_active' => 1,
            ),
            354 => 
            array (
                'id_model' => 1855,
                'id_manufacturer' => 241,
            'str_description' => '560 (107)',
            'str_description_slug' => '560-(107)',
                'bol_active' => 1,
            ),
            355 => 
            array (
                'id_model' => 1856,
                'id_manufacturer' => 241,
            'str_description' => '560 (126)',
            'str_description_slug' => '560-(126)',
                'bol_active' => 1,
            ),
            356 => 
            array (
                'id_model' => 1857,
                'id_manufacturer' => 241,
            'str_description' => '600 (100)',
            'str_description_slug' => '600-(100)',
                'bol_active' => 1,
            ),
            357 => 
            array (
                'id_model' => 1858,
                'id_manufacturer' => 241,
            'str_description' => '600 (140)',
            'str_description_slug' => '600-(140)',
                'bol_active' => 1,
            ),
            358 => 
            array (
                'id_model' => 1859,
                'id_manufacturer' => 241,
                'str_description' => '601',
                'str_description_slug' => '601',
                'bol_active' => 1,
            ),
            359 => 
            array (
                'id_model' => 1860,
                'id_manufacturer' => 241,
                'str_description' => '602',
                'str_description_slug' => '602',
                'bol_active' => 1,
            ),
            360 => 
            array (
                'id_model' => 1861,
                'id_manufacturer' => 241,
            'str_description' => 'a (176)',
            'str_description_slug' => 'a-(176)',
                'bol_active' => 1,
            ),
            361 => 
            array (
                'id_model' => 1862,
                'id_manufacturer' => 241,
                'str_description' => 'amg gt',
                'str_description_slug' => 'amg-gt',
                'bol_active' => 1,
            ),
            362 => 
            array (
                'id_model' => 1863,
                'id_manufacturer' => 241,
            'str_description' => 'b (246/242)',
            'str_description_slug' => 'b-(246/242)',
                'bol_active' => 1,
            ),
            363 => 
            array (
                'id_model' => 1864,
                'id_manufacturer' => 241,
            'str_description' => 'c (205)',
            'str_description_slug' => 'c-(205)',
                'bol_active' => 1,
            ),
            364 => 
            array (
                'id_model' => 1865,
                'id_manufacturer' => 241,
                'str_description' => 'citan',
                'str_description_slug' => 'citan',
                'bol_active' => 1,
            ),
            365 => 
            array (
                'id_model' => 1866,
                'id_manufacturer' => 241,
            'str_description' => 'cl (140)',
            'str_description_slug' => 'cl-(140)',
                'bol_active' => 1,
            ),
            366 => 
            array (
                'id_model' => 1867,
                'id_manufacturer' => 241,
            'str_description' => 'cl (215-216)',
            'str_description_slug' => 'cl-(215-216)',
                'bol_active' => 1,
            ),
            367 => 
            array (
                'id_model' => 1868,
                'id_manufacturer' => 241,
            'str_description' => 'cla (117)',
            'str_description_slug' => 'cla-(117)',
                'bol_active' => 1,
            ),
            368 => 
            array (
                'id_model' => 1869,
                'id_manufacturer' => 241,
                'str_description' => 'clc',
                'str_description_slug' => 'clc',
                'bol_active' => 1,
            ),
            369 => 
            array (
                'id_model' => 1870,
                'id_manufacturer' => 241,
            'str_description' => 'cls (218)',
            'str_description_slug' => 'cls-(218)',
                'bol_active' => 1,
            ),
            370 => 
            array (
                'id_model' => 1871,
                'id_manufacturer' => 241,
            'str_description' => 'cls (219)',
            'str_description_slug' => 'cls-(219)',
                'bol_active' => 1,
            ),
            371 => 
            array (
                'id_model' => 1872,
                'id_manufacturer' => 241,
            'str_description' => 'e (213)',
            'str_description_slug' => 'e-(213)',
                'bol_active' => 1,
            ),
            372 => 
            array (
                'id_model' => 1873,
                'id_manufacturer' => 241,
            'str_description' => 'g (463)',
            'str_description_slug' => 'g-(463)',
                'bol_active' => 1,
            ),
            373 => 
            array (
                'id_model' => 1874,
                'id_manufacturer' => 241,
            'str_description' => 'g 230 (461)',
            'str_description_slug' => 'g-230-(461)',
                'bol_active' => 1,
            ),
            374 => 
            array (
                'id_model' => 1875,
                'id_manufacturer' => 241,
            'str_description' => 'g 250 (460)',
            'str_description_slug' => 'g-250-(460)',
                'bol_active' => 1,
            ),
            375 => 
            array (
                'id_model' => 1876,
                'id_manufacturer' => 241,
            'str_description' => 'g 250 (461)',
            'str_description_slug' => 'g-250-(461)',
                'bol_active' => 1,
            ),
            376 => 
            array (
                'id_model' => 1877,
                'id_manufacturer' => 241,
            'str_description' => 'g 280 (461)',
            'str_description_slug' => 'g-280-(461)',
                'bol_active' => 1,
            ),
            377 => 
            array (
                'id_model' => 1878,
                'id_manufacturer' => 241,
            'str_description' => 'g 300 (460)',
            'str_description_slug' => 'g-300-(460)',
                'bol_active' => 1,
            ),
            378 => 
            array (
                'id_model' => 1879,
                'id_manufacturer' => 241,
            'str_description' => 'g 300 (461)',
            'str_description_slug' => 'g-300-(461)',
                'bol_active' => 1,
            ),
            379 => 
            array (
                'id_model' => 1880,
                'id_manufacturer' => 241,
            'str_description' => 'g 300 (463)',
            'str_description_slug' => 'g-300-(463)',
                'bol_active' => 1,
            ),
            380 => 
            array (
                'id_model' => 1881,
                'id_manufacturer' => 241,
            'str_description' => 'g 320 (463)',
            'str_description_slug' => 'g-320-(463)',
                'bol_active' => 1,
            ),
            381 => 
            array (
                'id_model' => 1882,
                'id_manufacturer' => 241,
            'str_description' => 'g 350 (463)',
            'str_description_slug' => 'g-350-(463)',
                'bol_active' => 1,
            ),
            382 => 
            array (
                'id_model' => 1883,
                'id_manufacturer' => 241,
                'str_description' => 'g 500',
                'str_description_slug' => 'g-500',
                'bol_active' => 1,
            ),
            383 => 
            array (
                'id_model' => 1884,
                'id_manufacturer' => 241,
            'str_description' => 'gl (164)',
            'str_description_slug' => 'gl-(164)',
                'bol_active' => 1,
            ),
            384 => 
            array (
                'id_model' => 1885,
                'id_manufacturer' => 241,
            'str_description' => 'gl (166)',
            'str_description_slug' => 'gl-(166)',
                'bol_active' => 1,
            ),
            385 => 
            array (
                'id_model' => 1886,
                'id_manufacturer' => 241,
            'str_description' => 'gla (156)',
            'str_description_slug' => 'gla-(156)',
                'bol_active' => 1,
            ),
            386 => 
            array (
                'id_model' => 1887,
                'id_manufacturer' => 241,
            'str_description' => 'glc (253)',
            'str_description_slug' => 'glc-(253)',
                'bol_active' => 1,
            ),
            387 => 
            array (
                'id_model' => 1888,
                'id_manufacturer' => 241,
            'str_description' => 'gle (166-292)',
            'str_description_slug' => 'gle-(166-292)',
                'bol_active' => 1,
            ),
            388 => 
            array (
                'id_model' => 1889,
                'id_manufacturer' => 241,
            'str_description' => 'glk (204)',
            'str_description_slug' => 'glk-(204)',
                'bol_active' => 1,
            ),
            389 => 
            array (
                'id_model' => 1890,
                'id_manufacturer' => 241,
                'str_description' => 'gls',
                'str_description_slug' => 'gls',
                'bol_active' => 1,
            ),
            390 => 
            array (
                'id_model' => 1891,
                'id_manufacturer' => 241,
                'str_description' => 'l 206',
                'str_description_slug' => 'l-206',
                'bol_active' => 1,
            ),
            391 => 
            array (
                'id_model' => 1892,
                'id_manufacturer' => 241,
                'str_description' => 'l 319',
                'str_description_slug' => 'l-319',
                'bol_active' => 1,
            ),
            392 => 
            array (
                'id_model' => 1893,
                'id_manufacturer' => 241,
                'str_description' => 'l 405',
                'str_description_slug' => 'l-405',
                'bol_active' => 1,
            ),
            393 => 
            array (
                'id_model' => 1894,
                'id_manufacturer' => 241,
                'str_description' => 'l 406',
                'str_description_slug' => 'l-406',
                'bol_active' => 1,
            ),
            394 => 
            array (
                'id_model' => 1895,
                'id_manufacturer' => 241,
                'str_description' => 'l 407',
                'str_description_slug' => 'l-407',
                'bol_active' => 1,
            ),
            395 => 
            array (
                'id_model' => 1896,
                'id_manufacturer' => 241,
                'str_description' => 'l 408',
                'str_description_slug' => 'l-408',
                'bol_active' => 1,
            ),
            396 => 
            array (
                'id_model' => 1897,
                'id_manufacturer' => 241,
                'str_description' => 'l 409',
                'str_description_slug' => 'l-409',
                'bol_active' => 1,
            ),
            397 => 
            array (
                'id_model' => 1898,
                'id_manufacturer' => 241,
                'str_description' => 'l 508',
                'str_description_slug' => 'l-508',
                'bol_active' => 1,
            ),
            398 => 
            array (
                'id_model' => 1899,
                'id_manufacturer' => 241,
                'str_description' => 'l 608',
                'str_description_slug' => 'l-608',
                'bol_active' => 1,
            ),
            399 => 
            array (
                'id_model' => 1900,
                'id_manufacturer' => 241,
                'str_description' => 'l 709',
                'str_description_slug' => 'l-709',
                'bol_active' => 1,
            ),
            400 => 
            array (
                'id_model' => 1901,
                'id_manufacturer' => 241,
            'str_description' => 'm (166)',
            'str_description_slug' => 'm-(166)',
                'bol_active' => 1,
            ),
            401 => 
            array (
                'id_model' => 1902,
                'id_manufacturer' => 241,
                'str_description' => 'maybach s',
                'str_description_slug' => 'maybach-s',
                'bol_active' => 1,
            ),
            402 => 
            array (
                'id_model' => 1903,
                'id_manufacturer' => 241,
                'str_description' => 'mb-100',
                'str_description_slug' => 'mb-100',
                'bol_active' => 1,
            ),
            403 => 
            array (
                'id_model' => 1904,
                'id_manufacturer' => 241,
                'str_description' => 'mb-120',
                'str_description_slug' => 'mb-120',
                'bol_active' => 1,
            ),
            404 => 
            array (
                'id_model' => 1905,
                'id_manufacturer' => 241,
                'str_description' => 'mb-130',
                'str_description_slug' => 'mb-130',
                'bol_active' => 1,
            ),
            405 => 
            array (
                'id_model' => 1906,
                'id_manufacturer' => 241,
                'str_description' => 'mb-140',
                'str_description_slug' => 'mb-140',
                'bol_active' => 1,
            ),
            406 => 
            array (
                'id_model' => 1907,
                'id_manufacturer' => 241,
                'str_description' => 'mb-150',
                'str_description_slug' => 'mb-150',
                'bol_active' => 1,
            ),
            407 => 
            array (
                'id_model' => 1908,
                'id_manufacturer' => 241,
                'str_description' => 'mb-160',
                'str_description_slug' => 'mb-160',
                'bol_active' => 1,
            ),
            408 => 
            array (
                'id_model' => 1909,
                'id_manufacturer' => 241,
                'str_description' => 'mb-170',
                'str_description_slug' => 'mb-170',
                'bol_active' => 1,
            ),
            409 => 
            array (
                'id_model' => 1910,
                'id_manufacturer' => 241,
                'str_description' => 'mb-180',
                'str_description_slug' => 'mb-180',
                'bol_active' => 1,
            ),
            410 => 
            array (
                'id_model' => 1911,
                'id_manufacturer' => 241,
                'str_description' => 'mb-207',
                'str_description_slug' => 'mb-207',
                'bol_active' => 1,
            ),
            411 => 
            array (
                'id_model' => 1912,
                'id_manufacturer' => 241,
                'str_description' => 'mb-210',
                'str_description_slug' => 'mb-210',
                'bol_active' => 1,
            ),
            412 => 
            array (
                'id_model' => 1913,
                'id_manufacturer' => 241,
                'str_description' => 'mb-307',
                'str_description_slug' => 'mb-307',
                'bol_active' => 1,
            ),
            413 => 
            array (
                'id_model' => 1914,
                'id_manufacturer' => 241,
                'str_description' => 'mb-308',
                'str_description_slug' => 'mb-308',
                'bol_active' => 1,
            ),
            414 => 
            array (
                'id_model' => 1915,
                'id_manufacturer' => 241,
                'str_description' => 'mb-310',
                'str_description_slug' => 'mb-310',
                'bol_active' => 1,
            ),
            415 => 
            array (
                'id_model' => 1916,
                'id_manufacturer' => 241,
                'str_description' => 'mb-409',
                'str_description_slug' => 'mb-409',
                'bol_active' => 1,
            ),
            416 => 
            array (
                'id_model' => 1917,
                'id_manufacturer' => 241,
                'str_description' => 'mb-410',
                'str_description_slug' => 'mb-410',
                'bol_active' => 1,
            ),
            417 => 
            array (
                'id_model' => 1918,
                'id_manufacturer' => 241,
                'str_description' => 'mb-80',
                'str_description_slug' => 'mb-80',
                'bol_active' => 1,
            ),
            418 => 
            array (
                'id_model' => 1919,
                'id_manufacturer' => 241,
                'str_description' => 'mb-90',
                'str_description_slug' => 'mb-90',
                'bol_active' => 1,
            ),
            419 => 
            array (
                'id_model' => 1920,
                'id_manufacturer' => 241,
                'str_description' => 'ms',
                'str_description_slug' => 'ms',
                'bol_active' => 1,
            ),
            420 => 
            array (
                'id_model' => 1921,
                'id_manufacturer' => 241,
                'str_description' => 'n',
                'str_description_slug' => 'n',
                'bol_active' => 1,
            ),
            421 => 
            array (
                'id_model' => 1922,
                'id_manufacturer' => 241,
            'str_description' => 'r (251)',
            'str_description_slug' => 'r-(251)',
                'bol_active' => 1,
            ),
            422 => 
            array (
                'id_model' => 1923,
                'id_manufacturer' => 241,
            'str_description' => 's (217)',
            'str_description_slug' => 's-(217)',
                'bol_active' => 1,
            ),
            423 => 
            array (
                'id_model' => 1924,
                'id_manufacturer' => 241,
            'str_description' => 's (220-221)',
            'str_description_slug' => 's-(220-221)',
                'bol_active' => 1,
            ),
            424 => 
            array (
                'id_model' => 1925,
                'id_manufacturer' => 241,
            'str_description' => 's (222)',
            'str_description_slug' => 's-(222)',
                'bol_active' => 1,
            ),
            425 => 
            array (
                'id_model' => 1926,
                'id_manufacturer' => 241,
            'str_description' => 's 280 (140)',
            'str_description_slug' => 's-280-(140)',
                'bol_active' => 1,
            ),
            426 => 
            array (
                'id_model' => 1927,
                'id_manufacturer' => 241,
            'str_description' => 's 300 (140)',
            'str_description_slug' => 's-300-(140)',
                'bol_active' => 1,
            ),
            427 => 
            array (
                'id_model' => 1928,
                'id_manufacturer' => 241,
            'str_description' => 's 320 (140)',
            'str_description_slug' => 's-320-(140)',
                'bol_active' => 1,
            ),
            428 => 
            array (
                'id_model' => 1929,
                'id_manufacturer' => 241,
            'str_description' => 's 350 (140)',
            'str_description_slug' => 's-350-(140)',
                'bol_active' => 1,
            ),
            429 => 
            array (
                'id_model' => 1930,
                'id_manufacturer' => 241,
            'str_description' => 's 420 (140)',
            'str_description_slug' => 's-420-(140)',
                'bol_active' => 1,
            ),
            430 => 
            array (
                'id_model' => 1931,
                'id_manufacturer' => 241,
            'str_description' => 's 500 (140)',
            'str_description_slug' => 's-500-(140)',
                'bol_active' => 1,
            ),
            431 => 
            array (
                'id_model' => 1932,
                'id_manufacturer' => 241,
            'str_description' => 's 600 (140)',
            'str_description_slug' => 's-600-(140)',
                'bol_active' => 1,
            ),
            432 => 
            array (
                'id_model' => 1933,
                'id_manufacturer' => 241,
            'str_description' => 'sl (129)',
            'str_description_slug' => 'sl-(129)',
                'bol_active' => 1,
            ),
            433 => 
            array (
                'id_model' => 1934,
                'id_manufacturer' => 241,
            'str_description' => 'sl (230)',
            'str_description_slug' => 'sl-(230)',
                'bol_active' => 1,
            ),
            434 => 
            array (
                'id_model' => 1935,
                'id_manufacturer' => 241,
            'str_description' => 'sl (231)',
            'str_description_slug' => 'sl-(231)',
                'bol_active' => 1,
            ),
            435 => 
            array (
                'id_model' => 1936,
                'id_manufacturer' => 241,
            'str_description' => 'sl 230 (113)',
            'str_description_slug' => 'sl-230-(113)',
                'bol_active' => 1,
            ),
            436 => 
            array (
                'id_model' => 1937,
                'id_manufacturer' => 241,
            'str_description' => 'sl 250 (113)',
            'str_description_slug' => 'sl-250-(113)',
                'bol_active' => 1,
            ),
            437 => 
            array (
                'id_model' => 1938,
                'id_manufacturer' => 241,
            'str_description' => 'sl 280 (113)',
            'str_description_slug' => 'sl-280-(113)',
                'bol_active' => 1,
            ),
            438 => 
            array (
                'id_model' => 1939,
                'id_manufacturer' => 241,
            'str_description' => 'sl 280 (129)',
            'str_description_slug' => 'sl-280-(129)',
                'bol_active' => 1,
            ),
            439 => 
            array (
                'id_model' => 1940,
                'id_manufacturer' => 241,
            'str_description' => 'sl 300 (129)',
            'str_description_slug' => 'sl-300-(129)',
                'bol_active' => 1,
            ),
            440 => 
            array (
                'id_model' => 1941,
                'id_manufacturer' => 241,
            'str_description' => 'sl 320 (129)',
            'str_description_slug' => 'sl-320-(129)',
                'bol_active' => 1,
            ),
            441 => 
            array (
                'id_model' => 1942,
                'id_manufacturer' => 241,
            'str_description' => 'sl 500 (129)',
            'str_description_slug' => 'sl-500-(129)',
                'bol_active' => 1,
            ),
            442 => 
            array (
                'id_model' => 1943,
                'id_manufacturer' => 241,
            'str_description' => 'sl 60 (129)',
            'str_description_slug' => 'sl-60-(129)',
                'bol_active' => 1,
            ),
            443 => 
            array (
                'id_model' => 1944,
                'id_manufacturer' => 241,
            'str_description' => 'sl 600 (129)',
            'str_description_slug' => 'sl-600-(129)',
                'bol_active' => 1,
            ),
            444 => 
            array (
                'id_model' => 1945,
                'id_manufacturer' => 241,
                'str_description' => 'slc',
                'str_description_slug' => 'slc',
                'bol_active' => 1,
            ),
            445 => 
            array (
                'id_model' => 1946,
                'id_manufacturer' => 241,
            'str_description' => 'slc (172)',
            'str_description_slug' => 'slc-(172)',
                'bol_active' => 1,
            ),
            446 => 
            array (
                'id_model' => 1947,
                'id_manufacturer' => 241,
            'str_description' => 'slk (172)',
            'str_description_slug' => 'slk-(172)',
                'bol_active' => 1,
            ),
            447 => 
            array (
                'id_model' => 1948,
                'id_manufacturer' => 241,
                'str_description' => 'slr',
                'str_description_slug' => 'slr',
                'bol_active' => 1,
            ),
            448 => 
            array (
                'id_model' => 1949,
                'id_manufacturer' => 241,
                'str_description' => 'sls',
                'str_description_slug' => 'sls',
                'bol_active' => 1,
            ),
            449 => 
            array (
                'id_model' => 1950,
                'id_manufacturer' => 241,
                'str_description' => 'sprinter',
                'str_description_slug' => 'sprinter',
                'bol_active' => 1,
            ),
            450 => 
            array (
                'id_model' => 1951,
                'id_manufacturer' => 241,
                'str_description' => 'sprinter 2',
                'str_description_slug' => 'sprinter-2',
                'bol_active' => 1,
            ),
            451 => 
            array (
                'id_model' => 1952,
                'id_manufacturer' => 241,
                'str_description' => 'ssk',
                'str_description_slug' => 'ssk',
                'bol_active' => 1,
            ),
            452 => 
            array (
                'id_model' => 1953,
                'id_manufacturer' => 241,
                'str_description' => 't-1 208',
                'str_description_slug' => 't-1-208',
                'bol_active' => 1,
            ),
            453 => 
            array (
                'id_model' => 1954,
                'id_manufacturer' => 241,
                'str_description' => 't-1 210',
                'str_description_slug' => 't-1-210',
                'bol_active' => 1,
            ),
            454 => 
            array (
                'id_model' => 1955,
                'id_manufacturer' => 241,
                'str_description' => 't-1 307',
                'str_description_slug' => 't-1-307',
                'bol_active' => 1,
            ),
            455 => 
            array (
                'id_model' => 1956,
                'id_manufacturer' => 241,
                'str_description' => 't-1 308',
                'str_description_slug' => 't-1-308',
                'bol_active' => 1,
            ),
            456 => 
            array (
                'id_model' => 1957,
                'id_manufacturer' => 241,
                'str_description' => 't-1 310',
                'str_description_slug' => 't-1-310',
                'bol_active' => 1,
            ),
            457 => 
            array (
                'id_model' => 1958,
                'id_manufacturer' => 241,
                'str_description' => 't2',
                'str_description_slug' => 't2',
                'bol_active' => 1,
            ),
            458 => 
            array (
                'id_model' => 1959,
                'id_manufacturer' => 241,
                'str_description' => 'unimog',
                'str_description_slug' => 'unimog',
                'bol_active' => 1,
            ),
            459 => 
            array (
                'id_model' => 1960,
                'id_manufacturer' => 241,
            'str_description' => 'v (638-639-447)',
            'str_description_slug' => 'v-(638-639-447)',
                'bol_active' => 1,
            ),
            460 => 
            array (
                'id_model' => 1961,
                'id_manufacturer' => 241,
                'str_description' => 'vaneo',
                'str_description_slug' => 'vaneo',
                'bol_active' => 1,
            ),
            461 => 
            array (
                'id_model' => 1962,
                'id_manufacturer' => 241,
                'str_description' => 'vario',
                'str_description_slug' => 'vario',
                'bol_active' => 1,
            ),
            462 => 
            array (
                'id_model' => 1963,
                'id_manufacturer' => 241,
                'str_description' => 'viano',
                'str_description_slug' => 'viano',
                'bol_active' => 1,
            ),
            463 => 
            array (
                'id_model' => 1964,
                'id_manufacturer' => 242,
                'str_description' => 'bobcat',
                'str_description_slug' => 'bobcat',
                'bol_active' => 1,
            ),
            464 => 
            array (
                'id_model' => 1965,
                'id_manufacturer' => 242,
                'str_description' => 'capri',
                'str_description_slug' => 'capri',
                'bol_active' => 1,
            ),
            465 => 
            array (
                'id_model' => 1966,
                'id_manufacturer' => 242,
                'str_description' => 'comet',
                'str_description_slug' => 'comet',
                'bol_active' => 1,
            ),
            466 => 
            array (
                'id_model' => 1967,
                'id_manufacturer' => 242,
                'str_description' => 'cougar',
                'str_description_slug' => 'cougar',
                'bol_active' => 1,
            ),
            467 => 
            array (
                'id_model' => 1968,
                'id_manufacturer' => 242,
                'str_description' => 'custom',
                'str_description_slug' => 'custom',
                'bol_active' => 1,
            ),
            468 => 
            array (
                'id_model' => 1969,
                'id_manufacturer' => 242,
                'str_description' => 'grand marquis',
                'str_description_slug' => 'grand-marquis',
                'bol_active' => 1,
            ),
            469 => 
            array (
                'id_model' => 1970,
                'id_manufacturer' => 242,
                'str_description' => 'mariner',
                'str_description_slug' => 'mariner',
                'bol_active' => 1,
            ),
            470 => 
            array (
                'id_model' => 1971,
                'id_manufacturer' => 242,
                'str_description' => 'marquis',
                'str_description_slug' => 'marquis',
                'bol_active' => 1,
            ),
            471 => 
            array (
                'id_model' => 1972,
                'id_manufacturer' => 242,
                'str_description' => 'milan',
                'str_description_slug' => 'milan',
                'bol_active' => 1,
            ),
            472 => 
            array (
                'id_model' => 1973,
                'id_manufacturer' => 242,
                'str_description' => 'monarch',
                'str_description_slug' => 'monarch',
                'bol_active' => 1,
            ),
            473 => 
            array (
                'id_model' => 1974,
                'id_manufacturer' => 242,
                'str_description' => 'monterey',
                'str_description_slug' => 'monterey',
                'bol_active' => 1,
            ),
            474 => 
            array (
                'id_model' => 1975,
                'id_manufacturer' => 242,
                'str_description' => 'mountaineer',
                'str_description_slug' => 'mountaineer',
                'bol_active' => 1,
            ),
            475 => 
            array (
                'id_model' => 1976,
                'id_manufacturer' => 242,
                'str_description' => 'mystique',
                'str_description_slug' => 'mystique',
                'bol_active' => 1,
            ),
            476 => 
            array (
                'id_model' => 1977,
                'id_manufacturer' => 242,
                'str_description' => 'park lane',
                'str_description_slug' => 'park-lane',
                'bol_active' => 1,
            ),
            477 => 
            array (
                'id_model' => 1978,
                'id_manufacturer' => 242,
                'str_description' => 'sable',
                'str_description_slug' => 'sable',
                'bol_active' => 1,
            ),
            478 => 
            array (
                'id_model' => 1979,
                'id_manufacturer' => 242,
                'str_description' => 'topaz',
                'str_description_slug' => 'topaz',
                'bol_active' => 1,
            ),
            479 => 
            array (
                'id_model' => 1980,
                'id_manufacturer' => 242,
                'str_description' => 'tracer',
                'str_description_slug' => 'tracer',
                'bol_active' => 1,
            ),
            480 => 
            array (
                'id_model' => 1981,
                'id_manufacturer' => 242,
                'str_description' => 'villager',
                'str_description_slug' => 'villager',
                'bol_active' => 1,
            ),
            481 => 
            array (
                'id_model' => 1982,
                'id_manufacturer' => 242,
                'str_description' => 'zephyr',
                'str_description_slug' => 'zephyr',
                'bol_active' => 1,
            ),
            482 => 
            array (
                'id_model' => 1983,
                'id_manufacturer' => 243,
                'str_description' => 'pocket',
                'str_description_slug' => 'pocket',
                'bol_active' => 1,
            ),
            483 => 
            array (
                'id_model' => 1984,
                'id_manufacturer' => 243,
                'str_description' => 'safari',
                'str_description_slug' => 'safari',
                'bol_active' => 1,
            ),
            484 => 
            array (
                'id_model' => 1985,
                'id_manufacturer' => 243,
                'str_description' => 'sport',
                'str_description_slug' => 'sport',
                'bol_active' => 1,
            ),
            485 => 
            array (
                'id_model' => 1986,
                'id_manufacturer' => 243,
                'str_description' => 'top',
                'str_description_slug' => 'top',
                'bol_active' => 1,
            ),
            486 => 
            array (
                'id_model' => 1987,
                'id_manufacturer' => 244,
                'str_description' => '1100',
                'str_description_slug' => '1100',
                'bol_active' => 1,
            ),
            487 => 
            array (
                'id_model' => 1988,
                'id_manufacturer' => 244,
                'str_description' => '1300',
                'str_description_slug' => '1300',
                'bol_active' => 1,
            ),
            488 => 
            array (
                'id_model' => 1989,
                'id_manufacturer' => 244,
                'str_description' => 'bgt',
                'str_description_slug' => 'bgt',
                'bol_active' => 1,
            ),
            489 => 
            array (
                'id_model' => 1990,
                'id_manufacturer' => 244,
                'str_description' => 'cgt',
                'str_description_slug' => 'cgt',
                'bol_active' => 1,
            ),
            490 => 
            array (
                'id_model' => 1991,
                'id_manufacturer' => 244,
                'str_description' => 'metro',
                'str_description_slug' => 'metro',
                'bol_active' => 1,
            ),
            491 => 
            array (
                'id_model' => 1992,
                'id_manufacturer' => 244,
                'str_description' => 'mga',
                'str_description_slug' => 'mga',
                'bol_active' => 1,
            ),
            492 => 
            array (
                'id_model' => 1993,
                'id_manufacturer' => 244,
                'str_description' => 'mgb',
                'str_description_slug' => 'mgb',
                'bol_active' => 1,
            ),
            493 => 
            array (
                'id_model' => 1994,
                'id_manufacturer' => 244,
                'str_description' => 'midget',
                'str_description_slug' => 'midget',
                'bol_active' => 1,
            ),
            494 => 
            array (
                'id_model' => 1995,
                'id_manufacturer' => 244,
                'str_description' => 'rv8',
                'str_description_slug' => 'rv8',
                'bol_active' => 1,
            ),
            495 => 
            array (
                'id_model' => 1996,
                'id_manufacturer' => 244,
                'str_description' => 'tc',
                'str_description_slug' => 'tc',
                'bol_active' => 1,
            ),
            496 => 
            array (
                'id_model' => 1997,
                'id_manufacturer' => 244,
                'str_description' => 'td',
                'str_description_slug' => 'td',
                'bol_active' => 1,
            ),
            497 => 
            array (
                'id_model' => 1998,
                'id_manufacturer' => 244,
                'str_description' => 'tf',
                'str_description_slug' => 'tf',
                'bol_active' => 1,
            ),
            498 => 
            array (
                'id_model' => 1999,
                'id_manufacturer' => 245,
                'str_description' => 'ch',
                'str_description_slug' => 'ch',
                'bol_active' => 1,
            ),
            499 => 
            array (
                'id_model' => 2000,
                'id_manufacturer' => 245,
                'str_description' => 'due',
                'str_description_slug' => 'due',
                'bol_active' => 1,
            ),
        ));
        \DB::table('automobile_models')->insert(array (
            0 => 
            array (
                'id_model' => 2001,
                'id_manufacturer' => 245,
                'str_description' => 'f8',
                'str_description_slug' => 'f8',
                'bol_active' => 1,
            ),
            1 => 
            array (
                'id_model' => 2002,
                'id_manufacturer' => 245,
                'str_description' => 'flex',
                'str_description_slug' => 'flex',
                'bol_active' => 1,
            ),
            2 => 
            array (
                'id_model' => 2003,
                'id_manufacturer' => 245,
                'str_description' => 'galaxia',
                'str_description_slug' => 'galaxia',
                'bol_active' => 1,
            ),
            3 => 
            array (
                'id_model' => 2004,
                'id_manufacturer' => 245,
                'str_description' => 'highland',
                'str_description_slug' => 'highland',
                'bol_active' => 1,
            ),
            4 => 
            array (
                'id_model' => 2005,
                'id_manufacturer' => 245,
                'str_description' => 'lyra',
                'str_description_slug' => 'lyra',
                'bol_active' => 1,
            ),
            5 => 
            array (
                'id_model' => 2006,
                'id_manufacturer' => 245,
                'str_description' => 'm8',
                'str_description_slug' => 'm8',
                'bol_active' => 1,
            ),
            6 => 
            array (
                'id_model' => 2007,
                'id_manufacturer' => 245,
                'str_description' => 'mc campus',
                'str_description_slug' => 'mc-campus',
                'bol_active' => 1,
            ),
            7 => 
            array (
                'id_model' => 2008,
                'id_manufacturer' => 245,
                'str_description' => 'mc cargo',
                'str_description_slug' => 'mc-cargo',
                'bol_active' => 1,
            ),
            8 => 
            array (
                'id_model' => 2009,
                'id_manufacturer' => 245,
                'str_description' => 'mc city',
                'str_description_slug' => 'mc-city',
                'bol_active' => 1,
            ),
            9 => 
            array (
                'id_model' => 2010,
                'id_manufacturer' => 245,
                'str_description' => 'mc city s',
                'str_description_slug' => 'mc-city-s',
                'bol_active' => 1,
            ),
            10 => 
            array (
                'id_model' => 2011,
                'id_manufacturer' => 245,
                'str_description' => 'mc family',
                'str_description_slug' => 'mc-family',
                'bol_active' => 1,
            ),
            11 => 
            array (
                'id_model' => 2012,
                'id_manufacturer' => 245,
                'str_description' => 'mc1',
                'str_description_slug' => 'mc1',
                'bol_active' => 1,
            ),
            12 => 
            array (
                'id_model' => 2013,
                'id_manufacturer' => 245,
                'str_description' => 'mc2',
                'str_description_slug' => 'mc2',
                'bol_active' => 1,
            ),
            13 => 
            array (
                'id_model' => 2014,
                'id_manufacturer' => 245,
                'str_description' => 'mgo',
                'str_description_slug' => 'mgo',
                'bol_active' => 1,
            ),
            14 => 
            array (
                'id_model' => 2015,
                'id_manufacturer' => 245,
                'str_description' => 'navy',
                'str_description_slug' => 'navy',
                'bol_active' => 1,
            ),
            15 => 
            array (
                'id_model' => 2016,
                'id_manufacturer' => 245,
                'str_description' => 'newstreet',
                'str_description_slug' => 'newstreet',
                'bol_active' => 1,
            ),
            16 => 
            array (
                'id_model' => 2017,
                'id_manufacturer' => 245,
                'str_description' => 'sherpa',
                'str_description_slug' => 'sherpa',
                'bol_active' => 1,
            ),
            17 => 
            array (
                'id_model' => 2018,
                'id_manufacturer' => 245,
                'str_description' => 'speed',
                'str_description_slug' => 'speed',
                'bol_active' => 1,
            ),
            18 => 
            array (
                'id_model' => 2019,
                'id_manufacturer' => 245,
                'str_description' => 'virgo',
                'str_description_slug' => 'virgo',
                'bol_active' => 1,
            ),
            19 => 
            array (
                'id_model' => 2020,
                'id_manufacturer' => 245,
                'str_description' => 'virgo ii',
                'str_description_slug' => 'virgo-ii',
                'bol_active' => 1,
            ),
            20 => 
            array (
                'id_model' => 2021,
                'id_manufacturer' => 245,
                'str_description' => 'virgo iii',
                'str_description_slug' => 'virgo-iii',
                'bol_active' => 1,
            ),
            21 => 
            array (
                'id_model' => 2022,
                'id_manufacturer' => 246,
                'str_description' => 'niza',
                'str_description_slug' => 'niza',
                'bol_active' => 1,
            ),
            22 => 
            array (
                'id_model' => 2023,
                'id_manufacturer' => 247,
                'str_description' => 'coyote',
                'str_description_slug' => 'coyote',
                'bol_active' => 1,
            ),
            23 => 
            array (
                'id_model' => 2024,
                'id_manufacturer' => 248,
                'str_description' => 'minauto',
                'str_description_slug' => 'minauto',
                'bol_active' => 1,
            ),
            24 => 
            array (
                'id_model' => 2025,
                'id_manufacturer' => 249,
                'str_description' => 'hercules',
                'str_description_slug' => 'hercules',
                'bol_active' => 1,
            ),
            25 => 
            array (
                'id_model' => 2026,
                'id_manufacturer' => 250,
                'str_description' => 'countryman',
                'str_description_slug' => 'countryman',
                'bol_active' => 1,
            ),
            26 => 
            array (
                'id_model' => 2027,
                'id_manufacturer' => 250,
                'str_description' => 'mini',
                'str_description_slug' => 'mini',
                'bol_active' => 1,
            ),
            27 => 
            array (
                'id_model' => 2028,
                'id_manufacturer' => 250,
                'str_description' => 'paceman',
                'str_description_slug' => 'paceman',
                'bol_active' => 1,
            ),
            28 => 
            array (
                'id_model' => 2029,
                'id_manufacturer' => 251,
                'str_description' => 'h3',
                'str_description_slug' => 'h3',
                'bol_active' => 1,
            ),
            29 => 
            array (
                'id_model' => 2030,
                'id_manufacturer' => 252,
                'str_description' => '3000 gt',
                'str_description_slug' => '3000-gt',
                'bol_active' => 1,
            ),
            30 => 
            array (
                'id_model' => 2031,
                'id_manufacturer' => 252,
                'str_description' => 'alley',
                'str_description_slug' => 'alley',
                'bol_active' => 1,
            ),
            31 => 
            array (
                'id_model' => 2032,
                'id_manufacturer' => 252,
                'str_description' => 'asx',
                'str_description_slug' => 'asx',
                'bol_active' => 1,
            ),
            32 => 
            array (
                'id_model' => 2033,
                'id_manufacturer' => 252,
                'str_description' => 'canter',
                'str_description_slug' => 'canter',
                'bol_active' => 1,
            ),
            33 => 
            array (
                'id_model' => 2034,
                'id_manufacturer' => 252,
                'str_description' => 'canter fb',
                'str_description_slug' => 'canter-fb',
                'bol_active' => 1,
            ),
            34 => 
            array (
                'id_model' => 2035,
                'id_manufacturer' => 252,
                'str_description' => 'canter fe',
                'str_description_slug' => 'canter-fe',
                'bol_active' => 1,
            ),
            35 => 
            array (
                'id_model' => 2036,
                'id_manufacturer' => 252,
                'str_description' => 'carisma',
                'str_description_slug' => 'carisma',
                'bol_active' => 1,
            ),
            36 => 
            array (
                'id_model' => 2037,
                'id_manufacturer' => 252,
                'str_description' => 'celeste',
                'str_description_slug' => 'celeste',
                'bol_active' => 1,
            ),
            37 => 
            array (
                'id_model' => 2038,
                'id_manufacturer' => 252,
                'str_description' => 'colt',
                'str_description_slug' => 'colt',
                'bol_active' => 1,
            ),
            38 => 
            array (
                'id_model' => 2039,
                'id_manufacturer' => 252,
                'str_description' => 'cordia',
                'str_description_slug' => 'cordia',
                'bol_active' => 1,
            ),
            39 => 
            array (
                'id_model' => 2040,
                'id_manufacturer' => 252,
                'str_description' => 'delica',
                'str_description_slug' => 'delica',
                'bol_active' => 1,
            ),
            40 => 
            array (
                'id_model' => 2041,
                'id_manufacturer' => 252,
                'str_description' => 'delicia',
                'str_description_slug' => 'delicia',
                'bol_active' => 1,
            ),
            41 => 
            array (
                'id_model' => 2042,
                'id_manufacturer' => 252,
                'str_description' => 'diamante',
                'str_description_slug' => 'diamante',
                'bol_active' => 1,
            ),
            42 => 
            array (
                'id_model' => 2043,
                'id_manufacturer' => 252,
                'str_description' => 'dodge stealth',
                'str_description_slug' => 'dodge-stealth',
                'bol_active' => 1,
            ),
            43 => 
            array (
                'id_model' => 2044,
                'id_manufacturer' => 252,
                'str_description' => 'eagle talon',
                'str_description_slug' => 'eagle-talon',
                'bol_active' => 1,
            ),
            44 => 
            array (
                'id_model' => 2045,
                'id_manufacturer' => 252,
                'str_description' => 'eclipse',
                'str_description_slug' => 'eclipse',
                'bol_active' => 1,
            ),
            45 => 
            array (
                'id_model' => 2046,
                'id_manufacturer' => 252,
                'str_description' => 'endeavor',
                'str_description_slug' => 'endeavor',
                'bol_active' => 1,
            ),
            46 => 
            array (
                'id_model' => 2047,
                'id_manufacturer' => 252,
                'str_description' => 'expo',
                'str_description_slug' => 'expo',
                'bol_active' => 1,
            ),
            47 => 
            array (
                'id_model' => 2048,
                'id_manufacturer' => 252,
                'str_description' => 'fto',
                'str_description_slug' => 'fto',
                'bol_active' => 1,
            ),
            48 => 
            array (
                'id_model' => 2049,
                'id_manufacturer' => 252,
                'str_description' => 'galant',
                'str_description_slug' => 'galant',
                'bol_active' => 1,
            ),
            49 => 
            array (
                'id_model' => 2050,
                'id_manufacturer' => 252,
                'str_description' => 'galant sigma',
                'str_description_slug' => 'galant-sigma',
                'bol_active' => 1,
            ),
            50 => 
            array (
                'id_model' => 2051,
                'id_manufacturer' => 252,
                'str_description' => 'grandis',
                'str_description_slug' => 'grandis',
                'bol_active' => 1,
            ),
            51 => 
            array (
                'id_model' => 2052,
                'id_manufacturer' => 252,
                'str_description' => 'gto',
                'str_description_slug' => 'gto',
                'bol_active' => 1,
            ),
            52 => 
            array (
                'id_model' => 2053,
                'id_manufacturer' => 252,
                'str_description' => 'i-miev',
                'str_description_slug' => 'i-miev',
                'bol_active' => 1,
            ),
            53 => 
            array (
                'id_model' => 2054,
                'id_manufacturer' => 252,
                'str_description' => 'j 54',
                'str_description_slug' => 'j-54',
                'bol_active' => 1,
            ),
            54 => 
            array (
                'id_model' => 2055,
                'id_manufacturer' => 252,
                'str_description' => 'l 200',
                'str_description_slug' => 'l-200',
                'bol_active' => 1,
            ),
            55 => 
            array (
                'id_model' => 2056,
                'id_manufacturer' => 252,
                'str_description' => 'l 300',
                'str_description_slug' => 'l-300',
                'bol_active' => 1,
            ),
            56 => 
            array (
                'id_model' => 2057,
                'id_manufacturer' => 252,
                'str_description' => 'l 400',
                'str_description_slug' => 'l-400',
                'bol_active' => 1,
            ),
            57 => 
            array (
                'id_model' => 2058,
                'id_manufacturer' => 252,
                'str_description' => 'lancer',
                'str_description_slug' => 'lancer',
                'bol_active' => 1,
            ),
            58 => 
            array (
                'id_model' => 2059,
                'id_manufacturer' => 252,
                'str_description' => 'mirage',
                'str_description_slug' => 'mirage',
                'bol_active' => 1,
            ),
            59 => 
            array (
                'id_model' => 2060,
                'id_manufacturer' => 252,
                'str_description' => 'montero',
                'str_description_slug' => 'montero',
                'bol_active' => 1,
            ),
            60 => 
            array (
                'id_model' => 2061,
                'id_manufacturer' => 252,
                'str_description' => 'montero io',
                'str_description_slug' => 'montero-io',
                'bol_active' => 1,
            ),
            61 => 
            array (
                'id_model' => 2062,
                'id_manufacturer' => 252,
                'str_description' => 'montero sport',
                'str_description_slug' => 'montero-sport',
                'bol_active' => 1,
            ),
            62 => 
            array (
                'id_model' => 2063,
                'id_manufacturer' => 252,
                'str_description' => 'nativa',
                'str_description_slug' => 'nativa',
                'bol_active' => 1,
            ),
            63 => 
            array (
                'id_model' => 2064,
                'id_manufacturer' => 252,
                'str_description' => 'outlander',
                'str_description_slug' => 'outlander',
                'bol_active' => 1,
            ),
            64 => 
            array (
                'id_model' => 2065,
                'id_manufacturer' => 252,
                'str_description' => 'pajero',
                'str_description_slug' => 'pajero',
                'bol_active' => 1,
            ),
            65 => 
            array (
                'id_model' => 2066,
                'id_manufacturer' => 252,
                'str_description' => 'pajero pinin',
                'str_description_slug' => 'pajero-pinin',
                'bol_active' => 1,
            ),
            66 => 
            array (
                'id_model' => 2067,
                'id_manufacturer' => 252,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            67 => 
            array (
                'id_model' => 2068,
                'id_manufacturer' => 252,
                'str_description' => 'sapporo',
                'str_description_slug' => 'sapporo',
                'bol_active' => 1,
            ),
            68 => 
            array (
                'id_model' => 2069,
                'id_manufacturer' => 252,
                'str_description' => 'shogun',
                'str_description_slug' => 'shogun',
                'bol_active' => 1,
            ),
            69 => 
            array (
                'id_model' => 2070,
                'id_manufacturer' => 252,
                'str_description' => 'shogun pinin',
                'str_description_slug' => 'shogun-pinin',
                'bol_active' => 1,
            ),
            70 => 
            array (
                'id_model' => 2071,
                'id_manufacturer' => 252,
                'str_description' => 'shogun sport',
                'str_description_slug' => 'shogun-sport',
                'bol_active' => 1,
            ),
            71 => 
            array (
                'id_model' => 2072,
                'id_manufacturer' => 252,
                'str_description' => 'sigma',
                'str_description_slug' => 'sigma',
                'bol_active' => 1,
            ),
            72 => 
            array (
                'id_model' => 2073,
                'id_manufacturer' => 252,
                'str_description' => 'space gear',
                'str_description_slug' => 'space-gear',
                'bol_active' => 1,
            ),
            73 => 
            array (
                'id_model' => 2074,
                'id_manufacturer' => 252,
                'str_description' => 'space runner',
                'str_description_slug' => 'space-runner',
                'bol_active' => 1,
            ),
            74 => 
            array (
                'id_model' => 2075,
                'id_manufacturer' => 252,
                'str_description' => 'space star',
                'str_description_slug' => 'space-star',
                'bol_active' => 1,
            ),
            75 => 
            array (
                'id_model' => 2076,
                'id_manufacturer' => 252,
                'str_description' => 'spacewagon',
                'str_description_slug' => 'spacewagon',
                'bol_active' => 1,
            ),
            76 => 
            array (
                'id_model' => 2077,
                'id_manufacturer' => 252,
                'str_description' => 'spyder',
                'str_description_slug' => 'spyder',
                'bol_active' => 1,
            ),
            77 => 
            array (
                'id_model' => 2078,
                'id_manufacturer' => 252,
                'str_description' => 'starion',
                'str_description_slug' => 'starion',
                'bol_active' => 1,
            ),
            78 => 
            array (
                'id_model' => 2079,
                'id_manufacturer' => 252,
                'str_description' => 'tredia',
                'str_description_slug' => 'tredia',
                'bol_active' => 1,
            ),
            79 => 
            array (
                'id_model' => 2080,
                'id_manufacturer' => 252,
                'str_description' => 'valley',
                'str_description_slug' => 'valley',
                'bol_active' => 1,
            ),
            80 => 
            array (
                'id_model' => 2081,
                'id_manufacturer' => 253,
                'str_description' => 'mc',
                'str_description_slug' => 'mc',
                'bol_active' => 1,
            ),
            81 => 
            array (
                'id_model' => 2082,
                'id_manufacturer' => 254,
                'str_description' => 'saga',
                'str_description_slug' => 'saga',
                'bol_active' => 1,
            ),
            82 => 
            array (
                'id_model' => 2083,
                'id_manufacturer' => 255,
                'str_description' => 'safari',
                'str_description_slug' => 'safari',
                'bol_active' => 1,
            ),
            83 => 
            array (
                'id_model' => 2084,
                'id_manufacturer' => 256,
                'str_description' => '6-50',
                'str_description_slug' => '6-50',
                'bol_active' => 1,
            ),
            84 => 
            array (
                'id_model' => 2085,
                'id_manufacturer' => 257,
                'str_description' => '3 wheeler',
                'str_description_slug' => '3-wheeler',
                'bol_active' => 1,
            ),
            85 => 
            array (
                'id_model' => 2086,
                'id_manufacturer' => 257,
                'str_description' => '4/4',
                'str_description_slug' => '4/4',
                'bol_active' => 1,
            ),
            86 => 
            array (
                'id_model' => 2087,
                'id_manufacturer' => 257,
                'str_description' => '1600',
                'str_description_slug' => '1600',
                'bol_active' => 1,
            ),
            87 => 
            array (
                'id_model' => 2088,
                'id_manufacturer' => 257,
                'str_description' => 'aero',
                'str_description_slug' => 'aero',
                'bol_active' => 1,
            ),
            88 => 
            array (
                'id_model' => 2089,
                'id_manufacturer' => 257,
                'str_description' => 'aero 8',
                'str_description_slug' => 'aero-8',
                'bol_active' => 1,
            ),
            89 => 
            array (
                'id_model' => 2090,
                'id_manufacturer' => 257,
                'str_description' => 'four four',
                'str_description_slug' => 'four-four',
                'bol_active' => 1,
            ),
            90 => 
            array (
                'id_model' => 2091,
                'id_manufacturer' => 257,
                'str_description' => 'plus eight',
                'str_description_slug' => 'plus-eight',
                'bol_active' => 1,
            ),
            91 => 
            array (
                'id_model' => 2092,
                'id_manufacturer' => 257,
                'str_description' => 'plus four',
                'str_description_slug' => 'plus-four',
                'bol_active' => 1,
            ),
            92 => 
            array (
                'id_model' => 2093,
                'id_manufacturer' => 257,
                'str_description' => 'roadster v6',
                'str_description_slug' => 'roadster-v6',
                'bol_active' => 1,
            ),
            93 => 
            array (
                'id_model' => 2094,
                'id_manufacturer' => 258,
                'str_description' => '1100',
                'str_description_slug' => '1100',
                'bol_active' => 1,
            ),
            94 => 
            array (
                'id_model' => 2095,
                'id_manufacturer' => 258,
                'str_description' => '1300',
                'str_description_slug' => '1300',
                'bol_active' => 1,
            ),
            95 => 
            array (
                'id_model' => 2096,
                'id_manufacturer' => 258,
                'str_description' => '1800',
                'str_description_slug' => '1800',
                'bol_active' => 1,
            ),
            96 => 
            array (
                'id_model' => 2097,
                'id_manufacturer' => 258,
                'str_description' => 'eight',
                'str_description_slug' => 'eight',
                'bol_active' => 1,
            ),
            97 => 
            array (
                'id_model' => 2098,
                'id_manufacturer' => 258,
                'str_description' => 'marina',
                'str_description_slug' => 'marina',
                'bol_active' => 1,
            ),
            98 => 
            array (
                'id_model' => 2099,
                'id_manufacturer' => 258,
                'str_description' => 'mini',
                'str_description_slug' => 'mini',
                'bol_active' => 1,
            ),
            99 => 
            array (
                'id_model' => 2100,
                'id_manufacturer' => 258,
                'str_description' => 'minor',
                'str_description_slug' => 'minor',
                'bol_active' => 1,
            ),
            100 => 
            array (
                'id_model' => 2101,
                'id_manufacturer' => 258,
                'str_description' => 'oxford',
                'str_description_slug' => 'oxford',
                'bol_active' => 1,
            ),
            101 => 
            array (
                'id_model' => 2102,
                'id_manufacturer' => 258,
                'str_description' => 'saloon',
                'str_description_slug' => 'saloon',
                'bol_active' => 1,
            ),
            102 => 
            array (
                'id_model' => 2103,
                'id_manufacturer' => 259,
                'str_description' => '2141',
                'str_description_slug' => '2141',
                'bol_active' => 1,
            ),
            103 => 
            array (
                'id_model' => 2104,
                'id_manufacturer' => 259,
                'str_description' => '2715',
                'str_description_slug' => '2715',
                'bol_active' => 1,
            ),
            104 => 
            array (
                'id_model' => 2105,
                'id_manufacturer' => 260,
                'str_description' => 'namib',
                'str_description_slug' => 'namib',
                'bol_active' => 1,
            ),
            105 => 
            array (
                'id_model' => 2106,
                'id_manufacturer' => 261,
                'str_description' => 'b',
                'str_description_slug' => 'b',
                'bol_active' => 1,
            ),
            106 => 
            array (
                'id_model' => 2107,
                'id_manufacturer' => 262,
                'str_description' => 'lafer',
                'str_description_slug' => 'lafer',
                'bol_active' => 1,
            ),
            107 => 
            array (
                'id_model' => 2108,
                'id_manufacturer' => 263,
                'str_description' => 'buggy',
                'str_description_slug' => 'buggy',
                'bol_active' => 1,
            ),
            108 => 
            array (
                'id_model' => 2109,
                'id_manufacturer' => 264,
                'str_description' => 'm 26',
                'str_description_slug' => 'm-26',
                'bol_active' => 1,
            ),
            109 => 
            array (
                'id_model' => 2110,
                'id_manufacturer' => 265,
                'str_description' => 'koala 6',
                'str_description_slug' => 'koala-6',
                'bol_active' => 1,
            ),
            110 => 
            array (
                'id_model' => 2111,
                'id_manufacturer' => 266,
                'str_description' => 'monster',
                'str_description_slug' => 'monster',
                'bol_active' => 1,
            ),
            111 => 
            array (
                'id_model' => 2112,
                'id_manufacturer' => 266,
                'str_description' => 'stormy',
                'str_description_slug' => 'stormy',
                'bol_active' => 1,
            ),
            112 => 
            array (
                'id_model' => 2113,
                'id_manufacturer' => 267,
                'str_description' => 'e2',
                'str_description_slug' => 'e2',
                'bol_active' => 1,
            ),
            113 => 
            array (
                'id_model' => 2114,
                'id_manufacturer' => 268,
                'str_description' => 'go kart',
                'str_description_slug' => 'go-kart',
                'bol_active' => 1,
            ),
            114 => 
            array (
                'id_model' => 2115,
                'id_manufacturer' => 269,
                'str_description' => 'light six',
                'str_description_slug' => 'light-six',
                'bol_active' => 1,
            ),
            115 => 
            array (
                'id_model' => 2116,
                'id_manufacturer' => 269,
                'str_description' => 'metropolitan',
                'str_description_slug' => 'metropolitan',
                'bol_active' => 1,
            ),
            116 => 
            array (
                'id_model' => 2117,
                'id_manufacturer' => 269,
                'str_description' => 'torpedo',
                'str_description_slug' => 'torpedo',
                'bol_active' => 1,
            ),
            117 => 
            array (
                'id_model' => 2118,
                'id_manufacturer' => 270,
                'str_description' => 'lk',
                'str_description_slug' => 'lk',
                'bol_active' => 1,
            ),
            118 => 
            array (
                'id_model' => 2119,
                'id_manufacturer' => 271,
                'str_description' => '100 nx',
                'str_description_slug' => '100-nx',
                'bol_active' => 1,
            ),
            119 => 
            array (
                'id_model' => 2120,
                'id_manufacturer' => 271,
                'str_description' => '200 sx',
                'str_description_slug' => '200-sx',
                'bol_active' => 1,
            ),
            120 => 
            array (
                'id_model' => 2121,
                'id_manufacturer' => 271,
                'str_description' => '240 sx',
                'str_description_slug' => '240-sx',
                'bol_active' => 1,
            ),
            121 => 
            array (
                'id_model' => 2122,
                'id_manufacturer' => 271,
                'str_description' => '280 zx',
                'str_description_slug' => '280-zx',
                'bol_active' => 1,
            ),
            122 => 
            array (
                'id_model' => 2123,
                'id_manufacturer' => 271,
                'str_description' => '300 zx',
                'str_description_slug' => '300-zx',
                'bol_active' => 1,
            ),
            123 => 
            array (
                'id_model' => 2124,
                'id_manufacturer' => 271,
                'str_description' => '350 z',
                'str_description_slug' => '350-z',
                'bol_active' => 1,
            ),
            124 => 
            array (
                'id_model' => 2125,
                'id_manufacturer' => 271,
                'str_description' => '370 z',
                'str_description_slug' => '370-z',
                'bol_active' => 1,
            ),
            125 => 
            array (
                'id_model' => 2126,
                'id_manufacturer' => 271,
                'str_description' => 'almera classic',
                'str_description_slug' => 'almera-classic',
                'bol_active' => 1,
            ),
            126 => 
            array (
                'id_model' => 2127,
                'id_manufacturer' => 271,
                'str_description' => 'altima',
                'str_description_slug' => 'altima',
                'bol_active' => 1,
            ),
            127 => 
            array (
                'id_model' => 2128,
                'id_manufacturer' => 271,
                'str_description' => 'armada',
                'str_description_slug' => 'armada',
                'bol_active' => 1,
            ),
            128 => 
            array (
                'id_model' => 2129,
                'id_manufacturer' => 271,
                'str_description' => 'atleon',
                'str_description_slug' => 'atleon',
                'bol_active' => 1,
            ),
            129 => 
            array (
                'id_model' => 2130,
                'id_manufacturer' => 271,
                'str_description' => 'bluebird',
                'str_description_slug' => 'bluebird',
                'bol_active' => 1,
            ),
            130 => 
            array (
                'id_model' => 2131,
                'id_manufacturer' => 271,
                'str_description' => 'cabstar',
                'str_description_slug' => 'cabstar',
                'bol_active' => 1,
            ),
            131 => 
            array (
                'id_model' => 2132,
                'id_manufacturer' => 271,
                'str_description' => 'cabstar e',
                'str_description_slug' => 'cabstar-e',
                'bol_active' => 1,
            ),
            132 => 
            array (
                'id_model' => 2133,
                'id_manufacturer' => 271,
                'str_description' => 'cedric',
                'str_description_slug' => 'cedric',
                'bol_active' => 1,
            ),
            133 => 
            array (
                'id_model' => 2134,
                'id_manufacturer' => 271,
                'str_description' => 'cherry',
                'str_description_slug' => 'cherry',
                'bol_active' => 1,
            ),
            134 => 
            array (
                'id_model' => 2135,
                'id_manufacturer' => 271,
                'str_description' => 'cube',
                'str_description_slug' => 'cube',
                'bol_active' => 1,
            ),
            135 => 
            array (
                'id_model' => 2136,
                'id_manufacturer' => 271,
                'str_description' => 'e 1000',
                'str_description_slug' => 'e-1000',
                'bol_active' => 1,
            ),
            136 => 
            array (
                'id_model' => 2137,
                'id_manufacturer' => 271,
                'str_description' => 'e 35',
                'str_description_slug' => 'e-35',
                'bol_active' => 1,
            ),
            137 => 
            array (
                'id_model' => 2138,
                'id_manufacturer' => 271,
                'str_description' => 'e 350',
                'str_description_slug' => 'e-350',
                'bol_active' => 1,
            ),
            138 => 
            array (
                'id_model' => 2139,
                'id_manufacturer' => 271,
                'str_description' => 'e 60',
                'str_description_slug' => 'e-60',
                'bol_active' => 1,
            ),
            139 => 
            array (
                'id_model' => 2140,
                'id_manufacturer' => 271,
                'str_description' => 'e-nv200',
                'str_description_slug' => 'e-nv200',
                'bol_active' => 1,
            ),
            140 => 
            array (
                'id_model' => 2141,
                'id_manufacturer' => 271,
                'str_description' => 'eco-t',
                'str_description_slug' => 'eco-t',
                'bol_active' => 1,
            ),
            141 => 
            array (
                'id_model' => 2142,
                'id_manufacturer' => 271,
                'str_description' => 'elgrand',
                'str_description_slug' => 'elgrand',
                'bol_active' => 1,
            ),
            142 => 
            array (
                'id_model' => 2143,
                'id_manufacturer' => 271,
                'str_description' => 'evalia',
                'str_description_slug' => 'evalia',
                'bol_active' => 1,
            ),
            143 => 
            array (
                'id_model' => 2144,
                'id_manufacturer' => 271,
                'str_description' => 'f 108',
                'str_description_slug' => 'f-108',
                'bol_active' => 1,
            ),
            144 => 
            array (
                'id_model' => 2145,
                'id_manufacturer' => 271,
                'str_description' => 'f 260',
                'str_description_slug' => 'f-260',
                'bol_active' => 1,
            ),
            145 => 
            array (
                'id_model' => 2146,
                'id_manufacturer' => 271,
                'str_description' => 'f 275',
                'str_description_slug' => 'f-275',
                'bol_active' => 1,
            ),
            146 => 
            array (
                'id_model' => 2147,
                'id_manufacturer' => 271,
                'str_description' => 'f 350',
                'str_description_slug' => 'f-350',
                'bol_active' => 1,
            ),
            147 => 
            array (
                'id_model' => 2148,
                'id_manufacturer' => 271,
                'str_description' => 'fair lady',
                'str_description_slug' => 'fair-lady',
                'bol_active' => 1,
            ),
            148 => 
            array (
                'id_model' => 2149,
                'id_manufacturer' => 271,
                'str_description' => 'figaro',
                'str_description_slug' => 'figaro',
                'bol_active' => 1,
            ),
            149 => 
            array (
                'id_model' => 2150,
                'id_manufacturer' => 271,
                'str_description' => 'frontier',
                'str_description_slug' => 'frontier',
                'bol_active' => 1,
            ),
            150 => 
            array (
                'id_model' => 2151,
                'id_manufacturer' => 271,
                'str_description' => 'gt-r',
                'str_description_slug' => 'gt-r',
                'bol_active' => 1,
            ),
            151 => 
            array (
                'id_model' => 2152,
                'id_manufacturer' => 271,
                'str_description' => 'infiniti',
                'str_description_slug' => 'infiniti',
                'bol_active' => 1,
            ),
            152 => 
            array (
                'id_model' => 2153,
                'id_manufacturer' => 271,
                'str_description' => 'interstar',
                'str_description_slug' => 'interstar',
                'bol_active' => 1,
            ),
            153 => 
            array (
                'id_model' => 2154,
                'id_manufacturer' => 271,
                'str_description' => 'iubize',
                'str_description_slug' => 'iubize',
                'bol_active' => 1,
            ),
            154 => 
            array (
                'id_model' => 2155,
                'id_manufacturer' => 271,
                'str_description' => 'juke',
                'str_description_slug' => 'juke',
                'bol_active' => 1,
            ),
            155 => 
            array (
                'id_model' => 2156,
                'id_manufacturer' => 271,
                'str_description' => 'king',
                'str_description_slug' => 'king',
                'bol_active' => 1,
            ),
            156 => 
            array (
                'id_model' => 2157,
                'id_manufacturer' => 271,
                'str_description' => 'kubistar',
                'str_description_slug' => 'kubistar',
                'bol_active' => 1,
            ),
            157 => 
            array (
                'id_model' => 2158,
                'id_manufacturer' => 271,
                'str_description' => 'l',
                'str_description_slug' => 'l',
                'bol_active' => 1,
            ),
            158 => 
            array (
                'id_model' => 2159,
                'id_manufacturer' => 271,
                'str_description' => 'l-35.08',
                'str_description_slug' => 'l-35.08',
                'bol_active' => 1,
            ),
            159 => 
            array (
                'id_model' => 2160,
                'id_manufacturer' => 271,
                'str_description' => 'l-35.09',
                'str_description_slug' => 'l-35.09',
                'bol_active' => 1,
            ),
            160 => 
            array (
                'id_model' => 2161,
                'id_manufacturer' => 271,
                'str_description' => 'largo',
                'str_description_slug' => 'largo',
                'bol_active' => 1,
            ),
            161 => 
            array (
                'id_model' => 2162,
                'id_manufacturer' => 271,
                'str_description' => 'laurel',
                'str_description_slug' => 'laurel',
                'bol_active' => 1,
            ),
            162 => 
            array (
                'id_model' => 2163,
                'id_manufacturer' => 271,
                'str_description' => 'leaf',
                'str_description_slug' => 'leaf',
                'bol_active' => 1,
            ),
            163 => 
            array (
                'id_model' => 2164,
                'id_manufacturer' => 271,
                'str_description' => 'maxima',
                'str_description_slug' => 'maxima',
                'bol_active' => 1,
            ),
            164 => 
            array (
                'id_model' => 2165,
                'id_manufacturer' => 271,
                'str_description' => 'maxima qx',
                'str_description_slug' => 'maxima-qx',
                'bol_active' => 1,
            ),
            165 => 
            array (
                'id_model' => 2166,
                'id_manufacturer' => 271,
                'str_description' => 'mistral',
                'str_description_slug' => 'mistral',
                'bol_active' => 1,
            ),
            166 => 
            array (
                'id_model' => 2167,
                'id_manufacturer' => 271,
                'str_description' => 'murano',
                'str_description_slug' => 'murano',
                'bol_active' => 1,
            ),
            167 => 
            array (
                'id_model' => 2168,
                'id_manufacturer' => 271,
                'str_description' => 'navara',
                'str_description_slug' => 'navara',
                'bol_active' => 1,
            ),
            168 => 
            array (
                'id_model' => 2169,
                'id_manufacturer' => 271,
                'str_description' => 'np300',
                'str_description_slug' => 'np300',
                'bol_active' => 1,
            ),
            169 => 
            array (
                'id_model' => 2170,
                'id_manufacturer' => 271,
                'str_description' => 'np300 navara',
                'str_description_slug' => 'np300-navara',
                'bol_active' => 1,
            ),
            170 => 
            array (
                'id_model' => 2171,
                'id_manufacturer' => 271,
                'str_description' => 'nt400',
                'str_description_slug' => 'nt400',
                'bol_active' => 1,
            ),
            171 => 
            array (
                'id_model' => 2172,
                'id_manufacturer' => 271,
                'str_description' => 'nt400 cabstar',
                'str_description_slug' => 'nt400-cabstar',
                'bol_active' => 1,
            ),
            172 => 
            array (
                'id_model' => 2173,
                'id_manufacturer' => 271,
                'str_description' => 'nt500',
                'str_description_slug' => 'nt500',
                'bol_active' => 1,
            ),
            173 => 
            array (
                'id_model' => 2174,
                'id_manufacturer' => 271,
                'str_description' => 'nv 200',
                'str_description_slug' => 'nv-200',
                'bol_active' => 1,
            ),
            174 => 
            array (
                'id_model' => 2175,
                'id_manufacturer' => 271,
                'str_description' => 'nv 400',
                'str_description_slug' => 'nv-400',
                'bol_active' => 1,
            ),
            175 => 
            array (
                'id_model' => 2176,
                'id_manufacturer' => 271,
                'str_description' => 'pathfinder',
                'str_description_slug' => 'pathfinder',
                'bol_active' => 1,
            ),
            176 => 
            array (
                'id_model' => 2177,
                'id_manufacturer' => 271,
                'str_description' => 'patrol gr',
                'str_description_slug' => 'patrol-gr',
                'bol_active' => 1,
            ),
            177 => 
            array (
                'id_model' => 2178,
                'id_manufacturer' => 271,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            178 => 
            array (
                'id_model' => 2179,
                'id_manufacturer' => 271,
                'str_description' => 'pixo',
                'str_description_slug' => 'pixo',
                'bol_active' => 1,
            ),
            179 => 
            array (
                'id_model' => 2180,
                'id_manufacturer' => 271,
                'str_description' => 'prairie',
                'str_description_slug' => 'prairie',
                'bol_active' => 1,
            ),
            180 => 
            array (
                'id_model' => 2181,
                'id_manufacturer' => 271,
                'str_description' => 'primastar',
                'str_description_slug' => 'primastar',
                'bol_active' => 1,
            ),
            181 => 
            array (
                'id_model' => 2182,
                'id_manufacturer' => 271,
                'str_description' => 'pulsar',
                'str_description_slug' => 'pulsar',
                'bol_active' => 1,
            ),
            182 => 
            array (
                'id_model' => 2183,
                'id_manufacturer' => 271,
                'str_description' => 'quest',
                'str_description_slug' => 'quest',
                'bol_active' => 1,
            ),
            183 => 
            array (
                'id_model' => 2184,
                'id_manufacturer' => 271,
                'str_description' => 'rogue',
                'str_description_slug' => 'rogue',
                'bol_active' => 1,
            ),
            184 => 
            array (
                'id_model' => 2185,
                'id_manufacturer' => 271,
                'str_description' => 'sentra',
                'str_description_slug' => 'sentra',
                'bol_active' => 1,
            ),
            185 => 
            array (
                'id_model' => 2186,
                'id_manufacturer' => 271,
                'str_description' => 'serena',
                'str_description_slug' => 'serena',
                'bol_active' => 1,
            ),
            186 => 
            array (
                'id_model' => 2187,
                'id_manufacturer' => 271,
                'str_description' => 'silvia',
                'str_description_slug' => 'silvia',
                'bol_active' => 1,
            ),
            187 => 
            array (
                'id_model' => 2188,
                'id_manufacturer' => 271,
                'str_description' => 'skyline',
                'str_description_slug' => 'skyline',
                'bol_active' => 1,
            ),
            188 => 
            array (
                'id_model' => 2189,
                'id_manufacturer' => 271,
                'str_description' => 'stanza',
                'str_description_slug' => 'stanza',
                'bol_active' => 1,
            ),
            189 => 
            array (
                'id_model' => 2190,
                'id_manufacturer' => 271,
                'str_description' => 'sunny',
                'str_description_slug' => 'sunny',
                'bol_active' => 1,
            ),
            190 => 
            array (
                'id_model' => 2191,
                'id_manufacturer' => 271,
                'str_description' => 'teana',
                'str_description_slug' => 'teana',
                'bol_active' => 1,
            ),
            191 => 
            array (
                'id_model' => 2192,
                'id_manufacturer' => 271,
                'str_description' => 'terrano',
                'str_description_slug' => 'terrano',
                'bol_active' => 1,
            ),
            192 => 
            array (
                'id_model' => 2193,
                'id_manufacturer' => 271,
                'str_description' => 'tiida',
                'str_description_slug' => 'tiida',
                'bol_active' => 1,
            ),
            193 => 
            array (
                'id_model' => 2194,
                'id_manufacturer' => 271,
                'str_description' => 'titan',
                'str_description_slug' => 'titan',
                'bol_active' => 1,
            ),
            194 => 
            array (
                'id_model' => 2195,
                'id_manufacturer' => 271,
                'str_description' => 'trade',
                'str_description_slug' => 'trade',
                'bol_active' => 1,
            ),
            195 => 
            array (
                'id_model' => 2196,
                'id_manufacturer' => 271,
                'str_description' => 'tsuru',
                'str_description_slug' => 'tsuru',
                'bol_active' => 1,
            ),
            196 => 
            array (
                'id_model' => 2197,
                'id_manufacturer' => 271,
                'str_description' => 'urvan',
                'str_description_slug' => 'urvan',
                'bol_active' => 1,
            ),
            197 => 
            array (
                'id_model' => 2198,
                'id_manufacturer' => 271,
                'str_description' => 'versa',
                'str_description_slug' => 'versa',
                'bol_active' => 1,
            ),
            198 => 
            array (
                'id_model' => 2199,
                'id_manufacturer' => 271,
                'str_description' => 'x-terra',
                'str_description_slug' => 'x-terra',
                'bol_active' => 1,
            ),
            199 => 
            array (
                'id_model' => 2200,
                'id_manufacturer' => 271,
                'str_description' => 'x-trail',
                'str_description_slug' => 'x-trail',
                'bol_active' => 1,
            ),
            200 => 
            array (
                'id_model' => 2201,
                'id_manufacturer' => 272,
                'str_description' => 'prinz',
                'str_description_slug' => 'prinz',
                'bol_active' => 1,
            ),
            201 => 
            array (
                'id_model' => 2202,
                'id_manufacturer' => 272,
                'str_description' => 'ro 80',
                'str_description_slug' => 'ro-80',
                'bol_active' => 1,
            ),
            202 => 
            array (
                'id_model' => 2203,
                'id_manufacturer' => 272,
                'str_description' => 'sport print',
                'str_description_slug' => 'sport-print',
                'bol_active' => 1,
            ),
            203 => 
            array (
                'id_model' => 2204,
                'id_manufacturer' => 273,
                'str_description' => 'om 32',
                'str_description_slug' => 'om-32',
                'bol_active' => 1,
            ),
            204 => 
            array (
                'id_model' => 2205,
                'id_manufacturer' => 273,
                'str_description' => 'om 40',
                'str_description_slug' => 'om-40',
                'bol_active' => 1,
            ),
            205 => 
            array (
                'id_model' => 2206,
                'id_manufacturer' => 274,
                'str_description' => 'doble phaeton',
                'str_description_slug' => 'doble-phaeton',
                'bol_active' => 1,
            ),
            206 => 
            array (
                'id_model' => 2207,
                'id_manufacturer' => 275,
                'str_description' => 'gippi',
                'str_description_slug' => 'gippi',
                'bol_active' => 1,
            ),
            207 => 
            array (
                'id_model' => 2208,
                'id_manufacturer' => 276,
                'str_description' => 'alero',
                'str_description_slug' => 'alero',
                'bol_active' => 1,
            ),
            208 => 
            array (
                'id_model' => 2209,
                'id_manufacturer' => 276,
                'str_description' => 'aurora',
                'str_description_slug' => 'aurora',
                'bol_active' => 1,
            ),
            209 => 
            array (
                'id_model' => 2210,
                'id_manufacturer' => 276,
                'str_description' => 'bravada',
                'str_description_slug' => 'bravada',
                'bol_active' => 1,
            ),
            210 => 
            array (
                'id_model' => 2211,
                'id_manufacturer' => 276,
                'str_description' => 'calais',
                'str_description_slug' => 'calais',
                'bol_active' => 1,
            ),
            211 => 
            array (
                'id_model' => 2212,
                'id_manufacturer' => 276,
                'str_description' => 'cutlass',
                'str_description_slug' => 'cutlass',
                'bol_active' => 1,
            ),
            212 => 
            array (
                'id_model' => 2213,
                'id_manufacturer' => 276,
                'str_description' => 'delta 88',
                'str_description_slug' => 'delta-88',
                'bol_active' => 1,
            ),
            213 => 
            array (
                'id_model' => 2214,
                'id_manufacturer' => 276,
                'str_description' => 'dinamic',
                'str_description_slug' => 'dinamic',
                'bol_active' => 1,
            ),
            214 => 
            array (
                'id_model' => 2215,
                'id_manufacturer' => 276,
                'str_description' => 'f',
                'str_description_slug' => 'f',
                'bol_active' => 1,
            ),
            215 => 
            array (
                'id_model' => 2216,
                'id_manufacturer' => 276,
                'str_description' => 'firenza',
                'str_description_slug' => 'firenza',
                'bol_active' => 1,
            ),
            216 => 
            array (
                'id_model' => 2217,
                'id_manufacturer' => 276,
                'str_description' => 'ninety eight',
                'str_description_slug' => 'ninety-eight',
                'bol_active' => 1,
            ),
            217 => 
            array (
                'id_model' => 2218,
                'id_manufacturer' => 276,
                'str_description' => 'omega',
                'str_description_slug' => 'omega',
                'bol_active' => 1,
            ),
            218 => 
            array (
                'id_model' => 2219,
                'id_manufacturer' => 276,
                'str_description' => 'regency',
                'str_description_slug' => 'regency',
                'bol_active' => 1,
            ),
            219 => 
            array (
                'id_model' => 2220,
                'id_manufacturer' => 276,
                'str_description' => 'silhouette',
                'str_description_slug' => 'silhouette',
                'bol_active' => 1,
            ),
            220 => 
            array (
                'id_model' => 2221,
                'id_manufacturer' => 276,
                'str_description' => 'starfire',
                'str_description_slug' => 'starfire',
                'bol_active' => 1,
            ),
            221 => 
            array (
                'id_model' => 2222,
                'id_manufacturer' => 276,
                'str_description' => 'toronado',
                'str_description_slug' => 'toronado',
                'bol_active' => 1,
            ),
            222 => 
            array (
                'id_model' => 2223,
                'id_manufacturer' => 276,
                'str_description' => 'toronto',
                'str_description_slug' => 'toronto',
                'bol_active' => 1,
            ),
            223 => 
            array (
                'id_model' => 2224,
                'id_manufacturer' => 277,
                'str_description' => 'e-bro',
                'str_description_slug' => 'e-bro',
                'bol_active' => 1,
            ),
            224 => 
            array (
                'id_model' => 2225,
                'id_manufacturer' => 278,
                'str_description' => '18',
                'str_description_slug' => '18',
                'bol_active' => 1,
            ),
            225 => 
            array (
                'id_model' => 2226,
                'id_manufacturer' => 278,
                'str_description' => 'adam',
                'str_description_slug' => 'adam',
                'bol_active' => 1,
            ),
            226 => 
            array (
                'id_model' => 2227,
                'id_manufacturer' => 278,
                'str_description' => 'admiral',
                'str_description_slug' => 'admiral',
                'bol_active' => 1,
            ),
            227 => 
            array (
                'id_model' => 2228,
                'id_manufacturer' => 278,
                'str_description' => 'agila',
                'str_description_slug' => 'agila',
                'bol_active' => 1,
            ),
            228 => 
            array (
                'id_model' => 2229,
                'id_manufacturer' => 278,
                'str_description' => 'antara',
                'str_description_slug' => 'antara',
                'bol_active' => 1,
            ),
            229 => 
            array (
                'id_model' => 2230,
                'id_manufacturer' => 278,
                'str_description' => 'arena',
                'str_description_slug' => 'arena',
                'bol_active' => 1,
            ),
            230 => 
            array (
                'id_model' => 2231,
                'id_manufacturer' => 278,
                'str_description' => 'ascona',
                'str_description_slug' => 'ascona',
                'bol_active' => 1,
            ),
            231 => 
            array (
                'id_model' => 2232,
                'id_manufacturer' => 278,
                'str_description' => 'astra twin top',
                'str_description_slug' => 'astra-twin-top',
                'bol_active' => 1,
            ),
            232 => 
            array (
                'id_model' => 2233,
                'id_manufacturer' => 278,
                'str_description' => 'blitz',
                'str_description_slug' => 'blitz',
                'bol_active' => 1,
            ),
            233 => 
            array (
                'id_model' => 2234,
                'id_manufacturer' => 278,
                'str_description' => 'cabrio',
                'str_description_slug' => 'cabrio',
                'bol_active' => 1,
            ),
            234 => 
            array (
                'id_model' => 2235,
                'id_manufacturer' => 278,
                'str_description' => 'calibra',
                'str_description_slug' => 'calibra',
                'bol_active' => 1,
            ),
            235 => 
            array (
                'id_model' => 2236,
                'id_manufacturer' => 278,
                'str_description' => 'campo',
                'str_description_slug' => 'campo',
                'bol_active' => 1,
            ),
            236 => 
            array (
                'id_model' => 2237,
                'id_manufacturer' => 278,
                'str_description' => 'capitan',
                'str_description_slug' => 'capitan',
                'bol_active' => 1,
            ),
            237 => 
            array (
                'id_model' => 2238,
                'id_manufacturer' => 278,
                'str_description' => 'commodore',
                'str_description_slug' => 'commodore',
                'bol_active' => 1,
            ),
            238 => 
            array (
                'id_model' => 2239,
                'id_manufacturer' => 278,
                'str_description' => 'diplomat',
                'str_description_slug' => 'diplomat',
                'bol_active' => 1,
            ),
            239 => 
            array (
                'id_model' => 2240,
                'id_manufacturer' => 278,
                'str_description' => 'frontera',
                'str_description_slug' => 'frontera',
                'bol_active' => 1,
            ),
            240 => 
            array (
                'id_model' => 2241,
                'id_manufacturer' => 278,
                'str_description' => 'gt',
                'str_description_slug' => 'gt',
                'bol_active' => 1,
            ),
            241 => 
            array (
                'id_model' => 2242,
                'id_manufacturer' => 278,
                'str_description' => 'insignia country tou',
                'str_description_slug' => 'insignia-country-tou',
                'bol_active' => 1,
            ),
            242 => 
            array (
                'id_model' => 2243,
                'id_manufacturer' => 278,
                'str_description' => 'kapitan',
                'str_description_slug' => 'kapitan',
                'bol_active' => 1,
            ),
            243 => 
            array (
                'id_model' => 2244,
                'id_manufacturer' => 278,
                'str_description' => 'karl',
                'str_description_slug' => 'karl',
                'bol_active' => 1,
            ),
            244 => 
            array (
                'id_model' => 2245,
                'id_manufacturer' => 278,
                'str_description' => 'lotus omega',
                'str_description_slug' => 'lotus-omega',
                'bol_active' => 1,
            ),
            245 => 
            array (
                'id_model' => 2246,
                'id_manufacturer' => 278,
                'str_description' => 'manta',
                'str_description_slug' => 'manta',
                'bol_active' => 1,
            ),
            246 => 
            array (
                'id_model' => 2247,
                'id_manufacturer' => 278,
                'str_description' => 'mokka',
                'str_description_slug' => 'mokka',
                'bol_active' => 1,
            ),
            247 => 
            array (
                'id_model' => 2248,
                'id_manufacturer' => 278,
                'str_description' => 'mokka x',
                'str_description_slug' => 'mokka-x',
                'bol_active' => 1,
            ),
            248 => 
            array (
                'id_model' => 2249,
                'id_manufacturer' => 278,
                'str_description' => 'monterey',
                'str_description_slug' => 'monterey',
                'bol_active' => 1,
            ),
            249 => 
            array (
                'id_model' => 2250,
                'id_manufacturer' => 278,
                'str_description' => 'monza',
                'str_description_slug' => 'monza',
                'bol_active' => 1,
            ),
            250 => 
            array (
                'id_model' => 2251,
                'id_manufacturer' => 278,
                'str_description' => 'movano',
                'str_description_slug' => 'movano',
                'bol_active' => 1,
            ),
            251 => 
            array (
                'id_model' => 2252,
                'id_manufacturer' => 278,
                'str_description' => 'olympia',
                'str_description_slug' => 'olympia',
                'bol_active' => 1,
            ),
            252 => 
            array (
                'id_model' => 2253,
                'id_manufacturer' => 278,
                'str_description' => 'omega',
                'str_description_slug' => 'omega',
                'bol_active' => 1,
            ),
            253 => 
            array (
                'id_model' => 2254,
                'id_manufacturer' => 278,
                'str_description' => 'p4',
                'str_description_slug' => 'p4',
                'bol_active' => 1,
            ),
            254 => 
            array (
                'id_model' => 2255,
                'id_manufacturer' => 278,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            255 => 
            array (
                'id_model' => 2256,
                'id_manufacturer' => 278,
                'str_description' => 'rekord',
                'str_description_slug' => 'rekord',
                'bol_active' => 1,
            ),
            256 => 
            array (
                'id_model' => 2257,
                'id_manufacturer' => 278,
                'str_description' => 'senator',
                'str_description_slug' => 'senator',
                'bol_active' => 1,
            ),
            257 => 
            array (
                'id_model' => 2258,
                'id_manufacturer' => 278,
                'str_description' => 'signum',
                'str_description_slug' => 'signum',
                'bol_active' => 1,
            ),
            258 => 
            array (
                'id_model' => 2259,
                'id_manufacturer' => 278,
                'str_description' => 'sintra',
                'str_description_slug' => 'sintra',
                'bol_active' => 1,
            ),
            259 => 
            array (
                'id_model' => 2260,
                'id_manufacturer' => 278,
                'str_description' => 'speedster',
                'str_description_slug' => 'speedster',
                'bol_active' => 1,
            ),
            260 => 
            array (
                'id_model' => 2261,
                'id_manufacturer' => 278,
                'str_description' => 'super six',
                'str_description_slug' => 'super-six',
                'bol_active' => 1,
            ),
            261 => 
            array (
                'id_model' => 2262,
                'id_manufacturer' => 278,
                'str_description' => 'tigra',
                'str_description_slug' => 'tigra',
                'bol_active' => 1,
            ),
            262 => 
            array (
                'id_model' => 2263,
                'id_manufacturer' => 278,
                'str_description' => 'vivaro',
                'str_description_slug' => 'vivaro',
                'bol_active' => 1,
            ),
            263 => 
            array (
                'id_model' => 2264,
                'id_manufacturer' => 278,
                'str_description' => 'zafira tourer',
                'str_description_slug' => 'zafira-tourer',
                'bol_active' => 1,
            ),
            264 => 
            array (
                'id_model' => 2265,
                'id_manufacturer' => 279,
                'str_description' => '110',
                'str_description_slug' => '110',
                'bol_active' => 1,
            ),
            265 => 
            array (
                'id_model' => 2266,
                'id_manufacturer' => 279,
                'str_description' => '120',
                'str_description_slug' => '120',
                'bol_active' => 1,
            ),
            266 => 
            array (
                'id_model' => 2267,
                'id_manufacturer' => 279,
                'str_description' => '250',
                'str_description_slug' => '250',
                'bol_active' => 1,
            ),
            267 => 
            array (
                'id_model' => 2268,
                'id_manufacturer' => 279,
                'str_description' => 'clipper',
                'str_description_slug' => 'clipper',
                'bol_active' => 1,
            ),
            268 => 
            array (
                'id_model' => 2269,
                'id_manufacturer' => 279,
                'str_description' => 'dietrich',
                'str_description_slug' => 'dietrich',
                'bol_active' => 1,
            ),
            269 => 
            array (
                'id_model' => 2270,
                'id_manufacturer' => 279,
                'str_description' => 'eight',
                'str_description_slug' => 'eight',
                'bol_active' => 1,
            ),
            270 => 
            array (
                'id_model' => 2271,
                'id_manufacturer' => 279,
                'str_description' => 'patrician',
                'str_description_slug' => 'patrician',
                'bol_active' => 1,
            ),
            271 => 
            array (
                'id_model' => 2272,
                'id_manufacturer' => 279,
                'str_description' => 'senior',
                'str_description_slug' => 'senior',
                'bol_active' => 1,
            ),
            272 => 
            array (
                'id_model' => 2273,
                'id_manufacturer' => 279,
                'str_description' => 'single',
                'str_description_slug' => 'single',
                'bol_active' => 1,
            ),
            273 => 
            array (
                'id_model' => 2274,
                'id_manufacturer' => 279,
                'str_description' => 'single six',
                'str_description_slug' => 'single-six',
                'bol_active' => 1,
            ),
            274 => 
            array (
                'id_model' => 2275,
                'id_manufacturer' => 279,
                'str_description' => 'six',
                'str_description_slug' => 'six',
                'bol_active' => 1,
            ),
            275 => 
            array (
                'id_model' => 2276,
                'id_manufacturer' => 279,
                'str_description' => 'super eight',
                'str_description_slug' => 'super-eight',
                'bol_active' => 1,
            ),
            276 => 
            array (
                'id_model' => 2277,
                'id_manufacturer' => 280,
                'str_description' => 'huayra',
                'str_description_slug' => 'huayra',
                'bol_active' => 1,
            ),
            277 => 
            array (
                'id_model' => 2278,
                'id_manufacturer' => 280,
                'str_description' => 'zonda',
                'str_description_slug' => 'zonda',
                'bol_active' => 1,
            ),
            278 => 
            array (
                'id_model' => 2279,
                'id_manufacturer' => 281,
                'str_description' => 'j72',
                'str_description_slug' => 'j72',
                'bol_active' => 1,
            ),
            279 => 
            array (
                'id_model' => 2280,
                'id_manufacturer' => 281,
                'str_description' => 'kallista',
                'str_description_slug' => 'kallista',
                'bol_active' => 1,
            ),
            280 => 
            array (
                'id_model' => 2281,
                'id_manufacturer' => 281,
                'str_description' => 'lima',
                'str_description_slug' => 'lima',
                'bol_active' => 1,
            ),
            281 => 
            array (
                'id_model' => 2282,
                'id_manufacturer' => 282,
                'str_description' => '217',
                'str_description_slug' => '217',
                'bol_active' => 1,
            ),
            282 => 
            array (
                'id_model' => 2283,
                'id_manufacturer' => 282,
                'str_description' => '415.2',
                'str_description_slug' => '415.2',
                'bol_active' => 1,
            ),
            283 => 
            array (
                'id_model' => 2284,
                'id_manufacturer' => 282,
                'str_description' => '515',
                'str_description_slug' => '515',
                'bol_active' => 1,
            ),
            284 => 
            array (
                'id_model' => 2285,
                'id_manufacturer' => 282,
                'str_description' => 'daily',
                'str_description_slug' => 'daily',
                'bol_active' => 1,
            ),
            285 => 
            array (
                'id_model' => 2286,
                'id_manufacturer' => 282,
                'str_description' => 'duty',
                'str_description_slug' => 'duty',
                'bol_active' => 1,
            ),
            286 => 
            array (
                'id_model' => 2287,
                'id_manufacturer' => 282,
                'str_description' => 'ekus',
                'str_description_slug' => 'ekus',
                'bol_active' => 1,
            ),
            287 => 
            array (
                'id_model' => 2288,
                'id_manufacturer' => 282,
                'str_description' => 'j4',
                'str_description_slug' => 'j4',
                'bol_active' => 1,
            ),
            288 => 
            array (
                'id_model' => 2289,
                'id_manufacturer' => 282,
                'str_description' => 'z 102',
                'str_description_slug' => 'z-102',
                'bol_active' => 1,
            ),
            289 => 
            array (
                'id_model' => 2290,
                'id_manufacturer' => 282,
                'str_description' => 'z 103',
                'str_description_slug' => 'z-103',
                'bol_active' => 1,
            ),
            290 => 
            array (
                'id_model' => 2291,
                'id_manufacturer' => 283,
                'str_description' => '104',
                'str_description_slug' => '104',
                'bol_active' => 1,
            ),
            291 => 
            array (
                'id_model' => 2292,
                'id_manufacturer' => 283,
                'str_description' => '107',
                'str_description_slug' => '107',
                'bol_active' => 1,
            ),
            292 => 
            array (
                'id_model' => 2293,
                'id_manufacturer' => 283,
                'str_description' => '108',
                'str_description_slug' => '108',
                'bol_active' => 1,
            ),
            293 => 
            array (
                'id_model' => 2294,
                'id_manufacturer' => 283,
                'str_description' => '109',
                'str_description_slug' => '109',
                'bol_active' => 1,
            ),
            294 => 
            array (
                'id_model' => 2295,
                'id_manufacturer' => 283,
                'str_description' => '163',
                'str_description_slug' => '163',
                'bol_active' => 1,
            ),
            295 => 
            array (
                'id_model' => 2296,
                'id_manufacturer' => 283,
                'str_description' => '172',
                'str_description_slug' => '172',
                'bol_active' => 1,
            ),
            296 => 
            array (
                'id_model' => 2297,
                'id_manufacturer' => 283,
                'str_description' => '190',
                'str_description_slug' => '190',
                'bol_active' => 1,
            ),
            297 => 
            array (
                'id_model' => 2298,
                'id_manufacturer' => 283,
                'str_description' => '201',
                'str_description_slug' => '201',
                'bol_active' => 1,
            ),
            298 => 
            array (
                'id_model' => 2299,
                'id_manufacturer' => 283,
                'str_description' => '202',
                'str_description_slug' => '202',
                'bol_active' => 1,
            ),
            299 => 
            array (
                'id_model' => 2300,
                'id_manufacturer' => 283,
                'str_description' => '203',
                'str_description_slug' => '203',
                'bol_active' => 1,
            ),
            300 => 
            array (
                'id_model' => 2301,
                'id_manufacturer' => 283,
                'str_description' => '204',
                'str_description_slug' => '204',
                'bol_active' => 1,
            ),
            301 => 
            array (
                'id_model' => 2302,
                'id_manufacturer' => 283,
                'str_description' => '206 +',
                'str_description_slug' => '206-+',
                'bol_active' => 1,
            ),
            302 => 
            array (
                'id_model' => 2303,
                'id_manufacturer' => 283,
                'str_description' => '207 +',
                'str_description_slug' => '207-+',
                'bol_active' => 1,
            ),
            303 => 
            array (
                'id_model' => 2304,
                'id_manufacturer' => 283,
                'str_description' => '207 cc',
                'str_description_slug' => '207-cc',
                'bol_active' => 1,
            ),
            304 => 
            array (
                'id_model' => 2305,
                'id_manufacturer' => 283,
                'str_description' => '208',
                'str_description_slug' => '208',
                'bol_active' => 1,
            ),
            305 => 
            array (
                'id_model' => 2306,
                'id_manufacturer' => 283,
                'str_description' => '301',
                'str_description_slug' => '301',
                'bol_active' => 1,
            ),
            306 => 
            array (
                'id_model' => 2307,
                'id_manufacturer' => 283,
                'str_description' => '302',
                'str_description_slug' => '302',
                'bol_active' => 1,
            ),
            307 => 
            array (
                'id_model' => 2308,
                'id_manufacturer' => 283,
                'str_description' => '304',
                'str_description_slug' => '304',
                'bol_active' => 1,
            ),
            308 => 
            array (
                'id_model' => 2309,
                'id_manufacturer' => 283,
                'str_description' => '305',
                'str_description_slug' => '305',
                'bol_active' => 1,
            ),
            309 => 
            array (
                'id_model' => 2310,
                'id_manufacturer' => 283,
                'str_description' => '309',
                'str_description_slug' => '309',
                'bol_active' => 1,
            ),
            310 => 
            array (
                'id_model' => 2311,
                'id_manufacturer' => 283,
                'str_description' => '401',
                'str_description_slug' => '401',
                'bol_active' => 1,
            ),
            311 => 
            array (
                'id_model' => 2312,
                'id_manufacturer' => 283,
                'str_description' => '402',
                'str_description_slug' => '402',
                'bol_active' => 1,
            ),
            312 => 
            array (
                'id_model' => 2313,
                'id_manufacturer' => 283,
                'str_description' => '403',
                'str_description_slug' => '403',
                'bol_active' => 1,
            ),
            313 => 
            array (
                'id_model' => 2314,
                'id_manufacturer' => 283,
                'str_description' => '404',
                'str_description_slug' => '404',
                'bol_active' => 1,
            ),
            314 => 
            array (
                'id_model' => 2315,
                'id_manufacturer' => 283,
                'str_description' => '405',
                'str_description_slug' => '405',
                'bol_active' => 1,
            ),
            315 => 
            array (
                'id_model' => 2316,
                'id_manufacturer' => 283,
                'str_description' => '408',
                'str_description_slug' => '408',
                'bol_active' => 1,
            ),
            316 => 
            array (
                'id_model' => 2317,
                'id_manufacturer' => 283,
                'str_description' => '504',
                'str_description_slug' => '504',
                'bol_active' => 1,
            ),
            317 => 
            array (
                'id_model' => 2318,
                'id_manufacturer' => 283,
                'str_description' => '504 v',
                'str_description_slug' => '504-v',
                'bol_active' => 1,
            ),
            318 => 
            array (
                'id_model' => 2319,
                'id_manufacturer' => 283,
                'str_description' => '505',
                'str_description_slug' => '505',
                'bol_active' => 1,
            ),
            319 => 
            array (
                'id_model' => 2320,
                'id_manufacturer' => 283,
                'str_description' => '508',
                'str_description_slug' => '508',
                'bol_active' => 1,
            ),
            320 => 
            array (
                'id_model' => 2321,
                'id_manufacturer' => 283,
                'str_description' => '604',
                'str_description_slug' => '604',
                'bol_active' => 1,
            ),
            321 => 
            array (
                'id_model' => 2322,
                'id_manufacturer' => 283,
                'str_description' => '605',
                'str_description_slug' => '605',
                'bol_active' => 1,
            ),
            322 => 
            array (
                'id_model' => 2323,
                'id_manufacturer' => 283,
                'str_description' => '607',
                'str_description_slug' => '607',
                'bol_active' => 1,
            ),
            323 => 
            array (
                'id_model' => 2324,
                'id_manufacturer' => 283,
                'str_description' => '806',
                'str_description_slug' => '806',
                'bol_active' => 1,
            ),
            324 => 
            array (
                'id_model' => 2325,
                'id_manufacturer' => 283,
                'str_description' => '807',
                'str_description_slug' => '807',
                'bol_active' => 1,
            ),
            325 => 
            array (
                'id_model' => 2326,
                'id_manufacturer' => 283,
                'str_description' => '1007',
                'str_description_slug' => '1007',
                'bol_active' => 1,
            ),
            326 => 
            array (
                'id_model' => 2327,
                'id_manufacturer' => 283,
                'str_description' => '2008',
                'str_description_slug' => '2008',
                'bol_active' => 1,
            ),
            327 => 
            array (
                'id_model' => 2328,
                'id_manufacturer' => 283,
                'str_description' => '3008',
                'str_description_slug' => '3008',
                'bol_active' => 1,
            ),
            328 => 
            array (
                'id_model' => 2329,
                'id_manufacturer' => 283,
                'str_description' => '4007',
                'str_description_slug' => '4007',
                'bol_active' => 1,
            ),
            329 => 
            array (
                'id_model' => 2330,
                'id_manufacturer' => 283,
                'str_description' => '4008',
                'str_description_slug' => '4008',
                'bol_active' => 1,
            ),
            330 => 
            array (
                'id_model' => 2331,
                'id_manufacturer' => 283,
                'str_description' => '5008',
                'str_description_slug' => '5008',
                'bol_active' => 1,
            ),
            331 => 
            array (
                'id_model' => 2332,
                'id_manufacturer' => 283,
                'str_description' => 'bipper',
                'str_description_slug' => 'bipper',
                'bol_active' => 1,
            ),
            332 => 
            array (
                'id_model' => 2333,
                'id_manufacturer' => 283,
                'str_description' => 'boxer',
                'str_description_slug' => 'boxer',
                'bol_active' => 1,
            ),
            333 => 
            array (
                'id_model' => 2334,
                'id_manufacturer' => 283,
                'str_description' => 'dallas',
                'str_description_slug' => 'dallas',
                'bol_active' => 1,
            ),
            334 => 
            array (
                'id_model' => 2335,
                'id_manufacturer' => 283,
                'str_description' => 'expert',
                'str_description_slug' => 'expert',
                'bol_active' => 1,
            ),
            335 => 
            array (
                'id_model' => 2336,
                'id_manufacturer' => 283,
                'str_description' => 'ion',
                'str_description_slug' => 'ion',
                'bol_active' => 1,
            ),
            336 => 
            array (
                'id_model' => 2337,
                'id_manufacturer' => 283,
                'str_description' => 'j-5',
                'str_description_slug' => 'j-5',
                'bol_active' => 1,
            ),
            337 => 
            array (
                'id_model' => 2338,
                'id_manufacturer' => 283,
                'str_description' => 'j-7',
                'str_description_slug' => 'j-7',
                'bol_active' => 1,
            ),
            338 => 
            array (
                'id_model' => 2339,
                'id_manufacturer' => 283,
                'str_description' => 'j-9',
                'str_description_slug' => 'j-9',
                'bol_active' => 1,
            ),
            339 => 
            array (
                'id_model' => 2340,
                'id_manufacturer' => 283,
                'str_description' => 'p4',
                'str_description_slug' => 'p4',
                'bol_active' => 1,
            ),
            340 => 
            array (
                'id_model' => 2341,
                'id_manufacturer' => 283,
                'str_description' => 'partner origin',
                'str_description_slug' => 'partner-origin',
                'bol_active' => 1,
            ),
            341 => 
            array (
                'id_model' => 2342,
                'id_manufacturer' => 283,
                'str_description' => 'rcz',
                'str_description_slug' => 'rcz',
                'bol_active' => 1,
            ),
            342 => 
            array (
                'id_model' => 2343,
                'id_manufacturer' => 283,
                'str_description' => 'traveller',
                'str_description_slug' => 'traveller',
                'bol_active' => 1,
            ),
            343 => 
            array (
                'id_model' => 2344,
                'id_manufacturer' => 284,
                'str_description' => 'bugracer',
                'str_description_slug' => 'bugracer',
                'bol_active' => 1,
            ),
            344 => 
            array (
                'id_model' => 2345,
                'id_manufacturer' => 284,
                'str_description' => 'bugrider',
                'str_description_slug' => 'bugrider',
                'bol_active' => 1,
            ),
            345 => 
            array (
                'id_model' => 2346,
                'id_manufacturer' => 284,
                'str_description' => 'bugxter 150',
                'str_description_slug' => 'bugxter-150',
                'bol_active' => 1,
            ),
            346 => 
            array (
                'id_model' => 2347,
                'id_manufacturer' => 284,
                'str_description' => 'cevennes',
                'str_description_slug' => 'cevennes',
                'bol_active' => 1,
            ),
            347 => 
            array (
                'id_model' => 2348,
                'id_manufacturer' => 284,
                'str_description' => 'speedster ii',
                'str_description_slug' => 'speedster-ii',
                'bol_active' => 1,
            ),
            348 => 
            array (
                'id_model' => 2349,
                'id_manufacturer' => 285,
                'str_description' => 'porter',
                'str_description_slug' => 'porter',
                'bol_active' => 1,
            ),
            349 => 
            array (
                'id_model' => 2350,
                'id_manufacturer' => 285,
                'str_description' => 'quargo',
                'str_description_slug' => 'quargo',
                'bol_active' => 1,
            ),
            350 => 
            array (
                'id_model' => 2351,
                'id_manufacturer' => 286,
                'str_description' => 'm 500',
                'str_description_slug' => 'm-500',
                'bol_active' => 1,
            ),
            351 => 
            array (
                'id_model' => 2352,
                'id_manufacturer' => 286,
                'str_description' => 'pk 500',
                'str_description_slug' => 'pk-500',
                'bol_active' => 1,
            ),
            352 => 
            array (
                'id_model' => 2353,
                'id_manufacturer' => 286,
                'str_description' => 'vespa 400',
                'str_description_slug' => 'vespa-400',
                'bol_active' => 1,
            ),
            353 => 
            array (
                'id_model' => 2354,
                'id_manufacturer' => 287,
                'str_description' => 'b',
                'str_description_slug' => 'b',
                'bol_active' => 1,
            ),
            354 => 
            array (
                'id_model' => 2355,
                'id_manufacturer' => 288,
                'str_description' => 'bulldog',
                'str_description_slug' => 'bulldog',
                'bol_active' => 1,
            ),
            355 => 
            array (
                'id_model' => 2356,
                'id_manufacturer' => 288,
                'str_description' => 'cobra',
                'str_description_slug' => 'cobra',
                'bol_active' => 1,
            ),
            356 => 
            array (
                'id_model' => 2357,
                'id_manufacturer' => 288,
                'str_description' => 'family tourer',
                'str_description_slug' => 'family-tourer',
                'bol_active' => 1,
            ),
            357 => 
            array (
                'id_model' => 2358,
                'id_manufacturer' => 288,
                'str_description' => 'sports',
                'str_description_slug' => 'sports',
                'bol_active' => 1,
            ),
            358 => 
            array (
                'id_model' => 2359,
                'id_manufacturer' => 288,
                'str_description' => 'sumo',
                'str_description_slug' => 'sumo',
                'bol_active' => 1,
            ),
            359 => 
            array (
                'id_model' => 2360,
                'id_manufacturer' => 289,
                'str_description' => 'barracuda',
                'str_description_slug' => 'barracuda',
                'bol_active' => 1,
            ),
            360 => 
            array (
                'id_model' => 2361,
                'id_manufacturer' => 289,
                'str_description' => 'belvedere',
                'str_description_slug' => 'belvedere',
                'bol_active' => 1,
            ),
            361 => 
            array (
                'id_model' => 2362,
                'id_manufacturer' => 289,
                'str_description' => 'colt',
                'str_description_slug' => 'colt',
                'bol_active' => 1,
            ),
            362 => 
            array (
                'id_model' => 2363,
                'id_manufacturer' => 289,
                'str_description' => 'deluxe',
                'str_description_slug' => 'deluxe',
                'bol_active' => 1,
            ),
            363 => 
            array (
                'id_model' => 2364,
                'id_manufacturer' => 289,
                'str_description' => 'fury',
                'str_description_slug' => 'fury',
                'bol_active' => 1,
            ),
            364 => 
            array (
                'id_model' => 2365,
                'id_manufacturer' => 289,
                'str_description' => 'laser',
                'str_description_slug' => 'laser',
                'bol_active' => 1,
            ),
            365 => 
            array (
                'id_model' => 2366,
                'id_manufacturer' => 289,
                'str_description' => 'p11',
                'str_description_slug' => 'p11',
                'bol_active' => 1,
            ),
            366 => 
            array (
                'id_model' => 2367,
                'id_manufacturer' => 289,
                'str_description' => 'p15',
                'str_description_slug' => 'p15',
                'bol_active' => 1,
            ),
            367 => 
            array (
                'id_model' => 2368,
                'id_manufacturer' => 289,
                'str_description' => 'prowler',
                'str_description_slug' => 'prowler',
                'bol_active' => 1,
            ),
            368 => 
            array (
                'id_model' => 2369,
                'id_manufacturer' => 289,
                'str_description' => 'road runner',
                'str_description_slug' => 'road-runner',
                'bol_active' => 1,
            ),
            369 => 
            array (
                'id_model' => 2370,
                'id_manufacturer' => 289,
                'str_description' => 'roadster',
                'str_description_slug' => 'roadster',
                'bol_active' => 1,
            ),
            370 => 
            array (
                'id_model' => 2371,
                'id_manufacturer' => 289,
                'str_description' => 'satellite sebring',
                'str_description_slug' => 'satellite-sebring',
                'bol_active' => 1,
            ),
            371 => 
            array (
                'id_model' => 2372,
                'id_manufacturer' => 289,
                'str_description' => 'savoy',
                'str_description_slug' => 'savoy',
                'bol_active' => 1,
            ),
            372 => 
            array (
                'id_model' => 2373,
                'id_manufacturer' => 289,
                'str_description' => 'suburban',
                'str_description_slug' => 'suburban',
                'bol_active' => 1,
            ),
            373 => 
            array (
                'id_model' => 2374,
                'id_manufacturer' => 289,
                'str_description' => 'sundance',
                'str_description_slug' => 'sundance',
                'bol_active' => 1,
            ),
            374 => 
            array (
                'id_model' => 2375,
                'id_manufacturer' => 289,
                'str_description' => 'valiant',
                'str_description_slug' => 'valiant',
                'bol_active' => 1,
            ),
            375 => 
            array (
                'id_model' => 2376,
                'id_manufacturer' => 289,
                'str_description' => 'voyager',
                'str_description_slug' => 'voyager',
                'bol_active' => 1,
            ),
            376 => 
            array (
                'id_model' => 2377,
                'id_manufacturer' => 290,
                'str_description' => 'ranger 1000',
                'str_description_slug' => 'ranger-1000',
                'bol_active' => 1,
            ),
            377 => 
            array (
                'id_model' => 2378,
                'id_manufacturer' => 290,
                'str_description' => 'ranger 400',
                'str_description_slug' => 'ranger-400',
                'bol_active' => 1,
            ),
            378 => 
            array (
                'id_model' => 2379,
                'id_manufacturer' => 290,
                'str_description' => 'ranger 425',
                'str_description_slug' => 'ranger-425',
                'bol_active' => 1,
            ),
            379 => 
            array (
                'id_model' => 2380,
                'id_manufacturer' => 290,
                'str_description' => 'ranger 500',
                'str_description_slug' => 'ranger-500',
                'bol_active' => 1,
            ),
            380 => 
            array (
                'id_model' => 2381,
                'id_manufacturer' => 290,
                'str_description' => 'ranger 570',
                'str_description_slug' => 'ranger-570',
                'bol_active' => 1,
            ),
            381 => 
            array (
                'id_model' => 2382,
                'id_manufacturer' => 290,
                'str_description' => 'ranger 700',
                'str_description_slug' => 'ranger-700',
                'bol_active' => 1,
            ),
            382 => 
            array (
                'id_model' => 2383,
                'id_manufacturer' => 290,
                'str_description' => 'ranger 800',
                'str_description_slug' => 'ranger-800',
                'bol_active' => 1,
            ),
            383 => 
            array (
                'id_model' => 2384,
                'id_manufacturer' => 290,
                'str_description' => 'ranger 900',
                'str_description_slug' => 'ranger-900',
                'bol_active' => 1,
            ),
            384 => 
            array (
                'id_model' => 2385,
                'id_manufacturer' => 290,
                'str_description' => 'ranger ev',
                'str_description_slug' => 'ranger-ev',
                'bol_active' => 1,
            ),
            385 => 
            array (
                'id_model' => 2386,
                'id_manufacturer' => 290,
                'str_description' => 'slingshot',
                'str_description_slug' => 'slingshot',
                'bol_active' => 1,
            ),
            386 => 
            array (
                'id_model' => 2387,
                'id_manufacturer' => 291,
                'str_description' => '6000',
                'str_description_slug' => '6000',
                'bol_active' => 1,
            ),
            387 => 
            array (
                'id_model' => 2388,
                'id_manufacturer' => 291,
                'str_description' => 'aztek',
                'str_description_slug' => 'aztek',
                'bol_active' => 1,
            ),
            388 => 
            array (
                'id_model' => 2389,
                'id_manufacturer' => 291,
                'str_description' => 'big six',
                'str_description_slug' => 'big-six',
                'bol_active' => 1,
            ),
            389 => 
            array (
                'id_model' => 2390,
                'id_manufacturer' => 291,
                'str_description' => 'bonneville',
                'str_description_slug' => 'bonneville',
                'bol_active' => 1,
            ),
            390 => 
            array (
                'id_model' => 2391,
                'id_manufacturer' => 291,
                'str_description' => 'catalina',
                'str_description_slug' => 'catalina',
                'bol_active' => 1,
            ),
            391 => 
            array (
                'id_model' => 2392,
                'id_manufacturer' => 291,
                'str_description' => 'chieftain',
                'str_description_slug' => 'chieftain',
                'bol_active' => 1,
            ),
            392 => 
            array (
                'id_model' => 2393,
                'id_manufacturer' => 291,
                'str_description' => 'fiero',
                'str_description_slug' => 'fiero',
                'bol_active' => 1,
            ),
            393 => 
            array (
                'id_model' => 2394,
                'id_manufacturer' => 291,
                'str_description' => 'firebird',
                'str_description_slug' => 'firebird',
                'bol_active' => 1,
            ),
            394 => 
            array (
                'id_model' => 2395,
                'id_manufacturer' => 291,
                'str_description' => 'g6',
                'str_description_slug' => 'g6',
                'bol_active' => 1,
            ),
            395 => 
            array (
                'id_model' => 2396,
                'id_manufacturer' => 291,
                'str_description' => 'grand am',
                'str_description_slug' => 'grand-am',
                'bol_active' => 1,
            ),
            396 => 
            array (
                'id_model' => 2397,
                'id_manufacturer' => 291,
                'str_description' => 'grand lemans',
                'str_description_slug' => 'grand-lemans',
                'bol_active' => 1,
            ),
            397 => 
            array (
                'id_model' => 2398,
                'id_manufacturer' => 291,
                'str_description' => 'grand prix',
                'str_description_slug' => 'grand-prix',
                'bol_active' => 1,
            ),
            398 => 
            array (
                'id_model' => 2399,
                'id_manufacturer' => 291,
                'str_description' => 'grand safari',
                'str_description_slug' => 'grand-safari',
                'bol_active' => 1,
            ),
            399 => 
            array (
                'id_model' => 2400,
                'id_manufacturer' => 291,
                'str_description' => 'grandville',
                'str_description_slug' => 'grandville',
                'bol_active' => 1,
            ),
            400 => 
            array (
                'id_model' => 2401,
                'id_manufacturer' => 291,
                'str_description' => 'gto',
                'str_description_slug' => 'gto',
                'bol_active' => 1,
            ),
            401 => 
            array (
                'id_model' => 2402,
                'id_manufacturer' => 291,
                'str_description' => 'le mans',
                'str_description_slug' => 'le-mans',
                'bol_active' => 1,
            ),
            402 => 
            array (
                'id_model' => 2403,
                'id_manufacturer' => 291,
                'str_description' => 'montana',
                'str_description_slug' => 'montana',
                'bol_active' => 1,
            ),
            403 => 
            array (
                'id_model' => 2404,
                'id_manufacturer' => 291,
                'str_description' => 'phaeton',
                'str_description_slug' => 'phaeton',
                'bol_active' => 1,
            ),
            404 => 
            array (
                'id_model' => 2405,
                'id_manufacturer' => 291,
                'str_description' => 'phoenix',
                'str_description_slug' => 'phoenix',
                'bol_active' => 1,
            ),
            405 => 
            array (
                'id_model' => 2406,
                'id_manufacturer' => 291,
                'str_description' => 'solstice',
                'str_description_slug' => 'solstice',
                'bol_active' => 1,
            ),
            406 => 
            array (
                'id_model' => 2407,
                'id_manufacturer' => 291,
                'str_description' => 'sunbird',
                'str_description_slug' => 'sunbird',
                'bol_active' => 1,
            ),
            407 => 
            array (
                'id_model' => 2408,
                'id_manufacturer' => 291,
                'str_description' => 'sunfire',
                'str_description_slug' => 'sunfire',
                'bol_active' => 1,
            ),
            408 => 
            array (
                'id_model' => 2409,
                'id_manufacturer' => 291,
                'str_description' => 'tempest',
                'str_description_slug' => 'tempest',
                'bol_active' => 1,
            ),
            409 => 
            array (
                'id_model' => 2410,
                'id_manufacturer' => 291,
                'str_description' => 'torpedo',
                'str_description_slug' => 'torpedo',
                'bol_active' => 1,
            ),
            410 => 
            array (
                'id_model' => 2411,
                'id_manufacturer' => 291,
                'str_description' => 'torrent',
                'str_description_slug' => 'torrent',
                'bol_active' => 1,
            ),
            411 => 
            array (
                'id_model' => 2412,
                'id_manufacturer' => 291,
                'str_description' => 'trans am',
                'str_description_slug' => 'trans-am',
                'bol_active' => 1,
            ),
            412 => 
            array (
                'id_model' => 2413,
                'id_manufacturer' => 291,
                'str_description' => 'trans sport',
                'str_description_slug' => 'trans-sport',
                'bol_active' => 1,
            ),
            413 => 
            array (
                'id_model' => 2414,
                'id_manufacturer' => 291,
                'str_description' => 'vibe',
                'str_description_slug' => 'vibe',
                'bol_active' => 1,
            ),
            414 => 
            array (
                'id_model' => 2415,
                'id_manufacturer' => 292,
                'str_description' => '356',
                'str_description_slug' => '356',
                'bol_active' => 1,
            ),
            415 => 
            array (
                'id_model' => 2416,
                'id_manufacturer' => 292,
                'str_description' => '356 a',
                'str_description_slug' => '356-a',
                'bol_active' => 1,
            ),
            416 => 
            array (
                'id_model' => 2417,
                'id_manufacturer' => 292,
                'str_description' => '356 b',
                'str_description_slug' => '356-b',
                'bol_active' => 1,
            ),
            417 => 
            array (
                'id_model' => 2418,
                'id_manufacturer' => 292,
                'str_description' => '356 c',
                'str_description_slug' => '356-c',
                'bol_active' => 1,
            ),
            418 => 
            array (
                'id_model' => 2419,
                'id_manufacturer' => 292,
                'str_description' => '356 sc',
                'str_description_slug' => '356-sc',
                'bol_active' => 1,
            ),
            419 => 
            array (
                'id_model' => 2420,
                'id_manufacturer' => 292,
                'str_description' => '356-1',
                'str_description_slug' => '356-1',
                'bol_active' => 1,
            ),
            420 => 
            array (
                'id_model' => 2421,
                'id_manufacturer' => 292,
                'str_description' => '718 boxster',
                'str_description_slug' => '718-boxster',
                'bol_active' => 1,
            ),
            421 => 
            array (
                'id_model' => 2422,
                'id_manufacturer' => 292,
                'str_description' => '718 cayman',
                'str_description_slug' => '718-cayman',
                'bol_active' => 1,
            ),
            422 => 
            array (
                'id_model' => 2423,
                'id_manufacturer' => 292,
                'str_description' => '718/8',
                'str_description_slug' => '718/8',
                'bol_active' => 1,
            ),
            423 => 
            array (
                'id_model' => 2424,
                'id_manufacturer' => 292,
                'str_description' => '904',
                'str_description_slug' => '904',
                'bol_active' => 1,
            ),
            424 => 
            array (
                'id_model' => 2425,
                'id_manufacturer' => 292,
                'str_description' => '911',
                'str_description_slug' => '911',
                'bol_active' => 1,
            ),
            425 => 
            array (
                'id_model' => 2426,
                'id_manufacturer' => 292,
                'str_description' => '912',
                'str_description_slug' => '912',
                'bol_active' => 1,
            ),
            426 => 
            array (
                'id_model' => 2427,
                'id_manufacturer' => 292,
                'str_description' => '914',
                'str_description_slug' => '914',
                'bol_active' => 1,
            ),
            427 => 
            array (
                'id_model' => 2428,
                'id_manufacturer' => 292,
                'str_description' => '918',
                'str_description_slug' => '918',
                'bol_active' => 1,
            ),
            428 => 
            array (
                'id_model' => 2429,
                'id_manufacturer' => 292,
                'str_description' => '924',
                'str_description_slug' => '924',
                'bol_active' => 1,
            ),
            429 => 
            array (
                'id_model' => 2430,
                'id_manufacturer' => 292,
                'str_description' => '928',
                'str_description_slug' => '928',
                'bol_active' => 1,
            ),
            430 => 
            array (
                'id_model' => 2431,
                'id_manufacturer' => 292,
                'str_description' => '944',
                'str_description_slug' => '944',
                'bol_active' => 1,
            ),
            431 => 
            array (
                'id_model' => 2432,
                'id_manufacturer' => 292,
                'str_description' => '959',
                'str_description_slug' => '959',
                'bol_active' => 1,
            ),
            432 => 
            array (
                'id_model' => 2433,
                'id_manufacturer' => 292,
                'str_description' => '968',
                'str_description_slug' => '968',
                'bol_active' => 1,
            ),
            433 => 
            array (
                'id_model' => 2434,
                'id_manufacturer' => 292,
                'str_description' => 'boxster',
                'str_description_slug' => 'boxster',
                'bol_active' => 1,
            ),
            434 => 
            array (
                'id_model' => 2435,
                'id_manufacturer' => 292,
                'str_description' => 'carrera gt',
                'str_description_slug' => 'carrera-gt',
                'bol_active' => 1,
            ),
            435 => 
            array (
                'id_model' => 2436,
                'id_manufacturer' => 292,
                'str_description' => 'cayenne',
                'str_description_slug' => 'cayenne',
                'bol_active' => 1,
            ),
            436 => 
            array (
                'id_model' => 2437,
                'id_manufacturer' => 292,
                'str_description' => 'cayman',
                'str_description_slug' => 'cayman',
                'bol_active' => 1,
            ),
            437 => 
            array (
                'id_model' => 2438,
                'id_manufacturer' => 292,
                'str_description' => 'macan',
                'str_description_slug' => 'macan',
                'bol_active' => 1,
            ),
            438 => 
            array (
                'id_model' => 2439,
                'id_manufacturer' => 292,
                'str_description' => 'panamera',
                'str_description_slug' => 'panamera',
                'bol_active' => 1,
            ),
            439 => 
            array (
                'id_model' => 2440,
                'id_manufacturer' => 293,
                'str_description' => '021a',
                'str_description_slug' => '021a',
                'bol_active' => 1,
            ),
            440 => 
            array (
                'id_model' => 2441,
                'id_manufacturer' => 293,
                'str_description' => 'super piccolo',
                'str_description_slug' => 'super-piccolo',
                'bol_active' => 1,
            ),
            441 => 
            array (
                'id_model' => 2442,
                'id_manufacturer' => 294,
                'str_description' => 'victory xl',
                'str_description_slug' => 'victory-xl',
                'bol_active' => 1,
            ),
            442 => 
            array (
                'id_model' => 2443,
                'id_manufacturer' => 295,
                'str_description' => 'persona',
                'str_description_slug' => 'persona',
                'bol_active' => 1,
            ),
            443 => 
            array (
                'id_model' => 2444,
                'id_manufacturer' => 295,
                'str_description' => 'saloon',
                'str_description_slug' => 'saloon',
                'bol_active' => 1,
            ),
            444 => 
            array (
                'id_model' => 2445,
                'id_manufacturer' => 295,
                'str_description' => 'satria',
                'str_description_slug' => 'satria',
                'bol_active' => 1,
            ),
            445 => 
            array (
                'id_model' => 2446,
                'id_manufacturer' => 295,
                'str_description' => 'savvy',
                'str_description_slug' => 'savvy',
                'bol_active' => 1,
            ),
            446 => 
            array (
                'id_model' => 2447,
                'id_manufacturer' => 296,
                'str_description' => '250',
                'str_description_slug' => '250',
                'bol_active' => 1,
            ),
            447 => 
            array (
                'id_model' => 2448,
                'id_manufacturer' => 297,
                'str_description' => 'puli',
                'str_description_slug' => 'puli',
                'bol_active' => 1,
            ),
            448 => 
            array (
                'id_model' => 2449,
                'id_manufacturer' => 298,
                'str_description' => 'dune',
                'str_description_slug' => 'dune',
                'bol_active' => 1,
            ),
            449 => 
            array (
                'id_model' => 2450,
                'id_manufacturer' => 299,
                'str_description' => 'gk 250',
                'str_description_slug' => 'gk-250',
                'bol_active' => 1,
            ),
            450 => 
            array (
                'id_model' => 2451,
                'id_manufacturer' => 299,
                'str_description' => 'qygk005',
                'str_description_slug' => 'qygk005',
                'bol_active' => 1,
            ),
            451 => 
            array (
                'id_model' => 2452,
                'id_manufacturer' => 300,
                'str_description' => 'mangusta',
                'str_description_slug' => 'mangusta',
                'bol_active' => 1,
            ),
            452 => 
            array (
                'id_model' => 2453,
                'id_manufacturer' => 301,
                'str_description' => '6',
                'str_description_slug' => '6',
                'bol_active' => 1,
            ),
            453 => 
            array (
                'id_model' => 2454,
                'id_manufacturer' => 301,
                'str_description' => 'marlin',
                'str_description_slug' => 'marlin',
                'bol_active' => 1,
            ),
            454 => 
            array (
                'id_model' => 2455,
                'id_manufacturer' => 302,
                'str_description' => 'gts',
                'str_description_slug' => 'gts',
                'bol_active' => 1,
            ),
            455 => 
            array (
                'id_model' => 2456,
                'id_manufacturer' => 303,
                'str_description' => 'magnum',
                'str_description_slug' => 'magnum',
                'bol_active' => 1,
            ),
            456 => 
            array (
                'id_model' => 2457,
                'id_manufacturer' => 304,
                'str_description' => 'adventure',
                'str_description_slug' => 'adventure',
                'bol_active' => 1,
            ),
            457 => 
            array (
                'id_model' => 2458,
                'id_manufacturer' => 304,
                'str_description' => 'obey',
                'str_description_slug' => 'obey',
                'bol_active' => 1,
            ),
            458 => 
            array (
                'id_model' => 2459,
                'id_manufacturer' => 305,
                'str_description' => 'regal',
                'str_description_slug' => 'regal',
                'bol_active' => 1,
            ),
            459 => 
            array (
                'id_model' => 2460,
                'id_manufacturer' => 305,
                'str_description' => 'rialto',
                'str_description_slug' => 'rialto',
                'bol_active' => 1,
            ),
            460 => 
            array (
                'id_model' => 2461,
                'id_manufacturer' => 305,
                'str_description' => 'scimitar',
                'str_description_slug' => 'scimitar',
                'bol_active' => 1,
            ),
            461 => 
            array (
                'id_model' => 2462,
                'id_manufacturer' => 306,
                'str_description' => '4',
                'str_description_slug' => '4',
                'bol_active' => 1,
            ),
            462 => 
            array (
                'id_model' => 2463,
                'id_manufacturer' => 306,
                'str_description' => '4 cv',
                'str_description_slug' => '4-cv',
                'bol_active' => 1,
            ),
            463 => 
            array (
                'id_model' => 2464,
                'id_manufacturer' => 306,
                'str_description' => '5',
                'str_description_slug' => '5',
                'bol_active' => 1,
            ),
            464 => 
            array (
                'id_model' => 2465,
                'id_manufacturer' => 306,
                'str_description' => '6',
                'str_description_slug' => '6',
                'bol_active' => 1,
            ),
            465 => 
            array (
                'id_model' => 2466,
                'id_manufacturer' => 306,
                'str_description' => '7',
                'str_description_slug' => '7',
                'bol_active' => 1,
            ),
            466 => 
            array (
                'id_model' => 2467,
                'id_manufacturer' => 306,
                'str_description' => '8',
                'str_description_slug' => '8',
                'bol_active' => 1,
            ),
            467 => 
            array (
                'id_model' => 2468,
                'id_manufacturer' => 306,
                'str_description' => '9',
                'str_description_slug' => '9',
                'bol_active' => 1,
            ),
            468 => 
            array (
                'id_model' => 2469,
                'id_manufacturer' => 306,
                'str_description' => '10',
                'str_description_slug' => '10',
                'bol_active' => 1,
            ),
            469 => 
            array (
                'id_model' => 2470,
                'id_manufacturer' => 306,
                'str_description' => '11',
                'str_description_slug' => '11',
                'bol_active' => 1,
            ),
            470 => 
            array (
                'id_model' => 2471,
                'id_manufacturer' => 306,
                'str_description' => '12',
                'str_description_slug' => '12',
                'bol_active' => 1,
            ),
            471 => 
            array (
                'id_model' => 2472,
                'id_manufacturer' => 306,
                'str_description' => '14',
                'str_description_slug' => '14',
                'bol_active' => 1,
            ),
            472 => 
            array (
                'id_model' => 2473,
                'id_manufacturer' => 306,
                'str_description' => '15',
                'str_description_slug' => '15',
                'bol_active' => 1,
            ),
            473 => 
            array (
                'id_model' => 2474,
                'id_manufacturer' => 306,
                'str_description' => '16',
                'str_description_slug' => '16',
                'bol_active' => 1,
            ),
            474 => 
            array (
                'id_model' => 2475,
                'id_manufacturer' => 306,
                'str_description' => '17',
                'str_description_slug' => '17',
                'bol_active' => 1,
            ),
            475 => 
            array (
                'id_model' => 2476,
                'id_manufacturer' => 306,
                'str_description' => '18',
                'str_description_slug' => '18',
                'bol_active' => 1,
            ),
            476 => 
            array (
                'id_model' => 2477,
                'id_manufacturer' => 306,
                'str_description' => '20',
                'str_description_slug' => '20',
                'bol_active' => 1,
            ),
            477 => 
            array (
                'id_model' => 2478,
                'id_manufacturer' => 306,
                'str_description' => '21',
                'str_description_slug' => '21',
                'bol_active' => 1,
            ),
            478 => 
            array (
                'id_model' => 2479,
                'id_manufacturer' => 306,
                'str_description' => '25',
                'str_description_slug' => '25',
                'bol_active' => 1,
            ),
            479 => 
            array (
                'id_model' => 2480,
                'id_manufacturer' => 306,
                'str_description' => '30',
                'str_description_slug' => '30',
                'bol_active' => 1,
            ),
            480 => 
            array (
                'id_model' => 2481,
                'id_manufacturer' => 306,
                'str_description' => 'alliance',
                'str_description_slug' => 'alliance',
                'bol_active' => 1,
            ),
            481 => 
            array (
                'id_model' => 2482,
                'id_manufacturer' => 306,
                'str_description' => 'alpine',
                'str_description_slug' => 'alpine',
                'bol_active' => 1,
            ),
            482 => 
            array (
                'id_model' => 2483,
                'id_manufacturer' => 306,
                'str_description' => 'arena',
                'str_description_slug' => 'arena',
                'bol_active' => 1,
            ),
            483 => 
            array (
                'id_model' => 2484,
                'id_manufacturer' => 306,
                'str_description' => 'avantime',
                'str_description_slug' => 'avantime',
                'bol_active' => 1,
            ),
            484 => 
            array (
                'id_model' => 2485,
                'id_manufacturer' => 306,
                'str_description' => 'b',
                'str_description_slug' => 'b',
                'bol_active' => 1,
            ),
            485 => 
            array (
                'id_model' => 2486,
                'id_manufacturer' => 306,
                'str_description' => 'b-110.35',
                'str_description_slug' => 'b-110.35',
                'bol_active' => 1,
            ),
            486 => 
            array (
                'id_model' => 2487,
                'id_manufacturer' => 306,
                'str_description' => 'b-120.35',
                'str_description_slug' => 'b-120.35',
                'bol_active' => 1,
            ),
            487 => 
            array (
                'id_model' => 2488,
                'id_manufacturer' => 306,
                'str_description' => 'b-70.35',
                'str_description_slug' => 'b-70.35',
                'bol_active' => 1,
            ),
            488 => 
            array (
                'id_model' => 2489,
                'id_manufacturer' => 306,
                'str_description' => 'b-80.35',
                'str_description_slug' => 'b-80.35',
                'bol_active' => 1,
            ),
            489 => 
            array (
                'id_model' => 2490,
                'id_manufacturer' => 306,
                'str_description' => 'b-90.35',
                'str_description_slug' => 'b-90.35',
                'bol_active' => 1,
            ),
            490 => 
            array (
                'id_model' => 2491,
                'id_manufacturer' => 306,
                'str_description' => 'captur',
                'str_description_slug' => 'captur',
                'bol_active' => 1,
            ),
            491 => 
            array (
                'id_model' => 2492,
                'id_manufacturer' => 306,
                'str_description' => 'caravelle',
                'str_description_slug' => 'caravelle',
                'bol_active' => 1,
            ),
            492 => 
            array (
                'id_model' => 2493,
                'id_manufacturer' => 306,
                'str_description' => 'celtaquatre',
                'str_description_slug' => 'celtaquatre',
                'bol_active' => 1,
            ),
            493 => 
            array (
                'id_model' => 2494,
                'id_manufacturer' => 306,
                'str_description' => 'dauphine',
                'str_description_slug' => 'dauphine',
                'bol_active' => 1,
            ),
            494 => 
            array (
                'id_model' => 2495,
                'id_manufacturer' => 306,
                'str_description' => 'duster',
                'str_description_slug' => 'duster',
                'bol_active' => 1,
            ),
            495 => 
            array (
                'id_model' => 2496,
                'id_manufacturer' => 306,
                'str_description' => 'espace',
                'str_description_slug' => 'espace',
                'bol_active' => 1,
            ),
            496 => 
            array (
                'id_model' => 2497,
                'id_manufacturer' => 306,
                'str_description' => 'estafette',
                'str_description_slug' => 'estafette',
                'bol_active' => 1,
            ),
            497 => 
            array (
                'id_model' => 2498,
                'id_manufacturer' => 306,
                'str_description' => 'express',
                'str_description_slug' => 'express',
                'bol_active' => 1,
            ),
            498 => 
            array (
                'id_model' => 2499,
                'id_manufacturer' => 306,
                'str_description' => 'florida',
                'str_description_slug' => 'florida',
                'bol_active' => 1,
            ),
            499 => 
            array (
                'id_model' => 2500,
                'id_manufacturer' => 306,
                'str_description' => 'fluence',
                'str_description_slug' => 'fluence',
                'bol_active' => 1,
            ),
        ));
        \DB::table('automobile_models')->insert(array (
            0 => 
            array (
                'id_model' => 2501,
                'id_manufacturer' => 306,
                'str_description' => 'fregat',
                'str_description_slug' => 'fregat',
                'bol_active' => 1,
            ),
            1 => 
            array (
                'id_model' => 2502,
                'id_manufacturer' => 306,
                'str_description' => 'fuego',
                'str_description_slug' => 'fuego',
                'bol_active' => 1,
            ),
            2 => 
            array (
                'id_model' => 2503,
                'id_manufacturer' => 306,
                'str_description' => 'goelette',
                'str_description_slug' => 'goelette',
                'bol_active' => 1,
            ),
            3 => 
            array (
                'id_model' => 2504,
                'id_manufacturer' => 306,
                'str_description' => 'gordini',
                'str_description_slug' => 'gordini',
                'bol_active' => 1,
            ),
            4 => 
            array (
                'id_model' => 2505,
                'id_manufacturer' => 306,
                'str_description' => 'grand espace',
                'str_description_slug' => 'grand-espace',
                'bol_active' => 1,
            ),
            5 => 
            array (
                'id_model' => 2506,
                'id_manufacturer' => 306,
                'str_description' => 'grand kangoo',
                'str_description_slug' => 'grand-kangoo',
                'bol_active' => 1,
            ),
            6 => 
            array (
                'id_model' => 2507,
                'id_manufacturer' => 306,
                'str_description' => 'grand modus',
                'str_description_slug' => 'grand-modus',
                'bol_active' => 1,
            ),
            7 => 
            array (
                'id_model' => 2508,
                'id_manufacturer' => 306,
                'str_description' => 'juvaquatre',
                'str_description_slug' => 'juvaquatre',
                'bol_active' => 1,
            ),
            8 => 
            array (
                'id_model' => 2509,
                'id_manufacturer' => 306,
                'str_description' => 'kadjar',
                'str_description_slug' => 'kadjar',
                'bol_active' => 1,
            ),
            9 => 
            array (
                'id_model' => 2510,
                'id_manufacturer' => 306,
                'str_description' => 'kangoo be bop',
                'str_description_slug' => 'kangoo-be-bop',
                'bol_active' => 1,
            ),
            10 => 
            array (
                'id_model' => 2511,
                'id_manufacturer' => 306,
                'str_description' => 'kangoo compact',
                'str_description_slug' => 'kangoo-compact',
                'bol_active' => 1,
            ),
            11 => 
            array (
                'id_model' => 2512,
                'id_manufacturer' => 306,
                'str_description' => 'kangoo maxi',
                'str_description_slug' => 'kangoo-maxi',
                'bol_active' => 1,
            ),
            12 => 
            array (
                'id_model' => 2513,
                'id_manufacturer' => 306,
                'str_description' => 'koleos',
                'str_description_slug' => 'koleos',
                'bol_active' => 1,
            ),
            13 => 
            array (
                'id_model' => 2514,
                'id_manufacturer' => 306,
                'str_description' => 'kz',
                'str_description_slug' => 'kz',
                'bol_active' => 1,
            ),
            14 => 
            array (
                'id_model' => 2515,
                'id_manufacturer' => 306,
                'str_description' => 'laguna coupe',
                'str_description_slug' => 'laguna-coupe',
                'bol_active' => 1,
            ),
            15 => 
            array (
                'id_model' => 2516,
                'id_manufacturer' => 306,
                'str_description' => 'latitude',
                'str_description_slug' => 'latitude',
                'bol_active' => 1,
            ),
            16 => 
            array (
                'id_model' => 2517,
                'id_manufacturer' => 306,
                'str_description' => 'major r',
                'str_description_slug' => 'major-r',
                'bol_active' => 1,
            ),
            17 => 
            array (
                'id_model' => 2518,
                'id_manufacturer' => 306,
                'str_description' => 'mascott',
                'str_description_slug' => 'mascott',
                'bol_active' => 1,
            ),
            18 => 
            array (
                'id_model' => 2519,
                'id_manufacturer' => 306,
                'str_description' => 'master',
                'str_description_slug' => 'master',
                'bol_active' => 1,
            ),
            19 => 
            array (
                'id_model' => 2520,
                'id_manufacturer' => 306,
                'str_description' => 'maxity',
                'str_description_slug' => 'maxity',
                'bol_active' => 1,
            ),
            20 => 
            array (
                'id_model' => 2521,
                'id_manufacturer' => 306,
                'str_description' => 'megane coupe cabrio',
                'str_description_slug' => 'megane-coupe-cabrio',
                'bol_active' => 1,
            ),
            21 => 
            array (
                'id_model' => 2522,
                'id_manufacturer' => 306,
                'str_description' => 'megane sport tourer',
                'str_description_slug' => 'megane-sport-tourer',
                'bol_active' => 1,
            ),
            22 => 
            array (
                'id_model' => 2523,
                'id_manufacturer' => 306,
                'str_description' => 'messenger b',
                'str_description_slug' => 'messenger-b',
                'bol_active' => 1,
            ),
            23 => 
            array (
                'id_model' => 2524,
                'id_manufacturer' => 306,
                'str_description' => 'modus',
                'str_description_slug' => 'modus',
                'bol_active' => 1,
            ),
            24 => 
            array (
                'id_model' => 2525,
                'id_manufacturer' => 306,
                'str_description' => 'monaquatre',
                'str_description_slug' => 'monaquatre',
                'bol_active' => 1,
            ),
            25 => 
            array (
                'id_model' => 2526,
                'id_manufacturer' => 306,
                'str_description' => 'monasix',
                'str_description_slug' => 'monasix',
                'bol_active' => 1,
            ),
            26 => 
            array (
                'id_model' => 2527,
                'id_manufacturer' => 306,
                'str_description' => 'nn',
                'str_description_slug' => 'nn',
                'bol_active' => 1,
            ),
            27 => 
            array (
                'id_model' => 2528,
                'id_manufacturer' => 306,
                'str_description' => 'ondine',
                'str_description_slug' => 'ondine',
                'bol_active' => 1,
            ),
            28 => 
            array (
                'id_model' => 2529,
                'id_manufacturer' => 306,
                'str_description' => 'primaquatre',
                'str_description_slug' => 'primaquatre',
                'bol_active' => 1,
            ),
            29 => 
            array (
                'id_model' => 2530,
                'id_manufacturer' => 306,
                'str_description' => 'rapid',
                'str_description_slug' => 'rapid',
                'bol_active' => 1,
            ),
            30 => 
            array (
                'id_model' => 2531,
                'id_manufacturer' => 306,
                'str_description' => 'rodeo 4',
                'str_description_slug' => 'rodeo-4',
                'bol_active' => 1,
            ),
            31 => 
            array (
                'id_model' => 2532,
                'id_manufacturer' => 306,
                'str_description' => 'rodeo 6',
                'str_description_slug' => 'rodeo-6',
                'bol_active' => 1,
            ),
            32 => 
            array (
                'id_model' => 2533,
                'id_manufacturer' => 306,
                'str_description' => 'safrane',
                'str_description_slug' => 'safrane',
                'bol_active' => 1,
            ),
            33 => 
            array (
                'id_model' => 2534,
                'id_manufacturer' => 306,
                'str_description' => 'spider',
                'str_description_slug' => 'spider',
                'bol_active' => 1,
            ),
            34 => 
            array (
                'id_model' => 2535,
                'id_manufacturer' => 306,
                'str_description' => 'supercinco',
                'str_description_slug' => 'supercinco',
                'bol_active' => 1,
            ),
            35 => 
            array (
                'id_model' => 2536,
                'id_manufacturer' => 306,
                'str_description' => 'symbol',
                'str_description_slug' => 'symbol',
                'bol_active' => 1,
            ),
            36 => 
            array (
                'id_model' => 2537,
                'id_manufacturer' => 306,
                'str_description' => 'talisman',
                'str_description_slug' => 'talisman',
                'bol_active' => 1,
            ),
            37 => 
            array (
                'id_model' => 2538,
                'id_manufacturer' => 306,
                'str_description' => 'talisman sport toure',
                'str_description_slug' => 'talisman-sport-toure',
                'bol_active' => 1,
            ),
            38 => 
            array (
                'id_model' => 2539,
                'id_manufacturer' => 306,
                'str_description' => 'thalia',
                'str_description_slug' => 'thalia',
                'bol_active' => 1,
            ),
            39 => 
            array (
                'id_model' => 2540,
                'id_manufacturer' => 306,
                'str_description' => 'torino',
                'str_description_slug' => 'torino',
                'bol_active' => 1,
            ),
            40 => 
            array (
                'id_model' => 2541,
                'id_manufacturer' => 306,
                'str_description' => 'truck',
                'str_description_slug' => 'truck',
                'bol_active' => 1,
            ),
            41 => 
            array (
                'id_model' => 2542,
                'id_manufacturer' => 306,
                'str_description' => 'twizy',
                'str_description_slug' => 'twizy',
                'bol_active' => 1,
            ),
            42 => 
            array (
                'id_model' => 2543,
                'id_manufacturer' => 306,
                'str_description' => 'type ah',
                'str_description_slug' => 'type-ah',
                'bol_active' => 1,
            ),
            43 => 
            array (
                'id_model' => 2544,
                'id_manufacturer' => 306,
                'str_description' => 'vel satis',
                'str_description_slug' => 'vel-satis',
                'bol_active' => 1,
            ),
            44 => 
            array (
                'id_model' => 2545,
                'id_manufacturer' => 306,
                'str_description' => 'vivasport',
                'str_description_slug' => 'vivasport',
                'bol_active' => 1,
            ),
            45 => 
            array (
                'id_model' => 2546,
                'id_manufacturer' => 306,
                'str_description' => 'wind',
                'str_description_slug' => 'wind',
                'bol_active' => 1,
            ),
            46 => 
            array (
                'id_model' => 2547,
                'id_manufacturer' => 306,
                'str_description' => 'zoe',
                'str_description_slug' => 'zoe',
                'bol_active' => 1,
            ),
            47 => 
            array (
                'id_model' => 2548,
                'id_manufacturer' => 307,
                'str_description' => 'off-road',
                'str_description_slug' => 'off-road',
                'bol_active' => 1,
            ),
            48 => 
            array (
                'id_model' => 2549,
                'id_manufacturer' => 307,
                'str_description' => 'rlg1',
                'str_description_slug' => 'rlg1',
                'bol_active' => 1,
            ),
            49 => 
            array (
                'id_model' => 2550,
                'id_manufacturer' => 308,
                'str_description' => 'deluxe',
                'str_description_slug' => 'deluxe',
                'bol_active' => 1,
            ),
            50 => 
            array (
                'id_model' => 2551,
                'id_manufacturer' => 308,
                'str_description' => 'reva i',
                'str_description_slug' => 'reva-i',
                'bol_active' => 1,
            ),
            51 => 
            array (
                'id_model' => 2552,
                'id_manufacturer' => 308,
                'str_description' => 'reva l-ion',
                'str_description_slug' => 'reva-l-ion',
                'bol_active' => 1,
            ),
            52 => 
            array (
                'id_model' => 2553,
                'id_manufacturer' => 308,
                'str_description' => 'standard',
                'str_description_slug' => 'standard',
                'bol_active' => 1,
            ),
            53 => 
            array (
                'id_model' => 2554,
                'id_manufacturer' => 309,
                'str_description' => '4/72',
                'str_description_slug' => '4/72',
                'bol_active' => 1,
            ),
            54 => 
            array (
                'id_model' => 2555,
                'id_manufacturer' => 309,
                'str_description' => 'elf',
                'str_description_slug' => 'elf',
                'bol_active' => 1,
            ),
            55 => 
            array (
                'id_model' => 2556,
                'id_manufacturer' => 309,
                'str_description' => 'rmb',
                'str_description_slug' => 'rmb',
                'bol_active' => 1,
            ),
            56 => 
            array (
                'id_model' => 2557,
                'id_manufacturer' => 309,
                'str_description' => 'special',
                'str_description_slug' => 'special',
                'bol_active' => 1,
            ),
            57 => 
            array (
                'id_model' => 2558,
                'id_manufacturer' => 310,
                'str_description' => 'isigo',
                'str_description_slug' => 'isigo',
                'bol_active' => 1,
            ),
            58 => 
            array (
                'id_model' => 2559,
                'id_manufacturer' => 311,
                'str_description' => 'sedan',
                'str_description_slug' => 'sedan',
                'bol_active' => 1,
            ),
            59 => 
            array (
                'id_model' => 2560,
                'id_manufacturer' => 312,
                'str_description' => '20',
                'str_description_slug' => '20',
                'bol_active' => 1,
            ),
            60 => 
            array (
                'id_model' => 2561,
                'id_manufacturer' => 312,
                'str_description' => '20/25',
                'str_description_slug' => '20/25',
                'bol_active' => 1,
            ),
            61 => 
            array (
                'id_model' => 2562,
                'id_manufacturer' => 312,
                'str_description' => '25/30',
                'str_description_slug' => '25/30',
                'bol_active' => 1,
            ),
            62 => 
            array (
                'id_model' => 2563,
                'id_manufacturer' => 312,
                'str_description' => 'bristol',
                'str_description_slug' => 'bristol',
                'bol_active' => 1,
            ),
            63 => 
            array (
                'id_model' => 2564,
                'id_manufacturer' => 312,
                'str_description' => 'camargue',
                'str_description_slug' => 'camargue',
                'bol_active' => 1,
            ),
            64 => 
            array (
                'id_model' => 2565,
                'id_manufacturer' => 312,
                'str_description' => 'corniche',
                'str_description_slug' => 'corniche',
                'bol_active' => 1,
            ),
            65 => 
            array (
                'id_model' => 2566,
                'id_manufacturer' => 312,
                'str_description' => 'corniche ii',
                'str_description_slug' => 'corniche-ii',
                'bol_active' => 1,
            ),
            66 => 
            array (
                'id_model' => 2567,
                'id_manufacturer' => 312,
                'str_description' => 'corniche iii',
                'str_description_slug' => 'corniche-iii',
                'bol_active' => 1,
            ),
            67 => 
            array (
                'id_model' => 2568,
                'id_manufacturer' => 312,
                'str_description' => 'flying',
                'str_description_slug' => 'flying',
                'bol_active' => 1,
            ),
            68 => 
            array (
                'id_model' => 2569,
                'id_manufacturer' => 312,
                'str_description' => 'ghost',
                'str_description_slug' => 'ghost',
                'bol_active' => 1,
            ),
            69 => 
            array (
                'id_model' => 2570,
                'id_manufacturer' => 312,
                'str_description' => 'park ward',
                'str_description_slug' => 'park-ward',
                'bol_active' => 1,
            ),
            70 => 
            array (
                'id_model' => 2571,
                'id_manufacturer' => 312,
                'str_description' => 'phantom',
                'str_description_slug' => 'phantom',
                'bol_active' => 1,
            ),
            71 => 
            array (
                'id_model' => 2572,
                'id_manufacturer' => 312,
                'str_description' => 'phantom ii',
                'str_description_slug' => 'phantom-ii',
                'bol_active' => 1,
            ),
            72 => 
            array (
                'id_model' => 2573,
                'id_manufacturer' => 312,
                'str_description' => 'phantom iii',
                'str_description_slug' => 'phantom-iii',
                'bol_active' => 1,
            ),
            73 => 
            array (
                'id_model' => 2574,
                'id_manufacturer' => 312,
                'str_description' => 'phantom v',
                'str_description_slug' => 'phantom-v',
                'bol_active' => 1,
            ),
            74 => 
            array (
                'id_model' => 2575,
                'id_manufacturer' => 312,
                'str_description' => 'phantom vi',
                'str_description_slug' => 'phantom-vi',
                'bol_active' => 1,
            ),
            75 => 
            array (
                'id_model' => 2576,
                'id_manufacturer' => 312,
                'str_description' => 'silver',
                'str_description_slug' => 'silver',
                'bol_active' => 1,
            ),
            76 => 
            array (
                'id_model' => 2577,
                'id_manufacturer' => 312,
                'str_description' => 'silver seraph',
                'str_description_slug' => 'silver-seraph',
                'bol_active' => 1,
            ),
            77 => 
            array (
                'id_model' => 2578,
                'id_manufacturer' => 312,
                'str_description' => 'silver wraith',
                'str_description_slug' => 'silver-wraith',
                'bol_active' => 1,
            ),
            78 => 
            array (
                'id_model' => 2579,
                'id_manufacturer' => 312,
                'str_description' => 'touring',
                'str_description_slug' => 'touring',
                'bol_active' => 1,
            ),
            79 => 
            array (
                'id_model' => 2580,
                'id_manufacturer' => 312,
                'str_description' => 'wraith',
                'str_description_slug' => 'wraith',
                'bol_active' => 1,
            ),
            80 => 
            array (
                'id_model' => 2581,
                'id_manufacturer' => 313,
                'str_description' => 'lantana',
                'str_description_slug' => 'lantana',
                'bol_active' => 1,
            ),
            81 => 
            array (
                'id_model' => 2582,
                'id_manufacturer' => 314,
                'str_description' => '25',
                'str_description_slug' => '25',
                'bol_active' => 1,
            ),
            82 => 
            array (
                'id_model' => 2583,
                'id_manufacturer' => 314,
                'str_description' => '45',
                'str_description_slug' => '45',
                'bol_active' => 1,
            ),
            83 => 
            array (
                'id_model' => 2584,
                'id_manufacturer' => 314,
                'str_description' => '75',
                'str_description_slug' => '75',
                'bol_active' => 1,
            ),
            84 => 
            array (
                'id_model' => 2585,
                'id_manufacturer' => 314,
                'str_description' => '110',
                'str_description_slug' => '110',
                'bol_active' => 1,
            ),
            85 => 
            array (
                'id_model' => 2586,
                'id_manufacturer' => 314,
                'str_description' => '111',
                'str_description_slug' => '111',
                'bol_active' => 1,
            ),
            86 => 
            array (
                'id_model' => 2587,
                'id_manufacturer' => 314,
                'str_description' => '114',
                'str_description_slug' => '114',
                'bol_active' => 1,
            ),
            87 => 
            array (
                'id_model' => 2588,
                'id_manufacturer' => 314,
                'str_description' => '115',
                'str_description_slug' => '115',
                'bol_active' => 1,
            ),
            88 => 
            array (
                'id_model' => 2589,
                'id_manufacturer' => 314,
                'str_description' => '200',
                'str_description_slug' => '200',
                'bol_active' => 1,
            ),
            89 => 
            array (
                'id_model' => 2590,
                'id_manufacturer' => 314,
                'str_description' => '211',
                'str_description_slug' => '211',
                'bol_active' => 1,
            ),
            90 => 
            array (
                'id_model' => 2591,
                'id_manufacturer' => 314,
                'str_description' => '213',
                'str_description_slug' => '213',
                'bol_active' => 1,
            ),
            91 => 
            array (
                'id_model' => 2592,
                'id_manufacturer' => 314,
                'str_description' => '214',
                'str_description_slug' => '214',
                'bol_active' => 1,
            ),
            92 => 
            array (
                'id_model' => 2593,
                'id_manufacturer' => 314,
                'str_description' => '216',
                'str_description_slug' => '216',
                'bol_active' => 1,
            ),
            93 => 
            array (
                'id_model' => 2594,
                'id_manufacturer' => 314,
                'str_description' => '218',
                'str_description_slug' => '218',
                'bol_active' => 1,
            ),
            94 => 
            array (
                'id_model' => 2595,
                'id_manufacturer' => 314,
                'str_description' => '220',
                'str_description_slug' => '220',
                'bol_active' => 1,
            ),
            95 => 
            array (
                'id_model' => 2596,
                'id_manufacturer' => 314,
                'str_description' => '414',
                'str_description_slug' => '414',
                'bol_active' => 1,
            ),
            96 => 
            array (
                'id_model' => 2597,
                'id_manufacturer' => 314,
                'str_description' => '416',
                'str_description_slug' => '416',
                'bol_active' => 1,
            ),
            97 => 
            array (
                'id_model' => 2598,
                'id_manufacturer' => 314,
                'str_description' => '418',
                'str_description_slug' => '418',
                'bol_active' => 1,
            ),
            98 => 
            array (
                'id_model' => 2599,
                'id_manufacturer' => 314,
                'str_description' => '420',
                'str_description_slug' => '420',
                'bol_active' => 1,
            ),
            99 => 
            array (
                'id_model' => 2600,
                'id_manufacturer' => 314,
                'str_description' => '618',
                'str_description_slug' => '618',
                'bol_active' => 1,
            ),
            100 => 
            array (
                'id_model' => 2601,
                'id_manufacturer' => 314,
                'str_description' => '620',
                'str_description_slug' => '620',
                'bol_active' => 1,
            ),
            101 => 
            array (
                'id_model' => 2602,
                'id_manufacturer' => 314,
                'str_description' => '623',
                'str_description_slug' => '623',
                'bol_active' => 1,
            ),
            102 => 
            array (
                'id_model' => 2603,
                'id_manufacturer' => 314,
                'str_description' => '820',
                'str_description_slug' => '820',
                'bol_active' => 1,
            ),
            103 => 
            array (
                'id_model' => 2604,
                'id_manufacturer' => 314,
                'str_description' => '825',
                'str_description_slug' => '825',
                'bol_active' => 1,
            ),
            104 => 
            array (
                'id_model' => 2605,
                'id_manufacturer' => 314,
                'str_description' => '827',
                'str_description_slug' => '827',
                'bol_active' => 1,
            ),
            105 => 
            array (
                'id_model' => 2606,
                'id_manufacturer' => 314,
                'str_description' => '2000',
                'str_description_slug' => '2000',
                'bol_active' => 1,
            ),
            106 => 
            array (
                'id_model' => 2607,
                'id_manufacturer' => 314,
                'str_description' => '2200',
                'str_description_slug' => '2200',
                'bol_active' => 1,
            ),
            107 => 
            array (
                'id_model' => 2608,
                'id_manufacturer' => 314,
                'str_description' => '2400',
                'str_description_slug' => '2400',
                'bol_active' => 1,
            ),
            108 => 
            array (
                'id_model' => 2609,
                'id_manufacturer' => 314,
                'str_description' => '2600',
                'str_description_slug' => '2600',
                'bol_active' => 1,
            ),
            109 => 
            array (
                'id_model' => 2610,
                'id_manufacturer' => 314,
                'str_description' => '3500',
                'str_description_slug' => '3500',
                'bol_active' => 1,
            ),
            110 => 
            array (
                'id_model' => 2611,
                'id_manufacturer' => 314,
                'str_description' => 'cityrover',
                'str_description_slug' => 'cityrover',
                'bol_active' => 1,
            ),
            111 => 
            array (
                'id_model' => 2612,
                'id_manufacturer' => 314,
                'str_description' => 'maestro',
                'str_description_slug' => 'maestro',
                'bol_active' => 1,
            ),
            112 => 
            array (
                'id_model' => 2613,
                'id_manufacturer' => 314,
                'str_description' => 'metro',
                'str_description_slug' => 'metro',
                'bol_active' => 1,
            ),
            113 => 
            array (
                'id_model' => 2614,
                'id_manufacturer' => 314,
                'str_description' => 'mg tf',
                'str_description_slug' => 'mg-tf',
                'bol_active' => 1,
            ),
            114 => 
            array (
                'id_model' => 2615,
                'id_manufacturer' => 314,
                'str_description' => 'mg xpower',
                'str_description_slug' => 'mg-xpower',
                'bol_active' => 1,
            ),
            115 => 
            array (
                'id_model' => 2616,
                'id_manufacturer' => 314,
                'str_description' => 'mg zr',
                'str_description_slug' => 'mg-zr',
                'bol_active' => 1,
            ),
            116 => 
            array (
                'id_model' => 2617,
                'id_manufacturer' => 314,
                'str_description' => 'mg zs',
                'str_description_slug' => 'mg-zs',
                'bol_active' => 1,
            ),
            117 => 
            array (
                'id_model' => 2618,
                'id_manufacturer' => 314,
                'str_description' => 'mg zt',
                'str_description_slug' => 'mg-zt',
                'bol_active' => 1,
            ),
            118 => 
            array (
                'id_model' => 2619,
                'id_manufacturer' => 314,
                'str_description' => 'mgf',
                'str_description_slug' => 'mgf',
                'bol_active' => 1,
            ),
            119 => 
            array (
                'id_model' => 2620,
                'id_manufacturer' => 314,
                'str_description' => 'mini',
                'str_description_slug' => 'mini',
                'bol_active' => 1,
            ),
            120 => 
            array (
                'id_model' => 2621,
                'id_manufacturer' => 314,
                'str_description' => 'montego',
                'str_description_slug' => 'montego',
                'bol_active' => 1,
            ),
            121 => 
            array (
                'id_model' => 2622,
                'id_manufacturer' => 314,
                'str_description' => 'p4 90',
                'str_description_slug' => 'p4-90',
                'bol_active' => 1,
            ),
            122 => 
            array (
                'id_model' => 2623,
                'id_manufacturer' => 314,
                'str_description' => 'p5b',
                'str_description_slug' => 'p5b',
                'bol_active' => 1,
            ),
            123 => 
            array (
                'id_model' => 2624,
                'id_manufacturer' => 314,
                'str_description' => 'streetwise',
                'str_description_slug' => 'streetwise',
                'bol_active' => 1,
            ),
            124 => 
            array (
                'id_model' => 2625,
                'id_manufacturer' => 315,
                'str_description' => '9-3',
                'str_description_slug' => '9-3',
                'bol_active' => 1,
            ),
            125 => 
            array (
                'id_model' => 2626,
                'id_manufacturer' => 315,
                'str_description' => '9-3x',
                'str_description_slug' => '9-3x',
                'bol_active' => 1,
            ),
            126 => 
            array (
                'id_model' => 2627,
                'id_manufacturer' => 315,
                'str_description' => '9-4x',
                'str_description_slug' => '9-4x',
                'bol_active' => 1,
            ),
            127 => 
            array (
                'id_model' => 2628,
                'id_manufacturer' => 315,
                'str_description' => '9-5',
                'str_description_slug' => '9-5',
                'bol_active' => 1,
            ),
            128 => 
            array (
                'id_model' => 2629,
                'id_manufacturer' => 315,
                'str_description' => '9-7x',
                'str_description_slug' => '9-7x',
                'bol_active' => 1,
            ),
            129 => 
            array (
                'id_model' => 2630,
                'id_manufacturer' => 315,
                'str_description' => '93',
                'str_description_slug' => '93',
                'bol_active' => 1,
            ),
            130 => 
            array (
                'id_model' => 2631,
                'id_manufacturer' => 315,
                'str_description' => '96',
                'str_description_slug' => '96',
                'bol_active' => 1,
            ),
            131 => 
            array (
                'id_model' => 2632,
                'id_manufacturer' => 315,
                'str_description' => '99',
                'str_description_slug' => '99',
                'bol_active' => 1,
            ),
            132 => 
            array (
                'id_model' => 2633,
                'id_manufacturer' => 315,
                'str_description' => '900',
                'str_description_slug' => '900',
                'bol_active' => 1,
            ),
            133 => 
            array (
                'id_model' => 2634,
                'id_manufacturer' => 315,
                'str_description' => '9000',
                'str_description_slug' => '9000',
                'bol_active' => 1,
            ),
            134 => 
            array (
                'id_model' => 2635,
                'id_manufacturer' => 315,
                'str_description' => 'sonett iii',
                'str_description_slug' => 'sonett-iii',
                'bol_active' => 1,
            ),
            135 => 
            array (
                'id_model' => 2636,
                'id_manufacturer' => 316,
                'str_description' => 'a1',
                'str_description_slug' => 'a1',
                'bol_active' => 1,
            ),
            136 => 
            array (
                'id_model' => 2637,
                'id_manufacturer' => 316,
                'str_description' => 'cobra',
                'str_description_slug' => 'cobra',
                'bol_active' => 1,
            ),
            137 => 
            array (
                'id_model' => 2638,
                'id_manufacturer' => 316,
                'str_description' => 'sunroad classic',
                'str_description_slug' => 'sunroad-classic',
                'bol_active' => 1,
            ),
            138 => 
            array (
                'id_model' => 2639,
                'id_manufacturer' => 317,
                'str_description' => 'buggy',
                'str_description_slug' => 'buggy',
                'bol_active' => 1,
            ),
            139 => 
            array (
                'id_model' => 2640,
                'id_manufacturer' => 318,
                'str_description' => 'sarir',
                'str_description_slug' => 'sarir',
                'bol_active' => 1,
            ),
            140 => 
            array (
                'id_model' => 2641,
                'id_manufacturer' => 319,
                'str_description' => 'sv',
                'str_description_slug' => 'sv',
                'bol_active' => 1,
            ),
            141 => 
            array (
                'id_model' => 2642,
                'id_manufacturer' => 320,
                'str_description' => '2.5',
                'str_description_slug' => '2.5',
                'bol_active' => 1,
            ),
            142 => 
            array (
                'id_model' => 2643,
                'id_manufacturer' => 320,
                'str_description' => '300',
                'str_description_slug' => '300',
                'bol_active' => 1,
            ),
            143 => 
            array (
                'id_model' => 2644,
                'id_manufacturer' => 320,
                'str_description' => '350',
                'str_description_slug' => '350',
                'bol_active' => 1,
            ),
            144 => 
            array (
                'id_model' => 2645,
                'id_manufacturer' => 320,
                'str_description' => '2000',
                'str_description_slug' => '2000',
                'bol_active' => 1,
            ),
            145 => 
            array (
                'id_model' => 2646,
                'id_manufacturer' => 320,
                'str_description' => '2500',
                'str_description_slug' => '2500',
                'bol_active' => 1,
            ),
            146 => 
            array (
                'id_model' => 2647,
                'id_manufacturer' => 320,
                'str_description' => '3500',
                'str_description_slug' => '3500',
                'bol_active' => 1,
            ),
            147 => 
            array (
                'id_model' => 2648,
                'id_manufacturer' => 320,
                'str_description' => 'anibal',
                'str_description_slug' => 'anibal',
                'bol_active' => 1,
            ),
            148 => 
            array (
                'id_model' => 2649,
                'id_manufacturer' => 320,
                'str_description' => 'land rover',
                'str_description_slug' => 'land-rover',
                'bol_active' => 1,
            ),
            149 => 
            array (
                'id_model' => 2650,
                'id_manufacturer' => 320,
                'str_description' => 'mahori',
                'str_description_slug' => 'mahori',
                'bol_active' => 1,
            ),
            150 => 
            array (
                'id_model' => 2651,
                'id_manufacturer' => 321,
                'str_description' => 'aura',
                'str_description_slug' => 'aura',
                'bol_active' => 1,
            ),
            151 => 
            array (
                'id_model' => 2652,
                'id_manufacturer' => 321,
                'str_description' => 'coupe',
                'str_description_slug' => 'coupe',
                'bol_active' => 1,
            ),
            152 => 
            array (
                'id_model' => 2653,
                'id_manufacturer' => 321,
                'str_description' => 'ion',
                'str_description_slug' => 'ion',
                'bol_active' => 1,
            ),
            153 => 
            array (
                'id_model' => 2654,
                'id_manufacturer' => 321,
                'str_description' => 'sl',
                'str_description_slug' => 'sl',
                'bol_active' => 1,
            ),
            154 => 
            array (
                'id_model' => 2655,
                'id_manufacturer' => 322,
                'str_description' => '415',
                'str_description_slug' => '415',
                'bol_active' => 1,
            ),
            155 => 
            array (
                'id_model' => 2656,
                'id_manufacturer' => 322,
                'str_description' => '5721',
                'str_description_slug' => '5721',
                'bol_active' => 1,
            ),
            156 => 
            array (
                'id_model' => 2657,
                'id_manufacturer' => 322,
                'str_description' => 'j4',
                'str_description_slug' => 'j4',
                'bol_active' => 1,
            ),
            157 => 
            array (
                'id_model' => 2658,
                'id_manufacturer' => 322,
                'str_description' => 's 217',
                'str_description_slug' => 's-217',
                'bol_active' => 1,
            ),
            158 => 
            array (
                'id_model' => 2659,
                'id_manufacturer' => 322,
                'str_description' => 's740',
                'str_description_slug' => 's740',
                'bol_active' => 1,
            ),
            159 => 
            array (
                'id_model' => 2660,
                'id_manufacturer' => 323,
                'str_description' => 's',
                'str_description_slug' => 's',
                'bol_active' => 1,
            ),
            160 => 
            array (
                'id_model' => 2661,
                'id_manufacturer' => 324,
                'str_description' => 'sm',
                'str_description_slug' => 'sm',
                'bol_active' => 1,
            ),
            161 => 
            array (
                'id_model' => 2662,
                'id_manufacturer' => 325,
                'str_description' => 'tc',
                'str_description_slug' => 'tc',
                'bol_active' => 1,
            ),
            162 => 
            array (
                'id_model' => 2663,
                'id_manufacturer' => 326,
                'str_description' => '124',
                'str_description_slug' => '124',
                'bol_active' => 1,
            ),
            163 => 
            array (
                'id_model' => 2664,
                'id_manufacturer' => 326,
                'str_description' => '127',
                'str_description_slug' => '127',
                'bol_active' => 1,
            ),
            164 => 
            array (
                'id_model' => 2665,
                'id_manufacturer' => 326,
                'str_description' => '128',
                'str_description_slug' => '128',
                'bol_active' => 1,
            ),
            165 => 
            array (
                'id_model' => 2666,
                'id_manufacturer' => 326,
                'str_description' => '131',
                'str_description_slug' => '131',
                'bol_active' => 1,
            ),
            166 => 
            array (
                'id_model' => 2667,
                'id_manufacturer' => 326,
                'str_description' => '132',
                'str_description_slug' => '132',
                'bol_active' => 1,
            ),
            167 => 
            array (
                'id_model' => 2668,
                'id_manufacturer' => 326,
                'str_description' => '133',
                'str_description_slug' => '133',
                'bol_active' => 1,
            ),
            168 => 
            array (
                'id_model' => 2669,
                'id_manufacturer' => 326,
                'str_description' => '600',
                'str_description_slug' => '600',
                'bol_active' => 1,
            ),
            169 => 
            array (
                'id_model' => 2670,
                'id_manufacturer' => 326,
                'str_description' => '800',
                'str_description_slug' => '800',
                'bol_active' => 1,
            ),
            170 => 
            array (
                'id_model' => 2671,
                'id_manufacturer' => 326,
                'str_description' => '850',
                'str_description_slug' => '850',
                'bol_active' => 1,
            ),
            171 => 
            array (
                'id_model' => 2672,
                'id_manufacturer' => 326,
                'str_description' => '1200',
                'str_description_slug' => '1200',
                'bol_active' => 1,
            ),
            172 => 
            array (
                'id_model' => 2673,
                'id_manufacturer' => 326,
                'str_description' => '1400',
                'str_description_slug' => '1400',
                'bol_active' => 1,
            ),
            173 => 
            array (
                'id_model' => 2674,
                'id_manufacturer' => 326,
                'str_description' => '1430',
                'str_description_slug' => '1430',
                'bol_active' => 1,
            ),
            174 => 
            array (
                'id_model' => 2675,
                'id_manufacturer' => 326,
                'str_description' => '1500',
                'str_description_slug' => '1500',
                'bol_active' => 1,
            ),
            175 => 
            array (
                'id_model' => 2676,
                'id_manufacturer' => 326,
                'str_description' => '1800',
                'str_description_slug' => '1800',
                'bol_active' => 1,
            ),
            176 => 
            array (
                'id_model' => 2677,
                'id_manufacturer' => 326,
                'str_description' => '2000',
                'str_description_slug' => '2000',
                'bol_active' => 1,
            ),
            177 => 
            array (
                'id_model' => 2678,
                'id_manufacturer' => 326,
                'str_description' => 'alba-panda',
                'str_description_slug' => 'alba-panda',
                'bol_active' => 1,
            ),
            178 => 
            array (
                'id_model' => 2679,
                'id_manufacturer' => 326,
                'str_description' => 'altea freetrack',
                'str_description_slug' => 'altea-freetrack',
                'bol_active' => 1,
            ),
            179 => 
            array (
                'id_model' => 2680,
                'id_manufacturer' => 326,
                'str_description' => 'ateca',
                'str_description_slug' => 'ateca',
                'bol_active' => 1,
            ),
            180 => 
            array (
                'id_model' => 2681,
                'id_manufacturer' => 326,
                'str_description' => 'fura',
                'str_description_slug' => 'fura',
                'bol_active' => 1,
            ),
            181 => 
            array (
                'id_model' => 2682,
                'id_manufacturer' => 326,
                'str_description' => 'imesa',
                'str_description_slug' => 'imesa',
                'bol_active' => 1,
            ),
            182 => 
            array (
                'id_model' => 2683,
                'id_manufacturer' => 326,
                'str_description' => 'inca',
                'str_description_slug' => 'inca',
                'bol_active' => 1,
            ),
            183 => 
            array (
                'id_model' => 2684,
                'id_manufacturer' => 326,
                'str_description' => 'leon x-perience',
                'str_description_slug' => 'leon-x-perience',
                'bol_active' => 1,
            ),
            184 => 
            array (
                'id_model' => 2685,
                'id_manufacturer' => 326,
                'str_description' => 'malaga',
                'str_description_slug' => 'malaga',
                'bol_active' => 1,
            ),
            185 => 
            array (
                'id_model' => 2686,
                'id_manufacturer' => 326,
                'str_description' => 'mii',
                'str_description_slug' => 'mii',
                'bol_active' => 1,
            ),
            186 => 
            array (
                'id_model' => 2687,
                'id_manufacturer' => 326,
                'str_description' => 'ozza',
                'str_description_slug' => 'ozza',
                'bol_active' => 1,
            ),
            187 => 
            array (
                'id_model' => 2688,
                'id_manufacturer' => 326,
                'str_description' => 'panda',
                'str_description_slug' => 'panda',
                'bol_active' => 1,
            ),
            188 => 
            array (
                'id_model' => 2689,
                'id_manufacturer' => 326,
                'str_description' => 'ritmo',
                'str_description_slug' => 'ritmo',
                'bol_active' => 1,
            ),
            189 => 
            array (
                'id_model' => 2690,
                'id_manufacturer' => 326,
                'str_description' => 'ronda',
                'str_description_slug' => 'ronda',
                'bol_active' => 1,
            ),
            190 => 
            array (
                'id_model' => 2691,
                'id_manufacturer' => 326,
                'str_description' => 'terra',
                'str_description_slug' => 'terra',
                'bol_active' => 1,
            ),
            191 => 
            array (
                'id_model' => 2692,
                'id_manufacturer' => 326,
                'str_description' => 'trans',
                'str_description_slug' => 'trans',
                'bol_active' => 1,
            ),
            192 => 
            array (
                'id_model' => 2693,
                'id_manufacturer' => 327,
                'str_description' => 'f16',
                'str_description_slug' => 'f16',
                'bol_active' => 1,
            ),
            193 => 
            array (
                'id_model' => 2694,
                'id_manufacturer' => 327,
                'str_description' => 'fun buggy',
                'str_description_slug' => 'fun-buggy',
                'bol_active' => 1,
            ),
            194 => 
            array (
                'id_model' => 2695,
                'id_manufacturer' => 327,
                'str_description' => 'fun extrem',
                'str_description_slug' => 'fun-extrem',
                'bol_active' => 1,
            ),
            195 => 
            array (
                'id_model' => 2696,
                'id_manufacturer' => 327,
                'str_description' => 'fun family',
                'str_description_slug' => 'fun-family',
                'bol_active' => 1,
            ),
            196 => 
            array (
                'id_model' => 2697,
                'id_manufacturer' => 327,
                'str_description' => 'fun quad',
                'str_description_slug' => 'fun-quad',
                'bol_active' => 1,
            ),
            197 => 
            array (
                'id_model' => 2698,
                'id_manufacturer' => 327,
                'str_description' => 'fun runner',
                'str_description_slug' => 'fun-runner',
                'bol_active' => 1,
            ),
            198 => 
            array (
                'id_model' => 2699,
                'id_manufacturer' => 327,
                'str_description' => 'fun tech',
                'str_description_slug' => 'fun-tech',
                'bol_active' => 1,
            ),
            199 => 
            array (
                'id_model' => 2700,
                'id_manufacturer' => 328,
                'str_description' => 'series 1',
                'str_description_slug' => 'series-1',
                'bol_active' => 1,
            ),
            200 => 
            array (
                'id_model' => 2701,
                'id_manufacturer' => 329,
                'str_description' => 'go-kart',
                'str_description_slug' => 'go-kart',
                'bol_active' => 1,
            ),
            201 => 
            array (
                'id_model' => 2702,
                'id_manufacturer' => 330,
                'str_description' => 'dn-4',
                'str_description_slug' => 'dn-4',
                'bol_active' => 1,
            ),
            202 => 
            array (
                'id_model' => 2703,
                'id_manufacturer' => 331,
                'str_description' => 'sceo',
                'str_description_slug' => 'sceo',
                'bol_active' => 1,
            ),
            203 => 
            array (
                'id_model' => 2704,
                'id_manufacturer' => 332,
                'str_description' => '40',
                'str_description_slug' => '40',
                'bol_active' => 1,
            ),
            204 => 
            array (
                'id_model' => 2705,
                'id_manufacturer' => 332,
                'str_description' => '3000',
                'str_description_slug' => '3000',
                'bol_active' => 1,
            ),
            205 => 
            array (
                'id_model' => 2706,
                'id_manufacturer' => 332,
                'str_description' => 'formichetta',
                'str_description_slug' => 'formichetta',
                'bol_active' => 1,
            ),
            206 => 
            array (
                'id_model' => 2707,
                'id_manufacturer' => 333,
                'str_description' => '8',
                'str_description_slug' => '8',
                'bol_active' => 1,
            ),
            207 => 
            array (
                'id_model' => 2708,
                'id_manufacturer' => 333,
                'str_description' => '900',
                'str_description_slug' => '900',
                'bol_active' => 1,
            ),
            208 => 
            array (
                'id_model' => 2709,
                'id_manufacturer' => 333,
                'str_description' => '1000',
                'str_description_slug' => '1000',
                'bol_active' => 1,
            ),
            209 => 
            array (
                'id_model' => 2710,
                'id_manufacturer' => 333,
                'str_description' => '1200',
                'str_description_slug' => '1200',
                'bol_active' => 1,
            ),
            210 => 
            array (
                'id_model' => 2711,
                'id_manufacturer' => 333,
                'str_description' => '1300',
                'str_description_slug' => '1300',
                'bol_active' => 1,
            ),
            211 => 
            array (
                'id_model' => 2712,
                'id_manufacturer' => 333,
                'str_description' => '1500',
                'str_description_slug' => '1500',
                'bol_active' => 1,
            ),
            212 => 
            array (
                'id_model' => 2713,
                'id_manufacturer' => 333,
                'str_description' => '1501',
                'str_description_slug' => '1501',
                'bol_active' => 1,
            ),
            213 => 
            array (
                'id_model' => 2714,
                'id_manufacturer' => 333,
                'str_description' => 'ariane',
                'str_description_slug' => 'ariane',
                'bol_active' => 1,
            ),
            214 => 
            array (
                'id_model' => 2715,
                'id_manufacturer' => 333,
                'str_description' => 'aronde p60',
                'str_description_slug' => 'aronde-p60',
                'bol_active' => 1,
            ),
            215 => 
            array (
                'id_model' => 2716,
                'id_manufacturer' => 333,
                'str_description' => 'vedette',
                'str_description_slug' => 'vedette',
                'bol_active' => 1,
            ),
            216 => 
            array (
                'id_model' => 2717,
                'id_manufacturer' => 334,
                'str_description' => 'chamois',
                'str_description_slug' => 'chamois',
                'bol_active' => 1,
            ),
            217 => 
            array (
                'id_model' => 2718,
                'id_manufacturer' => 334,
                'str_description' => 'gazelle',
                'str_description_slug' => 'gazelle',
                'bol_active' => 1,
            ),
            218 => 
            array (
                'id_model' => 2719,
                'id_manufacturer' => 334,
                'str_description' => 'vogue',
                'str_description_slug' => 'vogue',
                'bol_active' => 1,
            ),
            219 => 
            array (
                'id_model' => 2720,
                'id_manufacturer' => 335,
                'str_description' => 'brado twin',
                'str_description_slug' => 'brado-twin',
                'bol_active' => 1,
            ),
            220 => 
            array (
                'id_model' => 2721,
                'id_manufacturer' => 335,
                'str_description' => 'kontra',
                'str_description_slug' => 'kontra',
                'bol_active' => 1,
            ),
            221 => 
            array (
                'id_model' => 2722,
                'id_manufacturer' => 336,
                'str_description' => '130',
                'str_description_slug' => '130',
                'bol_active' => 1,
            ),
            222 => 
            array (
                'id_model' => 2723,
                'id_manufacturer' => 336,
                'str_description' => '136',
                'str_description_slug' => '136',
                'bol_active' => 1,
            ),
            223 => 
            array (
                'id_model' => 2724,
                'id_manufacturer' => 336,
                'str_description' => 'citigo',
                'str_description_slug' => 'citigo',
                'bol_active' => 1,
            ),
            224 => 
            array (
                'id_model' => 2725,
                'id_manufacturer' => 336,
                'str_description' => 'forman',
                'str_description_slug' => 'forman',
                'bol_active' => 1,
            ),
            225 => 
            array (
                'id_model' => 2726,
                'id_manufacturer' => 336,
                'str_description' => 'praktik',
                'str_description_slug' => 'praktik',
                'bol_active' => 1,
            ),
            226 => 
            array (
                'id_model' => 2727,
                'id_manufacturer' => 336,
                'str_description' => 'rapid',
                'str_description_slug' => 'rapid',
                'bol_active' => 1,
            ),
            227 => 
            array (
                'id_model' => 2728,
                'id_manufacturer' => 336,
                'str_description' => 's100',
                'str_description_slug' => 's100',
                'bol_active' => 1,
            ),
            228 => 
            array (
                'id_model' => 2729,
                'id_manufacturer' => 336,
                'str_description' => 's110',
                'str_description_slug' => 's110',
                'bol_active' => 1,
            ),
            229 => 
            array (
                'id_model' => 2730,
                'id_manufacturer' => 336,
                'str_description' => 'yeti outdoor',
                'str_description_slug' => 'yeti-outdoor',
                'bol_active' => 1,
            ),
            230 => 
            array (
                'id_model' => 2731,
                'id_manufacturer' => 337,
                'str_description' => 'cabrio',
                'str_description_slug' => 'cabrio',
                'bol_active' => 1,
            ),
            231 => 
            array (
                'id_model' => 2732,
                'id_manufacturer' => 337,
                'str_description' => 'city-coupe',
                'str_description_slug' => 'city-coupe',
                'bol_active' => 1,
            ),
            232 => 
            array (
                'id_model' => 2733,
                'id_manufacturer' => 337,
                'str_description' => 'crossblade',
                'str_description_slug' => 'crossblade',
                'bol_active' => 1,
            ),
            233 => 
            array (
                'id_model' => 2734,
                'id_manufacturer' => 337,
                'str_description' => 'forfour',
                'str_description_slug' => 'forfour',
                'bol_active' => 1,
            ),
            234 => 
            array (
                'id_model' => 2735,
                'id_manufacturer' => 337,
                'str_description' => 'fortwo cabrio',
                'str_description_slug' => 'fortwo-cabrio',
                'bol_active' => 1,
            ),
            235 => 
            array (
                'id_model' => 2736,
                'id_manufacturer' => 337,
                'str_description' => 'fortwo coupe',
                'str_description_slug' => 'fortwo-coupe',
                'bol_active' => 1,
            ),
            236 => 
            array (
                'id_model' => 2737,
                'id_manufacturer' => 337,
                'str_description' => 'roadster',
                'str_description_slug' => 'roadster',
                'bol_active' => 1,
            ),
            237 => 
            array (
                'id_model' => 2738,
                'id_manufacturer' => 337,
                'str_description' => 'roadster-coupe',
                'str_description_slug' => 'roadster-coupe',
                'bol_active' => 1,
            ),
            238 => 
            array (
                'id_model' => 2739,
                'id_manufacturer' => 338,
                'str_description' => 'fun kart',
                'str_description_slug' => 'fun-kart',
                'bol_active' => 1,
            ),
            239 => 
            array (
                'id_model' => 2740,
                'id_manufacturer' => 339,
                'str_description' => 'super star',
                'str_description_slug' => 'super-star',
                'bol_active' => 1,
            ),
            240 => 
            array (
                'id_model' => 2741,
                'id_manufacturer' => 340,
                'str_description' => '5020',
                'str_description_slug' => '5020',
                'bol_active' => 1,
            ),
            241 => 
            array (
                'id_model' => 2742,
                'id_manufacturer' => 340,
                'str_description' => '5021',
                'str_description_slug' => '5021',
                'bol_active' => 1,
            ),
            242 => 
            array (
                'id_model' => 2743,
                'id_manufacturer' => 340,
                'str_description' => '5023',
                'str_description_slug' => '5023',
                'bol_active' => 1,
            ),
            243 => 
            array (
                'id_model' => 2744,
                'id_manufacturer' => 340,
                'str_description' => 'bus',
                'str_description_slug' => 'bus',
                'bol_active' => 1,
            ),
            244 => 
            array (
                'id_model' => 2745,
                'id_manufacturer' => 340,
                'str_description' => 'chasis cabina',
                'str_description_slug' => 'chasis-cabina',
                'bol_active' => 1,
            ),
            245 => 
            array (
                'id_model' => 2746,
                'id_manufacturer' => 340,
                'str_description' => 'furgon',
                'str_description_slug' => 'furgon',
                'bol_active' => 1,
            ),
            246 => 
            array (
                'id_model' => 2747,
                'id_manufacturer' => 340,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            247 => 
            array (
                'id_model' => 2748,
                'id_manufacturer' => 341,
                'str_description' => 'actyon',
                'str_description_slug' => 'actyon',
                'bol_active' => 1,
            ),
            248 => 
            array (
                'id_model' => 2749,
                'id_manufacturer' => 341,
                'str_description' => 'actyon sports',
                'str_description_slug' => 'actyon-sports',
                'bol_active' => 1,
            ),
            249 => 
            array (
                'id_model' => 2750,
                'id_manufacturer' => 341,
                'str_description' => 'chairman',
                'str_description_slug' => 'chairman',
                'bol_active' => 1,
            ),
            250 => 
            array (
                'id_model' => 2751,
                'id_manufacturer' => 341,
                'str_description' => 'family',
                'str_description_slug' => 'family',
                'bol_active' => 1,
            ),
            251 => 
            array (
                'id_model' => 2752,
                'id_manufacturer' => 341,
                'str_description' => 'istana',
                'str_description_slug' => 'istana',
                'bol_active' => 1,
            ),
            252 => 
            array (
                'id_model' => 2753,
                'id_manufacturer' => 341,
                'str_description' => 'kallista',
                'str_description_slug' => 'kallista',
                'bol_active' => 1,
            ),
            253 => 
            array (
                'id_model' => 2754,
                'id_manufacturer' => 341,
                'str_description' => 'korando',
                'str_description_slug' => 'korando',
                'bol_active' => 1,
            ),
            254 => 
            array (
                'id_model' => 2755,
                'id_manufacturer' => 341,
                'str_description' => 'korando kj',
                'str_description_slug' => 'korando-kj',
                'bol_active' => 1,
            ),
            255 => 
            array (
                'id_model' => 2756,
                'id_manufacturer' => 341,
                'str_description' => 'kyron',
                'str_description_slug' => 'kyron',
                'bol_active' => 1,
            ),
            256 => 
            array (
                'id_model' => 2757,
                'id_manufacturer' => 341,
                'str_description' => 'musso',
                'str_description_slug' => 'musso',
                'bol_active' => 1,
            ),
            257 => 
            array (
                'id_model' => 2758,
                'id_manufacturer' => 341,
                'str_description' => 'musso sport',
                'str_description_slug' => 'musso-sport',
                'bol_active' => 1,
            ),
            258 => 
            array (
                'id_model' => 2759,
                'id_manufacturer' => 341,
                'str_description' => 'rexton',
                'str_description_slug' => 'rexton',
                'bol_active' => 1,
            ),
            259 => 
            array (
                'id_model' => 2760,
                'id_manufacturer' => 341,
                'str_description' => 'rexton w',
                'str_description_slug' => 'rexton-w',
                'bol_active' => 1,
            ),
            260 => 
            array (
                'id_model' => 2761,
                'id_manufacturer' => 341,
                'str_description' => 'rodius',
                'str_description_slug' => 'rodius',
                'bol_active' => 1,
            ),
            261 => 
            array (
                'id_model' => 2762,
                'id_manufacturer' => 341,
                'str_description' => 'sports pick up',
                'str_description_slug' => 'sports-pick-up',
                'bol_active' => 1,
            ),
            262 => 
            array (
                'id_model' => 2763,
                'id_manufacturer' => 341,
                'str_description' => 'stavic',
                'str_description_slug' => 'stavic',
                'bol_active' => 1,
            ),
            263 => 
            array (
                'id_model' => 2764,
                'id_manufacturer' => 341,
                'str_description' => 'tivoli',
                'str_description_slug' => 'tivoli',
                'bol_active' => 1,
            ),
            264 => 
            array (
                'id_model' => 2765,
                'id_manufacturer' => 341,
                'str_description' => 'xlv',
                'str_description_slug' => 'xlv',
                'bol_active' => 1,
            ),
            265 => 
            array (
                'id_model' => 2766,
                'id_manufacturer' => 342,
                'str_description' => 'open',
                'str_description_slug' => 'open',
                'bol_active' => 1,
            ),
            266 => 
            array (
                'id_model' => 2767,
                'id_manufacturer' => 343,
                'str_description' => 'g',
                'str_description_slug' => 'g',
                'bol_active' => 1,
            ),
            267 => 
            array (
                'id_model' => 2768,
                'id_manufacturer' => 343,
                'str_description' => 'gdm',
                'str_description_slug' => 'gdm',
                'bol_active' => 1,
            ),
            268 => 
            array (
                'id_model' => 2769,
                'id_manufacturer' => 343,
                'str_description' => 'haflinger',
                'str_description_slug' => 'haflinger',
                'bol_active' => 1,
            ),
            269 => 
            array (
                'id_model' => 2770,
                'id_manufacturer' => 343,
                'str_description' => 'pinzgauer 710',
                'str_description_slug' => 'pinzgauer-710',
                'bol_active' => 1,
            ),
            270 => 
            array (
                'id_model' => 2771,
                'id_manufacturer' => 343,
                'str_description' => 'pinzgauer 712',
                'str_description_slug' => 'pinzgauer-712',
                'bol_active' => 1,
            ),
            271 => 
            array (
                'id_model' => 2772,
                'id_manufacturer' => 344,
                'str_description' => 'champion',
                'str_description_slug' => 'champion',
                'bol_active' => 1,
            ),
            272 => 
            array (
                'id_model' => 2773,
                'id_manufacturer' => 344,
                'str_description' => 'commander',
                'str_description_slug' => 'commander',
                'bol_active' => 1,
            ),
            273 => 
            array (
                'id_model' => 2774,
                'id_manufacturer' => 344,
                'str_description' => 'dictator',
                'str_description_slug' => 'dictator',
                'bol_active' => 1,
            ),
            274 => 
            array (
                'id_model' => 2775,
                'id_manufacturer' => 344,
                'str_description' => 'erskine',
                'str_description_slug' => 'erskine',
                'bol_active' => 1,
            ),
            275 => 
            array (
                'id_model' => 2776,
                'id_manufacturer' => 344,
                'str_description' => 'hawk',
                'str_description_slug' => 'hawk',
                'bol_active' => 1,
            ),
            276 => 
            array (
                'id_model' => 2777,
                'id_manufacturer' => 344,
                'str_description' => 'lark',
                'str_description_slug' => 'lark',
                'bol_active' => 1,
            ),
            277 => 
            array (
                'id_model' => 2778,
                'id_manufacturer' => 344,
                'str_description' => 'silver hawk',
                'str_description_slug' => 'silver-hawk',
                'bol_active' => 1,
            ),
            278 => 
            array (
                'id_model' => 2779,
                'id_manufacturer' => 345,
                'str_description' => '1300',
                'str_description_slug' => '1300',
                'bol_active' => 1,
            ),
            279 => 
            array (
                'id_model' => 2780,
                'id_manufacturer' => 345,
                'str_description' => '1600',
                'str_description_slug' => '1600',
                'bol_active' => 1,
            ),
            280 => 
            array (
                'id_model' => 2781,
                'id_manufacturer' => 345,
                'str_description' => '1800',
                'str_description_slug' => '1800',
                'bol_active' => 1,
            ),
            281 => 
            array (
                'id_model' => 2782,
                'id_manufacturer' => 345,
                'str_description' => 'brz',
                'str_description_slug' => 'brz',
                'bol_active' => 1,
            ),
            282 => 
            array (
                'id_model' => 2783,
                'id_manufacturer' => 345,
                'str_description' => 'delivery',
                'str_description_slug' => 'delivery',
                'bol_active' => 1,
            ),
            283 => 
            array (
                'id_model' => 2784,
                'id_manufacturer' => 345,
                'str_description' => 'domingo',
                'str_description_slug' => 'domingo',
                'bol_active' => 1,
            ),
            284 => 
            array (
                'id_model' => 2785,
                'id_manufacturer' => 345,
                'str_description' => 'e-10',
                'str_description_slug' => 'e-10',
                'bol_active' => 1,
            ),
            285 => 
            array (
                'id_model' => 2786,
                'id_manufacturer' => 345,
                'str_description' => 'e-12',
                'str_description_slug' => 'e-12',
                'bol_active' => 1,
            ),
            286 => 
            array (
                'id_model' => 2787,
                'id_manufacturer' => 345,
                'str_description' => 'forester',
                'str_description_slug' => 'forester',
                'bol_active' => 1,
            ),
            287 => 
            array (
                'id_model' => 2788,
                'id_manufacturer' => 345,
                'str_description' => 'impreza',
                'str_description_slug' => 'impreza',
                'bol_active' => 1,
            ),
            288 => 
            array (
                'id_model' => 2789,
                'id_manufacturer' => 345,
                'str_description' => 'justy',
                'str_description_slug' => 'justy',
                'bol_active' => 1,
            ),
            289 => 
            array (
                'id_model' => 2790,
                'id_manufacturer' => 345,
                'str_description' => 'legacy',
                'str_description_slug' => 'legacy',
                'bol_active' => 1,
            ),
            290 => 
            array (
                'id_model' => 2791,
                'id_manufacturer' => 345,
                'str_description' => 'leone',
                'str_description_slug' => 'leone',
                'bol_active' => 1,
            ),
            291 => 
            array (
                'id_model' => 2792,
                'id_manufacturer' => 345,
                'str_description' => 'levorg',
                'str_description_slug' => 'levorg',
                'bol_active' => 1,
            ),
            292 => 
            array (
                'id_model' => 2793,
                'id_manufacturer' => 345,
                'str_description' => 'libero',
                'str_description_slug' => 'libero',
                'bol_active' => 1,
            ),
            293 => 
            array (
                'id_model' => 2794,
                'id_manufacturer' => 345,
                'str_description' => 'm 80',
                'str_description_slug' => 'm-80',
                'bol_active' => 1,
            ),
            294 => 
            array (
                'id_model' => 2795,
                'id_manufacturer' => 345,
                'str_description' => 'mini jumbo',
                'str_description_slug' => 'mini-jumbo',
                'bol_active' => 1,
            ),
            295 => 
            array (
                'id_model' => 2796,
                'id_manufacturer' => 345,
                'str_description' => 'outback',
                'str_description_slug' => 'outback',
                'bol_active' => 1,
            ),
            296 => 
            array (
                'id_model' => 2797,
                'id_manufacturer' => 345,
                'str_description' => 'rex',
                'str_description_slug' => 'rex',
                'bol_active' => 1,
            ),
            297 => 
            array (
                'id_model' => 2798,
                'id_manufacturer' => 345,
                'str_description' => 'station wagon',
                'str_description_slug' => 'station-wagon',
                'bol_active' => 1,
            ),
            298 => 
            array (
                'id_model' => 2799,
                'id_manufacturer' => 345,
                'str_description' => 'svx',
                'str_description_slug' => 'svx',
                'bol_active' => 1,
            ),
            299 => 
            array (
                'id_model' => 2800,
                'id_manufacturer' => 345,
                'str_description' => 'trezia',
                'str_description_slug' => 'trezia',
                'bol_active' => 1,
            ),
            300 => 
            array (
                'id_model' => 2801,
                'id_manufacturer' => 345,
                'str_description' => 'tribeca',
                'str_description_slug' => 'tribeca',
                'bol_active' => 1,
            ),
            301 => 
            array (
                'id_model' => 2802,
                'id_manufacturer' => 345,
                'str_description' => 'truck',
                'str_description_slug' => 'truck',
                'bol_active' => 1,
            ),
            302 => 
            array (
                'id_model' => 2803,
                'id_manufacturer' => 345,
                'str_description' => 'vivio',
                'str_description_slug' => 'vivio',
                'bol_active' => 1,
            ),
            303 => 
            array (
                'id_model' => 2804,
                'id_manufacturer' => 345,
                'str_description' => 'wrx sti',
                'str_description_slug' => 'wrx-sti',
                'bol_active' => 1,
            ),
            304 => 
            array (
                'id_model' => 2805,
                'id_manufacturer' => 345,
                'str_description' => 'xt',
                'str_description_slug' => 'xt',
                'bol_active' => 1,
            ),
            305 => 
            array (
                'id_model' => 2806,
                'id_manufacturer' => 345,
                'str_description' => 'xv',
                'str_description_slug' => 'xv',
                'bol_active' => 1,
            ),
            306 => 
            array (
                'id_model' => 2807,
                'id_manufacturer' => 346,
                'str_description' => '424',
                'str_description_slug' => '424',
                'bol_active' => 1,
            ),
            307 => 
            array (
                'id_model' => 2808,
                'id_manufacturer' => 346,
                'str_description' => '1500',
                'str_description_slug' => '1500',
                'bol_active' => 1,
            ),
            308 => 
            array (
                'id_model' => 2809,
                'id_manufacturer' => 346,
                'str_description' => 'alpine',
                'str_description_slug' => 'alpine',
                'bol_active' => 1,
            ),
            309 => 
            array (
                'id_model' => 2810,
                'id_manufacturer' => 346,
                'str_description' => 'avenger',
                'str_description_slug' => 'avenger',
                'bol_active' => 1,
            ),
            310 => 
            array (
                'id_model' => 2811,
                'id_manufacturer' => 346,
                'str_description' => 'hunter',
                'str_description_slug' => 'hunter',
                'bol_active' => 1,
            ),
            311 => 
            array (
                'id_model' => 2812,
                'id_manufacturer' => 346,
                'str_description' => 'rapier',
                'str_description_slug' => 'rapier',
                'bol_active' => 1,
            ),
            312 => 
            array (
                'id_model' => 2813,
                'id_manufacturer' => 346,
                'str_description' => 'tiger',
                'str_description_slug' => 'tiger',
                'bol_active' => 1,
            ),
            313 => 
            array (
                'id_model' => 2814,
                'id_manufacturer' => 346,
                'str_description' => 'venezia',
                'str_description_slug' => 'venezia',
                'bol_active' => 1,
            ),
            314 => 
            array (
                'id_model' => 2815,
                'id_manufacturer' => 347,
                'str_description' => 'rri',
                'str_description_slug' => 'rri',
                'bol_active' => 1,
            ),
            315 => 
            array (
                'id_model' => 2816,
                'id_manufacturer' => 348,
                'str_description' => 'clemcar',
                'str_description_slug' => 'clemcar',
                'bol_active' => 1,
            ),
            316 => 
            array (
                'id_model' => 2817,
                'id_manufacturer' => 349,
                'str_description' => 'aerio',
                'str_description_slug' => 'aerio',
                'bol_active' => 1,
            ),
            317 => 
            array (
                'id_model' => 2818,
                'id_manufacturer' => 349,
                'str_description' => 'alto',
                'str_description_slug' => 'alto',
                'bol_active' => 1,
            ),
            318 => 
            array (
                'id_model' => 2819,
                'id_manufacturer' => 349,
                'str_description' => 'baleno',
                'str_description_slug' => 'baleno',
                'bol_active' => 1,
            ),
            319 => 
            array (
                'id_model' => 2820,
                'id_manufacturer' => 349,
                'str_description' => 'cappuccino',
                'str_description_slug' => 'cappuccino',
                'bol_active' => 1,
            ),
            320 => 
            array (
                'id_model' => 2821,
                'id_manufacturer' => 349,
                'str_description' => 'carry',
                'str_description_slug' => 'carry',
                'bol_active' => 1,
            ),
            321 => 
            array (
                'id_model' => 2822,
                'id_manufacturer' => 349,
                'str_description' => 'celerio',
                'str_description_slug' => 'celerio',
                'bol_active' => 1,
            ),
            322 => 
            array (
                'id_model' => 2823,
                'id_manufacturer' => 349,
                'str_description' => 'cervo',
                'str_description_slug' => 'cervo',
                'bol_active' => 1,
            ),
            323 => 
            array (
                'id_model' => 2824,
                'id_manufacturer' => 349,
                'str_description' => 'geo metro',
                'str_description_slug' => 'geo-metro',
                'bol_active' => 1,
            ),
            324 => 
            array (
                'id_model' => 2825,
                'id_manufacturer' => 349,
                'str_description' => 'grand nomade',
                'str_description_slug' => 'grand-nomade',
                'bol_active' => 1,
            ),
            325 => 
            array (
                'id_model' => 2826,
                'id_manufacturer' => 349,
                'str_description' => 'grand vitara',
                'str_description_slug' => 'grand-vitara',
                'bol_active' => 1,
            ),
            326 => 
            array (
                'id_model' => 2827,
                'id_manufacturer' => 349,
                'str_description' => 'ignis',
                'str_description_slug' => 'ignis',
                'bol_active' => 1,
            ),
            327 => 
            array (
                'id_model' => 2828,
                'id_manufacturer' => 349,
                'str_description' => 'jimny',
                'str_description_slug' => 'jimny',
                'bol_active' => 1,
            ),
            328 => 
            array (
                'id_model' => 2829,
                'id_manufacturer' => 349,
                'str_description' => 'kizashi',
                'str_description_slug' => 'kizashi',
                'bol_active' => 1,
            ),
            329 => 
            array (
                'id_model' => 2830,
                'id_manufacturer' => 349,
                'str_description' => 'l60',
                'str_description_slug' => 'l60',
                'bol_active' => 1,
            ),
            330 => 
            array (
                'id_model' => 2831,
                'id_manufacturer' => 349,
                'str_description' => 'liana',
                'str_description_slug' => 'liana',
                'bol_active' => 1,
            ),
            331 => 
            array (
                'id_model' => 2832,
                'id_manufacturer' => 349,
                'str_description' => 'lj 50',
                'str_description_slug' => 'lj-50',
                'bol_active' => 1,
            ),
            332 => 
            array (
                'id_model' => 2833,
                'id_manufacturer' => 349,
                'str_description' => 'lj 80',
                'str_description_slug' => 'lj-80',
                'bol_active' => 1,
            ),
            333 => 
            array (
                'id_model' => 2834,
                'id_manufacturer' => 349,
                'str_description' => 'maruti',
                'str_description_slug' => 'maruti',
                'bol_active' => 1,
            ),
            334 => 
            array (
                'id_model' => 2835,
                'id_manufacturer' => 349,
                'str_description' => 'samurai',
                'str_description_slug' => 'samurai',
                'bol_active' => 1,
            ),
            335 => 
            array (
                'id_model' => 2836,
                'id_manufacturer' => 349,
                'str_description' => 'santana',
                'str_description_slug' => 'santana',
                'bol_active' => 1,
            ),
            336 => 
            array (
                'id_model' => 2837,
                'id_manufacturer' => 349,
                'str_description' => 'siderick',
                'str_description_slug' => 'siderick',
                'bol_active' => 1,
            ),
            337 => 
            array (
                'id_model' => 2838,
                'id_manufacturer' => 349,
                'str_description' => 'sj410 samurai',
                'str_description_slug' => 'sj410-samurai',
                'bol_active' => 1,
            ),
            338 => 
            array (
                'id_model' => 2839,
                'id_manufacturer' => 349,
                'str_description' => 'sj413 samurai',
                'str_description_slug' => 'sj413-samurai',
                'bol_active' => 1,
            ),
            339 => 
            array (
                'id_model' => 2840,
                'id_manufacturer' => 349,
                'str_description' => 'splash',
                'str_description_slug' => 'splash',
                'bol_active' => 1,
            ),
            340 => 
            array (
                'id_model' => 2841,
                'id_manufacturer' => 349,
                'str_description' => 'super carry',
                'str_description_slug' => 'super-carry',
                'bol_active' => 1,
            ),
            341 => 
            array (
                'id_model' => 2842,
                'id_manufacturer' => 349,
                'str_description' => 'swift',
                'str_description_slug' => 'swift',
                'bol_active' => 1,
            ),
            342 => 
            array (
                'id_model' => 2843,
                'id_manufacturer' => 349,
                'str_description' => 'sx4',
                'str_description_slug' => 'sx4',
                'bol_active' => 1,
            ),
            343 => 
            array (
                'id_model' => 2844,
                'id_manufacturer' => 349,
                'str_description' => 'sx4 s-cross',
                'str_description_slug' => 'sx4-s-cross',
                'bol_active' => 1,
            ),
            344 => 
            array (
                'id_model' => 2845,
                'id_manufacturer' => 349,
                'str_description' => 'vitara',
                'str_description_slug' => 'vitara',
                'bol_active' => 1,
            ),
            345 => 
            array (
                'id_model' => 2846,
                'id_manufacturer' => 349,
            'str_description' => 'vitara (mod 95)',
            'str_description_slug' => 'vitara-(mod-95)',
                'bol_active' => 1,
            ),
            346 => 
            array (
                'id_model' => 2847,
                'id_manufacturer' => 349,
                'str_description' => 'vitara xaloc',
                'str_description_slug' => 'vitara-xaloc',
                'bol_active' => 1,
            ),
            347 => 
            array (
                'id_model' => 2848,
                'id_manufacturer' => 349,
                'str_description' => 'wagon r+',
                'str_description_slug' => 'wagon-r+',
                'bol_active' => 1,
            ),
            348 => 
            array (
                'id_model' => 2849,
                'id_manufacturer' => 349,
                'str_description' => 'x-90',
                'str_description_slug' => 'x-90',
                'bol_active' => 1,
            ),
            349 => 
            array (
                'id_model' => 2850,
                'id_manufacturer' => 349,
                'str_description' => 'xl7',
                'str_description_slug' => 'xl7',
                'bol_active' => 1,
            ),
            350 => 
            array (
                'id_model' => 2851,
                'id_manufacturer' => 350,
                'str_description' => '2500',
                'str_description_slug' => '2500',
                'bol_active' => 1,
            ),
            351 => 
            array (
                'id_model' => 2852,
                'id_manufacturer' => 350,
                'str_description' => 'samurai',
                'str_description_slug' => 'samurai',
                'bol_active' => 1,
            ),
            352 => 
            array (
                'id_model' => 2853,
                'id_manufacturer' => 350,
                'str_description' => 'super carry',
                'str_description_slug' => 'super-carry',
                'bol_active' => 1,
            ),
            353 => 
            array (
                'id_model' => 2854,
                'id_manufacturer' => 350,
                'str_description' => 'vitara',
                'str_description_slug' => 'vitara',
                'bol_active' => 1,
            ),
            354 => 
            array (
                'id_model' => 2855,
                'id_manufacturer' => 351,
                'str_description' => '150',
                'str_description_slug' => '150',
                'bol_active' => 1,
            ),
            355 => 
            array (
                'id_model' => 2856,
                'id_manufacturer' => 351,
                'str_description' => '180',
                'str_description_slug' => '180',
                'bol_active' => 1,
            ),
            356 => 
            array (
                'id_model' => 2857,
                'id_manufacturer' => 351,
                'str_description' => 'horizon',
                'str_description_slug' => 'horizon',
                'bol_active' => 1,
            ),
            357 => 
            array (
                'id_model' => 2858,
                'id_manufacturer' => 351,
                'str_description' => 'matra murena',
                'str_description_slug' => 'matra-murena',
                'bol_active' => 1,
            ),
            358 => 
            array (
                'id_model' => 2859,
                'id_manufacturer' => 351,
                'str_description' => 'samba',
                'str_description_slug' => 'samba',
                'bol_active' => 1,
            ),
            359 => 
            array (
                'id_model' => 2860,
                'id_manufacturer' => 351,
                'str_description' => 'solara',
                'str_description_slug' => 'solara',
                'bol_active' => 1,
            ),
            360 => 
            array (
                'id_model' => 2861,
                'id_manufacturer' => 351,
                'str_description' => 'sunbeam',
                'str_description_slug' => 'sunbeam',
                'bol_active' => 1,
            ),
            361 => 
            array (
                'id_model' => 2862,
                'id_manufacturer' => 351,
                'str_description' => 'tagora',
                'str_description_slug' => 'tagora',
                'bol_active' => 1,
            ),
            362 => 
            array (
                'id_model' => 2863,
                'id_manufacturer' => 352,
                'str_description' => '233',
                'str_description_slug' => '233',
                'bol_active' => 1,
            ),
            363 => 
            array (
                'id_model' => 2864,
                'id_manufacturer' => 353,
                'str_description' => 'bingo',
                'str_description_slug' => 'bingo',
                'bol_active' => 1,
            ),
            364 => 
            array (
                'id_model' => 2865,
                'id_manufacturer' => 353,
                'str_description' => 'c1d',
                'str_description_slug' => 'c1d',
                'bol_active' => 1,
            ),
            365 => 
            array (
                'id_model' => 2866,
                'id_manufacturer' => 353,
                'str_description' => 'c2d',
                'str_description_slug' => 'c2d',
                'bol_active' => 1,
            ),
            366 => 
            array (
                'id_model' => 2867,
                'id_manufacturer' => 353,
                'str_description' => 'c3d',
                'str_description_slug' => 'c3d',
                'bol_active' => 1,
            ),
            367 => 
            array (
                'id_model' => 2868,
                'id_manufacturer' => 353,
                'str_description' => 'm1d',
                'str_description_slug' => 'm1d',
                'bol_active' => 1,
            ),
            368 => 
            array (
                'id_model' => 2869,
                'id_manufacturer' => 354,
                'str_description' => 'ace',
                'str_description_slug' => 'ace',
                'bol_active' => 1,
            ),
            369 => 
            array (
                'id_model' => 2870,
                'id_manufacturer' => 354,
                'str_description' => 'aria',
                'str_description_slug' => 'aria',
                'bol_active' => 1,
            ),
            370 => 
            array (
                'id_model' => 2871,
                'id_manufacturer' => 354,
                'str_description' => 'grand safari',
                'str_description_slug' => 'grand-safari',
                'bol_active' => 1,
            ),
            371 => 
            array (
                'id_model' => 2872,
                'id_manufacturer' => 354,
                'str_description' => 'indica',
                'str_description_slug' => 'indica',
                'bol_active' => 1,
            ),
            372 => 
            array (
                'id_model' => 2873,
                'id_manufacturer' => 354,
                'str_description' => 'indigo marina',
                'str_description_slug' => 'indigo-marina',
                'bol_active' => 1,
            ),
            373 => 
            array (
                'id_model' => 2874,
                'id_manufacturer' => 354,
                'str_description' => 'safari',
                'str_description_slug' => 'safari',
                'bol_active' => 1,
            ),
            374 => 
            array (
                'id_model' => 2875,
                'id_manufacturer' => 354,
                'str_description' => 'safari ex',
                'str_description_slug' => 'safari-ex',
                'bol_active' => 1,
            ),
            375 => 
            array (
                'id_model' => 2876,
                'id_manufacturer' => 354,
                'str_description' => 'sumo',
                'str_description_slug' => 'sumo',
                'bol_active' => 1,
            ),
            376 => 
            array (
                'id_model' => 2877,
                'id_manufacturer' => 354,
                'str_description' => 'telco',
                'str_description_slug' => 'telco',
                'bol_active' => 1,
            ),
            377 => 
            array (
                'id_model' => 2878,
                'id_manufacturer' => 354,
                'str_description' => 'vista',
                'str_description_slug' => 'vista',
                'bol_active' => 1,
            ),
            378 => 
            array (
                'id_model' => 2879,
                'id_manufacturer' => 354,
                'str_description' => 'xenon',
                'str_description_slug' => 'xenon',
                'bol_active' => 1,
            ),
            379 => 
            array (
                'id_model' => 2880,
                'id_manufacturer' => 355,
                'str_description' => 'aristocrat',
                'str_description_slug' => 'aristocrat',
                'bol_active' => 1,
            ),
            380 => 
            array (
                'id_model' => 2881,
                'id_manufacturer' => 355,
                'str_description' => 'carrymaster',
                'str_description_slug' => 'carrymaster',
                'bol_active' => 1,
            ),
            381 => 
            array (
                'id_model' => 2882,
                'id_manufacturer' => 355,
                'str_description' => 'e-tram',
                'str_description_slug' => 'e-tram',
                'bol_active' => 1,
            ),
            382 => 
            array (
                'id_model' => 2883,
                'id_manufacturer' => 355,
                'str_description' => 'electruck',
                'str_description_slug' => 'electruck',
                'bol_active' => 1,
            ),
            383 => 
            array (
                'id_model' => 2884,
                'id_manufacturer' => 355,
                'str_description' => 'et 3000',
                'str_description_slug' => 'et-3000',
                'bol_active' => 1,
            ),
            384 => 
            array (
                'id_model' => 2885,
                'id_manufacturer' => 355,
                'str_description' => 'expeditor',
                'str_description_slug' => 'expeditor',
                'bol_active' => 1,
            ),
            385 => 
            array (
                'id_model' => 2886,
                'id_manufacturer' => 355,
                'str_description' => 'huskey',
                'str_description_slug' => 'huskey',
                'bol_active' => 1,
            ),
            386 => 
            array (
                'id_model' => 2887,
                'id_manufacturer' => 355,
                'str_description' => 'loadmaster',
                'str_description_slug' => 'loadmaster',
                'bol_active' => 1,
            ),
            387 => 
            array (
                'id_model' => 2888,
                'id_manufacturer' => 355,
                'str_description' => 'model e',
                'str_description_slug' => 'model-e',
                'bol_active' => 1,
            ),
            388 => 
            array (
                'id_model' => 2889,
                'id_manufacturer' => 355,
                'str_description' => 'mule',
                'str_description_slug' => 'mule',
                'bol_active' => 1,
            ),
            389 => 
            array (
                'id_model' => 2890,
                'id_manufacturer' => 355,
                'str_description' => 'pullmaster',
                'str_description_slug' => 'pullmaster',
                'bol_active' => 1,
            ),
            390 => 
            array (
                'id_model' => 2891,
                'id_manufacturer' => 355,
                'str_description' => 'roadmaster',
                'str_description_slug' => 'roadmaster',
                'bol_active' => 1,
            ),
            391 => 
            array (
                'id_model' => 2892,
                'id_manufacturer' => 355,
                'str_description' => 'steepsaver',
                'str_description_slug' => 'steepsaver',
                'bol_active' => 1,
            ),
            392 => 
            array (
                'id_model' => 2893,
                'id_manufacturer' => 355,
                'str_description' => 'stockchaser',
                'str_description_slug' => 'stockchaser',
                'bol_active' => 1,
            ),
            393 => 
            array (
                'id_model' => 2894,
                'id_manufacturer' => 355,
                'str_description' => 'taylortruck',
                'str_description_slug' => 'taylortruck',
                'bol_active' => 1,
            ),
            394 => 
            array (
                'id_model' => 2895,
                'id_manufacturer' => 355,
                'str_description' => 'tourmaster',
                'str_description_slug' => 'tourmaster',
                'bol_active' => 1,
            ),
            395 => 
            array (
                'id_model' => 2896,
                'id_manufacturer' => 355,
                'str_description' => 'towmaster',
                'str_description_slug' => 'towmaster',
                'bol_active' => 1,
            ),
            396 => 
            array (
                'id_model' => 2897,
                'id_manufacturer' => 356,
                'str_description' => 'zero',
                'str_description_slug' => 'zero',
                'bol_active' => 1,
            ),
            397 => 
            array (
                'id_model' => 2898,
                'id_manufacturer' => 357,
                'str_description' => 'bi-scot',
                'str_description_slug' => 'bi-scot',
                'bol_active' => 1,
            ),
            398 => 
            array (
                'id_model' => 2899,
                'id_manufacturer' => 358,
                'str_description' => '3md050',
                'str_description_slug' => '3md050',
                'bol_active' => 1,
            ),
            399 => 
            array (
                'id_model' => 2900,
                'id_manufacturer' => 358,
                'str_description' => '6hd050',
                'str_description_slug' => '6hd050',
                'bol_active' => 1,
            ),
            400 => 
            array (
                'id_model' => 2901,
                'id_manufacturer' => 358,
                'str_description' => '8hd050',
                'str_description_slug' => '8hd050',
                'bol_active' => 1,
            ),
            401 => 
            array (
                'id_model' => 2902,
                'id_manufacturer' => 358,
                'str_description' => 'sprint 500',
                'str_description_slug' => 'sprint-500',
                'bol_active' => 1,
            ),
            402 => 
            array (
                'id_model' => 2903,
                'id_manufacturer' => 358,
                'str_description' => 'tlx 325',
                'str_description_slug' => 'tlx-325',
                'bol_active' => 1,
            ),
            403 => 
            array (
                'id_model' => 2904,
                'id_manufacturer' => 359,
                'str_description' => 'model s',
                'str_description_slug' => 'model-s',
                'bol_active' => 1,
            ),
            404 => 
            array (
                'id_model' => 2905,
                'id_manufacturer' => 359,
                'str_description' => 'model x',
                'str_description_slug' => 'model-x',
                'bol_active' => 1,
            ),
            405 => 
            array (
                'id_model' => 2906,
                'id_manufacturer' => 359,
                'str_description' => 'roadster',
                'str_description_slug' => 'roadster',
                'bol_active' => 1,
            ),
            406 => 
            array (
                'id_model' => 2907,
                'id_manufacturer' => 360,
                'str_description' => 'also',
                'str_description_slug' => 'also',
                'bol_active' => 1,
            ),
            407 => 
            array (
                'id_model' => 2908,
                'id_manufacturer' => 360,
                'str_description' => 'aragon',
                'str_description_slug' => 'aragon',
                'bol_active' => 1,
            ),
            408 => 
            array (
                'id_model' => 2909,
                'id_manufacturer' => 360,
                'str_description' => 'barcelona',
                'str_description_slug' => 'barcelona',
                'bol_active' => 1,
            ),
            409 => 
            array (
                'id_model' => 2910,
                'id_manufacturer' => 360,
                'str_description' => 'canarias',
                'str_description_slug' => 'canarias',
                'bol_active' => 1,
            ),
            410 => 
            array (
                'id_model' => 2911,
                'id_manufacturer' => 360,
                'str_description' => 'cartagena',
                'str_description_slug' => 'cartagena',
                'bol_active' => 1,
            ),
            411 => 
            array (
                'id_model' => 2912,
                'id_manufacturer' => 360,
                'str_description' => 'castilla',
                'str_description_slug' => 'castilla',
                'bol_active' => 1,
            ),
            412 => 
            array (
                'id_model' => 2913,
                'id_manufacturer' => 360,
                'str_description' => 'emoke',
                'str_description_slug' => 'emoke',
                'bol_active' => 1,
            ),
            413 => 
            array (
                'id_model' => 2914,
                'id_manufacturer' => 360,
                'str_description' => 'formentera',
                'str_description_slug' => 'formentera',
                'bol_active' => 1,
            ),
            414 => 
            array (
                'id_model' => 2915,
                'id_manufacturer' => 360,
                'str_description' => 'galicia',
                'str_description_slug' => 'galicia',
                'bol_active' => 1,
            ),
            415 => 
            array (
                'id_model' => 2916,
                'id_manufacturer' => 360,
                'str_description' => 'janto',
                'str_description_slug' => 'janto',
                'bol_active' => 1,
            ),
            416 => 
            array (
                'id_model' => 2917,
                'id_manufacturer' => 360,
                'str_description' => 'mickey',
                'str_description_slug' => 'mickey',
                'bol_active' => 1,
            ),
            417 => 
            array (
                'id_model' => 2918,
                'id_manufacturer' => 360,
                'str_description' => 'rioja',
                'str_description_slug' => 'rioja',
                'bol_active' => 1,
            ),
            418 => 
            array (
                'id_model' => 2919,
                'id_manufacturer' => 360,
                'str_description' => 'valencia',
                'str_description_slug' => 'valencia',
                'bol_active' => 1,
            ),
            419 => 
            array (
                'id_model' => 2920,
                'id_manufacturer' => 360,
                'str_description' => 'vigilante',
                'str_description_slug' => 'vigilante',
                'bol_active' => 1,
            ),
            420 => 
            array (
                'id_model' => 2921,
                'id_manufacturer' => 361,
                'str_description' => 'city',
                'str_description_slug' => 'city',
                'bol_active' => 1,
            ),
            421 => 
            array (
                'id_model' => 2922,
                'id_manufacturer' => 362,
                'str_description' => 'e-merge',
                'str_description_slug' => 'e-merge',
                'bol_active' => 1,
            ),
            422 => 
            array (
                'id_model' => 2923,
                'id_manufacturer' => 363,
                'str_description' => 'tm 2',
                'str_description_slug' => 'tm-2',
                'bol_active' => 1,
            ),
            423 => 
            array (
                'id_model' => 2924,
                'id_manufacturer' => 363,
                'str_description' => 'tm 5',
                'str_description_slug' => 'tm-5',
                'bol_active' => 1,
            ),
            424 => 
            array (
                'id_model' => 2925,
                'id_manufacturer' => 363,
                'str_description' => 'tm27',
                'str_description_slug' => 'tm27',
                'bol_active' => 1,
            ),
            425 => 
            array (
                'id_model' => 2926,
                'id_manufacturer' => 363,
                'str_description' => 'tm47',
                'str_description_slug' => 'tm47',
                'bol_active' => 1,
            ),
            426 => 
            array (
                'id_model' => 2927,
                'id_manufacturer' => 363,
                'str_description' => 'tm57',
                'str_description_slug' => 'tm57',
                'bol_active' => 1,
            ),
            427 => 
            array (
                'id_model' => 2928,
                'id_manufacturer' => 364,
                'str_description' => 'ferruccio',
                'str_description_slug' => 'ferruccio',
                'bol_active' => 1,
            ),
            428 => 
            array (
                'id_model' => 2929,
                'id_manufacturer' => 364,
                'str_description' => 'ginevra',
                'str_description_slug' => 'ginevra',
                'bol_active' => 1,
            ),
            429 => 
            array (
                'id_model' => 2930,
                'id_manufacturer' => 365,
                'str_description' => '4runner',
                'str_description_slug' => '4runner',
                'bol_active' => 1,
            ),
            430 => 
            array (
                'id_model' => 2931,
                'id_manufacturer' => 365,
                'str_description' => '1000',
                'str_description_slug' => '1000',
                'bol_active' => 1,
            ),
            431 => 
            array (
                'id_model' => 2932,
                'id_manufacturer' => 365,
                'str_description' => 'alphard',
                'str_description_slug' => 'alphard',
                'bol_active' => 1,
            ),
            432 => 
            array (
                'id_model' => 2933,
                'id_manufacturer' => 365,
                'str_description' => 'altezza',
                'str_description_slug' => 'altezza',
                'bol_active' => 1,
            ),
            433 => 
            array (
                'id_model' => 2934,
                'id_manufacturer' => 365,
                'str_description' => 'avalon',
                'str_description_slug' => 'avalon',
                'bol_active' => 1,
            ),
            434 => 
            array (
                'id_model' => 2935,
                'id_manufacturer' => 365,
                'str_description' => 'c-hr',
                'str_description_slug' => 'c-hr',
                'bol_active' => 1,
            ),
            435 => 
            array (
                'id_model' => 2936,
                'id_manufacturer' => 365,
                'str_description' => 'camry',
                'str_description_slug' => 'camry',
                'bol_active' => 1,
            ),
            436 => 
            array (
                'id_model' => 2937,
                'id_manufacturer' => 365,
                'str_description' => 'carina e',
                'str_description_slug' => 'carina-e',
                'bol_active' => 1,
            ),
            437 => 
            array (
                'id_model' => 2938,
                'id_manufacturer' => 365,
                'str_description' => 'carina ii',
                'str_description_slug' => 'carina-ii',
                'bol_active' => 1,
            ),
            438 => 
            array (
                'id_model' => 2939,
                'id_manufacturer' => 365,
                'str_description' => 'corona',
                'str_description_slug' => 'corona',
                'bol_active' => 1,
            ),
            439 => 
            array (
                'id_model' => 2940,
                'id_manufacturer' => 365,
                'str_description' => 'cressida',
                'str_description_slug' => 'cressida',
                'bol_active' => 1,
            ),
            440 => 
            array (
                'id_model' => 2941,
                'id_manufacturer' => 365,
                'str_description' => 'crown',
                'str_description_slug' => 'crown',
                'bol_active' => 1,
            ),
            441 => 
            array (
                'id_model' => 2942,
                'id_manufacturer' => 365,
                'str_description' => 'dyna',
                'str_description_slug' => 'dyna',
                'bol_active' => 1,
            ),
            442 => 
            array (
                'id_model' => 2943,
                'id_manufacturer' => 365,
                'str_description' => 'dyna 100',
                'str_description_slug' => 'dyna-100',
                'bol_active' => 1,
            ),
            443 => 
            array (
                'id_model' => 2944,
                'id_manufacturer' => 365,
                'str_description' => 'dyna 150',
                'str_description_slug' => 'dyna-150',
                'bol_active' => 1,
            ),
            444 => 
            array (
                'id_model' => 2945,
                'id_manufacturer' => 365,
                'str_description' => 'dyna 200',
                'str_description_slug' => 'dyna-200',
                'bol_active' => 1,
            ),
            445 => 
            array (
                'id_model' => 2946,
                'id_manufacturer' => 365,
                'str_description' => 'echo',
                'str_description_slug' => 'echo',
                'bol_active' => 1,
            ),
            446 => 
            array (
                'id_model' => 2947,
                'id_manufacturer' => 365,
                'str_description' => 'estima',
                'str_description_slug' => 'estima',
                'bol_active' => 1,
            ),
            447 => 
            array (
                'id_model' => 2948,
                'id_manufacturer' => 365,
                'str_description' => 'f',
                'str_description_slug' => 'f',
                'bol_active' => 1,
            ),
            448 => 
            array (
                'id_model' => 2949,
                'id_manufacturer' => 365,
                'str_description' => 'fj cruiser',
                'str_description_slug' => 'fj-cruiser',
                'bol_active' => 1,
            ),
            449 => 
            array (
                'id_model' => 2950,
                'id_manufacturer' => 365,
                'str_description' => 'fortuner',
                'str_description_slug' => 'fortuner',
                'bol_active' => 1,
            ),
            450 => 
            array (
                'id_model' => 2951,
                'id_manufacturer' => 365,
                'str_description' => 'granvia',
                'str_description_slug' => 'granvia',
                'bol_active' => 1,
            ),
            451 => 
            array (
                'id_model' => 2952,
                'id_manufacturer' => 365,
                'str_description' => 'gt86',
                'str_description_slug' => 'gt86',
                'bol_active' => 1,
            ),
            452 => 
            array (
                'id_model' => 2953,
                'id_manufacturer' => 365,
                'str_description' => 'hi-ace',
                'str_description_slug' => 'hi-ace',
                'bol_active' => 1,
            ),
            453 => 
            array (
                'id_model' => 2954,
                'id_manufacturer' => 365,
                'str_description' => 'highlander',
                'str_description_slug' => 'highlander',
                'bol_active' => 1,
            ),
            454 => 
            array (
                'id_model' => 2955,
                'id_manufacturer' => 365,
                'str_description' => 'hilux',
                'str_description_slug' => 'hilux',
                'bol_active' => 1,
            ),
            455 => 
            array (
                'id_model' => 2956,
                'id_manufacturer' => 365,
                'str_description' => 'hilux surf',
                'str_description_slug' => 'hilux-surf',
                'bol_active' => 1,
            ),
            456 => 
            array (
                'id_model' => 2957,
                'id_manufacturer' => 365,
                'str_description' => 'iq',
                'str_description_slug' => 'iq',
                'bol_active' => 1,
            ),
            457 => 
            array (
                'id_model' => 2958,
                'id_manufacturer' => 365,
                'str_description' => 'land cruiser 200',
                'str_description_slug' => 'land-cruiser-200',
                'bol_active' => 1,
            ),
            458 => 
            array (
                'id_model' => 2959,
                'id_manufacturer' => 365,
                'str_description' => 'land cruiser large',
                'str_description_slug' => 'land-cruiser-large',
                'bol_active' => 1,
            ),
            459 => 
            array (
                'id_model' => 2960,
                'id_manufacturer' => 365,
                'str_description' => 'lite ace',
                'str_description_slug' => 'lite-ace',
                'bol_active' => 1,
            ),
            460 => 
            array (
                'id_model' => 2961,
                'id_manufacturer' => 365,
                'str_description' => 'lucida',
                'str_description_slug' => 'lucida',
                'bol_active' => 1,
            ),
            461 => 
            array (
                'id_model' => 2962,
                'id_manufacturer' => 365,
                'str_description' => 'matrix',
                'str_description_slug' => 'matrix',
                'bol_active' => 1,
            ),
            462 => 
            array (
                'id_model' => 2963,
                'id_manufacturer' => 365,
                'str_description' => 'mega cruiser',
                'str_description_slug' => 'mega-cruiser',
                'bol_active' => 1,
            ),
            463 => 
            array (
                'id_model' => 2964,
                'id_manufacturer' => 365,
                'str_description' => 'model f',
                'str_description_slug' => 'model-f',
                'bol_active' => 1,
            ),
            464 => 
            array (
                'id_model' => 2965,
                'id_manufacturer' => 365,
                'str_description' => 'mr2',
                'str_description_slug' => 'mr2',
                'bol_active' => 1,
            ),
            465 => 
            array (
                'id_model' => 2966,
                'id_manufacturer' => 365,
                'str_description' => 'paseo',
                'str_description_slug' => 'paseo',
                'bol_active' => 1,
            ),
            466 => 
            array (
                'id_model' => 2967,
                'id_manufacturer' => 365,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            467 => 
            array (
                'id_model' => 2968,
                'id_manufacturer' => 365,
                'str_description' => 'picnic',
                'str_description_slug' => 'picnic',
                'bol_active' => 1,
            ),
            468 => 
            array (
                'id_model' => 2969,
                'id_manufacturer' => 365,
                'str_description' => 'previa',
                'str_description_slug' => 'previa',
                'bol_active' => 1,
            ),
            469 => 
            array (
                'id_model' => 2970,
                'id_manufacturer' => 365,
                'str_description' => 'prius+',
                'str_description_slug' => 'prius+',
                'bol_active' => 1,
            ),
            470 => 
            array (
                'id_model' => 2971,
                'id_manufacturer' => 365,
                'str_description' => 'proace',
                'str_description_slug' => 'proace',
                'bol_active' => 1,
            ),
            471 => 
            array (
                'id_model' => 2972,
                'id_manufacturer' => 365,
                'str_description' => 'proace verso',
                'str_description_slug' => 'proace-verso',
                'bol_active' => 1,
            ),
            472 => 
            array (
                'id_model' => 2973,
                'id_manufacturer' => 365,
                'str_description' => 'r 2g',
                'str_description_slug' => 'r-2g',
                'bol_active' => 1,
            ),
            473 => 
            array (
                'id_model' => 2974,
                'id_manufacturer' => 365,
                'str_description' => 'samurai',
                'str_description_slug' => 'samurai',
                'bol_active' => 1,
            ),
            474 => 
            array (
                'id_model' => 2975,
                'id_manufacturer' => 365,
                'str_description' => 'scion',
                'str_description_slug' => 'scion',
                'bol_active' => 1,
            ),
            475 => 
            array (
                'id_model' => 2976,
                'id_manufacturer' => 365,
                'str_description' => 'sequoia',
                'str_description_slug' => 'sequoia',
                'bol_active' => 1,
            ),
            476 => 
            array (
                'id_model' => 2977,
                'id_manufacturer' => 365,
                'str_description' => 'sera',
                'str_description_slug' => 'sera',
                'bol_active' => 1,
            ),
            477 => 
            array (
                'id_model' => 2978,
                'id_manufacturer' => 365,
                'str_description' => 'sienna',
                'str_description_slug' => 'sienna',
                'bol_active' => 1,
            ),
            478 => 
            array (
                'id_model' => 2979,
                'id_manufacturer' => 365,
                'str_description' => 'starlet',
                'str_description_slug' => 'starlet',
                'bol_active' => 1,
            ),
            479 => 
            array (
                'id_model' => 2980,
                'id_manufacturer' => 365,
                'str_description' => 'supra',
                'str_description_slug' => 'supra',
                'bol_active' => 1,
            ),
            480 => 
            array (
                'id_model' => 2981,
                'id_manufacturer' => 365,
                'str_description' => 'sw4',
                'str_description_slug' => 'sw4',
                'bol_active' => 1,
            ),
            481 => 
            array (
                'id_model' => 2982,
                'id_manufacturer' => 365,
                'str_description' => 't 100',
                'str_description_slug' => 't-100',
                'bol_active' => 1,
            ),
            482 => 
            array (
                'id_model' => 2983,
                'id_manufacturer' => 365,
                'str_description' => 'tacoma',
                'str_description_slug' => 'tacoma',
                'bol_active' => 1,
            ),
            483 => 
            array (
                'id_model' => 2984,
                'id_manufacturer' => 365,
                'str_description' => 'tercel',
                'str_description_slug' => 'tercel',
                'bol_active' => 1,
            ),
            484 => 
            array (
                'id_model' => 2985,
                'id_manufacturer' => 365,
                'str_description' => 'town ace',
                'str_description_slug' => 'town-ace',
                'bol_active' => 1,
            ),
            485 => 
            array (
                'id_model' => 2986,
                'id_manufacturer' => 365,
                'str_description' => 'trueno',
                'str_description_slug' => 'trueno',
                'bol_active' => 1,
            ),
            486 => 
            array (
                'id_model' => 2987,
                'id_manufacturer' => 365,
                'str_description' => 'tundra',
                'str_description_slug' => 'tundra',
                'bol_active' => 1,
            ),
            487 => 
            array (
                'id_model' => 2988,
                'id_manufacturer' => 365,
                'str_description' => 'urban cruiser',
                'str_description_slug' => 'urban-cruiser',
                'bol_active' => 1,
            ),
            488 => 
            array (
                'id_model' => 2989,
                'id_manufacturer' => 365,
                'str_description' => 'venza',
                'str_description_slug' => 'venza',
                'bol_active' => 1,
            ),
            489 => 
            array (
                'id_model' => 2990,
                'id_manufacturer' => 365,
                'str_description' => 'verso s',
                'str_description_slug' => 'verso-s',
                'bol_active' => 1,
            ),
            490 => 
            array (
                'id_model' => 2991,
                'id_manufacturer' => 366,
                'str_description' => 'berlina',
                'str_description_slug' => 'berlina',
                'bol_active' => 1,
            ),
            491 => 
            array (
                'id_model' => 2992,
                'id_manufacturer' => 366,
                'str_description' => 'familiar',
                'str_description_slug' => 'familiar',
                'bol_active' => 1,
            ),
            492 => 
            array (
                'id_model' => 2993,
                'id_manufacturer' => 366,
                'str_description' => 'p',
                'str_description_slug' => 'p',
                'bol_active' => 1,
            ),
            493 => 
            array (
                'id_model' => 2994,
                'id_manufacturer' => 366,
                'str_description' => 'tramp',
                'str_description_slug' => 'tramp',
                'bol_active' => 1,
            ),
            494 => 
            array (
                'id_model' => 2995,
                'id_manufacturer' => 367,
                'str_description' => 'tramontana',
                'str_description_slug' => 'tramontana',
                'bol_active' => 1,
            ),
            495 => 
            array (
                'id_model' => 2996,
                'id_manufacturer' => 368,
                'str_description' => 'transport 2p',
                'str_description_slug' => 'transport-2p',
                'bol_active' => 1,
            ),
            496 => 
            array (
                'id_model' => 2997,
                'id_manufacturer' => 368,
                'str_description' => 'transport 4p',
                'str_description_slug' => 'transport-4p',
                'bol_active' => 1,
            ),
            497 => 
            array (
                'id_model' => 2998,
                'id_manufacturer' => 369,
                'str_description' => 'scooter car',
                'str_description_slug' => 'scooter-car',
                'bol_active' => 1,
            ),
            498 => 
            array (
                'id_model' => 2999,
                'id_manufacturer' => 370,
                'str_description' => '2.5 pi',
                'str_description_slug' => '2.5-pi',
                'bol_active' => 1,
            ),
            499 => 
            array (
                'id_model' => 3000,
                'id_manufacturer' => 370,
                'str_description' => '1800',
                'str_description_slug' => '1800',
                'bol_active' => 1,
            ),
        ));
        \DB::table('automobile_models')->insert(array (
            0 => 
            array (
                'id_model' => 3001,
                'id_manufacturer' => 370,
                'str_description' => '2000',
                'str_description_slug' => '2000',
                'bol_active' => 1,
            ),
            1 => 
            array (
                'id_model' => 3002,
                'id_manufacturer' => 370,
                'str_description' => 'acclaim',
                'str_description_slug' => 'acclaim',
                'bol_active' => 1,
            ),
            2 => 
            array (
                'id_model' => 3003,
                'id_manufacturer' => 370,
                'str_description' => 'courier',
                'str_description_slug' => 'courier',
                'bol_active' => 1,
            ),
            3 => 
            array (
                'id_model' => 3004,
                'id_manufacturer' => 370,
                'str_description' => 'gentry',
                'str_description_slug' => 'gentry',
                'bol_active' => 1,
            ),
            4 => 
            array (
                'id_model' => 3005,
                'id_manufacturer' => 370,
                'str_description' => 'gt 6',
                'str_description_slug' => 'gt-6',
                'bol_active' => 1,
            ),
            5 => 
            array (
                'id_model' => 3006,
                'id_manufacturer' => 370,
                'str_description' => 'herald',
                'str_description_slug' => 'herald',
                'bol_active' => 1,
            ),
            6 => 
            array (
                'id_model' => 3007,
                'id_manufacturer' => 370,
                'str_description' => 'spitfire',
                'str_description_slug' => 'spitfire',
                'bol_active' => 1,
            ),
            7 => 
            array (
                'id_model' => 3008,
                'id_manufacturer' => 370,
                'str_description' => 'stag',
                'str_description_slug' => 'stag',
                'bol_active' => 1,
            ),
            8 => 
            array (
                'id_model' => 3009,
                'id_manufacturer' => 370,
                'str_description' => 'tr-3',
                'str_description_slug' => 'tr-3',
                'bol_active' => 1,
            ),
            9 => 
            array (
                'id_model' => 3010,
                'id_manufacturer' => 370,
                'str_description' => 'tr-4',
                'str_description_slug' => 'tr-4',
                'bol_active' => 1,
            ),
            10 => 
            array (
                'id_model' => 3011,
                'id_manufacturer' => 370,
                'str_description' => 'tr-5',
                'str_description_slug' => 'tr-5',
                'bol_active' => 1,
            ),
            11 => 
            array (
                'id_model' => 3012,
                'id_manufacturer' => 370,
                'str_description' => 'tr-6',
                'str_description_slug' => 'tr-6',
                'bol_active' => 1,
            ),
            12 => 
            array (
                'id_model' => 3013,
                'id_manufacturer' => 370,
                'str_description' => 'tr-7',
                'str_description_slug' => 'tr-7',
                'bol_active' => 1,
            ),
            13 => 
            array (
                'id_model' => 3014,
                'id_manufacturer' => 370,
                'str_description' => 'vitesse',
                'str_description_slug' => 'vitesse',
                'bol_active' => 1,
            ),
            14 => 
            array (
                'id_model' => 3015,
                'id_manufacturer' => 371,
                'str_description' => 'arq',
                'str_description_slug' => 'arq',
                'bol_active' => 1,
            ),
            15 => 
            array (
                'id_model' => 3016,
                'id_manufacturer' => 372,
                'str_description' => 'v4',
                'str_description_slug' => 'v4',
                'bol_active' => 1,
            ),
            16 => 
            array (
                'id_model' => 3017,
                'id_manufacturer' => 373,
                'str_description' => '280',
                'str_description_slug' => '280',
                'bol_active' => 1,
            ),
            17 => 
            array (
                'id_model' => 3018,
                'id_manufacturer' => 373,
                'str_description' => '290',
                'str_description_slug' => '290',
                'bol_active' => 1,
            ),
            18 => 
            array (
                'id_model' => 3019,
                'id_manufacturer' => 373,
                'str_description' => '350',
                'str_description_slug' => '350',
                'bol_active' => 1,
            ),
            19 => 
            array (
                'id_model' => 3020,
                'id_manufacturer' => 373,
                'str_description' => 'chimaera',
                'str_description_slug' => 'chimaera',
                'bol_active' => 1,
            ),
            20 => 
            array (
                'id_model' => 3021,
                'id_manufacturer' => 373,
                'str_description' => 'griffith',
                'str_description_slug' => 'griffith',
                'bol_active' => 1,
            ),
            21 => 
            array (
                'id_model' => 3022,
                'id_manufacturer' => 373,
                'str_description' => 'm-series',
                'str_description_slug' => 'm-series',
                'bol_active' => 1,
            ),
            22 => 
            array (
                'id_model' => 3023,
                'id_manufacturer' => 373,
                'str_description' => 's-series',
                'str_description_slug' => 's-series',
                'bol_active' => 1,
            ),
            23 => 
            array (
                'id_model' => 3024,
                'id_manufacturer' => 373,
                'str_description' => 'tuscan',
                'str_description_slug' => 'tuscan',
                'bol_active' => 1,
            ),
            24 => 
            array (
                'id_model' => 3025,
                'id_manufacturer' => 374,
                'str_description' => 'active',
                'str_description_slug' => 'active',
                'bol_active' => 1,
            ),
            25 => 
            array (
                'id_model' => 3026,
                'id_manufacturer' => 375,
                'str_description' => 'explorer',
                'str_description_slug' => 'explorer',
                'bol_active' => 1,
            ),
            26 => 
            array (
                'id_model' => 3027,
                'id_manufacturer' => 375,
                'str_description' => 'marathon',
                'str_description_slug' => 'marathon',
                'bol_active' => 1,
            ),
            27 => 
            array (
                'id_model' => 3028,
                'id_manufacturer' => 375,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            28 => 
            array (
                'id_model' => 3029,
                'id_manufacturer' => 376,
                'str_description' => 'gtr',
                'str_description_slug' => 'gtr',
                'bol_active' => 1,
            ),
            29 => 
            array (
                'id_model' => 3030,
                'id_manufacturer' => 377,
                'str_description' => '100',
                'str_description_slug' => '100',
                'bol_active' => 1,
            ),
            30 => 
            array (
                'id_model' => 3031,
                'id_manufacturer' => 377,
                'str_description' => '121',
                'str_description_slug' => '121',
                'bol_active' => 1,
            ),
            31 => 
            array (
                'id_model' => 3032,
                'id_manufacturer' => 377,
                'str_description' => 'alter',
                'str_description_slug' => 'alter',
                'bol_active' => 1,
            ),
            32 => 
            array (
                'id_model' => 3033,
                'id_manufacturer' => 377,
                'str_description' => 'jabato',
                'str_description_slug' => 'jabato',
                'bol_active' => 1,
            ),
            33 => 
            array (
                'id_model' => 3034,
                'id_manufacturer' => 378,
                'str_description' => 'ardilla',
                'str_description_slug' => 'ardilla',
                'bol_active' => 1,
            ),
            34 => 
            array (
                'id_model' => 3035,
                'id_manufacturer' => 378,
                'str_description' => 'gacela',
                'str_description_slug' => 'gacela',
                'bol_active' => 1,
            ),
            35 => 
            array (
                'id_model' => 3036,
                'id_manufacturer' => 379,
                'str_description' => 'arena',
                'str_description_slug' => 'arena',
                'bol_active' => 1,
            ),
            36 => 
            array (
                'id_model' => 3037,
                'id_manufacturer' => 379,
                'str_description' => 'astra',
                'str_description_slug' => 'astra',
                'bol_active' => 1,
            ),
            37 => 
            array (
                'id_model' => 3038,
                'id_manufacturer' => 379,
                'str_description' => 'calibra',
                'str_description_slug' => 'calibra',
                'bol_active' => 1,
            ),
            38 => 
            array (
                'id_model' => 3039,
                'id_manufacturer' => 379,
                'str_description' => 'carlton',
                'str_description_slug' => 'carlton',
                'bol_active' => 1,
            ),
            39 => 
            array (
                'id_model' => 3040,
                'id_manufacturer' => 379,
                'str_description' => 'cavalier',
                'str_description_slug' => 'cavalier',
                'bol_active' => 1,
            ),
            40 => 
            array (
                'id_model' => 3041,
                'id_manufacturer' => 379,
                'str_description' => 'corsa',
                'str_description_slug' => 'corsa',
                'bol_active' => 1,
            ),
            41 => 
            array (
                'id_model' => 3042,
                'id_manufacturer' => 379,
                'str_description' => 'cresta pb',
                'str_description_slug' => 'cresta-pb',
                'bol_active' => 1,
            ),
            42 => 
            array (
                'id_model' => 3043,
                'id_manufacturer' => 379,
                'str_description' => 'dx',
                'str_description_slug' => 'dx',
                'bol_active' => 1,
            ),
            43 => 
            array (
                'id_model' => 3044,
                'id_manufacturer' => 379,
                'str_description' => 'firenza',
                'str_description_slug' => 'firenza',
                'bol_active' => 1,
            ),
            44 => 
            array (
                'id_model' => 3045,
                'id_manufacturer' => 379,
                'str_description' => 'insignia',
                'str_description_slug' => 'insignia',
                'bol_active' => 1,
            ),
            45 => 
            array (
                'id_model' => 3046,
                'id_manufacturer' => 379,
                'str_description' => 'magnum',
                'str_description_slug' => 'magnum',
                'bol_active' => 1,
            ),
            46 => 
            array (
                'id_model' => 3047,
                'id_manufacturer' => 379,
                'str_description' => 'meriva',
                'str_description_slug' => 'meriva',
                'bol_active' => 1,
            ),
            47 => 
            array (
                'id_model' => 3048,
                'id_manufacturer' => 379,
                'str_description' => 'nova',
                'str_description_slug' => 'nova',
                'bol_active' => 1,
            ),
            48 => 
            array (
                'id_model' => 3049,
                'id_manufacturer' => 379,
                'str_description' => 'sports',
                'str_description_slug' => 'sports',
                'bol_active' => 1,
            ),
            49 => 
            array (
                'id_model' => 3050,
                'id_manufacturer' => 379,
                'str_description' => 'velox',
                'str_description_slug' => 'velox',
                'bol_active' => 1,
            ),
            50 => 
            array (
                'id_model' => 3051,
                'id_manufacturer' => 379,
                'str_description' => 'viva',
                'str_description_slug' => 'viva',
                'bol_active' => 1,
            ),
            51 => 
            array (
                'id_model' => 3052,
                'id_manufacturer' => 379,
                'str_description' => 'vivaro',
                'str_description_slug' => 'vivaro',
                'bol_active' => 1,
            ),
            52 => 
            array (
                'id_model' => 3053,
                'id_manufacturer' => 379,
                'str_description' => 'vxr8',
                'str_description_slug' => 'vxr8',
                'bol_active' => 1,
            ),
            53 => 
            array (
                'id_model' => 3054,
                'id_manufacturer' => 379,
                'str_description' => 'wyvern',
                'str_description_slug' => 'wyvern',
                'bol_active' => 1,
            ),
            54 => 
            array (
                'id_model' => 3055,
                'id_manufacturer' => 379,
                'str_description' => 'zafira',
                'str_description_slug' => 'zafira',
                'bol_active' => 1,
            ),
            55 => 
            array (
                'id_model' => 3056,
                'id_manufacturer' => 380,
                'str_description' => '214',
                'str_description_slug' => '214',
                'bol_active' => 1,
            ),
            56 => 
            array (
                'id_model' => 3057,
                'id_manufacturer' => 380,
                'str_description' => '215',
                'str_description_slug' => '215',
                'bol_active' => 1,
            ),
            57 => 
            array (
                'id_model' => 3058,
                'id_manufacturer' => 380,
                'str_description' => 'niva',
                'str_description_slug' => 'niva',
                'bol_active' => 1,
            ),
            58 => 
            array (
                'id_model' => 3059,
                'id_manufacturer' => 380,
                'str_description' => 'stawra',
                'str_description_slug' => 'stawra',
                'bol_active' => 1,
            ),
            59 => 
            array (
                'id_model' => 3060,
                'id_manufacturer' => 381,
                'str_description' => 'leonard',
                'str_description_slug' => 'leonard',
                'bol_active' => 1,
            ),
            60 => 
            array (
                'id_model' => 3061,
                'id_manufacturer' => 382,
                'str_description' => '260',
                'str_description_slug' => '260',
                'bol_active' => 1,
            ),
            61 => 
            array (
                'id_model' => 3062,
                'id_manufacturer' => 383,
                'str_description' => 'quovis',
                'str_description_slug' => 'quovis',
                'bol_active' => 1,
            ),
            62 => 
            array (
                'id_model' => 3063,
                'id_manufacturer' => 383,
                'str_description' => 'vit',
                'str_description_slug' => 'vit',
                'bol_active' => 1,
            ),
            63 => 
            array (
                'id_model' => 3064,
                'id_manufacturer' => 384,
                'str_description' => 'jeep',
                'str_description_slug' => 'jeep',
                'bol_active' => 1,
            ),
            64 => 
            array (
                'id_model' => 3065,
                'id_manufacturer' => 385,
                'str_description' => 'gts',
                'str_description_slug' => 'gts',
                'bol_active' => 1,
            ),
            65 => 
            array (
                'id_model' => 3066,
                'id_manufacturer' => 386,
                'str_description' => 'gk',
                'str_description_slug' => 'gk',
                'bol_active' => 1,
            ),
            66 => 
            array (
                'id_model' => 3067,
                'id_manufacturer' => 387,
                'str_description' => '18',
                'str_description_slug' => '18',
                'bol_active' => 1,
            ),
            67 => 
            array (
                'id_model' => 3068,
                'id_manufacturer' => 387,
                'str_description' => '22',
                'str_description_slug' => '22',
                'bol_active' => 1,
            ),
            68 => 
            array (
                'id_model' => 3069,
                'id_manufacturer' => 387,
                'str_description' => '251',
                'str_description_slug' => '251',
                'bol_active' => 1,
            ),
            69 => 
            array (
                'id_model' => 3070,
                'id_manufacturer' => 387,
                'str_description' => '411',
                'str_description_slug' => '411',
                'bol_active' => 1,
            ),
            70 => 
            array (
                'id_model' => 3071,
                'id_manufacturer' => 387,
                'str_description' => '1500',
                'str_description_slug' => '1500',
                'bol_active' => 1,
            ),
            71 => 
            array (
                'id_model' => 3072,
                'id_manufacturer' => 387,
                'str_description' => '1600',
                'str_description_slug' => '1600',
                'bol_active' => 1,
            ),
            72 => 
            array (
                'id_model' => 3073,
                'id_manufacturer' => 387,
                'str_description' => 'amarok',
                'str_description_slug' => 'amarok',
                'bol_active' => 1,
            ),
            73 => 
            array (
                'id_model' => 3074,
                'id_manufacturer' => 387,
                'str_description' => 'atlantic',
                'str_description_slug' => 'atlantic',
                'bol_active' => 1,
            ),
            74 => 
            array (
                'id_model' => 3075,
                'id_manufacturer' => 387,
                'str_description' => 'beetle',
                'str_description_slug' => 'beetle',
                'bol_active' => 1,
            ),
            75 => 
            array (
                'id_model' => 3076,
                'id_manufacturer' => 387,
                'str_description' => 'bestel',
                'str_description_slug' => 'bestel',
                'bol_active' => 1,
            ),
            76 => 
            array (
                'id_model' => 3077,
                'id_manufacturer' => 387,
                'str_description' => 'bora',
                'str_description_slug' => 'bora',
                'bol_active' => 1,
            ),
            77 => 
            array (
                'id_model' => 3078,
                'id_manufacturer' => 387,
                'str_description' => 'brasilia',
                'str_description_slug' => 'brasilia',
                'bol_active' => 1,
            ),
            78 => 
            array (
                'id_model' => 3079,
                'id_manufacturer' => 387,
                'str_description' => 'bus',
                'str_description_slug' => 'bus',
                'bol_active' => 1,
            ),
            79 => 
            array (
                'id_model' => 3080,
                'id_manufacturer' => 387,
                'str_description' => 'caddy',
                'str_description_slug' => 'caddy',
                'bol_active' => 1,
            ),
            80 => 
            array (
                'id_model' => 3081,
                'id_manufacturer' => 387,
                'str_description' => 'caddy maxi',
                'str_description_slug' => 'caddy-maxi',
                'bol_active' => 1,
            ),
            81 => 
            array (
                'id_model' => 3082,
                'id_manufacturer' => 387,
                'str_description' => 'california',
                'str_description_slug' => 'california',
                'bol_active' => 1,
            ),
            82 => 
            array (
                'id_model' => 3083,
                'id_manufacturer' => 387,
                'str_description' => 'caravelle',
                'str_description_slug' => 'caravelle',
                'bol_active' => 1,
            ),
            83 => 
            array (
                'id_model' => 3084,
                'id_manufacturer' => 387,
                'str_description' => 'cc',
                'str_description_slug' => 'cc',
                'bol_active' => 1,
            ),
            84 => 
            array (
                'id_model' => 3085,
                'id_manufacturer' => 387,
                'str_description' => 'coccinelle',
                'str_description_slug' => 'coccinelle',
                'bol_active' => 1,
            ),
            85 => 
            array (
                'id_model' => 3086,
                'id_manufacturer' => 387,
                'str_description' => 'combi',
                'str_description_slug' => 'combi',
                'bol_active' => 1,
            ),
            86 => 
            array (
                'id_model' => 3087,
                'id_manufacturer' => 387,
                'str_description' => 'corrado',
                'str_description_slug' => 'corrado',
                'bol_active' => 1,
            ),
            87 => 
            array (
                'id_model' => 3088,
                'id_manufacturer' => 387,
                'str_description' => 'crafter',
                'str_description_slug' => 'crafter',
                'bol_active' => 1,
            ),
            88 => 
            array (
                'id_model' => 3089,
                'id_manufacturer' => 387,
                'str_description' => 'crossfox',
                'str_description_slug' => 'crossfox',
                'bol_active' => 1,
            ),
            89 => 
            array (
                'id_model' => 3090,
                'id_manufacturer' => 387,
                'str_description' => 'crossgolf',
                'str_description_slug' => 'crossgolf',
                'bol_active' => 1,
            ),
            90 => 
            array (
                'id_model' => 3091,
                'id_manufacturer' => 387,
                'str_description' => 'crosspolo',
                'str_description_slug' => 'crosspolo',
                'bol_active' => 1,
            ),
            91 => 
            array (
                'id_model' => 3092,
                'id_manufacturer' => 387,
                'str_description' => 'crosstouran',
                'str_description_slug' => 'crosstouran',
                'bol_active' => 1,
            ),
            92 => 
            array (
                'id_model' => 3093,
                'id_manufacturer' => 387,
                'str_description' => 'dasher',
                'str_description_slug' => 'dasher',
                'bol_active' => 1,
            ),
            93 => 
            array (
                'id_model' => 3094,
                'id_manufacturer' => 387,
                'str_description' => 'derby',
                'str_description_slug' => 'derby',
                'bol_active' => 1,
            ),
            94 => 
            array (
                'id_model' => 3095,
                'id_manufacturer' => 387,
                'str_description' => 'eos',
                'str_description_slug' => 'eos',
                'bol_active' => 1,
            ),
            95 => 
            array (
                'id_model' => 3096,
                'id_manufacturer' => 387,
                'str_description' => 'escarabajo',
                'str_description_slug' => 'escarabajo',
                'bol_active' => 1,
            ),
            96 => 
            array (
                'id_model' => 3097,
                'id_manufacturer' => 387,
                'str_description' => 'eurovan',
                'str_description_slug' => 'eurovan',
                'bol_active' => 1,
            ),
            97 => 
            array (
                'id_model' => 3098,
                'id_manufacturer' => 387,
                'str_description' => 'fox',
                'str_description_slug' => 'fox',
                'bol_active' => 1,
            ),
            98 => 
            array (
                'id_model' => 3099,
                'id_manufacturer' => 387,
                'str_description' => 'furgoneta',
                'str_description_slug' => 'furgoneta',
                'bol_active' => 1,
            ),
            99 => 
            array (
                'id_model' => 3100,
                'id_manufacturer' => 387,
                'str_description' => 'fusca',
                'str_description_slug' => 'fusca',
                'bol_active' => 1,
            ),
            100 => 
            array (
                'id_model' => 3101,
                'id_manufacturer' => 387,
                'str_description' => 'gol',
                'str_description_slug' => 'gol',
                'bol_active' => 1,
            ),
            101 => 
            array (
                'id_model' => 3102,
                'id_manufacturer' => 387,
                'str_description' => 'golf alltrack',
                'str_description_slug' => 'golf-alltrack',
                'bol_active' => 1,
            ),
            102 => 
            array (
                'id_model' => 3103,
                'id_manufacturer' => 387,
                'str_description' => 'golf plus',
                'str_description_slug' => 'golf-plus',
                'bol_active' => 1,
            ),
            103 => 
            array (
                'id_model' => 3104,
                'id_manufacturer' => 387,
                'str_description' => 'golf sportsvan',
                'str_description_slug' => 'golf-sportsvan',
                'bol_active' => 1,
            ),
            104 => 
            array (
                'id_model' => 3105,
                'id_manufacturer' => 387,
                'str_description' => 'iltis',
                'str_description_slug' => 'iltis',
                'bol_active' => 1,
            ),
            105 => 
            array (
                'id_model' => 3106,
                'id_manufacturer' => 387,
                'str_description' => 'k 70',
                'str_description_slug' => 'k-70',
                'bol_active' => 1,
            ),
            106 => 
            array (
                'id_model' => 3107,
                'id_manufacturer' => 387,
                'str_description' => 'kafer',
                'str_description_slug' => 'kafer',
                'bol_active' => 1,
            ),
            107 => 
            array (
                'id_model' => 3108,
                'id_manufacturer' => 387,
                'str_description' => 'karmann',
                'str_description_slug' => 'karmann',
                'bol_active' => 1,
            ),
            108 => 
            array (
                'id_model' => 3109,
                'id_manufacturer' => 387,
                'str_description' => 'kever',
                'str_description_slug' => 'kever',
                'bol_active' => 1,
            ),
            109 => 
            array (
                'id_model' => 3110,
                'id_manufacturer' => 387,
                'str_description' => 'kleinbus',
                'str_description_slug' => 'kleinbus',
                'bol_active' => 1,
            ),
            110 => 
            array (
                'id_model' => 3111,
                'id_manufacturer' => 387,
                'str_description' => 'kubel',
                'str_description_slug' => 'kubel',
                'bol_active' => 1,
            ),
            111 => 
            array (
                'id_model' => 3112,
                'id_manufacturer' => 387,
                'str_description' => 'lt',
                'str_description_slug' => 'lt',
                'bol_active' => 1,
            ),
            112 => 
            array (
                'id_model' => 3113,
                'id_manufacturer' => 387,
                'str_description' => 'lt40',
                'str_description_slug' => 'lt40',
                'bol_active' => 1,
            ),
            113 => 
            array (
                'id_model' => 3114,
                'id_manufacturer' => 387,
                'str_description' => 'lt45',
                'str_description_slug' => 'lt45',
                'bol_active' => 1,
            ),
            114 => 
            array (
                'id_model' => 3115,
                'id_manufacturer' => 387,
                'str_description' => 'lupo',
                'str_description_slug' => 'lupo',
                'bol_active' => 1,
            ),
            115 => 
            array (
                'id_model' => 3116,
                'id_manufacturer' => 387,
                'str_description' => 'microbus',
                'str_description_slug' => 'microbus',
                'bol_active' => 1,
            ),
            116 => 
            array (
                'id_model' => 3117,
                'id_manufacturer' => 387,
                'str_description' => 'multivan',
                'str_description_slug' => 'multivan',
                'bol_active' => 1,
            ),
            117 => 
            array (
                'id_model' => 3118,
                'id_manufacturer' => 387,
                'str_description' => 'parati',
                'str_description_slug' => 'parati',
                'bol_active' => 1,
            ),
            118 => 
            array (
                'id_model' => 3119,
                'id_manufacturer' => 387,
                'str_description' => 'passat alltrack',
                'str_description_slug' => 'passat-alltrack',
                'bol_active' => 1,
            ),
            119 => 
            array (
                'id_model' => 3120,
                'id_manufacturer' => 387,
                'str_description' => 'passat cc',
                'str_description_slug' => 'passat-cc',
                'bol_active' => 1,
            ),
            120 => 
            array (
                'id_model' => 3121,
                'id_manufacturer' => 387,
                'str_description' => 'phaeton',
                'str_description_slug' => 'phaeton',
                'bol_active' => 1,
            ),
            121 => 
            array (
                'id_model' => 3122,
                'id_manufacturer' => 387,
                'str_description' => 'quantum',
                'str_description_slug' => 'quantum',
                'bol_active' => 1,
            ),
            122 => 
            array (
                'id_model' => 3123,
                'id_manufacturer' => 387,
                'str_description' => 'rabbit',
                'str_description_slug' => 'rabbit',
                'bol_active' => 1,
            ),
            123 => 
            array (
                'id_model' => 3124,
                'id_manufacturer' => 387,
                'str_description' => 'routan',
                'str_description_slug' => 'routan',
                'bol_active' => 1,
            ),
            124 => 
            array (
                'id_model' => 3125,
                'id_manufacturer' => 387,
                'str_description' => 'safari',
                'str_description_slug' => 'safari',
                'bol_active' => 1,
            ),
            125 => 
            array (
                'id_model' => 3126,
                'id_manufacturer' => 387,
                'str_description' => 'santana',
                'str_description_slug' => 'santana',
                'bol_active' => 1,
            ),
            126 => 
            array (
                'id_model' => 3127,
                'id_manufacturer' => 387,
                'str_description' => 'sharan',
                'str_description_slug' => 'sharan',
                'bol_active' => 1,
            ),
            127 => 
            array (
                'id_model' => 3128,
                'id_manufacturer' => 387,
                'str_description' => 'sp2',
                'str_description_slug' => 'sp2',
                'bol_active' => 1,
            ),
            128 => 
            array (
                'id_model' => 3129,
                'id_manufacturer' => 387,
                'str_description' => 'speedster',
                'str_description_slug' => 'speedster',
                'bol_active' => 1,
            ),
            129 => 
            array (
                'id_model' => 3130,
                'id_manufacturer' => 387,
                'str_description' => 'taro',
                'str_description_slug' => 'taro',
                'bol_active' => 1,
            ),
            130 => 
            array (
                'id_model' => 3131,
                'id_manufacturer' => 387,
                'str_description' => 'tc',
                'str_description_slug' => 'tc',
                'bol_active' => 1,
            ),
            131 => 
            array (
                'id_model' => 3132,
                'id_manufacturer' => 387,
                'str_description' => 'tipo 2',
                'str_description_slug' => 'tipo-2',
                'bol_active' => 1,
            ),
            132 => 
            array (
                'id_model' => 3133,
                'id_manufacturer' => 387,
                'str_description' => 'tipo 3',
                'str_description_slug' => 'tipo-3',
                'bol_active' => 1,
            ),
            133 => 
            array (
                'id_model' => 3134,
                'id_manufacturer' => 387,
                'str_description' => 'tipo 4',
                'str_description_slug' => 'tipo-4',
                'bol_active' => 1,
            ),
            134 => 
            array (
                'id_model' => 3135,
                'id_manufacturer' => 387,
                'str_description' => 'tipo 7',
                'str_description_slug' => 'tipo-7',
                'bol_active' => 1,
            ),
            135 => 
            array (
                'id_model' => 3136,
                'id_manufacturer' => 387,
                'str_description' => 'transporter 2010',
                'str_description_slug' => 'transporter-2010',
                'bol_active' => 1,
            ),
            136 => 
            array (
                'id_model' => 3137,
                'id_manufacturer' => 387,
                'str_description' => 'transporter 2015',
                'str_description_slug' => 'transporter-2015',
                'bol_active' => 1,
            ),
            137 => 
            array (
                'id_model' => 3138,
                'id_manufacturer' => 387,
                'str_description' => 'up',
                'str_description_slug' => 'up',
                'bol_active' => 1,
            ),
            138 => 
            array (
                'id_model' => 3139,
                'id_manufacturer' => 387,
                'str_description' => 'vanagon',
                'str_description_slug' => 'vanagon',
                'bol_active' => 1,
            ),
            139 => 
            array (
                'id_model' => 3140,
                'id_manufacturer' => 387,
                'str_description' => 'vento',
                'str_description_slug' => 'vento',
                'bol_active' => 1,
            ),
            140 => 
            array (
                'id_model' => 3141,
                'id_manufacturer' => 388,
                'str_description' => 'vi',
                'str_description_slug' => 'vi',
                'bol_active' => 1,
            ),
            141 => 
            array (
                'id_model' => 3142,
                'id_manufacturer' => 389,
                'str_description' => 'x4',
                'str_description_slug' => 'x4',
                'bol_active' => 1,
            ),
            142 => 
            array (
                'id_model' => 3143,
                'id_manufacturer' => 390,
                'str_description' => '66',
                'str_description_slug' => '66',
                'bol_active' => 1,
            ),
            143 => 
            array (
                'id_model' => 3144,
                'id_manufacturer' => 390,
                'str_description' => '121',
                'str_description_slug' => '121',
                'bol_active' => 1,
            ),
            144 => 
            array (
                'id_model' => 3145,
                'id_manufacturer' => 390,
                'str_description' => '122',
                'str_description_slug' => '122',
                'bol_active' => 1,
            ),
            145 => 
            array (
                'id_model' => 3146,
                'id_manufacturer' => 390,
                'str_description' => '123',
                'str_description_slug' => '123',
                'bol_active' => 1,
            ),
            146 => 
            array (
                'id_model' => 3147,
                'id_manufacturer' => 390,
                'str_description' => '144',
                'str_description_slug' => '144',
                'bol_active' => 1,
            ),
            147 => 
            array (
                'id_model' => 3148,
                'id_manufacturer' => 390,
                'str_description' => '145',
                'str_description_slug' => '145',
                'bol_active' => 1,
            ),
            148 => 
            array (
                'id_model' => 3149,
                'id_manufacturer' => 390,
                'str_description' => '164',
                'str_description_slug' => '164',
                'bol_active' => 1,
            ),
            149 => 
            array (
                'id_model' => 3150,
                'id_manufacturer' => 390,
                'str_description' => '240',
                'str_description_slug' => '240',
                'bol_active' => 1,
            ),
            150 => 
            array (
                'id_model' => 3151,
                'id_manufacturer' => 390,
                'str_description' => '242',
                'str_description_slug' => '242',
                'bol_active' => 1,
            ),
            151 => 
            array (
                'id_model' => 3152,
                'id_manufacturer' => 390,
                'str_description' => '244',
                'str_description_slug' => '244',
                'bol_active' => 1,
            ),
            152 => 
            array (
                'id_model' => 3153,
                'id_manufacturer' => 390,
                'str_description' => '245',
                'str_description_slug' => '245',
                'bol_active' => 1,
            ),
            153 => 
            array (
                'id_model' => 3154,
                'id_manufacturer' => 390,
                'str_description' => '262',
                'str_description_slug' => '262',
                'bol_active' => 1,
            ),
            154 => 
            array (
                'id_model' => 3155,
                'id_manufacturer' => 390,
                'str_description' => '264',
                'str_description_slug' => '264',
                'bol_active' => 1,
            ),
            155 => 
            array (
                'id_model' => 3156,
                'id_manufacturer' => 390,
                'str_description' => '265',
                'str_description_slug' => '265',
                'bol_active' => 1,
            ),
            156 => 
            array (
                'id_model' => 3157,
                'id_manufacturer' => 390,
                'str_description' => '329',
                'str_description_slug' => '329',
                'bol_active' => 1,
            ),
            157 => 
            array (
                'id_model' => 3158,
                'id_manufacturer' => 390,
                'str_description' => '340',
                'str_description_slug' => '340',
                'bol_active' => 1,
            ),
            158 => 
            array (
                'id_model' => 3159,
                'id_manufacturer' => 390,
                'str_description' => '343',
                'str_description_slug' => '343',
                'bol_active' => 1,
            ),
            159 => 
            array (
                'id_model' => 3160,
                'id_manufacturer' => 390,
                'str_description' => '345',
                'str_description_slug' => '345',
                'bol_active' => 1,
            ),
            160 => 
            array (
                'id_model' => 3161,
                'id_manufacturer' => 390,
                'str_description' => '360',
                'str_description_slug' => '360',
                'bol_active' => 1,
            ),
            161 => 
            array (
                'id_model' => 3162,
                'id_manufacturer' => 390,
                'str_description' => '440',
                'str_description_slug' => '440',
                'bol_active' => 1,
            ),
            162 => 
            array (
                'id_model' => 3163,
                'id_manufacturer' => 390,
                'str_description' => '460',
                'str_description_slug' => '460',
                'bol_active' => 1,
            ),
            163 => 
            array (
                'id_model' => 3164,
                'id_manufacturer' => 390,
                'str_description' => '480',
                'str_description_slug' => '480',
                'bol_active' => 1,
            ),
            164 => 
            array (
                'id_model' => 3165,
                'id_manufacturer' => 390,
                'str_description' => '544',
                'str_description_slug' => '544',
                'bol_active' => 1,
            ),
            165 => 
            array (
                'id_model' => 3166,
                'id_manufacturer' => 390,
                'str_description' => '740',
                'str_description_slug' => '740',
                'bol_active' => 1,
            ),
            166 => 
            array (
                'id_model' => 3167,
                'id_manufacturer' => 390,
                'str_description' => '744',
                'str_description_slug' => '744',
                'bol_active' => 1,
            ),
            167 => 
            array (
                'id_model' => 3168,
                'id_manufacturer' => 390,
                'str_description' => '745',
                'str_description_slug' => '745',
                'bol_active' => 1,
            ),
            168 => 
            array (
                'id_model' => 3169,
                'id_manufacturer' => 390,
                'str_description' => '760',
                'str_description_slug' => '760',
                'bol_active' => 1,
            ),
            169 => 
            array (
                'id_model' => 3170,
                'id_manufacturer' => 390,
                'str_description' => '780',
                'str_description_slug' => '780',
                'bol_active' => 1,
            ),
            170 => 
            array (
                'id_model' => 3171,
                'id_manufacturer' => 390,
                'str_description' => '850',
                'str_description_slug' => '850',
                'bol_active' => 1,
            ),
            171 => 
            array (
                'id_model' => 3172,
                'id_manufacturer' => 390,
                'str_description' => '940',
                'str_description_slug' => '940',
                'bol_active' => 1,
            ),
            172 => 
            array (
                'id_model' => 3173,
                'id_manufacturer' => 390,
                'str_description' => '945',
                'str_description_slug' => '945',
                'bol_active' => 1,
            ),
            173 => 
            array (
                'id_model' => 3174,
                'id_manufacturer' => 390,
                'str_description' => '960',
                'str_description_slug' => '960',
                'bol_active' => 1,
            ),
            174 => 
            array (
                'id_model' => 3175,
                'id_manufacturer' => 390,
                'str_description' => 'amazon',
                'str_description_slug' => 'amazon',
                'bol_active' => 1,
            ),
            175 => 
            array (
                'id_model' => 3176,
                'id_manufacturer' => 390,
                'str_description' => 'c30',
                'str_description_slug' => 'c30',
                'bol_active' => 1,
            ),
            176 => 
            array (
                'id_model' => 3177,
                'id_manufacturer' => 390,
                'str_description' => 'c303',
                'str_description_slug' => 'c303',
                'bol_active' => 1,
            ),
            177 => 
            array (
                'id_model' => 3178,
                'id_manufacturer' => 390,
                'str_description' => 'c70',
                'str_description_slug' => 'c70',
                'bol_active' => 1,
            ),
            178 => 
            array (
                'id_model' => 3179,
                'id_manufacturer' => 390,
                'str_description' => 'classic',
                'str_description_slug' => 'classic',
                'bol_active' => 1,
            ),
            179 => 
            array (
                'id_model' => 3180,
                'id_manufacturer' => 390,
                'str_description' => 'p 130',
                'str_description_slug' => 'p-130',
                'bol_active' => 1,
            ),
            180 => 
            array (
                'id_model' => 3181,
                'id_manufacturer' => 390,
                'str_description' => 'p 1800',
                'str_description_slug' => 'p-1800',
                'bol_active' => 1,
            ),
            181 => 
            array (
                'id_model' => 3182,
                'id_manufacturer' => 390,
                'str_description' => 'p 210',
                'str_description_slug' => 'p-210',
                'bol_active' => 1,
            ),
            182 => 
            array (
                'id_model' => 3183,
                'id_manufacturer' => 390,
                'str_description' => 'polar',
                'str_description_slug' => 'polar',
                'bol_active' => 1,
            ),
            183 => 
            array (
                'id_model' => 3184,
                'id_manufacturer' => 390,
                'str_description' => 'pv',
                'str_description_slug' => 'pv',
                'bol_active' => 1,
            ),
            184 => 
            array (
                'id_model' => 3185,
                'id_manufacturer' => 390,
                'str_description' => 's40',
                'str_description_slug' => 's40',
                'bol_active' => 1,
            ),
            185 => 
            array (
                'id_model' => 3186,
                'id_manufacturer' => 390,
                'str_description' => 's60',
                'str_description_slug' => 's60',
                'bol_active' => 1,
            ),
            186 => 
            array (
                'id_model' => 3187,
                'id_manufacturer' => 390,
                'str_description' => 's60 cross country',
                'str_description_slug' => 's60-cross-country',
                'bol_active' => 1,
            ),
            187 => 
            array (
                'id_model' => 3188,
                'id_manufacturer' => 390,
                'str_description' => 's70',
                'str_description_slug' => 's70',
                'bol_active' => 1,
            ),
            188 => 
            array (
                'id_model' => 3189,
                'id_manufacturer' => 390,
                'str_description' => 's80',
                'str_description_slug' => 's80',
                'bol_active' => 1,
            ),
            189 => 
            array (
                'id_model' => 3190,
                'id_manufacturer' => 390,
                'str_description' => 's90',
                'str_description_slug' => 's90',
                'bol_active' => 1,
            ),
            190 => 
            array (
                'id_model' => 3191,
                'id_manufacturer' => 390,
                'str_description' => 'v40',
                'str_description_slug' => 'v40',
                'bol_active' => 1,
            ),
            191 => 
            array (
                'id_model' => 3192,
                'id_manufacturer' => 390,
                'str_description' => 'v40 cross country',
                'str_description_slug' => 'v40-cross-country',
                'bol_active' => 1,
            ),
            192 => 
            array (
                'id_model' => 3193,
                'id_manufacturer' => 390,
                'str_description' => 'v50',
                'str_description_slug' => 'v50',
                'bol_active' => 1,
            ),
            193 => 
            array (
                'id_model' => 3194,
                'id_manufacturer' => 390,
                'str_description' => 'v60',
                'str_description_slug' => 'v60',
                'bol_active' => 1,
            ),
            194 => 
            array (
                'id_model' => 3195,
                'id_manufacturer' => 390,
                'str_description' => 'v60 cross country',
                'str_description_slug' => 'v60-cross-country',
                'bol_active' => 1,
            ),
            195 => 
            array (
                'id_model' => 3196,
                'id_manufacturer' => 390,
                'str_description' => 'v70',
                'str_description_slug' => 'v70',
                'bol_active' => 1,
            ),
            196 => 
            array (
                'id_model' => 3197,
                'id_manufacturer' => 390,
                'str_description' => 'v70 xc cross country',
                'str_description_slug' => 'v70-xc-cross-country',
                'bol_active' => 1,
            ),
            197 => 
            array (
                'id_model' => 3198,
                'id_manufacturer' => 390,
                'str_description' => 'v90',
                'str_description_slug' => 'v90',
                'bol_active' => 1,
            ),
            198 => 
            array (
                'id_model' => 3199,
                'id_manufacturer' => 390,
                'str_description' => 'v90 cross country',
                'str_description_slug' => 'v90-cross-country',
                'bol_active' => 1,
            ),
            199 => 
            array (
                'id_model' => 3200,
                'id_manufacturer' => 390,
                'str_description' => 'xc60',
                'str_description_slug' => 'xc60',
                'bol_active' => 1,
            ),
            200 => 
            array (
                'id_model' => 3201,
                'id_manufacturer' => 390,
                'str_description' => 'xc70',
                'str_description_slug' => 'xc70',
                'bol_active' => 1,
            ),
            201 => 
            array (
                'id_model' => 3202,
                'id_manufacturer' => 390,
                'str_description' => 'xc90',
                'str_description_slug' => 'xc90',
                'bol_active' => 1,
            ),
            202 => 
            array (
                'id_model' => 3203,
                'id_manufacturer' => 391,
                'str_description' => 'gk',
                'str_description_slug' => 'gk',
                'bol_active' => 1,
            ),
            203 => 
            array (
                'id_model' => 3204,
                'id_manufacturer' => 392,
                'str_description' => '223',
                'str_description_slug' => '223',
                'bol_active' => 1,
            ),
            204 => 
            array (
                'id_model' => 3205,
                'id_manufacturer' => 393,
                'str_description' => '353 w',
                'str_description_slug' => '353-w',
                'bol_active' => 1,
            ),
            205 => 
            array (
                'id_model' => 3206,
                'id_manufacturer' => 393,
                'str_description' => 'trabant',
                'str_description_slug' => 'trabant',
                'bol_active' => 1,
            ),
            206 => 
            array (
                'id_model' => 3207,
                'id_manufacturer' => 393,
                'str_description' => 'trans',
                'str_description_slug' => 'trans',
                'bol_active' => 1,
            ),
            207 => 
            array (
                'id_model' => 3208,
                'id_manufacturer' => 394,
                'str_description' => 'megabird',
                'str_description_slug' => 'megabird',
                'bol_active' => 1,
            ),
            208 => 
            array (
                'id_model' => 3209,
                'id_manufacturer' => 394,
                'str_description' => 'megablade',
                'str_description_slug' => 'megablade',
                'bol_active' => 1,
            ),
            209 => 
            array (
                'id_model' => 3210,
                'id_manufacturer' => 394,
                'str_description' => 'megabusa',
                'str_description_slug' => 'megabusa',
                'bol_active' => 1,
            ),
            210 => 
            array (
                'id_model' => 3211,
                'id_manufacturer' => 394,
                'str_description' => 'se',
                'str_description_slug' => 'se',
                'bol_active' => 1,
            ),
            211 => 
            array (
                'id_model' => 3212,
                'id_manufacturer' => 394,
                'str_description' => 'sei',
                'str_description_slug' => 'sei',
                'bol_active' => 1,
            ),
            212 => 
            array (
                'id_model' => 3213,
                'id_manufacturer' => 394,
                'str_description' => 'seight',
                'str_description_slug' => 'seight',
                'bol_active' => 1,
            ),
            213 => 
            array (
                'id_model' => 3214,
                'id_manufacturer' => 394,
                'str_description' => 'sport',
                'str_description_slug' => 'sport',
                'bol_active' => 1,
            ),
            214 => 
            array (
                'id_model' => 3215,
                'id_manufacturer' => 395,
                'str_description' => 'gt',
                'str_description_slug' => 'gt',
                'bol_active' => 1,
            ),
            215 => 
            array (
                'id_model' => 3216,
                'id_manufacturer' => 395,
                'str_description' => 'roadster',
                'str_description_slug' => 'roadster',
                'bol_active' => 1,
            ),
            216 => 
            array (
                'id_model' => 3217,
                'id_manufacturer' => 396,
                'str_description' => 'maxx',
                'str_description_slug' => 'maxx',
                'bol_active' => 1,
            ),
            217 => 
            array (
                'id_model' => 3218,
                'id_manufacturer' => 396,
                'str_description' => 'savage',
                'str_description_slug' => 'savage',
                'bol_active' => 1,
            ),
            218 => 
            array (
                'id_model' => 3219,
                'id_manufacturer' => 397,
                'str_description' => 'cj3',
                'str_description_slug' => 'cj3',
                'bol_active' => 1,
            ),
            219 => 
            array (
                'id_model' => 3220,
                'id_manufacturer' => 397,
                'str_description' => 'cj6',
                'str_description_slug' => 'cj6',
                'bol_active' => 1,
            ),
            220 => 
            array (
                'id_model' => 3221,
                'id_manufacturer' => 397,
                'str_description' => 'mb',
                'str_description_slug' => 'mb',
                'bol_active' => 1,
            ),
            221 => 
            array (
                'id_model' => 3222,
                'id_manufacturer' => 398,
                'str_description' => 'hornet',
                'str_description_slug' => 'hornet',
                'bol_active' => 1,
            ),
            222 => 
            array (
                'id_model' => 3223,
                'id_manufacturer' => 398,
                'str_description' => 'wolseley',
                'str_description_slug' => 'wolseley',
                'bol_active' => 1,
            ),
            223 => 
            array (
                'id_model' => 3224,
                'id_manufacturer' => 399,
                'str_description' => 'xykd',
                'str_description_slug' => 'xykd',
                'bol_active' => 1,
            ),
            224 => 
            array (
                'id_model' => 3225,
                'id_manufacturer' => 400,
                'str_description' => 'go kart',
                'str_description_slug' => 'go-kart',
                'bol_active' => 1,
            ),
            225 => 
            array (
                'id_model' => 3226,
                'id_manufacturer' => 401,
                'str_description' => 'go kart 1100',
                'str_description_slug' => 'go-kart-1100',
                'bol_active' => 1,
            ),
            226 => 
            array (
                'id_model' => 3227,
                'id_manufacturer' => 401,
                'str_description' => 'go kart 500',
                'str_description_slug' => 'go-kart-500',
                'bol_active' => 1,
            ),
            227 => 
            array (
                'id_model' => 3228,
                'id_manufacturer' => 401,
                'str_description' => 'utv',
                'str_description_slug' => 'utv',
                'bol_active' => 1,
            ),
            228 => 
            array (
                'id_model' => 3229,
                'id_manufacturer' => 401,
                'str_description' => 'xy force',
                'str_description_slug' => 'xy-force',
                'bol_active' => 1,
            ),
            229 => 
            array (
                'id_model' => 3230,
                'id_manufacturer' => 402,
                'str_description' => 'serie g',
                'str_description_slug' => 'serie-g',
                'bol_active' => 1,
            ),
            230 => 
            array (
                'id_model' => 3231,
                'id_manufacturer' => 402,
                'str_description' => 'yxm 700',
                'str_description_slug' => 'yxm-700',
                'bol_active' => 1,
            ),
            231 => 
            array (
                'id_model' => 3232,
                'id_manufacturer' => 402,
                'str_description' => 'yxr 660',
                'str_description_slug' => 'yxr-660',
                'bol_active' => 1,
            ),
            232 => 
            array (
                'id_model' => 3233,
                'id_manufacturer' => 402,
                'str_description' => 'yxr 700',
                'str_description_slug' => 'yxr-700',
                'bol_active' => 1,
            ),
            233 => 
            array (
                'id_model' => 3234,
                'id_manufacturer' => 402,
                'str_description' => 'yxz 1000r',
                'str_description_slug' => 'yxz-1000r',
                'bol_active' => 1,
            ),
            234 => 
            array (
                'id_model' => 3235,
                'id_manufacturer' => 403,
                'str_description' => 'dune',
                'str_description_slug' => 'dune',
                'bol_active' => 1,
            ),
            235 => 
            array (
                'id_model' => 3236,
                'id_manufacturer' => 404,
                'str_description' => 'roadster',
                'str_description_slug' => 'roadster',
                'bol_active' => 1,
            ),
            236 => 
            array (
                'id_model' => 3237,
                'id_manufacturer' => 405,
                'str_description' => 'ydgk',
                'str_description_slug' => 'ydgk',
                'bol_active' => 1,
            ),
            237 => 
            array (
                'id_model' => 3238,
                'id_manufacturer' => 406,
                'str_description' => 'go kart',
                'str_description_slug' => 'go-kart',
                'bol_active' => 1,
            ),
            238 => 
            array (
                'id_model' => 3239,
                'id_manufacturer' => 407,
                'str_description' => 'ytk',
                'str_description_slug' => 'ytk',
                'bol_active' => 1,
            ),
            239 => 
            array (
                'id_model' => 3240,
                'id_manufacturer' => 408,
                'str_description' => '45',
                'str_description_slug' => '45',
                'bol_active' => 1,
            ),
            240 => 
            array (
                'id_model' => 3241,
                'id_manufacturer' => 408,
                'str_description' => '55',
                'str_description_slug' => '55',
                'bol_active' => 1,
            ),
            241 => 
            array (
                'id_model' => 3242,
                'id_manufacturer' => 408,
                'str_description' => '65',
                'str_description_slug' => '65',
                'bol_active' => 1,
            ),
            242 => 
            array (
                'id_model' => 3243,
                'id_manufacturer' => 408,
                'str_description' => 'florida',
                'str_description_slug' => 'florida',
                'bol_active' => 1,
            ),
            243 => 
            array (
                'id_model' => 3244,
                'id_manufacturer' => 409,
                'str_description' => 'ymgk',
                'str_description_slug' => 'ymgk',
                'bol_active' => 1,
            ),
            244 => 
            array (
                'id_model' => 3245,
                'id_manufacturer' => 410,
                'str_description' => '750',
                'str_description_slug' => '750',
                'bol_active' => 1,
            ),
            245 => 
            array (
                'id_model' => 3246,
                'id_manufacturer' => 410,
                'str_description' => 'florida',
                'str_description_slug' => 'florida',
                'bol_active' => 1,
            ),
            246 => 
            array (
                'id_model' => 3247,
                'id_manufacturer' => 411,
                'str_description' => 'cabriolet',
                'str_description_slug' => 'cabriolet',
                'bol_active' => 1,
            ),
            247 => 
            array (
                'id_model' => 3248,
                'id_manufacturer' => 411,
                'str_description' => 'pick up',
                'str_description_slug' => 'pick-up',
                'bol_active' => 1,
            ),
            248 => 
            array (
                'id_model' => 3249,
                'id_manufacturer' => 411,
                'str_description' => 'roadster',
                'str_description_slug' => 'roadster',
                'bol_active' => 1,
            ),
            249 => 
            array (
                'id_model' => 3250,
                'id_manufacturer' => 412,
                'str_description' => 'yjgk',
                'str_description_slug' => 'yjgk',
                'bol_active' => 1,
            ),
            250 => 
            array (
                'id_model' => 3251,
                'id_manufacturer' => 413,
                'str_description' => 'golden spirit',
                'str_description_slug' => 'golden-spirit',
                'bol_active' => 1,
            ),
        ));
        
        
    }
}